/*
Navicat MySQL Data Transfer

Source Server         : admin 192.168.0.04
Source Server Version : 50546
Source Host           : 192.168.0.4:3306
Source Database       : qlana

Target Server Type    : MYSQL
Target Server Version : 50546
File Encoding         : 65001

Date: 2016-01-09 20:24:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `approval_role`
-- ----------------------------
DROP TABLE IF EXISTS `approval_role`;
CREATE TABLE `approval_role` (
  `id_approval_role` int(11) NOT NULL AUTO_INCREMENT,
  `approval_name` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `approval_role_code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_approval_role`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of approval_role
-- ----------------------------
INSERT INTO `approval_role` VALUES ('1', 'Account manager', '2015-12-14 13:07:11', 'AM', 'Account manager');
INSERT INTO `approval_role` VALUES ('2', 'Financial Manager', '2015-12-12 14:46:42', 'FM', null);
INSERT INTO `approval_role` VALUES ('3', 'Loan Officers', '2015-12-12 14:47:15', 'LF', null);
INSERT INTO `approval_role` VALUES ('4', 'Bank Manager', '2015-12-12 14:47:24', 'BM', null);

-- ----------------------------
-- Table structure for `attachment`
-- ----------------------------
DROP TABLE IF EXISTS `attachment`;
CREATE TABLE `attachment` (
  `id_attachment` int(11) NOT NULL,
  `crm_contact_id` int(11) DEFAULT NULL,
  `crm_company_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `attachment` text,
  `attachment_type` varchar(255) DEFAULT NULL,
  `comments` text,
  `created_by` int(11) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_attachment`),
  KEY `created_by` (`created_by`),
  KEY `crm_contact_id` (`crm_contact_id`),
  CONSTRAINT `attachment_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`),
  CONSTRAINT `attachment_ibfk_2` FOREIGN KEY (`crm_contact_id`) REFERENCES `crm_contact` (`id_crm_contact`),
  CONSTRAINT `attachment_ibfk_3` FOREIGN KEY (`crm_contact_id`) REFERENCES `crm_company` (`id_crm_company`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of attachment
-- ----------------------------

-- ----------------------------
-- Table structure for `bank_category`
-- ----------------------------
DROP TABLE IF EXISTS `bank_category`;
CREATE TABLE `bank_category` (
  `id_bank_category` int(11) NOT NULL AUTO_INCREMENT,
  `bank_category_name` varchar(50) DEFAULT NULL,
  `bank_category_code` varchar(50) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_bank_category`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bank_category
-- ----------------------------
INSERT INTO `bank_category` VALUES ('1', 'Commercial', 'CMCL', '2015-12-10 16:48:19');
INSERT INTO `bank_category` VALUES ('2', 'Retail', 'RTL', '2015-12-12 14:56:36');
INSERT INTO `bank_category` VALUES ('3', 'Investment', 'INVM', '2015-12-12 14:56:50');
INSERT INTO `bank_category` VALUES ('20', 'Cooperative', 'COP', '2015-12-12 14:57:01');
INSERT INTO `bank_category` VALUES ('21', 'Specialized', 'SP', '2015-12-12 14:57:16');
INSERT INTO `bank_category` VALUES ('22', 'Central', 'CNTL', '2015-12-10 18:32:01');

-- ----------------------------
-- Table structure for `branch_type`
-- ----------------------------
DROP TABLE IF EXISTS `branch_type`;
CREATE TABLE `branch_type` (
  `id_branch_type` int(11) NOT NULL AUTO_INCREMENT,
  `branch_type_name` varchar(50) DEFAULT NULL,
  `branch_type_code` varchar(50) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_branch_type`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of branch_type
-- ----------------------------
INSERT INTO `branch_type` VALUES ('1', 'head office', 'HO854', '2015-12-14 13:02:46', 'head office');
INSERT INTO `branch_type` VALUES ('2', 'divisional', 'DN362', '2015-12-10 16:03:21', null);
INSERT INTO `branch_type` VALUES ('3', 'sub divisional', 'SDN12', '2015-12-10 16:03:38', null);
INSERT INTO `branch_type` VALUES ('4', 'branch', 'BN123', '2015-12-10 18:29:04', null);

-- ----------------------------
-- Table structure for `company`
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `id_company` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(45) DEFAULT NULL,
  `company_logo` varchar(500) DEFAULT NULL,
  `company_address` varchar(45) DEFAULT NULL,
  `company_city` varchar(45) DEFAULT NULL,
  `company_state` varchar(45) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `contact_person_id` int(11) DEFAULT NULL,
  `bank_category_id` int(11) DEFAULT NULL,
  `company_status` tinyint(2) NOT NULL DEFAULT '1',
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `company_caption` varchar(45) NOT NULL,
  PRIMARY KEY (`id_company`) USING BTREE,
  KEY `plan_id` (`plan_id`),
  KEY `country_id` (`country_id`),
  KEY `bank_category_id` (`bank_category_id`),
  KEY `contact_person_id` (`contact_person_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `company_ibfk_1` FOREIGN KEY (`plan_id`) REFERENCES `plan` (`id_plan`),
  CONSTRAINT `company_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `country` (`id_country`),
  CONSTRAINT `company_ibfk_3` FOREIGN KEY (`bank_category_id`) REFERENCES `bank_category` (`id_bank_category`),
  CONSTRAINT `company_ibfk_4` FOREIGN KEY (`contact_person_id`) REFERENCES `user` (`id_user`),
  CONSTRAINT `company_ibfk_5` FOREIGN KEY (`user_id`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of company
-- ----------------------------
INSERT INTO `company` VALUES ('1', 'Q-lana', 'company5_1449214179.jpg', 'US', null, null, '4', '1', null, '1', '1', '2015-12-15 17:16:25', '2', 'customer service');
INSERT INTO `company` VALUES ('3', 'HSBC', 'azri_1449579007.png', 'hyderabad', null, null, '4', '2', null, '1', '1', '2015-12-15 17:16:26', '6', 'customer service');
INSERT INTO `company` VALUES ('4', 'Citi Bank', 'company5_1449214179.jpg', 'Citi Bank', null, null, '4', '3', null, '2', '1', '2015-12-15 17:16:27', '7', 'customer service');
INSERT INTO `company` VALUES ('5', 'Capital One', 'company3_1449214410.jpg', 'Capital one,\r\nsdafsdf,\r\nsdfasdfasdf', null, null, '2', '12', null, '1', '1', '2015-12-15 17:16:27', '8', 'customer service');
INSERT INTO `company` VALUES ('16', 'My own bank', 'SooChow_1449898577.png', 'hyderabad', null, null, '1', '3', null, '2', '1', '2015-12-15 17:16:28', '39', '');
INSERT INTO `company` VALUES ('19', 'IDBI', 'idbi_logo_1449902115.png', 'Jubilee Hills,Hyderabad', null, null, '1', '3', null, '2', '1', '2015-12-15 17:16:29', '42', '');
INSERT INTO `company` VALUES ('20', 'Andra bank', 'andrabank_logo_1449902345.png', 'California', null, null, '2', '12', null, '3', '1', '2015-12-15 17:16:29', '43', '');
INSERT INTO `company` VALUES ('21', 'baroda bank', 'bank-baroda_1449902992.JPG', 'hyderabad', null, null, '1', '2', null, '1', '1', '2015-12-15 17:16:31', '44', '');
INSERT INTO `company` VALUES ('22', 'dharavali', null, 'mumbai', null, null, '1', '2', null, '3', '1', '2015-12-15 17:16:31', '62', '');
INSERT INTO `company` VALUES ('24', 'My New Bank', 'Hydrangeas_1450259994.jpg', 'Sydney', null, null, '2', '12', null, '1', '1', '2015-12-16 15:29:51', '89', '');
INSERT INTO `company` VALUES ('26', 'HSBC', null, 'USA', null, null, '7', '12', null, '1', '1', '2015-12-23 16:19:24', '104', '');

-- ----------------------------
-- Table structure for `company_approval_group`
-- ----------------------------
DROP TABLE IF EXISTS `company_approval_group`;
CREATE TABLE `company_approval_group` (
  `id_approval_group` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `approval_group_name` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id_approval_group`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `company_approval_group_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id_company`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of company_approval_group
-- ----------------------------
INSERT INTO `company_approval_group` VALUES ('1', '1', 'document checking', '2015-12-11 16:11:18', null);

-- ----------------------------
-- Table structure for `company_approval_role`
-- ----------------------------
DROP TABLE IF EXISTS `company_approval_role`;
CREATE TABLE `company_approval_role` (
  `id_company_approval_role` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `approval_group_id` int(11) DEFAULT NULL,
  `approval_role_id` int(11) DEFAULT NULL,
  `reporting_role_id` int(11) DEFAULT NULL,
  `approval_status` varchar(255) DEFAULT 'active',
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_company_approval_role`),
  KEY `company_id` (`company_id`),
  KEY `approval_group_id` (`approval_group_id`),
  CONSTRAINT `company_approval_role_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id_company`),
  CONSTRAINT `company_approval_role_ibfk_3` FOREIGN KEY (`approval_group_id`) REFERENCES `company_approval_group` (`id_approval_group`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of company_approval_role
-- ----------------------------
INSERT INTO `company_approval_role` VALUES ('4', '1', '1', '1', '0', 'active', '2015-12-11 19:15:12');
INSERT INTO `company_approval_role` VALUES ('5', '1', '1', '2', '1', 'active', '2015-12-11 19:15:12');
INSERT INTO `company_approval_role` VALUES ('6', '1', '1', '3', '2', 'active', '2015-12-11 19:15:12');
INSERT INTO `company_approval_role` VALUES ('7', '20', '1', '1', '0', 'active', '2015-12-12 12:18:09');
INSERT INTO `company_approval_role` VALUES ('8', '20', '1', '2', '1', 'active', '2015-12-12 12:18:10');
INSERT INTO `company_approval_role` VALUES ('9', '19', '1', '1', '0', 'active', '2015-12-14 11:47:32');
INSERT INTO `company_approval_role` VALUES ('10', '19', '1', '4', '1', 'active', '2015-12-14 11:47:32');
INSERT INTO `company_approval_role` VALUES ('12', '24', '1', '1', '0', 'active', '2015-12-16 17:12:41');
INSERT INTO `company_approval_role` VALUES ('13', '24', '1', '2', '1', 'active', '2015-12-16 17:12:41');
INSERT INTO `company_approval_role` VALUES ('14', '24', '1', '3', '2', 'active', '2015-12-16 17:12:41');

-- ----------------------------
-- Table structure for `company_branch`
-- ----------------------------
DROP TABLE IF EXISTS `company_branch`;
CREATE TABLE `company_branch` (
  `id_branch` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_type_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `legal_name` varchar(255) DEFAULT NULL,
  `branch_code` varchar(255) DEFAULT NULL,
  `branch_address` varchar(255) DEFAULT NULL,
  `branch_city` varchar(255) DEFAULT NULL,
  `branch_state` varchar(255) DEFAULT '1',
  `branch_logo` varchar(255) DEFAULT NULL,
  `branch_status` tinyint(2) NOT NULL DEFAULT '1',
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reporting_branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_branch`),
  KEY `company_id` (`company_id`),
  KEY `branch_type_id` (`branch_type_id`),
  KEY `country_id` (`country_id`),
  CONSTRAINT `company_branch_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id_company`),
  CONSTRAINT `company_branch_ibfk_2` FOREIGN KEY (`branch_type_id`) REFERENCES `branch_type` (`id_branch_type`),
  CONSTRAINT `company_branch_ibfk_3` FOREIGN KEY (`country_id`) REFERENCES `country` (`id_country`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of company_branch
-- ----------------------------
INSERT INTO `company_branch` VALUES ('1', '3', '1', '1', 'Madhapur Branch', null, 'mumbai', 'hyderabad', 'hyderabad', null, '0', '2015-12-15 17:17:47', null);
INSERT INTO `company_branch` VALUES ('2', '3', '2', '1', 'Ameerpet Branch', null, 'hyderabad', 'hyderabad', 'hyderabad', null, '0', '2015-12-15 17:17:50', null);
INSERT INTO `company_branch` VALUES ('15', '1', '3', '1', 'Begumpet Branch', '123123', 'abdsc, floor,najnzj colonyrer', 'hyderabad', 'hyderabad', 'Branchlogo-default_1449835575.png', '0', '2015-12-15 17:17:55', '15');
INSERT INTO `company_branch` VALUES ('16', '1', '4', '1', 'Brodipet Branch', 'ACD', 'line no 3,brodipet, guntur', 'Guntur g', 'Guntur g', 'Branchlogo-default_1449835575.png', '0', '2015-12-15 17:18:05', '16');
INSERT INTO `company_branch` VALUES ('17', '20', '1', '1', 'Jubille Hills', 'JUB', 'Road No:10', 'Hyderabad', 'Hyderabad', 'idbi_logo_1449903531.png', '0', '2015-12-15 17:18:10', '0');
INSERT INTO `company_branch` VALUES ('18', '1', '1', '1', 'Country Head Office', 'CHO123', 'Begumpet, SP Road, Hyderabad -500035', 'Hyderabad', 'Hyderabad', null, '0', '2015-12-15 17:18:15', '0');
INSERT INTO `company_branch` VALUES ('19', '1', '2', '1', 'Banjarahills', null, 'road no:10,banjarahills.', 'Hyderabad', 'Hyderabad', null, '0', '2015-12-15 17:18:20', '1');
INSERT INTO `company_branch` VALUES ('23', '20', '2', '1', 'Banjarahills', null, 'road no:10,banjarahills.', 'Hyderabad', 'Hyderabad', null, '0', '2015-12-15 17:18:24', '1');
INSERT INTO `company_branch` VALUES ('24', '20', '2', '1', 'Uppal', null, 'uppal', 'Hyderabad', 'Hyderabad', null, '0', '2015-12-15 17:18:29', '18');
INSERT INTO `company_branch` VALUES ('25', '20', '1', '1', 'Ecil', null, 'ecil', 'secunderabad', 'secunderabad', null, '0', '2015-12-15 17:18:33', '18');
INSERT INTO `company_branch` VALUES ('26', '20', '4', '1', 'Lakdikapool', null, 'lakdikapool', 'hyderabad', 'hyderabad', null, '0', '2015-12-15 17:18:36', '16');
INSERT INTO `company_branch` VALUES ('27', '20', '2', '6', 'Cape', null, 'uppal', 'Cape ', 'Cape ', null, '0', '2015-12-15 17:18:40', '18');
INSERT INTO `company_branch` VALUES ('28', '20', '1', '6', 'Johannesburg', null, 'ecil', 'Johannesburg', 'Johannesburg', null, '0', '2015-12-15 17:18:43', '18');
INSERT INTO `company_branch` VALUES ('29', '20', '2', '6', 'Uppal', null, 'uppal', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-15 18:40:48', '18');
INSERT INTO `company_branch` VALUES ('30', '20', '1', '6', 'Ecil', null, 'ecil', 'secunderabad', 'andhrapradesh', null, '1', '2015-12-15 18:40:48', '18');
INSERT INTO `company_branch` VALUES ('31', '20', '2', '1', 'kukatpalli', null, 'kukatpalli', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-15 18:40:48', '1');
INSERT INTO `company_branch` VALUES ('32', '20', '1', '1', 'Miyapur', null, 'miyapur', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-15 18:40:49', '1');
INSERT INTO `company_branch` VALUES ('33', '20', '1', '1', 'Nellore', null, 'nellore', 'Nellore', 'Andhra Pradesh', null, '1', '2015-12-15 18:40:49', '18');
INSERT INTO `company_branch` VALUES ('34', '20', '1', '1', 'Kavali', null, 'kavali', 'kavali', 'Andhra Pradesh', null, '1', '2015-12-15 18:40:49', '18');
INSERT INTO `company_branch` VALUES ('35', '20', '2', '1', 'ongole', null, 'ongole', 'ongole', 'Andhra Pradesh', null, '1', '2015-12-15 18:40:49', '18');
INSERT INTO `company_branch` VALUES ('36', '20', '2', '6', 'Uppal', null, 'uppal', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-16 10:24:18', '18');
INSERT INTO `company_branch` VALUES ('37', '20', '1', '6', 'Ecil', null, 'ecil', 'secunderabad', 'andhrapradesh', null, '1', '2015-12-16 10:24:18', '18');
INSERT INTO `company_branch` VALUES ('38', '20', '2', '1', 'kukatpalli', null, 'kukatpalli', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-16 10:24:18', '1');
INSERT INTO `company_branch` VALUES ('39', '20', '1', '1', 'Miyapur', null, 'miyapur', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-16 10:24:18', '1');
INSERT INTO `company_branch` VALUES ('40', '20', '1', '1', 'Nellore', null, 'nellore', 'Nellore', 'Andhra Pradesh', null, '1', '2015-12-16 10:24:18', '18');
INSERT INTO `company_branch` VALUES ('41', '20', '1', '1', 'Kavali', null, 'kavali', 'kavali', 'Andhra Pradesh', null, '1', '2015-12-16 10:24:18', '18');
INSERT INTO `company_branch` VALUES ('42', '20', '2', '1', 'ongole', null, 'ongole', 'ongole', 'Andhra Pradesh', null, '1', '2015-12-16 10:24:18', '18');
INSERT INTO `company_branch` VALUES ('43', '20', '1', '1', 'karimnagar', null, 'karimnagar', 'karimnagar', 'Telangana', null, '1', '2015-12-16 10:24:18', '18');
INSERT INTO `company_branch` VALUES ('44', '20', '2', '6', 'Uppal', null, 'uppal', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-16 10:26:14', '18');
INSERT INTO `company_branch` VALUES ('45', '20', '1', '6', 'Ecil', null, 'ecil', 'secunderabad', 'andhrapradesh', null, '1', '2015-12-16 10:26:14', '18');
INSERT INTO `company_branch` VALUES ('46', '20', '2', '1', 'kukatpalli', null, 'kukatpalli', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-16 10:26:14', '1');
INSERT INTO `company_branch` VALUES ('47', '20', '1', '1', 'Miyapur', null, 'miyapur', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-16 10:26:14', '1');
INSERT INTO `company_branch` VALUES ('48', '20', '1', '1', 'Nellore', null, 'nellore', 'Nellore', 'Andhra Pradesh', null, '1', '2015-12-16 10:26:14', '18');
INSERT INTO `company_branch` VALUES ('49', '20', '1', '1', 'Kavali', null, 'kavali', 'kavali', 'Andhra Pradesh', null, '1', '2015-12-16 10:26:14', '18');
INSERT INTO `company_branch` VALUES ('50', '20', '2', '1', 'ongole', null, 'ongole', 'ongole', 'Andhra Pradesh', null, '1', '2015-12-16 10:26:14', '18');
INSERT INTO `company_branch` VALUES ('51', '20', '1', '1', 'warangal', null, 'karimnagar', 'karimnagar', 'Telangana', null, '1', '2015-12-16 10:26:14', '18');
INSERT INTO `company_branch` VALUES ('52', '20', '2', '6', 'Uppal', null, 'uppal', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-16 10:27:51', '18');
INSERT INTO `company_branch` VALUES ('53', '20', '1', '6', 'Ecil', null, 'ecil', 'secunderabad', 'andhrapradesh', null, '1', '2015-12-16 10:27:51', '18');
INSERT INTO `company_branch` VALUES ('54', '20', '2', '1', 'kukatpalli', null, 'kukatpalli', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-16 10:27:51', '1');
INSERT INTO `company_branch` VALUES ('55', '20', '1', '1', 'Miyapur', null, 'miyapur', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-16 10:27:51', '1');
INSERT INTO `company_branch` VALUES ('56', '20', '1', '1', 'Nellore', null, 'nellore', 'Nellore', 'Andhra Pradesh', null, '1', '2015-12-16 10:27:51', '18');
INSERT INTO `company_branch` VALUES ('57', '20', '1', '1', 'Kavali', null, 'kavali', 'kavali', 'Andhra Pradesh', null, '1', '2015-12-16 10:27:51', '18');
INSERT INTO `company_branch` VALUES ('58', '20', '2', '1', 'ongole', null, 'ongole', 'ongole', 'Andhra Pradesh', null, '1', '2015-12-16 10:27:51', '18');
INSERT INTO `company_branch` VALUES ('59', '20', '1', '1', 'warangal13212', null, 'karimnagar', 'karimnagar', 'Telangana', null, '1', '2015-12-16 10:27:51', '18');
INSERT INTO `company_branch` VALUES ('60', '20', '2', '6', 'Uppal', null, 'uppal', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-16 10:29:21', '18');
INSERT INTO `company_branch` VALUES ('61', '20', '1', '6', 'Ecil', null, 'ecil', 'secunderabad', 'andhrapradesh', null, '1', '2015-12-16 10:29:21', '18');
INSERT INTO `company_branch` VALUES ('62', '20', '2', '1', 'kukatpalli', null, 'kukatpalli', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-16 10:29:21', '1');
INSERT INTO `company_branch` VALUES ('63', '20', '1', '1', 'Miyapur', null, 'miyapur', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-16 10:29:21', '1');
INSERT INTO `company_branch` VALUES ('64', '20', '1', '1', 'Nellore', null, 'nellore', 'Nellore', 'Andhra Pradesh', null, '1', '2015-12-16 10:29:21', '18');
INSERT INTO `company_branch` VALUES ('65', '20', '1', '1', 'Kavali', null, 'kavali', 'kavali', 'Andhra Pradesh', null, '1', '2015-12-16 10:29:21', '18');
INSERT INTO `company_branch` VALUES ('66', '20', '2', '1', 'ongole', null, 'ongole', 'ongole', 'Andhra Pradesh', null, '1', '2015-12-16 10:29:21', '18');
INSERT INTO `company_branch` VALUES ('67', '20', '1', '1', 'warangal', null, 'karimnagar', 'karimnagar', 'Telangana', null, '1', '2015-12-16 10:29:21', '18');
INSERT INTO `company_branch` VALUES ('68', '20', '2', '6', 'Uppal', null, 'uppal', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-16 10:32:28', '18');
INSERT INTO `company_branch` VALUES ('69', '20', '1', '6', 'Ecil', null, 'ecil', 'secunderabad', 'andhrapradesh', null, '1', '2015-12-16 10:32:28', '18');
INSERT INTO `company_branch` VALUES ('70', '20', '2', '1', 'kukatpalli', null, 'kukatpalli', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-16 10:32:28', '1');
INSERT INTO `company_branch` VALUES ('71', '20', '1', '1', 'Miyapur', null, 'miyapur', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-16 10:32:28', '1');
INSERT INTO `company_branch` VALUES ('72', '20', '1', '1', 'Nellore', null, 'nellore', 'Nellore', 'Andhra Pradesh', null, '1', '2015-12-16 10:32:28', '18');
INSERT INTO `company_branch` VALUES ('73', '20', '1', '1', 'Kavali', null, 'kavali', 'kavali', 'Andhra Pradesh', null, '1', '2015-12-16 10:32:28', '18');
INSERT INTO `company_branch` VALUES ('74', '20', '2', '1', 'ongole', null, 'ongole', 'ongole', 'Andhra Pradesh', null, '1', '2015-12-16 10:32:28', '18');
INSERT INTO `company_branch` VALUES ('75', '20', '1', '1', 'warangal 12312dfgfdg', null, 'karimnagar', 'karimnagar', 'Telangana', null, '1', '2015-12-16 10:32:28', '18');
INSERT INTO `company_branch` VALUES ('76', '24', '1', '1', 'Hyderabad branch', 'HYD', 'Hyderabad', 'Hyderabad', 'Telangana', null, '1', '2015-12-16 17:06:47', '0');
INSERT INTO `company_branch` VALUES ('77', '24', '2', '1', 'Madhapur branch', 'MDP', 'Madhapur', 'Hyderabad', 'Telangana', 'Hydrangeas_1450346155.jpg', '1', '2015-12-17 15:25:50', '77');
INSERT INTO `company_branch` VALUES ('78', '24', '2', '6', 'Uppal', 'null', 'uppal', 'Hyderabad', 'Andhra Pradesh', 'http://localhost/q-lana/rest/images/company-logo.jpg', '1', '2015-12-17 11:00:18', '78');
INSERT INTO `company_branch` VALUES ('79', '24', '1', '6', 'Ecil', null, 'ecil', 'secunderabad', 'andhrapradesh', null, '1', '2015-12-17 10:53:16', '18');
INSERT INTO `company_branch` VALUES ('80', '24', '2', '1', 'kukatpalli', null, 'kukatpalli', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-17 10:53:16', '1');
INSERT INTO `company_branch` VALUES ('81', '24', '1', '1', 'Miyapur', null, 'miyapur', 'Hyderabad', 'Andhra Pradesh', null, '1', '2015-12-17 10:53:16', '1');
INSERT INTO `company_branch` VALUES ('82', '24', '1', '1', 'Nellore', null, 'nellore', 'Nellore', 'Andhra Pradesh', null, '1', '2015-12-17 10:53:16', '18');
INSERT INTO `company_branch` VALUES ('83', '24', '1', '1', 'Kavali', null, 'kavali', 'kavali', 'Andhra Pradesh', null, '1', '2015-12-17 10:53:17', '18');
INSERT INTO `company_branch` VALUES ('84', '24', '2', '1', 'ongole', null, 'ongole', 'ongole', 'Andhra Pradesh', null, '1', '2015-12-17 10:53:17', '18');
INSERT INTO `company_branch` VALUES ('85', '24', '1', '1', 'warangal 12312dfgfdg', null, 'karimnagar', 'karimnagar', 'Telangana', null, '1', '2015-12-17 10:53:17', '18');

-- ----------------------------
-- Table structure for `company_branch_type`
-- ----------------------------
DROP TABLE IF EXISTS `company_branch_type`;
CREATE TABLE `company_branch_type` (
  `id_company_branch_type` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `branch_type_id` int(11) NOT NULL,
  `reporting_branch_type_id` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_company_branch_type`),
  KEY `company_id` (`company_id`),
  KEY `branch_type_id` (`branch_type_id`),
  CONSTRAINT `company_branch_type_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id_company`),
  CONSTRAINT `company_branch_type_ibfk_2` FOREIGN KEY (`branch_type_id`) REFERENCES `branch_type` (`id_branch_type`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of company_branch_type
-- ----------------------------
INSERT INTO `company_branch_type` VALUES ('1', '3', '1', '0', '2015-12-10 16:21:31');
INSERT INTO `company_branch_type` VALUES ('2', '3', '2', '1', '2015-12-10 17:02:07');
INSERT INTO `company_branch_type` VALUES ('3', '3', '3', '2', '2015-12-10 17:02:11');
INSERT INTO `company_branch_type` VALUES ('4', '3', '4', '2', '2015-12-10 17:23:30');
INSERT INTO `company_branch_type` VALUES ('5', '1', '1', '0', '2015-12-11 16:05:04');
INSERT INTO `company_branch_type` VALUES ('6', '1', '2', '1', '2015-12-11 16:05:05');
INSERT INTO `company_branch_type` VALUES ('7', '1', '3', '2', '2015-12-11 16:05:05');
INSERT INTO `company_branch_type` VALUES ('8', '1', '4', '3', '2015-12-11 17:10:09');
INSERT INTO `company_branch_type` VALUES ('19', '20', '1', '0', '2015-12-12 12:20:04');
INSERT INTO `company_branch_type` VALUES ('20', '20', '2', '1', '2015-12-12 12:20:04');
INSERT INTO `company_branch_type` VALUES ('21', '19', '1', '0', '2015-12-14 11:39:35');
INSERT INTO `company_branch_type` VALUES ('22', '19', '3', '1', '2015-12-14 11:39:35');
INSERT INTO `company_branch_type` VALUES ('23', '19', '4', '3', '2015-12-14 11:39:35');
INSERT INTO `company_branch_type` VALUES ('24', '24', '1', '0', '2015-12-16 16:51:03');
INSERT INTO `company_branch_type` VALUES ('25', '24', '2', '1', '2015-12-16 16:51:03');

-- ----------------------------
-- Table structure for `company_user`
-- ----------------------------
DROP TABLE IF EXISTS `company_user`;
CREATE TABLE `company_user` (
  `id_company_user` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `company_approval_role_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `reporting_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_company_user`),
  KEY `user_id` (`user_id`),
  KEY `company_id` (`company_id`),
  KEY `approval_role_id` (`company_approval_role_id`),
  KEY `branch_id` (`branch_id`),
  CONSTRAINT `company_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id_user`),
  CONSTRAINT `company_user_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `company` (`id_company`),
  CONSTRAINT `company_user_ibfk_4` FOREIGN KEY (`branch_id`) REFERENCES `company_branch` (`id_branch`),
  CONSTRAINT `company_user_ibfk_5` FOREIGN KEY (`company_approval_role_id`) REFERENCES `company_approval_role` (`id_company_approval_role`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of company_user
-- ----------------------------
INSERT INTO `company_user` VALUES ('1', '1', '4', '4', '16', '0');
INSERT INTO `company_user` VALUES ('3', '1', '20', null, '16', '0');
INSERT INTO `company_user` VALUES ('13', '16', '39', null, null, null);
INSERT INTO `company_user` VALUES ('16', '19', '42', null, null, null);
INSERT INTO `company_user` VALUES ('19', '1', '56', '5', '15', '0');
INSERT INTO `company_user` VALUES ('20', '1', '57', '4', '15', '57');
INSERT INTO `company_user` VALUES ('21', '1', '58', '5', '16', '0');
INSERT INTO `company_user` VALUES ('22', '1', '59', '5', '15', '0');
INSERT INTO `company_user` VALUES ('24', '20', '61', '8', '17', '0');
INSERT INTO `company_user` VALUES ('25', '22', '62', null, null, null);
INSERT INTO `company_user` VALUES ('44', '1', '87', '6', '1', '43');
INSERT INTO `company_user` VALUES ('46', '24', '89', null, null, null);
INSERT INTO `company_user` VALUES ('48', '24', '91', '12', '76', '0');
INSERT INTO `company_user` VALUES ('49', '24', '92', '13', '76', '91');
INSERT INTO `company_user` VALUES ('50', '1', '93', '6', '15', '57');
INSERT INTO `company_user` VALUES ('51', '24', '94', '12', '1', '4');
INSERT INTO `company_user` VALUES ('52', '24', '95', '12', '2', '2');
INSERT INTO `company_user` VALUES ('53', '24', '96', '12', '17', '6');
INSERT INTO `company_user` VALUES ('54', '24', '97', '13', '19', '87');
INSERT INTO `company_user` VALUES ('55', '24', '98', '13', '24', '2');
INSERT INTO `company_user` VALUES ('56', '24', '99', '13', '15', '4');
INSERT INTO `company_user` VALUES ('57', '24', '100', '12', '16', '87');
INSERT INTO `company_user` VALUES ('58', '24', '101', '12', '27', '2');
INSERT INTO `company_user` VALUES ('59', '24', '102', '13', '76', '91');
INSERT INTO `company_user` VALUES ('61', '26', '104', null, null, null);

-- ----------------------------
-- Table structure for `country`
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `id_country` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(50) DEFAULT NULL,
  `country_code` varchar(50) DEFAULT NULL,
  `country_timezone` varchar(50) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_country`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of country
-- ----------------------------
INSERT INTO `country` VALUES ('1', 'India', 'IND', null, '2015-12-04 12:28:19');
INSERT INTO `country` VALUES ('2', 'Australia', 'AUS', null, '2015-12-04 12:29:05');
INSERT INTO `country` VALUES ('4', 'Zimbabwe', 'ZIM', null, '2015-12-10 18:28:47');
INSERT INTO `country` VALUES ('6', 'South Africa', 'SA', null, '2015-12-08 18:40:33');
INSERT INTO `country` VALUES ('7', 'Newzeland', 'NZ', null, '2015-12-08 18:53:49');

-- ----------------------------
-- Table structure for `crm_company`
-- ----------------------------
DROP TABLE IF EXISTS `crm_company`;
CREATE TABLE `crm_company` (
  `id_crm_company` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `company_name` varchar(225) NOT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `about` text,
  PRIMARY KEY (`id_crm_company`),
  KEY `company_id` (`company_id`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `crm_company_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id_company`),
  CONSTRAINT `crm_company_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crm_company
-- ----------------------------
INSERT INTO `crm_company` VALUES ('22', '24', '89', 'Accesories', '2015-12-22 18:53:58', null, null, null);
INSERT INTO `crm_company` VALUES ('23', '1', '2', 'Accesories', '2015-12-22 18:55:23', null, null, null);
INSERT INTO `crm_company` VALUES ('24', '1', '2', 'dfghgfh', '2015-12-23 10:58:10', null, null, null);
INSERT INTO `crm_company` VALUES ('25', '1', '2', 'Accesorie up', '2016-01-04 17:02:50', null, null, null);
INSERT INTO `crm_company` VALUES ('26', '24', '89', 'Exports', '2015-12-24 12:58:55', null, null, null);
INSERT INTO `crm_company` VALUES ('28', '24', '89', 'Provisions', '2015-12-24 14:25:41', null, null, null);
INSERT INTO `crm_company` VALUES ('29', '1', '2', 'Company1', '2016-01-04 17:18:05', 'company@gmail.com', '955201212', 'asfsf');
INSERT INTO `crm_company` VALUES ('30', '1', '2', 'company2', '2016-01-04 17:18:55', 'comapny2@gmail.com', '99854111', null);
INSERT INTO `crm_company` VALUES ('31', '1', '2', 'Comapnay3', '2016-01-04 17:20:20', 'rakesh@gmail.com', '5465645656', null);
INSERT INTO `crm_company` VALUES ('32', '1', '2', 'Comapnay4', '2016-01-04 17:21:58', 'comapnay4@gmail.com', '444415522', '1125');
INSERT INTO `crm_company` VALUES ('33', '1', '2', 'Comapnay44', '2016-01-04 17:22:29', 'comapnay4@gmail.com', '8525212152', '2123ads');
INSERT INTO `crm_company` VALUES ('34', '1', '2', 'company4', '2016-01-04 17:23:04', 'rakesh.website@gmail.com', '545554', 'sdasdf');
INSERT INTO `crm_company` VALUES ('35', '1', '2', 'Company40', '2016-01-04 17:23:54', 'company1@gmail.com', '12212', '3');
INSERT INTO `crm_company` VALUES ('36', '1', '2', 'test', '2016-01-05 14:28:17', 'company4@gmail.com', '54355541112', '21213');
INSERT INTO `crm_company` VALUES ('37', '1', '2', 'software solutions', '2016-01-09 10:10:53', 'software@gmail.com', '7894561231', null);
INSERT INTO `crm_company` VALUES ('38', '1', '2', 'test789', '2016-01-09 10:13:54', 'testq@gmail.com', '7894561231', null);
INSERT INTO `crm_company` VALUES ('39', '1', '2', 'test68', '2016-01-09 10:14:28', 'theq@gmail.com', '7894561235', null);

-- ----------------------------
-- Table structure for `crm_company_contact`
-- ----------------------------
DROP TABLE IF EXISTS `crm_company_contact`;
CREATE TABLE `crm_company_contact` (
  `id_crm_company_contact` int(11) NOT NULL AUTO_INCREMENT,
  `crm_company_id` int(11) DEFAULT NULL,
  `crm_contact_id` int(11) DEFAULT NULL,
  `crm_company_designation_id` int(11) DEFAULT NULL,
  `is_primary_company` tinyint(4) DEFAULT NULL,
  `crm_company_contact_status` tinyint(4) DEFAULT '1',
  `created_by` int(11) DEFAULT '1',
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_crm_company_contact`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crm_company_contact
-- ----------------------------
INSERT INTO `crm_company_contact` VALUES ('1', '24', '111', '1', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('2', '35', '111', '1', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('3', '22', '111', '1', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('5', '25', '111', '1', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('6', '23', '111', '2', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('7', '30', '111', '2', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('8', '110', '36', '1', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('9', '84', '36', '2', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('10', '84', '36', '2', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('11', '84', '39', '2', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('12', '29', '116', '1', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('13', '22', '116', '1', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('14', '23', '116', '2', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('15', '30', '116', '1', '1', '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('16', '31', '116', '1', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('17', '84', '39', '1', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('18', '111', '39', '1', '1', '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('19', '84', '39', '2', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('20', '84', '39', '2', '1', '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('21', '84', '39', '1', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('22', '39', '84', '2', '1', '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('23', '39', '84', '1', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('24', '39', '110', '2', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('25', '39', '97', '1', '1', '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('26', '39', '80', '1', '1', '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('27', '25', '116', '2', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('28', '33', '116', '2', '1', '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('29', '23', '115', '1', '1', '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('30', '35', '116', '2', '1', '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('31', '39', '116', '2', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('32', '36', '116', '1', '1', '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('33', '38', '116', '2', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('34', '23', '120', '1', '1', '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('35', '1', '121', '2', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('38', '25', '126', '2', null, '1', '2', '2016-01-09 17:50:21');
INSERT INTO `crm_company_contact` VALUES ('39', '22', '127', '1', '0', '0', '2', '2016-01-09 18:36:26');
INSERT INTO `crm_company_contact` VALUES ('40', '25', '127', '2', '0', '1', '2', '2016-01-09 18:46:45');
INSERT INTO `crm_company_contact` VALUES ('41', '37', '127', '3', '0', '0', '2', '2016-01-09 18:34:20');
INSERT INTO `crm_company_contact` VALUES ('42', '23', '127', '1', '0', '1', '2', '2016-01-09 18:36:46');
INSERT INTO `crm_company_contact` VALUES ('43', '32', '127', '1', '0', '0', '2', '2016-01-09 18:40:45');
INSERT INTO `crm_company_contact` VALUES ('44', '30', '127', '2', '0', '1', '2', '2016-01-09 18:50:02');
INSERT INTO `crm_company_contact` VALUES ('45', '31', '127', '2', '0', '0', '2', '2016-01-09 18:41:17');
INSERT INTO `crm_company_contact` VALUES ('46', '35', '127', '1', '1', '1', '2', '2016-01-09 18:50:02');
INSERT INTO `crm_company_contact` VALUES ('47', '29', '127', '2', '0', '1', '2', '2016-01-09 18:50:02');

-- ----------------------------
-- Table structure for `crm_company_data`
-- ----------------------------
DROP TABLE IF EXISTS `crm_company_data`;
CREATE TABLE `crm_company_data` (
  `id_crm_company_data` int(11) NOT NULL AUTO_INCREMENT,
  `crm_company_id` int(11) NOT NULL,
  `form_field_id` int(11) DEFAULT NULL,
  `form_field_value` text,
  `created_date_time` datetime DEFAULT NULL,
  `update_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_crm_company_data`),
  KEY `crm_company_id` (`crm_company_id`),
  KEY `form_field_id` (`form_field_id`),
  CONSTRAINT `crm_company_data_ibfk_1` FOREIGN KEY (`crm_company_id`) REFERENCES `crm_company` (`id_crm_company`),
  CONSTRAINT `crm_company_data_ibfk_2` FOREIGN KEY (`form_field_id`) REFERENCES `form_field` (`id_form_field`)
) ENGINE=InnoDB AUTO_INCREMENT=510 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crm_company_data
-- ----------------------------
INSERT INTO `crm_company_data` VALUES ('26', '22', '46', 'Accesories', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('27', '22', '47', 'yes', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('28', '22', '48', '10/10/1994', null, '2015-12-22 19:01:17');
INSERT INTO `crm_company_data` VALUES ('29', '22', '49', 'Single', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('30', '22', '50', 'fdf', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('31', '22', '51', 'Self', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('32', '22', '52', 'mobiles', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('33', '22', '53', 'Yes', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('34', '22', '54', 'yes', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('35', '22', '55', '76536', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('36', '22', '56', 'Hyderabad', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('37', '22', '57', '1992', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('38', '22', '58', '6574657654', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('39', '22', '59', '65476576', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('40', '22', '60', 'manasa.t@thresholdsoft.com', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('41', '22', '61', '', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('42', '22', '62', '2', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('43', '22', '63', '2', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('44', '22', '64', 'dds', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('45', '22', '65', 'laptops', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('46', '22', '66', '100', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('47', '22', '67', '20', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('48', '22', '68', '20', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('49', '22', '69', '200', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('50', '22', '70', 'gfdshshgfh', null, '2015-12-22 18:53:58');
INSERT INTO `crm_company_data` VALUES ('51', '23', '46', 'Accesories', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('52', '23', '47', 'yes', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('53', '23', '48', '', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('54', '23', '49', 'Single', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('55', '23', '50', 'fdf', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('56', '23', '51', 'Self', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('57', '23', '52', 'mobiles', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('58', '23', '53', 'Yes', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('59', '23', '54', 'yes', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('60', '23', '55', '76536', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('61', '23', '56', 'Hyderabad', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('62', '23', '57', '1992', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('63', '23', '58', '6574657654', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('64', '23', '59', '65476576', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('65', '23', '60', 'manasa.t@thresholdsoft.com', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('66', '23', '61', '', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('67', '23', '62', '2', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('68', '23', '63', '2', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('69', '23', '64', 'dds', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('70', '23', '65', 'laptops', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('71', '23', '66', '100', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('72', '23', '67', '20', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('73', '23', '68', '20', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('74', '23', '69', '200', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('75', '23', '70', 'gfdshshgfh', null, '2015-12-22 18:55:23');
INSERT INTO `crm_company_data` VALUES ('76', '23', '71', '', null, '2015-12-22 18:56:04');
INSERT INTO `crm_company_data` VALUES ('77', '23', '72', '', null, '2015-12-22 18:56:04');
INSERT INTO `crm_company_data` VALUES ('78', '23', '73', '', null, '2015-12-22 18:56:04');
INSERT INTO `crm_company_data` VALUES ('79', '23', '74', '', null, '2015-12-22 18:56:04');
INSERT INTO `crm_company_data` VALUES ('80', '23', '75', '', null, '2015-12-22 18:56:04');
INSERT INTO `crm_company_data` VALUES ('81', '23', '76', '', null, '2015-12-22 18:56:04');
INSERT INTO `crm_company_data` VALUES ('82', '23', '77', '', null, '2015-12-22 18:56:04');
INSERT INTO `crm_company_data` VALUES ('83', '23', '78', '', null, '2015-12-22 18:56:04');
INSERT INTO `crm_company_data` VALUES ('84', '23', '79', '', null, '2015-12-22 18:56:04');
INSERT INTO `crm_company_data` VALUES ('85', '23', '80', '', null, '2015-12-22 18:56:04');
INSERT INTO `crm_company_data` VALUES ('86', '23', '81', '', null, '2015-12-22 18:56:04');
INSERT INTO `crm_company_data` VALUES ('87', '23', '82', '[[]]', null, '2015-12-22 18:56:06');
INSERT INTO `crm_company_data` VALUES ('88', '23', '83', '[[]]', null, '2015-12-22 18:56:07');
INSERT INTO `crm_company_data` VALUES ('89', '23', '84', '[[]]', null, '2015-12-22 18:56:07');
INSERT INTO `crm_company_data` VALUES ('90', '23', '85', '[[]]', null, '2015-12-22 18:56:08');
INSERT INTO `crm_company_data` VALUES ('91', '23', '86', '[[]]', null, '2015-12-22 18:56:09');
INSERT INTO `crm_company_data` VALUES ('92', '22', '71', 'Accessories', null, '2015-12-22 18:56:09');
INSERT INTO `crm_company_data` VALUES ('93', '22', '72', 'mobiles', null, '2015-12-22 18:56:09');
INSERT INTO `crm_company_data` VALUES ('94', '22', '73', 'sales', null, '2015-12-22 18:56:09');
INSERT INTO `crm_company_data` VALUES ('95', '22', '74', 'new', null, '2015-12-22 18:56:09');
INSERT INTO `crm_company_data` VALUES ('96', '22', '75', 'high', null, '2015-12-22 18:56:09');
INSERT INTO `crm_company_data` VALUES ('97', '22', '76', 'high', null, '2015-12-22 18:56:09');
INSERT INTO `crm_company_data` VALUES ('98', '22', '77', 'medium', null, '2015-12-22 18:56:09');
INSERT INTO `crm_company_data` VALUES ('99', '22', '78', 'low', null, '2015-12-22 18:56:09');
INSERT INTO `crm_company_data` VALUES ('100', '22', '79', 'high', null, '2015-12-22 18:56:09');
INSERT INTO `crm_company_data` VALUES ('101', '22', '80', 'production', null, '2015-12-22 18:56:09');
INSERT INTO `crm_company_data` VALUES ('102', '22', '81', 'production', null, '2015-12-22 18:56:09');
INSERT INTO `crm_company_data` VALUES ('103', '23', '87', '[[]]', null, '2015-12-22 18:56:16');
INSERT INTO `crm_company_data` VALUES ('104', '22', '82', '[{\"question\":\"warranty\",\"explanation\":\"yes\"},[]]', null, '2015-12-22 18:56:24');
INSERT INTO `crm_company_data` VALUES ('105', '22', '83', '[{\"suppliers\":\"suppliers\",\"comments\":\"comments\"},[]]', null, '2015-12-22 18:56:40');
INSERT INTO `crm_company_data` VALUES ('106', '22', '84', '[{\"substitutes\":\"substitutes\",\"comments\":\"yes\"},[]]', null, '2015-12-22 18:56:50');
INSERT INTO `crm_company_data` VALUES ('107', '22', '85', '[{\"buyers\":\"buyers\",\"comments\":\"comments\"},[]]', null, '2015-12-22 18:57:03');
INSERT INTO `crm_company_data` VALUES ('108', '22', '86', '[{\"newentrants\":\"New entrant1\",\"comments\":\"comment\"},[]]', null, '2015-12-22 18:57:21');
INSERT INTO `crm_company_data` VALUES ('109', '22', '87', '[{\"buyers\":\"cash inflow1\",\"month1\":\"343\",\"month2\":\"342\",\"month3\":\"4324\",\"month4\":\"4324\",\"month5\":\"4243\",\"month6\":\"424\",\"month7\":\"4243\",\"month8\":\"4324\",\"month9\":\"532\",\"month10\":\"4324\",\"month12\":\"4324\"},[]]', null, '2015-12-22 18:57:47');
INSERT INTO `crm_company_data` VALUES ('110', '22', '88', '[{\"cashoutflows\":\"cash outflow\",\"month1\":\"32432\",\"month2\":\"43232\",\"month3\":\"4324\",\"month4\":\"432423\",\"month5\":\"5324\",\"month6\":\"3243\",\"month7\":\"443\",\"month8\":\"4343\",\"month9\":\"43432\",\"month10\":\"432\",\"month12\":\"43\"},[]]', null, '2015-12-22 18:58:11');
INSERT INTO `crm_company_data` VALUES ('111', '22', '89', '[{\"buyers\":\"financial indicator\",\"month1\":\"4242\",\"month3\":\"4343\",\"month2\":\"42355\",\"month5\":\"4554\",\"month4\":\"5454\",\"month6\":\"533\",\"month7\":\"6546\",\"month8\":\"546\",\"month9\":\"5435\",\"month10\":\"543543\",\"month12\":\"5435\"},[]]', null, '2015-12-22 18:58:44');
INSERT INTO `crm_company_data` VALUES ('112', '22', '90', 'Marketing', null, '2015-12-22 18:59:30');
INSERT INTO `crm_company_data` VALUES ('113', '22', '91', 'tangible', null, '2015-12-22 18:59:30');
INSERT INTO `crm_company_data` VALUES ('114', '22', '92', 'normal', null, '2015-12-22 18:59:30');
INSERT INTO `crm_company_data` VALUES ('115', '22', '93', 'marketing', null, '2015-12-22 18:59:30');
INSERT INTO `crm_company_data` VALUES ('116', '22', '94', '5 years', null, '2015-12-22 18:59:30');
INSERT INTO `crm_company_data` VALUES ('117', '22', '95', 'old', null, '2015-12-22 19:00:06');
INSERT INTO `crm_company_data` VALUES ('118', '22', '96', 'accounting', null, '2015-12-22 19:00:06');
INSERT INTO `crm_company_data` VALUES ('119', '22', '97', 'dffds', null, '2015-12-22 19:00:06');
INSERT INTO `crm_company_data` VALUES ('120', '22', '98', 'fsd', null, '2015-12-22 19:00:06');
INSERT INTO `crm_company_data` VALUES ('121', '22', '99', 'production', null, '2015-12-22 19:00:06');
INSERT INTO `crm_company_data` VALUES ('122', '22', '100', 'fwe', null, '2015-12-22 19:00:18');
INSERT INTO `crm_company_data` VALUES ('123', '22', '101', 'ewre', null, '2015-12-22 19:00:18');
INSERT INTO `crm_company_data` VALUES ('124', '22', '102', 'rewr', null, '2015-12-22 19:00:18');
INSERT INTO `crm_company_data` VALUES ('125', '22', '103', 'rew', null, '2015-12-22 19:00:18');
INSERT INTO `crm_company_data` VALUES ('126', '22', '104', 'fse', null, '2015-12-22 19:00:18');
INSERT INTO `crm_company_data` VALUES ('127', '22', '105', 'dsfdf', null, '2015-12-22 19:00:18');
INSERT INTO `crm_company_data` VALUES ('128', '22', '106', 'fgdwre', null, '2015-12-22 19:00:24');
INSERT INTO `crm_company_data` VALUES ('129', '22', '107', 'trewtr', null, '2015-12-22 19:00:24');
INSERT INTO `crm_company_data` VALUES ('130', '22', '108', 'erte', null, '2015-12-22 19:00:24');
INSERT INTO `crm_company_data` VALUES ('131', '22', '109', 'rewtre', null, '2015-12-22 19:00:24');
INSERT INTO `crm_company_data` VALUES ('132', '22', '110', 'tretre', null, '2015-12-22 19:00:24');
INSERT INTO `crm_company_data` VALUES ('133', '24', '46', 'Business Name', null, '2015-12-28 19:41:25');
INSERT INTO `crm_company_data` VALUES ('134', '24', '47', 'yes', null, '2015-12-29 11:02:01');
INSERT INTO `crm_company_data` VALUES ('135', '24', '48', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('136', '24', '49', 'Owner', null, '2015-12-29 11:02:01');
INSERT INTO `crm_company_data` VALUES ('137', '24', '50', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('138', '24', '51', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('139', '24', '52', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('140', '24', '53', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('141', '24', '54', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('142', '24', '55', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('143', '24', '56', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('144', '24', '57', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('145', '24', '58', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('146', '24', '59', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('147', '24', '60', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('148', '24', '61', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('149', '24', '62', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('150', '24', '63', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('151', '24', '64', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('152', '24', '65', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('153', '24', '66', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('154', '24', '67', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('155', '24', '68', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('156', '24', '69', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('157', '24', '70', '', null, '2015-12-23 10:58:10');
INSERT INTO `crm_company_data` VALUES ('158', '25', '46', 'Accesorie up', null, '2016-01-04 17:02:50');
INSERT INTO `crm_company_data` VALUES ('159', '25', '47', 'yes', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('160', '25', '48', '10/10/1994', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('161', '25', '49', 'Single', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('162', '25', '50', 'fdf', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('163', '25', '51', 'Self', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('164', '25', '52', 'mobiles', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('165', '25', '53', 'Yes', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('166', '25', '54', 'yes', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('167', '25', '55', '76536', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('168', '25', '56', 'Hyderabad', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('169', '25', '57', '1992', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('170', '25', '58', '6574657654', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('171', '25', '59', '65476576', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('172', '25', '60', 'manasa.t@thresholdsoft.com', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('173', '25', '61', '', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('174', '25', '62', '2', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('175', '25', '63', '2', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('176', '25', '64', 'dds', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('177', '25', '65', 'laptops', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('178', '25', '66', '100', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('179', '25', '67', '20', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('180', '25', '68', '20', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('181', '25', '69', '200', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('182', '25', '70', 'gfdshshgfh', null, '2015-12-23 15:46:04');
INSERT INTO `crm_company_data` VALUES ('352', '25', '71', 'History of the Business - Explain the history of the business', null, '2015-12-28 19:39:58');
INSERT INTO `crm_company_data` VALUES ('353', '25', '72', '', null, '2015-12-28 19:39:45');
INSERT INTO `crm_company_data` VALUES ('354', '25', '73', '', null, '2015-12-28 19:39:45');
INSERT INTO `crm_company_data` VALUES ('355', '25', '74', '', null, '2015-12-28 19:39:45');
INSERT INTO `crm_company_data` VALUES ('356', '25', '75', '', null, '2015-12-28 19:39:45');
INSERT INTO `crm_company_data` VALUES ('357', '25', '76', '', null, '2015-12-28 19:39:45');
INSERT INTO `crm_company_data` VALUES ('358', '25', '77', '', null, '2015-12-28 19:39:45');
INSERT INTO `crm_company_data` VALUES ('359', '25', '78', '', null, '2015-12-28 19:39:45');
INSERT INTO `crm_company_data` VALUES ('360', '25', '79', '', null, '2015-12-28 19:39:45');
INSERT INTO `crm_company_data` VALUES ('361', '25', '80', '', null, '2015-12-28 19:39:45');
INSERT INTO `crm_company_data` VALUES ('362', '25', '81', 'Operations - production technology/MIS', null, '2015-12-28 19:39:45');
INSERT INTO `crm_company_data` VALUES ('363', '24', '71', 'History of the Business - Explain the history of the business', null, '2015-12-28 19:40:18');
INSERT INTO `crm_company_data` VALUES ('364', '24', '72', '', null, '2015-12-28 19:40:18');
INSERT INTO `crm_company_data` VALUES ('365', '24', '73', '', null, '2015-12-28 19:40:18');
INSERT INTO `crm_company_data` VALUES ('366', '24', '74', '', null, '2015-12-28 19:40:18');
INSERT INTO `crm_company_data` VALUES ('367', '24', '75', '', null, '2015-12-28 19:40:18');
INSERT INTO `crm_company_data` VALUES ('368', '24', '76', '', null, '2015-12-28 19:40:18');
INSERT INTO `crm_company_data` VALUES ('369', '24', '77', '', null, '2015-12-28 19:40:18');
INSERT INTO `crm_company_data` VALUES ('370', '24', '78', '', null, '2015-12-28 19:40:18');
INSERT INTO `crm_company_data` VALUES ('371', '24', '79', '', null, '2015-12-28 19:40:18');
INSERT INTO `crm_company_data` VALUES ('372', '24', '80', '', null, '2015-12-28 19:40:18');
INSERT INTO `crm_company_data` VALUES ('373', '24', '81', '', null, '2015-12-28 19:40:18');
INSERT INTO `crm_company_data` VALUES ('374', '34', '46', 'sdafsdf', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('375', '34', '47', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('376', '34', '48', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('377', '34', '49', 'sadfasdf', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('378', '34', '50', 'fsdfa', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('379', '34', '51', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('380', '34', '52', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('381', '34', '53', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('382', '34', '54', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('383', '34', '55', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('384', '34', '56', '321-2210-+2110', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('385', '34', '57', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('386', '34', '58', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('387', '34', '59', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('388', '34', '60', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('389', '34', '61', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('390', '34', '62', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('391', '34', '63', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('392', '34', '64', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('393', '34', '65', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('394', '34', '66', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('395', '34', '67', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('396', '34', '68', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('397', '34', '69', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('398', '34', '70', '', null, '2016-01-04 17:23:25');
INSERT INTO `crm_company_data` VALUES ('399', '36', '46', 'test', null, '2016-01-05 14:28:17');
INSERT INTO `crm_company_data` VALUES ('400', '36', '47', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('401', '36', '48', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('402', '36', '49', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('403', '36', '50', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('404', '36', '51', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('405', '36', '52', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('406', '36', '53', 'No', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('407', '36', '54', 'Recording Facility', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('408', '36', '55', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('409', '36', '56', 'Business Physical Address*', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('410', '36', '57', 'Operating in this place since:', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('411', '36', '58', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('412', '36', '59', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('413', '36', '60', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('414', '36', '61', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('415', '36', '62', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('416', '36', '63', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('417', '36', '64', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('418', '36', '65', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('419', '36', '66', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('420', '36', '67', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('421', '36', '68', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('422', '36', '69', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('423', '36', '70', '', null, '2016-01-04 17:38:49');
INSERT INTO `crm_company_data` VALUES ('424', '36', '71', 'History of the Business - Explain the history of the business', null, '2016-01-04 17:38:57');
INSERT INTO `crm_company_data` VALUES ('425', '36', '72', 'Products, describe the products and services of the company', null, '2016-01-04 17:38:57');
INSERT INTO `crm_company_data` VALUES ('426', '36', '73', 'Markets - define the target markets and market segments in which the company competes', null, '2016-01-04 17:38:57');
INSERT INTO `crm_company_data` VALUES ('427', '36', '74', '', null, '2016-01-04 17:38:57');
INSERT INTO `crm_company_data` VALUES ('428', '36', '75', '', null, '2016-01-04 17:38:57');
INSERT INTO `crm_company_data` VALUES ('429', '36', '76', '', null, '2016-01-04 17:38:57');
INSERT INTO `crm_company_data` VALUES ('430', '36', '77', '', null, '2016-01-04 17:38:57');
INSERT INTO `crm_company_data` VALUES ('431', '36', '78', '', null, '2016-01-04 17:38:57');
INSERT INTO `crm_company_data` VALUES ('432', '36', '79', '', null, '2016-01-04 17:38:57');
INSERT INTO `crm_company_data` VALUES ('433', '36', '80', '', null, '2016-01-04 17:38:57');
INSERT INTO `crm_company_data` VALUES ('434', '36', '81', '', null, '2016-01-04 17:38:57');
INSERT INTO `crm_company_data` VALUES ('435', '37', '46', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('436', '37', '47', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('437', '37', '48', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('438', '37', '49', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('439', '37', '50', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('440', '37', '51', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('441', '37', '52', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('442', '37', '53', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('443', '37', '54', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('444', '37', '55', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('445', '37', '56', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('446', '37', '57', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('447', '37', '58', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('448', '37', '59', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('449', '37', '60', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('450', '37', '61', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('451', '37', '62', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('452', '37', '63', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('453', '37', '64', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('454', '37', '65', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('455', '37', '66', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('456', '37', '67', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('457', '37', '68', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('458', '37', '69', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('459', '37', '70', '', null, '2016-01-09 10:10:53');
INSERT INTO `crm_company_data` VALUES ('460', '38', '46', 'test789', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('461', '38', '47', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('462', '38', '48', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('463', '38', '49', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('464', '38', '50', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('465', '38', '51', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('466', '38', '52', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('467', '38', '53', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('468', '38', '54', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('469', '38', '55', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('470', '38', '56', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('471', '38', '57', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('472', '38', '58', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('473', '38', '59', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('474', '38', '60', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('475', '38', '61', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('476', '38', '62', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('477', '38', '63', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('478', '38', '64', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('479', '38', '65', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('480', '38', '66', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('481', '38', '67', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('482', '38', '68', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('483', '38', '69', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('484', '38', '70', '', null, '2016-01-09 10:13:54');
INSERT INTO `crm_company_data` VALUES ('485', '39', '46', 'test68', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('486', '39', '47', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('487', '39', '48', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('488', '39', '49', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('489', '39', '50', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('490', '39', '51', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('491', '39', '52', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('492', '39', '53', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('493', '39', '54', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('494', '39', '55', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('495', '39', '56', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('496', '39', '57', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('497', '39', '58', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('498', '39', '59', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('499', '39', '60', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('500', '39', '61', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('501', '39', '62', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('502', '39', '63', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('503', '39', '64', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('504', '39', '65', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('505', '39', '66', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('506', '39', '67', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('507', '39', '68', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('508', '39', '69', '', null, '2016-01-09 10:14:28');
INSERT INTO `crm_company_data` VALUES ('509', '39', '70', '', null, '2016-01-09 10:14:28');

-- ----------------------------
-- Table structure for `crm_company_designation`
-- ----------------------------
DROP TABLE IF EXISTS `crm_company_designation`;
CREATE TABLE `crm_company_designation` (
  `id_crm_company_designation` int(11) NOT NULL AUTO_INCREMENT,
  `designation_name` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_crm_company_designation`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crm_company_designation
-- ----------------------------
INSERT INTO `crm_company_designation` VALUES ('1', 'manager', '2016-01-07 12:00:57');
INSERT INTO `crm_company_designation` VALUES ('2', 'accountent', '2016-01-07 12:01:09');
INSERT INTO `crm_company_designation` VALUES ('3', 'employee', '2016-01-07 12:01:31');

-- ----------------------------
-- Table structure for `crm_contact`
-- ----------------------------
DROP TABLE IF EXISTS `crm_contact`;
CREATE TABLE `crm_contact` (
  `id_crm_contact` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_number` bigint(20) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `contact_status` tinyint(4) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `about` text,
  `designation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_crm_contact`),
  KEY `company_id` (`company_id`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `crm_contact_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id_company`),
  CONSTRAINT `crm_contact_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crm_contact
-- ----------------------------
INSERT INTO `crm_contact` VALUES ('80', '1', '2', 'ravi', 'kumar', 'ravikumar@gmail.com', '950066451', 'threshold', null, null, '2015-12-31 19:01:07', null, null);
INSERT INTO `crm_contact` VALUES ('81', '1', '2', 'priya', 'anand', 'priya@gmail.com', '4567891235', 'tech soft', null, null, '2015-12-31 19:01:12', null, null);
INSERT INTO `crm_contact` VALUES ('84', '1', '2', 'chandra', 'sekhar', 'chandra@gmail.com', '789456123', null, null, null, '2015-12-21 18:10:35', null, null);
INSERT INTO `crm_contact` VALUES ('85', '1', '2', 'ram', 'krishna', 'krishna@gmail.com', null, null, null, null, '2015-12-24 15:12:49', null, null);
INSERT INTO `crm_contact` VALUES ('86', '1', '2', 'Rakesh', 'kumar', 'kumar@gmail.com', null, null, null, null, '2015-12-24 15:12:57', null, null);
INSERT INTO `crm_contact` VALUES ('90', '24', '89', 'Rakesh', 'Kumar', null, null, null, null, null, '2015-12-22 17:47:00', null, null);
INSERT INTO `crm_contact` VALUES ('91', '24', '89', 'Manasa', 'Thummala', 'manasa.t@thresholdsoft.com', '9876543210', null, '', null, '2015-12-24 12:55:58', null, null);
INSERT INTO `crm_contact` VALUES ('92', '1', '2', 'Ravi', 'Kumar', 'rk@mail.net', '96969696989', null, '', null, '2015-12-24 15:21:05', null, null);
INSERT INTO `crm_contact` VALUES ('93', '1', '2', 'Ravi', 'Kumar', 'rk@mail.net', '96969696989', null, '', null, '2015-12-24 15:21:10', null, null);
INSERT INTO `crm_contact` VALUES ('94', '1', '2', 'Ramesh', 'kumar', 'tes23@hml.com', '96969696989', '28centuries.pvt.lit', null, null, '2015-12-31 19:02:34', null, null);
INSERT INTO `crm_contact` VALUES ('95', '1', '2', 'Mohan', 'Babu', 'test2@gmail.eom', '96969696989', '50centuries.pvt.lit', null, null, '2015-12-31 19:02:27', null, null);
INSERT INTO `crm_contact` VALUES ('96', '1', '2', 'naresh', 'gurrala', 'test6@gmail.com', '96969696989', 'c21enturies.pvt.lit', null, null, '2015-12-31 19:02:21', null, null);
INSERT INTO `crm_contact` VALUES ('97', '1', '2', 'rakesh', 'kuamr', 'test5@gmail.com', '212122215', 'centuries.pvt.lit', '', null, '2015-12-31 19:01:57', null, null);
INSERT INTO `crm_contact` VALUES ('104', '1', '2', 'Ramesh', 'Kim', 'tset5@test.com', '54164566', 'centuries.pvt.lit', null, null, '2015-12-31 19:01:55', null, null);
INSERT INTO `crm_contact` VALUES ('105', '1', '2', 'Mahesh', 'test', 'tes6@test.com', '262353363', 'soft pro', null, null, '2015-12-31 19:01:17', null, null);
INSERT INTO `crm_contact` VALUES ('108', '1', '2', 'Rakesh', 'Kumar', 'rakesh!@gmai.com', '456132134', 'thresholdsoft.com', '', null, '2015-12-31 19:06:41', null, null);
INSERT INTO `crm_contact` VALUES ('109', '1', '2', 'Murali', 'Krishana', 'murali@krishan.com', '9564561261', 'Threshold soft pro', '', null, '2016-01-04 15:51:36', '', 'UI Developer');
INSERT INTO `crm_contact` VALUES ('110', '1', '2', 'Bhushan', 'Babu', 'bhushan@babu.com', '9554541', 'nextGen', '', null, '2016-01-04 15:59:03', null, 'Market Excute');
INSERT INTO `crm_contact` VALUES ('111', '1', '2', 'chandra', 'sekhar', 'test2.user@gmail.com', '9564561261', 'nextGen', '', null, '2016-01-04 16:00:39', '', 'UI Developer');
INSERT INTO `crm_contact` VALUES ('112', '1', '2', 'bhasha', 'shaik', 'bhasha.s@thresholdsoft.com', '7894561231', 'test', '', null, '2016-01-08 19:47:34', null, null);
INSERT INTO `crm_contact` VALUES ('113', '1', '2', 'bhasha', 'shaik', 'bhasha.s@thresholdsoft.com', '7894561231', 'test', '', null, '2016-01-08 19:49:12', null, null);
INSERT INTO `crm_contact` VALUES ('114', '1', '2', 'bhasha', 'shaik', 'bhasha.s@thresholdsoft.com', '7894561231', 'test', '', null, '2016-01-08 19:50:10', null, null);
INSERT INTO `crm_contact` VALUES ('115', '1', '2', 'bhasha', 'shaik', 'bhasha.s@thresholdsoft.com', '7894561231', 'test', '', null, '2016-01-08 19:50:30', null, null);
INSERT INTO `crm_contact` VALUES ('116', '1', '2', 'sthanka', 'shaik', 'bhasha.s1@thresholdsoft.com', '7894561231', 'sdfcdas', '', null, '2016-01-08 19:58:10', null, null);

-- ----------------------------
-- Table structure for `crm_contact_data`
-- ----------------------------
DROP TABLE IF EXISTS `crm_contact_data`;
CREATE TABLE `crm_contact_data` (
  `id_crm_contact_data` int(11) NOT NULL AUTO_INCREMENT,
  `crm_contact_id` int(11) DEFAULT NULL,
  `form_field_id` int(11) DEFAULT NULL,
  `form_field_value` text,
  `created_date_time` datetime DEFAULT NULL,
  `update_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_crm_contact_data`),
  KEY `crm_contact_id` (`crm_contact_id`),
  KEY `form_field_id` (`form_field_id`),
  CONSTRAINT `crm_contact_data_ibfk_1` FOREIGN KEY (`crm_contact_id`) REFERENCES `crm_contact` (`id_crm_contact`),
  CONSTRAINT `crm_contact_data_ibfk_2` FOREIGN KEY (`form_field_id`) REFERENCES `form_field` (`id_form_field`)
) ENGINE=InnoDB AUTO_INCREMENT=694 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crm_contact_data
-- ----------------------------
INSERT INTO `crm_contact_data` VALUES ('1', '80', '1', 'p ravi', null, '2015-12-23 15:45:29');
INSERT INTO `crm_contact_data` VALUES ('2', '80', '2', 'kumar', null, '2015-12-21 15:17:36');
INSERT INTO `crm_contact_data` VALUES ('3', '80', '3', 'Male', null, '2015-12-21 15:17:36');
INSERT INTO `crm_contact_data` VALUES ('4', '80', '4', '2015-12-20T18:30:00.000Z', null, '2015-12-21 17:19:21');
INSERT INTO `crm_contact_data` VALUES ('5', '80', '5', 'hyderabad', null, '2015-12-21 15:17:36');
INSERT INTO `crm_contact_data` VALUES ('6', '80', '6', '45621', null, '2015-12-21 15:17:36');
INSERT INTO `crm_contact_data` VALUES ('7', '80', '7', 'hyderabad', null, '2015-12-21 15:17:36');
INSERT INTO `crm_contact_data` VALUES ('8', '80', '8', 'No', null, '2015-12-21 15:17:36');
INSERT INTO `crm_contact_data` VALUES ('9', '80', '9', 'btech', null, '2015-12-21 15:17:36');
INSERT INTO `crm_contact_data` VALUES ('10', '80', '10', 'working', null, '2015-12-21 15:17:36');
INSERT INTO `crm_contact_data` VALUES ('11', '80', '11', 'software', null, '2015-12-21 15:17:36');
INSERT INTO `crm_contact_data` VALUES ('12', '80', '12', 'software', null, '2015-12-21 15:20:56');
INSERT INTO `crm_contact_data` VALUES ('13', '80', '13', 'Yes', null, '2015-12-21 15:17:36');
INSERT INTO `crm_contact_data` VALUES ('14', '80', '14', 'Yes', null, '2015-12-21 15:17:36');
INSERT INTO `crm_contact_data` VALUES ('15', '80', '15', 'bhasha', null, '2015-12-21 15:22:10');
INSERT INTO `crm_contact_data` VALUES ('16', '80', '16', '524', null, '2015-12-21 15:18:54');
INSERT INTO `crm_contact_data` VALUES ('17', '80', '17', 'Bhash', null, '2015-12-21 15:18:54');
INSERT INTO `crm_contact_data` VALUES ('18', '80', '18', 'yes', null, '2015-12-21 16:01:20');
INSERT INTO `crm_contact_data` VALUES ('19', '80', '19', 'yes', null, '2015-12-21 16:01:20');
INSERT INTO `crm_contact_data` VALUES ('20', '80', '20', 'B-tech', null, '2015-12-21 16:01:20');
INSERT INTO `crm_contact_data` VALUES ('21', '80', '21', 'guntoor', null, '2015-12-21 16:01:20');
INSERT INTO `crm_contact_data` VALUES ('22', '80', '22', 'hyd', null, '2015-12-21 16:01:20');
INSERT INTO `crm_contact_data` VALUES ('23', '80', '23', 'Yes', null, '2015-12-21 17:20:16');
INSERT INTO `crm_contact_data` VALUES ('24', '80', '24', '1123123', null, '2015-12-21 16:01:20');
INSERT INTO `crm_contact_data` VALUES ('25', '80', '25', '12', null, '2015-12-21 17:20:08');
INSERT INTO `crm_contact_data` VALUES ('26', '80', '26', '1', null, '2015-12-21 15:21:39');
INSERT INTO `crm_contact_data` VALUES ('27', '80', '27', '1', null, '2015-12-21 15:21:39');
INSERT INTO `crm_contact_data` VALUES ('28', '80', '28', 'thresholdsoft', null, '2015-12-21 16:01:20');
INSERT INTO `crm_contact_data` VALUES ('29', '80', '29', 'thresholdsoft', null, '2015-12-21 16:01:20');
INSERT INTO `crm_contact_data` VALUES ('30', '80', '30', 'thresholdsoft', null, '2015-12-21 16:01:20');
INSERT INTO `crm_contact_data` VALUES ('31', '80', '31', 'thresholdsoft', null, '2015-12-21 16:01:20');
INSERT INTO `crm_contact_data` VALUES ('32', '80', '32', 'No', null, '2015-12-21 15:21:39');
INSERT INTO `crm_contact_data` VALUES ('33', '80', '33', '950066451', null, '2015-12-21 15:21:39');
INSERT INTO `crm_contact_data` VALUES ('34', '80', '34', '950066451', null, '2015-12-21 15:18:54');
INSERT INTO `crm_contact_data` VALUES ('35', '80', '35', 'ravikumar@gmail.com', null, '2015-12-23 15:51:33');
INSERT INTO `crm_contact_data` VALUES ('36', '80', '36', null, null, '2015-12-21 15:55:33');
INSERT INTO `crm_contact_data` VALUES ('41', '80', '38', '123123', '2015-12-21 11:26:05', '2015-12-21 15:56:05');
INSERT INTO `crm_contact_data` VALUES ('42', '80', '39', '123123', '2015-12-21 11:26:05', '2015-12-21 15:56:05');
INSERT INTO `crm_contact_data` VALUES ('43', '80', '40', '123456', '2015-12-21 11:26:05', '2015-12-21 15:56:05');
INSERT INTO `crm_contact_data` VALUES ('44', '80', '41', '2 years', '2015-12-21 11:26:05', '2015-12-21 15:56:05');
INSERT INTO `crm_contact_data` VALUES ('45', '80', '42', '2 years', '2015-12-21 11:26:05', '2015-12-21 15:56:05');
INSERT INTO `crm_contact_data` VALUES ('46', '80', '43', '4 years', '2015-12-21 11:26:05', '2015-12-21 15:56:05');
INSERT INTO `crm_contact_data` VALUES ('47', '81', '1', 'mohan', '2015-12-21 11:35:10', '2015-12-21 16:05:10');
INSERT INTO `crm_contact_data` VALUES ('48', '81', '2', 'babu', '2015-12-21 11:35:10', '2015-12-21 16:05:10');
INSERT INTO `crm_contact_data` VALUES ('49', '81', '3', '', '2015-12-21 11:35:10', '2015-12-21 16:05:10');
INSERT INTO `crm_contact_data` VALUES ('50', '81', '4', '', '2015-12-21 11:35:10', '2015-12-21 16:05:10');
INSERT INTO `crm_contact_data` VALUES ('51', '81', '5', '', '2015-12-21 11:35:10', '2015-12-21 16:05:10');
INSERT INTO `crm_contact_data` VALUES ('52', '81', '6', '', '2015-12-21 11:35:10', '2015-12-21 16:05:10');
INSERT INTO `crm_contact_data` VALUES ('53', '81', '7', '', '2015-12-21 11:35:10', '2015-12-21 16:05:10');
INSERT INTO `crm_contact_data` VALUES ('54', '81', '8', '', '2015-12-21 11:35:10', '2015-12-21 16:05:10');
INSERT INTO `crm_contact_data` VALUES ('55', '81', '9', '', '2015-12-21 11:35:10', '2015-12-21 16:05:10');
INSERT INTO `crm_contact_data` VALUES ('56', '81', '10', '', '2015-12-21 11:35:10', '2015-12-21 16:05:10');
INSERT INTO `crm_contact_data` VALUES ('57', '81', '11', '', '2015-12-21 11:35:10', '2015-12-21 16:05:10');
INSERT INTO `crm_contact_data` VALUES ('58', '81', '12', '', '2015-12-21 11:35:10', '2015-12-21 16:05:10');
INSERT INTO `crm_contact_data` VALUES ('59', '81', '13', '', '2015-12-21 11:35:10', '2015-12-21 16:05:10');
INSERT INTO `crm_contact_data` VALUES ('60', '81', '14', '', '2015-12-21 11:35:10', '2015-12-21 16:05:10');
INSERT INTO `crm_contact_data` VALUES ('141', '84', '1', 'chandra up', '2015-12-21 12:29:48', '2016-01-04 16:24:06');
INSERT INTO `crm_contact_data` VALUES ('142', '84', '2', 'sekhar', '2015-12-21 12:29:48', '2015-12-21 16:59:48');
INSERT INTO `crm_contact_data` VALUES ('143', '84', '3', 'Male', '2015-12-21 12:29:48', '2015-12-21 16:59:48');
INSERT INTO `crm_contact_data` VALUES ('144', '84', '4', '2016-01-03T18:30:00.000Z', '2015-12-21 12:29:48', '2016-01-04 16:24:06');
INSERT INTO `crm_contact_data` VALUES ('145', '84', '5', '', '2015-12-21 12:29:48', '2015-12-21 16:59:48');
INSERT INTO `crm_contact_data` VALUES ('146', '84', '6', '', '2015-12-21 12:29:48', '2015-12-21 16:59:48');
INSERT INTO `crm_contact_data` VALUES ('147', '84', '7', '', '2015-12-21 12:29:48', '2015-12-21 16:59:48');
INSERT INTO `crm_contact_data` VALUES ('148', '84', '8', '', '2015-12-21 12:29:48', '2015-12-21 16:59:48');
INSERT INTO `crm_contact_data` VALUES ('149', '84', '9', 'B-tech', '2015-12-21 12:29:48', '2015-12-24 12:00:41');
INSERT INTO `crm_contact_data` VALUES ('150', '84', '10', '', '2015-12-21 12:29:48', '2015-12-21 16:59:48');
INSERT INTO `crm_contact_data` VALUES ('151', '84', '11', 'yes', '2015-12-21 12:29:48', '2015-12-24 12:00:41');
INSERT INTO `crm_contact_data` VALUES ('152', '84', '12', 'Bhasha', '2015-12-21 12:29:48', '2015-12-24 12:00:41');
INSERT INTO `crm_contact_data` VALUES ('153', '84', '13', 'Yes', '2015-12-21 12:29:48', '2015-12-24 12:00:41');
INSERT INTO `crm_contact_data` VALUES ('154', '84', '14', 'Yes', '2015-12-21 12:29:48', '2015-12-24 12:00:41');
INSERT INTO `crm_contact_data` VALUES ('155', '80', '37', '[{\"bank\":\"sdafs\",\"branch\":\"sdfasdf\",\"opening_year\":\"sdfasdf\",\"account_name\":\"sdfasdf\",\"account_number\":\"sdfasdf\",\"currency\":\"sdfa\",\"balance\":\"sdfasdf\"},{\"bank\":null,\"branch\":null,\"opening_year\":null,\"account_name\":null,\"account_number\":null,\"currency\":null,\"balance\":null}]', '2015-12-21 12:33:03', '2015-12-21 18:54:36');
INSERT INTO `crm_contact_data` VALUES ('156', '84', '15', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('157', '84', '16', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('158', '84', '17', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('159', '84', '18', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('160', '84', '19', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('161', '84', '20', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('162', '84', '21', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('163', '84', '22', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('164', '84', '23', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('165', '84', '24', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('166', '84', '25', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('167', '84', '26', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('168', '84', '27', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('169', '84', '28', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('170', '84', '29', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('171', '84', '30', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('172', '84', '31', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('173', '84', '32', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('174', '84', '33', '789456123', '2015-12-21 12:37:34', '2015-12-21 18:10:20');
INSERT INTO `crm_contact_data` VALUES ('175', '84', '34', '', '2015-12-21 12:37:34', '2015-12-21 17:07:33');
INSERT INTO `crm_contact_data` VALUES ('176', '84', '35', 'chandra@gmail.com', '2015-12-21 12:37:34', '2015-12-21 18:10:35');
INSERT INTO `crm_contact_data` VALUES ('177', '84', '36', null, '2015-12-21 12:37:34', '2015-12-21 18:10:20');
INSERT INTO `crm_contact_data` VALUES ('178', '84', '37', '[{\"bank\":\"idbi\",\"branch\":\"hyd\",\"opening_year\":\"5\",\"account_name\":\"chandra\",\"account_number\":\"123456789\",\"currency\":\"inr\",\"balance\":123},[],{\"bank\":null,\"branch\":null,\"opening_year\":null,\"account_name\":null,\"account_number\":null,\"currency\":null,\"balance\":null}]', '2015-12-21 12:38:10', '2015-12-21 17:37:41');
INSERT INTO `crm_contact_data` VALUES ('180', '85', '1', 'Mohanbabu', '2015-12-21 12:50:07', '2015-12-21 17:20:06');
INSERT INTO `crm_contact_data` VALUES ('181', '85', '2', 'Pachipala', '2015-12-21 12:50:07', '2015-12-21 17:20:06');
INSERT INTO `crm_contact_data` VALUES ('182', '85', '3', '', '2015-12-21 12:50:07', '2015-12-21 17:20:06');
INSERT INTO `crm_contact_data` VALUES ('183', '85', '4', '', '2015-12-21 12:50:07', '2015-12-21 17:20:06');
INSERT INTO `crm_contact_data` VALUES ('184', '85', '5', '', '2015-12-21 12:50:07', '2015-12-21 17:20:06');
INSERT INTO `crm_contact_data` VALUES ('185', '85', '6', '', '2015-12-21 12:50:07', '2015-12-21 17:20:06');
INSERT INTO `crm_contact_data` VALUES ('186', '85', '7', '', '2015-12-21 12:50:07', '2015-12-21 17:20:06');
INSERT INTO `crm_contact_data` VALUES ('187', '85', '8', '', '2015-12-21 12:50:07', '2015-12-21 17:20:06');
INSERT INTO `crm_contact_data` VALUES ('188', '85', '9', '', '2015-12-21 12:50:07', '2015-12-21 17:20:06');
INSERT INTO `crm_contact_data` VALUES ('189', '85', '10', '', '2015-12-21 12:50:07', '2015-12-21 17:20:06');
INSERT INTO `crm_contact_data` VALUES ('190', '85', '11', '', '2015-12-21 12:50:07', '2015-12-21 17:20:06');
INSERT INTO `crm_contact_data` VALUES ('191', '85', '12', '', '2015-12-21 12:50:07', '2015-12-21 17:20:06');
INSERT INTO `crm_contact_data` VALUES ('192', '85', '13', '', '2015-12-21 12:50:07', '2015-12-21 17:20:06');
INSERT INTO `crm_contact_data` VALUES ('193', '85', '14', '', '2015-12-21 12:50:07', '2015-12-21 17:20:06');
INSERT INTO `crm_contact_data` VALUES ('194', '85', '37', '[{\"bank\":\"IDBI\",\"branch\":\"Jubile Hills\",\"opening_year\":\"2015\",\"account_name\":\"Mohanbabu\",\"account_number\":\"306021\",\"currency\":\"$\",\"balance\":123},[]]', '2015-12-21 12:52:24', '2015-12-21 17:22:23');
INSERT INTO `crm_contact_data` VALUES ('195', '80', '44', '[{\"title\":\"adsd\",\"amount\":200,\"comments\":\"sadasd\"},[]]', '2015-12-21 14:25:37', '2015-12-22 10:36:24');
INSERT INTO `crm_contact_data` VALUES ('196', '80', '45', '[{\"title\":\"dasdas\",\"amount\":200},[]]', '2015-12-21 14:25:48', '2015-12-22 10:36:33');
INSERT INTO `crm_contact_data` VALUES ('197', '86', '1', 'Rakesh', '2015-12-21 14:44:58', '2015-12-21 19:14:58');
INSERT INTO `crm_contact_data` VALUES ('198', '86', '2', 'kumar', '2015-12-21 14:44:58', '2015-12-21 19:14:58');
INSERT INTO `crm_contact_data` VALUES ('199', '86', '3', 'Male', '2015-12-21 14:44:58', '2015-12-21 19:14:58');
INSERT INTO `crm_contact_data` VALUES ('200', '86', '4', '2015-12-22T18:30:00.000Z', '2015-12-21 14:44:58', '2015-12-21 19:14:58');
INSERT INTO `crm_contact_data` VALUES ('201', '86', '5', 'Hyderabad', '2015-12-21 14:44:58', '2015-12-21 19:14:58');
INSERT INTO `crm_contact_data` VALUES ('202', '86', '6', '123456', '2015-12-21 14:44:58', '2015-12-21 19:14:58');
INSERT INTO `crm_contact_data` VALUES ('203', '86', '7', '1233', '2015-12-21 14:44:58', '2015-12-21 19:14:58');
INSERT INTO `crm_contact_data` VALUES ('204', '86', '8', 'Yes', '2015-12-21 14:44:58', '2015-12-21 19:14:58');
INSERT INTO `crm_contact_data` VALUES ('205', '86', '9', 'Education', '2015-12-21 14:44:58', '2015-12-21 19:14:58');
INSERT INTO `crm_contact_data` VALUES ('206', '86', '10', 'Employment', '2015-12-21 14:44:58', '2015-12-21 19:14:58');
INSERT INTO `crm_contact_data` VALUES ('207', '86', '11', 'Employment', '2015-12-21 14:44:58', '2015-12-21 19:14:58');
INSERT INTO `crm_contact_data` VALUES ('208', '86', '12', 'Profession', '2015-12-21 14:44:58', '2015-12-21 19:14:58');
INSERT INTO `crm_contact_data` VALUES ('209', '86', '13', 'Yes', '2015-12-21 14:44:58', '2015-12-21 19:14:58');
INSERT INTO `crm_contact_data` VALUES ('210', '86', '14', 'Yes', '2015-12-21 14:44:58', '2015-12-21 19:14:58');
INSERT INTO `crm_contact_data` VALUES ('211', '86', '15', 'Spouse Name', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('212', '86', '16', 'Spouse Id', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('213', '86', '17', 'Profession', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('214', '86', '18', 'Employment', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('215', '86', '19', 'Employment Status', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('216', '86', '20', 'Education', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('217', '86', '21', 'Residence Physical Address', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('218', '86', '22', 'Living in Residence Since', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('219', '86', '23', 'Yes', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('220', '86', '24', 'Landlord', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('221', '86', '25', 'Number of Dependants', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('222', '86', '26', 'Male', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('223', '86', '27', 'Female', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('224', '86', '28', 'Thereof pre-school', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('225', '86', '29', 'Thereof in schoo', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('226', '86', '30', 'Thereof university', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('227', '86', '31', 'Thereof post-school', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('228', '86', '32', 'Yes', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('229', '86', '33', '95555', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('230', '86', '34', '65655', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('231', '86', '35', 'email3l@gamil.com', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('232', '86', '36', '', '2015-12-21 14:46:04', '2015-12-21 19:16:04');
INSERT INTO `crm_contact_data` VALUES ('233', '84', '44', '[{\"title\":\"try\",\"amount\":12312,\"comments\":\"ytr\"},[],[],[],[],[],[],[],[],[],[]]', '2015-12-21 14:52:07', '2015-12-24 14:44:41');
INSERT INTO `crm_contact_data` VALUES ('329', '90', '1', 'Rakesh', '2015-12-22 13:16:58', '2015-12-22 17:47:00');
INSERT INTO `crm_contact_data` VALUES ('330', '90', '2', 'Kumar', '2015-12-22 13:16:58', '2015-12-22 17:47:00');
INSERT INTO `crm_contact_data` VALUES ('331', '90', '3', '', '2015-12-22 13:16:58', '2015-12-22 17:47:00');
INSERT INTO `crm_contact_data` VALUES ('332', '90', '4', '2015-12-16T18:30:00.000Z', '2015-12-22 13:16:58', '2015-12-22 17:47:00');
INSERT INTO `crm_contact_data` VALUES ('333', '90', '5', 'hyderabad', '2015-12-22 13:16:58', '2015-12-22 17:47:00');
INSERT INTO `crm_contact_data` VALUES ('334', '90', '6', '123456', '2015-12-22 13:16:58', '2015-12-22 17:47:00');
INSERT INTO `crm_contact_data` VALUES ('335', '90', '7', 'Hyderabad', '2015-12-22 13:16:58', '2015-12-22 17:47:00');
INSERT INTO `crm_contact_data` VALUES ('336', '90', '8', 'Yes', '2015-12-22 13:16:58', '2015-12-22 17:47:00');
INSERT INTO `crm_contact_data` VALUES ('337', '90', '9', '', '2015-12-22 13:16:58', '2015-12-22 17:47:00');
INSERT INTO `crm_contact_data` VALUES ('338', '90', '10', 'Employe', '2015-12-22 13:16:58', '2015-12-22 17:47:00');
INSERT INTO `crm_contact_data` VALUES ('339', '90', '11', 'software', '2015-12-22 13:16:58', '2015-12-22 17:47:00');
INSERT INTO `crm_contact_data` VALUES ('340', '90', '12', '', '2015-12-22 13:16:58', '2015-12-22 17:47:00');
INSERT INTO `crm_contact_data` VALUES ('341', '90', '13', '', '2015-12-22 13:16:58', '2015-12-22 17:47:00');
INSERT INTO `crm_contact_data` VALUES ('342', '90', '14', '', '2015-12-22 13:16:58', '2015-12-22 17:47:00');
INSERT INTO `crm_contact_data` VALUES ('357', '86', '44', '[{\"title\":\"Income\",\"amount\":500},{\"title\":\"Rent\",\"amount\":25000},{\"title\":null}]', '2015-12-22 14:48:08', '2015-12-22 19:18:06');
INSERT INTO `crm_contact_data` VALUES ('358', '86', '37', '[{\"bank\":\"IDBI\",\"branch\":\"52001\",\"opening_year\":\"2015\",\"account_name\":\"Rakesh Kumar\",\"account_number\":\"123456853\",\"currency\":\"IND\",\"balance\":25000},{\"bank\":\"SBI\",\"branch\":\"51200\",\"opening_year\":\"2015\",\"account_name\":\"RAkesh Kumar\",\"account_number\":\"1235130321\",\"currency\":\"IND\",\"balance\":25000},{\"bank\":null}]', '2015-12-22 14:49:48', '2015-12-22 19:19:46');
INSERT INTO `crm_contact_data` VALUES ('359', '85', '15', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('360', '85', '16', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('361', '85', '17', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('362', '85', '18', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('363', '85', '19', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('364', '85', '20', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('365', '85', '21', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('366', '85', '22', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('367', '85', '23', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('368', '85', '24', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('369', '85', '25', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('370', '85', '26', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('371', '85', '27', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('372', '85', '28', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('373', '85', '29', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('374', '85', '30', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('375', '85', '31', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('376', '85', '32', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('377', '85', '33', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('378', '85', '34', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('379', '85', '35', 'ngurrala9@gmail.com', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('380', '85', '36', '', '2015-12-23 05:44:15', '2015-12-23 10:14:12');
INSERT INTO `crm_contact_data` VALUES ('381', '85', '38', '', '2015-12-23 05:45:01', '2015-12-23 10:14:58');
INSERT INTO `crm_contact_data` VALUES ('382', '85', '39', '', '2015-12-23 05:45:01', '2015-12-23 10:14:58');
INSERT INTO `crm_contact_data` VALUES ('383', '85', '40', '', '2015-12-23 05:45:01', '2015-12-23 10:14:58');
INSERT INTO `crm_contact_data` VALUES ('384', '85', '41', '', '2015-12-23 05:45:01', '2015-12-23 10:14:58');
INSERT INTO `crm_contact_data` VALUES ('385', '85', '42', '', '2015-12-23 05:45:01', '2015-12-23 10:14:58');
INSERT INTO `crm_contact_data` VALUES ('386', '85', '43', '', '2015-12-23 05:45:01', '2015-12-23 10:14:58');
INSERT INTO `crm_contact_data` VALUES ('387', '84', '38', '', '2015-12-24 10:16:16', '2015-12-24 14:46:11');
INSERT INTO `crm_contact_data` VALUES ('388', '84', '39', '', '2015-12-24 10:16:16', '2015-12-24 14:46:11');
INSERT INTO `crm_contact_data` VALUES ('389', '84', '40', '', '2015-12-24 10:16:16', '2015-12-24 14:46:11');
INSERT INTO `crm_contact_data` VALUES ('390', '84', '41', '', '2015-12-24 10:16:16', '2015-12-24 14:46:11');
INSERT INTO `crm_contact_data` VALUES ('391', '84', '42', '', '2015-12-24 10:16:16', '2015-12-24 14:46:11');
INSERT INTO `crm_contact_data` VALUES ('392', '84', '43', '', '2015-12-24 10:16:16', '2015-12-24 14:46:11');
INSERT INTO `crm_contact_data` VALUES ('393', '94', '1', 'Ramesh', '2015-12-29 12:48:15', '2015-12-29 17:18:13');
INSERT INTO `crm_contact_data` VALUES ('394', '94', '2', 'kumar', '2015-12-29 12:48:15', '2015-12-29 17:18:13');
INSERT INTO `crm_contact_data` VALUES ('395', '94', '3', '', '2015-12-29 12:48:15', '2015-12-29 17:18:13');
INSERT INTO `crm_contact_data` VALUES ('396', '94', '4', '2015-12-09T18:30:00.000Z', '2015-12-29 12:48:15', '2015-12-29 17:18:13');
INSERT INTO `crm_contact_data` VALUES ('397', '94', '5', '', '2015-12-29 12:48:15', '2015-12-29 17:18:13');
INSERT INTO `crm_contact_data` VALUES ('398', '94', '6', '', '2015-12-29 12:48:15', '2015-12-29 17:18:13');
INSERT INTO `crm_contact_data` VALUES ('399', '94', '7', '', '2015-12-29 12:48:15', '2015-12-29 17:18:13');
INSERT INTO `crm_contact_data` VALUES ('400', '94', '8', '', '2015-12-29 12:48:15', '2015-12-29 17:18:13');
INSERT INTO `crm_contact_data` VALUES ('401', '94', '9', '', '2015-12-29 12:48:15', '2015-12-29 17:18:13');
INSERT INTO `crm_contact_data` VALUES ('402', '94', '10', '', '2015-12-29 12:48:15', '2015-12-29 17:18:13');
INSERT INTO `crm_contact_data` VALUES ('403', '94', '11', '', '2015-12-29 12:48:15', '2015-12-29 17:18:13');
INSERT INTO `crm_contact_data` VALUES ('404', '94', '12', '', '2015-12-29 12:48:15', '2015-12-29 17:18:13');
INSERT INTO `crm_contact_data` VALUES ('405', '94', '13', '', '2015-12-29 12:48:15', '2015-12-29 17:18:13');
INSERT INTO `crm_contact_data` VALUES ('406', '94', '14', '', '2015-12-29 12:48:15', '2015-12-29 17:18:13');
INSERT INTO `crm_contact_data` VALUES ('407', '95', '1', 'Mohan', '2015-12-29 12:59:52', '2015-12-29 17:29:53');
INSERT INTO `crm_contact_data` VALUES ('408', '95', '2', 'Babu', '2015-12-29 12:59:52', '2015-12-29 17:29:53');
INSERT INTO `crm_contact_data` VALUES ('409', '95', '3', '', '2015-12-29 12:59:52', '2015-12-29 17:29:53');
INSERT INTO `crm_contact_data` VALUES ('410', '95', '4', '1986-12-21T18:30:00.000Z', '2015-12-29 12:59:52', '2015-12-29 17:29:53');
INSERT INTO `crm_contact_data` VALUES ('411', '95', '5', 'tirupathi', '2015-12-29 12:59:52', '2015-12-29 17:29:53');
INSERT INTO `crm_contact_data` VALUES ('412', '95', '6', '11708', '2015-12-29 12:59:52', '2015-12-29 17:29:53');
INSERT INTO `crm_contact_data` VALUES ('413', '95', '7', '', '2015-12-29 12:59:52', '2015-12-29 17:29:53');
INSERT INTO `crm_contact_data` VALUES ('414', '95', '8', 'Yes', '2015-12-29 12:59:52', '2015-12-29 17:29:53');
INSERT INTO `crm_contact_data` VALUES ('415', '95', '9', 'MCA', '2015-12-29 12:59:52', '2015-12-29 17:29:53');
INSERT INTO `crm_contact_data` VALUES ('416', '95', '10', 'Active', '2015-12-29 12:59:52', '2015-12-29 17:29:53');
INSERT INTO `crm_contact_data` VALUES ('417', '95', '11', 'Team Lead', '2015-12-29 12:59:52', '2015-12-29 17:29:53');
INSERT INTO `crm_contact_data` VALUES ('418', '95', '12', 'Software', '2015-12-29 12:59:52', '2015-12-29 17:29:53');
INSERT INTO `crm_contact_data` VALUES ('419', '95', '13', '', '2015-12-29 12:59:52', '2015-12-29 17:29:53');
INSERT INTO `crm_contact_data` VALUES ('420', '95', '14', '', '2015-12-29 12:59:52', '2015-12-29 17:29:53');
INSERT INTO `crm_contact_data` VALUES ('421', '95', '15', 'Sthanka', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('422', '95', '16', '11708', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('423', '95', '17', 'Software', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('424', '95', '18', 'Project member', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('425', '95', '19', 'Active', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('426', '95', '20', 'Btech', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('427', '95', '21', 'Tenali', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('428', '95', '22', '1990', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('429', '95', '23', 'No', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('430', '95', '24', 'Sthanka', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('431', '95', '25', '12', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('432', '95', '26', '2', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('433', '95', '27', '5', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('434', '95', '28', '3', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('435', '95', '29', '1', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('436', '95', '30', '1', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('437', '95', '31', '1', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('438', '95', '32', 'No', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('439', '95', '33', '9876544535', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('440', '95', '34', '', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('441', '95', '35', 'mohanbabu.p@thresholdsoft.com', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('442', '95', '36', '', '2015-12-29 13:01:34', '2015-12-29 17:31:35');
INSERT INTO `crm_contact_data` VALUES ('443', '95', '37', '[{\"bank\":\"ICICI\",\"branch\":\"Sanath Nagar\",\"opening_year\":\"1990\",\"account_name\":\"Mohan\",\"account_number\":\"76576546\",\"currency\":\"indian\",\"balance\":200000},[],[],[],[],[],[],[],[],[],[]]', '2015-12-29 13:03:05', '2015-12-29 17:33:06');
INSERT INTO `crm_contact_data` VALUES ('444', '95', '38', '100000', '2015-12-29 13:03:50', '2015-12-29 17:33:50');
INSERT INTO `crm_contact_data` VALUES ('445', '95', '39', '10000000', '2015-12-29 13:03:50', '2015-12-29 17:33:50');
INSERT INTO `crm_contact_data` VALUES ('446', '95', '40', '10', '2015-12-29 13:03:50', '2015-12-29 17:33:50');
INSERT INTO `crm_contact_data` VALUES ('447', '95', '41', '1990', '2015-12-29 13:03:50', '2015-12-29 17:33:50');
INSERT INTO `crm_contact_data` VALUES ('448', '95', '42', '1000000', '2015-12-29 13:03:50', '2015-12-29 17:33:50');
INSERT INTO `crm_contact_data` VALUES ('449', '95', '43', '1000000', '2015-12-29 13:03:50', '2015-12-29 17:33:50');
INSERT INTO `crm_contact_data` VALUES ('450', '95', '44', '[{\"title\":\"House Rent\",\"amount\":10000,\"comments\":\"high\"},[],[],[],[],[],[],[],[],[],[]]', '2015-12-29 13:05:58', '2015-12-29 17:35:59');
INSERT INTO `crm_contact_data` VALUES ('451', '95', '45', '[{\"title\":\"future savings\",\"amount\":65646546546},[],[],[],[],[],[],[],[],[],[]]', '2015-12-29 13:06:42', '2015-12-29 17:39:06');
INSERT INTO `crm_contact_data` VALUES ('452', '96', '1', 'naresh', '2015-12-29 14:15:29', '2015-12-29 18:45:33');
INSERT INTO `crm_contact_data` VALUES ('453', '96', '2', 'gurrala', '2015-12-29 14:15:29', '2015-12-29 18:45:33');
INSERT INTO `crm_contact_data` VALUES ('454', '96', '3', 'Male', '2015-12-29 14:15:29', '2015-12-29 18:49:15');
INSERT INTO `crm_contact_data` VALUES ('455', '96', '4', '1990-02-07T18:30:00.000Z', '2015-12-29 14:15:29', '2015-12-29 18:55:11');
INSERT INTO `crm_contact_data` VALUES ('456', '96', '5', '', '2015-12-29 14:15:29', '2015-12-29 18:45:33');
INSERT INTO `crm_contact_data` VALUES ('457', '96', '6', '', '2015-12-29 14:15:29', '2015-12-29 18:45:33');
INSERT INTO `crm_contact_data` VALUES ('458', '96', '7', '', '2015-12-29 14:15:29', '2015-12-29 18:45:33');
INSERT INTO `crm_contact_data` VALUES ('459', '96', '8', '', '2015-12-29 14:15:29', '2015-12-29 18:45:33');
INSERT INTO `crm_contact_data` VALUES ('460', '96', '9', '', '2015-12-29 14:15:29', '2015-12-29 18:45:33');
INSERT INTO `crm_contact_data` VALUES ('461', '96', '10', '', '2015-12-29 14:15:29', '2015-12-29 18:45:33');
INSERT INTO `crm_contact_data` VALUES ('462', '96', '11', '', '2015-12-29 14:15:29', '2015-12-29 18:45:33');
INSERT INTO `crm_contact_data` VALUES ('463', '96', '12', '', '2015-12-29 14:15:29', '2015-12-29 18:45:33');
INSERT INTO `crm_contact_data` VALUES ('464', '96', '13', '', '2015-12-29 14:15:29', '2015-12-29 18:45:33');
INSERT INTO `crm_contact_data` VALUES ('465', '96', '14', '', '2015-12-29 14:15:29', '2015-12-29 18:45:33');
INSERT INTO `crm_contact_data` VALUES ('466', '96', '15', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('467', '96', '16', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('468', '96', '17', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('469', '96', '18', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('470', '96', '19', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('471', '96', '20', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('472', '96', '21', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('473', '96', '22', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('474', '96', '23', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('475', '96', '24', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('476', '96', '25', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('477', '96', '26', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('478', '96', '27', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('479', '96', '28', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('480', '96', '29', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('481', '96', '30', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('482', '96', '31', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('483', '96', '32', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('484', '96', '33', '7894561235', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('485', '96', '34', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('486', '96', '35', 'naresh.g@thresholdsoft.com', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('487', '96', '36', '', '2015-12-29 14:15:42', '2015-12-29 18:45:46');
INSERT INTO `crm_contact_data` VALUES ('488', '104', '111', 'Running', '2015-12-31 11:12:56', '2015-12-31 15:42:58');
INSERT INTO `crm_contact_data` VALUES ('489', '104', '112', 'Active', '2015-12-31 11:12:56', '2015-12-31 15:42:58');
INSERT INTO `crm_contact_data` VALUES ('490', '104', '113', 'QLANA', '2015-12-31 11:12:56', '2015-12-31 15:42:58');
INSERT INTO `crm_contact_data` VALUES ('491', '104', '114', 'banking domain', '2015-12-31 11:12:56', '2015-12-31 15:42:58');
INSERT INTO `crm_contact_data` VALUES ('492', '104', '115', 'Commercial', '2015-12-31 11:12:56', '2015-12-31 15:42:58');
INSERT INTO `crm_contact_data` VALUES ('493', '104', '116', 'Sub commercial', '2015-12-31 11:12:56', '2015-12-31 15:42:58');
INSERT INTO `crm_contact_data` VALUES ('494', '105', '111', 'Running', '2015-12-31 11:14:49', '2015-12-31 15:44:51');
INSERT INTO `crm_contact_data` VALUES ('495', '105', '112', 'Active', '2015-12-31 11:14:49', '2015-12-31 15:44:51');
INSERT INTO `crm_contact_data` VALUES ('496', '105', '113', 'QLANA', '2015-12-31 11:14:49', '2015-12-31 15:44:51');
INSERT INTO `crm_contact_data` VALUES ('497', '105', '114', 'banking domain', '2015-12-31 11:14:49', '2015-12-31 15:44:51');
INSERT INTO `crm_contact_data` VALUES ('498', '105', '115', 'Commercial', '2015-12-31 11:14:49', '2015-12-31 15:44:51');
INSERT INTO `crm_contact_data` VALUES ('499', '105', '116', 'Sub commercial', '2015-12-31 11:14:49', '2015-12-31 15:44:51');
INSERT INTO `crm_contact_data` VALUES ('526', '105', '1', 'Rakesh', '2016-01-04 11:57:20', '2016-01-04 16:27:20');
INSERT INTO `crm_contact_data` VALUES ('527', '105', '2', 'kuMAR', '2016-01-04 11:57:20', '2016-01-04 16:27:20');
INSERT INTO `crm_contact_data` VALUES ('528', '105', '3', '', '2016-01-04 11:57:20', '2016-01-04 16:27:20');
INSERT INTO `crm_contact_data` VALUES ('529', '105', '4', '2016-01-03T18:30:00.000Z', '2016-01-04 11:57:20', '2016-01-04 16:27:20');
INSERT INTO `crm_contact_data` VALUES ('530', '105', '5', 'hyd', '2016-01-04 11:57:20', '2016-01-04 16:27:20');
INSERT INTO `crm_contact_data` VALUES ('531', '105', '6', '', '2016-01-04 11:57:20', '2016-01-04 16:27:20');
INSERT INTO `crm_contact_data` VALUES ('532', '105', '7', '', '2016-01-04 11:57:20', '2016-01-04 16:27:20');
INSERT INTO `crm_contact_data` VALUES ('533', '105', '8', '', '2016-01-04 11:57:20', '2016-01-04 16:27:20');
INSERT INTO `crm_contact_data` VALUES ('534', '105', '9', '', '2016-01-04 11:57:20', '2016-01-04 16:27:20');
INSERT INTO `crm_contact_data` VALUES ('535', '105', '10', '', '2016-01-04 11:57:20', '2016-01-04 16:27:20');
INSERT INTO `crm_contact_data` VALUES ('536', '105', '11', '', '2016-01-04 11:57:20', '2016-01-04 16:27:20');
INSERT INTO `crm_contact_data` VALUES ('537', '105', '12', '', '2016-01-04 11:57:20', '2016-01-04 16:27:20');
INSERT INTO `crm_contact_data` VALUES ('538', '105', '13', '', '2016-01-04 11:57:20', '2016-01-04 16:27:20');
INSERT INTO `crm_contact_data` VALUES ('539', '105', '14', '', '2016-01-04 11:57:20', '2016-01-04 16:27:20');
INSERT INTO `crm_contact_data` VALUES ('540', '111', '1', 'mohan', '2016-01-05 07:54:21', '2016-01-05 12:24:27');
INSERT INTO `crm_contact_data` VALUES ('541', '111', '2', 'babu', '2016-01-05 07:54:21', '2016-01-05 12:24:27');
INSERT INTO `crm_contact_data` VALUES ('542', '111', '3', '', '2016-01-05 07:54:21', '2016-01-05 12:24:27');
INSERT INTO `crm_contact_data` VALUES ('543', '111', '4', '', '2016-01-05 07:54:21', '2016-01-05 12:24:27');
INSERT INTO `crm_contact_data` VALUES ('544', '111', '5', '', '2016-01-05 07:54:21', '2016-01-05 12:24:27');
INSERT INTO `crm_contact_data` VALUES ('545', '111', '6', '', '2016-01-05 07:54:21', '2016-01-05 12:24:27');
INSERT INTO `crm_contact_data` VALUES ('546', '111', '7', '', '2016-01-05 07:54:21', '2016-01-05 12:24:27');
INSERT INTO `crm_contact_data` VALUES ('547', '111', '8', '', '2016-01-05 07:54:21', '2016-01-05 12:24:27');
INSERT INTO `crm_contact_data` VALUES ('548', '111', '9', '', '2016-01-05 07:54:21', '2016-01-05 12:24:27');
INSERT INTO `crm_contact_data` VALUES ('549', '111', '10', '', '2016-01-05 07:54:21', '2016-01-05 12:24:27');
INSERT INTO `crm_contact_data` VALUES ('550', '111', '11', '', '2016-01-05 07:54:21', '2016-01-05 12:24:27');
INSERT INTO `crm_contact_data` VALUES ('551', '111', '12', '', '2016-01-05 07:54:21', '2016-01-05 12:24:27');
INSERT INTO `crm_contact_data` VALUES ('552', '111', '13', '', '2016-01-05 07:54:21', '2016-01-05 12:24:27');
INSERT INTO `crm_contact_data` VALUES ('553', '111', '14', '', '2016-01-05 07:54:21', '2016-01-05 12:24:27');
INSERT INTO `crm_contact_data` VALUES ('554', '112', '1', null, '2016-01-08 15:17:43', '2016-01-08 19:47:34');
INSERT INTO `crm_contact_data` VALUES ('555', '112', '2', null, '2016-01-08 15:17:43', '2016-01-08 19:47:34');
INSERT INTO `crm_contact_data` VALUES ('556', '112', '3', '', '2016-01-08 15:17:43', '2016-01-08 19:47:34');
INSERT INTO `crm_contact_data` VALUES ('557', '112', '4', '', '2016-01-08 15:17:43', '2016-01-08 19:47:34');
INSERT INTO `crm_contact_data` VALUES ('558', '112', '5', '', '2016-01-08 15:17:43', '2016-01-08 19:47:34');
INSERT INTO `crm_contact_data` VALUES ('559', '112', '6', '', '2016-01-08 15:17:43', '2016-01-08 19:47:34');
INSERT INTO `crm_contact_data` VALUES ('560', '112', '7', '', '2016-01-08 15:17:43', '2016-01-08 19:47:34');
INSERT INTO `crm_contact_data` VALUES ('561', '112', '8', '', '2016-01-08 15:17:43', '2016-01-08 19:47:34');
INSERT INTO `crm_contact_data` VALUES ('562', '112', '9', '', '2016-01-08 15:17:43', '2016-01-08 19:47:34');
INSERT INTO `crm_contact_data` VALUES ('563', '112', '10', '', '2016-01-08 15:17:43', '2016-01-08 19:47:34');
INSERT INTO `crm_contact_data` VALUES ('564', '112', '11', '', '2016-01-08 15:17:43', '2016-01-08 19:47:34');
INSERT INTO `crm_contact_data` VALUES ('565', '112', '12', '', '2016-01-08 15:17:43', '2016-01-08 19:47:34');
INSERT INTO `crm_contact_data` VALUES ('566', '112', '13', '', '2016-01-08 15:17:43', '2016-01-08 19:47:34');
INSERT INTO `crm_contact_data` VALUES ('567', '112', '14', '', '2016-01-08 15:17:43', '2016-01-08 19:47:34');
INSERT INTO `crm_contact_data` VALUES ('568', '115', '1', null, '2016-01-08 15:20:39', '2016-01-08 19:50:30');
INSERT INTO `crm_contact_data` VALUES ('569', '115', '2', null, '2016-01-08 15:20:39', '2016-01-08 19:50:30');
INSERT INTO `crm_contact_data` VALUES ('570', '115', '3', '', '2016-01-08 15:20:39', '2016-01-08 19:50:30');
INSERT INTO `crm_contact_data` VALUES ('571', '115', '4', '', '2016-01-08 15:20:39', '2016-01-08 19:50:30');
INSERT INTO `crm_contact_data` VALUES ('572', '115', '5', '', '2016-01-08 15:20:39', '2016-01-08 19:50:30');
INSERT INTO `crm_contact_data` VALUES ('573', '115', '6', '', '2016-01-08 15:20:39', '2016-01-08 19:50:30');
INSERT INTO `crm_contact_data` VALUES ('574', '115', '7', '', '2016-01-08 15:20:39', '2016-01-08 19:50:30');
INSERT INTO `crm_contact_data` VALUES ('575', '115', '8', '', '2016-01-08 15:20:39', '2016-01-08 19:50:30');
INSERT INTO `crm_contact_data` VALUES ('576', '115', '9', '', '2016-01-08 15:20:39', '2016-01-08 19:50:30');
INSERT INTO `crm_contact_data` VALUES ('577', '115', '10', '', '2016-01-08 15:20:39', '2016-01-08 19:50:30');
INSERT INTO `crm_contact_data` VALUES ('578', '115', '11', '', '2016-01-08 15:20:39', '2016-01-08 19:50:30');
INSERT INTO `crm_contact_data` VALUES ('579', '115', '12', '', '2016-01-08 15:20:39', '2016-01-08 19:50:30');
INSERT INTO `crm_contact_data` VALUES ('580', '115', '13', '', '2016-01-08 15:20:39', '2016-01-08 19:50:30');
INSERT INTO `crm_contact_data` VALUES ('581', '115', '14', '', '2016-01-08 15:20:39', '2016-01-08 19:50:30');
INSERT INTO `crm_contact_data` VALUES ('582', '116', '1', 'sthanka', '2016-01-08 15:23:25', '2016-01-08 19:58:10');
INSERT INTO `crm_contact_data` VALUES ('583', '116', '2', 'shaik', '2016-01-08 15:23:25', '2016-01-08 19:57:08');
INSERT INTO `crm_contact_data` VALUES ('584', '116', '3', '', '2016-01-08 15:23:25', '2016-01-08 19:53:16');
INSERT INTO `crm_contact_data` VALUES ('585', '116', '4', '', '2016-01-08 15:23:25', '2016-01-08 19:53:16');
INSERT INTO `crm_contact_data` VALUES ('586', '116', '5', '', '2016-01-08 15:23:25', '2016-01-08 19:53:16');
INSERT INTO `crm_contact_data` VALUES ('587', '116', '6', '', '2016-01-08 15:23:25', '2016-01-08 19:53:16');
INSERT INTO `crm_contact_data` VALUES ('588', '116', '7', '', '2016-01-08 15:23:25', '2016-01-08 19:53:16');
INSERT INTO `crm_contact_data` VALUES ('589', '116', '8', '', '2016-01-08 15:23:25', '2016-01-08 19:53:16');
INSERT INTO `crm_contact_data` VALUES ('590', '116', '9', '', '2016-01-08 15:23:25', '2016-01-08 19:53:16');
INSERT INTO `crm_contact_data` VALUES ('591', '116', '10', '', '2016-01-08 15:23:25', '2016-01-08 19:53:16');
INSERT INTO `crm_contact_data` VALUES ('592', '116', '11', '', '2016-01-08 15:23:25', '2016-01-08 19:53:16');
INSERT INTO `crm_contact_data` VALUES ('593', '116', '12', '', '2016-01-08 15:23:25', '2016-01-08 19:53:16');
INSERT INTO `crm_contact_data` VALUES ('594', '116', '13', '', '2016-01-08 15:23:25', '2016-01-08 19:53:16');
INSERT INTO `crm_contact_data` VALUES ('595', '116', '14', '', '2016-01-08 15:23:25', '2016-01-08 19:53:16');

-- ----------------------------
-- Table structure for `crm_contact_group`
-- ----------------------------
DROP TABLE IF EXISTS `crm_contact_group`;
CREATE TABLE `crm_contact_group` (
  `id_crm_contact_group` int(11) NOT NULL AUTO_INCREMENT,
  `crm_contact_group_name` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_crm_contact_group`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crm_contact_group
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_contact_group_contact`
-- ----------------------------
DROP TABLE IF EXISTS `crm_contact_group_contact`;
CREATE TABLE `crm_contact_group_contact` (
  `id_crm_contact_group_contact` int(11) NOT NULL AUTO_INCREMENT,
  `crm_contact_group_id` int(11) DEFAULT NULL,
  `crm_contact_id` int(11) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_crm_contact_group_contact`),
  KEY `crm_contact_group_id` (`crm_contact_group_id`),
  KEY `crm_contact_id` (`crm_contact_id`),
  CONSTRAINT `crm_contact_group_contact_ibfk_1` FOREIGN KEY (`crm_contact_group_id`) REFERENCES `crm_contact_group` (`id_crm_contact_group`),
  CONSTRAINT `crm_contact_group_contact_ibfk_2` FOREIGN KEY (`crm_contact_id`) REFERENCES `crm_contact` (`id_crm_contact`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crm_contact_group_contact
-- ----------------------------

-- ----------------------------
-- Table structure for `crm_document`
-- ----------------------------
DROP TABLE IF EXISTS `crm_document`;
CREATE TABLE `crm_document` (
  `id_crm_document` int(11) NOT NULL AUTO_INCREMENT,
  `crm_document_type_id` int(11) DEFAULT NULL,
  `uploaded_from_id` int(11) DEFAULT NULL,
  `document_name` varchar(255) DEFAULT NULL,
  `document_source` varchar(255) DEFAULT NULL,
  `document_description` varchar(255) DEFAULT NULL,
  `document_mime_type` varchar(255) DEFAULT NULL,
  `uploaded_by` int(11) DEFAULT NULL,
  `document_status` tinyint(4) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_crm_document`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crm_document
-- ----------------------------
INSERT INTO `crm_document` VALUES ('1', '1', '84', null, 'Desert_1451552169.jpg', 'sdf', 'image/jpeg', '2', null, '2015-12-31 14:26:03');
INSERT INTO `crm_document` VALUES ('2', '1', '84', null, 'Chrysanthemum_1451552169.jpg', 'fdfd', 'image/jpeg', '2', null, '2015-12-31 14:26:03');
INSERT INTO `crm_document` VALUES ('3', '1', '1', null, 'Desert_1451556657.jpg', '123', 'image/jpeg', '2', null, '2015-12-31 15:41:01');
INSERT INTO `crm_document` VALUES ('4', '1', '1', null, 'Desert_1451556664.jpg', '123', 'image/jpeg', '2', null, '2015-12-31 15:41:08');
INSERT INTO `crm_document` VALUES ('5', '1', '1', null, 'Hydrangeas_1451556772.jpg', 'sdfsd', 'image/jpeg', '2', null, '2015-12-31 15:42:57');
INSERT INTO `crm_document` VALUES ('6', '2', '1', null, 'Hydrangeas - Copy_1451556809.jpg', 'xzcxzc', 'image/jpeg', '2', null, '2015-12-31 15:43:33');
INSERT INTO `crm_document` VALUES ('7', '1', '1', null, 'Chrysanthemum_1451883057.jpg', '', 'image/jpeg', '2', null, '2016-01-04 10:21:03');
INSERT INTO `crm_document` VALUES ('8', '2', '1', null, 'index_1451883057.jpg', '', 'image/jpeg', '2', null, '2016-01-04 10:21:03');
INSERT INTO `crm_document` VALUES ('9', '2', '1', null, 'Hydrangeas - Copy_1451883057.jpg', '', 'image/jpeg', '2', null, '2016-01-04 10:21:03');
INSERT INTO `crm_document` VALUES ('10', '2', '1', null, 'Desert_1451883057.jpg', '', 'image/jpeg', '2', null, '2016-01-04 10:21:03');
INSERT INTO `crm_document` VALUES ('11', '2', '1', null, 'Hydrangeas_1451883057.jpg', '', 'image/jpeg', '2', null, '2016-01-04 10:21:03');
INSERT INTO `crm_document` VALUES ('12', '2', '1', null, 'index - Copy_1451883057.jpg', '', 'image/jpeg', '2', null, '2016-01-04 10:21:03');
INSERT INTO `crm_document` VALUES ('13', '2', '1', null, 'index1_1451883057.jpg', '', 'image/jpeg', '2', null, '2016-01-04 10:21:03');
INSERT INTO `crm_document` VALUES ('14', '2', '1', null, 'index4_1451883057.jpg', '', 'image/jpeg', '2', null, '2016-01-04 10:21:03');
INSERT INTO `crm_document` VALUES ('15', '2', '1', null, 'Chrysanthemum_1451883078.jpg', '', 'image/jpeg', '2', null, '2016-01-04 10:21:24');
INSERT INTO `crm_document` VALUES ('16', '2', '1', null, 'Desert_1451883078.jpg', '', 'image/jpeg', '2', null, '2016-01-04 10:21:24');
INSERT INTO `crm_document` VALUES ('17', '2', '1', null, 'Hydrangeas - Copy_1451883078.jpg', '', 'image/jpeg', '2', null, '2016-01-04 10:21:24');
INSERT INTO `crm_document` VALUES ('18', '2', '1', null, 'Hydrangeas_1451883078.jpg', '', 'image/jpeg', '2', null, '2016-01-04 10:21:24');
INSERT INTO `crm_document` VALUES ('19', '2', '1', null, 'index - Copy_1451883078.jpg', '', 'image/jpeg', '2', null, '2016-01-04 10:21:24');
INSERT INTO `crm_document` VALUES ('20', '2', '1', null, 'index_1451883078.jpg', '', 'image/jpeg', '2', null, '2016-01-04 10:21:24');
INSERT INTO `crm_document` VALUES ('21', '2', '1', null, 'index1_1451883078.jpg', '', 'image/jpeg', '2', null, '2016-01-04 10:21:24');
INSERT INTO `crm_document` VALUES ('70', '7', '2', 'Hydrangeas.jpg', '1/Hydrangeas_1451997569.jpg', 'sefere', 'image/jpeg', '2', null, '2016-01-05 18:09:30');
INSERT INTO `crm_document` VALUES ('72', '2', '105', 'Hydrangeas - Copy.jpg', '1/Hydrangeas - Copy_1452055949.jpg', 'fdg', 'image/jpeg', '2', null, '2016-01-06 10:22:36');
INSERT INTO `crm_document` VALUES ('73', '7', '23', 'Chrysanthemum.jpg', '1/Chrysanthemum_1452141109.jpg', 'test', 'image/jpeg', '2', null, '2016-01-07 10:01:43');
INSERT INTO `crm_document` VALUES ('88', '1', '111', 'TrendBrew Overview.xlsx', '1/TrendBrew Overview_1452251817.xlsx', 'licence', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '2', null, '2016-01-08 16:47:05');
INSERT INTO `crm_document` VALUES ('89', '1', '111', 'Tulips.jpg', '1/Tulips_1452256841.jpg', 'sdf', 'image/jpeg', '2', null, '2016-01-08 18:10:33');
INSERT INTO `crm_document` VALUES ('90', '2', '111', 'titular.png', '1/titular_1452256841.png', 'naresh', 'image/png', '2', null, '2016-01-08 18:10:33');
INSERT INTO `crm_document` VALUES ('91', '1', '111', 'timesheet.zip', '1/timesheet_1452257602.zip', 'sdfds', 'application/octet-stream', '2', null, '2016-01-08 18:23:14');
INSERT INTO `crm_document` VALUES ('92', '7', '23', 'TrendBrew Overview.xlsx', '1/TrendBrew Overview_1452261387.xlsx', 'dfdsas', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '2', null, '2016-01-08 19:26:36');
INSERT INTO `crm_document` VALUES ('93', '4', '36', 'TrendBrew Overview.xlsx', '1/TrendBrew Overview_1452261591.xlsx', 'dfgfdg', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '2', null, '2016-01-08 19:29:59');
INSERT INTO `crm_document` VALUES ('94', '7', '23', 'TrendBrew Overview.xlsx', '1/TrendBrew Overview_1452261634.xlsx', 'sdfdsf', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '2', null, '2016-01-08 19:30:42');

-- ----------------------------
-- Table structure for `crm_document_type`
-- ----------------------------
DROP TABLE IF EXISTS `crm_document_type`;
CREATE TABLE `crm_document_type` (
  `id_crm_document_type` int(11) NOT NULL AUTO_INCREMENT,
  `crm_module_id` int(11) DEFAULT NULL,
  `document_type_name` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_crm_document_type`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crm_document_type
-- ----------------------------
INSERT INTO `crm_document_type` VALUES ('1', '1', 'pan card', '2015-12-31 10:22:29');
INSERT INTO `crm_document_type` VALUES ('2', '1', 'address proof', '2015-12-31 10:24:30');
INSERT INTO `crm_document_type` VALUES ('3', '2', 'licence ', '2015-12-31 10:25:48');
INSERT INTO `crm_document_type` VALUES ('4', '2', 'pay slips', '2015-12-31 10:26:49');
INSERT INTO `crm_document_type` VALUES ('5', '1', 'others', '2016-01-05 10:44:29');
INSERT INTO `crm_document_type` VALUES ('6', '2', 'others', '2016-01-05 10:44:35');
INSERT INTO `crm_document_type` VALUES ('7', '3', 'others', '2016-01-05 10:44:50');

-- ----------------------------
-- Table structure for `crm_module`
-- ----------------------------
DROP TABLE IF EXISTS `crm_module`;
CREATE TABLE `crm_module` (
  `id_crm_module` int(11) NOT NULL AUTO_INCREMENT,
  `crm_module_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_crm_module`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crm_module
-- ----------------------------
INSERT INTO `crm_module` VALUES ('1', 'contact');
INSERT INTO `crm_module` VALUES ('2', 'company');
INSERT INTO `crm_module` VALUES ('3', 'project');

-- ----------------------------
-- Table structure for `crm_project`
-- ----------------------------
DROP TABLE IF EXISTS `crm_project`;
CREATE TABLE `crm_project` (
  `id_crm_project` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `project_title` varchar(255) DEFAULT NULL,
  `project_description` varchar(255) DEFAULT NULL,
  `project_main_sector_id` int(11) DEFAULT NULL,
  `project_sub_sector_id` int(11) DEFAULT NULL,
  `amount` bigint(20) DEFAULT NULL,
  `project_status` tinyint(4) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_crm_project`),
  KEY `company_id` (`company_id`),
  KEY `created_by` (`created_by`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `crm_project_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `product` (`id_product`),
  CONSTRAINT `crm_project_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id_company`),
  CONSTRAINT `crm_project_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crm_project
-- ----------------------------
INSERT INTO `crm_project` VALUES ('1', '1', '2', null, 'QLANA', 'Qlana', '2', '1', null, null, '2016-01-05 19:19:36');
INSERT INTO `crm_project` VALUES ('2', '1', '2', null, 'Stand to Fitness', 'sthand to fitness', null, null, null, null, '2016-01-04 16:09:25');
INSERT INTO `crm_project` VALUES ('9', '1', '2', null, 'Mandarin', 'mandarin', null, null, null, null, '2016-01-04 18:55:06');
INSERT INTO `crm_project` VALUES ('10', '1', '2', null, 'sdx', 'dcx', null, null, null, null, '2016-01-05 11:29:54');
INSERT INTO `crm_project` VALUES ('11', '1', '2', null, 'dsja', 'asda', null, null, null, null, '2016-01-05 11:32:29');
INSERT INTO `crm_project` VALUES ('12', '1', '2', null, 'Test Project', 'Project For test', '2', '6', null, null, '2016-01-05 17:42:25');
INSERT INTO `crm_project` VALUES ('13', '1', '2', null, 'Project Title*', null, null, null, null, null, '2016-01-05 17:43:14');
INSERT INTO `crm_project` VALUES ('14', '1', '2', null, 'Project Title*', null, null, null, null, null, '2016-01-05 17:43:14');
INSERT INTO `crm_project` VALUES ('15', '1', '2', null, 'New Project', 'prek', '1', '4', null, null, '2016-01-05 17:45:55');
INSERT INTO `crm_project` VALUES ('16', '1', '2', null, 'New Pero', '1221', '4', '5', null, null, '2016-01-05 17:47:20');
INSERT INTO `crm_project` VALUES ('17', '1', '2', null, 'Qlana2', 'qlana', '2', '1', null, null, '2016-01-05 17:57:05');
INSERT INTO `crm_project` VALUES ('18', '1', '2', null, 'fgth', null, null, null, null, null, '2016-01-05 18:44:11');
INSERT INTO `crm_project` VALUES ('19', '1', '2', null, 'personal loan', 'personal loan', '4', '5', null, null, '2016-01-05 19:21:12');
INSERT INTO `crm_project` VALUES ('20', '1', '2', null, 'test', 'test', '2', '1', null, null, '2016-01-05 19:25:19');
INSERT INTO `crm_project` VALUES ('21', '1', '2', null, 'dfsgdf', 'gsdfgdfg', '3', '0', null, null, '2016-01-05 19:28:51');
INSERT INTO `crm_project` VALUES ('22', '1', '2', null, 'car loan', 'to buy a car', '4', '5', null, null, '2016-01-06 10:39:03');
INSERT INTO `crm_project` VALUES ('23', '1', '2', null, 'test', 'gh', '2', '1', null, null, '2016-01-06 12:28:46');
INSERT INTO `crm_project` VALUES ('31', '1', '2', '1', 'test', 'test', '1', '4', '5000', null, '2016-01-09 19:40:48');

-- ----------------------------
-- Table structure for `crm_project_data`
-- ----------------------------
DROP TABLE IF EXISTS `crm_project_data`;
CREATE TABLE `crm_project_data` (
  `id_crm_project_data` int(11) NOT NULL AUTO_INCREMENT,
  `crm_project_id` int(11) DEFAULT NULL,
  `form_field_id` int(11) DEFAULT NULL,
  `form_field_value` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_crm_project_data`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crm_project_data
-- ----------------------------
INSERT INTO `crm_project_data` VALUES ('1', '1', '111', 'Running', '2016-01-04 17:31:09', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('2', '1', '112', 'Active', '2016-01-04 11:08:19', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('3', '1', '113', 'QLANA', '2016-01-04 18:42:51', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('4', '1', '114', 'sdaf', '2016-01-04 11:08:19', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('5', '1', '115', '2', '2016-01-05 17:25:45', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('6', '1', '116', '1', '2016-01-05 17:25:45', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('7', '1', '117', 'EADB', '2016-01-04 11:08:32', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('8', '1', '118', 'EADB', '2016-01-04 11:08:32', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('9', '1', '119', 'US', '2016-01-04 11:08:32', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('10', '1', '120', 'Temporary', '2016-01-04 11:08:32', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('11', '1', '121', 'EABD123', '2016-01-04 11:08:32', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('12', '1', '122', 'EADB description', '2016-01-04 11:10:25', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('13', '1', '123', 'EADB Concept', '2016-01-04 11:10:25', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('14', '1', '124', 'EADB implementation', '2016-01-04 11:10:25', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('15', '1', '125', '[{\"a\":\"Plan A\",\"b\":\"Plan B\",\"c\":\"Plan C\",\"d\":\"Plan D\"},[]]', '2016-01-04 11:10:25', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('16', '1', '126', 'dss', '2016-01-04 11:10:25', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('17', '1', '127', '[{\"date\":\"Jan 2015\",\"currency\":\"Indian\",\"amount\":100000,\"status\":\"active\"},[]]', '2016-01-04 11:12:56', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('18', '1', '128', '[{\"facility1\":\"facility 1\",\"facility2\":\"facility 2\",\"facility3\":\"facility 3\",\"facility4\":\"facility 4\",\"facility5\":\"facility 5\",\"facility6\":\"facility 6\"},[]]', '2016-01-04 11:34:31', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('19', '1', '129', 'purpose', '2016-01-04 11:34:31', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('20', '1', '130', 'Repayment', '2016-01-04 11:34:31', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('21', '1', '131', '10000000', '2016-01-04 11:34:31', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('22', '1', '132', 'convenants', '2016-01-04 11:34:31', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('23', '1', '133', '', '2016-01-04 11:34:31', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('24', '1', '134', 'comments', '2016-01-04 11:34:31', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('25', '1', '135', '[{\"risk\":\"Risk 1\",\"type\":\"Type 1\",\"description\":\"Description 1\",\"mitigation\":\"Mitigation 1\",\"riskgrade\":\"Risk Grade 1\"},[]]', '2016-01-04 11:36:45', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('26', '1', '136', 'Loan officers comments', '2016-01-04 11:38:24', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('27', '1', '137', 'argument', '2016-01-04 11:38:24', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('28', '1', '138', 'all client documents', '2016-01-04 17:33:28', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('29', '1', '139', 'monthly payment', '2016-01-04 11:42:26', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('30', '1', '140', 'payment previousloans', '2016-01-04 11:42:26', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('60', '1', '141', '[{\"loanamount\":1000000,\"terms\":\"4\",\"interest\":\"2\",\"installment\":1000},[]]', '2016-01-04 17:47:18', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('61', '1', '142', 'fgfdfdg', '2016-01-04 17:34:01', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('62', '13', '111', 'Project status', '2016-01-05 17:43:14', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('63', '13', '112', 'Pipeline', '2016-01-05 17:43:14', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('64', '13', '113', 'Project Title*', '2016-01-05 17:43:14', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('65', '13', '114', 'Project Description', '2016-01-05 17:43:14', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('66', '13', '115', '1', '2016-01-05 17:43:14', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('67', '13', '116', '4', '2016-01-05 17:43:14', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('68', '14', '111', 'Project status', '2016-01-05 17:43:14', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('69', '14', '112', 'Pipeline', '2016-01-05 17:43:14', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('70', '14', '113', 'Project Title*', '2016-01-05 17:43:14', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('71', '14', '114', 'Project Description', '2016-01-05 17:43:14', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('72', '14', '115', '1', '2016-01-05 17:43:14', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('73', '14', '116', '4', '2016-01-05 17:43:14', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('74', '12', '111', 'Project status', '2016-01-05 17:44:11', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('75', '12', '112', 'Pipeline', '2016-01-05 17:44:11', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('76', '12', '113', 'Project Title*', '2016-01-05 17:44:11', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('77', '12', '114', 'sds', '2016-01-05 17:44:11', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('78', '12', '115', '', '2016-01-05 17:44:11', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('79', '12', '116', '', '2016-01-05 17:44:11', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('80', '18', '111', 'active', '2016-01-05 18:44:12', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('81', '18', '112', 'fgh', '2016-01-05 18:44:12', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('82', '18', '113', 'fgth', '2016-01-05 18:44:12', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('83', '18', '114', 'sdfds', '2016-01-05 18:44:12', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('84', '18', '115', '1', '2016-01-05 18:44:12', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('85', '18', '116', '4', '2016-01-05 18:44:12', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('86', '20', '111', 'active', '2016-01-05 19:27:18', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('87', '20', '112', 'kumar', '2016-01-05 19:27:18', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('88', '20', '113', 'testing', '2016-01-05 19:27:18', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('89', '20', '114', 'testing', '2016-01-05 19:27:18', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('90', '20', '115', '2', '2016-01-05 19:27:18', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('91', '20', '116', '1', '2016-01-05 19:27:18', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('98', '22', '111', '', '2016-01-06 12:14:29', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('99', '22', '112', '', '2016-01-06 12:14:29', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('100', '22', '113', 'car loan', '2016-01-06 10:39:03', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('101', '22', '114', 'car loan application', '2016-01-06 10:38:24', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('102', '22', '115', '2', '2016-01-06 10:38:24', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('103', '22', '116', '1', '2016-01-06 10:38:24', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('104', '23', '111', '', '2016-01-06 12:28:46', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('105', '23', '112', '', '2016-01-06 12:28:46', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('106', '23', '113', 'test', '2016-01-06 12:28:46', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('107', '23', '114', 'gfjgjkl', '2016-01-06 12:28:46', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('108', '23', '115', '3', '2016-01-06 17:57:15', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('109', '23', '116', '2', '2016-01-06 17:57:15', '0000-00-00 00:00:00');
INSERT INTO `crm_project_data` VALUES ('110', '31', '111', '', '2016-01-09 19:40:48', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `crm_touch_point`
-- ----------------------------
DROP TABLE IF EXISTS `crm_touch_point`;
CREATE TABLE `crm_touch_point` (
  `id_crm_touch_point` int(11) NOT NULL AUTO_INCREMENT,
  `crm_module_id` int(11) DEFAULT NULL,
  `crm_touch_point_type_id` int(11) DEFAULT NULL,
  `touch_point_from_id` int(11) DEFAULT NULL,
  `touch_point_description` text,
  `reminder_date` datetime DEFAULT NULL,
  `reminder_time` time DEFAULT NULL,
  `is_reminder` tinyint(4) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `touch_point_status` tinyint(4) DEFAULT '1',
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_crm_touch_point`),
  KEY `crm_module_id` (`crm_module_id`),
  KEY `created_by` (`created_by`),
  KEY `crm_touch_point_type_id` (`crm_touch_point_type_id`),
  CONSTRAINT `crm_touch_point_ibfk_1` FOREIGN KEY (`crm_module_id`) REFERENCES `crm_module` (`id_crm_module`),
  CONSTRAINT `crm_touch_point_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`),
  CONSTRAINT `crm_touch_point_ibfk_3` FOREIGN KEY (`crm_touch_point_type_id`) REFERENCES `crm_touch_point_type` (`id_crm_touch_point_type`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crm_touch_point
-- ----------------------------
INSERT INTO `crm_touch_point` VALUES ('21', '1', '4', '111', 'dfgdfg', '2016-01-05 18:30:00', null, '1', '2', '1', '2016-01-08 17:37:33');
INSERT INTO `crm_touch_point` VALUES ('22', '1', '2', '111', 'dfgf', '2016-01-20 18:30:00', null, '1', '2', '1', '2016-01-08 17:49:42');
INSERT INTO `crm_touch_point` VALUES ('23', '1', '2', '111', 'sdfdsf', '2016-01-09 18:30:00', null, '1', '2', '1', '2016-01-08 17:52:11');
INSERT INTO `crm_touch_point` VALUES ('24', '1', '3', '111', 'sdfsdf', '2016-01-06 18:30:00', null, '1', '2', '1', '2016-01-08 17:52:31');
INSERT INTO `crm_touch_point` VALUES ('25', '1', '2', '111', 'naresh gurrala', '2016-01-02 18:30:00', null, '1', '2', '1', '2016-01-08 17:57:01');
INSERT INTO `crm_touch_point` VALUES ('26', '1', '3', '111', 'call discussion going on', '2016-01-08 18:30:00', null, null, '2', '1', '2016-01-08 19:03:34');
INSERT INTO `crm_touch_point` VALUES ('27', '1', '3', '111', 'grhttgtnjhjygjyt', '2016-01-08 18:30:00', null, null, '2', '1', '2016-01-08 19:03:51');
INSERT INTO `crm_touch_point` VALUES ('28', '1', '3', '111', 'testing', '2016-01-05 18:30:00', null, '1', '2', '1', '2016-01-08 19:06:48');
INSERT INTO `crm_touch_point` VALUES ('29', '2', '2', '36', 'sdfsdf', '2016-01-07 18:30:00', null, '1', '2', '1', '2016-01-08 19:24:34');
INSERT INTO `crm_touch_point` VALUES ('30', '3', '3', '23', 'sdfsdf', '2016-01-19 18:30:00', null, '1', '2', '1', '2016-01-08 19:24:55');
INSERT INTO `crm_touch_point` VALUES ('31', '3', '4', '23', 'dsfsdf', '2016-01-13 18:30:00', null, '1', '2', '1', '2016-01-08 19:25:04');
INSERT INTO `crm_touch_point` VALUES ('32', '2', '3', '36', 'sdfsdf', '2016-01-07 18:30:00', null, '1', '2', '1', '2016-01-08 19:25:25');
INSERT INTO `crm_touch_point` VALUES ('33', '3', '2', '23', 'dsfdsf', '2016-01-05 18:30:00', null, '1', '2', '1', '2016-01-08 19:32:42');
INSERT INTO `crm_touch_point` VALUES ('34', '3', '2', '23', 'sdfsdf', '2015-12-30 18:30:00', null, '1', '2', '1', '2016-01-08 19:32:55');
INSERT INTO `crm_touch_point` VALUES ('35', '3', '2', '23', 'sfdfsd', '2015-12-30 18:30:00', null, '1', '2', '1', '2016-01-08 19:33:12');
INSERT INTO `crm_touch_point` VALUES ('36', '1', '2', '116', '2016-01-06 added, naresh was call to me', '2016-01-04 18:30:00', null, null, '2', '1', '2016-01-09 09:45:45');
INSERT INTO `crm_touch_point` VALUES ('37', '1', '4', '116', 'sdfdsf', '2016-01-02 18:30:00', null, '1', '2', '1', '2016-01-09 09:51:04');
INSERT INTO `crm_touch_point` VALUES ('38', '1', '1', '116', 'add to caontact', '2016-01-06 18:30:00', null, null, '2', '1', '2016-01-09 09:57:41');
INSERT INTO `crm_touch_point` VALUES ('39', '2', '1', '36', 'add to company', '2016-01-05 18:30:00', null, null, '2', '1', '2016-01-09 09:58:09');
INSERT INTO `crm_touch_point` VALUES ('40', '3', '1', '23', 'add to project', '2016-01-06 18:30:00', null, null, '2', '1', '2016-01-09 09:59:02');
INSERT INTO `crm_touch_point` VALUES ('41', '1', '2', '116', 'add to contact', '2016-01-06 18:30:00', null, null, '2', '1', '2016-01-09 10:01:12');
INSERT INTO `crm_touch_point` VALUES ('42', '1', '3', '115', 'add to 2 form', '2016-01-08 18:30:00', null, '1', '2', '1', '2016-01-09 10:05:41');
INSERT INTO `crm_touch_point` VALUES ('43', '1', '2', '116', 'drgdfg', '2016-01-02 18:30:00', null, null, '2', '1', '2016-01-09 16:17:52');
INSERT INTO `crm_touch_point` VALUES ('44', '1', '1', '116', 'dfgdfg', '2016-01-06 18:30:00', null, null, '2', '1', '2016-01-09 12:29:14');
INSERT INTO `crm_touch_point` VALUES ('45', '1', '2', '116', 'fghfgh', '2016-01-05 18:30:00', null, null, '2', '1', '2016-01-09 16:17:37');
INSERT INTO `crm_touch_point` VALUES ('46', '1', '2', '116', 'fghfg', '2016-01-06 18:30:00', null, null, '2', '1', '2016-01-09 16:17:38');
INSERT INTO `crm_touch_point` VALUES ('47', '1', '4', '116', 'fghfgh', '2016-01-06 18:30:00', null, null, '2', '1', '2016-01-09 16:17:40');
INSERT INTO `crm_touch_point` VALUES ('48', '1', '2', '116', 'fgh maresg gurrasda', '2016-01-08 18:30:00', null, null, '2', '1', '2016-01-09 16:17:54');
INSERT INTO `crm_touch_point` VALUES ('49', '1', '2', '116', 'dfgdfg naresdfsdfsdf sdfsdf', '2016-01-04 23:55:00', null, '1', '2', '1', '2016-01-09 11:55:50');
INSERT INTO `crm_touch_point` VALUES ('50', '1', '2', '116', 'fghfgh sdfsdrwer', '2016-01-06 00:00:00', null, '1', '2', '1', '2016-01-09 11:56:19');
INSERT INTO `crm_touch_point` VALUES ('51', '1', '2', '116', 'gh ramehs dfgfd', '2016-01-07 21:45:00', null, '1', '2', '1', '2016-01-09 11:56:53');
INSERT INTO `crm_touch_point` VALUES ('52', '1', '2', '116', 'dfgdfg', '2016-01-08 08:40:00', null, '1', '2', '1', '2016-01-09 11:57:34');
INSERT INTO `crm_touch_point` VALUES ('53', '1', '4', '116', 'ddad', '2015-12-29 18:30:00', null, null, '2', '1', '2016-01-09 12:12:41');
INSERT INTO `crm_touch_point` VALUES ('54', '1', '4', '116', 'dsds', '2016-01-06 18:30:00', null, null, '2', '1', '2016-01-09 12:12:54');
INSERT INTO `crm_touch_point` VALUES ('55', '1', '2', '116', 'asd good asd', '2016-01-07 09:20:00', null, '1', '2', '1', '2016-01-09 12:57:36');
INSERT INTO `crm_touch_point` VALUES ('56', '1', '3', '116', 'dfdsf', '2016-01-21 09:00:00', null, '1', '2', '1', '2016-01-09 13:00:53');
INSERT INTO `crm_touch_point` VALUES ('57', '1', '3', '116', 'sasdsadsa', '2016-01-27 09:20:00', null, '1', '2', '1', '2016-01-09 13:01:20');
INSERT INTO `crm_touch_point` VALUES ('58', '1', '2', '116', 'xzcxzcxzc', '2016-01-27 13:20:00', null, '1', '2', '1', '2016-01-09 13:07:12');
INSERT INTO `crm_touch_point` VALUES ('59', '1', '3', '116', 'testing touch point feature chat added', '2016-01-28 13:00:00', null, '1', '2', '1', '2016-01-09 16:19:22');
INSERT INTO `crm_touch_point` VALUES ('60', '2', '2', '39', 'dfgf', '2015-12-30 05:00:00', null, '1', '2', '1', '2016-01-09 16:47:57');
INSERT INTO `crm_touch_point` VALUES ('61', '1', '2', '126', 'fdygdfgfd', '2015-12-24 20:40:00', null, '1', '2', '1', '2016-01-09 17:22:50');
INSERT INTO `crm_touch_point` VALUES ('62', '1', '2', '126', 'sdfdsf', '2016-01-30 14:20:00', null, '1', '2', '1', '2016-01-09 17:23:09');
INSERT INTO `crm_touch_point` VALUES ('63', '1', '3', '127', 'fsfsf', '2016-01-05 21:00:00', null, '1', '2', '1', '2016-01-09 18:26:32');
INSERT INTO `crm_touch_point` VALUES ('64', '1', '2', '127', 'hjhj', '2016-01-06 01:40:00', null, '1', '2', '1', '2016-01-09 18:27:32');
INSERT INTO `crm_touch_point` VALUES ('65', '1', '2', '127', 'test', '2016-01-02 22:35:00', null, null, '2', '1', '2016-01-09 18:29:11');
INSERT INTO `crm_touch_point` VALUES ('66', '1', '4', '127', 'wewe', '2016-01-05 11:40:00', null, null, '2', '1', '2016-01-09 18:29:33');
INSERT INTO `crm_touch_point` VALUES ('67', '1', '3', '127', 'wrwr', '2016-01-05 16:35:00', null, '1', '2', '1', '2016-01-09 18:29:52');
INSERT INTO `crm_touch_point` VALUES ('68', '1', '3', '127', 'bootstrap modal confirmation dialogs all over the place and I am sick of  ...', '2016-01-27 01:00:00', null, '1', '2', '1', '2016-01-09 18:52:09');

-- ----------------------------
-- Table structure for `crm_touch_point_type`
-- ----------------------------
DROP TABLE IF EXISTS `crm_touch_point_type`;
CREATE TABLE `crm_touch_point_type` (
  `id_crm_touch_point_type` int(11) NOT NULL AUTO_INCREMENT,
  `crm_touch_point_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_crm_touch_point_type`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of crm_touch_point_type
-- ----------------------------
INSERT INTO `crm_touch_point_type` VALUES ('1', 'Mail');
INSERT INTO `crm_touch_point_type` VALUES ('2', 'Call');
INSERT INTO `crm_touch_point_type` VALUES ('3', 'Chat');
INSERT INTO `crm_touch_point_type` VALUES ('4', 'Message');

-- ----------------------------
-- Table structure for `form`
-- ----------------------------
DROP TABLE IF EXISTS `form`;
CREATE TABLE `form` (
  `id_form` int(11) NOT NULL AUTO_INCREMENT,
  `form_name` varchar(255) NOT NULL,
  `form_template` varchar(255) DEFAULT NULL,
  `form_order` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_form`),
  KEY `section_id` (`section_id`),
  CONSTRAINT `form_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `section` (`id_section`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of form
-- ----------------------------
INSERT INTO `form` VALUES ('1', 'Applicant\'s Information', 'applicant-information.html', '1', '1');
INSERT INTO `form` VALUES ('2', 'Family and Residence Information', 'family-information.html', '2', '1');
INSERT INTO `form` VALUES ('3', 'Personal Financial Information', 'financial-information.html', '3', '1');
INSERT INTO `form` VALUES ('4', 'Previous Experience with insitution', 'previous-experience.html', '4', '1');
INSERT INTO `form` VALUES ('5', 'Other Sources of Houshold Income (monthly) ', 'other-sources-income.html', '1', '2');
INSERT INTO `form` VALUES ('6', 'Non Business Expenditure', 'non-business-expenditure.html', '2', '2');
INSERT INTO `form` VALUES ('7', 'Business Information', 'business-information.html', '1', '3');
INSERT INTO `form` VALUES ('8', 'Business Assessment', 'business-assessment.html', '1', '4');
INSERT INTO `form` VALUES ('9', 'Assessment Questions', 'assessment-questions.html', '1', '5');
INSERT INTO `form` VALUES ('10', 'Risks coming from Suppliers', 'risk-suppliers.html', '2', '5');
INSERT INTO `form` VALUES ('11', 'Threats from Subsitutes', 'threats-subsitutes.html', '3', '5');
INSERT INTO `form` VALUES ('12', 'Bargaining power of buyers', 'bargaining-buyers.html', '4', '5');
INSERT INTO `form` VALUES ('13', 'Threats of New Entrants', 'threats-of-new-entrants.html', '5', '5');
INSERT INTO `form` VALUES ('14', 'Cash Inflows', 'cash-inflow.html', '1', '6');
INSERT INTO `form` VALUES ('15', 'Cash Outflows', 'cash-outflow.html', '2', '6');
INSERT INTO `form` VALUES ('16', 'Financial Indicators', 'financial-indicators.html', '3', '6');
INSERT INTO `form` VALUES ('17', 'Strengths', 'strengths.html', '1', '7');
INSERT INTO `form` VALUES ('18', 'Weakness', 'weakness.html', '2', '7');
INSERT INTO `form` VALUES ('19', 'Opportunities', 'opportunities.html', '3', '7');
INSERT INTO `form` VALUES ('20', 'Threats', 'threats.html', '4', '7');
INSERT INTO `form` VALUES ('21', 'Basic Inforamation', 'basic-information.html', '1', '8');
INSERT INTO `form` VALUES ('22', 'Counterpart Information', 'counterpart-information.html', '2', '8');
INSERT INTO `form` VALUES ('24', 'Concept Note Details', 'concept-note-details.html', '4', '8');
INSERT INTO `form` VALUES ('25', 'Loan Information', 'loan-information.html', '5', '8');
INSERT INTO `form` VALUES ('26', 'Facilities', 'facilities-form.html', '1', '9');
INSERT INTO `form` VALUES ('27', 'Risk Assessment ', 'risk-assessment.html', '1', '10');
INSERT INTO `form` VALUES ('28', 'Analysis and Recommendation', 'analysis-recommendation.html', '1', '11');
INSERT INTO `form` VALUES ('29', 'Key Risk Factors ', 'key-risk-factors.html ', '2', '11');
INSERT INTO `form` VALUES ('30', 'Recommended Terms ', 'recommended-terms.html', '3', '11');

-- ----------------------------
-- Table structure for `form_field`
-- ----------------------------
DROP TABLE IF EXISTS `form_field`;
CREATE TABLE `form_field` (
  `id_form_field` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) DEFAULT NULL,
  `field_label` varchar(255) DEFAULT NULL,
  `field_name` varchar(255) DEFAULT NULL,
  `field_type` varchar(255) DEFAULT NULL,
  `field_tooltip` varchar(255) DEFAULT NULL,
  `is_required` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_form_field`),
  KEY `form_id` (`form_id`),
  CONSTRAINT `form_field_ibfk_1` FOREIGN KEY (`form_id`) REFERENCES `form` (`id_form`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of form_field
-- ----------------------------
INSERT INTO `form_field` VALUES ('1', '1', 'First Name', 'first_name', 'input', null, null);
INSERT INTO `form_field` VALUES ('2', '1', 'Last Name', 'last_name', 'input', null, null);
INSERT INTO `form_field` VALUES ('3', '1', 'Gender', 'gender', 'radio', null, null);
INSERT INTO `form_field` VALUES ('4', '1', 'Date of Birth', 'date_of_birth', 'date', null, null);
INSERT INTO `form_field` VALUES ('5', '1', 'Place of Birth', 'place_of_birth', 'date', null, null);
INSERT INTO `form_field` VALUES ('6', '1', 'ID Number', 'id_number', 'input', null, null);
INSERT INTO `form_field` VALUES ('7', '1', 'Place/Date of Issue', 'issued_from', 'input', null, null);
INSERT INTO `form_field` VALUES ('8', '1', 'Marital Status', 'marital_status', 'radio', null, null);
INSERT INTO `form_field` VALUES ('9', '1', 'Education', 'education', 'input', null, null);
INSERT INTO `form_field` VALUES ('10', '1', 'Employment Status', 'employment_status', 'input', null, null);
INSERT INTO `form_field` VALUES ('11', '1', 'Employment', 'employment', 'input', null, null);
INSERT INTO `form_field` VALUES ('12', '1', 'Profession', 'profession', 'input', null, null);
INSERT INTO `form_field` VALUES ('13', '1', 'Is the client in good health?', 'client_good_health', 'radio', null, null);
INSERT INTO `form_field` VALUES ('14', '1', 'Does the client have life insurance', 'client_life_insurance', 'radio', null, null);
INSERT INTO `form_field` VALUES ('15', '2', 'Spouse Name', 'spouse_name', 'input', null, null);
INSERT INTO `form_field` VALUES ('16', '2', 'Spouse ID', 'spouse_id', 'input', null, null);
INSERT INTO `form_field` VALUES ('17', '2', 'Profession', 'spouse_profession', 'input', null, null);
INSERT INTO `form_field` VALUES ('18', '2', 'Employment', 'spouse_employment', 'input', null, null);
INSERT INTO `form_field` VALUES ('19', '2', 'Employment Status', 'spouse_employment_status', 'input', null, null);
INSERT INTO `form_field` VALUES ('20', '2', 'Education', 'spouse_education', 'input', null, null);
INSERT INTO `form_field` VALUES ('21', '2', 'Residence Physical Address', 'residence_address', 'input', null, null);
INSERT INTO `form_field` VALUES ('22', '2', 'Living in Residence Since', 'residence_since', 'input', null, null);
INSERT INTO `form_field` VALUES ('23', '2', 'Residence', 'residence', 'radio', null, null);
INSERT INTO `form_field` VALUES ('24', '2', 'Landlord', 'landlord', 'input', null, null);
INSERT INTO `form_field` VALUES ('25', '2', 'Number of Dependants', 'total_dependants', 'input', null, null);
INSERT INTO `form_field` VALUES ('26', '2', 'Male', 'male_dependant', 'input', null, null);
INSERT INTO `form_field` VALUES ('27', '2', 'Female', 'female_dependant', 'input', null, null);
INSERT INTO `form_field` VALUES ('28', '2', 'Thereof pre-school', 'thereof_pre_school', 'input', null, null);
INSERT INTO `form_field` VALUES ('29', '2', 'Thereof in school', 'thereof_in_school', 'input', null, null);
INSERT INTO `form_field` VALUES ('30', '2', 'Thereof in university', 'thereof_in_university', 'input', null, null);
INSERT INTO `form_field` VALUES ('31', '2', 'Thereof post-school', 'thereof_post_school', 'input', null, null);
INSERT INTO `form_field` VALUES ('32', '2', 'Is there a family memberwith chronic illness or disability ?', 'member_disability', 'radio', null, null);
INSERT INTO `form_field` VALUES ('33', '2', 'Phone (main)', 'phone_number', 'input', null, null);
INSERT INTO `form_field` VALUES ('34', '2', 'Phone (alt)', 'alternative_phone_number', 'input', null, null);
INSERT INTO `form_field` VALUES ('35', '2', 'E-mail', 'email', 'input', null, null);
INSERT INTO `form_field` VALUES ('36', '2', 'Other (web/FB/…)', 'other_networks', 'json', null, null);
INSERT INTO `form_field` VALUES ('37', '3', 'Personal Financial Information ', 'personal_financial_information', 'json', null, null);
INSERT INTO `form_field` VALUES ('38', '4', 'Current Loan Amount Disb.', 'current_loan_amount_distribution', 'input', null, null);
INSERT INTO `form_field` VALUES ('39', '4', 'Currenct outstanding loan amt', 'current_outstanding_loan_amount', 'input', null, null);
INSERT INTO `form_field` VALUES ('40', '4', 'Total previous loans', 'total_previous_loans', 'input', null, null);
INSERT INTO `form_field` VALUES ('41', '4', 'Client since', 'client_since', 'input', null, null);
INSERT INTO `form_field` VALUES ('42', '4', 'Current loan payments on time', 'current_loan_payments_on_time', 'radio', null, null);
INSERT INTO `form_field` VALUES ('43', '4', 'Previous loan payments on time', 'previous_loan_payments_on_time', 'radio', null, null);
INSERT INTO `form_field` VALUES ('44', '5', 'Other Surces of Houshold Income (monthly)', 'other_sources_income', 'json', null, null);
INSERT INTO `form_field` VALUES ('45', '6', 'Non Business Expenditure', 'non_business_expenditure', 'json', null, null);
INSERT INTO `form_field` VALUES ('46', '7', 'Business Name', 'business_name', 'input', null, null);
INSERT INTO `form_field` VALUES ('47', '7', 'New Business', 'new_business', 'radio', null, null);
INSERT INTO `form_field` VALUES ('48', '7', 'if no, start date', 'business_start_date', 'date', null, null);
INSERT INTO `form_field` VALUES ('49', '7', 'Ownership', 'ownership', 'input', null, null);
INSERT INTO `form_field` VALUES ('50', '7', 'Comments', 'ownership_comments', 'input', null, null);
INSERT INTO `form_field` VALUES ('51', '7', 'Key Relationship Manager in the Institution', 'key_relationship', 'input', null, null);
INSERT INTO `form_field` VALUES ('52', '7', 'Classification', 'classification', 'input', null, null);
INSERT INTO `form_field` VALUES ('53', '7', 'Business Registered?', 'business_registered', 'radio', null, null);
INSERT INTO `form_field` VALUES ('54', '7', 'Recording Facility', 'recording_facility', 'input', null, null);
INSERT INTO `form_field` VALUES ('55', '7', 'Registration Number', 'registration_number', 'input', null, null);
INSERT INTO `form_field` VALUES ('56', '7', 'Business Physical Address', 'business_physical_address', 'input', null, null);
INSERT INTO `form_field` VALUES ('57', '7', 'Operating in this place since:', 'operating_in_this_place', 'input', null, null);
INSERT INTO `form_field` VALUES ('58', '7', 'Business phone main', 'business_phone_number', 'input', null, null);
INSERT INTO `form_field` VALUES ('59', '7', 'Business phone alt.', 'business_alternative_phone_number', 'input', null, null);
INSERT INTO `form_field` VALUES ('60', '7', 'Buiness e-mail', 'business_email', 'input', null, null);
INSERT INTO `form_field` VALUES ('61', '7', 'Other (web/FB/..)', 'business_social_networks', 'input', null, null);
INSERT INTO `form_field` VALUES ('62', '7', 'Years in similar Business', 'years_in_similar_business', 'input', null, null);
INSERT INTO `form_field` VALUES ('63', '7', 'Years in Business', 'years_in_business', 'input', null, null);
INSERT INTO `form_field` VALUES ('64', '7', 'Subsector', 'subsector', 'input', null, null);
INSERT INTO `form_field` VALUES ('65', '7', 'Nature/Type of Business', 'type_of_business', 'input', null, null);
INSERT INTO `form_field` VALUES ('66', '7', 'Number of skilled employees', 'number_of_skilled_employees', 'input', null, null);
INSERT INTO `form_field` VALUES ('67', '7', 'Thereof management', 'thereof_management', 'input', null, null);
INSERT INTO `form_field` VALUES ('68', '7', 'Thereof relatives', 'thereof_relatives', 'input', null, null);
INSERT INTO `form_field` VALUES ('69', '7', 'Number of Employees', 'number_of_employees', 'input', null, null);
INSERT INTO `form_field` VALUES ('70', '7', 'Description of the Business', 'business_description', 'input', null, null);
INSERT INTO `form_field` VALUES ('71', '8', 'History of the Business - Explain the history of the business', 'business_history', 'input', null, null);
INSERT INTO `form_field` VALUES ('72', '8', 'Products, describe the products and services of the company', 'company_services', 'input', null, null);
INSERT INTO `form_field` VALUES ('73', '8', 'Markets - define the target markets and market segments in which the company competes', 'company_markets', 'input', null, null);
INSERT INTO `form_field` VALUES ('74', '8', 'Customer – describe the existing customer base', 'existing_customer_base', 'input', null, null);
INSERT INTO `form_field` VALUES ('75', '8', 'Competition – Outline the competitive situation', 'competitive_situation', 'input', null, null);
INSERT INTO `form_field` VALUES ('76', '8', 'Opportunities - market positioning of the products', 'market_positioning', 'input', null, null);
INSERT INTO `form_field` VALUES ('77', '8', 'Opportunities - pricing policy', 'pricing_policy', 'input', null, null);
INSERT INTO `form_field` VALUES ('78', '8', 'Opportunities - promotion strategy', 'promotion_strategy', 'input', null, null);
INSERT INTO `form_field` VALUES ('79', '8', 'Opportunities - sales channels', 'sales_channels', 'input', null, null);
INSERT INTO `form_field` VALUES ('80', '8', 'Operations - premises of the institution/production facilities', 'production_facilities', 'input', null, null);
INSERT INTO `form_field` VALUES ('81', '8', 'Operations - production technology/MIS', 'production_technology', 'input', null, null);
INSERT INTO `form_field` VALUES ('82', '9', 'Assessment Questions', 'assessment_questions', 'json', null, null);
INSERT INTO `form_field` VALUES ('83', '10', 'Risks coming from Suppliers', 'risks_from_suppliers', 'json', null, null);
INSERT INTO `form_field` VALUES ('84', '11', 'Threats from Subsitutes', 'threats_from_subsitutes', 'json', null, null);
INSERT INTO `form_field` VALUES ('85', '12', 'Bargaining power of buyers', 'bargaining_buyers', 'json', null, null);
INSERT INTO `form_field` VALUES ('86', '13', 'Threats of New Entrants', 'threats_new_entrants', 'json', null, null);
INSERT INTO `form_field` VALUES ('87', '14', 'Cash Inflows', 'cash_inflow', 'json', null, null);
INSERT INTO `form_field` VALUES ('88', '15', 'Cash Outflows', 'cash_outflow', 'json', null, null);
INSERT INTO `form_field` VALUES ('89', '16', 'Financial Indicators', 'financial_indicators', 'json', null, null);
INSERT INTO `form_field` VALUES ('90', '17', 'What is the client doing well (in sales, marketing, operations, management)?', 'client_doing_well', 'input', null, null);
INSERT INTO `form_field` VALUES ('91', '17', 'What are the client’s assets (tangibles as well as intangibles)?', 'client_assests', 'input', null, null);
INSERT INTO `form_field` VALUES ('92', '17', 'What are the client’s core competencies?', 'client_core_competencies', 'input', null, null);
INSERT INTO `form_field` VALUES ('93', '17', 'Where is the client earning money with the company?', 'client_earning_money', 'input', null, null);
INSERT INTO `form_field` VALUES ('94', '17', 'What experience does the client have?', 'client_experience', 'input', null, null);
INSERT INTO `form_field` VALUES ('95', '18', 'Where is the company looking old/outdated?', 'company_looking', 'input', null, null);
INSERT INTO `form_field` VALUES ('96', '18', 'What areas are needed but not existing (customer service, marketing, accounting, planning)?', 'areas_needed', 'input', null, null);
INSERT INTO `form_field` VALUES ('97', '18', 'Where do resources lack? Is the company relying on outdated equipment? Are enough funds available?', 'resources_lack', 'input', null, null);
INSERT INTO `form_field` VALUES ('98', '18', 'What can be done better? Are there inefficiencies?', 'inefficiencies', 'input', null, null);
INSERT INTO `form_field` VALUES ('99', '18', 'Where is the company losing money?', 'company_losing_money', 'input', null, null);
INSERT INTO `form_field` VALUES ('100', '19', 'Where is the blue sky in your environment?', 'blue_sky_environment', 'input', null, null);
INSERT INTO `form_field` VALUES ('101', '19', 'What new needs of customers could you meet?', 'customer_needs', 'input', null, null);
INSERT INTO `form_field` VALUES ('102', '19', 'What are the economic trends that benefit you? Are there financial backers looking for opportunities?', 'economic_trends', 'input', null, null);
INSERT INTO `form_field` VALUES ('103', '19', 'What are the emerging political and social opportunities?', 'political_social_opportunities', 'input', null, null);
INSERT INTO `form_field` VALUES ('104', '19', 'What are the technological breakthroughs?', 'technological_breakthroughs', 'input', null, null);
INSERT INTO `form_field` VALUES ('105', '19', 'Where niches have your competitors missed? Are there areas from which competitors are withdrawing?', 'competitors_areas', 'input', null, null);
INSERT INTO `form_field` VALUES ('106', '20', 'Where are the red alerts in your environment? Is the company relying on one key supplier, client, or employee)?', 'environment_alerts', 'input', null, null);
INSERT INTO `form_field` VALUES ('107', '20', 'What are the negative economic trends?', 'negative_economic_trends', 'input', null, null);
INSERT INTO `form_field` VALUES ('108', '20', 'What are the negative political and social trends?', 'negative_political_social_trends', 'input', null, null);
INSERT INTO `form_field` VALUES ('109', '20', 'Where are competitors about to bite you?', 'competitors', 'input', null, null);
INSERT INTO `form_field` VALUES ('110', '20', 'Where are you vulnerable?', 'vulnerable', 'input', null, null);
INSERT INTO `form_field` VALUES ('111', '21', 'Project Status', 'project_status', 'input', null, null);
INSERT INTO `form_field` VALUES ('112', '21', 'Pipeline', 'pile_line', 'input', null, null);
INSERT INTO `form_field` VALUES ('113', '21', 'Project Title', 'project_title', 'input', null, null);
INSERT INTO `form_field` VALUES ('114', '21', 'Project Description', 'project_description', 'input', null, null);
INSERT INTO `form_field` VALUES ('115', '21', 'Project Main Sector', 'project_main_sector_id', 'input', null, null);
INSERT INTO `form_field` VALUES ('116', '21', 'Project Subsector', 'project_sub_sector_id', 'input', null, null);
INSERT INTO `form_field` VALUES ('117', '22', 'Borrower Parent Company', 'borrower_parent_company', 'input', null, null);
INSERT INTO `form_field` VALUES ('118', '22', 'Borrower Legal Name', 'borrower_legal_name', 'input', null, null);
INSERT INTO `form_field` VALUES ('119', '22', 'Borrower Adress', 'borrower_adress', 'input', null, null);
INSERT INTO `form_field` VALUES ('120', '22', 'Borrower Registration Type', 'borrower_registration_type', 'input', null, null);
INSERT INTO `form_field` VALUES ('121', '22', 'Borrower Registration Number', 'borrower_registration_number', 'input', null, null);
INSERT INTO `form_field` VALUES ('122', '24', 'Project Description', 'project_description', 'input', null, null);
INSERT INTO `form_field` VALUES ('123', '24', 'Project Concept/Purpose', 'project_concept_purpose', 'input', null, null);
INSERT INTO `form_field` VALUES ('124', '24', 'Status of Project Implementation', 'project_implementation_status', 'input', null, null);
INSERT INTO `form_field` VALUES ('125', '24', 'Project Cost / Financing Plan ', 'project_cost', 'json', null, null);
INSERT INTO `form_field` VALUES ('126', '24', 'Summary of availabale information ', 'summary', 'input', null, null);
INSERT INTO `form_field` VALUES ('127', '25', 'Loan Information', 'loan_information', 'json', null, null);
INSERT INTO `form_field` VALUES ('128', '26', 'Facility Details', 'facility_details', 'json', null, null);
INSERT INTO `form_field` VALUES ('129', '26', 'Purpose', 'purpose', 'input', null, null);
INSERT INTO `form_field` VALUES ('130', '26', 'Repayment structure', 'repayment_structure', 'input', null, null);
INSERT INTO `form_field` VALUES ('131', '26', 'Fees and Commissions', 'fees_commission', 'input', null, null);
INSERT INTO `form_field` VALUES ('132', '26', 'Covenants', 'covenants', 'input', null, null);
INSERT INTO `form_field` VALUES ('133', '26', 'Collateral ', 'collateral ', 'input', null, null);
INSERT INTO `form_field` VALUES ('134', '26', 'Comments', 'comments', 'input', null, null);
INSERT INTO `form_field` VALUES ('135', '27', 'Risk Assessment ', 'risk_assessment', 'json', null, null);
INSERT INTO `form_field` VALUES ('136', '28', 'Loan Officer\'s Comments on Borrower', 'loanofficers_comments_borrower', 'input', null, null);
INSERT INTO `form_field` VALUES ('137', '28', 'Loan Officer\'s Arguments', 'loanofficers_arguments', 'input', null, null);
INSERT INTO `form_field` VALUES ('138', '29', 'Client failed to deliver all documents', 'client_failed_deliver_documents', 'input', null, null);
INSERT INTO `form_field` VALUES ('139', '29', 'High monthly payment comparing to the client salary (define %amount)', 'payment_comparing_client_salary', 'input', null, null);
INSERT INTO `form_field` VALUES ('140', '29', 'Late payment from previous loans from [MFI]', 'last_payment_previous_loan', 'input', null, null);
INSERT INTO `form_field` VALUES ('141', '30', 'Recommended Terms', 'recommended_terms', 'json', null, null);
INSERT INTO `form_field` VALUES ('142', '30', 'Comments or other Conditions (e.g. collateral, co-guarantors, etc.)', 'comments_conditions', 'input', null, null);

-- ----------------------------
-- Table structure for `keys`
-- ----------------------------
DROP TABLE IF EXISTS `keys`;
CREATE TABLE `keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of keys
-- ----------------------------
INSERT INTO `keys` VALUES ('1', '094c51f3e6771788b2c1058d50056a58', '0', '0', '0', null, '2015-12-29 18:38:26', '1');
INSERT INTO `keys` VALUES ('2', 'a5c4d22169036c46a0ba99f845a72af3', '0', '0', '0', null, '2016-01-04 09:35:33', '2');
INSERT INTO `keys` VALUES ('3', '414d4a52a9f68e74c7e4f422c6139317', '0', '0', '0', null, '2015-12-16 15:21:07', '39');
INSERT INTO `keys` VALUES ('4', '482360161964662ccd743a6c87d147a9', '0', '0', '0', null, '2015-12-12 11:12:59', '41');
INSERT INTO `keys` VALUES ('5', '8c13ce13815240874b5b4452a716e960', '0', '0', '0', null, '2015-12-17 10:44:55', '43');
INSERT INTO `keys` VALUES ('6', 'b8b106fb8a0d699aaed4a33f900ebb94', '0', '0', '0', null, '2015-12-14 13:13:59', '42');
INSERT INTO `keys` VALUES ('7', '55d018d7c605e306b371078fdc130c8a', '0', '0', '0', null, '2015-12-16 15:38:36', '87');
INSERT INTO `keys` VALUES ('8', '749a1fc005075ce9cbe2f2d93f9304cb', '0', '0', '0', null, '2015-12-24 13:00:47', '89');

-- ----------------------------
-- Table structure for `media`
-- ----------------------------
DROP TABLE IF EXISTS `media`;
CREATE TABLE `media` (
  `id_media` int(11) NOT NULL AUTO_INCREMENT,
  `media_title` varchar(255) DEFAULT NULL,
  `media_target` varchar(255) DEFAULT NULL,
  `media_module_id` int(11) DEFAULT NULL,
  `media_status` tinyint(4) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_media`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of media
-- ----------------------------
INSERT INTO `media` VALUES ('2', 'Id proof', 'contact', '84', null, '2015-12-30 15:14:41');
INSERT INTO `media` VALUES ('3', null, 'contact', null, null, '2015-12-30 17:12:50');
INSERT INTO `media` VALUES ('4', null, 'contact', null, null, '2015-12-30 17:16:41');
INSERT INTO `media` VALUES ('5', null, 'contact', null, null, '2015-12-30 17:19:15');
INSERT INTO `media` VALUES ('6', null, 'contact', null, null, '2015-12-30 17:21:26');
INSERT INTO `media` VALUES ('7', null, 'contact', null, null, '2015-12-30 17:36:08');
INSERT INTO `media` VALUES ('8', null, 'contact', null, null, '2015-12-30 17:37:12');
INSERT INTO `media` VALUES ('9', null, 'contact', null, null, '2015-12-30 17:41:47');
INSERT INTO `media` VALUES ('10', null, 'contact', null, null, '2015-12-30 17:42:10');
INSERT INTO `media` VALUES ('11', null, 'contact', null, null, '2015-12-30 17:48:18');
INSERT INTO `media` VALUES ('12', null, 'contact', null, null, '2015-12-30 17:51:30');
INSERT INTO `media` VALUES ('13', null, 'contact', null, null, '2015-12-30 17:51:37');
INSERT INTO `media` VALUES ('14', null, 'contact', null, null, '2015-12-30 17:55:01');
INSERT INTO `media` VALUES ('15', null, 'contact', null, null, '2015-12-30 18:20:46');
INSERT INTO `media` VALUES ('16', null, 'contact', null, null, '2015-12-30 18:20:59');
INSERT INTO `media` VALUES ('17', null, 'contact', null, null, '2015-12-30 18:26:05');
INSERT INTO `media` VALUES ('18', null, 'contact', null, null, '2015-12-30 18:26:05');
INSERT INTO `media` VALUES ('19', null, 'contact', null, null, '2015-12-30 18:26:06');
INSERT INTO `media` VALUES ('20', null, 'contact', null, null, '2015-12-30 18:26:06');
INSERT INTO `media` VALUES ('21', null, 'contact', null, null, '2015-12-30 18:26:06');
INSERT INTO `media` VALUES ('22', null, 'contact', null, null, '2015-12-30 18:26:06');
INSERT INTO `media` VALUES ('23', null, 'contact', null, null, '2015-12-30 18:28:01');
INSERT INTO `media` VALUES ('24', null, 'contact', null, null, '2015-12-30 18:28:01');
INSERT INTO `media` VALUES ('25', null, 'contact', null, null, '2015-12-30 18:28:01');
INSERT INTO `media` VALUES ('26', null, 'contact', null, null, '2015-12-30 18:28:01');
INSERT INTO `media` VALUES ('27', null, 'contact', null, null, '2015-12-30 18:28:01');
INSERT INTO `media` VALUES ('28', null, 'contact', null, null, '2015-12-30 18:28:01');
INSERT INTO `media` VALUES ('29', null, 'contact', null, null, '2015-12-30 18:28:27');
INSERT INTO `media` VALUES ('30', null, 'contact', null, null, '2015-12-30 18:28:28');
INSERT INTO `media` VALUES ('31', null, 'contact', null, null, '2015-12-30 18:28:28');
INSERT INTO `media` VALUES ('32', null, 'contact', null, null, '2015-12-30 18:28:28');
INSERT INTO `media` VALUES ('33', null, 'contact', null, null, '2015-12-30 18:28:28');
INSERT INTO `media` VALUES ('34', null, 'contact', null, null, '2015-12-30 18:31:57');
INSERT INTO `media` VALUES ('35', null, 'contact', null, null, '2015-12-30 18:31:57');
INSERT INTO `media` VALUES ('36', null, 'contact', null, null, '2015-12-30 18:31:57');
INSERT INTO `media` VALUES ('37', null, 'contact', null, null, '2015-12-30 18:31:57');
INSERT INTO `media` VALUES ('38', null, 'contact', null, null, '2015-12-30 18:31:57');
INSERT INTO `media` VALUES ('39', null, 'contact', null, null, '2015-12-30 18:31:57');
INSERT INTO `media` VALUES ('40', null, 'contact', null, null, '2015-12-30 18:33:44');
INSERT INTO `media` VALUES ('41', null, 'contact', null, null, '2015-12-30 18:33:44');
INSERT INTO `media` VALUES ('42', null, 'contact', null, null, '2015-12-30 18:33:44');
INSERT INTO `media` VALUES ('43', null, 'contact', null, null, '2015-12-30 18:33:44');
INSERT INTO `media` VALUES ('44', null, 'contact', null, null, '2015-12-30 18:33:44');
INSERT INTO `media` VALUES ('45', null, 'contact', null, null, '2015-12-30 18:33:44');
INSERT INTO `media` VALUES ('46', null, 'contact', null, null, '2015-12-30 18:34:38');
INSERT INTO `media` VALUES ('47', null, 'contact', null, null, '2015-12-30 18:34:38');
INSERT INTO `media` VALUES ('48', null, 'contact', null, null, '2015-12-30 18:34:38');
INSERT INTO `media` VALUES ('49', null, 'contact', null, null, '2015-12-30 18:34:38');
INSERT INTO `media` VALUES ('50', null, 'contact', null, null, '2015-12-30 18:34:38');
INSERT INTO `media` VALUES ('51', null, 'contact', null, null, '2015-12-30 18:41:45');
INSERT INTO `media` VALUES ('52', null, 'contact', null, null, '2015-12-30 18:41:45');
INSERT INTO `media` VALUES ('53', null, 'contact', null, null, '2015-12-30 18:41:45');
INSERT INTO `media` VALUES ('54', null, 'contact', null, null, '2015-12-30 18:41:45');
INSERT INTO `media` VALUES ('55', null, 'contact', null, null, '2015-12-30 18:41:45');
INSERT INTO `media` VALUES ('56', null, 'contact', null, null, '2015-12-30 18:43:07');
INSERT INTO `media` VALUES ('57', null, 'contact', null, null, '2015-12-30 18:43:08');
INSERT INTO `media` VALUES ('58', null, 'contact', null, null, '2015-12-30 18:43:08');
INSERT INTO `media` VALUES ('59', null, 'contact', null, null, '2015-12-30 18:43:08');
INSERT INTO `media` VALUES ('60', null, 'contact', null, null, '2015-12-30 18:43:08');
INSERT INTO `media` VALUES ('61', null, 'contact', '84', null, '2015-12-31 12:02:23');
INSERT INTO `media` VALUES ('62', null, 'contact', '84', null, '2015-12-31 12:02:35');

-- ----------------------------
-- Table structure for `media_item`
-- ----------------------------
DROP TABLE IF EXISTS `media_item`;
CREATE TABLE `media_item` (
  `id_media_item` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `media_type` varchar(255) DEFAULT NULL,
  `media_source` varchar(255) DEFAULT NULL,
  `media_comments` varchar(255) DEFAULT NULL,
  `is_primary_media` tinyint(4) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `original_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_media_item`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of media_item
-- ----------------------------
INSERT INTO `media_item` VALUES ('1', '2', '2', 'image/jpeg', 'Chrysanthemum_1451460659.jpg', 'id proof', '0', '2015-12-30 13:01:00', 'Chrysanthemum.jpg');
INSERT INTO `media_item` VALUES ('2', '2', '2', 'image/jpeg', 'Hydrangeas_1451460659.jpg', 'id proof', '0', '2015-12-30 13:01:01', 'Hydrangeas.jpg');
INSERT INTO `media_item` VALUES ('3', '2', '2', 'image/jpeg', 'Lighthouse_1451460659.jpg', 'id proof', '0', '2015-12-30 13:01:01', 'Lighthouse.jpg');
INSERT INTO `media_item` VALUES ('4', '2', '2', 'image/jpeg', 'Tulips_1451460659.jpg', 'id proof', '0', '2015-12-30 13:01:01', 'Tulips.jpg');
INSERT INTO `media_item` VALUES ('5', '3', '2', 'jpg', 'Chrysanthemum_1451475766.jpg', null, '0', '2015-12-30 17:12:50', null);
INSERT INTO `media_item` VALUES ('6', '3', '2', 'jpg', 'Desert_1451475766.jpg', null, '0', '2015-12-30 17:12:51', null);
INSERT INTO `media_item` VALUES ('7', '4', '2', 'jpg', 'Desert_1451475996.jpg', null, '0', '2015-12-30 17:16:41', null);
INSERT INTO `media_item` VALUES ('8', '4', '2', 'jpg', 'flow2_1451475996.jpg', null, '0', '2015-12-30 17:16:41', null);
INSERT INTO `media_item` VALUES ('9', '5', '2', 'jpg', 'Chrysanthemum_1451476151.jpg', null, '0', '2015-12-30 17:19:15', null);
INSERT INTO `media_item` VALUES ('10', '5', '2', 'jpg', 'Desert_1451476151.jpg', null, '0', '2015-12-30 17:19:15', null);
INSERT INTO `media_item` VALUES ('11', '6', '2', 'jpg', 'Chrysanthemum_1451476281.jpg', null, '0', '2015-12-30 17:21:26', null);
INSERT INTO `media_item` VALUES ('12', '6', '2', 'jpg', 'Desert_1451476281.jpg', null, '0', '2015-12-30 17:21:26', null);
INSERT INTO `media_item` VALUES ('13', '7', '2', 'jpg', 'Chrysanthemum_1451477164.jpg', null, '0', '2015-12-30 17:36:08', null);
INSERT INTO `media_item` VALUES ('14', '7', '2', 'jpg', 'Desert_1451477164.jpg', null, '0', '2015-12-30 17:36:08', null);
INSERT INTO `media_item` VALUES ('15', '8', '2', 'jpg', 'Chrysanthemum_1451477228.jpg', null, '0', '2015-12-30 17:37:12', null);
INSERT INTO `media_item` VALUES ('16', '9', '2', 'jpg', 'Chrysanthemum_1451477502.jpg', null, '0', '2015-12-30 17:41:47', null);
INSERT INTO `media_item` VALUES ('17', '10', '2', 'jpg', 'Chrysanthemum_1451477525.jpg', null, '0', '2015-12-30 17:42:10', null);
INSERT INTO `media_item` VALUES ('18', '11', '2', 'jpg', 'Desert_1451477894.jpg', null, '0', '2015-12-30 17:48:19', null);
INSERT INTO `media_item` VALUES ('19', '12', '2', 'jpg', 'Desert_1451478086.jpg', null, '0', '2015-12-30 17:51:30', null);
INSERT INTO `media_item` VALUES ('20', '13', '2', 'jpg', 'Desert_1451478092.jpg', null, '0', '2015-12-30 17:51:37', null);
INSERT INTO `media_item` VALUES ('21', '14', '2', 'jpg', 'Desert_1451478297.jpg', null, '0', '2015-12-30 17:55:01', null);
INSERT INTO `media_item` VALUES ('22', '15', '2', 'jpg', 'Desert_1451479842.jpg', null, '0', '2015-12-30 18:20:46', null);
INSERT INTO `media_item` VALUES ('23', '16', '2', 'jpg', 'Desert_1451479855.jpg', null, '0', '2015-12-30 18:20:59', null);

-- ----------------------------
-- Table structure for `oauth_access_token_scopes`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_token_scopes`;
CREATE TABLE `oauth_access_token_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `access_token_id` int(11) NOT NULL,
  `scope_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `oauth_access_token_scopes_access_token_id_index` (`access_token_id`),
  KEY `oauth_access_token_scopes_scope_id_index` (`scope_id`),
  CONSTRAINT `oauth_access_token_scopes_ibfk_1` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`),
  CONSTRAINT `oauth_access_token_scopes_ibfk_2` FOREIGN KEY (`access_token_id`) REFERENCES `oauth_access_tokens` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of oauth_access_token_scopes
-- ----------------------------

-- ----------------------------
-- Table structure for `oauth_access_tokens`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE `oauth_access_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_token` text COLLATE utf8_unicode_ci,
  `session_id` int(10) unsigned NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_access_tokens_id_session_id_unique` (`id`,`session_id`),
  KEY `oauth_access_tokens_session_id_index` (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of oauth_access_tokens
-- ----------------------------
INSERT INTO `oauth_access_tokens` VALUES ('1', 'Xpl8woxtykQTiSJCRZHxMQlgSUZKA4B9j7Ldftcl', '12', '1451370594', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('2', 'lrYSymqvC7boPJRBphotjAkABg33htpbaHD8QBJz', '13', '1451371744', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('3', '625FJPSOpJAnUnK7EI3sMGWkFVmdE4jkdMSFVUzc', '14', '1451374976', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('4', 'UeBVeECElaZdK5Ycj0LjdSTNaE9BnjndkDaLCEsF', '15', '1451375187', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('5', 'T2sOwySAKoLLfditcyOYDJQSlzh4Glalfhh8tqkg', '16', '1451375215', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('6', 'PqQrlu0HDOkJZwoLpmTVcBUkcbUPEVHdQwOzaMrZ', '17', '1451375361', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('7', '7QinM2m4GEZNAXPVjXmYJioSlDKY3EJE028Far4e', '18', '1451375397', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('8', 'JApBtrgd6dx9z7Ky6LDlJmk8qEvHbpvnrbcQfJrl', '19', '1451375489', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('9', 'FUqa6mnlw6GJBr3Pqn4WqTbsgOqlmVEnQkpE40Wn', '20', '1451375544', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('10', 'nG8QiCQDG03lsqO2TVvu5PHkJv08ER33r5ABmBpa', '21', '1451375727', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('11', 'a7FWA1VQION2zwBVSkbNoeUoU8QwT0RmVxRITDci', '22', '1451375753', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('12', 'RBWpBEjlEd978NgkK4zHSta4XWYdnpP6mgud4KBS', '23', '1451375832', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('13', 'wXDPQ2YERTrUmfHMXxvNLjBKS7v8YxQw7UO0HVpL', '24', '1451375841', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('14', 'EfrBV7URLHg9R26Ykpd5BIdWTqRKHAlPNwbLkouj', '25', '1451375896', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('15', 'uApnKMZVxI11S37d0WJ0PLPF2xtzre6wnHRPaRj5', '26', '1451375925', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('16', 'dB4MrmQ63ttRa2Ff9SvNhe0wlyyvcLcdRZOBDxx4', '27', '1451375977', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('17', 'vSUAW1Q9fmrOH5BSxEbre7ICB07eRk2WMtjJE8Ic', '28', '1451376094', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('18', 'OUWPSxsbS2LlvyfYoqWNCJf4vhaow1qoZcBI2rbC', '29', '1451376137', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('19', 'p9iM1AN9OkfpcPG9zXCFEBAPEqpn4aT2KmGu1H35', '30', '1451376245', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('20', 'du8r5J78AhBhgntvQBpJveZSHuWkh45SQ4rgdEPD', '31', '1451378027', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('21', 'EvfhCXITbppaaJZe4hp9OhzoTnr1pPt016eiBkjJ', '32', '1451381968', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('22', 'trO7z0tgDKhvgYfMD32ry8F8TFvTx4CViaN06ixe', '33', '1451381971', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('23', 'Ylho2R0UlAgoqcJuTXUSQhkcNEZQJ25EHIv9i2SR', '34', '1451384456', '0000-00-00 00:00:00', '2015-12-29 14:50:53');
INSERT INTO `oauth_access_tokens` VALUES ('24', 'jOOTc3zDG0qOgLwTyCWRde6VTR1RGT1J6121fJM2', '35', '1451389737', '0000-00-00 00:00:00', '2015-12-29 16:18:54');
INSERT INTO `oauth_access_tokens` VALUES ('25', 'R3HZ5Yzf8hhMR02ONcdmznXeGymjesyJJ0ej4I0S', '36', '1451389766', '0000-00-00 00:00:00', '2015-12-29 16:19:24');
INSERT INTO `oauth_access_tokens` VALUES ('26', 'YYEErdi7x8ut2HiSBAijAxR4pnBMoIjUoiJUFlvn', '37', '1451389867', '0000-00-00 00:00:00', '2015-12-29 16:21:05');
INSERT INTO `oauth_access_tokens` VALUES ('27', 'OT0BHYegp6UV67RYcOurd9uGqoEP03SWjK36qTqn', '38', '1451393184', '0000-00-00 00:00:00', '2015-12-29 17:16:22');
INSERT INTO `oauth_access_tokens` VALUES ('28', 'RQM1f2ZrISurkkm4pEjjkDhrZeeuSdcoRE9GsTiH', '39', '1451393343', '0000-00-00 00:00:00', '2015-12-29 17:19:00');
INSERT INTO `oauth_access_tokens` VALUES ('29', 'qIHn6KSXLzcYxz17ACHtbc9gIjCSBkRQu1CbRkGP', '40', '1451399589', '0000-00-00 00:00:00', '2015-12-29 19:03:07');
INSERT INTO `oauth_access_tokens` VALUES ('30', 'PEoNzSrkhaCpSSO96YADl4JEld3Uoz45fE2nWFMJ', '41', '1451399625', '0000-00-00 00:00:00', '2015-12-29 19:03:42');
INSERT INTO `oauth_access_tokens` VALUES ('31', 'fYix2hYP670cKNPZUAoWn49WOD7Ab1fdu5YNyeo3', '42', '1451399645', '0000-00-00 00:00:00', '2015-12-29 19:04:03');
INSERT INTO `oauth_access_tokens` VALUES ('32', 'gKjPPUKKLH68c0upZBe8yjT3F5kahQos2gqaH6s1', '43', '1451399748', '0000-00-00 00:00:00', '2015-12-29 19:05:46');
INSERT INTO `oauth_access_tokens` VALUES ('33', 'Mp8YVv7YyBrnXDjSxD5GhI2K1Zbs4mL0uDF6t1Jt', '44', '1451399771', '0000-00-00 00:00:00', '2015-12-29 19:06:09');
INSERT INTO `oauth_access_tokens` VALUES ('34', '45o5GoHYgL7ahx2TgY9r4SkpxjwByLkt14vefPvn', '45', '1451399789', '0000-00-00 00:00:00', '2015-12-29 19:06:27');
INSERT INTO `oauth_access_tokens` VALUES ('35', 'iBxhkp4ezcktzJ20CG8MytvmSOJ8yrW2fCoqLl0q', '46', '1451399883', '0000-00-00 00:00:00', '2015-12-29 19:08:01');
INSERT INTO `oauth_access_tokens` VALUES ('36', 'YsvCXNGOTqJn8df7wiMSiVtCwxvr2g2BmiWuxR0q', '47', '1451399892', '0000-00-00 00:00:00', '2015-12-29 19:08:10');
INSERT INTO `oauth_access_tokens` VALUES ('37', 'vMGLu7bng4XGb5hZ6iaroYMKam7NUkGohLNDCbzD', '48', '1451399898', '0000-00-00 00:00:00', '2015-12-29 19:08:15');
INSERT INTO `oauth_access_tokens` VALUES ('38', '5rAF53wXBzHR1OL7ZSYOQmheJuQQgeCrJBXpG1k2', '49', '1451399939', '0000-00-00 00:00:00', '2015-12-29 19:08:57');
INSERT INTO `oauth_access_tokens` VALUES ('39', 'p1siNT0nBHRCBSuCxZTvRn7jb2QuxqwNMF0RMalP', '50', '1451400059', '0000-00-00 00:00:00', '2015-12-29 19:10:57');
INSERT INTO `oauth_access_tokens` VALUES ('40', 'opGpQQUZJNbyXFHwAnmbynPYu8i6YbccSMZat9Zp', '51', '1451400066', '0000-00-00 00:00:00', '2015-12-29 19:11:04');
INSERT INTO `oauth_access_tokens` VALUES ('41', 'hNvuzaLrENMsWFUz534FouAJxDMXqoqmhrUWkwA9', '52', '1451400086', '0000-00-00 00:00:00', '2015-12-29 19:11:23');
INSERT INTO `oauth_access_tokens` VALUES ('42', 'doqfx9iR5UTqbvCBG2Fir7Jd7LCrx2nypsNxsm5H', '53', '1451400103', '0000-00-00 00:00:00', '2015-12-29 19:11:40');
INSERT INTO `oauth_access_tokens` VALUES ('43', '6wSz7eMQZun45DDj6vIWMsWihPiudCfClUmm3qnZ', '54', '1451400126', '0000-00-00 00:00:00', '2015-12-29 19:12:04');
INSERT INTO `oauth_access_tokens` VALUES ('44', 'CDWmEV4GEmp1lHfiAho5am8hELaYAngalN6XDFyq', '55', '1451400195', '0000-00-00 00:00:00', '2015-12-29 19:13:12');
INSERT INTO `oauth_access_tokens` VALUES ('45', 'nlmLIArocxxYk4AnhUieesSpadkhUhkATz0SUvqE', '56', '1451400233', '0000-00-00 00:00:00', '2015-12-29 19:13:51');
INSERT INTO `oauth_access_tokens` VALUES ('46', 'xe1hiaH6Zkw3u5pWsSG8HNyNXHxYrA7sSmMajwxg', '57', '1451400293', '0000-00-00 00:00:00', '2015-12-29 19:14:51');
INSERT INTO `oauth_access_tokens` VALUES ('47', 'KY9foZ009fvvPAJRcM1OwMyPg84J4crxw2lYLXZx', '58', '1451400312', '0000-00-00 00:00:00', '2015-12-29 19:15:10');
INSERT INTO `oauth_access_tokens` VALUES ('48', 'IqZDfwvGTpkmL99Lb7w2Uq4swppYtdHoqpZI0haw', '59', '1451400370', '0000-00-00 00:00:00', '2015-12-29 19:16:08');
INSERT INTO `oauth_access_tokens` VALUES ('49', 'BfxqqCFU87nE6qTFCtvxeN2T8alXMy8WJh0ga2IV', '60', '1451400453', '0000-00-00 00:00:00', '2015-12-29 19:17:30');
INSERT INTO `oauth_access_tokens` VALUES ('50', 'CBiBfTgxhKDJrLEK9pfOWjkxuIQoXWYHXVXH7dt7', '61', '1451400455', '0000-00-00 00:00:00', '2015-12-29 19:17:33');
INSERT INTO `oauth_access_tokens` VALUES ('51', 'YjYUrBHUBOgOV4IgEfhZqRcRaP1wExXB71BEOm4W', '62', '1451400455', '0000-00-00 00:00:00', '2015-12-29 19:17:33');
INSERT INTO `oauth_access_tokens` VALUES ('52', '2zDbAnAFlBxaNyvROik5RP329wORIwUMoeYvsSfU', '63', '1451400455', '0000-00-00 00:00:00', '2015-12-29 19:17:33');
INSERT INTO `oauth_access_tokens` VALUES ('53', 'MZjQx0d4ngXadA5qtbXVAO96oAw3CJqVpY65MLz6', '64', '1451400456', '0000-00-00 00:00:00', '2015-12-29 19:17:33');
INSERT INTO `oauth_access_tokens` VALUES ('54', 'YjIOUqqBTuShrzTDSP6hkXi4O2uMYCZURSdlyOyb', '65', '1451400456', '0000-00-00 00:00:00', '2015-12-29 19:17:33');
INSERT INTO `oauth_access_tokens` VALUES ('55', '300Vw6sA0Hr3ZBh7lsw4xJtVx8j0HuXV7N4QPcW8', '66', '1451400456', '0000-00-00 00:00:00', '2015-12-29 19:17:34');
INSERT INTO `oauth_access_tokens` VALUES ('56', 'EPOqfT1PCqHIBl1kopvDGeRvfWoyCb5qc4q7JOl0', '67', '1451400456', '0000-00-00 00:00:00', '2015-12-29 19:17:34');
INSERT INTO `oauth_access_tokens` VALUES ('57', 'Tv1OzDJkTsnd5QftFlQdNIdiiSxNhdorNVMKMIac', '68', '1451400456', '0000-00-00 00:00:00', '2015-12-29 19:17:34');
INSERT INTO `oauth_access_tokens` VALUES ('58', 'B5uGLqQlw3SzXydP3zpN9AuhsRd6ZzpYQglXEZvK', '69', '1451400456', '0000-00-00 00:00:00', '2015-12-29 19:17:34');
INSERT INTO `oauth_access_tokens` VALUES ('59', 'tzulH67hOIEJjYn5Oou2otOW1Y4vCEvrFHru9hY4', '70', '1451400457', '0000-00-00 00:00:00', '2015-12-29 19:17:34');
INSERT INTO `oauth_access_tokens` VALUES ('60', 'NKHILONeVIghvHzF48DxqGOVhmdZ2FnDOezhu4sM', '71', '1451400457', '0000-00-00 00:00:00', '2015-12-29 19:17:34');
INSERT INTO `oauth_access_tokens` VALUES ('61', 'YjLphi19jvgsbkHXEIbOCZOE0p6khhoUdzxO8SWU', '72', '1451400457', '0000-00-00 00:00:00', '2015-12-29 19:17:35');
INSERT INTO `oauth_access_tokens` VALUES ('62', 'DFw5oB7OO2Ym40qf24F9oFl4qLYtAJnJ3nttACuS', '73', '1451400755', '0000-00-00 00:00:00', '2015-12-29 19:22:33');
INSERT INTO `oauth_access_tokens` VALUES ('63', 'A3qYZzE6S1AHLfWrN7azkqzG8Ed1Yk4Q2GUaZnzs', '74', '1451401052', '0000-00-00 00:00:00', '2015-12-29 19:27:30');
INSERT INTO `oauth_access_tokens` VALUES ('64', 'Mfhft3XZt9AkhxOeMyzeIiKQN4ZQ4yo3pUanrR63', '75', '1451401129', '0000-00-00 00:00:00', '2015-12-29 19:28:47');
INSERT INTO `oauth_access_tokens` VALUES ('65', 'e8ScldBmhxAHMBxDCG0fV7XZ5dZF5uRtODJMT1KR', '76', '1451401142', '0000-00-00 00:00:00', '2015-12-29 19:29:00');
INSERT INTO `oauth_access_tokens` VALUES ('66', 'Qf1S5B0P91OLyrpeQbf2BTg6ybqQa1wxkfA6px09', '77', '1451401187', '0000-00-00 00:00:00', '2015-12-29 19:29:45');
INSERT INTO `oauth_access_tokens` VALUES ('67', 'LXjPnUPtvoeeyIxWXmYMoAB3ishB415bNobeuMkL', '78', '1451401208', '0000-00-00 00:00:00', '2015-12-29 19:30:05');
INSERT INTO `oauth_access_tokens` VALUES ('68', 'FkVZYsfE8tWU29Xg5z6IGarWNIPb6DZ1cz1qklxq', '79', '1451401230', '0000-00-00 00:00:00', '2015-12-29 19:30:27');
INSERT INTO `oauth_access_tokens` VALUES ('69', 'kQSvfutCbI1wmpzsYPecHJK0Azuy3lrmQV4vYqMV', '80', '1451401244', '0000-00-00 00:00:00', '2015-12-29 19:30:41');
INSERT INTO `oauth_access_tokens` VALUES ('70', 'YUex65xSkJAsIYnfo2KQ7LHFFFRRiWbzBbibcXVo', '81', '1451401268', '0000-00-00 00:00:00', '2015-12-29 19:31:06');
INSERT INTO `oauth_access_tokens` VALUES ('71', 'BCJxlrHuC904XOybaLxpGsrXuHfluVbNmEMlcXxU', '82', '1451401337', '0000-00-00 00:00:00', '2015-12-29 19:32:14');
INSERT INTO `oauth_access_tokens` VALUES ('72', 'v3gbYLc7D5v1bdjimPGpPL9bKcCnUZvBRh4FtISh', '83', '1451453852', '0000-00-00 00:00:00', '2015-12-30 10:07:27');
INSERT INTO `oauth_access_tokens` VALUES ('73', 'oNijKvmicMF2iliZXPWa207QgCj4nuDnyGWWqDuU', '84', '1451456163', '0000-00-00 00:00:00', '2015-12-30 10:45:59');
INSERT INTO `oauth_access_tokens` VALUES ('74', 'bxnGxZR7dlaDp5x8RWYvk28kQW8rsxNN9rUjXxzk', '85', '1451456175', '0000-00-00 00:00:00', '2015-12-30 10:46:10');
INSERT INTO `oauth_access_tokens` VALUES ('75', 'rhOOPoakum8UyfO7d3qN6BRKjvC2oWjaH7xeKjhX', '86', '1451456246', '0000-00-00 00:00:00', '2015-12-30 10:47:22');
INSERT INTO `oauth_access_tokens` VALUES ('76', 'DwjkXTRvb9haJJT4v1yjQcMu2ELMDLnqvSwSd9df', '87', '1451456256', '0000-00-00 00:00:00', '2015-12-30 10:47:32');
INSERT INTO `oauth_access_tokens` VALUES ('77', 'NnsfPW4myemi9gfhlVcyAIufuwaBmzhkhbxP3oTb', '88', '1451456265', '0000-00-00 00:00:00', '2015-12-30 10:47:41');
INSERT INTO `oauth_access_tokens` VALUES ('78', 'CBYa8aNXztQOHbNi6bV1GyRsuz0BcII84H3DLXBd', '89', '1451456363', '0000-00-00 00:00:00', '2015-12-30 10:49:18');
INSERT INTO `oauth_access_tokens` VALUES ('79', 'WmByqwgDFw20ppZjgaM5cRzwvLg0EWQJQhpCJTOs', '90', '1451462968', '0000-00-00 00:00:00', '2015-12-30 12:39:23');
INSERT INTO `oauth_access_tokens` VALUES ('80', '9MKRTZgp0YPy0AcL4H959tgsub1QONHVWYTjOXbk', '91', '1451463100', '0000-00-00 00:00:00', '2015-12-30 12:41:35');
INSERT INTO `oauth_access_tokens` VALUES ('81', 'HwFk49SNLmZblGvuObeZJWcDFoBYLkYaWwiSsJFC', '92', '1451463127', '0000-00-00 00:00:00', '2015-12-30 12:42:03');
INSERT INTO `oauth_access_tokens` VALUES ('82', 'royD9hcc0QPxLFgWZ63zEnjnQlvD9P8lOdo4hliE', '93', '1451463323', '0000-00-00 00:00:00', '2015-12-30 12:45:19');
INSERT INTO `oauth_access_tokens` VALUES ('83', '85OvhNnnmzfqCA5OdugJAJR318h9V1MuDLBA5jGy', '94', '1451463341', '0000-00-00 00:00:00', '2015-12-30 12:45:37');
INSERT INTO `oauth_access_tokens` VALUES ('84', 'qtz0oM7aUFeNwgWImrvrg4pAJoqzHMqI0eMd86aZ', '95', '1451463948', '0000-00-00 00:00:00', '2015-12-30 12:55:44');
INSERT INTO `oauth_access_tokens` VALUES ('85', '4mYaCsPOxnKSXNGpRvKpGjbsorCc0REyLTbm7GOj', '96', '1451470937', '0000-00-00 00:00:00', '2015-12-30 14:52:20');
INSERT INTO `oauth_access_tokens` VALUES ('86', 'IgjgFl3Ms0iMxhglk3tMBn3SzNaWx6mSdPcGsES6', '97', '1451471814', '0000-00-00 00:00:00', '2015-12-30 15:06:56');
INSERT INTO `oauth_access_tokens` VALUES ('87', 'a2atdVCZkP40kcHdpRXYyoRBv69wpfur6ikRnAoI', '98', '1451472068', '0000-00-00 00:00:00', '2015-12-30 15:11:10');
INSERT INTO `oauth_access_tokens` VALUES ('88', 'VKjLRUJhyBKzYMAI560vpCrifD9e8JyKzSiPORYi', '99', '1451472191', '0000-00-00 00:00:00', '2015-12-30 15:13:14');
INSERT INTO `oauth_access_tokens` VALUES ('89', 'BYC3Iml76kkmEnqLfNqeFEFUvkbiGXg4MzDrOnRk', '100', '1451473313', '0000-00-00 00:00:00', '2015-12-30 15:31:56');
INSERT INTO `oauth_access_tokens` VALUES ('90', 'hdLE38Ts7pjFmE7OB1vWeYzB5CfCHCgi1vpeNo9V', '101', '1451473803', '0000-00-00 00:00:00', '2015-12-30 15:40:05');
INSERT INTO `oauth_access_tokens` VALUES ('91', 'H20Bbavh3hjNxtRq4TlqQM3DD3LYm7moRLQnKQue', '102', '1451473819', '0000-00-00 00:00:00', '2015-12-30 15:40:21');
INSERT INTO `oauth_access_tokens` VALUES ('92', 'Xu4fXUA4TuOghon1T2YxBFxN7xKAeVNz9SmMgSrw', '103', '1451476225', '0000-00-00 00:00:00', '2015-12-30 16:20:21');
INSERT INTO `oauth_access_tokens` VALUES ('93', 'QJoIi52zKIbABiNtlS5U5kCeHUiPW3c3bJyRheFL', '104', '1451477455', '0000-00-00 00:00:00', '2015-12-30 16:40:58');
INSERT INTO `oauth_access_tokens` VALUES ('94', 'HEDkwnlUqxofnVOiOUZVTZCNZpEjNP3iV4RrEj2p', '105', '1451562596', '0000-00-00 00:00:00', '2015-12-30 17:19:52');
INSERT INTO `oauth_access_tokens` VALUES ('95', 'OJeKPizG6uOoUWE6wm8wQHCgaYfCBtae8Ax7Ei00', '106', '1451483846', '2015-12-30 18:27:29', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('96', '3jNLhuPZOqGgNw3CxGKx6Su0gmomOUU2qZVNccod', '107', '1451486708', '2015-12-30 19:15:10', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('97', 'VmXLCP2uPuE6EQuUzI47kOwxEvRwKpHEppZlvKdh', '108', '1451622555', '2015-12-31 09:59:08', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('98', 'Ugzlwqrea99nGJHD0zyrAjsNXTEZDApTkpCjs1nX', '109', '1451540784', '2015-12-31 10:16:27', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('99', 'HeSJiB1v23JgAMbepOaNaynHOu3jnx6o6k16tJGC', '110', '1451624872', '2015-12-31 10:37:55', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('100', '3rgYJvf65oYNjBpk5aVsgMrgpgWPOUEOMjqyBKQl', '111', '1451624939', '2015-12-31 10:39:03', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('101', 'fp32EqvDV2IixZb77DqZCY9YkgrQwRmT7bbHD8BM', '112', '1451628193', '2015-12-31 11:33:07', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('102', 'poVTIowE2Lbd3V3S65SHCC0BiyCeiTuK5K7VXq3a', '113', '1451628432', '2015-12-31 11:37:06', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('103', '2MIi8BL0cVwcVHErVgI6JwaSAgx5rnz7fHm5hPmL', '114', '1451658140', '2015-12-31 19:52:24', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('104', 'teXdpyQnn5yrPT3eHjLNyDXIfS1oiQcg1awFEy6d', '115', '1451658198', '2015-12-31 19:53:22', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('105', 'Ef2t2Z2sJatVqqClTCdfDFjQJQgtDWc9xaffJ58O', '116', '1451970229', '2016-01-04 10:33:33', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('106', 'F9oHCOqW6UWIunmy3dQRNQK0lAbKncg06vG1HL0o', '117', '1451971448', '2016-01-04 10:54:08', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('107', 'GfTPF92H5aXdzeV3YBJlUzy0XJNCh4WHIet3O3E2', '118', '1451971609', '2016-01-04 10:56:49', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('108', '2slya9jIVQBCByhUj5Wuiv2Rv3IbYsSAtyWgj3lK', '119', '1451971651', '2016-01-04 10:57:31', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('109', 'i7aIrO6DMmecCKsc15BdPoP0B938ypl3DX98y6x0', '120', '1451971668', '2016-01-04 10:57:48', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('110', 'FFOpFtbBozsYZTtRZCf3BFrzxmswjqYGYcv1jYGg', '121', '1451975080', '2016-01-04 11:54:24', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('111', 'bZpF7sCKQlzaDuqK6pth6JUA54tKCNVR9eiVhzbH', '122', '1451977447', '2016-01-04 12:34:06', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('112', 'AVb7KHhvAnPjyyvorn5ATTDH7cBIrKPdf3NhIvBr', '123', '1451979249', '2016-01-04 13:03:53', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('113', '9Qaz3NhbXTPTuZMx6qjXHqamBImbKxpr91RynHoH', '124', '1451984471', '2016-01-04 14:31:10', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('114', 'V8jvwDkzzBlNnyKdccKg5MD21YO83pPDkRpADymd', '125', '1451986470', '2016-01-04 15:04:30', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('115', 'CeJ8lgugr9AuJvZRMKvTNpXRtEnYaLcDv7UtMvDB', '126', '1451987079', '2016-01-04 15:14:39', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('116', 'Iu1W0EGqX59ys55zTSQqFr651VNRjiYB48t2NV1E', '127', '1451988377', '2016-01-04 15:36:14', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('117', 'TJDds9rAHk9ncBvhgtwCYIS8EEJr6BoNmWyP2pri', '128', '1451988487', '2016-01-04 15:38:05', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('118', '74tgrVplOc5a84KcLLX5NyavY8GPwfP8eIUzAomX', '129', '1451992821', '2016-01-04 16:50:21', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('119', 'lDSyv2fbNLz4ZLJWoH8Jd5nEbsuhISuO1Zjv9xol', '130', '1451999518', '2016-01-04 18:41:58', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('120', 'vfOV8D4m8ZmZrXg1UV6rxcxM9VlpN6zo1RyG5y6x', '131', '1452000029', '2016-01-04 18:50:29', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('121', 'pxesOM1WJuhT8sM6BBmcWj4FhT1OxiETjeKADxgt', '132', '1452002747', '2016-01-04 19:35:47', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('122', 'ahFcSKL7wncautyifHXMkOAbnuRBxhkzInUOJvzr', '133', '1452053923', '2016-01-05 09:48:40', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('123', 'FEXQC11wKwSfjT2qlWH1pAkTkEtmVGTenYTxdOnb', '134', '1452054760', '2016-01-05 10:02:42', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('124', 'Ydlkr8Ni9oTzsokREzWIHRmVIQhlYkzJ6Cf5wGpy', '135', '1452055677', '2016-01-05 10:17:58', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('125', 'C4AOWwyYkPSHuyv4r6mIDA9cdWiKoDpJVZ2sLQB5', '136', '1452055757', '2016-01-05 10:19:19', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('126', 'YQTVTcHUNN03AQCtnB48bj9hSw0cKQSuvN4IaqL9', '137', '1452064868', '2016-01-05 12:51:10', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('127', 'GvRZOS9Il8YjZRBqgxo3IQeVw3DP5HjgERo2YL2i', '138', '1452074606', '2016-01-05 15:33:28', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('128', 'kyl8ltJltoc3QAm6F9BDQEe8cnk3Uv4OfGJRcc81', '139', '1452075500', '2016-01-05 15:48:23', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('129', '4l571xDebSRRBgEE5YeBn7YIAumbRype03Wpe5rS', '140', '1452077920', '2016-01-05 16:28:37', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('130', 'SpGQzHPLeFGMhcm9p7EpjuogrpuOnzTfNOzxnmbu', '141', '1452078181', '2016-01-05 16:32:59', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('131', 'cB4xqYhrNfwM0fJSxW7C8hCGlVW7m2PfYrzKVtin', '142', '1452078358', '2016-01-05 16:36:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('132', 'eti7zeI1DOU6IUr1Bw6srP9tTYZMu8QPburAeeR3', '143', '1452078431', '2016-01-05 16:37:12', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('133', 'xyrTDC3RsxFswrPveIiOo4ANI7L4JFq7DYoOcuEU', '144', '1452081877', '2016-01-05 17:34:38', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('134', 'LJRNSs1y8oMBpkiCxpePmibNEpPTPlTayxER5MHE', '145', '1452086556', '2016-01-05 18:52:34', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('135', 'kf47ycOdDaxdpyoZbAiUxYCwM6T4FgIHYsA9y01H', '146', '1452145495', '2016-01-06 11:14:57', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('136', 'y0zrhNi0nnO8bcIle7FqrXoq52sW2AddNPo3l9TU', '147', '1452160756', '2016-01-06 15:29:12', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('137', '4yjbXoIfG9rrc8pK59cntYSb3KZyplffEXD2Ta6E', '148', '1452160780', '2016-01-06 15:29:36', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('138', 'uC70we6Zm4QVgdJ3zRQTqjUh6FGhDwJFvM6kkgqQ', '149', '1452162205', '2016-01-06 15:53:21', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('139', '3DIGYQqoDjn2ftMQxkIqraAQYDpITrmiPgYZ4A2P', '150', '1452167214', '2016-01-06 17:16:50', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('140', 'P1rfW9ZdK6h5yeFyhZzjzq2oIotCXi26IxXWzgfH', '151', '1452323428', '2016-01-08 12:40:20', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('141', '6jQl7lBNdkZMnTUaRvoQFdFynv2UlaQE1xalS4to', '152', '1452324862', '2016-01-08 13:04:22', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('142', '9VM1ikjDbfg0PDM6hY2IiejPcKPSfhhdWgkoPtmM', '153', '1452324880', '2016-01-08 13:04:41', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('143', 'vthgzDBQy4WjvjbAeb3AftU8LzSwrWm7aL5Pb0NE', '154', '1452325623', '2016-01-08 13:17:04', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('144', 'zVzPat2UbcKMoJu2Ewon0sstB6sIo70t83JrjCdw', '155', '1452325697', '2016-01-08 13:18:18', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('145', 'oA2ioSbt253jRQHCgEDmObLIEFNawgX7JvlKfDFL', '156', '1452326654', '2016-01-08 13:34:15', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('146', 'nOcAgdscdEC7nljWl7DIunr7jqUBf7hP1jdCsc1a', '157', '1452327091', '2016-01-08 13:41:32', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('147', 'ndksiF7BWC4BU3R3m5ly0HEqLqefIJvq4kMLQpc7', '158', '1452327228', '2016-01-08 13:43:40', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('148', 'ACCC16HLpl1mFv39AXxEVxQB20mKTPT1MH3aL239', '159', '1452327988', '2016-01-08 13:56:20', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('149', 'B2V88zI9HejzznACZRBKBCH6Iya3HJHVUPBIDSJF', '160', '1452329655', '2016-01-08 14:24:07', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('150', 'qXGNhukC6xDUPERXyXNJsSoM25vLTTCcnvVoZPXi', '161', '1452330143', '2016-01-08 14:32:15', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('151', 'X0ObLGNjEUyVkut6UDA3tyaubGRDEAcCLgVKJlbQ', '162', '1452331193', '2016-01-08 14:50:01', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('152', 'pxWYoONH4Tv3HxbFFi4K2hPLjXWdFuomsadAtN9H', '163', '1452332125', '2016-01-08 15:05:17', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('153', 'VM3x3qz0NsX8VAQDDuL37fsoH61LG7TWYfrQKIVc', '164', '1452333118', '2016-01-08 15:22:06', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('154', 'Y2UodNH3UbAY8Cc9os6hrKUvX3cjaKaY6VvTmp1p', '165', '1452333126', '2016-01-08 15:22:14', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('155', 'io83dV6UJ1IG2snSQaLm4ezgMI6GbXA5NJz2pwB7', '166', '1452333183', '2016-01-08 15:22:55', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('156', 'a5t1AnLpzvps0moaAZl5Fj78Z2X3ENLitSKnEqh7', '167', '1452333300', '2016-01-08 15:24:52', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('157', 'YD4Bgnm64na3mDvW4MmkgRPL9yDv2TE5eOKPH20m', '168', '1452333727', '2016-01-08 15:31:59', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('158', 'WbjUGjSZcpT57LOOW04mVr7ptbESGWz8PYvtX8iA', '169', '1452333975', '2016-01-08 15:36:19', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('159', 'RxRdmW1cBbZuhyGeSH2ImWFgZAEidwud2xBTfdmo', '170', '1452334838', '2016-01-08 15:50:45', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('160', 'ZVkAWAeV9gg7eGXKWYd6C0beVaxPehVHs21tE5oA', '171', '1452335038', '2016-01-08 15:53:50', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('161', 'odJw22GHCRhGU327FY83oPZGZ1c6iAak5vHdfGWb', '172', '1452335071', '2016-01-08 15:54:23', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('162', 'Z3TgyJRb511kLtlcmbAQVS5t8pGi6940qTdsBBDO', '173', '1452335112', '2016-01-08 15:55:04', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('163', 'mg40lhOHTFxeopWIf7wPEQW3NhGTAmlv7h8XCyuc', '174', '1452335127', '2016-01-08 15:55:19', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('164', 'FmuV5RKc3vT9AtyWNlaf7sE7AVfHtxjOLNiPOd5S', '175', '1452336682', '2016-01-08 16:21:14', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('165', 'bgczczEGAguY0i2p8PQJGj9e4jxH5uwvWZ3CAkTW', '176', '1452338201', '2016-01-08 16:46:49', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('166', 'knfukXUYKbKXErhI7TK5bJYDQNVC9VqrKbFuUpvn', '177', '1452339146', '2016-01-08 17:02:35', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('167', 'AYcAkD2UBikJJhwSSw85xgJ18SOBu08fiAfNNW5l', '178', '1452339768', '2016-01-08 17:12:40', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('168', '9fkgSolldk4zxzyCtPLqrcXU1Sizp8ckeurIl15O', '179', '1452340403', '2016-01-08 17:23:24', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('169', 'D6ptVyyFClOBpD3TgkhkEh64hqgyEUFwxoI2ZmSk', '180', '1452340875', '2016-01-08 17:31:16', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('170', '0ij3k2p4TKC32HWtBDw2rUHFWRH1lVNqVoNOqZ3o', '181', '1452342455', '2016-01-08 17:57:36', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('171', 'OJane9GIysdBzuIPYZJ1J8Uu1mP6zWQHYpmpD98d', '182', '1452343222', '2016-01-08 18:10:24', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('172', '0WLW5GEP5fDQBKTfrilZ0yLSg4vX16JoQSV6icfG', '183', '1452344231', '2016-01-08 18:27:03', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('173', 'o2CyoBlUxVY8imKYvVhy1hAH1N1o9kSi29uQHzgY', '184', '1452344544', '2016-01-08 18:32:26', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('174', 'UFdyMj9RJO2d21ovRcoUMzy8TOA9x6kbZ7BFuvAT', '185', '1452348168', '2016-01-08 19:32:39', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('175', '1E4X664x0KMM0pdIaHPNMGxOBm5wNuY48b8RVo2w', '186', '1452348168', '2016-01-08 19:32:40', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('176', 'oPU7yas4glR8MwEtNEJi4TStFB03uomRTSrHRJiz', '187', '1452398718', '2016-01-09 09:35:21', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('177', '39kktAhZyk0zlUdRO6jhvZCG9uL1UUqZTWspgRMk', '188', '1452400082', '2016-01-09 09:57:52', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('178', '6V2xyDP4Fo7gLrqJ6XnlzGk8VEexyyYOGA5bpAJ9', '189', '1452400725', '2016-01-09 10:08:54', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('179', 'K3R0JnhgvlRCZcNCgtdsB6Gva4CZktKFhPi4zc3Y', '190', '1452401231', '2016-01-09 10:17:20', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('180', '16QOtGoNx46GkSxmDgzFyrsmwCvEbHSKLIpFNmWJ', '191', '1452403496', '2016-01-09 10:54:56', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('181', 'RqEDU4RbQOGJuz6as4w5hB7jiUAjVQaCEDRuJ8jn', '192', '1452404905', '2016-01-09 11:18:34', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('182', '3iL2GFPcu3e6TyIFBtDikuIYmOUFpobJI6xXjA88', '193', '1452405890', '2016-01-09 11:34:58', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('183', 'TqcZ2hJ6Xt8cMghUP92hf7dPIGuu8g37wxYQ2Npd', '194', '1452406026', '2016-01-09 11:37:15', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('184', 'AzoPSd0meESAhDd11DokF5UoRReDv655B4ZBAZ5L', '195', '1452406152', '2016-01-09 11:39:21', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('185', 'ySoyC7Q3R6U7eVDV3Q5nx6b2T3lCGaZkLaHtLY0E', '196', '1452406262', '2016-01-09 11:41:11', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('186', 'F04P26hwTDt1csPnwkLlQ5pbvmmyUIZY4clpY8NZ', '197', '1452406341', '2016-01-09 11:42:30', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('187', '8zgb5kiZ6FfOfm9sIJ8FUjsmqmdNxsTfkx0VbJ81', '198', '1452406620', '2016-01-09 11:47:08', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('188', 'xPGiVWAoVtDcpLiTBQF34lCWkvWpbopsnqQ8hoev', '199', '1452407033', '2016-01-09 11:54:02', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('189', 'p45HLxtz7opSqPPwz5UIsJJau2YIzMzlG2ImouwV', '200', '1452407496', '2016-01-09 12:01:45', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('190', 'y9tF7bq7S9bwymbJNIWnZoLgWBK2yOf4mrj1yGqj', '201', '1452408474', '2016-01-09 12:18:03', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('191', 'FH8EfYcxonOSfe2RIKKGuaJr6QUSSAxEcuTfbuBz', '202', '1452409686', '2016-01-09 12:38:06', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('192', 'lJLrmkTBLzdT8mQPudrrkivT5BVVB0Sz3pxUYE9c', '203', '1452409803', '2016-01-09 12:40:12', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('193', 'Ciq6NPh1y8M6MCJLpnacGaTnw3VkS7t2B71qf4Xo', '204', '1452409876', '2016-01-09 12:41:16', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('194', 'qniWX2OhuD0Bsg9WAIkfkD5apRfuEgn1UC8kFh2v', '205', '1452410369', '2016-01-09 12:49:29', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('195', 'yRYIGNoeKrIa1pCoIwaWDEYSqu2e8wmE5qs97wmW', '206', '1452410430', '2016-01-09 12:50:39', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('196', 'lR5mANTIEkuVbRytgmBKbDMfWchPYCEPqOaro4rx', '207', '1452410520', '2016-01-09 12:52:09', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('197', 'olIY3juLcQAZbDg2OwZahHGxEF1BQOr1NDYpoM2C', '208', '1452410930', '2016-01-09 12:58:59', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('198', 'f1g7V6Ryxq39knL2oF1Kdj2IO2iqzUmqxEsQ6nFt', '209', '1452411021', '2016-01-09 13:00:30', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('199', 'h1ZBKhFt9vgzy0dSqaKhj4KHAL6H3SHKNOBmHyMn', '210', '1452411030', '2016-01-09 13:00:30', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('200', 'xXbnIyGqiBNYetOCLXyEXYg9y2gQgYWRInaGoD2n', '211', '1452411105', '2016-01-09 13:01:54', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('201', 'pTVdxX1Rz55x80UTTOgR7gp3vPEvtG9aO1PEc5Bn', '212', '1452411151', '2016-01-09 13:02:32', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('202', 'sX3n12KeG6aOPwyNsMhWdtBLJuH5XhTHWqyEOWJL', '213', '1452411654', '2016-01-09 13:11:03', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('203', 'c7xO9OzG3ciDIFQeOM0CHUdbeRycVtevEHZQ1jO6', '214', '1452420410', '2016-01-09 15:36:59', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('204', 'pDh0Ta1K0jntzN9hOai6MQJbOeLZxDjEQk3e6A8Y', '215', '1452420866', '2016-01-09 15:44:27', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('205', 'QJeV5EQ8NHwRKsMuHfXkjA3q4eCD87y3Nsjo3Wpz', '216', '1452427943', '2016-01-09 17:42:13', '0000-00-00 00:00:00');
INSERT INTO `oauth_access_tokens` VALUES ('206', '7pzVxM5Ti4EvPUbZeHyF6upuvQTHpWoAWwHQCUNW', '217', '1452431781', '2016-01-09 18:46:11', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `oauth_auth_code_scopes`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_auth_code_scopes`;
CREATE TABLE `oauth_auth_code_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `auth_code_id` int(11) NOT NULL,
  `scope_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_code_scopes_auth_code_id_index` (`auth_code_id`),
  KEY `oauth_auth_code_scopes_scope_id_index` (`scope_id`),
  CONSTRAINT `oauth_auth_code_scopes_ibfk_1` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`),
  CONSTRAINT `oauth_auth_code_scopes_ibfk_2` FOREIGN KEY (`auth_code_id`) REFERENCES `oauth_auth_codes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of oauth_auth_code_scopes
-- ----------------------------

-- ----------------------------
-- Table structure for `oauth_auth_codes`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE `oauth_auth_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(10) unsigned NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_session_id_index` (`session_id`),
  CONSTRAINT `oauth_auth_codes_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of oauth_auth_codes
-- ----------------------------

-- ----------------------------
-- Table structure for `oauth_client_endpoints`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_endpoints`;
CREATE TABLE `oauth_client_endpoints` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_client_endpoints_client_id_redirect_uri_unique` (`client_id`,`redirect_uri`),
  CONSTRAINT `oauth_client_endpoints_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of oauth_client_endpoints
-- ----------------------------

-- ----------------------------
-- Table structure for `oauth_client_grants`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_grants`;
CREATE TABLE `oauth_client_grants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `grant_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `oauth_client_grants_client_id_index` (`client_id`),
  KEY `oauth_client_grants_grant_id_index` (`grant_id`),
  CONSTRAINT `oauth_client_grants_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`),
  CONSTRAINT `oauth_client_grants_ibfk_2` FOREIGN KEY (`grant_id`) REFERENCES `oauth_grants` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of oauth_client_grants
-- ----------------------------

-- ----------------------------
-- Table structure for `oauth_client_scopes`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_scopes`;
CREATE TABLE `oauth_client_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `scope_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `oauth_client_scopes_client_id_index` (`client_id`),
  KEY `oauth_client_scopes_scope_id_index` (`scope_id`),
  CONSTRAINT `oauth_client_scopes_ibfk_1` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`),
  CONSTRAINT `oauth_client_scopes_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of oauth_client_scopes
-- ----------------------------

-- ----------------------------
-- Table structure for `oauth_clients`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `secret` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_clients_id_secret_unique` (`id`,`secret`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of oauth_clients
-- ----------------------------
INSERT INTO `oauth_clients` VALUES ('1', '0', '123456', 'BASHA', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_clients` VALUES ('2', '2', '4a5419c0e7a719a31dbb', 'rakesh kumar', '0000-00-00 00:00:00', '2015-12-29 17:19:00');
INSERT INTO `oauth_clients` VALUES ('3', '1', '24337ce8a206c54ddd99', 'super admin', '0000-00-00 00:00:00', '2015-12-30 15:40:21');

-- ----------------------------
-- Table structure for `oauth_grant_scopes`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_grant_scopes`;
CREATE TABLE `oauth_grant_scopes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grant_id` int(11) NOT NULL,
  `scope_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `oauth_grant_scopes_grant_id_index` (`grant_id`),
  KEY `oauth_grant_scopes_scope_id_index` (`scope_id`),
  CONSTRAINT `oauth_grant_scopes_ibfk_1` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`),
  CONSTRAINT `oauth_grant_scopes_ibfk_2` FOREIGN KEY (`grant_id`) REFERENCES `oauth_grants` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of oauth_grant_scopes
-- ----------------------------

-- ----------------------------
-- Table structure for `oauth_grants`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_grants`;
CREATE TABLE `oauth_grants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of oauth_grants
-- ----------------------------

-- ----------------------------
-- Table structure for `oauth_refresh_tokens`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE `oauth_refresh_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_token_id` int(11) NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_refresh_tokens_id_unique` (`id`),
  KEY `access_token_id` (`access_token_id`),
  CONSTRAINT `oauth_refresh_tokens_ibfk_1` FOREIGN KEY (`access_token_id`) REFERENCES `oauth_access_tokens` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of oauth_refresh_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for `oauth_scopes`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_scopes`;
CREATE TABLE `oauth_scopes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of oauth_scopes
-- ----------------------------

-- ----------------------------
-- Table structure for `oauth_session_scopes`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_session_scopes`;
CREATE TABLE `oauth_session_scopes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(10) unsigned NOT NULL,
  `scope_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `oauth_session_scopes_session_id_index` (`session_id`),
  KEY `oauth_session_scopes_scope_id_index` (`scope_id`),
  CONSTRAINT `oauth_session_scopes_ibfk_1` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`),
  CONSTRAINT `oauth_session_scopes_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of oauth_session_scopes
-- ----------------------------

-- ----------------------------
-- Table structure for `oauth_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `oauth_sessions`;
CREATE TABLE `oauth_sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `owner_type` enum('client','user') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `owner_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_redirect_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_browser` text COLLATE utf8_unicode_ci,
  `client_remote_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `oauth_sessions_client_id_owner_type_owner_id_index` (`client_id`,`owner_type`,`owner_id`),
  CONSTRAINT `oauth_sessions_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of oauth_sessions
-- ----------------------------
INSERT INTO `oauth_sessions` VALUES ('1', '1', 'client', '1', null, null, null, '2015-12-31 11:37:06', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('2', '1', 'client', '1', null, null, null, '2015-12-31 19:52:24', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('3', '1', 'client', '1', null, null, null, '2015-12-31 19:53:22', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('4', '1', 'client', '1', null, null, null, '2016-01-04 10:33:33', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('5', '1', 'client', '1', null, null, null, '2016-01-04 10:54:08', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('6', '1', 'client', '1', null, null, null, '2016-01-04 10:56:49', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('7', '1', 'client', '1', null, null, null, '2016-01-04 10:57:31', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('8', '1', 'client', '1', null, null, null, '2016-01-04 10:57:48', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('9', '1', 'client', '1', null, null, null, '2016-01-04 11:54:24', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('10', '1', 'client', '1', null, null, null, '2016-01-04 12:34:06', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('11', '1', 'client', '1', null, null, null, '2016-01-04 13:03:53', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('12', '1', 'client', '1', null, null, null, '2016-01-04 14:31:10', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('13', '1', 'client', '1', null, null, null, '2016-01-04 15:04:30', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('14', '1', 'client', '1', null, null, null, '2016-01-04 15:14:39', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('15', '1', 'client', '1', null, null, null, '2016-01-04 15:36:14', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('16', '1', 'client', '1', null, null, null, '2016-01-04 15:38:05', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('17', '1', 'client', '1', null, null, null, '2016-01-04 16:50:21', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('18', '1', 'client', '1', null, null, null, '2016-01-04 18:41:58', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('19', '1', 'client', '1', null, null, null, '2016-01-04 18:50:29', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('20', '1', 'client', '1', null, null, null, '2016-01-04 19:35:47', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('21', '1', 'client', '1', null, null, null, '2016-01-05 09:48:40', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('22', '1', 'client', '1', null, null, null, '2016-01-05 10:02:42', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('23', '1', 'client', '1', null, null, null, '2016-01-05 10:17:58', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('24', '1', 'client', '1', null, null, null, '2016-01-05 10:19:19', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('25', '1', 'client', '1', null, null, null, '2016-01-05 12:51:10', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('26', '1', 'client', '1', null, null, null, '2016-01-05 15:33:28', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('27', '1', 'client', '1', null, null, null, '2016-01-05 15:48:23', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('28', '1', 'client', '1', null, null, null, '2016-01-05 16:28:37', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('29', '1', 'client', '1', null, null, null, '2016-01-05 16:32:59', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('30', '1', 'client', '1', null, null, null, '2016-01-05 16:36:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('31', '1', 'client', '1', null, null, null, '2016-01-05 16:37:12', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('32', '1', 'client', '1', null, null, null, '2016-01-05 17:34:38', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('33', '1', 'client', '1', null, null, null, '2016-01-05 18:52:34', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('34', '1', 'client', '1', null, null, null, '2016-01-06 11:14:57', '2015-12-29 14:50:53');
INSERT INTO `oauth_sessions` VALUES ('35', '1', 'client', '1', null, null, null, '2016-01-06 15:29:12', '2015-12-29 16:18:54');
INSERT INTO `oauth_sessions` VALUES ('36', '1', 'client', '1', null, null, null, '2016-01-06 15:29:36', '2015-12-29 16:19:24');
INSERT INTO `oauth_sessions` VALUES ('37', '1', 'client', '1', null, null, null, '2016-01-06 15:53:21', '2015-12-29 16:21:05');
INSERT INTO `oauth_sessions` VALUES ('38', '1', 'client', '1', null, null, null, '2016-01-06 17:16:50', '2015-12-29 17:16:22');
INSERT INTO `oauth_sessions` VALUES ('39', '2', 'client', '2', null, null, null, '2015-12-31 11:37:06', '2015-12-29 17:19:00');
INSERT INTO `oauth_sessions` VALUES ('40', '2', 'client', '2', null, null, null, '2015-12-31 19:52:24', '2015-12-29 19:03:07');
INSERT INTO `oauth_sessions` VALUES ('41', '2', 'client', '2', null, null, null, '2015-12-31 19:53:22', '2015-12-29 19:03:42');
INSERT INTO `oauth_sessions` VALUES ('42', '2', 'client', '2', null, null, null, '2016-01-04 10:33:33', '2015-12-29 19:04:03');
INSERT INTO `oauth_sessions` VALUES ('43', '2', 'client', '2', null, null, null, '2016-01-04 10:54:08', '2015-12-29 19:05:46');
INSERT INTO `oauth_sessions` VALUES ('44', '2', 'client', '2', null, null, null, '2016-01-04 10:56:49', '2015-12-29 19:06:09');
INSERT INTO `oauth_sessions` VALUES ('45', '2', 'client', '2', null, null, null, '2016-01-04 10:57:31', '2015-12-29 19:06:27');
INSERT INTO `oauth_sessions` VALUES ('46', '2', 'client', '2', null, null, null, '2016-01-04 10:57:48', '2015-12-29 19:08:01');
INSERT INTO `oauth_sessions` VALUES ('47', '2', 'client', '2', null, null, null, '2016-01-04 11:54:24', '2015-12-29 19:08:10');
INSERT INTO `oauth_sessions` VALUES ('48', '2', 'client', '2', null, null, null, '2016-01-04 12:34:06', '2015-12-29 19:08:15');
INSERT INTO `oauth_sessions` VALUES ('49', '2', 'client', '2', null, null, null, '2016-01-04 13:03:53', '2015-12-29 19:08:57');
INSERT INTO `oauth_sessions` VALUES ('50', '2', 'client', '2', null, null, null, '2016-01-04 14:31:10', '2015-12-29 19:10:57');
INSERT INTO `oauth_sessions` VALUES ('51', '2', 'client', '2', null, null, null, '2016-01-04 15:04:30', '2015-12-29 19:11:04');
INSERT INTO `oauth_sessions` VALUES ('52', '2', 'client', '2', null, null, null, '2016-01-04 15:14:39', '2015-12-29 19:11:23');
INSERT INTO `oauth_sessions` VALUES ('53', '2', 'client', '2', null, null, null, '2016-01-04 15:36:14', '2015-12-29 19:11:40');
INSERT INTO `oauth_sessions` VALUES ('54', '2', 'client', '2', null, null, null, '2016-01-04 15:38:05', '2015-12-29 19:12:04');
INSERT INTO `oauth_sessions` VALUES ('55', '2', 'client', '2', null, null, null, '2016-01-04 16:50:21', '2015-12-29 19:13:12');
INSERT INTO `oauth_sessions` VALUES ('56', '2', 'client', '2', null, null, null, '2016-01-04 18:41:58', '2015-12-29 19:13:51');
INSERT INTO `oauth_sessions` VALUES ('57', '2', 'client', '2', null, null, null, '2016-01-04 18:50:29', '2015-12-29 19:14:51');
INSERT INTO `oauth_sessions` VALUES ('58', '2', 'client', '2', null, null, null, '2016-01-04 19:35:47', '2015-12-29 19:15:10');
INSERT INTO `oauth_sessions` VALUES ('59', '2', 'client', '2', null, null, null, '2016-01-05 09:48:40', '2015-12-29 19:16:08');
INSERT INTO `oauth_sessions` VALUES ('60', '2', 'client', '2', null, null, null, '2016-01-05 10:02:42', '2015-12-29 19:17:30');
INSERT INTO `oauth_sessions` VALUES ('61', '2', 'client', '2', null, null, null, '2016-01-05 10:17:58', '2015-12-29 19:17:33');
INSERT INTO `oauth_sessions` VALUES ('62', '2', 'client', '2', null, null, null, '2016-01-05 10:19:19', '2015-12-29 19:17:33');
INSERT INTO `oauth_sessions` VALUES ('63', '2', 'client', '2', null, null, null, '2016-01-05 12:51:10', '2015-12-29 19:17:33');
INSERT INTO `oauth_sessions` VALUES ('64', '2', 'client', '2', null, null, null, '2016-01-05 15:33:28', '2015-12-29 19:17:33');
INSERT INTO `oauth_sessions` VALUES ('65', '2', 'client', '2', null, null, null, '2016-01-05 15:48:23', '2015-12-29 19:17:33');
INSERT INTO `oauth_sessions` VALUES ('66', '2', 'client', '2', null, null, null, '2016-01-05 16:28:37', '2015-12-29 19:17:33');
INSERT INTO `oauth_sessions` VALUES ('67', '2', 'client', '2', null, null, null, '2016-01-05 16:32:59', '2015-12-29 19:17:34');
INSERT INTO `oauth_sessions` VALUES ('68', '2', 'client', '2', null, null, null, '2016-01-05 16:36:00', '2015-12-29 19:17:34');
INSERT INTO `oauth_sessions` VALUES ('69', '2', 'client', '2', null, null, null, '2016-01-05 16:37:12', '2015-12-29 19:17:34');
INSERT INTO `oauth_sessions` VALUES ('70', '2', 'client', '2', null, null, null, '2016-01-05 17:34:38', '2015-12-29 19:17:34');
INSERT INTO `oauth_sessions` VALUES ('71', '2', 'client', '2', null, null, null, '2016-01-05 18:52:34', '2015-12-29 19:17:34');
INSERT INTO `oauth_sessions` VALUES ('72', '2', 'client', '2', null, null, null, '2016-01-06 11:14:57', '2015-12-29 19:17:35');
INSERT INTO `oauth_sessions` VALUES ('73', '2', 'client', '2', null, null, null, '2016-01-06 15:29:12', '2015-12-29 19:22:33');
INSERT INTO `oauth_sessions` VALUES ('74', '2', 'client', '2', null, null, null, '2016-01-06 15:29:36', '2015-12-29 19:27:30');
INSERT INTO `oauth_sessions` VALUES ('75', '2', 'client', '2', null, null, null, '2016-01-06 15:53:21', '2015-12-29 19:28:47');
INSERT INTO `oauth_sessions` VALUES ('76', '2', 'client', '2', null, null, null, '2016-01-06 17:16:50', '2015-12-29 19:29:00');
INSERT INTO `oauth_sessions` VALUES ('77', '2', 'client', '2', null, null, null, '2015-12-31 11:37:06', '2015-12-29 19:29:45');
INSERT INTO `oauth_sessions` VALUES ('78', '2', 'client', '2', null, null, null, '2015-12-31 19:52:24', '2015-12-29 19:30:05');
INSERT INTO `oauth_sessions` VALUES ('79', '2', 'client', '2', null, null, null, '2015-12-31 19:53:22', '2015-12-29 19:30:27');
INSERT INTO `oauth_sessions` VALUES ('80', '2', 'client', '2', null, null, null, '2016-01-04 10:33:33', '2015-12-29 19:30:41');
INSERT INTO `oauth_sessions` VALUES ('81', '2', 'client', '2', null, null, null, '2016-01-04 10:54:08', '2015-12-29 19:31:06');
INSERT INTO `oauth_sessions` VALUES ('82', '2', 'client', '2', null, null, null, '2016-01-04 10:56:49', '2015-12-29 19:32:14');
INSERT INTO `oauth_sessions` VALUES ('83', '2', 'client', '2', null, null, null, '2016-01-04 10:57:31', '2015-12-30 10:07:27');
INSERT INTO `oauth_sessions` VALUES ('84', '2', 'client', '2', null, null, null, '2016-01-04 10:57:48', '2015-12-30 10:45:59');
INSERT INTO `oauth_sessions` VALUES ('85', '2', 'client', '2', null, null, null, '2016-01-04 11:54:24', '2015-12-30 10:46:10');
INSERT INTO `oauth_sessions` VALUES ('86', '2', 'client', '2', null, null, null, '2016-01-04 12:34:06', '2015-12-30 10:47:22');
INSERT INTO `oauth_sessions` VALUES ('87', '2', 'client', '2', null, null, null, '2016-01-04 13:03:53', '2015-12-30 10:47:32');
INSERT INTO `oauth_sessions` VALUES ('88', '2', 'client', '2', null, null, null, '2016-01-04 14:31:10', '2015-12-30 10:47:40');
INSERT INTO `oauth_sessions` VALUES ('89', '2', 'client', '2', null, null, null, '2016-01-04 15:04:30', '2015-12-30 10:49:18');
INSERT INTO `oauth_sessions` VALUES ('90', '2', 'client', '2', null, null, null, '2016-01-04 15:14:39', '2015-12-30 12:39:23');
INSERT INTO `oauth_sessions` VALUES ('91', '2', 'client', '2', null, null, null, '2016-01-04 15:36:14', '2015-12-30 12:41:35');
INSERT INTO `oauth_sessions` VALUES ('92', '2', 'client', '2', null, null, null, '2016-01-04 15:38:05', '2015-12-30 12:42:03');
INSERT INTO `oauth_sessions` VALUES ('93', '2', 'client', '2', null, null, null, '2016-01-04 16:50:21', '2015-12-30 12:45:19');
INSERT INTO `oauth_sessions` VALUES ('94', '2', 'client', '2', null, null, null, '2016-01-04 18:41:58', '2015-12-30 12:45:37');
INSERT INTO `oauth_sessions` VALUES ('95', '2', 'client', '2', null, null, null, '2016-01-04 18:50:29', '2015-12-30 12:55:43');
INSERT INTO `oauth_sessions` VALUES ('96', '2', 'client', '2', null, null, null, '2016-01-04 19:35:47', '2015-12-30 14:52:20');
INSERT INTO `oauth_sessions` VALUES ('97', '2', 'client', '2', null, null, null, '2016-01-05 09:48:40', '2015-12-30 15:06:56');
INSERT INTO `oauth_sessions` VALUES ('98', '2', 'client', '2', null, null, null, '2016-01-05 10:02:42', '2015-12-30 15:11:10');
INSERT INTO `oauth_sessions` VALUES ('99', '2', 'client', '2', null, null, null, '2016-01-05 10:17:58', '2015-12-30 15:13:14');
INSERT INTO `oauth_sessions` VALUES ('100', '2', 'client', '2', null, null, null, '2016-01-05 10:19:19', '2015-12-30 15:31:56');
INSERT INTO `oauth_sessions` VALUES ('101', '2', 'client', '2', null, null, null, '2016-01-05 12:51:10', '2015-12-30 15:40:05');
INSERT INTO `oauth_sessions` VALUES ('102', '3', 'client', '3', null, null, null, '2016-01-05 15:33:28', '2015-12-30 15:40:21');
INSERT INTO `oauth_sessions` VALUES ('103', '2', 'client', '2', null, null, null, '2016-01-05 15:48:23', '2015-12-30 16:20:21');
INSERT INTO `oauth_sessions` VALUES ('104', '2', 'client', '2', null, null, null, '2016-01-05 16:28:37', '2015-12-30 16:40:58');
INSERT INTO `oauth_sessions` VALUES ('105', '2', 'client', '2', null, null, null, '2016-01-05 16:32:59', '2015-12-30 17:19:52');
INSERT INTO `oauth_sessions` VALUES ('106', '2', 'client', '2', null, null, null, '2016-01-05 16:36:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('107', '2', 'client', '2', null, null, null, '2016-01-05 16:37:12', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('108', '2', 'client', '2', null, null, null, '2016-01-05 17:34:38', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('109', '2', 'client', '2', null, null, null, '2016-01-05 18:52:34', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('110', '3', 'client', '3', null, null, null, '2016-01-06 11:14:57', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('111', '2', 'client', '2', null, null, null, '2016-01-06 15:29:12', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('112', '2', 'client', '2', null, null, null, '2016-01-06 15:29:36', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('113', '2', 'client', '2', null, null, null, '2016-01-06 15:53:21', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('114', '2', 'client', '2', null, null, null, '2016-01-06 17:16:50', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('115', '2', 'client', '2', null, null, null, '2015-12-31 19:53:22', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('116', '2', 'client', '2', null, null, null, '2016-01-04 10:33:33', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('117', '2', 'client', '2', null, null, null, '2016-01-04 10:54:08', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('118', '2', 'client', '2', null, null, null, '2016-01-04 10:56:49', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('119', '2', 'client', '2', null, null, null, '2016-01-04 10:57:31', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('120', '2', 'client', '2', null, null, null, '2016-01-04 10:57:48', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('121', '2', 'client', '2', null, null, null, '2016-01-04 11:54:24', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('122', '2', 'client', '2', null, null, null, '2016-01-04 12:34:06', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('123', '2', 'client', '2', null, null, null, '2016-01-04 13:03:53', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('124', '2', 'client', '2', null, null, null, '2016-01-04 14:31:10', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('125', '2', 'client', '2', null, null, null, '2016-01-04 15:04:30', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('126', '2', 'client', '2', null, null, null, '2016-01-04 15:14:39', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('127', '2', 'client', '2', null, null, null, '2016-01-04 15:36:14', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('128', '2', 'client', '2', null, null, null, '2016-01-04 15:38:05', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('129', '2', 'client', '2', null, null, null, '2016-01-04 16:50:21', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('130', '2', 'client', '2', null, null, null, '2016-01-04 18:41:58', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('131', '2', 'client', '2', null, null, null, '2016-01-04 18:50:29', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('132', '2', 'client', '2', null, null, null, '2016-01-04 19:35:47', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('133', '2', 'client', '2', null, null, null, '2016-01-05 09:48:40', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('134', '2', 'client', '2', null, null, null, '2016-01-05 10:02:42', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('135', '2', 'client', '2', null, null, null, '2016-01-05 10:17:58', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('136', '2', 'client', '2', null, null, null, '2016-01-05 10:19:19', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('137', '2', 'client', '2', null, null, null, '2016-01-05 12:51:10', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('138', '2', 'client', '2', null, null, null, '2016-01-05 15:33:28', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('139', '2', 'client', '2', null, null, null, '2016-01-05 15:48:23', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('140', '2', 'client', '2', null, null, null, '2016-01-05 16:28:37', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('141', '2', 'client', '2', null, null, null, '2016-01-05 16:32:59', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('142', '2', 'client', '2', null, null, null, '2016-01-05 16:36:00', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('143', '2', 'client', '2', null, null, null, '2016-01-05 16:37:12', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('144', '2', 'client', '2', null, null, null, '2016-01-05 17:34:38', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('145', '2', 'client', '2', null, null, null, '2016-01-05 18:52:34', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('146', '2', 'client', '2', null, null, null, '2016-01-06 11:14:57', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('147', '2', 'client', '2', null, null, null, '2016-01-06 15:29:12', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('148', '2', 'client', '2', null, null, null, '2016-01-06 15:29:36', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('149', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '', '2016-01-06 15:53:21', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('150', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '', '2016-01-06 17:16:50', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('151', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '', '2016-01-08 12:40:20', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('152', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '', '2016-01-08 13:04:22', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('153', '3', 'client', '3', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '', '2016-01-08 13:04:41', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('154', '3', 'client', '3', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '', '2016-01-08 13:17:03', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('155', '3', 'client', '3', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '', '2016-01-08 13:18:18', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('156', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '', '2016-01-08 13:34:15', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('157', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '', '2016-01-08 13:41:32', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('158', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '', '2016-01-08 13:43:40', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('159', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '', '2016-01-08 13:56:20', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('160', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '', '2016-01-08 14:24:07', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('161', '3', 'client', '3', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '', '2016-01-08 14:32:15', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('162', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '', '2016-01-08 14:50:01', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('163', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-08 15:05:17', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('164', '3', 'client', '3', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '', '2016-01-08 15:22:06', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('165', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '', '2016-01-08 15:22:14', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('166', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-08 15:22:55', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('167', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '192.168.0.69', '2016-01-08 15:24:52', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('168', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-08 15:31:59', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('169', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '', '2016-01-08 15:36:19', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('170', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '127.0.0.1', '2016-01-08 15:50:45', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('171', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '192.168.0.69', '2016-01-08 15:53:50', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('172', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-08 15:54:23', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('173', '2', 'client', '2', null, '', '0', '2016-01-08 15:55:04', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('174', '2', 'client', '2', null, '', '192.168.0.69', '2016-01-08 15:55:19', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('175', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '192.168.0.69', '2016-01-08 16:21:14', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('176', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-08 16:46:49', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('177', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-08 17:02:35', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('178', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '192.168.0.62', '2016-01-08 17:12:40', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('179', '3', 'client', '3', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-08 17:23:24', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('180', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-08 17:31:16', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('181', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-08 17:57:36', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('182', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-08 18:10:24', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('183', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '192.168.0.91', '2016-01-08 18:27:03', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('184', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-08 18:32:26', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('185', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-08 19:32:39', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('186', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-08 19:32:40', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('187', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '127.0.0.1', '2016-01-09 09:35:21', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('188', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '127.0.0.1', '2016-01-09 09:57:52', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('189', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '127.0.0.1', '2016-01-09 10:08:54', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('190', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:42.0) Gecko/20100101 Firefox/42.0', '127.0.0.1', '2016-01-09 10:17:20', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('191', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '127.0.0.1', '2016-01-09 10:54:56', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('192', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 11:18:34', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('193', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 11:34:58', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('194', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 11:37:15', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('195', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 11:39:20', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('196', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 11:41:11', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('197', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 11:42:30', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('198', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 11:47:08', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('199', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 11:54:02', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('200', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 12:01:45', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('201', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 12:18:03', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('202', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-09 12:38:06', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('203', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 12:40:11', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('204', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-09 12:41:16', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('205', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-09 12:49:29', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('206', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 12:50:39', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('207', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 12:52:09', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('208', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 12:58:59', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('209', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 13:00:30', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('210', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-09 13:00:30', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('211', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 13:01:54', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('212', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-09 13:02:32', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('213', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 13:11:03', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('214', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '127.0.0.1', '2016-01-09 15:36:59', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('215', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '0', '2016-01-09 15:44:27', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('216', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0', '127.0.0.1', '2016-01-09 17:42:13', '0000-00-00 00:00:00');
INSERT INTO `oauth_sessions` VALUES ('217', '2', 'client', '2', null, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', '0', '2016-01-09 18:46:11', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `plan`
-- ----------------------------
DROP TABLE IF EXISTS `plan`;
CREATE TABLE `plan` (
  `id_plan` int(11) NOT NULL AUTO_INCREMENT,
  `plan_name` varchar(50) DEFAULT NULL,
  `no_of_loans` int(11) DEFAULT NULL,
  `price_per_loan` varchar(50) DEFAULT NULL,
  `no_of_users` varchar(50) DEFAULT NULL,
  `total_disk_space` varchar(50) DEFAULT NULL,
  `expired_date` timestamp NULL DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_plan`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of plan
-- ----------------------------
INSERT INTO `plan` VALUES ('1', 'Standard', '2', '100000', '5', '15', null, '2015-12-10 18:32:16');
INSERT INTO `plan` VALUES ('2', 'Graduated', '3', '300000', '10', '20', null, '2015-12-15 16:46:48');
INSERT INTO `plan` VALUES ('3', 'Extended', '5', '5000', '10', '10GB', null, '2015-12-10 16:21:31');
INSERT INTO `plan` VALUES ('12', 'Income Based', '0', '200000', '6', '4', null, '2015-12-10 16:23:03');
INSERT INTO `plan` VALUES ('13', 'Pay As You Earn', '0', '50000', '3', '5GB', null, '2015-12-10 16:24:21');
INSERT INTO `plan` VALUES ('14', 'Income Contingent', '2', '500000', '4', '6', null, '2015-12-10 16:23:38');
INSERT INTO `plan` VALUES ('15', 'Income Sensitive', '10', '1000000', '10', '10', null, '2015-12-10 16:25:20');

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', 'Car loan', '2016-01-09 12:58:03', null);
INSERT INTO `product` VALUES ('2', 'Business loan', '2016-01-09 12:58:03', null);
INSERT INTO `product` VALUES ('3', 'House loan', '2016-01-09 12:58:03', null);

-- ----------------------------
-- Table structure for `project_contact`
-- ----------------------------
DROP TABLE IF EXISTS `project_contact`;
CREATE TABLE `project_contact` (
  `id_project_contact` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT NULL,
  `crm_project_id` int(11) DEFAULT NULL,
  `crm_contact_id` int(11) DEFAULT NULL,
  `is_primary_contact` tinyint(4) DEFAULT NULL,
  `project_contact_status` tinyint(4) DEFAULT '1',
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_project_contact`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project_contact
-- ----------------------------
INSERT INTO `project_contact` VALUES ('1', '2', '31', '116', null, '1', '2016-01-09 20:00:55');
INSERT INTO `project_contact` VALUES ('2', '2', '31', '115', null, '1', '2016-01-09 20:03:17');
INSERT INTO `project_contact` VALUES ('3', '2', '31', '109', null, '1', '2016-01-09 20:04:07');

-- ----------------------------
-- Table structure for `project_team`
-- ----------------------------
DROP TABLE IF EXISTS `project_team`;
CREATE TABLE `project_team` (
  `id_project_team` int(11) NOT NULL,
  `crm_project_id` int(11) DEFAULT NULL,
  `team_member_id` int(11) DEFAULT NULL,
  `team_member_status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_project_team`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project_team
-- ----------------------------

-- ----------------------------
-- Table structure for `section`
-- ----------------------------
DROP TABLE IF EXISTS `section`;
CREATE TABLE `section` (
  `id_section` int(11) NOT NULL AUTO_INCREMENT,
  `section_name` varchar(255) DEFAULT NULL,
  `section_order` int(11) DEFAULT NULL,
  `crm_module_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_section`),
  KEY `crm_module_id` (`crm_module_id`),
  CONSTRAINT `section_ibfk_1` FOREIGN KEY (`crm_module_id`) REFERENCES `crm_module` (`id_crm_module`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of section
-- ----------------------------
INSERT INTO `section` VALUES ('1', 'Basic Information', '1', '1');
INSERT INTO `section` VALUES ('2', 'Income Information', '2', '1');
INSERT INTO `section` VALUES ('3', 'Business Key Data', '1', '2');
INSERT INTO `section` VALUES ('4', 'Business Assessment Part 1', '2', '2');
INSERT INTO `section` VALUES ('5', 'Business Assessment Part 2', '3', '2');
INSERT INTO `section` VALUES ('6', 'Cash Flow Planning', '4', '2');
INSERT INTO `section` VALUES ('7', 'Client SWOT Analysis', '5', '2');
INSERT INTO `section` VALUES ('8', 'Pipeline', '1', '3');
INSERT INTO `section` VALUES ('9', 'Terms and Conditions', '2', '3');
INSERT INTO `section` VALUES ('10', 'Risk Assessment', '3', '3');
INSERT INTO `section` VALUES ('11', 'Summary and Recommendation', '4', '3');

-- ----------------------------
-- Table structure for `sector`
-- ----------------------------
DROP TABLE IF EXISTS `sector`;
CREATE TABLE `sector` (
  `id_sector` int(11) NOT NULL AUTO_INCREMENT,
  `sector_name` varchar(45) NOT NULL,
  `parent_sector_id` int(11) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sector_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_sector`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sector
-- ----------------------------
INSERT INTO `sector` VALUES ('1', 'Commercial', '2', '2015-12-04 18:42:43', null);
INSERT INTO `sector` VALUES ('2', 'Agriculture', '3', '2015-12-08 00:00:00', null);
INSERT INTO `sector` VALUES ('3', 'Public', '5', '2015-12-10 16:00:20', null);
INSERT INTO `sector` VALUES ('4', 'Private', '1', '2015-12-10 16:01:21', null);
INSERT INTO `sector` VALUES ('5', 'Regional', '4', '2015-12-10 16:02:04', null);
INSERT INTO `sector` VALUES ('6', 'testing', '2', '2016-01-05 00:00:00', null);
INSERT INTO `sector` VALUES ('7', 'test1', '0', '2016-01-05 00:00:00', null);

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_id` int(11) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email_id` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `phone_number` bigint(20) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `profile_image` varchar(500) DEFAULT NULL,
  `user_status` tinyint(2) NOT NULL DEFAULT '1',
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_user`) USING BTREE,
  KEY `fk_users_user_role_idx` (`user_role_id`),
  KEY `country_id` (`country_id`),
  CONSTRAINT `fk_users_user_role` FOREIGN KEY (`user_role_id`) REFERENCES `user_role` (`id_user_role`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id_country`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', 'super', 'admin', 'superadmin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9652429394', '310, Road Number 25, Jubilee Hills,500034', 'hyderabad', 'telangana', '2', 'jayerowe_1449214410.png', '1', '2015-12-15 17:15:29', null);
INSERT INTO `user` VALUES ('2', '2', 'rakesh', 'kumar', 'admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '54353543545', 'Hills,500034', 'hyderabad', 'telangana', '2', 'Tulips_1449138679.jpg', '1', '2015-12-15 17:15:31', null);
INSERT INTO `user` VALUES ('4', '2', 'mohan', 'babu', 'mohan@gmail.com', '70d08dc4adfb0e3b1e1670536b2cc450', '7894561236', 'Hills,500034', 'hyderabad', 'telangana', '1', 'index_1450071481.jpg', '1', '2015-12-15 17:15:37', null);
INSERT INTO `user` VALUES ('6', '2', 'naresh', 'babu', 'nareshkumar@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1234567895', 'hyderabad', 'hyderabad', 'telangana', '1', 'Penguins_1449213984.jpg', '1', '2015-12-18 15:14:52', null);
INSERT INTO `user` VALUES ('7', '1', 'santhosh', 'kumar', 'rocck_mic@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9456456545', 'Hills,500034', 'hyderabad', 'telangana', '1', 'jayerowe_1449214410.png', '1', '2015-12-16 15:24:50', null);
INSERT INTO `user` VALUES ('8', '1', 'ramesh', 'babu', 'johenpoop@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '54564512323', 'Hills,500034', 'hyderabad', 'telangana', '1', 'Tulips_1449138679.jpg', '0', '2015-12-16 15:24:51', null);
INSERT INTO `user` VALUES ('20', '2', 'Sthanka', 'bhasha', 'bhasha1.s@thresholdsoft.com', '93e3937418aa143a83d0349e6cd7bde0', '86546546', 'hyderabad', 'hyderabad', 'telangana', '1', '', '1', '2015-12-16 15:22:23', null);
INSERT INTO `user` VALUES ('21', '2', 'Mohanbabu', 'Pachipala', 'palaekirimohanbabu@gmail.com', '79c04b93fa7a', '9581565753', 'Hills,500034', 'hyderabad', 'telangana', '1', 'Tulips_1449138679.jpg', '1', '2015-12-15 17:15:41', null);
INSERT INTO `user` VALUES ('24', '2', 'krishna', 'pvr', 'bhashqa.s@thresholdsoft.com', '6ffb418b1088', '1234567895', null, null, null, '2', null, '1', '2015-12-15 17:15:41', null);
INSERT INTO `user` VALUES ('39', '2', 'Manasa', 'Thummala', 'manasa.th@thresholdsoft.com', '05763fdd435d2e46a89f608ffcd13307', '9767676665', 'Nellore', 'null', 'null', '4', 'Feng_Chia_University_1449898577.png', '1', '2015-12-16 15:25:51', null);
INSERT INTO `user` VALUES ('42', '2', 'Mohanbabu', 'Pachipala', 'mohanbabu1.p@thresholdsoft.com', 'e10adc3949ba59abbe56e057f20f883e', '9581565753', null, null, null, '4', '10298703_656786114393056_6107077187498387812_n_1449902115.jpg', '1', '2015-12-15 17:15:43', null);
INSERT INTO `user` VALUES ('43', '2', 'Mohanbabu', 'Pachipala', 'mohanbabu.p@thresholdsoft.com', 'e10adc3949ba59abbe56e057f20f883e', '9581565753', null, null, null, '4', '10298703_656786114393056_6107077187498387812_n_1449902345.jpg', '1', '2015-12-15 17:15:44', null);
INSERT INTO `user` VALUES ('44', '2', 'praveen', 'kumar', 'sthanka.shaik@gmail.com', '45b06af9d0de8e4f8072b8786f2c6d96', '7894561231', null, null, null, '4', 'Person-Donald-900x1080_1449902992.jpg', '1', '2015-12-15 17:15:43', null);
INSERT INTO `user` VALUES ('56', '3', 'ravi', 'kumar', 'ravi@gmail.com', '7a346c72477e9ada010e01703b0503c2', '7894561235', 'hyderabad 222', 'null', 'null', '6', '1Penguins_1451994157.jpg', '0', '2016-01-05 17:13:06', null);
INSERT INTO `user` VALUES ('57', '3', 'naresh', 'gurrala', 'naresh@gmail.com', '7824c46b27a1d614215c14790874e083', '123123123', 'hydarabad\r\nkukatpally 123', 'null', 'null', '6', 'flow3_1450269769.jpg', '1', '2015-12-18 15:15:30', null);
INSERT INTO `user` VALUES ('58', '3', 'naresh', 'gurrala', 'suresh@gmail.com', '9749f5bf283a95d77712dd7b00d19f01', '1231231231', 'hydarabad\r\nkukatpally', 'null', 'null', '6', 'http://localhost/q-lana/rest/images/default-img.png', '0', '2015-12-18 15:15:36', null);
INSERT INTO `user` VALUES ('59', '3', 'naresh', 'gurrala', 'ramesh@gmail.com', '077278a5102318681ceecac693ac7462', '1234567896', 'dsf', 'null', 'null', '6', 'default-img.png', '1', '2015-12-18 15:15:37', null);
INSERT INTO `user` VALUES ('61', '3', 'mohan', 'gurrala', 'mohan3333@gmail.com', '5677509f00f7af6fb727e30428a1be4c', '12312313', 'hydarabad\r\nkukatpally', 'null', 'null', '6', 'default-img.png', '1', '2015-12-15 17:15:49', null);
INSERT INTO `user` VALUES ('62', '2', 'fgd', 'fgd', 'dgfd@gmail.com', '1a1bef0feac345298eab0ea32a98a84b', '7894561235', null, null, null, '4', null, '1', '2015-12-16 11:34:48', null);
INSERT INTO `user` VALUES ('87', '3', 'radha', 'krishna', 'bhasha.s@thresholdsoft.com', '59ce0246849504aee156986943414525', '4561237895', 'hyderabad', null, null, '1', null, '1', '2015-12-16 15:22:53', null);
INSERT INTO `user` VALUES ('89', '2', 'Manasa', 'Thummala', 'manasa.t@thresholdsoft.com', 'e10adc3949ba59abbe56e057f20f883e', '9876543210', null, null, null, null, 'Chrysanthemum_1450259994.jpg', '1', '2015-12-16 16:03:19', null);
INSERT INTO `user` VALUES ('91', '3', 'Siva', 'Kumar', 'sivakumar@gmail.com', 'b2a874225409d6267a5039a3789549cb', '9876543210', 'madhapur', null, null, null, null, '1', '2015-12-16 17:19:02', null);
INSERT INTO `user` VALUES ('92', '3', 'Balaji', 'babu', 'balaji@gmail.com', '3dbc8f1b370012b674f9c915afa9c2a0', '9864456356', 'ameerpet', 'null', 'null', null, 'Penguins_1450268025.jpg', '0', '2015-12-21 11:10:27', null);
INSERT INTO `user` VALUES ('93', '3', 'krishna', 'prasad', 'prasad@gmail.com', 'f5aae721131329c166277216f2430826', '7894561231', 'hyderabad', 'null', 'null', null, 'http://desk8-tss/q-lana/rest/images/default-img.png', '1', '2015-12-16 18:58:11', null);
INSERT INTO `user` VALUES ('94', '3', 'Manasa', 'Thummala', 'manasa.t@gmail.com', '2e83271b282360f61d9f016e49e29bc6', '9876543210', 'hyderabad', null, null, '1', null, '1', '2015-12-17 10:51:00', null);
INSERT INTO `user` VALUES ('95', '3', 'Sthanka', 'Bhasha', 'sthankabhasha@gmail.com', '110d44f8ba4ef3450c7bad3a4fc4186e', '9876543211', 'guntur', null, null, '1', null, '1', '2015-12-17 10:51:02', null);
INSERT INTO `user` VALUES ('96', '3', 'mohan', 'babu', 'mohanbabu@gmail.com', '4dd357d09ff43dc65e6c97fad228a2f6', '9876543212', 'tirupati', null, null, '1', null, '1', '2015-12-17 10:51:03', null);
INSERT INTO `user` VALUES ('97', '3', 'ram', 'babu', 'rambabu@gmail.com', '58d913b3d5747734b81570d607634f20', '9876543213', 'hyderabad', null, null, '1', null, '1', '2015-12-17 10:51:04', null);
INSERT INTO `user` VALUES ('98', '3', 'ramesh', 'paul', 'rameshpaul@gmail.com', 'dbe1fe24af3711d158e386d46a0dfadd', '9876543214', 'chitoor', null, null, '1', null, '1', '2015-12-17 10:51:04', null);
INSERT INTO `user` VALUES ('99', '2', 'Naresh', 'a', 'nareshbabu@gmail.com', '62baab58dca4b45f9af95d3ad21e45c0', '8675656658', 'warangal', null, null, '1', null, '1', '2015-12-18 15:13:44', null);
INSERT INTO `user` VALUES ('100', '3', 'Divya', 'Sree', 'divyasree@gmail.com', '57ff55d9d783442df028b944b0719e2d', '8675656659', 'kakinada', 'null', 'null', '1', 'Koala_1450676481.jpg', '1', '2015-12-21 11:11:21', null);
INSERT INTO `user` VALUES ('101', '3', 'Sundeepthi', 'mallipudi', 'sundeepthi@gmail.com', 'a01c694f61fcef56cbb1adc7b22258b8', '8675656660', 'karimnagar', null, null, '6', null, '1', '2015-12-17 10:51:06', null);
INSERT INTO `user` VALUES ('102', '3', 'Masthani', 'shaik', 'masthani@gmail.com', 'dd9e28c2908736e58b3a7b13a2d17661', '8675656661', 'ananthapur', 'null', 'null', '6', 'http://localhost/q-lana/rest/images/default-img.png', '1', '2015-12-17 11:01:17', null);
INSERT INTO `user` VALUES ('104', '2', 'Manasa', 'Thummala', 'hsbc@gmail.com', 'bb5b1fb5c5e5722c10ed0e446da8b8b7', '987654321', null, null, null, null, null, '0', '2015-12-23 16:19:24', null);

-- ----------------------------
-- Table structure for `user_role`
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id_user_role` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_user_role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('1', 'superadmin');
INSERT INTO `user_role` VALUES ('2', 'admin');
INSERT INTO `user_role` VALUES ('3', 'user');
