/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.4_3306
Source Server Version : 50546
Source Host           : 192.168.0.4:3306
Source Database       : qlana

Target Server Type    : MYSQL
Target Server Version : 50546
File Encoding         : 65001

Date: 2015-12-12 19:59:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `approval_role`
-- ----------------------------
DROP TABLE IF EXISTS `approval_role`;
CREATE TABLE `approval_role` (
  `id_approval_role` int(11) NOT NULL AUTO_INCREMENT,
  `approval_name` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `approval_role_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_approval_role`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of approval_role
-- ----------------------------
INSERT INTO `approval_role` VALUES ('1', 'Account manager', '2015-12-12 14:46:29', 'AM');
INSERT INTO `approval_role` VALUES ('2', 'Financial Manager', '2015-12-12 14:46:42', 'FM');
INSERT INTO `approval_role` VALUES ('3', 'Loan Officers', '2015-12-12 14:47:15', 'LF');
INSERT INTO `approval_role` VALUES ('4', 'Bank Manager', '2015-12-12 14:47:24', 'BM');

-- ----------------------------
-- Table structure for `bank_category`
-- ----------------------------
DROP TABLE IF EXISTS `bank_category`;
CREATE TABLE `bank_category` (
  `id_bank_category` int(11) NOT NULL AUTO_INCREMENT,
  `bank_category_name` varchar(50) DEFAULT NULL,
  `bank_category_code` varchar(50) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_bank_category`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bank_category
-- ----------------------------
INSERT INTO `bank_category` VALUES ('1', 'Commercial', 'CMCL', '2015-12-10 16:48:19');
INSERT INTO `bank_category` VALUES ('2', 'Retail', 'RTL', '2015-12-12 14:56:36');
INSERT INTO `bank_category` VALUES ('3', 'Investment', 'INVM', '2015-12-12 14:56:50');
INSERT INTO `bank_category` VALUES ('20', 'Cooperative', 'COP', '2015-12-12 14:57:01');
INSERT INTO `bank_category` VALUES ('21', 'Specialized', 'SP', '2015-12-12 14:57:16');
INSERT INTO `bank_category` VALUES ('22', 'Central', 'CNTL', '2015-12-10 18:32:01');

-- ----------------------------
-- Table structure for `branch_type`
-- ----------------------------
DROP TABLE IF EXISTS `branch_type`;
CREATE TABLE `branch_type` (
  `id_branch_type` int(11) NOT NULL AUTO_INCREMENT,
  `branch_type_name` varchar(50) DEFAULT NULL,
  `branch_type_code` varchar(50) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_branch_type`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of branch_type
-- ----------------------------
INSERT INTO `branch_type` VALUES ('1', 'head office', 'HO854', '2015-12-10 16:03:09');
INSERT INTO `branch_type` VALUES ('2', 'divisional', 'DN362', '2015-12-10 16:03:21');
INSERT INTO `branch_type` VALUES ('3', 'sub divisional', 'SDN12', '2015-12-10 16:03:38');
INSERT INTO `branch_type` VALUES ('4', 'branch', 'BN123', '2015-12-10 18:29:04');

-- ----------------------------
-- Table structure for `company`
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `id_company` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(45) DEFAULT NULL,
  `company_logo` varchar(500) DEFAULT NULL,
  `company_address` varchar(45) DEFAULT NULL,
  `company_city` varchar(45) DEFAULT NULL,
  `company_state` varchar(45) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `contact_person_id` int(11) DEFAULT NULL,
  `bank_category_id` int(11) DEFAULT NULL,
  `company_status` varchar(45) NOT NULL DEFAULT 'active',
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `company_caption` varchar(45) NOT NULL,
  PRIMARY KEY (`id_company`) USING BTREE,
  KEY `plan_id` (`plan_id`),
  KEY `country_id` (`country_id`),
  KEY `bank_category_id` (`bank_category_id`),
  KEY `contact_person_id` (`contact_person_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `company_ibfk_1` FOREIGN KEY (`plan_id`) REFERENCES `plan` (`id_plan`),
  CONSTRAINT `company_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `country` (`id_country`),
  CONSTRAINT `company_ibfk_3` FOREIGN KEY (`bank_category_id`) REFERENCES `bank_category` (`id_bank_category`),
  CONSTRAINT `company_ibfk_4` FOREIGN KEY (`contact_person_id`) REFERENCES `user` (`id_user`),
  CONSTRAINT `company_ibfk_5` FOREIGN KEY (`user_id`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of company
-- ----------------------------
INSERT INTO `company` VALUES ('1', 'Q-lana', 'company5_1449214179.jpg', 'US', null, null, null, '1', null, null, '', '2015-12-12 10:51:17', '2', 'customer service');
INSERT INTO `company` VALUES ('3', 'HSBC', 'azri_1449579007.png', 'hyderabad', null, null, null, '2', null, null, '', '2015-12-12 10:51:23', '6', 'customer service');
INSERT INTO `company` VALUES ('4', 'Citi Bank', 'company5_1449214179.jpg', 'Citi Bank', null, null, null, '3', null, null, '', '2015-12-12 10:51:25', '7', 'customer service');
INSERT INTO `company` VALUES ('5', 'Capital One', 'company3_1449214410.jpg', 'Capital one,\r\nsdafsdf,\r\nsdfasdfasdf', null, null, null, '12', null, null, '', '2015-12-12 12:23:37', '8', 'customer service');
INSERT INTO `company` VALUES ('16', 'My own bank', 'SooChow_1449898577.png', 'hyderabad', null, null, '1', '3', null, null, '', '2015-12-12 11:06:20', '39', '');
INSERT INTO `company` VALUES ('19', 'IDBI', 'idbi_logo_1449902115.png', 'Jubilee Hills,Hyderabad', null, null, '1', '3', null, null, '', '2015-12-12 12:05:15', '42', '');
INSERT INTO `company` VALUES ('20', 'Andra bank', 'andrabank_logo_1449902345.png', 'California', null, null, '2', '12', null, null, '', '2015-12-12 12:09:06', '43', '');
INSERT INTO `company` VALUES ('21', 'baroda bank', 'bank-baroda_1449902992.JPG', 'hyderabad', null, null, '1', '2', null, null, '', '2015-12-12 12:23:04', '44', '');

-- ----------------------------
-- Table structure for `company_approval_group`
-- ----------------------------
DROP TABLE IF EXISTS `company_approval_group`;
CREATE TABLE `company_approval_group` (
  `id_approval_group` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `approval_group_name` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id_approval_group`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `company_approval_group_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id_company`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of company_approval_group
-- ----------------------------
INSERT INTO `company_approval_group` VALUES ('1', '1', 'document checking', '2015-12-11 16:11:18', null);

-- ----------------------------
-- Table structure for `company_approval_role`
-- ----------------------------
DROP TABLE IF EXISTS `company_approval_role`;
CREATE TABLE `company_approval_role` (
  `id_company_approval_role` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `approval_group_id` int(11) DEFAULT NULL,
  `approval_role_id` int(11) DEFAULT NULL,
  `reporting_role_id` int(11) DEFAULT NULL,
  `approval_status` varchar(255) DEFAULT 'active',
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_company_approval_role`),
  KEY `company_id` (`company_id`),
  KEY `approval_group_id` (`approval_group_id`),
  CONSTRAINT `company_approval_role_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id_company`),
  CONSTRAINT `company_approval_role_ibfk_3` FOREIGN KEY (`approval_group_id`) REFERENCES `company_approval_group` (`id_approval_group`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of company_approval_role
-- ----------------------------
INSERT INTO `company_approval_role` VALUES ('4', '1', '1', '1', '0', 'active', '2015-12-11 19:15:12');
INSERT INTO `company_approval_role` VALUES ('5', '1', '1', '2', '1', 'active', '2015-12-11 19:15:12');
INSERT INTO `company_approval_role` VALUES ('6', '1', '1', '3', '2', 'active', '2015-12-11 19:15:12');
INSERT INTO `company_approval_role` VALUES ('7', '20', '1', '1', '0', 'active', '2015-12-12 12:18:09');
INSERT INTO `company_approval_role` VALUES ('8', '20', '1', '2', '1', 'active', '2015-12-12 12:18:10');

-- ----------------------------
-- Table structure for `company_branch`
-- ----------------------------
DROP TABLE IF EXISTS `company_branch`;
CREATE TABLE `company_branch` (
  `id_branch` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_type_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `legal_name` varchar(255) DEFAULT NULL,
  `branch_address` varchar(255) DEFAULT NULL,
  `branch_city` varchar(255) DEFAULT NULL,
  `branch_state` varchar(255) DEFAULT NULL,
  `branch_logo` varchar(255) DEFAULT NULL,
  `branch_status` varchar(255) DEFAULT 'active',
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reporting_branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_branch`),
  KEY `company_id` (`company_id`),
  KEY `branch_type_id` (`branch_type_id`),
  KEY `country_id` (`country_id`),
  CONSTRAINT `company_branch_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id_company`),
  CONSTRAINT `company_branch_ibfk_2` FOREIGN KEY (`branch_type_id`) REFERENCES `branch_type` (`id_branch_type`),
  CONSTRAINT `company_branch_ibfk_3` FOREIGN KEY (`country_id`) REFERENCES `country` (`id_country`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of company_branch
-- ----------------------------
INSERT INTO `company_branch` VALUES ('1', '3', '1', '1', 'Madhapur Branch', 'mumbai', 'hyderabad', 'hyderabad', null, 'active', '2015-12-12 11:01:44', null);
INSERT INTO `company_branch` VALUES ('2', '3', '2', '1', 'Ameerpet Branch', 'hyderabad', 'hyderabad', 'hyderabad', null, 'active', '2015-12-12 11:01:49', null);
INSERT INTO `company_branch` VALUES ('15', '1', '3', '1', 'Begumpet Branch', 'abdsc, floor,najnzj colony', 'Hyderabad', 'Andrapradesh', null, 'active', '2015-12-12 11:00:01', null);
INSERT INTO `company_branch` VALUES ('16', '1', '4', '1', 'Brodipet Branch', 'line no 3,brodipet, guntur', 'Guntur', 'Andrapradesh', 'Branchlogo-default_1449835575.png', 'active', '2015-12-12 11:02:47', null);
INSERT INTO `company_branch` VALUES ('17', '20', '1', '1', 'Jubille Hills', 'Road No:10', 'Hyderabad', 'Telangana', 'idbi_logo_1449903531.png', 'active', '2015-12-12 12:28:41', null);
INSERT INTO `company_branch` VALUES ('18', null, '4', '1', 'panjagutta branch', 'hyderabad', 'hyderabad', 'hyderabad', null, 'active', '2015-12-12 17:18:00', '2');

-- ----------------------------
-- Table structure for `company_branch_type`
-- ----------------------------
DROP TABLE IF EXISTS `company_branch_type`;
CREATE TABLE `company_branch_type` (
  `id_company_branch_type` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `branch_type_id` int(11) NOT NULL,
  `reporting_branch_type_id` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_company_branch_type`),
  KEY `company_id` (`company_id`),
  KEY `branch_type_id` (`branch_type_id`),
  CONSTRAINT `company_branch_type_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id_company`),
  CONSTRAINT `company_branch_type_ibfk_2` FOREIGN KEY (`branch_type_id`) REFERENCES `branch_type` (`id_branch_type`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of company_branch_type
-- ----------------------------
INSERT INTO `company_branch_type` VALUES ('1', '3', '1', '0', '2015-12-10 16:21:31');
INSERT INTO `company_branch_type` VALUES ('2', '3', '2', '1', '2015-12-10 17:02:07');
INSERT INTO `company_branch_type` VALUES ('3', '3', '3', '2', '2015-12-10 17:02:11');
INSERT INTO `company_branch_type` VALUES ('4', '3', '4', '2', '2015-12-10 17:23:30');
INSERT INTO `company_branch_type` VALUES ('5', '1', '1', '0', '2015-12-11 16:05:04');
INSERT INTO `company_branch_type` VALUES ('6', '1', '2', '1', '2015-12-11 16:05:05');
INSERT INTO `company_branch_type` VALUES ('7', '1', '3', '2', '2015-12-11 16:05:05');
INSERT INTO `company_branch_type` VALUES ('8', '1', '4', '3', '2015-12-11 17:10:09');
INSERT INTO `company_branch_type` VALUES ('19', '20', '1', '0', '2015-12-12 12:20:04');
INSERT INTO `company_branch_type` VALUES ('20', '20', '2', '1', '2015-12-12 12:20:04');

-- ----------------------------
-- Table structure for `company_user`
-- ----------------------------
DROP TABLE IF EXISTS `company_user`;
CREATE TABLE `company_user` (
  `id_company_user` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `company_approval_role_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `reporting_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_company_user`),
  KEY `user_id` (`user_id`),
  KEY `company_id` (`company_id`),
  KEY `approval_role_id` (`company_approval_role_id`),
  KEY `branch_id` (`branch_id`),
  CONSTRAINT `company_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id_user`),
  CONSTRAINT `company_user_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `company` (`id_company`),
  CONSTRAINT `company_user_ibfk_4` FOREIGN KEY (`branch_id`) REFERENCES `company_branch` (`id_branch`),
  CONSTRAINT `company_user_ibfk_5` FOREIGN KEY (`company_approval_role_id`) REFERENCES `company_approval_role` (`id_company_approval_role`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of company_user
-- ----------------------------
INSERT INTO `company_user` VALUES ('1', '1', '4', null, '15', '0');
INSERT INTO `company_user` VALUES ('3', '1', '20', null, '15', '0');
INSERT INTO `company_user` VALUES ('13', '16', '39', null, null, null);
INSERT INTO `company_user` VALUES ('16', '19', '42', null, null, null);
INSERT INTO `company_user` VALUES ('17', '20', '43', null, null, null);
INSERT INTO `company_user` VALUES ('18', '21', '44', null, null, null);

-- ----------------------------
-- Table structure for `country`
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `id_country` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(50) DEFAULT NULL,
  `country_code` varchar(50) DEFAULT NULL,
  `country_timezone` varchar(50) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_country`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of country
-- ----------------------------
INSERT INTO `country` VALUES ('1', 'India', 'IND', null, '2015-12-04 12:28:19');
INSERT INTO `country` VALUES ('2', 'Australia', 'AUS', null, '2015-12-04 12:29:05');
INSERT INTO `country` VALUES ('4', 'Zimbabwe', 'ZIM', null, '2015-12-10 18:28:47');
INSERT INTO `country` VALUES ('6', 'South Africa', 'SA', null, '2015-12-08 18:40:33');
INSERT INTO `country` VALUES ('7', 'Newzeland', 'NZ', null, '2015-12-08 18:53:49');

-- ----------------------------
-- Table structure for `keys`
-- ----------------------------
DROP TABLE IF EXISTS `keys`;
CREATE TABLE `keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of keys
-- ----------------------------
INSERT INTO `keys` VALUES ('1', '17aa0fdb4df9b06281e401d40acbfb37', '0', '0', '0', null, '2015-12-12 19:11:31', '1');
INSERT INTO `keys` VALUES ('2', 'dffbb18dfc6cb06b607f65314a7cfdfc', '0', '0', '0', null, '2015-12-12 19:44:55', '2');
INSERT INTO `keys` VALUES ('3', '57eb1d6d0d033b9956f9410078789de6', '0', '0', '0', null, '2015-12-12 11:32:34', '39');
INSERT INTO `keys` VALUES ('4', '482360161964662ccd743a6c87d147a9', '0', '0', '0', null, '2015-12-12 11:12:59', '41');
INSERT INTO `keys` VALUES ('5', '65d0c4e905952923e7c8ce273e2c569f', '0', '0', '0', null, '2015-12-12 18:46:58', '43');

-- ----------------------------
-- Table structure for `plan`
-- ----------------------------
DROP TABLE IF EXISTS `plan`;
CREATE TABLE `plan` (
  `id_plan` int(11) NOT NULL AUTO_INCREMENT,
  `plan_name` varchar(50) DEFAULT NULL,
  `no_of_loans` int(11) DEFAULT NULL,
  `price_per_loan` varchar(50) DEFAULT NULL,
  `no_of_users` varchar(50) DEFAULT NULL,
  `total_disk_space` varchar(50) DEFAULT NULL,
  `expired_date` timestamp NULL DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_plan`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of plan
-- ----------------------------
INSERT INTO `plan` VALUES ('1', 'Standard', '2', '100000', '5', '15', null, '2015-12-10 18:32:16');
INSERT INTO `plan` VALUES ('2', 'Graduated', '3', '300000', '10', null, null, '2015-12-10 16:21:23');
INSERT INTO `plan` VALUES ('3', 'Extended', '5', '5000', '10', '10GB', null, '2015-12-10 16:21:31');
INSERT INTO `plan` VALUES ('12', 'Income Based', '0', '200000', '6', '4', null, '2015-12-10 16:23:03');
INSERT INTO `plan` VALUES ('13', 'Pay As You Earn', '0', '50000', '3', '5GB', null, '2015-12-10 16:24:21');
INSERT INTO `plan` VALUES ('14', 'Income Contingent', '2', '500000', '4', '6', null, '2015-12-10 16:23:38');
INSERT INTO `plan` VALUES ('15', 'Income Sensitive', '10', '1000000', '10', '10', null, '2015-12-10 16:25:20');

-- ----------------------------
-- Table structure for `sector`
-- ----------------------------
DROP TABLE IF EXISTS `sector`;
CREATE TABLE `sector` (
  `id_sector` int(11) NOT NULL AUTO_INCREMENT,
  `sector_name` varchar(45) NOT NULL,
  `parent_sector_id` int(11) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sector_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_sector`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sector
-- ----------------------------
INSERT INTO `sector` VALUES ('1', 'Commercial', '2', '2015-12-04 18:42:43', null);
INSERT INTO `sector` VALUES ('2', 'Agriculture', '3', '2015-12-08 00:00:00', null);
INSERT INTO `sector` VALUES ('3', 'Public', '5', '2015-12-10 16:00:20', null);
INSERT INTO `sector` VALUES ('4', 'Private', '1', '2015-12-10 16:01:21', null);
INSERT INTO `sector` VALUES ('5', 'Regional', '4', '2015-12-10 16:02:04', null);

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_id` int(11) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email_id` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `phone_number` bigint(20) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `profile_image` varchar(500) DEFAULT NULL,
  `user_status` varchar(6) DEFAULT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_user`) USING BTREE,
  KEY `fk_users_user_role_idx` (`user_role_id`),
  KEY `country_id` (`country_id`),
  CONSTRAINT `fk_users_user_role` FOREIGN KEY (`user_role_id`) REFERENCES `user_role` (`id_user_role`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id_country`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', 'super', 'admin', 'superadmin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9652429394', '310, Road Number 25, Jubilee Hills,500034', 'hyderabad', 'telangana', '2', 'jayerowe_1449214410.png', null, '2015-12-12 10:48:11', null);
INSERT INTO `user` VALUES ('2', '2', 'rakesh', 'kumar', 'admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '54353543545', 'Hills,500034', 'hyderabad', 'telangana', null, 'Tulips_1449138679.jpg', null, '2015-12-11 16:51:32', null);
INSERT INTO `user` VALUES ('4', '1', 'mohan', 'babu', 'mohan@gmail.com', '1388736cd27848a55ac8a73a544cd6e7', '7894561236', 'Hills,500034', 'hyderabad', 'telangana', null, 'http://localhost/q-lana/rest/uploads/http://localhost/q-lana/rest/uploads/http://localhost/q-lana/rest/uploads/http://localhost/q-lana/rest/uploads/http://localhost/q-lana/rest/uploads/http://localhost/q-lana/rest/uploads/http://localhost/q-lana/rest/uploads/http://localhost/q-lana/rest/uploads/http://localhost/q-lana/rest/uploads/http://localhost/q-lana/rest/uploads/http://localhost/q-lana/rest/uploads/http://localhost/q-lana/rest/uploads/http://localhost/q-lana/rest/uploads/http://localhost/q-', null, '2015-12-12 18:24:32', null);
INSERT INTO `user` VALUES ('6', '2', 'naresh', 'kumar', 'nareshkumar@gmail.com', null, '1234567895', 'hyderabad', 'hyderabad', 'telangana', null, 'Penguins_1449213984.jpg', null, '2015-12-11 16:13:18', null);
INSERT INTO `user` VALUES ('7', '1', 'santhosh', 'kumar', 'rocck_mic@gmail.com', null, '9456456545', 'Hills,500034', 'hyderabad', 'telangana', null, 'jayerowe_1449214410.png', null, '2015-12-11 16:50:22', null);
INSERT INTO `user` VALUES ('8', '1', 'ramesh', 'babu', 'johenpoop@gmail.com', null, '54564512323', 'Hills,500034', 'hyderabad', 'telangana', null, 'Tulips_1449138679.jpg', null, '2015-12-11 16:51:28', null);
INSERT INTO `user` VALUES ('20', '3', 'Sthanka', 'bhasha', 'bhasha.s@thresholdsoft.com', '6787ada72244a6ea1f141b78bdf880fe', '86546546', 'hyderabad', 'hyderabad', 'telangana', null, 'http://localhost/q-lana/rest/uploads/jayerowe_1449214410.png', null, '2015-12-12 17:50:40', null);
INSERT INTO `user` VALUES ('21', '2', 'Mohanbabu', 'Pachipala', 'palaekirimohanbabu@gmail.com', '79c04b93fa7a', '9581565753', 'Hills,500034', 'hyderabad', 'telangana', null, 'Tulips_1449138679.jpg', null, '2015-12-11 16:51:27', null);
INSERT INTO `user` VALUES ('24', '2', 'krishna', 'pvr', 'bhashqa.s@thresholdsoft.com', '6ffb418b1088', '1234567895', null, null, null, null, null, null, '2015-12-11 17:17:18', null);
INSERT INTO `user` VALUES ('39', '2', 'Manasa', 'Thummala', 'manasa.t@thresholdsoft.com', 'bc1d59a847ac8455b6bc76fad5f37a11', '9767676665', 'Nellore', 'null', 'null', null, 'Feng_Chia_University_1449898577.png', null, '2015-12-12 11:47:49', null);
INSERT INTO `user` VALUES ('42', '2', 'Mohanbabu', 'Pachipala', 'mohanbabu1.p@thresholdsoft.com', 'b7baca5535c8', '9581565753', null, null, null, null, '10298703_656786114393056_6107077187498387812_n_1449902115.jpg', null, '2015-12-12 12:08:48', null);
INSERT INTO `user` VALUES ('43', '2', 'Mohanbabu', 'Pachipala', 'mohanbabu.p@thresholdsoft.com', 'e10adc3949ba59abbe56e057f20f883e', '9581565753', null, null, null, null, '10298703_656786114393056_6107077187498387812_n_1449902345.jpg', null, '2015-12-12 12:12:16', null);
INSERT INTO `user` VALUES ('44', '2', 'praveen', 'kumar', 'sthanka.shaik@gmail.com', '45b06af9d0de8e4f8072b8786f2c6d96', '7894561231', null, null, null, null, 'Person-Donald-900x1080_1449902992.jpg', null, '2015-12-12 12:19:42', null);

-- ----------------------------
-- Table structure for `user_role`
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id_user_role` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_user_role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('1', 'superadmin');
INSERT INTO `user_role` VALUES ('2', 'admin');
INSERT INTO `user_role` VALUES ('3', 'user');
