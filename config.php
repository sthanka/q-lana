<?php
error_reporting(E_ALL ^ E_DEPRECATED);
ini_set('display_errors','0');
define('ENV','DEV');
switch(ENV)
{
    case 'DEV':
        $base_host = sprintf('%s://%s/',$_SERVER['SERVER_PORT'] == 443 ? 'https' : 'http',$_SERVER['SERVER_NAME'])."q-lana/";

        /* site urls starts */
        define('WEB_BASE_URL', $base_host);
        define('REST_API_URL', $base_host.'rest/');
        define('PAGING_LIMIT', '10');
        /* site urls ends */

        /* database configuration starts*/
        define('DB_HOST', '192.168.0.4');
        define('DB_USERNAME', 'admin');
        define('DB_PASSWORD', 'the@123');
        define('DB_NAME', 'qlana');
        /* database configuration ends*/

        define('EXCEL_UPLOAD_SIZE','2097152');

        break;

    case 'STAGE':
        $base_host = sprintf('%s://%s/',$_SERVER['SERVER_PORT'] == 443 ? 'https' : 'http',$_SERVER['SERVER_NAME']);

        /* site urls starts */
        define('WEB_BASE_URL', $base_host);
        define('REST_API_URL', $base_host.'rest/');
        define('PAGING_LIMIT', '10');
        /* site urls ends */

        /* database configuration starts*/
        define('DB_HOST', 'localhost');
        define('DB_USERNAME', 'admin');
        define('DB_PASSWORD', 'the@123');
        define('DB_NAME', 'qlana');
        /* database configuration ends*/
        define('EXCEL_UPLOAD_SIZE','2097152');

        break;
}

?>
