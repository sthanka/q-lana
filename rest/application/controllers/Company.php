<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Company extends REST_Controller
{

    public $userNameRules               = array(
                                                'required'=> 'Enter valid user name'
                                                );
    public $firstNameRules              = array(
                                                'required'=> 'Enter valid first name'
                                                );
    public $lastNameRules               = array(
                                                'required'=> 'Enter valid last name'
                                                );
    public $companyNameRules            = array(
                                                'required'=> 'Enter valid company name'
                                                );
    public $emailRules                  = array(
                                                'required'=> 'Email required',
                                                'valid_email' => 'Enter valid email'
                                                );
    public $passwordRules               = array(
                                                'required'=> 'Password required'
                                               );
    public $confirmPasswordRules        = array(
                                                'required'=>'Confirm Password required',
                                                'match_field-password'=>'Password not matched'
                                               );
    public $addressRules                = array(
                                                'required'=> 'Address required',
                                               );
    public $phoneRules                  = array(
                                                'required'=> 'Phone Number required',
                                                'numeric'=>  'Phone number should be numeric',
                                                'min_len-6' => 'Phone no must be minimum 6 digits'
                                            );
    public $companyPhoneRules                  = array(
                                                'required'=> 'Company phone Number required',
                                                'numeric'=>  'Company phone number should be numeric',
                                                'min_len-6' => 'Phone no must be minimum 6 digits'
                                                );
    public $req                         = array(
                                                'required'=> 'Phone Number required'
                                                );
    public $ordered_committee_data      = array();
    public $forward_to                  = '';
    public $ordered_data                = '';
    public $last_node                   = '';
    public $count                       = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Company_model');
        $this->load->model('User_model');
        $this->load->model('Crm_model');
        $this->load->model('Master_model');
        $this->load->model('Activity_model');
        //$this->load->library('common/form_validator');
        //$this->load->library('parser');
    }

    public function companyList_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->companyList($data);
        for($s=0;$s<count($result);$s++){
                $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
                $result[$s]['company_logo'] = getImageUrl($result[$s]['company_logo'],'company');
        }
        $total_records = $this->Company_model->totalCompanyList();
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function company_get($id)
    {
        $data['id'] = $id;
        //validating data
        $idRule = array('required'=> 'id required');
        $this->form_validator->add_rules('id', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Company_model->getCompanyById($id);
        if(!empty($result)){
            $result->profile_image = getImageUrl($result->profile_image,'profile');
            $result->company_logo = getImageUrl($result->company_logo,'company');
            //$result->users = count($this->Company_model->getCompanyUsers(array('company_id' => $result->id_company)));
            //$result->branches = count($this->Company_model->getCompanyBranchByCompanyId($result->id_company));
            //$result->approval_roles = count($this->Company_model->getCompanyApprovalRolesByCompanyId($result->id_company));
            //$result->branch_types = count($this->Company_model->getCompanyBranchTypeByCompanyId($result->id_company));

            $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else{
            $result = array('status'=>FALSE, 'error' => 'No records found', 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }

    public function companyInformation_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required' => 'Company id required'));
        $result = $this->Company_model->getCompanyById($data['company_id']);
        //echo "<pre>"; print_r($result); exit;
        $company = array('id_company' => $result->id_company,'company_name' => $result->company_name,'company_logo' => getImageUrl($result->company_logo,'company'),'country_id' => $result->company_country_id,'bank_category_id' => $result->bank_category_id, 'company_address' => $result->company_address, 'currency_id' => $result->currency_id,'plan_id' => $result->plan_id);
        $user = array('id_user' => $result->id_user,'first_name' => $result->first_name,'last_name' => $result->last_name,'profile_image' => getImageUrl($result->profile_image,'user'),'email_id' => $result->email_id,'phone_number' => $result->phone_number);
        $result = array('company' => $company, 'user' => $user);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function company_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        //echo "<pre>"; print_r($_FILES); exit;
        $this->form_validator->add_rules('email_id', $this->emailRules);
        $this->form_validator->add_rules('phone_number', $this->phoneRules);
        $this->form_validator->add_rules('last_name', $this->lastNameRules);
        $this->form_validator->add_rules('first_name', $this->firstNameRules);
        $this->form_validator->add_rules('company_name', $this->companyNameRules);
        $this->form_validator->add_rules('company_address', array('required'=>'Company address required'));
        $this->form_validator->add_rules('plan_id', array('required'=>'Plan required'));
        $this->form_validator->add_rules('country_id', array('required'=>'Country required'));
        $this->form_validator->add_rules('bank_category_id', array('required'=>'Bank category required'));
        $this->form_validator->add_rules('currency_id', array('required'=>'Currency required'));
        $validated = $this->form_validator->validate(array_merge($data['user'],$data['company']));
        $error = '';
        if($validated != 1)
        {
            if($error!=''){ $validated = array_merge($error,$validated); }
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $company_id = 0;
            if(isset($data['company']['id_company'])){ $company_id = $data['company']['id_company'];  }
            $check_company_name = $this->Company_model->getCompany(array('company_name' => trim($data['company']['company_name']),'id' => $company_id));
            unset($company_id);
            if(!empty($check_company_name))
            {
                $result = array('status'=>FALSE,'error'=>array('company_name' => 'Company name already exists'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['user']['id_user']) && isset($data['company']['id_company']))
        {
            $email_check = $this->User_model->check_email($data['user']['email_id'],'user',$data['user']['id_user']);
            unset($data['user']['profile_image']); unset($data['company']['company_logo']);
        }
        else
        {
            $email_check = $this->User_model->check_email($data['user']['email_id']);
        }

        if(!empty($email_check)){
            $result = array('status'=>FALSE,'error'=>array('email_id'=>'Email already exists'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        //print_r($_FILES); exit;
        $path='uploads/';
        if(isset($_FILES) && !empty($_FILES['file']['name']['profile_image']))
        {
            $imageName = doUpload($_FILES['file']['tmp_name']['profile_image'],$_FILES['file']['name']['profile_image'],$path,'','image');
            if($imageName===0){
                $result = array('status' => FALSE, 'error' => array('image' => 'upload only jpg,png format files only'), 'data' => '');
                $this->response($result, REST_Controller::HTTP_OK); exit;
            }
            $data['user']['profile_image'] = $imageName;
        }
        else{
            $data['user']['profile_image'] = '';
        }

        if(isset($_FILES) && !empty($_FILES['file']['name']['company_logo']))
        {
            $imageName = doUpload($_FILES['file']['tmp_name']['company_logo'],$_FILES['file']['name']['company_logo'],$path,'','image');
            if($imageName===0){
                $result = array('status' => FALSE, 'error' => array('image' => 'upload only jpg,png format files only'), 'data' => '');
                $this->response($result, REST_Controller::HTTP_OK); exit;
            }
            $data['company']['company_logo'] = $imageName;
        }
        else{
            $data['company']['company_logo']='';
        }

        $user_password = generatePassword(6);
        $data['user']['password'] = md5($user_password);
        $data['user']['user_status'] = '0';


        if(isset($data['user']['id_user']) && isset($data['company']['id_company']))
        {
            unset( $data['user']['password']); unset($data['user']['user_status']);
            if($data['user']['profile_image']==''){ unset($data['user']['profile_image']); }
            if($data['company']['company_logo']==''){ unset($data['company']['company_logo']); }

            $this->Company_model->updateUser($data['user']);
            $result = $this->Company_model->updateCompanyInfo($data['company']);
            //moving image to company folder
            if(!file_exists('uploads/'.$data['company']['id_company'])){
                mkdir('uploads/'.$data['company']['id_company']);
            }

            if(isset($data['company']['company_logo']) && $data['company']['company_logo']!='')
            {

                rename('uploads/'.$data['company']['company_logo'], 'uploads/'.$data['company']['id_company'].'/'.$data['company']['company_logo']);
                $this->Company_model->updateCompanyData(array('company_logo' => $data['company']['id_company'].'/'.$data['company']['company_logo']),$data['company']['id_company']);
            }
            if(isset($data['user']['profile_image']) && $data['user']['profile_image']!='')
            {
                rename('uploads/'.$data['user']['profile_image'], 'uploads/'.$data['company']['id_company'].'/'.$data['user']['profile_image']);
                $this->User_model->updateUserData(array('profile_image' => $data['company']['id_company'].'/'.$data['user']['profile_image']),$data['user']['id_user']);
            }


        }
        else
        {
            $data['company']['user_id'] = $this->User_model->createUserInfo($data['user']);
            $result = $this->Company_model->createCompany($data['company']);
            //moving image to company folder
            if(!file_exists('uploads/'.$result)){
                mkdir('uploads/'.$result);
            }
            if(isset($data['user']['profile_image']) && $data['user']['profile_image']!='')
                rename('uploads/'.$data['user']['profile_image'], 'uploads/'.$result.'/'.$data['user']['profile_image']);
            if(isset($data['user']['company_logo']) && $data['user']['company_logo']!='')
                rename('uploads/'.$data['company']['company_logo'], 'uploads/'.$result.'/'.$data['company']['company_logo']);

            $this->Company_model->updateCompanyData(array('company_logo' => $data['company']['company_logo']),$result);
            $user_id = $this->Company_model->createCompanyUser(array('company_id' => $result,'user_id' => $data['company']['user_id']));
            $this->User_model->updateUserData(array('profile_image' => $data['user']['profile_image']),$user_id);

            $template_data = array(
                'base_url' => REST_API_URL,
                'user_name' => $data['user']['first_name'].' '.$data['user']['last_name'],
                'email' => $data['user']['email_id'],
                'password' => $user_password,
                'activation_code' => $this->User_model->encode($data['company']['user_id'])
            );
            $template_data = $this->parser->parse('templates/company.html', $template_data);
            sendmail($data['user']['email_id'],'Q-lana Account Created',$template_data);
        }




        //sendmail($data['user']['email_id'],'Q-lana Account Created','<p>hello '.$data['user']['first_name'].' '.$data['user']['last_name'].', welcome to q-lana,</p><p>Your company created successfully...! your login details are</p><p>Email: '.$data['user']['email_id'].'<br>Password: '.$user_password.'</p>');

        $result = array('status'=>TRUE, 'message' => 'Customer created successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    /*public function company_put()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('company_name', $this->companyNameRules);
        //$this->form_validator->add_rules('company_phone', $this->companyPhoneRules);
        //$this->form_validator->add_rules('phone_number', $this->phoneRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->updateCompany($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }*/

    public function company_delete($id)
    {
       $data['id'] = $id;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('id', $this->req);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->deleteCompany($data);
        $result = array('status'=>TRUE, 'message' => 'Company Inactivated successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function userList_get()
    {
        $data = $this->input->get();

        if(isset($data['id_company_approval_role']) && $data['id_company_approval_role']!=''){
            $company_approval_role = $this->Company_model->getCompanyApprovalRole(array('company_id' => $data['company_id'],'id_company_approval_role' => $data['id_company_approval_role']));
            $data['approval_role_id'] = $company_approval_role[0]['approval_role_id'];
            //print_r($data['approval_role_id']); exit;
            if(isset($data['branch_id']) && $data['branch_id']!=''){
                $reporting_branch = $this->Company_model->getReportingBranch($data['branch_id']);
                $data['allowed_branch_ids'] = array();
                array_push($data['allowed_branch_ids'],$data['branch_id']);
                unset($data['branch_id']);
                if(!empty($reporting_branch)){
                    array_push($data['allowed_branch_ids'],$reporting_branch[0]);
                }

            }
        }
        if(isset($data['approval_role_id'])){
            //echo $data['approval_role_id']; exit;
            $reporting_approval_ids = $this->Company_model->getReportingApprovalRoles($data['company_id'],$data['approval_role_id']);
            //echo "<pre>"; print_r($reporting_approval_ids); exit;
            if(!empty($reporting_approval_ids))
            {
                /*$approval_role_ids = implode(',',$reporting_approval_ids);
                $allowed_approval_role_ids  = '(';
                $allowed_approval_role_ids.= $approval_role_ids;
                $allowed_approval_role_ids.= ')';
                $data['allowed_approval_role_ids'] = $allowed_approval_role_ids;*/
                //echo $allowed_approval_role_ids; exit;
                /*$data['allowed_approval_role_ids'] = '('.$reporting_approval_ids[0].','.$data['id_company_approval_role'].')';*/

                $data['allowed_approval_role_id'] = $reporting_approval_ids[0];
                //print_r($data['allowed_approval_role_ids']); exit;
                unset($data['approval_role_id']);
                $result = $this->Company_model->getCompanyUsers($data);
                //echo "<pre>"; print_r($result); exit;
            }
            else{
                $result = array();
            }

        }
        else{
            $result = $this->Company_model->getCompanyUsers($data);
        }

        for($s=0;$s<count($result);$s++){
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
            $result[$s]['reporting'] = $this->Company_model->getReportingUser($result[$s]['reporting_user_id']);
            //echo "<pre>"; print_r($result[$s]['reporting']); exit;
        }
        $total_records = $this->Company_model->getTotalCompanyUsers($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' => $result,'total_records' => $total_records->total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function user_get($id=0)
    {
        if($id)
        {
            $result = $this->Company_model->getCompanyUser($id);
            if(!empty($result)){
                $result->profile_image = getImageUrl($result->profile_image,'profile');
                $result->branch = $this->Company_model->getBranchName(array('company_id' => $result->company_id));
                $result->userRole = $this->Company_model->getApprovalRole(array('company_id' => $result->company_id));
                $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
            else{
                $result = array('status'=>FALSE, 'error' => 'No records found', 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        else{
            $result = array('status'=>FALSE, 'error' => 'No records found', 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
    }

    public function user_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $companyRules = array(
            'required'=> 'Company id required'
        );
        $userRoleRules = array(
            'required'=> 'User role required'
        );
        $userBranchRules = array(
            'required'=> 'Branch required'
        );
        $this->form_validator->add_rules('email_id', $this->emailRules);
        $this->form_validator->add_rules('phone_number', $this->phoneRules);
        $this->form_validator->add_rules('last_name', $this->lastNameRules);
        $this->form_validator->add_rules('first_name', $this->firstNameRules);
        $this->form_validator->add_rules('company_id', $companyRules);
        $this->form_validator->add_rules('user_role_id', $userRoleRules);
        $this->form_validator->add_rules('branch_id', $userBranchRules);
        $this->form_validator->add_rules('branch_type_id', array('required'=> 'Branch type required'));
        //$this->form_validator->add_rules('reporting_user_id', array('required'=> 'reporting user required'));
        $validated = $this->form_validator->validate($data['user']);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $email_check = $this->User_model->check_email($data['user']['email_id'],'company_user',$data['user']['company_id']);
        //echo "<pre>"; print_r($email_check); exit;
        if(!empty($email_check)){
            if(isset($data['user']['id_user']))
            {
                if($data['user']['id_user'] !=$email_check->id_user){
                    $result = array('status'=>FALSE,'error'=>array('email_id'=>'Email already exists'),'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
            else
            {
                $result = array('status'=>FALSE,'error'=>array('email_id'=>'Email already exists'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        if(!file_exists('uploads/'.$data['user']['company_id'])){
            mkdir('uploads/'.$data['user']['company_id']);
        }

        $path='uploads/';
        if(isset($_FILES) && !empty($_FILES['file']['name']['profile_image']))
        {
            $imageName = doUpload($_FILES['file']['tmp_name']['profile_image'],$_FILES['file']['name']['profile_image'],$path,$data['user']['company_id'],'image');
            if($imageName===0){
                $result = array('status' => FALSE, 'error' => 'upload only jpg,png format files only', 'data' => '');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($imageName==0){
                $result = array('status' => FALSE, 'error' => 'upload only jpg,png format files only', 'data' => '');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $data['user']['profile_image'] = $imageName;
        }
        //echo $data['user']['profile_image']; exit;
        $branch_id = $data['user']['branch_id'];
        $company_id =  $data['user']['company_id'];
        if(isset($data['user']['reporting_user_id']))
             $reporting_user_id =  $data['user']['reporting_user_id'];
        else $reporting_user_id=0;
        $user_role_id = $data['user']['user_role_id'];
        $id_company_user = '';

        unset($data['user']['branch_id']);
        unset($data['user']['company_id']);
        unset($data['user']['reporting_user_id']);
        unset($data['user']['reporting_user_name']);
        unset($data['user']['user_role_id']);
        unset($data['user']['branch_type_id']);
        unset($data['user']['id_branch']);

        $user_password =  generatePassword(6);

        $data['user']['user_role_id'] = 3;
        if(isset($data['user']['id_user']) || isset($data['user']['user_id']))
        {
            $data['user']['profile_image'] = str_replace(REST_API_URL.'uploads/','',$data['user']['profile_image']);

            $user_info = array(
                'id_user' => $data['user']['id_user'],
                'first_name' => $data['user']['first_name'],
                'last_name' => $data['user']['last_name'],
                'email_id' => $data['user']['email_id'],
                'phone_number' => $data['user']['phone_number'],
                'address' => $data['user']['address'],
                'city' => $data['user']['city'],
                'state' => $data['user']['state'],
                'country_id' => $data['user']['country_id'],
                'zip_code' => $data['user']['zip_code'],
                'profile_image' => $data['user']['profile_image']
            );

            $id_company_user = $data['user']['id_company_user'];
            if($data['user']['id_user']){ $user_id = $data['user']['id_user']; }else { $user_id = $data['user']['user_id']; }
            $this->User_model->updateUserInfo($user_info);
            $result = $this->Company_model->updateCompanyUser(array('id_company_user' => $id_company_user, 'company_id' => $company_id, 'user_id' => $user_id, 'company_approval_role_id' => $user_role_id,'branch_id' => $branch_id, 'reporting_user_id' => $reporting_user_id));
        }
        else{
            $data['user']['password'] = md5($user_password);
            $user_id = $this->User_model->createUserInfo($data['user']);
            $result = $this->Company_model->createCompanyUser(array('company_id' => $company_id, 'user_id' => $user_id,'company_approval_role_id' => $user_role_id, 'branch_id' => $branch_id,'reporting_user_id' => $reporting_user_id));
            sendmail($data['user']['email_id'],'Account Created','<p>hello '.$data['user']['first_name'].' '.$data['user']['last_name'].', welcome to q-lana,</p><p>your login details are</p><p>Email: '.$data['user']['email_id'].'<br>Password: '.$user_password.'</p>');
        }


        if(isset($data['user']['id_user']) || isset($data['user']['user_id'])){
            $suc_msg = 'User updated successfully.';
        }
        else{
            $suc_msg = 'User created successfully.';
        }

        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    /*public function user_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('email_id', $this->emailRules);
        $this->form_validator->add_rules('phone_number', $this->phoneRules);
        $this->form_validator->add_rules('last_name', $this->lastNameRules);
        $this->form_validator->add_rules('first_name', $this->firstNameRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $email_check = $this->User_model->check_email($data['email_id]);
        if(!empty($email_check)){
            $result = array('status'=>FALSE,'error'=>'Email already exists','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //print_r($_FILES); exit;
        if(isset($_FILES) && !empty($_FILES['file']['name']['profile_image']))
        {
            $path='uploads/';
            $tmp = $_FILES['file']['tmp_name']['profile_image'];
            $image=$_FILES['file']['name']['profile_image'];
            list($txt, $ext) = explode(".", $image);
            $imageName = $txt."_".time().".".$ext;
            move_uploaded_file($tmp, $path.$imageName);
            $data['profile_image'] = $imageName;
        }

        $user_id = $this->User_model->createUserInfo($data);
        $result = $this->Company_model->createCompanyUser(array('company_id' => $data['company_id'], 'user_id' => $user_id));
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }*/

    public function branches_get($company_id,$branch_type=0)
    {
        $branch_type = $this->Company_model->getBranchTypeByCompanyId($company_id,$branch_type);
        if(count($branch_type) > 1)
            $branch_type = $this->getOrderedData($branch_type,'0','reporting_branch_type_id','branch_type_id');
        $this->ordered_data = '';
        for($s=0;$s<count($branch_type);$s++)
        {
            $branch_type[$s]['branches'] = $this->Company_model->getCompanyBranchesBybranchId($branch_type[$s]['branch_type_id'],$company_id);

            for($r=0;$r<count($branch_type[$s]['branches']);$r++){
                $branch_type[$s]['branches'][$r]['branch_logo'] = getImageUrl($branch_type[$s]['branches'][$r]['branch_logo'],'company');
            }
        }

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$branch_type);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getOrderedData($data,$identifier,$compare_identifier,$id_identifier)
    {
        for($t=0;$t<count($data);$t++){
            if(count($this->ordered_data)==count($data)){  break; }
            if($data[$t][$compare_identifier]==$identifier){
                $this->ordered_data[] = $data[$t];
                $identifier = $data[$t][$id_identifier];
                if(count($this->ordered_data)==count($data)){  break; }
                else{ $this->getOrderedData($data,$identifier,$compare_identifier,$id_identifier); }
            }
        }

        return $this->ordered_data;
    }

    public function branchTypeStructure_get($company_id)
    {
        $result = $this->Company_model->getCompanyBranchTypeStructure($company_id);
        if(count($result) > 1)
            $result = $this->getOrderedData($result,'0','reporting_branch_type_id','branch_type_id');
        $this->ordered_data = '';
        for($s=0;$s<count($result);$s++)
        {
            if($result[$s]['created_by']=='0'){ $result[$s]['is_edit'] = 0; }
            else{ $result[$s]['is_edit'] = 1; }
        }
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function branchDetails_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required' => 'company id required'));
        $this->form_validator->add_rules('branch_id', array('required' => 'branch_id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->getBranchDetails($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['branch_logo'] = getImageUrl($result[$s]['branch_logo'],'company');
        }
        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function branch_get($id)
    {
        $result = $this->Company_model->getCompanyBranch($id);
        $result->branch_logo = getImageUrl($result->branch_logo,'company');
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyBranch_get($company_id,$branch_id='')
    {
        if($branch_id!='' && $branch_id!=0){
            $branch_details = $this->Company_model->getCompanyBranch($branch_id);
            $branch_type = $branch_details->branch_type_id;
            $allowed_branch_type_ids = $this->Company_model->getReportingBranchTypes($company_id,$branch_type);
            //echo "<pre>"; print_r($allowed_branch_type_ids); exit;
            if(!empty($allowed_branch_type_ids)){
                $branch_type_ids = implode(',',$allowed_branch_type_ids);
                $allowed_branch_type_ids  = '(';
                $allowed_branch_type_ids.= $branch_type_ids;
                $allowed_branch_type_ids.= ')';
            }
            else{
                $allowed_branch_type_ids = 0;
            }

            $result = $this->Company_model->getCompanyBranchByCompanyId($company_id,$branch_id,$allowed_branch_type_ids);
        }
        else
        {
            $result = $this->Company_model->getCompanyBranchByCompanyId($company_id,$branch_id);
        }

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyBranchList_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getCompanyBranchListByCompanyId($data);
        for($s=0;$s<count($result);$s++)
        {
            $total_users = $this->Company_model->getCompanyBranchUsersByBranchId($result[$s]['id_branch'],$data['company_id']);
            $result[$s]['branch_users'] = $total_users->total_users;
        }
        $total_records = $this->Company_model->getTotalCompanyBranchByCompanyId($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' => $result,'total_records' => $total_records->total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyBranch_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->clearRules();
        $this->form_validator->add_rules('company_id', array('required' => 'company id required'));
        $this->form_validator->add_rules('branch_type_id', array('required' => 'branch type id required'));
        $this->form_validator->add_rules('country_id', array('required' => 'country required'));
        $this->form_validator->add_rules('legal_name', array('required'=> 'Branch name required'));
        $this->form_validator->add_rules('branch_address', array('required'=> 'Address required'));
        $this->form_validator->add_rules('branch_city', array('required'=> 'City required'));
        //$this->form_validator->add_rules('branch_state', array('required'=> 'State required'));
        $this->form_validator->add_rules('branch_code', array('required'=> 'Branch code required'));
        //$this->form_validator->add_rules('branch_zip_code', array('required'=> 'Zip code required'));
        //$this->form_validator->add_rules('reporting_branch_id', array('required'=> 'Reporting Branch required'));
        if(isset($data['branch']) && $data['branch']!='' && $data['branch']!=0)
            $this->form_validator->add_rules('branch_phone_number', array('min_len-6' => 'Phone no must be minimum 6 digits','numeric' => 'Phone number must be numeric'));
        //echo "<pre>"; print_r($data); exit;
        $validated = $this->form_validator->validate($data['branch']);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else{
            if(isset($data['branch']['branch_email']) && trim($data['branch']['branch_email'])!='') {
                $company_branch_id = 0;
                if(isset($data['branch']['id_branch'])){ $company_branch_id = $data['branch']['id_branch']; }
                $check_email = $this->User_model->check_email($data['branch']['branch_email'],'company_branch',$company_branch_id);
                if(!empty($check_email)){
                    $result = array('status'=>FALSE,'error'=>array('branch_email' => 'Email already exists'),'data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }
        }
        unset($data['branch']['id_branch_type']);
        unset($data['branch']['branch_type_name']);

        $path='uploads/';
        if(isset($_FILES) && !empty($_FILES['file']['name']['branch_logo']))
        {
            $imageName = doUpload($_FILES['file']['tmp_name']['branch_logo'],$_FILES['file']['name']['branch_logo'],$path,$data['branch']['company_id'],'image');
            if($imageName===0){
                $result = array('status' => FALSE, 'error' => 'upload only jpg,png format files only', 'data' => '');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($imageName===0){
                $result = array('status' => FALSE, 'error' => 'upload only jpg,png format files only', 'data' => '');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $data['branch']['branch_logo'] = $imageName;
        }

        if(isset($data['branch']['id_branch']))
        {
            if(!isset($data['branch']['reporting_branch_id']) || empty($data['branch']['reporting_branch_id'])) $data['branch']['reporting_branch_id'] =0;
            $fields = array(
                        'company_id' => $data['branch']['company_id'],
                        'branch_type_id' => $data['branch']['branch_type_id'],
                        'country_id' => $data['branch']['country_id'],
                        'legal_name' => $data['branch']['legal_name'],
                        'branch_address' => $data['branch']['branch_address'],
                        'branch_city' => $data['branch']['branch_city'],
                        'branch_state' => $data['branch']['branch_state'],
                        'branch_code' => $data['branch']['branch_code'],
                        'reporting_branch_id' => $data['branch']['reporting_branch_id'],
                        'branch_logo' => $data['branch']['branch_logo'],
                        'branch_status' => $data['branch']['branch_status'],
                        'branch_zip_code' => $data['branch']['branch_zip_code'],
                        'branch_phone_number' => $data['branch']['branch_phone_number'],
                        'branch_email' => $data['branch']['branch_email']
            );

            $branch_id = $data['branch']['id_branch'];
            unset($data['branch']['id_branch']);
            $fields['branch_logo'] = str_replace(REST_API_URL.'uploads/','',$fields['branch_logo']);
            $result = $this->Company_model->updateCompanyBranch($fields,$branch_id);
            $data['branch']['id_branch'] = $branch_id;
        }
        else
        {
            $result = $this->Company_model->createCompanyBranch($data['branch']);
        }
        if(isset($data['branch']['id_branch'])){
            $suc_msg = 'Branch updated successfully';
        }
        else{
            $suc_msg = 'Branch created successfully';
        }

        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function branchType_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required' => 'company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->getCompanyBranchType($data);
        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function branchType_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //echo "<pre>"; print_r($data); exit;
        if(!isset($data['id_branch_type']) && (!isset($data['branch_type_name']) || $data['branch_type_name']==''))
            $this->form_validator->add_rules('id_branch_type', array('required' => 'Branch type required'));
        if((!$data['is_primary'] || $data['is_primary']==false || $data['is_primary']==0 ) && ((!isset($data['reporting_branch_type_id']) || $data['reporting_branch_type_id']=='' || $data['reporting_branch_type_id']==0))) {
            unset($data['reporting_branch_type_id']);
            $this->form_validator->add_rules('reporting_branch_type_id', array('required' => 'Reporting branch type required'));
        }
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_branch_type']) && isset($data['reporting_branch_type_id']))
        {
            if($data['id_branch_type']==$data['reporting_branch_type_id']){
                $result = array('status'=>FALSE,'error'=>array('reporting_branch_type_id' => 'You can not report to same branch type'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }


        if(!isset($data['branch_type_code'])){ $data['branch_type_code']=''; }
        if(!isset($data['description'])){ $data['description']=''; }

        //check branch_type_id in company_branch_type_id
        if(isset($data['id_branch_type']))
        {
            $check = $this->Company_model->getCompanyBranchTypeByBranchId($data['company_id'],$data['id_branch_type']);
            if(empty($check)){
                $total_company_branch_types = $this->Company_model->getCompanyBranchTypeByCompanyId($data['company_id']);
                $ids = array();
                foreach($total_company_branch_types as $item){ $ids[] = $item['reporting_branch_type_id']; }
                $total_company_branch_types_str = '';
                if(count($ids) > 0)
                {
                    $total_company_branch_types_str = '(';
                    $total_company_branch_types_str .= implode(',',$ids);
                    $total_company_branch_types_str .= ')';
                }
                $last_node = $this->Company_model->getLastNodeOfCompanyBranchType($data['company_id'],$total_company_branch_types_str);
                $reporting_to = (!empty($last_node)) ? $last_node[0]['branch_type_id'] : 0;
                $this->Company_model->createCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $data['id_branch_type'], 'reporting_branch_type_id' => $reporting_to));
            }
        }


        //primary, checking nodes
        $current_primary = array();
        if($data['is_primary']){
            $current_primary = $this->Company_model->getPrimaryCompanyBranchType(array('company_id' => $data['company_id']));
            if(isset($data['id_branch_type'])){
                $current_updating_branch_type = $this->Company_model->getCompanyBranchTypeByBranchId($data['company_id'], $data['id_branch_type']);
                $branch_type_has_current_branch_as_reporting = $this->Company_model->getCompanyBranchTypeAsReportingBranchType($data['company_id'], $data['id_branch_type']);
            }
        }
        else{
            //$branch_type_has_current_branch_as_reporting = $this->Company_model->getCompanyBranchTypeAsReportingBranchType($data['company_id'], $data['id_branch_type']);
            if(isset($data['id_branch_type'])){
                    $current_updating_branch_type = $this->Company_model->getCompanyBranchTypeByBranchId($data['company_id'], $data['id_branch_type']);
                    $branch_type_has_current_branch_as_reporting = $this->Company_model->getCompanyBranchTypeAsReportingBranchType($data['company_id'], $data['id_branch_type']);
                    //checking not updating reporting_id
                    if(isset($current_updating_branch_type[0]['reporting_branch_type_id']) && ($current_updating_branch_type[0]['reporting_branch_type_id'] == $data['reporting_branch_type_id']))
                    {
                        unset($current_updating_branch_type);
                        unset($branch_type_has_current_branch_as_reporting);
                    }
            }
            $branch_type_has_updating_branch_as_reporting = $this->Company_model->getCompanyBranchTypeAsReportingBranchType($data['company_id'], $data['reporting_branch_type_id']);
        }
        //checking nodes

        //update
        if(isset($data['id_branch_type']))
        {


            $branch_type_id = $data['id_branch_type'];
            if(isset($data['branch_type_name']) && $data['branch_type_name']==''){
                $result = array('status'=>FALSE,'error'=>array('branch_type_name' => 'Invalid branch type'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            if($data['is_primary']){ $report_to = 0; } else{ $report_to = $data['reporting_branch_type_id']; }

            if(isset($data['branch_type_name']))
                $this->Company_model->updateBranchType(array('id_branch_type' => $data['id_branch_type'],'branch_type_name' => $data['branch_type_name'], 'branch_type_code' => $data['branch_type_code'], 'description' => $data['description']));
            //echo $data['company_id'].'---'.$branch_type_id.'---'.$report_to; exit;
            if($branch_type_id != $report_to)
                $this->Company_model->updateCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $branch_type_id, 'reporting_branch_type_id' => $report_to));
            $suc_msg = 'Branch type updated successfully.';
            //updating reporting branch types

            if($data['is_primary']){
                if(isset($current_primary[0]['branch_type_id']) && $current_primary[0]['branch_type_id'] != $branch_type_id){
                    $this->Company_model->updateCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $current_primary[0]['branch_type_id'], 'reporting_branch_type_id' => $branch_type_id));
                    if(isset($branch_type_has_current_branch_as_reporting) && !empty($branch_type_has_current_branch_as_reporting))
                        $this->Company_model->updateCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $branch_type_has_current_branch_as_reporting[0]['branch_type_id'], 'reporting_branch_type_id' => $current_updating_branch_type[0]['reporting_branch_type_id']));
                }

                //updating branch type which has current branch as reporting

            }
            else{
                if(isset($branch_type_has_current_branch_as_reporting) && !empty($branch_type_has_current_branch_as_reporting)){
                    $this->Company_model->updateCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $branch_type_has_current_branch_as_reporting[0]['branch_type_id'], 'reporting_branch_type_id' => $current_updating_branch_type[0]['reporting_branch_type_id']));
                }
                if(isset($branch_type_has_updating_branch_as_reporting) && !empty($branch_type_has_updating_branch_as_reporting) && isset($current_updating_branch_type) && !empty($current_updating_branch_type)){
                    $this->Company_model->updateCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $branch_type_has_updating_branch_as_reporting[0]['branch_type_id'], 'reporting_branch_type_id' => $current_updating_branch_type[0]['branch_type_id']));
                }
            }
        }
        else if(isset($data['branch_type_name']))
        {
            if($data['is_primary']){ $report_to = 0; } else{ $report_to = $data['reporting_branch_type_id']; }
            if($data['branch_type_name']==''){
                $result = array('status'=>FALSE,'error'=>array('branch_type_name' => 'Invalid branch type'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $branch_type_id = $this->Company_model->addBranchType(array('branch_type_name' => $data['branch_type_name'],'branch_type_code' => $data['branch_type_name'], 'description' => $data['description'], 'company_id' => $data['company_id']));
            //echo $data['company_id']."---".$branch_type_id.'---'.$report_to; exit;
            $this->Company_model->createCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $branch_type_id, 'reporting_branch_type_id' => $report_to));
            $suc_msg = 'Branch type added successfully.';
            //updating reporting branch types
            if($data['is_primary']){
                if(!empty($current_primary))
                    $this->Company_model->updateCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $current_primary[0]['branch_type_id'], 'reporting_branch_type_id' => $branch_type_id));
            }
            else{
                if(isset($branch_type_has_current_branch_as_reporting) && !empty($branch_type_has_current_branch_as_reporting))
                    $this->Company_model->updateCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $branch_type_has_current_branch_as_reporting[0]['branch_type_id'], 'reporting_branch_type_id' => $branch_type_id));
                if(isset($branch_type_has_updating_branch_as_reporting) && !empty($branch_type_has_updating_branch_as_reporting)){
                    if(!isset($current_updating_branch_type) && !empty($current_updating_branch_type)){ $branch_type_id = $current_updating_branch_type[0]['branch_type_id']; }
                    $this->Company_model->updateCompanyBranchType(array('company_id' => $data['company_id'], 'branch_type_id' => $branch_type_has_updating_branch_as_reporting[0]['branch_type_id'], 'reporting_branch_type_id' => $branch_type_id));
                }
            }
        }
        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyBranchType_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required' => 'company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['branch_type_id']) && $data['branch_type_id']!=0 && $data['branch_type_id']!=''){
            $allowed_branch_type_ids = $this->Company_model->getReportingBranchTypes($data['company_id'],$data['branch_type_id']);
            array_push($allowed_branch_type_ids,$data['branch_type_id']);
            if(!empty($allowed_branch_type_ids)){
                $branch_type_ids = implode(',',$allowed_branch_type_ids);
                $allowed_branch_type_ids  = '(';
                $allowed_branch_type_ids.= $branch_type_ids;
                $allowed_branch_type_ids.= ')';
            }
            else{
                $allowed_branch_type_ids = 0;
            }
            if($allowed_branch_type_ids){
                $result = $this->Company_model->getCompanyBranchTypeByCompanyId($data['company_id'],$allowed_branch_type_ids);
            }
            else
                $result = array();
        }
        else
        {
            $result = $this->Company_model->getCompanyBranchTypeByCompanyId($data['company_id']);
        }


        $result = array('status'=>TRUE, 'message' => 'Success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyBranches_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required' => 'company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $branch_id = $branch_type_details = $allowed_branch_type_ids = '';

        if(isset($data['branch_id']) && $data['branch_id']!='' && $data['branch_id']!='undefined'){ $branch_id = $data['branch_id']; }

        if(isset($data['branch_type_id'])){
            $allowed_branch_type_ids = $this->Company_model->getReportingBranchTypes($data['company_id'],$data['branch_type_id']);
            //echo "<pre>"; print_r($allowed_branch_type_ids); exit;
            array_push($allowed_branch_type_ids,$data['branch_type_id']);
            if(!empty($allowed_branch_type_ids)){
                $branch_type_ids = implode(',',$allowed_branch_type_ids);
                $allowed_branch_type_ids  = '(';
                $allowed_branch_type_ids.= $branch_type_ids;
                $allowed_branch_type_ids.= ')';
            }
            else{
                $allowed_branch_type_ids = 0;
            }

            $branch_type_details = $this->Company_model->getBranchTypeById($data['branch_type_id']);
            if($allowed_branch_type_ids){
                $result = $this->Company_model->getCompanyBranchByCompanyId($data['company_id'],$branch_id,$allowed_branch_type_ids);
            }
            else
                $result = array();
        }
        else
        {
            $result = $this->Company_model->getCompanyBranchByCompanyId($data['company_id'],$branch_id);
        }

        $result = array('status'=>TRUE, 'message' => 'Success', 'data'=>array('data' => $result,'branch_type' =>$branch_type_details));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyBranchType_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $company_id = array('required'=> 'Company id required');
        $this->form_validator->add_rules('company_id', $company_id);
        $validated = $this->form_validator->validate($data);

        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $company_id = $data['company_id'];
        $data = $data['data'];
        for($s=0;$s<count($data);$s++)
        {
            $this->Company_model->createCompanyBranchType(array('company_id' => $company_id,'branch_type_id' => $data[$s]['branch_type_id'],'reporting_branch_type_id' => $data[$s]['reporting_branch_type_id']));
        }

        $result = array('status'=>TRUE, 'message' => 'Branch Type created successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function branchUsers_get()
    {
        $data = $this->input->get();
        $data['branch_id'] = 0;
        if(isset($data['project_id'])){
            $project_details = $this->Crm_model->getProject($data['project_id']);
            $data['user_id'] = $project_details[0]['created_by'];
        }

        if(isset($data['user_id'])){
            $company_user = $this->Company_model->getCompanyUser($data['user_id']);
            if(empty($company_user))
            {
                $result = array('status'=>TRUE, 'message' => 'success', 'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK); exit;
            }
            $data['branch_id'] = $company_user->branch_id;
        }

        $result = $this->Company_model->getBranchUsers($data['branch_id']);
        $allBranchUsers = [];
        for($i=0; $i<count($result);$i++)
        {
            $branchUsers = $result[$i];
            for($u=0; $u<count($branchUsers); $u++)
            {
                $allBranchUsers[] = $result[$i][$u];
            }
        }
        $result = $allBranchUsers;
        for($s=0;$s<count($result);$s++){
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
        }
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalRole_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo "<pre>"; print_r($data); exit;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //$data['id_approval_role'] = $data['approval_role_id'];
        if(!isset($data['approval_role_code'])){ $data['approval_role_code']=''; }
        if(!isset($data['description'])){ $data['description']=''; }


        if(!isset($data['id_approval_role']) && (!isset($data['approval_name']) || $data['approval_name']==''))
            $this->form_validator->add_rules('id_approval_role', array('required' => 'Approval role required'));
        if(isset($data['id_approval_role']) && isset($data['approval_name']) && trim($data['approval_name'])=='')
            $this->form_validator->add_rules('approval_name', array('required' => 'Approval Name required'));
        if(!$data['is_primary'] && (!isset($data['reporting_id']) || $data['reporting_id']!=''))
            $this->form_validator->add_rules('reporting_role_id', array('required' => 'Reporting role required'));
        $this->form_validator->add_rules('company_id', array('required' => 'Company id required'));
        $this->form_validator->add_rules('branch_type_id', array('required' => 'Branch type required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_approval_role']))
        {
            if(isset($data['reporting_role_id']) && $data['id_approval_role']==$data['reporting_role_id']){
                $result = array('status'=>FALSE,'error'=>array('reporting_id' => 'You can not report to same approval role'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }


        //check branch_type_id in company_branch_type_id
        if(isset($data['id_approval_role']))
        {
            $check = $this->Company_model->getCompanyApprovalRoleByApprovalId($data['company_id'],$data['id_approval_role']);
            if(empty($check)){
                $total_company_approval_roles = $this->Company_model->getCompanyApprovalRolesByCompanyId($data['company_id']);
                $ids = array();
                foreach($total_company_approval_roles as $item){ $ids[] = $item['reporting_role_id']; }
                $total_company_approval_roles_str = '';
                if(count($ids) > 0)
                {
                    $total_company_approval_roles_str = '(';
                    $total_company_approval_roles_str .= implode(',',$ids);
                    $total_company_approval_roles_str .= ')';
                }
                $last_node = $this->Company_model->getLastNodeOfCompanyApprovalRole($data['company_id'],$total_company_approval_roles_str);
                $reporting_role_id = (!empty($last_node)) ? $last_node[0]['approval_role_id'] : 0;
                $this->Company_model->createCompanyApprovalRole(array('company_id' => $data['company_id'], 'branch_type_id' => $data['branch_type_id'], 'approval_role_id' => $data['id_approval_role'], 'reporting_role_id' => $reporting_role_id));
            }
        }

        //echo "<pre>"; print_r($data); exit;



        //primary, checking nodes
        $current_primary = array();
        if($data['is_primary']){
            $current_primary = $this->Company_model->getPrimaryCompanyApprovalRole(array('company_id' => $data['company_id']));
            if(isset($data['id_approval_role'])){
                $current_updating_branch_type = $this->Company_model->getCompanyApprovalRoleByApprovalRoleId($data['company_id'], $data['id_approval_role']);
                $branch_type_has_current_branch_as_reporting = $this->Company_model->getCompanyApprovalRoleAsReportingApprovalRole($data['company_id'], $data['id_approval_role']);
            }
        }
        else{
            //$branch_type_has_current_branch_as_reporting = $this->Company_model->getCompanyBranchTypeAsReportingBranchType($data['company_id'], $data['id_branch_type']);
            if(isset($data['id_approval_role'])){
                $current_updating_branch_type = $this->Company_model->getCompanyApprovalRoleByApprovalRoleId($data['company_id'], $data['id_approval_role']);
                $branch_type_has_current_branch_as_reporting = $this->Company_model->getCompanyApprovalRoleAsReportingApprovalRole($data['company_id'], $data['id_approval_role']);
                //checking not updating reporting_id
                if($current_updating_branch_type[0]['reporting_role_id'] == $data['reporting_role_id'])
                {
                    unset($current_updating_branch_type);
                    unset($branch_type_has_current_branch_as_reporting);
                }
            }
            $branch_type_has_updating_branch_as_reporting = $this->Company_model->getCompanyApprovalRoleAsReportingApprovalRole($data['company_id'], $data['reporting_role_id']);
        }
        //checking nodes

        //update
        if(isset($data['id_approval_role']))
        {
            $approval_role_id = $data['id_approval_role'];
            /*if($data['approval_name']==''){
                $result = array('status'=>FALSE,'error'=>array('approval_name' => 'Invalid approval name'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }*/
            if($data['is_primary']){ $report_to = 0; } else{ $report_to = $data['reporting_role_id']; }

            if(isset($data['approval_name']))
                $this->Company_model->updateApprovalRole(array('id_approval_role' => $data['id_approval_role'],'approval_name' => $data['approval_name'], 'approval_role_code' => $data['approval_role_code'], 'description' => $data['description']));
            //echo $data['company_id'].'---'.$branch_type_id.'---'.$report_to; exit;
            $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'branch_type_id' => $data['branch_type_id'], 'approval_role_id' => $approval_role_id, 'reporting_role_id' => $report_to));
            $suc_msg = 'Approval role updated successfully.';
            //updating reporting branch types
            if($data['is_primary']){
                if(isset($current_primary[0]['approval_role_id']) && $current_primary[0]['approval_role_id'] != $approval_role_id)
                    $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'approval_role_id' => $current_primary[0]['approval_role_id'], 'reporting_role_id' => $approval_role_id));
                //updating branch type which has current branch as reporting

                if(isset($branch_type_has_current_branch_as_reporting) && !empty($branch_type_has_current_branch_as_reporting))
                    $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'approval_role_id' => $branch_type_has_current_branch_as_reporting[0]['approval_role_id'], 'reporting_role_id' => $current_updating_branch_type[0]['reporting_role_id']));
            }
            else{
                if(isset($branch_type_has_current_branch_as_reporting) && !empty($branch_type_has_current_branch_as_reporting)){
                    $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'approval_role_id' => $branch_type_has_current_branch_as_reporting[0]['approval_role_id'], 'reporting_role_id' => $current_updating_branch_type[0]['reporting_role_id']));
                }
                if(isset($branch_type_has_updating_branch_as_reporting) && !empty($branch_type_has_updating_branch_as_reporting) && isset($current_updating_branch_type) && !empty($current_updating_branch_type)){
                    $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'approval_role_id' => $branch_type_has_updating_branch_as_reporting[0]['approval_role_id'], 'reporting_role_id' => $current_updating_branch_type[0]['approval_role_id']));
                }
            }
        }
        else if(isset($data['approval_name']))
        {
            if($data['is_primary']){ $report_to = 0; } else{ $report_to = $data['reporting_role_id']; }
            if($data['approval_name']==''){
                $result = array('status'=>FALSE,'error'=>array('approval_name' => 'Invalid approval name'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $approval_role_id = $this->Company_model->addApprovalRole(array('approval_name' => $data['approval_name'],'approval_role_code' => $data['approval_role_code'], 'description' => $data['description'], 'company_id' => $data['company_id']));
            //echo $data['company_id']."---".$branch_type_id.'---'.$report_to; exit;
            $this->Company_model->createCompanyApprovalRole(array('company_id' => $data['company_id'], 'branch_type_id' => $data['branch_type_id'], 'approval_role_id' => $approval_role_id, 'reporting_role_id' => $report_to));
            $suc_msg = 'Approval role added successfully.';
            //updating reporting branch types
            if($data['is_primary']){
                if(isset($current_primary[0]['approval_role_id']))
                    $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'approval_role_id' => $current_primary[0]['approval_role_id'], 'reporting_role_id' => $approval_role_id));
            }
            else{
                if(isset($branch_type_has_current_branch_as_reporting) && !empty($branch_type_has_current_branch_as_reporting))
                    $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'approval_role_id' => $branch_type_has_current_branch_as_reporting[0]['approval_role_id'], 'reporting_role_id' => $approval_role_id));
                if(isset($branch_type_has_updating_branch_as_reporting) && !empty($branch_type_has_updating_branch_as_reporting)){
                    if(!isset($current_updating_branch_type) && !empty($current_updating_branch_type)){ $approval_role_id = $current_updating_branch_type[0]['approval_role_id']; }
                    $this->Company_model->updateCompanyApprovalRoleByRoleId(array('company_id' => $data['company_id'], 'approval_role_id' => $branch_type_has_updating_branch_as_reporting[0]['approval_role_id'], 'reporting_role_id' => $approval_role_id));
                }
            }
        }
        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);

        /*$data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required' => 'Company id required'));
        $this->form_validator->add_rules('approval_role_id', array('required' => 'Approval role id required'));
        $this->form_validator->add_rules('reporting_role_id', array('required' => 'Reporting role id required'));
        $this->form_validator->add_rules('branch_type_id', array('required' => 'Branch type id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['id_company_approval_role'])){
            $check_approval_role_exists = $this->Company_model->getCompanyApprovalRole(array('approval_role_id' => $data['approval_role_id'], 'company_id' => $data['company_id']));
            if(!empty($check_approval_role_exists)){
                $result = array('status'=>FALSE,'error'=>'approval_role_id already exists','data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        if(isset($data['id_company_approval_role']))
        {
            $this->Company_model->updateCompanyApprovalRole(array('branch_type_id' => $data['branch_type_id'], 'id_company_approval_role' => $data['id_company_approval_role']));
            $company_approval_role_id = $data['id_company_approval_role'];
            $suc_msg = 'Approval role updated successfully.';
        }
        else
        {
            $company_approval_role_id = $this->Company_model->createCompanyApprovalRole(array('company_id' => $data['company_id'],'approval_group_id' => 1,'approval_role_id' => $data['approval_role_id'],'reporting_role_id' => $data['reporting_role_id'],'branch_type_id' => $data['branch_type_id']));
            $suc_msg = 'Approval role created successfully.';
        }

        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>$company_approval_role_id);
        $this->response($result, REST_Controller::HTTP_OK);*/
    }

    public function companyApprovalRole_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getCompanyApprovalRole($data);
        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvals_get($company_id)
    {
        $approval_role = $this->Company_model->getApprovalRoleByCompanyId($company_id);
        if(count($approval_role) > 1)
            $approval_role = $result = $this->getOrderedData($approval_role,'0','reporting_role_id','approval_role_id');
        $this->ordered_data = '';
        for($s=0;$s<count($approval_role);$s++)
        {
            $approval_role[$s]['users'] = $this->Company_model->getCompanyUsersByApprovalRole($approval_role[$s]['company_approval_role_id'],$company_id);
            for($r=0;$r<count($approval_role[$s]['users']);$r++){
                $approval_role[$s]['users'][$r]['profile_image'] = getImageUrl($approval_role[$s]['users'][$r]['profile_image'],'profile');
            }
        }

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$approval_role);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalTypeStructure_get($company_id)
    {
        $result = $this->Company_model->getCompanyApprovalTypeStructure($company_id);
        for($s=0;$s<count($result);$s++)
        {
            if($result[$s]['is_edit']=='0'){ $result[$s]['is_edit'] = 0; }
            else{ $result[$s]['is_edit'] = 1; }
        }
        //echo count($result)."<pre>"; print_r($result);
        if(count($result) > 1)
            $result = $this->getOrderedData($result,'0','reporting_role_id','approval_role_id');
        //echo '---'.count($result)."<pre>"; print_r($result); exit;
        $this->ordered_data = '';
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function downloadBranchExcel_get()
    {
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>REST_API_URL.'Classes/company_branches.xls');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function downloadUserExcel_get()
    {
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>REST_API_URL.'Classes/company_users.xls');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function uploadBranch_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(!isset($data['company_id']) || $data['company_id']==''){
            $result = array('status'=>FALSE,'error'=>'Invalid data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_id = $data['company_id'];
        if(isset($_FILES) && !empty($_FILES['excel']['name'])){
            $file_name = $_FILES['excel']['name'];
            $allowed =  array('xls','xlsx');
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                $result = array('status'=>FALSE,'error'=>'upload only xls,xlsx format files only','data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $result = array('status'=>FALSE,'error'=>'Please upload excel','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($_FILES['excel']['size']>EXCEL_UPLOAD_SIZE)
        {
            $result = array('status'=>FALSE,'error'=>'Maximum '.EXCEL_UPLOAD_SIZE.' files allowed','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $headers = array('legal_name','country','state','city','address','reporting_branch','branch_type');
        $excel_headers = array_values(excelHeaders($_FILES['excel']['tmp_name'], true));

        if($headers!=$excel_headers){
            $result = array('status'=>FALSE,'error'=>'Excel headers does not match','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $data = excelToArray($_FILES['excel']['tmp_name'], true);

        if(empty($data)){
            $result = array('status'=>FALSE, 'error' => 'Excel is empty', 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $errors = array();
        for($s=0;$s<count($data);$s++)
        {
            $errors[$s]['row'] = ($s+2);
            $this->form_validator->clearRules();
            $this->form_validator->add_rules('legal_name', array('required'=> 'Legal name required'));
            $this->form_validator->add_rules('country', array('required'=> 'Country required'));
            $this->form_validator->add_rules('state', array('required'=> 'State required'));
            $this->form_validator->add_rules('city', array('required'=> 'City required'));
            $this->form_validator->add_rules('address', array('required'=> 'Legal name required'));
            $this->form_validator->add_rules('reporting_branch', array('required'=> 'Reporting Branch required'));
            $this->form_validator->add_rules('branch_type', array('required'=> 'Branch Type required'));
            $validated = $this->form_validator->validate($data[$s]);
            if($validated != 1)
            {
                $errors[$s]['errors'] = $validated;
            }
            else
            {
                $country = $this->Company_model->getCountryByName(strtolower($data[$s]['country']));
                $reporting_branch = $this->Company_model->getCompanyBranchByName(strtolower($data[$s]['reporting_branch']));
                //$legal_name = $this->Company_model->getCompanyBranchByName(strtolower($data[$s]['legal_name']));
                $branch_type = $this->Company_model->getCompanyBranchTypeByName(strtolower($data[$s]['branch_type']),$company_id);
                if(empty($country)){
                    $errors[$s]['errors']['country'] = 'invalid country';
                }
                else{
                    unset($data[$s]['country']);
                    $data[$s]['country_id'] = $country->id_country;
                }
                if(empty($reporting_branch)){
                    $errors[$s]['errors']['reporting_branch'] = 'invalid Reporting branch';
                }
                else{
                    unset($data[$s]['reporting_branch']);
                    $data[$s]['reporting_branch_id'] = $reporting_branch->id_branch;
                }
                /*if(!empty($legal_name))
                {
                    $errors[$s]['errors']['legal_name'] = 'Legal name already exists';
                    unset($data[$s]['legal_name']);
                }*/

                if(empty($branch_type)){
                    $errors[$s]['errors']['branch_type'] = 'invalid Branch type';
                }
                else{
                    unset($data[$s]['branch_type']);
                    $data[$s]['branch_type_id'] = $branch_type->id_branch_type;
                }

                if(isset($data[$s]['country_id']) && isset($data[$s]['reporting_branch_id']) && isset($data[$s]['branch_type_id'])){ //&& isset($data[$s]['legal_name'])
                    $data[$s]['branch_state'] = $data[$s]['state'];
                    $data[$s]['branch_city'] = $data[$s]['city'];
                    $data[$s]['branch_address'] = $data[$s]['address'];
                    unset($data[$s]['state']); unset($data[$s]['city']); unset($data[$s]['address']);

                    $data[$s]['company_id'] = $company_id; //change to dynamic company_id
                    $this->Company_model->createCompanyBranch($data[$s]);
                    $errors[$s]['message'] = 'Successfully added';
                }
            }
        }

        //$result = array('status'=>True, 'message' => 'success', 'data'=>$errors);
        $result = array('status'=>True, 'message' => 'Branches uploaded successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function uploadUser_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(!isset($data['id_company']) || $data['id_company']==''){
            $result = array('status'=>FALSE,'error'=>'Invalid data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_id = $data['id_company'];
        if(isset($_FILES) && !empty($_FILES['excel']['name'])){
            $file_name = $_FILES['excel']['name'];
            $allowed =  array('xls','xlsx');
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                $result = array('status'=>FALSE,'error'=>'upload only xls,xlsx format files only','data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $result = array('status'=>FALSE,'error'=>'Please upload excel file','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($_FILES['excel']['size']>EXCEL_UPLOAD_SIZE)
        {
            $result = array('status'=>FALSE,'error'=>'Maximum 2MB files allowed','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $headers = array('first_name','last_name','email','phone_number','address','user_role','reporting_to','branch');
        $excel_headers = array_values(excelHeaders($_FILES['excel']['tmp_name'], true));

        if($headers!=$excel_headers){
            $result = array('status'=>FALSE,'error'=>'Excel headers does not match','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $data = excelToArray($_FILES['excel']['tmp_name'], true);
        //echo "<pre>"; print_r($data); exit;

        if(empty($data)){
            $result = array('status'=>FALSE, 'error' => 'Excel is empty', 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $errors = array();
        for($s=0;$s<count($data);$s++)
        {
            $errors[$s]['row'] = ($s+2);
            $this->form_validator->clearRules();
            $this->form_validator->add_rules('first_name', $this->firstNameRules);
            $this->form_validator->add_rules('last_name', $this->lastNameRules);
            $this->form_validator->add_rules('email', $this->emailRules);
            $this->form_validator->add_rules('phone_number', $this->phoneRules);
            $this->form_validator->add_rules('address', array('required'=> 'address required'));
            $this->form_validator->add_rules('user_role', array('required'=> 'User role required'));
            $this->form_validator->add_rules('reporting_to', array('required'=> 'Reporting_to required'));
            $this->form_validator->add_rules('branch', array('required'=> 'Branch required'));
            $validated = $this->form_validator->validate($data[$s]);
            if($validated != 1)
            {
                $errors[$s]['errors'] = $validated;
            }
            else
            {
                $email_status = 0;
                $email_check = $this->User_model->check_email(strtolower($data[$s]['email']));
                $user_role = $this->Company_model->getCompanyApprovalRoleByName(strtolower($data[$s]['user_role']),$company_id);
                $branch = $this->Company_model->getCompanyBranchByName(strtolower($data[$s]['branch']));
                $reporting_to = $this->User_model->check_email(strtolower($data[$s]['reporting_to']));

                if(!empty($email_check)){
                    $errors[$s]['errors']['email'] = 'Email already exists';
                    $email_status = 1;
                }
                else{
                    $data[$s]['email_id'] = $data[$s]['email'];
                    unset($data[$s]['email']);
                }

                if(empty($user_role)){
                    $errors[$s]['errors']['user_role'] = 'invalid user role';
                }
                else{
                    unset($data[$s]['user_role']);
                    $data[$s]['company_approval_role_id'] = $user_role->id_company_approval_role;
                }

                if(empty($branch)){
                    $errors[$s]['errors']['branch'] = 'invalid Branch';
                }
                else{
                    unset($data[$s]['branch']);
                    $data[$s]['branch_id'] = $branch->id_branch;
                    $data[$s]['country_id'] = $branch->country_id;
                }

                if(empty($reporting_to)){
                    $errors[$s]['errors']['reporting_to'] = 'invalid reporting_to';
                }
                else{
                    unset($data[$s]['reporting_to']);
                    $data[$s]['reporting_user_id'] = $reporting_to->id_user;
                }
                /*echo 'email'.$data[$s]['email_id'];
                echo 'app'.$data[$s]['company_approval_role_id'];
                echo 'branch'.$data[$s]['branch_id'];
                echo 'report'.$data[$s]['reporting_user_id'];*/
                if($email_status==0 && isset($data[$s]['email_id']) && isset($data[$s]['company_approval_role_id']) && isset($data[$s]['branch_id']) && isset($data[$s]['reporting_user_id']))
                {
                    $data[$s]['user_role_id'] = 3;
                    //$company_id = 1; //change to dynamic company_id
                    $company_approval_role_id = $data[$s]['company_approval_role_id'];
                    $branch_id = $data[$s]['branch_id'];
                    $reporting_user_id = $data[$s]['reporting_user_id'];

                    unset($data[$s]['company_approval_role_id']); unset($data[$s]['branch_id']); unset($data[$s]['reporting_user_id']);
                    $user_password = generatePassword(6);
                    $data[$s]['password'] = md5($user_password);
                    $user_id = $this->User_model->createUserInfo($data[$s]);
                    $this->Company_model->createCompanyUser(array('company_id' => $company_id, 'user_id' => $user_id,'company_approval_role_id' => $company_approval_role_id, 'branch_id' => $branch_id,'reporting_user_id' => $reporting_user_id));
                    $errors[$s]['message'] = 'Successfully added';

                    sendmail($data[$s]['email_id'],'Account Created','<p>hello '.$data[$s]['first_name'].' '.$data[$s]['last_name'].', welcome to q-lana,</p><p>your login details are</p><p>Email: '.$data[$s]['email_id'].'<br>Password: '.$user_password.'</p>');
                }
            }
        }

        $result = array('status'=>True, 'message' => 'Users uploaded successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function branchTypeRole_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getBranchTypeRole($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getOrderCommitteeData($data,$identifier)
    {
        for($t=0;$t<count($data);$t++){
            if(count($this->ordered_committee_data)==count($data)){  break; }
            if($data[$t]['credit_committee_forward_to']==$identifier){
                $this->ordered_committee_data[] = $data[$t];
                $identifier = $data[$t]['id_company_approval_credit_committee'];
                if(count($this->ordered_committee_data)==count($data)){  break; }
                else{ $this->getOrderCommitteeData($data,$identifier); }
            }
        }

        return $this->ordered_committee_data;
    }

    public function approvalStructure_get()
    {
        $data = $this->input->get();
        $approval_structure_details = $this->Company_model->getApprovalStructure($data);

        if(!empty($approval_structure_details))
            $approval_structure_name = $approval_structure_details[0]['approval_structure_name'];
        else
            $approval_structure_name = '';

        if(isset($data['id_company_approval_structure']) && $data['id_company_approval_structure']!='')
        {
            $branch_type_data = $this->Company_model->getBranchTypeByCompanyId($data['company_id']);
            if(count($branch_type_data) > 1)
            $branch_type_data = $this->getOrderedData($branch_type_data,'0','reporting_branch_type_id','branch_type_id');
            $this->ordered_data = '';
            //echo "<pre>"; print_r($branch_type_data); exit;

            $result = $members_data = $members = array();
            for($s=0;$s<count($branch_type_data);$s++)
            {
                $this->ordered_committee_data = array();
                $committee_data = $this->Company_model->getApprovalCreditCommittee(array('branch_type_id' => $branch_type_data[$s]['branch_type_id'],'id_company_approval_structure' => $data['id_company_approval_structure']));
                //echo count($committee_data).'---';
                $committee_data = $this->getOrderCommitteeData($committee_data,0);
                //echo count($committee_data); exit;
                //echo "<pre> final:"; print_r($committee_data); exit;
                for($r=0;$r<count($committee_data);$r++)
                {
                    $members = array();
                    $members_data = $this->Company_model->getApprovalCreditCommitteeStructure(array('id_company_approval_credit_committee' => $committee_data[$r]['id_company_approval_credit_committee']));
                    for($st=0;$st<count($members_data);$st++)
                    {
                        if($members_data[$st]['branch_type_role_id']==1){
                            $committee_data[$r]['secretary'] = $members_data[$st]['approval_name'];
                        }
                        else{
                            $members[] = $members_data[$st];
                        }
                    }
                    $committee_data[$r]['members'] = $members;
                }


                $result[] = array(
                    'id_branch_type' => $branch_type_data[$s]['branch_type_id'],
                    'branch_type_name' => $branch_type_data[$s]['branch_type_name'],
                    'committee' => $committee_data
                );
            }
        }
        else
        {
            $result = $this->Company_model->getApprovalStructure($data);
        }
        //echo "<pre>"; print_r($result); exit;
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=> array('data' => $result, 'approval_structure_name' => $approval_structure_name));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalStructure_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('company_id', array('required'=>'Company id required'));
        $this->form_validator->add_rules('approval_structure_name', array('required'=>'Name required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_company_approval_structure'])){
            $check_name = $this->Company_model->getApprovalStructure(array('company_id' => $data['company_id'],'approval_structure_name' => trim($data['approval_structure_name']),'id_company_approval_structure_not' => $data['id_company_approval_structure']));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('approval_structure_name' => 'Name already exists'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $company_approval_structure_id = $this->Company_model->updateCompanyApprovalStructure($data);
            $suc_msg = 'Approval structure updated successfully.';
        }
        else{
            $check_name = $this->Company_model->getApprovalStructure(array('company_id' => $data['company_id'],'approval_structure_name' => trim($data['approval_structure_name'])));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('approval_structure_name' => 'Name already exists'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $company_approval_structure_id = $this->Company_model->addCompanyApprovalStructure($data);
            $suc_msg = 'Approval structure added successfully.';
        }

        $result = array('status'=>True, 'message' => $suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalCreditCommitteeList_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getApprovalCreditCommitteeList($data);
        //echo "<pre>"; print_r($result);
        /*if(isset($data['id_company_approval_credit_committee']))
        {
            $result['committee'] = $result[0];
            unset($result[0]);
            $members = $this->Company_model->getApprovalCreditCommitteeStructure($data);
            for($s=0;$s<count($members);$s++)
            {
                if($members[$s]['branch_type_role_id']==1){
                    $result['committee']['committee_secretary'] = $members[$s]['approval_role_id'];
                }
                else{
                    $result['committee_members'][] = $members[$s];
                }
            }
        }*/

        //$result = $this->Company_model->getApprovalCreditCommittee($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }


    public function committeeSecretary_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('crm_project_id', array('required'=>'Project id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_details = $this->Crm_model->getProject($data['crm_project_id']);
        $company_id = $project_details[0]['company_id'];
        $user_id = $project_details[0]['assigned_to'];
        $product_id = $project_details[0]['product_id'];
        $committee_id = $project_details[0]['committee_id'];
        $branch_id = 0;
        $company_branch = $this->Company_model->getCompanyUsers(array('company_id' => $company_id, 'user_id'=> $user_id));
        if(!empty($company_branch))
            $branch_id = $company_branch[0]['branch_id'];
        //getting branch
        if(!$committee_id)
        {
            //getting approval structure
            $approval_structure = $this->Crm_model->getProduct(array('company_id' => $company_id, 'id_product'=> $product_id));
            //$branch_type_id = $company_branch[0]['branch_type_id'];
            $company_approval_structure_id = $approval_structure[0]['company_approval_structure_id'];
            //getting last node
            $branch_type_data = $this->Company_model->getBranchTypeByCompanyId($data['company_id']);
            $branch_type_data = $this->getOrderedData($branch_type_data,'0','reporting_branch_type_id','branch_type_id');
            $this->ordered_data = '';
            $branch_type_id = $branch_type_data[count($branch_type_data)-1]['branch_type_id'];
        }
        else
        {
            $committee_details = $this->Company_model->getApprovalCreditCommittee(array('id_company_approval_credit_committee' => $committee_id));
            $branch_type_id = $committee_details[0]['branch_type_id'];
            $company_approval_structure_id = $committee_details[0]['company_approval_structure_id'];
        }
        $last_node = $this->getReportingBranchTypeCommittee(array('company_id' => $company_id,'branch_type_id' => $branch_type_id,'id_company_approval_structure' => $company_approval_structure_id,'committee_id' => $committee_id));

        if(empty($last_node))
        {
            $data = array(
                'details' => array(
                    'committee_id' => 0,
                    'committee_name' => ''
                ),
                'users' => ''
            );
            $result = array('status'=>TRUE, 'message' => 'No committees exists', 'data'=> $data);
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $committee_structure = $this->Company_model->checkApprovalCreditCommitteeStructure(array('id_company_approval_credit_committee' => $last_node[0]['id_company_approval_credit_committee']));

        $approval_role_id=0;
        for($s=0;$s<count($committee_structure);$s++)
        {
            if($committee_structure[$s]['branch_type_role_id']==1)
                $approval_role_id = $committee_structure[$s]['approval_role_id'];
        }

        /*$company_approval_role = $this->Company_model->getCompanyApprovalRoleByApprovalRoleId($company_id,$approval_role_id);

        $ids = array();
        foreach($company_approval_role as $item){ $ids[] = $item['approval_role_id']; }
        if(!empty($ids)){
            $company_approval_role_ids = '(';
            $company_approval_role_ids .= implode(',',$ids);
            $company_approval_role_ids .= ')';
        }
        else{ $company_approval_role_ids=0; }*/
        $company_approval_role_id = 0;
        if($approval_role_id){ $company_approval_role_id = $approval_role_id; }
        //echo $company_approval_role_ids; exit;
        if($company_approval_role_id){
            $this->forward_to = $user_id;
            $users = $this->getReportingBranchUsers(array('company_id' => $company_id, 'id_branch' => $branch_id, 'company_approval_role_id' => $company_approval_role_id, 'user_id' => $user_id));
            //$this->last_node = '';
            //$result = $this->getReportingUserData($users,$user_id,$company_id);
            //unset($users);
            //$users[0] = $result;
            //$this->last_node = '';
            //$users = $this->Company_model->getCompanyUsersByRole(array('company_id' => $company_id, 'id_branch' => $branch_id, 'company_approval_role_id' => $company_approval_role_id));
        }
        else
            $users = array();

        $committee = array(
            'details' => array(
                'committee_id' => $last_node[0]['id_company_approval_credit_committee'],
                'committee_name' => $last_node[0]['credit_committee_name']
            ),
            'users' => $users
        );
        //for eliminating current user
        for($s=0;$s<count($committee['users']);$s++)
        {
            if($committee['users'][$s]['id_user']==$data['user_id']){
                unset($committee['users'][$s]);
            }
        }
        $committee['users'] = array_values($committee['users']);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=> $committee);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getReportingBranchTypeCommittee($data)
    {
        $company_id = $data['company_id'];
        $branch_type_id = $data['branch_type_id'];
        $id_company_approval_structure = $data['id_company_approval_structure'];
        $committee_id = $data['committee_id'];
        if($committee_id)
        {
            $committee_details = $this->Company_model->getApprovalCreditCommittee(array('id_company_approval_credit_committee' => $committee_id));
            if($committee_details[0]['credit_committee_forward_to']!=0){
                $last_node = $this->Company_model->getApprovalCreditCommittee(array('id_company_approval_credit_committee' => $committee_details[0]['credit_committee_forward_to']));
                return $last_node;
            }
            else
            {
                $company_branch_type = $this->Company_model->getCompanyBranchTypeByBranchId($company_id,$branch_type_id);
                if(empty($company_branch_type) || $company_branch_type[0]['reporting_branch_type_id']==0){
                    return array();
                }
                else{
                    $this->getReportingBranchTypeCommittee(array('company_id' => $company_id,'branch_type_id' => $company_branch_type[0]['reporting_branch_type_id'],'id_company_approval_structure' => $id_company_approval_structure,'committee_id' => 0));
                }
            }
        }
        else
        {
            $total_committee_list = $this->Company_model->getApprovalCreditCommitteeIds(array('company_id' => $company_id,'branch_type_id' => $branch_type_id,'id_company_approval_structure' => $id_company_approval_structure));
            if(!empty($total_committee_list))
            {
                $ids = array();
                foreach($total_committee_list as $item){ $ids[] = $item['id']; }
                $total_committee_list_str = '(';
                $total_committee_list_str .= implode(',',$ids);
                $total_committee_list_str .= ')';
                if(empty($ids)){ $total_committee_list_str = 0; }
                $this->last_node = $this->Company_model->getLastCompanyApprovalCreditCommittee(array('company_id' => $company_id,'branch_type_id' => $branch_type_id,'id_company_approval_structure' => $id_company_approval_structure, 'committee_ids' => $total_committee_list_str));
                return $this->last_node;
            }
            else
            {
                $company_branch_type = $this->Company_model->getCompanyBranchTypeByBranchId($company_id,$branch_type_id);
                if(empty($company_branch_type) || $company_branch_type[0]['reporting_branch_type_id']==0){
                    $this->last_node = array();
                }
                else{
                    $this->getReportingBranchTypeCommittee(array('company_id' => $company_id,'branch_type_id' => $company_branch_type[0]['reporting_branch_type_id'],'id_company_approval_structure' => $id_company_approval_structure,'committee_id' => $committee_id));
                }
            }
        }

        return $this->last_node;
    }

    public function getReportingBranchUsers($data)
    {
        $user_id = $data['user_id'];
        $company_id = $data['company_id'];
        $id_branch = $data['id_branch'];
        $company_approval_role_id = $data['company_approval_role_id'];
        $users = $this->Company_model->getCompanyUsersByRole(array('company_id' => $company_id, 'id_branch' => $id_branch, 'company_approval_role_id' => $company_approval_role_id));
        $this->last_node = $users;

        if(empty($users))
        {
            $branch_details = $this->Company_model->getCompanyBranch($id_branch);
            if(!empty($branch_details))
            {
                $this->getReportingBranchUsers(array('company_id' => $company_id, 'id_branch' => $branch_details->reporting_branch_id, 'company_approval_role_id' => $company_approval_role_id, 'user_id' => $user_id));
            }
        }

        return $this->last_node;
    }

    public function getReportingUserData($data,$user_id,$company_id)
    {
        $report_user = $this->Company_model->getCompanyUsers(array('user_id' => $user_id, 'company_id' => $company_id));
        if(!empty($report_user)){
            for($s=0;$s<count($data);$s++)
            {
                if($report_user[0]['reporting_user_id']==$data[$s]['id_user']){ $this->last_node = $data[$s]; }
            }

            if($this->last_node===''){
                $this->getReportingUserData($data,$report_user[0]['reporting_user_id'],$company_id);
            }
        }
       return $this->last_node;
    }

    public function approvalCreditCommittee_get()
    {
        $data = $this->input->get();
        //echo "<pre>"; print_r($result);
        if(isset($data['id_company_approval_credit_committee']))
        {
            $result['committee'] = $this->Company_model->getApprovalCreditCommittee($data);
            if($result['committee'][0]['credit_committee_forward_to']=="0"){ $result['committee'][0]['is_primary_committee'] = true; }
            if(isset($result['committee'][0])){ $result['committee'] = $result['committee'][0]; }
            $members = $this->Company_model->getApprovalCreditCommitteeStructure($data);
            //echo "<pre>"; print_r($members); exit;
            for($s=0;$s<count($members);$s++)
            {
                if($members[$s]['branch_type_role_id']==1){
                    $result['committee']['committee_secretary'] = $members[$s]['approval_role_id'];
                }
                else{
                    $result['committee_members'][] = $members[$s];
                }
            }
        }
        else{
            $result = $this->Company_model->getApprovalCreditCommittee($data);
        }

        //$result = $this->Company_model->getApprovalCreditCommittee($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function forwardCommittee_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_approval_structure_id = 0 ;
        $result = array();
        if(isset($data['committee']['credit_committee_forward_to']))
        {
            if(isset($data['committee']['id_company_approval_credit_committee']))
                $company_approval_structure_id = $data['committee']['id_company_approval_credit_committee'];

            $result = $this->Company_model->checkCommitteesForwardTo($data['committee'],$company_approval_structure_id);
            if(!empty($result))
            {
                $result = array('status'=>FALSE, 'error' => 'Already '.$result[0]['credit_committee_name'].' committee forward to '.$result[0]['forward_committee_name'].' committee, do you want to continue?', 'data'=> $result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
            else{
                $result = array('status'=>TRUE, 'message' => 'success', 'data'=> $result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalCreditCommittee_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo "<pre>"; print_r($data); exit;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        //validating data
        $this->form_validator->add_rules('company_id', array('required'=>'Company id required'));
        $this->form_validator->add_rules('credit_committee_name', array('required'=>'Committee name required'));
        $this->form_validator->add_rules('branch_type_id', array('required'=>'Branch type required'));
        $this->form_validator->add_rules('company_approval_structure_id', array('required'=>'Approval structure id name required'));
        $this->form_validator->add_rules('committee_secretary', array('required'=>'Committee secretary required'));
        $validated = $this->form_validator->validate($data['committee']);
        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $check_approval_credit_committee_id = 0;
            if(isset($data['committee']['id_company_approval_credit_committee'])){ $check_approval_credit_committee_id = $data['committee']['id_company_approval_credit_committee']; }
            $check_name = $this->Company_model->getApprovalCreditCommittee(array('company_id' => $data['committee']['company_id'], 'id_company_approval_structure' => $data['committee']['company_approval_structure_id'], 'branch_type_id' => $data['committee']['branch_type_id'], 'id_company_approval_credit_committee_not' => $check_approval_credit_committee_id, 'credit_committee_name' => trim($data['committee']['credit_committee_name'])));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('credit_committee_name' => 'Committee name already exists'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        if(!isset($data['committee']['credit_committee_forward_to']) && !isset($data['committee']['is_primary_committee']))
        {
            $result = array('status'=>FALSE,'error'=>array('credit_committee_forwarded_to' => 'Forwarded to committee required'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $committee_secretary = $is_primary_committee = 0; $committee_structure = $insert_committee_structure = $delete_structure = array();

        if(isset($data['committee']['committee_secretary']))
            $committee_secretary = $data['committee']['committee_secretary'];
        if(isset($data['committee']['is_primary_committee']) && $data['committee']['is_primary_committee']!='' && $data['committee']['is_primary_committee']!=0){
            $is_primary_committee = 1;
        }

        unset($data['committee']['committee_secretary']);
        unset($data['committee']['is_primary_committee']);


        if($is_primary_committee){
            $current_primary = $this->Company_model->checkPrimaryCreditCommittees(array('company_id' => $data['committee']['company_id'],'company_approval_structure_id' => $data['committee']['company_approval_structure_id'],'branch_type_id' => $data['committee']['branch_type_id']));
            if(!empty($current_primary) && isset($data['committee']['id_company_approval_credit_committee']))
                if($current_primary[0]['id_company_approval_credit_committee']==$data['committee']['id_company_approval_credit_committee'])
                {
                    unset($current_primary);
                }

            if(isset($data['committee']['id_company_approval_credit_committee'])){
                $current_updating_committee = $this->Company_model->getApprovalCreditCommittee(array('id_company_approval_credit_committee' => $data['committee']['id_company_approval_credit_committee']));
                $committee_has_current_committee_as_reporting = $this->Company_model->getCompanyApprovalCreditCommitteeAsReportingCreditCommittee($data['committee']['company_id'],$data['committee']['id_company_approval_credit_committee']);
                if(isset($current_updating_committee[0]['credit_committee_forward_to']) && ($current_updating_committee[0]['credit_committee_forward_to'] == $data['committee']['credit_committee_forward_to']))
                {
                    unset($current_updating_committee);
                    unset($committee_has_current_committee_as_reporting);
                }
            }
        }
        else{
            //$branch_type_has_current_branch_as_reporting = $this->Company_model->getCompanyBranchTypeAsReportingBranchType($data['company_id'], $data['id_branch_type']);
            if(isset($data['committee']['id_company_approval_credit_committee'])){
                $current_updating_committee = $this->Company_model->getApprovalCreditCommittee(array('id_company_approval_credit_committee' => $data['committee']['id_company_approval_credit_committee']));
                $committee_has_current_committee_as_reporting = $this->Company_model->getCompanyApprovalCreditCommitteeAsReportingCreditCommittee($data['committee']['company_id'],$data['committee']['id_company_approval_credit_committee']);
                //checking not updating reporting_id
                if(isset($current_updating_committee[0]['credit_committee_forward_to']) && ($current_updating_committee[0]['credit_committee_forward_to'] == $data['committee']['credit_committee_forward_to']))
                {
                    unset($current_updating_committee);
                    unset($committee_has_current_committee_as_reporting);
                }
            }
            $committee_has_updating_committee_as_reporting = $this->Company_model->getCompanyApprovalCreditCommitteeAsReportingCreditCommittee($data['committee']['company_id'], $data['committee']['credit_committee_forward_to']);


        }

        if(isset($data['committee']['id_company_approval_credit_committee']))
        {
            if($is_primary_committee){ $data['committee']['credit_committee_forward_to']=0; }
            else if(!isset($data['committee']['credit_committee_forward_to'])){ $data['committee']['credit_committee_forward_to']=0; }
            else if(isset($data['committee']['credit_committee_forward_to']) && $data['committee']['credit_committee_forward_to']==''){ $data['committee']['credit_committee_forward_to']=0; }


            $this->Company_model->updateApprovalCreditCommittee($data['committee']);
            $id_company_approval_credit_committee = $data['committee']['id_company_approval_credit_committee'];
            if(isset($data['delete_members']))
            {
                for($s=0;$s<count($data['delete_members']);$s++)
                {
                    $delete_structure[] = array(
                        'id_company_approval_credit_committee_structure' => $data['delete_members'][$s],
                        'status' => '0',
                    );
                }
            }

            if(!empty($delete_structure))
                $this->Company_model->updateApprovalCreditCommitteeStructure($delete_structure);

            //echo "<pre>"; print_r($data['committee_members']); exit;
            if($committee_secretary){
                array_push($data['committee_members'],array(
                    'branch_type_role_id' => 1,
                    'company_approval_role_id' => $committee_secretary,
                    'company_approval_credit_committee_id' => $id_company_approval_credit_committee
                ));
            }
            //print_r($data['committee_members']); exit;
            for($s=0;$s<count($data['committee_members']);$s++)
            {
                $check_member_exists =$this->Company_model->checkApprovalCreditCommitteeStructure(array('branch_type_role_id' => $data['committee_members'][$s]['branch_type_role_id'],'approval_role_id' => $data['committee_members'][$s]['company_approval_role_id'],'id_company_approval_credit_committee' => $id_company_approval_credit_committee));
                //print_r($check_member_exists); exit;
                if(empty($check_member_exists))
                {
                    $insert_committee_structure[] = array(
                        'company_approval_credit_committee_id' => $id_company_approval_credit_committee,
                        'branch_type_role_id' => $data['committee_members'][$s]['branch_type_role_id'],
                        'approval_role_id' => $data['committee_members'][$s]['company_approval_role_id'],
                    );
                }
                else
                {
                    if(!isset($data['committee_members'][$s]['id_company_approval_credit_committee_structure'])){
                        $data['committee_members'][$s]['id_company_approval_credit_committee_structure'] = $check_member_exists[0]['id_company_approval_credit_committee_structure'];
                    }
                    //echo "<pre>"; print_r($data['committee_members']);
                    $committee_structure[] = array(
                        'id_company_approval_credit_committee_structure' => $data['committee_members'][$s]['id_company_approval_credit_committee_structure'],
                        'company_approval_credit_committee_id' => $id_company_approval_credit_committee,
                        'branch_type_role_id' => $data['committee_members'][$s]['branch_type_role_id'],
                        'approval_role_id' => $data['committee_members'][$s]['company_approval_role_id'],
                        'status' => 1
                    );
                }

            }
            //echo "<pre>"; print_r($insert_committee_structure);
            //echo "<pre>"; print_r($committee_structure); exit;
            //print_r($committee_structure); exit;
            if(!empty($insert_committee_structure))
                $this->Company_model->addApprovalCreditCommitteeStructure($insert_committee_structure);

            if(!empty($committee_structure))
                $this->Company_model->updateApprovalCreditCommitteeStructure($committee_structure);
            /*$committee_structure[] = array(
                'company_approval_credit_committee_id' => $id_company_approval_credit_committee,
                'branch_type_role_id' => 1,
                'approval_role_id' => $committee_secretary,
            );*/

            $suc_msg = 'Credit committee updated successfully.';

            //updating reporting branch types
            if($is_primary_committee){
                if(!empty($current_primary))
                 $this->Company_model->updateApprovalCreditCommittee(array('company_id' => $data['committee']['company_id'], 'id_company_approval_credit_committee' => $current_primary[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $id_company_approval_credit_committee));
                //updating branch type which has current branch as reporting
                if(isset($committee_has_current_committee_as_reporting) && !empty($committee_has_current_committee_as_reporting))
                    $this->Company_model->updateApprovalCreditCommittee(array('company_id' => $data['committee']['company_id'], 'id_company_approval_credit_committee' => $committee_has_current_committee_as_reporting[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $current_updating_committee[0]['credit_committee_forward_to']));
            }
            else{
                if(isset($committee_has_current_committee_as_reporting) && !empty($committee_has_current_committee_as_reporting)){
                    $this->Company_model->updateApprovalCreditCommittee(array('company_id' => $data['committee']['company_id'], 'id_company_approval_credit_committee' => $committee_has_current_committee_as_reporting[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $current_updating_committee[0]['credit_committee_forward_to']));
                }
                if(isset($committee_has_updating_committee_as_reporting) && !empty($committee_has_updating_committee_as_reporting) && isset($current_updating_committee) && !empty($current_updating_committee)){
                    $this->Company_model->updateApprovalCreditCommittee(array('company_id' =>$data['committee']['company_id'], 'id_company_approval_credit_committee' => $committee_has_updating_committee_as_reporting[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $current_updating_committee[0]['id_company_approval_credit_committee']));
                }
            }
        }
        else
        {
            if($is_primary_committee){ $data['committee']['credit_committee_forward_to']=0; }
            else if(!isset($data['committee']['credit_committee_forward_to'])){ $data['committee']['credit_committee_forward_to']=0; }
            else if(isset($data['committee']['credit_committee_forward_to']) && $data['committee']['credit_committee_forward_to']==''){ $data['committee']['credit_committee_forward_to']=0; }
            $id_company_approval_credit_committee = $this->Company_model->addApprovalCreditCommittee($data['committee']);
            for($s=0;$s<count($data['committee_members']);$s++)
            {
                $committee_structure[] = array(
                    'company_approval_credit_committee_id' => $id_company_approval_credit_committee,
                    'branch_type_role_id' => $data['committee_members'][$s]['branch_type_role_id'],
                    'approval_role_id' => $data['committee_members'][$s]['company_approval_role_id'],
                );
            }
            $committee_structure[] = array(
                'company_approval_credit_committee_id' => $id_company_approval_credit_committee,
                'branch_type_role_id' => 1,
                'approval_role_id' => $committee_secretary,
            );

            $this->Company_model->addApprovalCreditCommitteeStructure($committee_structure);
            $suc_msg = 'Credit committee added successfully.';

            //updating reporting branch types
            if($is_primary_committee){
                if(!empty($current_primary))
                $this->Company_model->updateApprovalCreditCommittee(array('company_id' => $data['committee']['company_id'], 'id_company_approval_credit_committee' => $current_primary[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $id_company_approval_credit_committee));
            }
            else{
                if(isset($committee_has_current_committee_as_reporting) && !empty($committee_has_current_committee_as_reporting))
                    $this->Company_model->updateApprovalCreditCommittee(array('company_id' => $data['committee']['company_id'], 'id_company_approval_credit_committee' => $committee_has_current_committee_as_reporting[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $id_company_approval_credit_committee));
                if(isset($committee_has_updating_committee_as_reporting) && !empty($committee_has_updating_committee_as_reporting)){
                    if(!isset($current_updating_committee) && !empty($current_updating_committee)){ $id_company_approval_credit_committee = $current_updating_committee[0]['id_company_approval_credit_committee']; }
                    $this->Company_model->updateApprovalCreditCommittee(array('company_id' => $data['committee']['company_id'], 'id_company_approval_credit_committee' => $committee_has_updating_committee_as_reporting[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $id_company_approval_credit_committee));
                }
            }
        }

        /*if($is_primary_committee){
            $committee_point_to_current_committee = $this->Company_model->checkCommitteesForwardTo(array('company_id' => $data['committee']['company_id'],'branch_type_id' => $data['committee']['branch_type_id'],'credit_committee_forward_to' =>$id_company_approval_credit_committee) ,0);
            $result = $this->Company_model->checkPrimaryCreditCommittees($data['committee'],$id_company_approval_credit_committee);
            //echo "<pre>"; print_r($result); exit;
            if(!empty($result)){
                $this->Company_model->updateApprovalCreditCommittee(array('id_company_approval_credit_committee' => $result[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $id_company_approval_credit_committee));
            }

            //$total_committee_list = $this->Company_model->getApprovalCreditCommitteeIds(array('company_id' => $data['committee']['company_id'],'branch_type_id' => $data['committee']['branch_type_id'],'id_company_approval_structure' => $id_company_approval_structure));
            if(!empty($committee_point_to_current_committee)){
                $id_company_approval_structure = $committee_point_to_current_committee[0]['company_approval_structure_id'];
                $total_committee_list = $this->Company_model->getApprovalCreditCommitteeIds(array('company_id' => $data['committee']['company_id'],'branch_type_id' => $data['committee']['branch_type_id'],'id_company_approval_structure' => $id_company_approval_structure));
                foreach($total_committee_list as $item){ $ids[] = $item['id']; }
                array_push($ids,$committee_point_to_current_committee[0]['id_company_approval_credit_committee']);
                $total_committee_list_str = '(';
                $total_committee_list_str .= implode(',',$ids);
                $total_committee_list_str .= ')';
                $last_node = $this->Company_model->getLastCompanyApprovalCreditCommittee(array('company_id' => $data['committee']['company_id'],'branch_type_id' => $data['committee']['branch_type_id'],'id_company_approval_structure' => $id_company_approval_structure, 'committee_ids' => $total_committee_list_str));
                //echo "<pre>"; print_r($last_node); exit;
                if(!empty($last_node))
                    $this->Company_model->updateApprovalCreditCommittee(array('id_company_approval_credit_committee' => $committee_point_to_current_committee[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $last_node[0]['id_company_approval_credit_committee']));
            }

        }
        else if(isset($data['committee']['credit_committee_forward_to']))
        {
            $id_company_approval_structure = $this->Company_model->getApprovalCreditCommittee(array('id_company_approval_credit_committee' => $id_company_approval_credit_committee));
            $id_company_approval_structure = $id_company_approval_structure[0]['company_approval_structure_id'];

            $committee_point_to_current_committee = $this->Company_model->checkCommitteesForwardTo(array('company_id' => $data['committee']['company_id'],'branch_type_id' => $data['committee']['branch_type_id'],'credit_committee_forward_to' =>$id_company_approval_credit_committee) ,0);

            $current_committee_point_to_committee = $this->Company_model->checkCommitteesForwardTo($data['committee'],$id_company_approval_credit_committee);

            if(!empty($current_committee_point_to_committee)){
                $this->Company_model->updateApprovalCreditCommittee(array('id_company_approval_credit_committee' => $current_committee_point_to_committee[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $id_company_approval_credit_committee));
            }
            //$total_committee_list = $this->Company_model->getApprovalCreditCommitteeIds(array('company_id' => $data['committee']['company_id'],'branch_type_id' => $data['committee']['branch_type_id'],'id_company_approval_structure' => $id_company_approval_structure));
            if(!empty($committee_point_to_current_committee)){
                $total_committee_list = $this->Company_model->getApprovalCreditCommitteeIds(array('company_id' => $data['committee']['company_id'],'branch_type_id' => $data['committee']['branch_type_id'],'id_company_approval_structure' => $id_company_approval_structure));
                foreach($total_committee_list as $item){ $ids[] = $item['id']; }
                array_push($ids,$committee_point_to_current_committee[0]['id_company_approval_credit_committee']);
                $total_committee_list_str = '(';
                $total_committee_list_str .= implode(',',$ids);
                $total_committee_list_str .= ')';
                $last_node = $this->Company_model->getLastCompanyApprovalCreditCommittee(array('company_id' => $data['committee']['company_id'],'branch_type_id' => $data['committee']['branch_type_id'],'id_company_approval_structure' => $id_company_approval_structure, 'committee_ids' => $total_committee_list_str));
                //echo "<pre>"; print_r($last_node); exit;
                if(!empty($last_node))
                    $this->Company_model->updateApprovalCreditCommittee(array('id_company_approval_credit_committee' => $committee_point_to_current_committee[0]['id_company_approval_credit_committee'], 'credit_committee_forward_to' => $last_node[0]['id_company_approval_credit_committee']));
            }
        }*/

        $result = array('status'=>True, 'message' => $suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalCreditCommitteeStructure_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getApprovalCreditCommitteeStructure($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalCreditCommitteeStructure_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('company_id', array('required'=>'Company id required'));
        $this->form_validator->add_rules('approval_structure_name', array('required'=>'Name required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_company_approval_structure'])){
            $company_approval_structure_id = $this->Company_model->updateApprovalCreditCommitteeStructure($data);
            $suc_msg = 'Credit committee structure updated successfully.';
        }
        else{
            $company_approval_structure_id = $this->Company_model->addApprovalCreditCommitteeStructure($data);
            $suc_msg = 'Credit committee structure added successfully.';
        }

        $result = array('status'=>True, 'message' => $suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalLimitSector_get()
    {
        $data = $this->input->get();
        $result = $this->Master_model->getSectorsList($data);
        $res = array();
        foreach($result as $row){
            $res[] = array(
                'id_product_approval_limit' => '0',
                'amount' => '0',
                'id_sector' => $row['id_sector'],
                'sector_name' => $row['sector_name']
            );
        }
        $total_records=$this->Master_model->getSectorCount();
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$res,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function checkApprovalLimitSector_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $committees = $this->Company_model->getApprovalCreditCommittee(array('company_id' => $data['company_id'], 'id_company_approval_structure' => $data['id_company_approval_structure']));
        //echo "<pre>"; print_r($committees); exit;
        $ids = array();
        for($s=0;$s<count($committees);$s++){
            $ids[] = $committees[$s]['id_company_approval_credit_committee'];
        }
        if(!empty($ids))
        {
            $committee_ids = '(';
            $committee_ids .= implode(',',$ids);
            $committee_ids .= ')';
            $data['committees'] = $committee_ids;
        }
        else
        {
            $data['committees'] = '(0)';
        }

        $result = $this->Company_model->getApprovalLimitExceptStatus($data);

        if(!empty($result))
        {
            $this->Company_model->updateApprovalLimitBySector($data,1);
        }
        else
        {
            $add = array();
            $branch_type_data = $this->Company_model->getBranchTypeByCompanyId($data['company_id']);
            for($s=0;$s<count($branch_type_data);$s++)
            {
                $data['branch_type_id'] = $branch_type_data[$s]['branch_type_id'];
                $committee_data = $this->Company_model->getApprovalCreditCommittee($data);
                for($r=0;$r<count($committee_data);$r++)
                {
                    $add[] = array(
                        'company_id' => $data['company_id'],
                        'company_approval_credit_committee_id' => $committee_data[$r]['id_company_approval_credit_committee'],
                        'sector_id' => $data['sector_id'],
                        'product_id' => $data['product_id'],
                        'amount' => '0',
                        'created_by' => $data['created_by']
                    );
                }
            }
            if(!empty($add)){ $this->Company_model->addApprovalLimit($add); }
        }
        $result = array('status'=>True, 'message' => '', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalLimit_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $approval_limit_data = array();
        $product_details = $this->Crm_model->getProduct(array('id_product' => $data['product_id'], 'company_id' => $data['company_id']));
        //echo "<pre>"; print_r($product_details); exit;
        $data['id_company_approval_structure'] = $product_details[0]['company_approval_structure_id'];

        $approval_structure_details = array(
            'id_company_approval_structure' => $product_details[0]['company_approval_structure_id'],
            'approval_structure_name' => $product_details[0]['approval_structure_name']
        );


        $committees = $this->Company_model->getApprovalCreditCommittee(array('company_id' => $data['company_id'], 'id_company_approval_structure' => $product_details[0]['company_approval_structure_id']));
        //echo "<pre>"; print_r($committees); exit;
        $ids = array();
        for($s=0;$s<count($committees);$s++){
            $ids[] = $committees[$s]['id_company_approval_credit_committee'];
        }
        if(!empty($ids))
        {
            $committee_ids = '(';
            $committee_ids .= implode(',',$ids);
            $committee_ids .= ')';
            $data['committees'] = $committee_ids;
        }
        else
        {
            $data['committees'] = '(0)';
        }

        $sectors = $this->Company_model->getApprovalLimitSectors($data);
        //echo "<pre>"; print_r($sectors); exit;
        $sector_ids = array_values(array_unique(array_map(function ($i) { return $i['id_sector']; }, $sectors)));
        if(!in_array(0,$sector_ids)){
            array_unshift($sectors, array('id_sector' => '0','sector_name' => 'All Sectors'));
        }

        if(empty($sectors)){
            $sectors[] = array(
                'id_sector' => '0',
                'sector_name' => 'All Sectors'
            );
        }
        unset($data['committees']);

        $branch_type_data = $this->Company_model->getBranchTypeByCompanyId($data['company_id']);
        if(count($branch_type_data) > 1)
            $branch_type_data = $this->getOrderedData($branch_type_data,'0','reporting_branch_type_id','branch_type_id');
        $this->ordered_data = '';
        for($s=0;$s<count($branch_type_data);$s++)
        {
            $data['branch_type_id'] = $branch_type_data[$s]['branch_type_id'];
            $this->ordered_committee_data = array();
            $committee_data = $this->Company_model->getApprovalCreditCommittee($data);
            //echo "<pre>"; print_r($committee_data); exit;
            $committee_data = $this->getOrderCommitteeData($committee_data,0);
            //lets check if product data exists or not
            $check_data = $this->Company_model->getApprovalLimit(array('product_id' => $data['product_id'], 'company_id' => $data['company_id']));
            //echo "<pre>"; print_r($check_data); exit;
            if(empty($check_data))
            {   //if no data present, creating all sectors data
                for($r=0;$r<count($committee_data);$r++)
                {
                    $committee_data[$r]['limit'][] = array(
                        'id_product_approval_limit' => '0',
                        'amount' => '0',
                        'id_sector' => '0',
                        'sector_name' => 'All Sectors'
                    );
                }
            }
            else
            {   //if data present, getting data from database
                //echo "<pre>"; print_r($committee_data); exit;
                for($r=0;$r<count($committee_data);$r++)
                {
                    $data['company_approval_credit_committee_id'] = $committee_data[$r]['id_company_approval_credit_committee'];
                    //print_r($data); exit;
                    $committee_data[$r]['limit'] = $this->Company_model->getApprovalLimit($data);
                    //echo "<pre>"; print_r($committee_data[$r]['limit']); exit;
                    if(empty($committee_data[$r]['limit'])){
                        for($sr = 0;$sr<count($sectors);$sr++)
                        {
                            $committee_data[$r]['limit'][] = array(
                                'id_product_approval_limit' => '0',
                                'amount' => '0',
                                'id_sector' => $sectors[$sr]['id_sector'],
                                'sector_name' => 'All Sectors'
                            );
                        }
                    }
                    else
                    {

                        $check_sectors = array();
                        for($u=0;$u<count($committee_data[$r]['limit']);$u++)
                        {
                            $check_sectors[] = $committee_data[$r]['limit'][$u]['id_sector'];
                        }

                        for($sr=0;$sr<count($sectors);$sr++)
                        {
                            if(!in_array($sectors[$sr]['id_sector'],$check_sectors))
                            {
                                $committee_data[$r]['limit'][] = array(
                                    'id_product_approval_limit' => '0',
                                    'amount' => '0',
                                    'id_sector' => $sectors[$sr]['id_sector'],
                                    'sector_name' => 'All Sectors'
                                );
                            }
                        }
                        $temp = array();

                        for($sr=0;$sr<count($sectors);$sr++)
                        {
                            if($sectors[$sr]['id_sector']==0){
                                $temp = $sectors[$sr];
                                unset($sectors[$sr]);
                                array_values($sectors);
                                array_unshift($sectors,$temp);
                                break;
                            }
                        }
                        $temp = array();
                        //ordering sector wise

                        for($sr=0;$sr<count($sectors);$sr++)
                        {
                            for($u=0;$u<count($committee_data[$r]['limit']);$u++)
                            {
                                if($sectors[$sr]['id_sector']==$committee_data[$r]['limit'][$u]['id_sector'])
                                    $temp[] = $committee_data[$r]['limit'][$u];
                            }
                        }

                        $committee_data[$r]['limit'] = $temp;
                    }

                    unset($data['company_approval_credit_committee_id']);
                }
            }

            $result[] = array(
                'branch_type_id' => $branch_type_data[$s]['branch_type_id'],
                'branch_type_name' => $branch_type_data[$s]['branch_type_name'],
                'committee' => $committee_data
            );
        }


        for($u=0;$u<count($sectors);$u++)
        {
            if($sectors[$u]['id_sector']==''){ $sectors[$u]['id_sector']='0'; }
            if($sectors[$u]['sector_name']==''){ $sectors[$u]['sector_name']='All sectors'; }
        }
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=> array( 'data' => $result, 'sectors' => $sectors, 'approval_structure' => $approval_structure_details));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalLimit_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $details = $data['details'];
        $data = $data['data'];
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_id = $details['id_company'];
        $product_id = $details['product_id'];
        $created_by = $details['created_by'];
        //print_r($data); exit;
        $add = $update = $limit = array();
        for($s=0;$s<count($data);$s++)
        {
            $committee = $data[$s]['committee'];
            for($r=0;$r<count($committee);$r++)
            {
                $committee_id = $committee[$r]['id_company_approval_credit_committee'];
                $limit = $committee[$r]['limit'];
                for($t=0;$t<count($limit);$t++)
                {
                    if(isset($details['updatedSectorId'])){
                        if($limit[$t]['id_sector']==$details['updatedSectorId'])
                        {
                            if(isset($limit[$t]['id_product_approval_limit']) && $limit[$t]['id_product_approval_limit']==0){
                                $add[] = array(
                                    'company_id' => $company_id,
                                    'company_approval_credit_committee_id' => $committee_id,
                                    'sector_id' => $limit[$t]['id_sector'],
                                    'product_id' => $product_id,
                                    'amount' => $limit[$t]['amount'],
                                    'created_by' => $created_by
                                );
                            }
                            else if(isset($limit[$t]['id_product_approval_limit']) && $limit[$t]['id_product_approval_limit']!=0){
                                $update[] = array(
                                    'id_product_approval_limit' => $limit[$t]['id_product_approval_limit'],
                                    'company_id' => $company_id,
                                    'company_approval_credit_committee_id' => $committee_id,
                                    'sector_id' => $limit[$t]['id_sector'],
                                    'product_id' => $product_id,
                                    'amount' => $limit[$t]['amount'],
                                    'created_by' => $created_by
                                );
                            }
                        }
                    }
                    else
                    {
                        if(isset($limit[$t]['id_product_approval_limit']) && $limit[$t]['id_product_approval_limit']==0){
                            $add[] = array(
                                'company_id' => $company_id,
                                'company_approval_credit_committee_id' => $committee_id,
                                'sector_id' => $limit[$t]['id_sector'],
                                'product_id' => $product_id,
                                'amount' => $limit[$t]['amount'],
                                'created_by' => $created_by,
                                'updated_date' => date('Y-m-d H:i:s')
                            );
                        }
                        else if(isset($limit[$t]['id_product_approval_limit']) && $limit[$t]['id_product_approval_limit']!=0){
                            $update[] = array(
                                'id_product_approval_limit' => $limit[$t]['id_product_approval_limit'],
                                'company_id' => $company_id,
                                'company_approval_credit_committee_id' => $committee_id,
                                'sector_id' => $limit[$t]['id_sector'],
                                'product_id' => $product_id,
                                'amount' => $limit[$t]['amount'],
                                'created_by' => $created_by,
                                'updated_date' => date('Y-m-d H:i:s')
                            );
                        }
                    }
                }
            }
        }

        if(!empty($add)){ $this->Company_model->addApprovalLimit($add); }
        if(!empty($update)){ $this->Company_model->updateApprovalLimit($update); }

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function approvalLimit_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('company_id', array('required'=>'Company id required'));
        $this->form_validator->add_rules('product_id', array('required'=>'Product id required'));
        $this->form_validator->add_rules('sector_id', array('required'=>'sector id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Company_model->updateApprovalLimitBySector($data,0);

        $result = array('status'=>TRUE, 'message' =>'Approval limit deleted successfully..', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function product_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $this->form_validator->add_rules('id_product', array('required'=> 'Product name required'));
        $this->form_validator->add_rules('company_approval_structure_id', array('required'=> 'Approval structure required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $product = array(
            'company_id' => $data['company_id'],
            'id_product' => $data['id_product'],
            'company_approval_structure_id' => $data['company_approval_structure_id']
        );
        $product_id = $this->Crm_model->updateProduct($product);

        /*if(isset($data['company_approval_structure_id'])){
            $this->Company_model->ChangeApprovalLimitStatus(array('product_id' => $data['id_product'], 'product_approval_limit_status' =>0));
            $committees = $this->Company_model->getApprovalCreditCommittee(array('company_id' => $data['company_id'], 'id_company_approval_structure' => $data['company_approval_structure_id']));
            $ids = array();
            for($s=0;$s<count($committees);$s++){
                $ids[] = $committees[$s]['id_company_approval_credit_committee'];
            }
            if(!empty($ids))
            {
                $committee_ids = '(';
                $committee_ids .= implode(',',$ids);
                $committee_ids .= ')';
                $this->Company_model->ChangeApprovalLimitStatus(array('product_id' => $data['id_product'], 'product_approval_limit_status' =>1, 'company_approval_credit_committee_id' => $committee_ids));
            }

        }*/

        $result = array('status'=>TRUE, 'message' =>'Product updated successfully.', 'data'=>$product_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function financialStatements_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getFinancialStatements($data);
        if(isset($data['crm_company_id']))
        {
            for($s=0;$s<count($result);$s++)
            {
                $result[$s]['status'] = 0;
                $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $result[$s]['id_financial_statement'], 'reference_type' => 'financial'));
                if(!empty($module_status)){ $result[$s]['status'] = 1; }
            }
        }

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function financialExcel_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data = $data['financeData'];

        /*if(!isset($data['id_company']) || $data['id_company']==''){
            $result = array('status'=>FALSE,'error'=>'Invalid data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }*/
        $this->form_validator->add_rules('crm_company_id', array('required'=> 'Crm company id required'));
        $this->form_validator->add_rules('financial_statement_id', array('required'=> 'Financial statement required'));
        $this->form_validator->add_rules('user_id', array('required'=> 'User id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($_FILES) && !empty($_FILES['file']['name'])){
            $file_name = $_FILES['file']['name']['excel'];
            $allowed =  array('xls','xlsx');
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                $result = array('status'=>FALSE,'error'=>'upload only xls,xlsx format files only','data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $result = array('status'=>FALSE,'error'=>'Please upload excel file','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        /*if($_FILES['excel']['size']>EXCEL_UPLOAD_SIZE)
        {
            $result = array('status'=>FALSE,'error'=>'Maximum 2MB files allowed','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }*/

        $financial_statement_id = $data['financial_statement_id'];
        $crm_company_id = $data['crm_company_id'];
        $user_id = $data['user_id'];

        $excel_headers = array_values(excelHeaders($_FILES['file']['tmp_name']['excel'], true));
        $data = excelToArray($_FILES['file']['tmp_name']['excel'], true);

        if(empty($data)){
            $result = array('status'=>FALSE, 'error' => 'Excel is empty', 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $company_financial_statement_data = array('crm_company_id' => $crm_company_id, 'created_by' => $user_id, 'financial_statement_id' => $financial_statement_id);
        $company_financial_statement_id = $this->Company_model->addCompanyFinancialStatement($company_financial_statement_data);

        $financial_statement_header_item_id = $financial_statement_final_header = $header_temp_var = $temp = 0; $financial_statement_header_id = array();
        $item_type = '';

        for($s=0;$s<count($data);$s++)
        {
            for($r=0;$r<count($excel_headers);$r++)
            {
                $final_header = '';
                $data[$s][$excel_headers[$r]] = strtolower(trim($data[$s][$excel_headers[$r]]));
                if($data[$s][$excel_headers[$r]]!='')
                {
                    if($r==0)
                    {
                        $header = trim(strtolower($data[$s][$excel_headers[$r]]));
                        $header = explode('-',$header);

                        if($temp)
                        {
                            if(is_numeric($header[0]) || is_float($header[0]))
                            {
                                if (strpos($header[0], $temp.'.') !== false) {
                                    for($sr=1;$sr<count($header);$sr++){
                                        if($sr!=1){ $final_header .= '-'; }
                                        $final_header .= $header[$sr];
                                    }
                                    $financial_statement_final_header = $this->Company_model->addFinancialStatementHeader(array('company_financial_statement_id' => $company_financial_statement_id, 'header_name' => $final_header, 'parent_header_id' => $financial_statement_header_id[$temp]));
                                }
                                else if(isset($header[0]) && isset($header[1])){
                                    $temp = $header[0];
                                    for($sr=1;$sr<count($header);$sr++){
                                        if($sr!=1){ $final_header .= '-'; }
                                        $final_header .= $header[$sr];
                                    }
                                    $financial_statement_header_id[$temp] = $this->Company_model->addFinancialStatementHeader(array('company_financial_statement_id' => $company_financial_statement_id, 'header_name' => $final_header, 'parent_header_id' => 0));
                                    $financial_statement_final_header = 0;
                                }
                            }
                            else
                            {
                                $item_type = '';
                                if (strpos($header[0], 'total') !== false) { $item_type = 'total'; }
                                $final_header = $header[0];
                                if(!$financial_statement_final_header)
                                {
                                    if(isset($financial_statement_header_id[$temp]))
                                    {
                                        $financial_statement_final_header = $financial_statement_header_id[$temp]; $header_temp_var = 1;
                                    }
                                    else
                                    {
                                        $financial_statement_final_header = $this->Company_model->addFinancialStatementHeader(array('company_financial_statement_id' => $company_financial_statement_id, 'header_name' => '', 'parent_header_id' => 0));
                                    }
                                }
                                $financial_statement_header_item_id = $this->Company_model->addFinancialStatementHeaderItem(array('financial_statement_header_id' => $financial_statement_final_header, 'header_item_name' => $final_header, 'item_type' => $item_type));
                                if($header_temp_var==1){ $financial_statement_final_header = 0; $header_temp_var = 0; }
                            }
                        }
                        else
                        {
                            $temp = $header[0];
                            for($sr=1;$sr<count($header);$sr++){
                                if($sr!=1){ $final_header .= '-'; }
                                $final_header .= $header[$sr];
                            }
                            $financial_statement_header_id[$temp] = $this->Company_model->addFinancialStatementHeader(array('company_financial_statement_id' => $company_financial_statement_id, 'header_name' => $final_header, 'parent_header_id' => 0));
                            if(!is_numeric($header[0]))
                            {
                                if (strpos($header[0], 'total') !== false) { $item_type = 'total'; }
                                $financial_statement_header_item_id = $this->Company_model->addFinancialStatementHeaderItem(array('financial_statement_header_id' => $financial_statement_header_id[$temp], 'header_item_name' => $header[0], 'item_type' => $item_type));
                            }
                        }
                    }
                    else
                    {
                        $this->Company_model->addFinancialItemData(array(
                            'financial_statement_header_item_id' => $financial_statement_header_item_id,
                            'column_header' => $excel_headers[$r],
                            'price' => str_replace(',','',$data[$s][$excel_headers[$r]])
                        ));
                    }
                }
            }
        }
        $result = '';

        //adding module status for financial sheets
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $crm_company_id, 'crm_module_type' => 'company', 'reference_id' => $financial_statement_id, 'reference_type' => 'financial'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $crm_company_id, 'crm_module_type' => 'company', 'reference_id' => $financial_statement_id, 'reference_type' => 'financial', 'created_by' => $user_id, 'updated_date_time' => date('Y-m-d H:i:s')));
        $financial_info = $this->Company_model->getFinancialStatements(array('id_financial_statement' => $financial_statement_id));
        $this->Activity_model->addActivity(array('activity_name' => 'Financial Sheet','activity_template' => $financial_info[0]['statement_name'].' information has been updated','module_type' => 'company','module_id' => $crm_company_id, 'activity_type' => 'financial','activity_reference_id' => $financial_statement_id,'created_by' => $user_id));

        $_GET = array('crm_company_id' => $crm_company_id, 'financial_statement_id' => $financial_statement_id, 'user_id' => $user_id);
        $result = $this->financialInformation_get();

        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function financialInformation_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_company_id', array('required'=> 'Crm company id required'));
        $this->form_validator->add_rules('financial_statement_id', array('required'=> 'Financial statement id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $financial_statement_details = $this->Company_model->getFinancialStatements(array('id_financial_statement' => $data['financial_statement_id']));
        //print_r($financial_statement_details); exit;
        $financial_statement = $this->Company_model->getCompanyFinancialStatement(array('financial_statement_id' => $data['financial_statement_id'], 'crm_company_id' => $data['crm_company_id']));


        $template = '';
        if($financial_statement_details[0]['statement_key']=='balance_sheet'){
            $template = REST_API_URL.'templates/balance_sheet.xlsx';
        }
        else if($financial_statement_details[0]['statement_key']=='income_statement'){
            $template = REST_API_URL.'templates/income_statement.xlsx';
        }
        else if($financial_statement_details[0]['statement_key']=='cash_flow_statement'){
            $template = REST_API_URL.'templates/cash_flow_statement.xlsx';
        }
        else if($financial_statement_details[0]['statement_key']=='ratio_analysis'){
            $template = REST_API_URL.'templates/ratio_analysis.xlsx';
        }

        if(empty($financial_statement))
        {
            $result = array('status'=>TRUE, 'message' =>'', 'data'=>array('data' => '','template' => $template));
            $this->response($result, REST_Controller::HTTP_OK);
        }


        $header_names = $this->Company_model->getFinancialHeaders(array('id_company_financial_statement' => $financial_statement[0]['id_company_financial_statement']));
        //echo "<pre>"; print_r($header_names); exit;
        $result = $this->Company_model->getFinancialInformation(array('id_company_financial_statement' => $financial_statement[0]['id_company_financial_statement']));
        $headers = array_keys($result[0]);

        $html = '<div class="col-sm-12 bal-sheet-wrap table-responsive pb20">';
        $html .= '<div class="bal-sheet-header">';
        $html .= '<h4 class="f13 bold dark-blue pt5 pb5 text-uppercase">'.$financial_statement[0]['statement_name'].'</h4>';
        $html .= '</div>';
        $html .= '<table class="table bal-sheet-table table-striped" width="100%" border="0" cellspacing="0" cellpadding="0">';
        $html .= '<thead><tr><th></th>';
        for($s=2;$s<count($headers);$s++){
            $html .='<th>'.$headers[$s].'</th>';
        }
        $html .='</tr></thead><tbody>';
        $sr=0;
        for($s=0;$s<count($header_names);$s++)
        {

            if($header_names[$s]['parent_header_id']){ $html .='<tr><td class="border-none" colspan="'.(count($headers)-1).'" ><span class="f14 dark-blue text-uppercase" >'.$header_names[$s]['header_name'].'</span></td></tr>'; }
            else { $html .='<tr><td class="border-none" colspan="'.(count($headers)-1).'" ><h4 ';
                   if(trim($header_names[$s]['header_name'])!=''){
                        $html .=' class="f13 bold darkblue-bg white-color pt5 pb5 text-uppercase text-center" ';
                   }
                   $html .='>'.$header_names[$s]['header_name'].'</h4></td></tr>'; }

            for($r=0;$r<$header_names[$s]['loop_count'];$r++)
            {
                if(isset($result[$sr]))
                {
                    $html .='<tr';
                    if(isset($result[$sr][$headers[1]]) && ($result[$sr][$headers[1]]=='total')){ $html .=' class="mod-total" '; }
                    $html .='>';
                    for($u=0;$u<count($headers);$u++)
                    {
                        if($u==0){
                            $html .= '<td';
                            if(isset($result[$sr][$headers[1]]) && ($result[$sr][$headers[1]]=='total')){ $html .=' class="text-uppercase" '; }
                            $html .=' title ="'.$result[$sr][$headers[$u]].'" >'.$result[$sr][$headers[$u]].'</td>';
                        }
                        else if($u==1){  }
                        else $html .='<td>'.'$'.number_format($result[$sr][$headers[$u]],2).'</td>';
                    }
                    $html .='</tr>';
                    $sr++;
                }
            }
        }
        $html .='</tbody></table>';

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>array('data' => $html,'template' => $template));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyFinancialInformation_get()
    {
        error_reporting(0);
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_company_id', array('required'=> 'Crm company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_financial_statements = $this->Company_model->getAllCompanyFinancialStatements($data['crm_company_id']);
        //print_r($company_financial_statements); exit;
        for($s=0;$s<count($company_financial_statements);$s++)
        {
            $header_names = $this->Company_model->getFinancialHeaders(array('id_company_financial_statement' => $company_financial_statements[$s]['id_company_financial_statement']));
            $result = $this->Company_model->getFinancialInformation(array('id_company_financial_statement' => $company_financial_statements[$s]['id_company_financial_statement']));
            $headers = array_keys($result[0]);

            $html = '<div class="col-sm-12 bal-sheet-wrap pb20 table-responsive">';
            $html .= '<div class="bal-sheet-header">';
            $html .= '<h4 class="f13 bold dark-blue pt5 pb5 text-uppercase">'.$company_financial_statements[$s]['statement_name'].'</h4>';
            $html .= '</div>';
            $html .= '<table class="table bal-sheet-table table-striped" width="100%" border="0" cellspacing="0" cellpadding="0">';
            $html .= '<thead><tr><th></th>';
            for($sr=2;$sr<count($headers);$sr++){
                $html .='<th>'.$headers[$sr].'</th>';
            }
            $html .='</tr></thead><tbody>';
            $sr=0;
            for($st=0;$st<count($header_names);$st++)
            {
                if($header_names[$st]['parent_header_id']){ $html .='<tr><td class="border-none" colspan="'.(count($headers)-1).'" ><span class="f14 dark-blue text-uppercase">'.$header_names[$st]['header_name'].'</span></td></tr>'; }
                else { $html .='<tr><td class="border-none" colspan="'.(count($headers)-1).'" ><h4 ';
                                          if(trim($header_names[$st]['header_name'])!=''){
                                               $html .=' class="f13 bold darkblue-bg white-color pt5 pb5 text-uppercase text-center" ';
                                          }
                                          $html .='>'.$header_names[$st]['header_name'].'</h4></td></tr>'; }

                for($r=0;$r<$header_names[$st]['loop_count'];$r++)
                {
                    if(isset($result[$sr]))
                    {
                        $html .='<tr';
                        if(isset($result[$sr][$headers[1]]) && ($result[$sr][$headers[1]]=='total')){ $html .=' class="mod-total" '; }
                        $html .='>';
                        for($u=0;$u<count($headers);$u++)
                        {
                            if($u==0){
                                $html .= '<td';
                                if(isset($result[$sr][$headers[1]]) && ($result[$sr][$headers[1]]=='total')){ $html .=' class="text-uppercase" '; }
                                $html .=' title="'.$result[$sr][$headers[$u]].'" >'.$result[$sr][$headers[$u]].'</td>';
                            }
                            else if($u==1){  }
                            else $html .='<td>'.'$'.number_format($result[$sr][$headers[$u]],2).'</td>';
                        }
                        $html .='</tr>';
                        $sr++;
                    }
                }
            }
            $html .='</tbody></table>';
            $company_financial_statements[$s]['sheet'] = $html;
        }

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$company_financial_statements);
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function assessmentQuestion_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['id_assessment_question']))
        {
            $result = $this->Company_model->getAssessmentQuestion($data);
        }
        else
        {
            $result = $this->Company_model->getAssessmentQuestionType($data);
            $order = 'DESC';
            if($result[0]['assessment_question_type_key']=='pre_disbursement_checklist'){ $order = 'ASC'; }
            for($s=0;$s<count($result);$s++)
            {
                if(!isset($data['product_id'])){ $data['product_id']=0; }
                if(!isset($data['collateral_type_id'])){ $data['collateral_type_id']=0; }
                $result[$s]['category'] = $this->Company_model->getAssessmentQuestionCategory(array('assessment_question_type_id' => $result[$s]['id_assessment_question_type'], 'company_id' => $data['company_id'], 'product_id' => $data['product_id'], 'collateral_type_id' => $data['collateral_type_id'], 'order' => $order));
                for($r=0;$r<count($result[$s]['category']);$r++)
                {
                    $result[$s]['category'][$r]['question'] = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $result[$s]['category'][$r]['id_assessment_question_category']));
                    for($sr=0;$sr<count($result[$s]['category'][$r]['question']);$sr++)
                    {
                        if(isset($result[$s]['category'][$r]['question'][$sr]['question_option']) && $result[$s]['category'][$r]['question'][$sr]['question_option']!=''){
                            $result[$s]['category'][$r]['question'][$sr]['question_option'] = json_decode($result[$s]['category'][$r]['question'][$sr]['question_option']);
                        }
                    }
                }
            }
        }
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function assessmentQuestionCategory_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $this->form_validator->add_rules('assessment_question_type_id', array('required'=> 'Question type id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Company_model->getAssessmentQuestionCategory($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function assessmentQuestionCategory_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo "<pre>"; print_r($data); exit;
        $this->form_validator->add_rules('assessment_question_category_name', array('required'=> 'Category name required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $collateral_type_id = 0;
        if(isset($data['collateral_type_id'])){ $collateral_type_id = $data['collateral_type_id']; }

        if(isset($data['id_assessment_question_category'])){
            $check_name = $this->Company_model->getAssessmentQuestionCategory(array('company_id' => $data['company_id'], 'assessment_question_category_name' => trim($data['assessment_question_category_name']),'id_assessment_question_category_not' => $data['id_assessment_question_category'], 'collateral_type_id' =>$collateral_type_id));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('assessment_question_category_name' => 'Name already exists'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $result = $this->Company_model->updateAssessmentQuestionCategory($data);
            $suc_msg = 'Question category updated successfully';
        }
        else
        {
            $check_name = $this->Company_model->getAssessmentQuestionCategory(array('company_id' => $data['company_id'], 'assessment_question_category_name' => trim($data['assessment_question_category_name']),'collateral_type_id' =>$collateral_type_id));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('assessment_question_category_name' => 'Name already exists'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $result = $this->Company_model->addAssessmentQuestionCategory($data);
            $suc_msg = 'Question category Added successfully';
        }

        $result = array('status'=>TRUE, 'message' =>$suc_msg, 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function assessmentQuestion_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(isset($data['question_option'])){ $data['question_option'] = json_encode($data['question_option']); }

        $this->form_validator->add_rules('assessment_question_category_id', array('required'=> 'Question category id required'));
        $this->form_validator->add_rules('assessment_question', array('required'=> 'question required'));
        $this->form_validator->add_rules('question_type', array('required'=> 'Question type required'));
        $this->form_validator->add_rules('question_option', array('required'=> 'Question option required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_assessment_question'])) {
            $check_name = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $data['assessment_question_category_id'], 'assessment_question' => trim($data['assessment_question']),'id_assessment_question_not' => $data['id_assessment_question']));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('assessment_question_category_id' => 'Question already exists'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $this->Company_model->updateAssessmentQuestion($data);
            $suc_msg = 'Question updated successfully.';
        }
        else {
            $check_name = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $data['assessment_question_category_id'], 'assessment_question' => trim($data['assessment_question'])));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('assessment_question_category_id' => 'Question already exists'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $this->Company_model->addAssessmentQuestion($data);
            $suc_msg = 'Question added successfully.';
        }
        $result = array('status'=>TRUE, 'message' =>$suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function assessment_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> 'company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Company_model->getAssessmentForCompany($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function allCompanyAssessments_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_company_id', array('required'=> 'crm company id required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_assessment = $this->Company_model->getCompanyAssessment($data);
        $crm_company_details = $this->Crm_model->getCompany($data['crm_company_id']);
        for($st=0;$st<count($company_assessment);$st++)
        {
            $data['type'] = $company_assessment[$st]['assessment_type'];
            $data['assessment_id'] = $company_assessment[$st]['id_assessment'];
            $data['assessment_key'] = $company_assessment[$st]['assessment_key'];

            if($data['type']=='item' || $data['type']=='item-canvas')
            {
                $company_assessment[$st]['data'] = $this->Company_model->getCompanyAssessment($data);
                for($s=0;$s<count($company_assessment[$st]['data']);$s++)
                {
                    $company_assessment[$st]['data'][$s]['item'] = $this->Company_model->getAssessmentItem(array('assessment_id' => $company_assessment[$st]['data'][$s]['assessment_id']));
                    for($r=0;$r<count($company_assessment[$st]['data'][$s]['item']);$r++)
                    {
                        $company_assessment[$st]['data'][$s]['item'][$r]['step'] = $this->Company_model->getCompanyAssessmentItemStep(array('crm_company_id' => $data['crm_company_id'], 'assessment_item_id' =>$company_assessment[$st]['data'][$s]['item'][$r]['id_assessment_item']));
                    }
                }
            }
            else if($data['type']='question')
            {
                $company_assessment[$st]['data'] = $this->Company_model->getAssessmentQuestionType(array('assessment_question_type_key' => $data['assessment_key']));
                for($s=0;$s<count($company_assessment[$st]['data']);$s++)
                {
                    $company_assessment[$st]['data'][$s]['category'] = $this->Company_model->getAssessmentQuestionCategory(array('assessment_question_type_id' => $company_assessment[$st]['data'][$s]['id_assessment_question_type'], 'company_id' => $data['company_id'], 'assessment_question_category_status' => 1, 'sector_id' => $crm_company_details[0]['sector_id'], 'sub_sector_id' => $crm_company_details[0]['sub_sector_id']));
                    for($r=0;$r<count($company_assessment[$st]['data'][$s]['category']);$r++)
                    {
                        $company_assessment[$st]['data'][$s]['category'][$r]['question'] = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $company_assessment[$st]['data'][$s]['category'][$r]['id_assessment_question_category'], 'question_status' => 1));
                        for($sr=0;$sr<count($company_assessment[$st]['data'][$s]['category'][$r]['question']);$sr++)
                        {
                            if(isset($company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['question_option']) && $company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['question_option']!=''){
                                $company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['question_option'] = json_decode($company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['question_option']);
                            }
                            $company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['answer'] = $this->Company_model->getAssessmentQuestionAnswer(array('assessment_question_id' => $company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['id_assessment_question'], 'crm_company_id' => $data['crm_company_id']));

                            if(isset($company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['answer']->assessment_answer) && $company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['answer']->assessment_answer!=''){
                                if($company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['question_type']=='checkbox')
                                    $company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['answer']->assessment_answer = json_decode($company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['answer']->assessment_answer);
                                else if($company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['question_type']=='file')
                                    $company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['answer']->assessment_answer = getImageUrl($company_assessment[$st]['data'][$s]['category'][$r]['question'][$sr]['answer']->assessment_answer,'file');
                            }
                        }
                    }
                }
            }
        }

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$company_assessment);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyAssessment_get()
    {
        $data = $this->input->get();
        //echo "<pre>"; print_r($data); exit;
        //$this->form_validator->add_rules('type', array('required'=> 'type(item,question) required'));
        //$this->form_validator->add_rules('crm_company_id', array('required'=> 'crm company id required'));
        //$this->form_validator->add_rules('company_id', array('required'=> 'company id required'));
        if(isset($data['type']) && $data['type']=='question') {
            //$this->form_validator->add_rules('company_id', array('required' => 'Company id required'));
            $this->form_validator->add_rules('crm_company_id', array('required' => 'crm company id required'));
            $this->form_validator->add_rules('assessment_key', array('required' => 'Assessment key required'));
        }
        else if(isset($data['type']) && $data['type']=='item') {
            $this->form_validator->add_rules('crm_company_id', array('required'=> 'crm company id required'));
            $this->form_validator->add_rules('assessment_id', array('required' => 'Assessment id required'));
        }

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }


        if(!isset($data['type'])) {
            $result = $this->Company_model->getCompanyAssessment($data);
            if(isset($data['crm_company_id']))
            {
                for($s=0;$s<count($result);$s++)
                {
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $result[$s]['assessment_id'], 'reference_type' => 'assessment'));
                    if(empty($module_status)){ $result[$s]['status'] = 0; }
                    else{ $result[$s]['status'] = 1; }
                }
            }
        }

        else if($data['type']=='item' || $data['type']=='item-canvas')
        {
            $crm_company_details = $this->Crm_model->getCompany($data['crm_company_id']);
            $data['company_id'] = $crm_company_details[0]['company_id'];
            $result = $this->Company_model->getCompanyAssessment($data);
            for($s=0;$s<count($result);$s++)
            {
                $result[$s]['item'] = $this->Company_model->getAssessmentItem(array('assessment_id' => $result[$s]['assessment_id']));
                for($r=0;$r<count($result[$s]['item']);$r++)
                {
                    //$result[$s]['item'][$r]['step'] = $this->Company_model->getCompanyAssessmentItemStep(array('company_assessment_id' => $result[$s]['id_company_assessment'], 'assessment_item_id' =>$result[$s]['item'][$r]['id_assessment_item']));
                    $result[$s]['item'][$r]['step'] = $this->Company_model->getCompanyAssessmentItemStep(array('crm_company_id' => $data['crm_company_id'], 'assessment_item_id' =>$result[$s]['item'][$r]['id_assessment_item']));
                }
            }
        }
        else if($data['type']='question')
        {
            $crm_company_details = $this->Crm_model->getCompany($data['crm_company_id']);
            $data['company_id'] = $crm_company_details[0]['company_id'];
            $result = $this->Company_model->getAssessmentQuestionType(array('assessment_question_type_key' => $data['assessment_key']));
            for($s=0;$s<count($result);$s++)
            {
                $result[$s]['category'] = $this->Company_model->getAssessmentQuestionCategory(array('assessment_question_type_id' => $result[$s]['id_assessment_question_type'], 'company_id' => $data['company_id'], 'assessment_question_category_status' => 1, 'sector_id' => $crm_company_details[0]['sector_id'], 'sub_sector_id' => $crm_company_details[0]['sub_sector_id']));
                for($r=0;$r<count($result[$s]['category']);$r++)
                {
                    $result[$s]['category'][$r]['question'] = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $result[$s]['category'][$r]['id_assessment_question_category'], 'question_status' => 1));
                    for($sr=0;$sr<count($result[$s]['category'][$r]['question']);$sr++)
                    {
                        if(isset($result[$s]['category'][$r]['question'][$sr]['question_option']) && $result[$s]['category'][$r]['question'][$sr]['question_option']!=''){
                            $result[$s]['category'][$r]['question'][$sr]['question_option'] = json_decode($result[$s]['category'][$r]['question'][$sr]['question_option']);
                        }
                        $result[$s]['category'][$r]['question'][$sr]['answer'] = $this->Company_model->getAssessmentQuestionAnswer(array('assessment_question_id' => $result[$s]['category'][$r]['question'][$sr]['id_assessment_question'], 'crm_company_id' => $data['crm_company_id']));

                        if(isset($result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer) && $result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer!=''){
                            if($result[$s]['category'][$r]['question'][$sr]['question_type']=='checkbox')
                                $result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer = json_decode($result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer);
                            else if($result[$s]['category'][$r]['question'][$sr]['question_type']=='file')
                                $result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer = getImageUrl($result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer);
                        }
                    }
                }
            }
        }

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyAssessment_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        //print_r($data); exit;
        //$this->form_validator->add_rules('crm_company_id', array('required'=> 'crm company id required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'crm company id required'));
        $this->form_validator->add_rules('created_by', array('required'=> 'created by required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else{
            if(!isset($data['assessments']) && empty($data['assessments'])){
                $result = array('status'=>FALSE,'error'=>'Please select assessments','data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

       //$crm_company_id = $data['crm_company_id'];
        $company_id = $data['company_id'];
        $assessments = $data['assessments'];
        $created_by = $data['created_by'];
        $add = $update = $delete = array();

        //$total_company_assessments = $this->Company_model->getCompanyAssessment(array('crm_company_id' => $crm_company_id, 'company_assessment_status' => 'all'));
        $total_company_assessments = $this->Company_model->getCompanyAssessment(array('company_id' => $company_id, 'company_assessment_status' => 'all'));
        $total_company_assessments = array_values(array_unique(array_map(function ($i) { return $i['assessment_id']; }, $total_company_assessments)));
        for($s=0;$s<count($assessments);$s++)
        {
            //$check_company_assessment = $this->Company_model->getCompanyAssessment(array('crm_company_id' => $crm_company_id, 'assessment_id' => $assessments[$s],'company_assessment_status' => 'all'));
            $check_company_assessment = $this->Company_model->getCompanyAssessment(array('company_id' => $company_id, 'assessment_id' => $assessments[$s],'company_assessment_status' => 'all'));
            if(empty($check_company_assessment)){
                $add[] = array(
                    'assessment_id' => $assessments[$s],
                    //'crm_company_id' => $crm_company_id,
                    'company_id' => $company_id,
                    'created_by' => $created_by,
                );
            }
            else{
                $update[] = array(
                    'id_company_assessment' => $check_company_assessment[0]['id_company_assessment'],
                    'assessment_id' => $assessments[$s],
                    //'crm_company_id' => $crm_company_id,
                    'company_id' => $company_id,
                    'created_by' => $created_by,
                    'company_assessment_status' => 1
                );
            }

            for($r=0;$r<count($total_company_assessments);$r++){
                if($assessments[$s]==$total_company_assessments[$r]){ unset($total_company_assessments[$r]); $total_company_assessments = array_values($total_company_assessments); }
            }

        }

        if(!empty($total_company_assessments)){
            foreach($total_company_assessments as $assessment_id){
                $delete[] = array(
                    //'crm_company_id' => $crm_company_id,
                    'company_id' => $company_id,
                    'assessment_id' => $assessment_id,
                    'company_assessment_status' => 0
                );
            }
        }

        if(!empty($add)){ $this->Company_model->addCompanyAssessment($add); }
        if(!empty($update)){ $this->Company_model->updateCompanyAssessment($update); }

        foreach($delete as $item){
            $this->Company_model->updateCompanyAssessmentByAssessmentId($item);
        }

        $result = array('status'=>TRUE, 'message' =>'Assessment updated successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyAssessmentItemStep_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        //print_r($data); exit;
        $this->form_validator->add_rules('crm_company_id', array('required'=> 'Crm company id required'));
        $this->form_validator->add_rules('assessment_item_id', array('required'=> 'Assessment item id required'));
        $this->form_validator->add_rules('step_title', array('required'=> 'Step title required'));
        $this->form_validator->add_rules('created_by', array('required'=> 'Created by required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $assessment_id = 0;
        //if(isset($data['crm_company_id'])){ $crm_company_id = $data['crm_company_id']; unset($data['crm_company_id']); }
        if(isset($data['assessment_id'])){ $assessment_id = $data['assessment_id']; unset($data['assessment_id']); }

        $this->Company_model->addCompanyAssessmentItemStep($data);

        if(isset($data['crm_company_id']) && isset($data['assessment_id']))
        {
            $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $data['assessment_id'], 'reference_type' => 'assessment'));
            if(empty($module_status))
                $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $data['assessment_id'], 'reference_type' => 'assessment', 'created_by' => $data['created_by'], 'updated_date_time' => date('Y-m-d H:i:s')));
            $this->Activity_model->addActivity(array('activity_name' => 'Assessment','activity_template' => 'Assessment answers added','module_type' => 'company','module_id' => $data['crm_company_id'], 'activity_type' => 'assessment','activity_reference_id' => $data['assessment_id'],'created_by' => $data['created_by']));
        }

        if($assessment_id){
            $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $assessment_id, 'reference_type' => 'assessment'));
            if(empty($module_status))
                $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $assessment_id, 'reference_type' => 'assessment', 'created_by' => $data['created_by'], 'updated_date_time' => date('y-m-d H:i:s')));
            $this->Activity_model->addActivity(array('activity_name' => 'Assessment','activity_template' => 'Assessment answers added','module_type' => 'company','module_id' => $data['crm_company_id'], 'activity_type' => 'assessment','activity_reference_id' => $assessment_id,'created_by' => $data['created_by']));
        }

        $result = array('status'=>TRUE, 'message' =>'Added successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyAssessmentItemStep_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_company_assessment_item_step', array('required'=> 'item id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Company_model->deleteCompanyAssessmentItemStep($data);
        $result = array('status'=>TRUE, 'message' =>'Deleted successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function assessmentAnswer_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        //print_r($_FILES);
        if(isset($_FILES['file']['name'])){
            $data = $data['answer'];
        }
            //echo "<pre>"; print_r($data); exit;
        $this->form_validator->add_rules('company_id', array('required'=> 'company id required'));
        $this->form_validator->add_rules('assessment_question_id', array('required'=> 'Question id required'));
        /*if(!isset($_FILES['file']['name']))
            $this->form_validator->add_rules('assessment_answer', array('required'=> 'Answer required'));*/
        $this->form_validator->add_rules('crm_reference_id', array('required'=> 'crm reference id required'));
        $this->form_validator->add_rules('created_by', array('required'=> 'Created by required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $crm_project_id = $crm_company_id = $id_assessment_question_type = $assessment_id = 0;

        if(isset($data['crm_project_id'])){ $crm_project_id = $data['crm_project_id']; unset($data['crm_project_id']); }
        if(isset($data['crm_company_id'])){ $crm_company_id = $data['crm_company_id']; unset($data['crm_company_id']); }
        if(isset($data['id_assessment_question_type'])){ $id_assessment_question_type = $data['id_assessment_question_type']; unset($data['id_assessment_question_type']); }
        if(isset($data['assessment_id'])){ $assessment_id = $data['assessment_id']; unset($data['assessment_id']); }

        if(!isset($data['comment'])){ $data['comment'] = ''; }

        if(isset($_FILES) && !empty($_FILES['file']['name']))
        {
            $path='uploads/';
            $fileName = doUpload($_FILES['file']['tmp_name'],$_FILES['file']['name'],$path,$data['company_id'],'');
            if(isset($data['type']) && $data['type']=='media'){
                $data['comment_file'] = $fileName;
            }
            else{
                $data['assessment_answer'] = $fileName;
            }

        }
        unset($data['company_id']); unset($data['type']);
        $question_details = $this->Company_model->getAssessmentQuestion(array('id_assessment_question' => $data['assessment_question_id']));
        //echo "<pre>"; print_r($question_details); exit;

        if($question_details[0]['question_type']=='checkbox')
            if(isset($data['assessment_answer'])){ $data['assessment_answer'] = json_encode($data['assessment_answer']); }

        if(isset($data['id_assessment_answer'])) {
            $this->Company_model->updateAssessmentAnswer($data);
            $suc_msg = 'Answer update successfully.';
        }
        else {
            $this->Company_model->addAssessmentAnswer($data);
            $suc_msg = 'Answer added successfully.';
        }

        if($crm_project_id && $id_assessment_question_type)
        {
            $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $crm_project_id, 'crm_module_type' => 'project', 'reference_id' => $id_assessment_question_type, 'reference_type' => 'assessment'));
            if(empty($module_status))
                $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $crm_project_id, 'crm_module_type' => 'project', 'reference_id' => $id_assessment_question_type, 'reference_type' => 'assessment', 'created_by' => $data['created_by'], 'updated_date_time' => date('Y-m-d H:i:s')));
            $this->Activity_model->addActivity(array('activity_name' => 'Assessment','activity_template' => 'Assessment answers added','module_type' => 'project','module_id' => $crm_project_id, 'activity_type' => 'assessment','activity_reference_id' => $id_assessment_question_type,'created_by' => $data['created_by']));

        }
        else if($crm_company_id && $assessment_id)
        {
            $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $crm_company_id, 'crm_module_type' => 'company', 'reference_id' => $assessment_id, 'reference_type' => 'assessment'));
            if(empty($module_status))
                $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $crm_company_id, 'crm_module_type' => 'company', 'reference_id' => $assessment_id, 'reference_type' => 'assessment', 'created_by' => $data['created_by'], 'updated_date_time' => date('Y-m-d H:i:s')));
            $this->Activity_model->addActivity(array('activity_name' => 'Assessment','activity_template' => 'Assessment answers added','module_type' => 'company','module_id' => $crm_company_id, 'activity_type' => 'assessment','activity_reference_id' => $assessment_id,'created_by' => $data['created_by']));
        }

        $result = array('status'=>TRUE, 'message' =>$suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    /*public function bulkAssessmentAnswer_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        print_r($data);
        print_r($_FILES); exit;
        if(isset($_FILES['file']['name'])){
            $data = $data['answer'];
        }

        $this->form_validator->add_rules('company_id', array('required'=> 'company id required'));
        $this->form_validator->add_rules('assessment_question_id', array('required'=> 'assessment question id required'));
        if(!isset($_FILES['file']['name']))
            $this->form_validator->add_rules('assessment_answer', array('required'=> 'Assessment answer required'));
        $this->form_validator->add_rules('crm_reference_id', array('required'=> 'crm reference id required'));
        $this->form_validator->add_rules('created_by', array('required'=> 'Created by required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($_FILES) && !empty($_FILES['file']['name']))
        {
            $path='uploads/';
            $fileName = doUpload($_FILES['file']['tmp_name'],$_FILES['file']['name'],$path,$data['company_id'],'');
            $data['assessment_answer'] = $fileName;
        }
        unset($data['company_id']);
        $question_details = $this->Company_model->getAssessmentQuestion(array('id_assessment_question' => $data['assessment_question_id']));
        //echo "<pre>"; print_r($question_details); exit;

        if($question_details[0]['question_type']=='checkbox')
            if(isset($data['assessment_answer'])){ $data['assessment_answer'] = json_encode($data['assessment_answer']); }

        if(isset($data['id_assessment_answer'])) {
            $this->Company_model->updateAssessmentAnswer($data);
            $suc_msg = 'Answer update successfully.';
        }
        else {
            $this->Company_model->addAssessmentAnswer($data);
            $suc_msg = 'Answer added successfully.';
        }
        $result = array('status'=>TRUE, 'message' =>$suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }*/

    public function assessmentAnswer_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_assessment_answer', array('required'=> 'assessment answer id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Company_model->deleteAssessmentAnswer($data);
        $result = array('status'=>TRUE, 'message' =>'Deleted successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function productTerm_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getProductTerms($data);
        //echo "<pre>"; print_r($result); exit;
        if(isset($data['crm_project_id']))
        {
            for($s=0;$s<count($result);$s++){
                $result[$s]['status'] = 0;
                if($result[$s]['product_term_key']=='risk_assessment')
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => 0, 'reference_type' => 'risk'));
                else if(($result[$s]['product_term_key']=='pre_disbursement_checklist')||($result[$s]['product_term_key']=='restriction_limit'))
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $result[$s]['id_product_term'], 'reference_type' => 'assessment'));
                else
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $result[$s]['id_product_term'], 'reference_type' => 'term'));
                if(!empty($module_status)){ $result[$s]['status'] = 1; }
            }
        }
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function productTermItem_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('product_term_key', array('required'=> 'Product term key required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $term = $this->Company_model->getProductTerms($data);
        //echo $term[0]['id_product_term']; exit;
        $result = array();
        if(!empty($term))
            $result = $this->Company_model->getProductTermItems(array('product_term_id' => $term[0]['id_product_term']));
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    //company risk
    public function riskCategory_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => 0));
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['sub_category'] = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => $result[$s]['id_risk_category']));
            for($r=0;$r<count($result[$s]['sub_category']);$r++)
            {
                $result[$s]['sub_category'][$r]['attributes'] = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => $result[$s]['sub_category'][$r]['id_risk_category']));
                for($u=0;$u<count($result[$s]['sub_category'][$r]['attributes']); $u++)
                {
                    $result[$s]['sub_category'][$r]['attributes'][$u]['sector_list'] = $this->Company_model->getRiskCategoryItemsSectors(array('risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['id_risk_category']));
                    for($h=0;$h<count($result[$s]['sub_category'][$r]['attributes'][$u]['sector_list']);$h++)
                    {
                        $result[$s]['sub_category'][$r]['attributes'][$u]['sector_list'][$h]['items'] = $this->Company_model->getRiskCategoryItems(array('risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['sector_list'][$h]['risk_category_id'], 'sector_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['sector_list'][$h]['id_sector']));
                    }
                }
            }
        }
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function riskCategory_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $this->form_validator->add_rules('risk_category_name', array('required'=> 'Category name required'));
        $this->form_validator->add_rules('parent_risk_category_id', array('required'=> 'Parent category id required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $this->form_validator->add_rules('risk_percentage', array('required'=> 'risk percentage required'));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }



        if(!isset($data['id_risk_category']))
        {
            //$check_name = $this->Company_model->getRiskCategories(array('risk_category_name' => trim($data['risk_category_name']), 'company_id' => $data['company_id'], 'parent_risk_category_id' => $data['parent_risk_category_id']));
            $check_name = $this->Company_model->getRiskCategories(array('risk_category_name' => trim($data['risk_category_name']), 'parent_risk_category_id' =>$data['parent_risk_category_id'], 'company_id' => $data['company_id']));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('risk_category_name' => 'Name already exits') ,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }

            $check_percentage = $this->Company_model->getTotalRiskCategoryPercentage(array('parent_risk_category_id' => $data['parent_risk_category_id'], 'company_id' => $data['company_id']));

            if(!empty($check_percentage) && ($check_percentage[0]['risk_percentage']+$data['risk_percentage'])>100){
                $result = array('status'=>FALSE,'error'=>array('risk_percentage' => 'Risk percentage exceeds'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $risk_category_id = $this->Company_model->addRiskCategory($data);
            $suc_msg = 'Risk category added successfully.';
        }
        else
        {
            //$check_name = $this->Company_model->getRiskCategories(array('risk_category_name' => trim($data['risk_category_name']), 'company_id' => $data['company_id'], 'parent_risk_category_id' => $data['parent_risk_category_id'],'risk_category_id_not' => $data['id_risk_category']));
            $check_name = $this->Company_model->getRiskCategories(array('risk_category_name' => trim($data['risk_category_name']), 'company_id' => $data['company_id'], 'risk_category_id_not' => $data['id_risk_category']));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('risk_category_name' => 'Name already exits') ,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }

            $check_percentage = $this->Company_model->getTotalRiskCategoryPercentage(array('parent_risk_category_id' => $data['parent_risk_category_id'],'company_id' => $data['company_id'], 'id_risk_category' => $data['id_risk_category']));
            //print_r($check_percentage); exit;
            if(($check_percentage[0]['risk_percentage']+$data['risk_percentage'])>100){
                $result = array('status'=>FALSE,'error'=>array('risk_percentage' => 'Risk percentage exceeds'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $this->Company_model->updateRiskCategory($data);
            $risk_category_id = $data['id_risk_category'];
            $suc_msg = 'Risk category updated successfully.';
        }

        $result = $this->Company_model->getRiskCategories(array('risk_category_id' => $risk_category_id));

        $result = array('status'=>TRUE, 'message' =>$suc_msg, 'data'=>$result[0]);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function riskCategoryItem_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $this->form_validator->add_rules('risk_category_id', array('required'=> 'Category id required'));
        $this->form_validator->add_rules('risk_category_item_name', array('required'=> 'category item name required'));
        $this->form_validator->add_rules('risk_category_item_grade', array('required'=> 'Category item grade required'));
        $this->form_validator->add_rules('sector_id', array('required'=> 'Sector id required'));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['id_risk_category_item']))
        {
            $check_name = $this->Company_model->getRiskCategoryItems(array('risk_category_id' => $data['risk_category_id'], 'risk_category_item_name' => trim($data['risk_category_item_name'])));
            //echo "<pre>"; print_r($check_name); exit;
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('risk_category_name' => 'Name already exits') ,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }

            $risk_category_item_id = $this->Company_model->addRiskCategoryItem($data);
            $suc_msg = 'Risk category Item added successfully.';
        }
        else
        {
            $check_name = $this->Company_model->getRiskCategoryItems(array('risk_category_id' => $data['risk_category_id'], 'risk_category_item_name' => trim($data['risk_category_item_name']), 'id_risk_category_item_not' => $data['id_risk_category_item']));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('risk_category_name' => 'Name already exits') ,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }

            $this->Company_model->updateRiskCategoryItem($data);
            $risk_category_item_id = $data['id_risk_category_item'];
            $suc_msg = 'Risk category Item updated successfully.';
        }
        $result = $this->Company_model->getRiskCategoryItems(array('risk_category_item_id' => $risk_category_item_id));
        $result = array('status'=>TRUE, 'message' =>$suc_msg, 'data'=>$result[0]);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function riskCategory_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('type', array('required'=> 'type required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        /*else if($data['type']=='category'){
            $this->form_validator->add_rules('risk_category_id', array('required'=> 'Risk category id required'));
            $validated = $this->form_validator->validate($data);
            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }

            $risk_Categories = $this->Company_model->getRiskCategories(array('parent_risk_category_id' => $data['risk_category_id']));
            $sub_risk_category_ids = array_values(array_unique(array_map(function ($i) { return $i['id_risk_category']; }, $risk_Categories)));

            $attributes = $this->Company_model->getRiskCategories(array('array_parent_risk_category_id' => $sub_risk_category_ids));
            $attributes_ids = array_values(array_unique(array_map(function ($i) { return $i['id_risk_category']; }, $attributes)));

            $this->Company_model->deleteRiskCategoryItem(array('array_risk_category_id' => $attributes_ids));
            $this->Company_model->deleteRiskCategory(array('array_parent_risk_category_id' => $sub_risk_category_ids));
            $this->Company_model->deleteRiskCategory(array('parent_risk_category_id' => $data['risk_category_id']));
            $this->Company_model->deleteRiskCategory(array('risk_category_id' => $data['risk_category_id']));

        }
        else if($data['type']=='sub_category'){
            $this->form_validator->add_rules('risk_category_id', array('required'=> 'Risk category id required'));
            $validated = $this->form_validator->validate($data);
            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $risk_Categories = $this->Company_model->getRiskCategories(array('parent_risk_category_id' => $data['risk_category_id']));
            $risk_category_ids = array_values(array_unique(array_map(function ($i) { return $i['id_risk_category']; }, $risk_Categories)));
            $this->Company_model->deleteRiskCategoryItem(array('array_risk_category_id' => $risk_category_ids));
            $this->Company_model->deleteRiskCategory(array('parent_risk_category_id' => $data['risk_category_id']));
            $this->Company_model->deleteRiskCategory(array('risk_category_id' => $data['risk_category_id']));
        }
        else if($data['type']=='attribute'){
            $this->form_validator->add_rules('risk_category_id', array('required'=> 'Risk category id required'));
            $validated = $this->form_validator->validate($data);
            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }

            $this->Company_model->deleteRiskCategoryItem(array('risk_category_id' => $data['risk_category_id']));
            $this->Company_model->deleteRiskCategory(array('risk_category_id' => $data['risk_category_id']));
        }
        else if($data['type']=='sector'){
            $this->form_validator->add_rules('sector_id', array('required'=> 'Sector id required'));
            $validated = $this->form_validator->validate($data);
            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $this->Company_model->deleteRiskCategoryItem(array('sector_id' => $data['sector_id']));
        }
        else if($data['type']=='item'){
            $this->form_validator->add_rules('risk_category_item_id', array('required'=> 'Risk category item id required'));
            $validated = $this->form_validator->validate($data);
            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $this->Company_model->deleteRiskCategoryItem(array('id_risk_category_item' => $data['risk_category_item_id']));
        }*/
        else if($data['type']=='category' || $data['type']=='sub_category' || $data['type']=='attribute' || $data['type']=='sector')
        {
            $this->form_validator->add_rules('risk_category_item_id', array('required'=> 'Risk category item id required'));
            $validated = $this->form_validator->validate($data);
            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $this->Company_model->updateRiskCategory(array('id_risk_category' => $data['risk_category_id'], 'risk_category_status' => 0));
        }
        else if($data['type']=='item')
        {
            $this->form_validator->add_rules('risk_category_item_id', array('required'=> 'Risk category item id required'));
            $validated = $this->form_validator->validate($data);
            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $this->Company_model->deleteRiskCategoryItem(array('id_risk_category_item' => $data['risk_category_item_id']));
        }
        else{
            $result = array('status'=>FALSE,'error'=>array('type' => 'Invalid type'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

    }

    public function knowledge_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $data = $data['data'];
        $this->form_validator->add_rules('document_title', array('required'=> 'Document title required'));
        $this->form_validator->add_rules('document_type', array('required'=> 'Document type required'));
        $this->form_validator->add_rules('uploaded_by', array('required'=> 'User id required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $this->form_validator->add_rules('knowledge_document_status', array('required'=> 'Document status required'));
        $this->form_validator->add_rules('tags', array('required'=> 'tags required'));
        //echo "<pre>"; print_r($data); exit;
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $title = $source = '';
        if(!isset($data['document_description'])){ $data['document_description'] = ''; }
        if($data['document_type']=='document')
        {
            if(isset($data['id_knowledge_document']) && empty($_FILES))
            {

            }
            else
            {
                $path='uploads/';
                if(isset($_FILES) && !empty($_FILES['file']['name']['document']))
                {
                    $title = $_FILES['file']['name']['document'];
                    $fineName = doUpload($_FILES['file']['tmp_name']['document'],$_FILES['file']['name']['document'],$path,$data['company_id'],'');
                    if($fineName===0){
                        $result = array('status' => FALSE, 'error' => 'Invalid format file', 'data' => '');
                        $this->response($result, REST_Controller::HTTP_OK); exit;
                    }
                    $source = $fineName;
                }
                else
                {
                    $result = array('status'=>FALSE,'error'=>'Please upload document','data'=>'');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
            }

        }
        else if($data['document_type']=='video')
        {
            $source = $data['document_video'];
        }
        else if($data['document_type']=='news')
        {
            $source = $data['document_video'];
        }

        if(isset($data['id_knowledge_document'])){
            if($data['document_type']=='document' && $source=='')
            {
                $document_id = $this->Company_model->updateKnowledgeDocument(array(
                    'id_knowledge_document' => $data['id_knowledge_document'],
                    'document_title' => $data['document_title'],
                    'document_type' => $data['document_type'],
                    'document_description' => $data['document_description'],
                    'uploaded_by' => $data['uploaded_by'],
                    'company_id' => $data['company_id'],
                    'knowledge_document_status' => $data['knowledge_document_status']
                ));
            }
            else
            {
                $document_id = $this->Company_model->updateKnowledgeDocument(array(
                    'id_knowledge_document' => $data['id_knowledge_document'],
                    'document_title' => $data['document_title'],
                    'document_type' => $data['document_type'],
                    'document_source' => $source,
                    'document_description' => $data['document_description'],
                    'uploaded_by' => $data['uploaded_by'],
                    'company_id' => $data['company_id'],
                    'knowledge_document_status' => $data['knowledge_document_status']
                ));
            }

            $suc_msg = 'Document updated successfully.';
        }
        else{
            $document_id = $this->Company_model->addKnowledgeDocument(array(
                'document_title' => $data['document_title'],
                'document_type' => $data['document_type'],
                'document_source' => $source,
                'document_description' => $data['document_description'],
                'uploaded_by' => $data['uploaded_by'],
                'company_id' => $data['company_id'],
                'knowledge_document_status' => $data['knowledge_document_status']
            ));
            $suc_msg = 'Document added successfully.';
        }


        $tags = array();
        for($s=0;$s<count($data['tags']);$s++){
            $tags[] = array(
                'knowledge_document_id' => $document_id,
                'tag_name' => $data['tags'][$s]
            );
        }
        if(isset($data['id_knowledge_document'])){
            $previous_tags = $this->Company_model->getKnowledgeDocumentTags(array('knowledge_document_id' => $data['id_knowledge_document']));
            $previous_tag_name = array_values(array_unique(array_map(function ($i) { return $i['tag_name']; }, $previous_tags)));
            $add = $delete = array();
            $tags_lower = array_map('strtolower', $data['tags']);
            $previous_tag_name = array_map('strtolower', $previous_tag_name);

            for($s=0;$s<count($data['tags']);$s++){
                if(trim($tags_lower[$s])!='')
                {
                    if(!in_array($tags_lower[$s],$previous_tag_name)){
                        $add[] = array(
                            'knowledge_document_id' => $data['id_knowledge_document'],
                            'tag_name' => $data['tags'][$s]
                        );
                    }
                }

            }

            for($s=0;$s<count($previous_tags);$s++){
                if(!in_array(trim(strtolower($previous_tags[$s]['tag_name'])),$tags_lower)){
                    array_push($delete,$previous_tags[$s]['id_knowledge_document_tag']);
                }
            }
            //echo "<pre>"; print_r($add); print_r($delete); exit;
            if(!empty($add)){ $this->Company_model->addKnowledgeDocumentTags($add); }
            if(!empty($delete)){ $this->Company_model->deleteKnowledgeTags($delete); }
            /*$tags = array();
            for($s=0;$s<count($data['tags']);$s++){
                $tags[] = array(
                    'knowledge_document_id' => $data['id_knowledge_document'],
                    'tag_name' => $data['tags'][$s]
                );
            }
            $this->Company_model->deleteKnowledgeTags(array('knowledge_document_id' => $data['id_knowledge_document']));
            $this->Company_model->addKnowledgeDocumentTags($tags);*/
        }
        else{
            $this->Company_model->addKnowledgeDocumentTags($tags);
        }


        $result = array('status'=>TRUE, 'message' =>$suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledgeTags_get()
    {
        $data = $this->input->get();
        $sector = $this->Master_model->getSectorsList(array());
        $assessment = $this->Company_model->getAssessment();
        $forms = $this->Company_model->getForms();
        $financial = $this->Company_model->getFinancialStatements(array());
        $term = $this->Company_model->getProductTerms(array());

        $result = $result1 = array();
        for($s=0;$s<count($sector);$s++)
        {
            $result[]['tag'] = $sector[$s]['sector_name'];
        }
        for($s=0;$s<count($assessment);$s++){
            $result[]['tag'] = $assessment[$s]['assessment_name'];
        }
        for($s=0;$s<count($forms);$s++){
            $result[]['tag'] = $forms[$s]['form_name'];
        }
        for($s=0;$s<count($financial);$s++){
            $result[]['tag'] = $financial[$s]['statement_name'];
        }
        for($s=0;$s<count($term);$s++){
            $result[]['tag'] = $term[$s]['product_term_name'];
        }

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledgeList_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $data['tags'] = array();
        if(!isset($data['search_key'])){
            $data['search_key'] = '';
            if(isset($data['search_tags'])){
                $data['search_key'] = json_decode($data['search_tags']);
            }
        }
        if(isset($data['search_key']) && !empty($data['search_key'])) {
            $tags = '(';
            for ($s = 0; $s < count($data['search_key']); $s++) {
                if ($s > 0) {
                    $tags .= ',';
                }
                $tags .= '"' . $data['search_key'][$s] . '"';
            }
            $tags .= ')';
            $data['tags'] = $data['search_key'] = $tags;
        }
        else{ $data['tags'] = array(); }

        $search_results = $this->Company_model->getKnowledgeDocuments($data);
        for($s=0;$s<count($search_results);$s++){
            $search_results[$s]['tag_names'] = str_replace('$$',',',$search_results[$s]['tags']);
            $search_results[$s]['tags'] = explode('$$',$search_results[$s]['tags']);
            $search_results[$s]['document_video'] = '';
            if($search_results[$s]['document_type']=='video')
            {
                $search_results[$s]['document_video'] = $search_results[$s]['document_source'];
                $search_results[$s]['document_source'] = '';
            }
            if($search_results[$s]['document_type']=='news')
            {
                $search_results[$s]['document_video'] = $search_results[$s]['document_source'];
                $search_results[$s]['document_source'] = '';
            }

            //$search_results[$s]['tags'] = array_combine(array_keys(array_flip($search_results[$s]['tags'])), $search_results[$s]['tags']);
        }

        $total_records = count($this->Company_model->getKnowledgeDocuments(array('search_key' => $data['tags'], 'company_id' => $data['company_id'])));
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>array('data' => $search_results, 'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);

    }

    public function knowledge_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['search_key']))
        {
            $search_results = $this->Company_model->getKnowledgeDocuments($data);
            $total_records = $this->Company_model->getKnowledgeDocuments(array('search_key' => $data['search_key'], 'company_id' => $data['company_id']));
            $result = array('status'=>TRUE, 'message' =>'', 'data'=>array('data' => $search_results, 'total_records' => $total_records));
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $document_type = array(
            array(
                'name' => 'Total',
                'label' => 'Total Files',
                'total' => 0
            ),
            array(
                'name' => 'document',
                'label' => 'Documents',
                'total' => 0
            ),
            array(
                'name' => 'blog',
                'label' => 'Blog Notes',
                'total' => 0
            ),
            array(
                'name' => 'news',
                'label' => 'News Items',
                'total' => 0
            ),
            array(
                'name' => 'video',
                'label' => 'Videos',
                'total' => 0
            )
        );

        if(isset($data['created_by']))
            $knowledge_documents = $this->Company_model->getTotalKnowledgeDocuments(array('company_id' => $data['company_id'], 'limit' => 5, 'created_by' => $data['created_by']));
        else
            $knowledge_documents = $this->Company_model->getTotalKnowledgeDocuments(array('company_id' => $data['company_id'], 'limit' => 5));

        if(isset($data['created_by']))
            $total_knowledge_documents = $this->Company_model->getTotalKnowledgeDocuments(array('company_id' => $data['company_id'], 'created_by' => $data['created_by']));
        else
            $total_knowledge_documents = $this->Company_model->getTotalKnowledgeDocuments(array('company_id' => $data['company_id']));
        $total_records_count = count($this->Company_model->getTotalKnowledgeDocuments(array('company_id' => $data['company_id'])));
        $document_type_records = $this->Company_model->getDocumentTypeKnowledgeDocuments(array('company_id' => $data['company_id']));
        for($s=0;$s<count($document_type_records);$s++)
        {
            for($r=0;$r<count($document_type);$r++)
            {
                if($document_type_records[$s]['document_type']==$document_type[$r]['name']){
                    $document_type[$r]['total'] = $document_type_records[$s]['total'];
                }
            }
        }

        for($s=0;$s<count($document_type);$s++)
        {
            if($document_type[$s]['name']=='Total')
            $document_type[$s]['total'] = $total_records_count;
        }
        $users = $this->Company_model->getKnowledgeDocumentsUsers(array('company_id' => $data['company_id']));
        for($s=0;$s<count($users);$s++)
        {
            $users[$s]['profile_image'] = getImageUrl($users[$s]['profile_image'],'profile');
        }

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>array('data' => $knowledge_documents, 'total_records' => $total_records_count, 'document_type_data' => $document_type, 'users' => $users));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledgeGraph_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
		
		$views = $this->Company_model->getKnowledgeViewsByMonth($data);
		$uploads = $this->Company_model->getKnowledgeUploadsByMonth($data);
		$result = array('status'=>TRUE, 'message' =>'', 'data'=>array('views' => $views, 'uploads' => $uploads));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getLowerApprovalRoles($company_id,$approval_id)
    {
        $data = $this->Company_model->getLowerApprovalRoles($company_id,$approval_id);
        return $data;
    }

    public function companyCurrencySelected_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_details = $this->Company_model->getCompanyById($data['company_id']);
        $data['except_currency_id'] = $company_details->currency_id;
        $result = $this->Company_model->getCompanyCurrencySelected($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyCurrency_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $company_details = $this->Company_model->getCompanyById($data['company_id']);
        $data['except_currency_id'] = $company_details->currency_id;
        $data['company_currency_status'] = 1;
        $result = $this->Company_model->getCompanyCurrencyDetails($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyCurrencyList_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->getCompanyCurrency($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyCurrency_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>'Invalid data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $this->form_validator->add_rules('currency_id', array('required'=> 'Currency required'));
        //$this->form_validator->add_rules('currency_value', array('required'=> 'currency value required', 'float' => 'Invalid value'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $previous_company_currencies = $this->Company_model->getCompanyCurrencyDetails(array('company_id' => $data['company_id']));
        $previous_company_currency_ids = array_map(function ($i) { return $i['currency_id']; }, $previous_company_currencies);
        $result = $add = $update = $update1 = array();

        for($s=0;$s<count($data['currency_id']);$s++)
        {
            if(!in_array($data['currency_id'][$s],$previous_company_currency_ids)){
                $add[] = array('company_id' => $data['company_id'], 'currency_id' => $data['currency_id'][$s]);
            }

        }

        for($s=0;$s<count($previous_company_currencies);$s++)
        {
            if(!in_array($previous_company_currencies[$s]['currency_id'],$data['currency_id'])){
                $update[] = array('id_company_currency' => $previous_company_currencies[$s]['id_company_currency'], 'company_currency_status' => 0);
            }
            else{
                $update1[] = array('id_company_currency' => $previous_company_currencies[$s]['id_company_currency'], 'company_currency_status' => 1);
            }
        }

        if(!empty($add)) $this->Company_model->addCompanyCurrencyBatch($add);
        if(!empty($update)) $this->Company_model->updateCompanyCurrencyBatch($update);
        if(!empty($update1)) $this->Company_model->updateCompanyCurrencyBatch($update1);


        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyCurrencyValue_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data))
        {
            $result = array('status'=>FALSE,'error'=>'Invalid data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $add = array();
        for($s=0;$s<count($data);$s++)
        {
           /*$this->form_validator->add_rules('company_currency_id', array('required'=> 'Company currency id required'));
            $this->form_validator->add_rules('company_currency_value', array('required'=> 'currency value required', 'float' => 'Invalid value'));
            $validated = $this->form_validator->validate($data[$s]);

            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }*/
            if((isset($data[$s]['company_currency_id']) && $data[$s]['company_currency_id']!=0 && $data[$s]['company_currency_id']!='') && (isset($data[$s]['company_currency_value']) && $data[$s]['company_currency_value']!='')) {
                $add[] = array('company_currency_id' => $data[$s]['company_currency_id'], 'company_currency_value' => $data[$s]['company_currency_value']);
            }
        }
        $result = array();
        if(!empty($add))
            $result = $this->Company_model->addCompanyCurrencyValue($add);

        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    /*public function getCompanyTouchPoints_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_company_id', array('required'=> 'Crm company id required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['id'] = $data['crm_company_id'];
        $company_details = $this->Crm_model->getCompany($data['crm_company_id']);

        if($company_details[0]['sub_sector']!=''){ $data['sector'] = $company_details[0]['sub_sector']; }
        else if($company_details[0]['sector']!=''){ $data['sector'] = $company_details[0]['sector']; }
        else{ $data['sector']=''; }
        $result = $this->Company_model->getCompanyTouchPoints($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectTouchPoints_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_project_id', array('required'=> 'Crm project id required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $project_details = $this->Crm_model->getProject($data['crm_project_id']);

        if($project_details[0]['sub_sector']!=''){ $data['sector'] = $project_details[0]['sub_sector']; }
        else if($project_details[0]['sector']!=''){ $data['sector'] = $project_details[0]['sector']; }
        else{ $data['sector']=''; }
        $result = $this->Company_model->getProjectTouchPoints($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactTouchPoints_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_contact_id', array('required'=> 'Crm contact id required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->getContactTouchPoints($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }*/


    public function facilityStatus_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_project_id', array('required'=> 'Crm project id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => 0, 'reference_type' => 'facility'));
        if(empty($module_status)){ $status = 0; }
        else { $status = 1; }

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$status);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function currency_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $results = $this->Company_model->getCompanyCurrencyDetails(array('company_id' => $data['company_id'], 'company_currency_status' => 1));

        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function primaryCurrency_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => 'Invalid Data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('company_id', array('required'=>'Company required'));
        $this->form_validator->add_rules('currency_id', array('required'=>'Currency required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data = array('currency_id' => $data['currency_id'], 'id_company' => $data['company_id']);
        $result = $this->Company_model->updatePrimaryCurrency($data);
        $result = array('status'=>TRUE, 'message' =>'Primary currency updated successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralType_get()
    {
        $data = $this->input->get();
        $result = $this->Company_model->getCollateralType();
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralStage_get()
    {
        $data = $this->input->get();
        if(isset($data['company_id']) && isset($data['collateral_type_id']))
            $results = $this->Company_model->getCollateralStageForCompany($data);
        else
            $results = $this->Company_model->getCollateralStage($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralTypeStage_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => 'Invalid Data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('company_id', array('required'=>'Company required'));
        $this->form_validator->add_rules('collateral_type_id', array('required'=>'Company required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }



        $add = $update = $delete = array();
        $stage_ids = $data['collateral_stage_ids'];
        //$total_company_assessments = $this->Company_model->getCompanyAssessment(array('crm_company_id' => $crm_company_id, 'company_assessment_status' => 'all'));
        $prev_data = $this->Company_model->getCollateralTypeStage($data);
        $prev_state_id = array_values(array_unique(array_map(function ($i) { return $i['collateral_stage_id']; }, $prev_data)));


        for($s=0;$s<count($stage_ids);$s++)
        {
            if(!in_array($stage_ids[$s],$prev_state_id)){
                $add[] = array(
                    'company_id' => $data['company_id'],
                    'collateral_type_id' => $data['collateral_type_id'],
                    'collateral_stage_id' => $stage_ids[$s]
                );
            }
        }

        for($s=0;$s<count($prev_data);$s++)
        {
            if(!in_array($prev_data[$s]['collateral_stage_id'],$stage_ids)){
                $this->Company_model->updateCollateralTypeStage(array('id_collateral_type_stage' => $prev_data[$s]['id_collateral_type_stage'],'collateral_type_id' => $data['collateral_type_id'],'collateral_stage_id' => $prev_data[$s]['collateral_stage_id'],'status' => 0));
            }
            else
            {
                $this->Company_model->updateCollateralTypeStage(array('id_collateral_type_stage' => $prev_data[$s]['id_collateral_type_stage'],'collateral_type_id' => $data['collateral_type_id'],'collateral_stage_id' => $prev_data[$s]['collateral_stage_id'],'status' => 1));
            }
        }

        if(!empty($add)){ $this->Company_model->addCollateralTypeStage($add); }
        //if(!empty($update)){ $this->Company_model->updateCollateralTypeStage($update); }
        //if(!empty($delete)){ $this->Company_model->updateCollateralTypeStage($delete); }

        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralTypeFields_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('collateral_type_id', array('required'=> 'Collateral type id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $results = $this->Company_model->getCollateralTypeFields($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralTypeStage_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('collateral_type_id', array('required'=> 'Collateral type id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $results = $this->Company_model->getCollateralTypeStage($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralStageFields_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('collateral_stage_id', array('required'=> 'Collateral stage id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $results = $this->Company_model->getCollateralStageFields($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenantType_get()
    {
        $data = $this->input->get();
        $results = $this->Company_model->getCovenantType($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenantCategoryList_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('covenant_type_key', array('required'=> $this->lang->line('covenant_type_key_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Company_model->getCovenantCategoryList($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenantCategory_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('covenant_type_key', array('required'=> $this->lang->line('covenant_type_key_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $covenant_category_id = array();
        $category = $this->Company_model->getCovenantCategory($data);
        //echo "<pre>"; print_r($category); exit;
        for($s=0;$s<count($category);$s++)
        {
            if(!in_array($category[$s]['id_covenant_category'],$covenant_category_id)){
                array_push($covenant_category_id,$category[$s]['id_covenant_category']);

                $result[$category[$s]['id_covenant_category']] = array(
                    'id_covenant_category' => $category[$s]['id_covenant_category'],
                    'covenant_category' => $category[$s]['covenant_category'],
                    'covenant_type_id' => $category[$s]['covenant_type_id'],
                    'company_id' => $category[$s]['company_id'],
                    'sector' => $category[$s]['sector'],
                    'covenant' => array()
                );
                if($category[$s]['id_covenant']!='')
                    $result[$category[$s]['id_covenant_category']]['covenant'][] = array(
                        'covenant_category_id' => $category[$s]['covenant_category_id'],
                        'id_covenant' => $category[$s]['id_covenant'],
                        'covenant_name' => $category[$s]['covenant_name'],
                        'covenant_status'=> $category[$s]['covenant_status']
                    );
            }
            else{
                if($category[$s]['id_covenant']!='')
                    $result[$category[$s]['id_covenant_category']]['covenant'][] = array(
                        'covenant_category_id' => $category[$s]['covenant_category_id'],
                        'id_covenant' => $category[$s]['id_covenant'],
                        'covenant_name' => $category[$s]['covenant_name'],
                        'covenant_status'=> $category[$s]['covenant_status']
                    );
            }
        }
        $result = array_values($result);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenantCategory_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('covenant_type_key', array('required'=> $this->lang->line('covenant_type_key_req')));
        $this->form_validator->add_rules('sector', array('required'=> $this->lang->line('covenant_sector_req')));
        $this->form_validator->add_rules('covenant_category', array('required'=> $this->lang->line('covenant_category_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            if(isset($data['covenant_type_key'])){
                $covenant_type_details = $this->Company_model->getCovenantType(array('covenant_type_key' => $data['covenant_type_key']));
                if(!empty($covenant_type_details))
                    $data['covenant_type_id'] = $covenant_type_details[0]['id_covenant_type'];
            }
            $covenant_category_id = 0;
            if(isset($data['id_covenant_category'])){ $covenant_category_id = $data['id_covenant_category']; }
            $check_name = $this->Company_model->getCovenantCategory(array('company_id' => $data['company_id'], 'covenant_category' => trim($data['covenant_category']),'covenant_type_id' => $data['covenant_type_id'], 'covenant_category_id_not' => $covenant_category_id));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('covenant_category' => $this->lang->line('covenant_category_unique')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        if(isset($data['id_covenant_category']))
        {
            $results = $this->Company_model->updateCovenantCategory(array(
                'id_covenant_category' => $data['id_covenant_category'],
                'covenant_type_id' => $data['covenant_type_id'],
                'covenant_category' => $data['covenant_category'],
                'sector' => $data['sector'],
                'company_id' => $data['company_id']
            ));
            $suc = $this->lang->line('covenant_category_update');
        }
        else{

            $results = $this->Company_model->addCovenantCategory(array(
                'covenant_type_id' => $data['covenant_type_id'],
                'covenant_category' => $data['covenant_category'],
                'sector' => $data['sector'],
                'company_id' => $data['company_id']
            ));
            $suc = $this->lang->line('covenant_category_add');
        }

        $result = array('status'=>TRUE, 'message' =>$suc, 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenant_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('covenant_category_id', array('required'=> $this->lang->line('covenant_category_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $results = $this->Company_model->getCovenant($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenant_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('covenant_category_id', array('required'=> $this->lang->line('covenant_category_id_req')));
        $this->form_validator->add_rules('covenant_name', array('required'=> $this->lang->line('covenant_name_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $covenant_id = 0;
            if(isset($data['id_covenant'])){ $covenant_id = $data['id_covenant']; }
            $check_name = $this->Company_model->getCovenant(array('covenant_category_id' => $data['covenant_category_id'], 'covenant_name' => trim($data['covenant_name']),'covenant_id_not' => $covenant_id));
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('covenant_name' => $this->lang->line('covenant_name_unique')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(!isset($data['covenant_status'])){ $data['covenant_status']=1; }
        if(isset($data['id_covenant'])){
            $results = $this->Company_model->updateCovenant(array(
                'id_covenant' => $data['id_covenant'],
                'covenant_category_id' => $data['covenant_category_id'],
                'covenant_name' => $data['covenant_name'],
                'covenant_status' => $data['covenant_status'],
                'created_by' => $data['created_by']
            ));
            $suc = $this->lang->line('covenant_update');
        }
        else{
            $covenant_id = $this->Company_model->addCovenant(array(
                'covenant_category_id' => $data['covenant_category_id'],
                'covenant_name' => $data['covenant_name'],
                'covenant_status' => $data['covenant_status'],
                'created_by' => $data['created_by']
            ));
            $suc = $this->lang->line('covenant_add');

            if(isset($data['crm_project_id'])){
                $this->Company_model->addProjectCovenant(
                    array(
                        'covenant_id' => $covenant_id,
                        'project_id' => $data['crm_project_id'],
                        'created_by' => $data['created_by']
                    )
                );
            }
        }



        $result = array('status'=>TRUE, 'message' =>$suc, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    /*public function module_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('application_role_id', array('required'=> $this->lang->line('application_role_id')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(!isset($data['type'])){ $data['type'] = 1; }
        $results = $this->Company_model->getModules($data);
        $data['status']=1;
        $approval_role = $this->Company_model->getApplicationUserRoleApprovalRole($data);
        unset($data['status']);
        $approval_role = array_map(function($s){ return $s['company_approval_role_id']; },$approval_role);
        $result = array();
        $module_ids = array();
        if(isset($data['type']) && $data['type']==1)
        {
            for($s=0;$s<count($results);$s++)
            {
                if(!in_array($results[$s]['id_module'],$module_ids)) {
                    array_push($module_ids,$results[$s]['id_module']);
                    $result[$results[$s]['id_module']] = array(
                        'id_module' => $results[$s]['id_module'],
                        'module_name' => $results[$s]['module_name'],
                        'module_key' => $results[$s]['module_key'],
                        'module_url' => $results[$s]['module_url'],
                        'checked' => '0',
                        'page' => array()
                    );
                }

                for($t=0;$t<count($results);$t++)
                {
                    if($results[$t]['module_id']==$results[$s]['id_module'])
                    {
                        $result[$results[$s]['id_module']]['page'][$results[$t]['id_module_page']] = array(
                            'id_module_page' => $results[$t]['id_module_page'],
                            'module_id' => $results[$t]['module_id'],
                            'page_name' => $results[$t]['page_name'],
                            'page_key' => $results[$t]['page_key'],
                            'page_url' => $results[$t]['page_url'],
                            'checked' => '0',
                            'action' => array()
                        );
                        for($h=0;$h<count($results);$h++)
                        {
                            if($results[$t]['module_page_id']==$results[$h]['id_module_page']){
                                //getting parent actions
                                if($results[$h]['parent_action_id']==0)
                                {
                                    $result[$results[$s]['id_module']]['page'][$results[$t]['id_module_page']]['action'][$results[$h]['id_page_action']] = array(
                                        'id_page_action' => $results[$h]['id_page_action'],
                                        'module_page_id' => $results[$h]['module_page_id'],
                                        'parent_action_id' => $results[$h]['parent_action_id'],
                                        'action_name' => $results[$h]['action_name'],
                                        'action_key' => $results[$h]['action_key'],
                                        'action_url' => $results[$h]['action_url'],
                                        'checked' => $results[$h]['checked'],
                                        'child' => array()
                                    );
                                    if($results[$h]['checked']==1){
                                        $result[$results[$s]['id_module']]['page'][$results[$t]['id_module_page']]['checked'] = $results[$h]['checked'];
                                        $result[$results[$s]['id_module']]['checked'] = $results[$h]['checked'];
                                    }
                                    for($a=0;$a<count($results);$a++)
                                    {
                                        if($results[$h]['id_page_action']==$results[$a]['parent_action_id'])
                                        {
                                            $result[$results[$s]['id_module']]['page'][$results[$t]['id_module_page']]['action'][$results[$h]['id_page_action']]['child'][] = array(
                                                'id_page_action' => $results[$a]['id_page_action'],
                                                'module_page_id' => $results[$a]['module_page_id'],
                                                'parent_action_id' => $results[$a]['parent_action_id'],
                                                'action_name' => $results[$a]['action_name'],
                                                'action_key' => $results[$a]['action_key'],
                                                'action_url' => $results[$a]['action_url'],
                                                'checked' => $results[$a]['checked'],
                                            );

                                            if($results[$a]['checked']==1){
                                                $result[$results[$s]['id_module']]['page'][$results[$t]['id_module_page']]['checked'] = $results[$a]['checked'];
                                                $result[$results[$s]['id_module']]['checked'] = $results[$a]['checked'];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else{ $result = $results; }
        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>$result, 'approval_role_id' => $approval_role);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function module_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => 'Invalid Data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('application_role_id', array('required'=> $this->lang->line('application_role_id')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else if(!isset($data['data'])){
            $result = array('status'=>FALSE,'error'=>array('data' => 'Module data required'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['approval_role_id']))
        {
            $add = $update = array();
            $approval_roles = $data['approval_role_id'];

            $prev_roles = $this->Company_model->getApplicationUserRole(array('application_role_id' => $data['application_role_id']));
            //echo "<pre>"; print_r($prev_roles); exit;
            $prev_approval_roles = array_map(function($i){ return $i['company_approval_role_id']; },$prev_roles);
            echo "<pre>"; print_r($prev_approval_roles);
            for($s=0;$s<count($approval_roles);$s++)
            {
                if(!in_array($approval_roles[0],$prev_approval_roles)){
                    $add[] = array(
                        'application_role_id' => $data['application_role_id'],
                        'company_approval_role_id' => $approval_roles[$s]
                    );
                }
                else{
                    $update[] = array(
                        'application_role_id' => $data['application_role_id'],
                        'company_approval_role_id' => $approval_roles[$s],
                        'status' => 1
                    );
                }
            }
            for($s=0;$s<count($prev_roles);$s++){
                if(!in_array($prev_roles[$s]['company_approval_role_id'],$approval_roles)){
                    $update[] = array(
                        'application_role_id' => $data['application_role_id'],
                        'company_approval_role_id' => $prev_roles[$s]['company_approval_role_id'],
                        'status' => 0
                    );
                }
            }

            if(!empty($add)) $this->Company_model->addApplicationUserRoleBatch($add);
            if(!empty($update)){
                for($s=0;$s<count($update);$s++){
                    $this->Company_model->updateApplicationUserRole($update[$s]);
                }
            }
        }



        $prev_data = $this->Company_model->getApplicationRoleAccess($data);
        $prv_action_id = array_values(array_unique(array_map(function ($i) { return $i['page_action_id']; }, $prev_data)));
        $add = $update = $new_page_action_id = array();

        for($s=1;$s<count($data['data']);$s++)
        {
            for($t=0;$t<count($data['data'][$s]['page']);$t++)
            {
                for($h=0;$h<count($data['data'][$s]['page'][$t]['action']);$h++)
                {
                    if(!empty($data['data'][$s]['page'][$t]['action'][$h]['child']))
                    {
                        for($a=0;$a<count($data['data'][$s]['page'][$t]['action'][$h]['child']);$a++)
                        {
                            if(!in_array($data['data'][$s]['page'][$t]['action'][$h]['child'][$a]['id_page_action'],$prv_action_id)){
                                $add[] = array(
                                    'module_id' => $data['data'][$s]['id_module'],
                                    'page_id' => $data['data'][$s]['page'][$t]['id_module_page'],
                                    'page_action_id' => $data['data'][$s]['page'][$t]['action'][$h]['child'][$a]['id_page_action'],
                                    'application_role_id' => $data['application_role_id'],
                                    'company_id' => $data['company_id']
                                );
                            }
                            else
                            {
                                $update[] = array(
                                    'page_action_id' => $data['data'][$s]['page'][$t]['action'][$h]['child'][$a]['id_page_action'],
                                    'application_role_id' => $data['application_role_id'],
                                    'company_id' => $data['company_id'],
                                    'status' => 1
                                );
                            }
                            array_push($new_page_action_id,$data['data'][$s]['page'][$t]['action'][$h]['child'][$a]['id_page_action']);
                        }
                    }
                    else
                    {
                        if(!in_array($data['data'][$s]['page'][$t]['action'][$h]['id_page_action'],$prv_action_id)){
                            $add[] = array(
                                'module_id' => $data['data'][$s]['id_module'],
                                'page_id' => $data['data'][$s]['page'][$t]['id_module_page'],
                                'page_action_id' => $data['data'][$s]['page'][$t]['action'][$h]['id_page_action'],
                                'application_role_id' => $data['application_role_id'],
                                'company_id' => $data['company_id']
                            );
                        }
                        else
                        {
                            $update[] = array(
                                'page_action_id' => $data['data'][$s]['page'][$t]['action'][$h]['id_page_action'],
                                'application_role_id' => $data['application_role_id'],
                                'company_id' => $data['company_id'],
                                'status' => 1
                            );
                        }
                        array_push($new_page_action_id,$data['data'][$s]['page'][$t]['action'][$h]['id_page_action']);
                    }
                }
            }
        }

        for($s=0;$s<count($prev_data);$s++)
        {
            if(!in_array($prev_data[$s]['page_action_id'],$new_page_action_id))
            {
                $update[] = array(
                    'page_action_id' => $prev_data[$s]['page_action_id'],
                    'application_role_id' => $prev_data[$s]['application_role_id'],
                    'company_id' => $prev_data[$s]['company_id'],
                    'status' => 0
                );
            }
        }

        if(!empty($add)) $this->Company_model->addApplicationRoleAccess($add);
        if(!empty($update)){
            for($s=0;$s<count($update);$s++){
                $this->Company_model->updateApplicationRoleAccess($update[$s]);
            }
        }

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('module_access_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }*/

    public function getChildNodes($data,$parent_id,$array)
    {
        for($s=0;$s<count($data);$s++){
            if(isset($data[$s])){
                if($data[$s]['parent_module_id']==$parent_id){

                    $array[$data[$s]['id_module']] = array(
                        'id_module' => $data[$s]['id_module'],
                        'parent_module_id' => $data[$s]['parent_module_id'],
                        'module_name' => $data[$s]['module_name'],
                        'module_key' => $data[$s]['module_key'],
                        'module_url' => $data[$s]['module_url'],
                        'sub_module' => $data[$s]['sub_module'],
                        'childs' => array(),
                        'action' => array(),
                        'checked' => $data[$s]['checked']
                    );

                    if( $data[$s]['sub_module']==0){
                         for($st=0;$st<count($data);$st++){
                             if($data[$s]['id_module']==$data[$st]['module_id']){
                                 $array[$data[$s]['id_module']]['action'][] = array(
                                     'id_module_action' => $data[$st]['id_module_action'],
                                     'action_name' => $data[$st]['action_name'],
                                     'action_key' => $data[$st]['action_key'],
                                     'action_url' => $data[$st]['action_url'],
                                     'checked' => $data[$st]['checked']
                                 );
                             }
                         }
                    }

                    $child = $this->getChildNodes($data,$data[$s]['id_module'],$array[$data[$s]['id_module']]['childs']);

                    if(!empty($child)){
                        $array[$data[$s]['id_module']]['childs'] = array_values($child);
                    }
                    else{
                        $array[$data[$s]['id_module']]['childs'] = $child;
                    }
                }
            }
        }
        return $array;
    }

    public function module_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('application_role_id', array('required'=> $this->lang->line('application_role_id')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(!isset($data['type'])){ $data['type'] = 1; }
        $result = $this->Company_model->getModules($data);

        //getting modules in array format
        $final_data = $this->getChildNodes($result,0,array());
        $final_data = array_values($final_data);

        $result = array('status'=>TRUE, 'message' =>'success', 'data'=>$final_data);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getCheckedActions($data,$application_role,$module_access_action_ids)
    {
        for($s=0;$s<count($data);$s++)
        {
            if($data[$s]['sub_module']==1)
            {
                $this->getCheckedActions($data[$s]['childs'],$application_role,$module_access_action_ids);
            }
            else
            {
                for($t=0;$t<count($data[$s]['action']);$t++)
                {
                    if(isset($data[$s]['action'][$t]))
                    if($data[$s]['action'][$t]['checked']==1)
                    {
                        if(in_array($data[$s]['action'][$t]['id_module_action'],$module_access_action_ids)){
                            $this->Company_model->updateModuleAccess(array('module_action_id' => $data[$s]['action'][$t]['id_module_action'],'application_role_id' => $application_role, 'module_access_status' => 1));
                        }
                        else{
                            $this->Company_model->addModuleAccess(array('module_id' => $data[$s]['id_module'], 'module_action_id' => $data[$s]['action'][$t]['id_module_action'],'application_role_id' => $application_role, 'module_access_status' => 1));
                        }
                        $this->ordered_data[] =array('module_id' => $data[$s]['id_module'], 'module_action_id' => $data[$s]['action'][$t]['id_module_action'],'application_role_id' => $application_role);
                    }
                }
            }
        }

        return $this->ordered_data;
    }

    public function module_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => 'Invalid Data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('application_role_id', array('required'=> $this->lang->line('application_role_id')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else if(!isset($data['data'])){
            $result = array('status'=>FALSE,'error'=>array('data' => 'Module data required'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $module_access = $this->Company_model->getModuleAccess(array('application_role_id' => $data['application_role_id']));
        $module_access_action_ids = array_map(function($sr){ return $sr['module_action_id']; },$module_access);
        $this->ordered_data = array();
        $module_action_post = $this->getCheckedActions($data['data'],$data['application_role_id'],$module_access_action_ids);
        $module_action_post_ids = array_map(function($sr){ return $sr['module_action_id']; },$module_action_post);

        for($s=0;$s<count($module_access);$s++)
        {
            if(!in_array($module_access[$s]['module_action_id'],$module_action_post_ids)){
                $this->Company_model->updateModuleAccess(array('module_action_id' => $module_access[$s]['module_action_id'],'application_role_id' => $data['application_role_id'], 'module_access_status' => 0));
            }
        }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('module_access_update'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function modulePage_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => 'Invalid Data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('module_id', $this->lang->line('module_id_req'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $results = $this->Company_model->getModulePage($data);
        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function applicationRole_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => 'Invalid Data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $results = $this->Company_model->getApplicationRole($data);
        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function applicationRole_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('application_role_name', array('required'=> $this->lang->line('application_role_name_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $application_role_id = 0;
            if(isset($data['id_application_role'])){ $application_role_id = $data['id_application_role']; }
            $check_name = $this->Company_model->getApplicationRole(array('company_id' => $data['company_id'], 'application_role_name' => trim($data['application_role_name']), 'id_application_role_not' => $application_role_id));
            //echo "<pre>"; print_r($check_name); exit;
            if(!empty($check_name)){
                $result = array('status'=>FALSE,'error'=>array('application_role_name' => $this->lang->line('application_role_name_unique')),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }
        if(isset($data['id_application_role'])){
            $results = $this->Company_model->updateApplicationRole(array(
                'id_application_role' => $data['id_application_role'],
                'application_role_name' => $data['application_role_name']
            ));
            $suc = $this->lang->line('application_role_name_update');
        }
        else{
            $results = $this->Company_model->addApplicationRole(array(
                'application_role_name' => $data['application_role_name'],
                'company_id' => $data['company_id']
            ));
            $suc = $this->lang->line('application_role_name_add');
        }

        $result = array('status'=>TRUE, 'message' =>$suc, 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function applicationUserRole_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => 'Invalid Data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('application_role_id', array('required'=> $this->lang->line('application_role_id')));
        $this->form_validator->add_rules('type', array('required'=> $this->lang->line('type_application_user_role_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->getApplicationUserRole($data);
        for($s=0;$s<count($result);$s++)
        {
            if($result[$s]['profile_image']!='')
                $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
        }
        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function applicationUserRole_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('application_role_id', array('required'=> $this->lang->line('application_role_req')));
        if((!isset($data['company_approval_role_id']) && !isset($data['user_id'])) || ((isset($data['company_approval_role_id']) && $data['company_approval_role_id']=='') && $data['user_id']==''))
            $this->form_validator->add_rules('company_approval_role_id', array('required'=> $this->lang->line('company_approval_role_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['company_approval_role_id']) && $data['company_approval_role_id']!='')
        {
            $check = $this->Company_model->getApplicationUserRole(array('company_id' => $data['company_id'],'company_approval_role_id' => $data['company_approval_role_id'],'status' => 'all'));
            //echo "<pre>"; print_r($check); exit;

            if(isset($data['id_application_user_role'])){
                $results = $this->Company_model->updateApplicationUserRole(array(
                    'id_application_user_role' => $data['id_application_user_role'],
                    'application_role_id' => $data['application_role_id'],
                    'company_approval_role_id' => $data['company_approval_role_id']
                ));
                $suc = $this->lang->line('application_user_role_update');
            }
            else{

                if(!empty($check)){
                    if($check[0]['application_role_id']==$data['application_role_id'])
                    {
                        $results = $this->Company_model->updateApplicationUserRole(array(
                            'id_application_user_role' => $check[0]['id_application_user_role'],
                            'application_role_id' => $check[0]['application_role_id'],
                            'company_approval_role_id' => $check[0]['company_approval_role_id'],
                            'status' => 1
                        ));
                        $suc = $this->lang->line('application_user_role_update');
                    }
                    else{
                        $result = array('status'=>FALSE,'error'=>array('company_approval_role_id' => str_replace('%s',$check[0]['application_role_name'],$this->lang->line('approval_role_application_role_unique'))),'data'=>'');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }

                }
                else
                {
                    $results = $this->Company_model->addApplicationUserRole(array(
                        'application_role_id' => $data['application_role_id'],
                        'company_approval_role_id' => $data['company_approval_role_id']
                    ));
                    $suc = $this->lang->line('application_user_role_add');
                }
            }
        }
        else if(isset($data['user_id']) && $data['user_id']!='')
        {
            if(isset($data['id_application_user_role'])){
                $results = $this->Company_model->updateApplicationUserRole(array(
                    'id_application_user_role' => $data['id_application_user_role'],
                    'application_role_id' => $data['application_role_id'],
                    'user_id' => $data['user_id']
                ));
                $suc = $this->lang->line('application_user_role_update');
            }
            else{
                $check = $this->Company_model->getApplicationUserRole(array('company_id' => $data['company_id'],'user_id' => $data['user_id'],'status' => 'all'));
                //echo "<pre>"; print_r($check); exit;
                if(!empty($check)){
                    if($check[0]['application_role_id']==$data['application_role_id'])
                    {
                        $results = $this->Company_model->updateApplicationUserRole(array(
                            'id_application_user_role' => $check[0]['id_application_user_role'],
                            'application_role_id' => $check[0]['application_role_id'],
                            'user_id' => $check[0]['user_id'],
                            'status' => 1
                        ));
                        $suc = $this->lang->line('application_user_role_update');
                    }
                    else
                    {
                        $result = array('status'=>FALSE,'error'=>array('user_id' => str_replace('%s',$check[0]['application_role_name'],$this->lang->line('user_application_role_unique'))),'data'=>'');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }
                }
                else
                {
                    $results = $this->Company_model->addApplicationUserRole(array(
                        'application_role_id' => $data['application_role_id'],
                        'user_id' => $data['user_id']
                    ));
                    $suc = $this->lang->line('application_user_role_add');
                }
            }
        }

        $result = array('status'=>TRUE, 'message' =>$suc, 'data'=>$results);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function applicationUserRole_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => 'Invalid Data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_application_user_role', array('required'=> $this->lang->line('application_user_role_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->deleteApplicationUserRole(array(
                        'id_application_user_role' => $data['id_application_user_role']
                    ));

        $result = array('status'=>TRUE, 'message' =>$this->lang->line('application_user_role_del'), 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getUserListForApplicationRole_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => 'Invalid Data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('application_role_id', array('required'=> $this->lang->line('application_role_req')));
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('type', array('required'=> $this->lang->line('type_application_user_role_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($data['type']=='user')
            $result = $this->Company_model->getUserListForApplicationRole(array('company_id' => $data['company_id'],'application_role_id' => $data['application_role_id']));
        else if($data['type']=='approval_role')
            $result = $this->Company_model->getApprovalRoleListForApplicationRole(array('company_id' => $data['company_id'],'application_role_id' => $data['application_role_id']));

        $result = array('status'=>TRUE, 'message' =>'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }


    /*public function test_get()
    {
        $result = $this->Company_model->getCompanyCurrency(array('company_id' => 1));
        $new = array();
        for($s=0;$s<count($result);$s++)
        {
            if(!isset($new[$result[$s]['currency_code']])){ $new[$result[$s]['currency_code']] = array(); }
            $new[$result[$s]['currency_code']][] = $result[$s]['currency_value'];
            $new['date'][] = $result[$s]['created_date_time'];
        }
        echo "<pre>"; print_r($new); exit;
    }*/

}