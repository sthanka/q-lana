<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Activity extends REST_Controller{

    public function __construct() {
        parent::__construct();
        $this->load->model('Activity_model');
        $this->load->model('Crm_model');
        $this->load->model('Company_model');
        $this->load->model('User_model');
    }

    public function task_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $data = $data['task'];
        $this->form_validator->add_rules('task_name',array('required'=> 'task required'));
        //$this->form_validator->add_rules('task_type',array('required'=> 'task type required'));
        $this->form_validator->add_rules('project_id',array('required'=> 'project id required'));
        $this->form_validator->add_rules('created_by',array('required'=> 'user id required'));
        //$this->form_validator->add_rules('assigned_to',array('required'=> 'assigned to required'));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['task_type'] = 'project';
        $add = $delete = array();
        $member = $data['member'];
        unset($data['member']);
        if(isset($data['id_task']) && $data['id_task']!='')
        {
            $task_id = $this->Activity_model->updateTask($data);
            $data['member'] = $member;
            $task_members = $this->Activity_model->getTaskMembers(array('task_id' => $data['id_task']));
            $task_member_ids = array_values(array_map(function ($obj) { return $obj['member_id']; }, $task_members));
            for($s=0;$s<count($data['member']);$s++)
            {
                if(!in_array($data['member'][$s],$task_member_ids)){
                    $add[] = array(
                        'task_id' => $data['id_task'],
                        'member_id' => $data['member'][$s]
                    );
                }
            }

            for($s=0;$s<count($task_members);$s++)
            {
                if(!in_array($task_members[$s]['member_id'],$data['member'])){
                    $delete[] = $task_members[$s]['id_task_member'];
                }
            }

            if(!empty($add)) $this->Activity_model->addTaskMembers($add);
            if(!empty($delete)) $this->Activity_model->deleteTaskMembers($delete);
        }
        else
        {
            $task_id = $this->Activity_model->addTask($data);
            $data['member'] = $member;
            for($s=0;$s<count($data['member']);$s++)
            {
                $add[] = array(
                    'task_id' => $task_id,
                    'member_id' => $data['member'][$s]
                );
            }
            $this->Activity_model->addTaskMembers($add);
        }

        $document_type = $this->Crm_model->getDocumentTypeByName('task');
        $data['document_type'] = $document_type->id_crm_document_type;
        $project_details = $this->Crm_model->getProject($data['project_id']);
        $path='uploads/'; $no_files = 0;

        if(isset($_FILES) && !empty($_FILES['file']['name']['attachment']))
        {
            if(isset($data['id_task'])){ $task_id = $data['id_task']; }
            if(is_array($_FILES['file']['name']['attachment']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachment']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachment'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachment'][$s],$_FILES['file']['name']['attachment'][$s],$path,$project_details[0]['company_id'],'');
                    if($imageName==0){
                        $result = array('status' => FALSE, 'error' => 'attachment upload error occurred', 'data' => '');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachment'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $task_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by']);
                    //echo "<pre>"; print_r($document); exit;
                    $document_id = $this->Crm_model->addDocument($document);
                    $no_files++;
                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachment'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachment'],$_FILES['file']['name']['attachment'],$path,$project_details[0]['company_id'],'');
                if($imageName==0){
                    $result = array('status' => FALSE, 'error' => 'attachment upload error occurred', 'data' => '');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachment'], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $task_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by']);
                //echo "<pre>"; print_r($document); exit;
                $document_id = $this->Crm_model->addDocument($document);
                $no_files++;
            }
            if(isset($data['id_task'])){ unset($task_id); }
        }

        if(!isset($data['id_task']) || $data['id_task']==''){
            $created_user = $this->User_model->getUserInfo(array('id' => $data['created_by']));
            for($s=0;$s<count($data['member']);$s++)
            {
                $assigned_user = $this->User_model->getUserInfo(array('id' => $data['member'][$s]));
                $activityTemplate = '<p class="f12"><span class="blue">'.ucfirst($created_user->first_name).' '.ucfirst($created_user->last_name).'</span> has created new task <a class="sky-blue" href="javascript:;" data-activity-id = "'.$task_id.'" data-activity-type = "task">'.ucfirst($data['task_name']).'</a> and assigned to <span class="blue">'.ucfirst($assigned_user->first_name).' '.ucfirst($assigned_user->last_name).'</span></p>';

                $activity = array(
                    'activity_name' => 'Task',
                    'activity_type' => 'task',
                    'activity_template' =>$activityTemplate,
                    'activity_reference_id' => $task_id,
                    'module_type' => 'project',
                    'module_id' => $data['project_id'],
                    'created_by' => $data['created_by']
                );
                $notification = array(
                    'sent_by' => $data['created_by'],
                    'sent_to' => $data['member'][$s],
                    'project_id' => $data['project_id'],
                    'notification_type' => $data['task_name'],
                    'notification_template' => '<p class="f12"><a class="sky-blue" href="javascript:;">'.$created_user->first_name.' '.$created_user->last_name.'</a> has assigned new task</a></p>'
                );
                $this->addActivity($activity);
                $this->addNotification($notification);
            }
        }
        if(!isset($data['id_task']) || $data['id_task']==''){
            $suc_msg = 'Task added successfully.';
        }
        else{
            $suc_msg = 'Task updated successfully.';
        }
        if(isset($data['id_task'])){ $task_id = $data['id_task']; }
        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>$task_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function task_get()
    {
        $data = $this->input->get();
        $document_type = $this->Crm_model->getDocumentTypeByName('task');
        $data['crm_document_type_id'] = $document_type->id_crm_document_type;
        $result = $this->Activity_model->getTask($data);
        if(isset($data['id_task']))
        {
            $result[0]['attachment'] = $this->Activity_model->getAttachments($data['id_task'],$data['crm_document_type_id']);

            for($r=0;$r<count($result[0]['attachment']);$r++)
            {
                $result[0]['attachment'][$r]['full_path'] = getImageUrl($result[0]['attachment'][$r]['full_path']);
            }

            $result[0]['member'] = $this->Activity_model->getTaskMembers(array('task_id' => $result[0]['id_task']));
            $result[0]['member'] = array_map(function($obj){ return $obj['member_id']; }, $result[0]['member']);
        }
        $total_records = $this->Activity_model->totalTasks($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' => $result, 'total_records' =>$total_records->total_tasks));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function updateTask_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $result = $this->Activity_model->updateTask($data);
        $result = array('status'=>TRUE, 'message' => 'Task updated successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function task_delete()
    {
        $data = $this->input->get();
        $data['task_status'] = 'deleted';
        $result = $this->Activity_model->updateTask($data);
        $result = array('status'=>TRUE, 'message' => 'Task deleted successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function meetingGuest_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id',array('required'=> 'Company id required'));
        $this->form_validator->add_rules('user_id',array('required'=> 'User id required'));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $user_branch_details = $this->Company_model->getCompanyUsers(array('user_id' => $data['user_id'], 'company_id' => $data['company_id']));
        $users = $this->Company_model->getCompanyUsers(array('branch_id' => $user_branch_details[0]['branch_id'], 'company_id' => $data['company_id']));

        $result = array('status'=>TRUE, 'message' => 'Success', 'data'=>$users);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function meeting_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $data = $data['meeting'];
        $this->form_validator->add_rules('meeting_name',array('required'=> 'Meeting name required'));
        //$this->form_validator->add_rules('meeting_type',array('required'=> 'task type required'));
        $this->form_validator->add_rules('project_id',array('required'=> 'project id required'));
        $this->form_validator->add_rules('created_by',array('required'=> 'user id required'));
        $this->form_validator->add_rules('where',array('required'=> 'where parameter required'));
        $this->form_validator->add_rules('when',array('required'=> 'when parameter to required'));
        //$this->form_validator->add_rules('from_time',array('required'=> 'From time required'));
        //$this->form_validator->add_rules('to_time',array('required'=> 'End time required'));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_approval_id = 0;
        if(isset($data['project_approval_id'])){
            $project_approval_id = $data['project_approval_id'];
            unset($data['project_approval_id']);
        }

        $invitations = $invitation_person = array();
        if(isset($data['invitation_id']) && is_array($data['invitation_id'])){
            $invitations = $data['invitation_id'];
        }
        else if(isset($data['invitation_id'])){
            $invitations = explode(',',$data['invitation_id']);
        }
        unset($data['invitation_id']);

        $guest = $guest_data = array();
        if(isset($data['guest'])){
            //$guest = explode(',',$data['guest']);
            $guest = array_values(array_unique($data['guest']));
            unset($data['guest']);
        }

        //insert or update based on id_meeting
        $data['meeting_type'] = 'project';
        if(!isset($data['id_meeting'])){
            $meeting_id = $this->Activity_model->addMeeting($data);
        }
        else{
            $meeting_id = $this->Activity_model->updateMeeting($data);
        }
        $document_type = $this->Crm_model->getDocumentTypeByName('meeting');
        $data['document_type'] = $document_type->id_crm_document_type;
        $project_details = $this->Crm_model->getProject($data['project_id']);
        $path='uploads/'; $no_files = 0;
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachment']))
        {
            if(isset($data['id_meeting'])){ $meeting_id = $data['id_meeting']; }
            if(is_array($_FILES['file']['name']['attachment']))
            {
                for($s=0;$s<count($_FILES['file']['name']['attachment']);$s++)
                {
                    $mediaType = $_FILES['file']['type']['attachment'][$s];
                    $imageName = doUpload($_FILES['file']['tmp_name']['attachment'][$s],$_FILES['file']['name']['attachment'][$s],$path,$project_details[0]['company_id'],'');
                    if($imageName==0){
                        $result = array('status' => FALSE, 'error' => 'attachment upload error occurred', 'data' => '');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }

                    $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachment'][$s], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $meeting_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by']);
                    $document_id = $this->Crm_model->addDocument($document);
                    $no_files++;
                }
            }
            else{
                $mediaType = $_FILES['file']['type']['attachment'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachment'],$_FILES['file']['name']['attachment'],$path,$project_details[0]['company_id'],'');
                if($imageName==0){
                    $result = array('status' => FALSE, 'error' => 'attachment upload error occurred', 'data' => '');
                    $this->response($result, REST_Controller::HTTP_OK);
                }

                $document = array('document_source' => $imageName, 'document_name' => $_FILES['file']['name']['attachment'], 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $meeting_id, 'document_description' => '', 'document_mime_type' => $mediaType, 'uploaded_by' => $data['created_by']);
                $document_id = $this->Crm_model->addDocument($document);
                $no_files++;
            }
            if(isset($data['id_meeting'])){ unset($meeting_id); }
        }
        $invitation_data = array();
        if(isset($data['id_meeting'])){
            $exists_invitation_data = $this->Activity_model->getInvitationsByMeetingId($data['id_meeting']);
            for($s=0;$s<count($exists_invitation_data);$s++){
                if(!in_array($exists_invitation_data[$s]['invitation_id'],$invitations)){
                    $this->Activity_model->deleteMeetingInvitation($exists_invitation_data[$s]['invitation_id'],$data['id_meeting']);
                }
            }
            //echo "<pre>"; print_r($exists_invitation_data); exit;
            $exists_invitation_ids = array_values(array_map(function ($obj) { return $obj['invitation_id']; }, $exists_invitation_data));
            for($s=0;$s<count($invitations);$s++){
                if(!in_array($invitations[$s],$exists_invitation_ids)){
                    $invitation_data[] = array(
                        'meeting_id' => $data['id_meeting'],
                        'invitation_id' => $invitations[$s]
                    );
                }
            }

            $exists_guest_data = $this->Activity_model->getGuestByMeetingId($data['id_meeting']);
            for($s=0;$s<count($exists_guest_data);$s++){
                if(!in_array($exists_guest_data[$s]['email'],$guest)){
                    $this->Activity_model->deleteMeetingGuest($exists_guest_data[$s]['email'],$data['id_meeting']);
                }
            }
            //echo "<pre>"; print_r($exists_invitation_data); exit;
            $exists_guest_ids = array_values(array_map(function ($obj) { return $obj['email']; }, $exists_guest_data));
            for($s=0;$s<count($guest);$s++){
                if(!in_array($guest[$s],$exists_guest_ids)){
                    $guest_data[] = array(
                        'meeting_id' => $data['id_meeting'],
                        'email' => $guest[$s]
                    );
                }
            }

        }
        else{
            for($s=0;$s<count($invitations);$s++){
                $invitation_data[] = array(
                    'meeting_id' => $meeting_id,
                    'invitation_id' => $invitations[$s]
                );
            }

            for($s=0;$s<count($guest);$s++){
                $guest_data[] = array(
                    'meeting_id' => $meeting_id,
                    'email' => $guest[$s]
                );
            }
        }
        //echo "<pre>"; print_r($invitation_data); exit;
        //echo "<pre>"; print_r($guest_data); exit;
        if(!empty($invitation_data)){
            $this->Activity_model->addMeetingInvitations($invitation_data);
        }
        if(!empty($guest_data)){
            $this->Activity_model->addMeetingGuest($guest_data);
        }
        $user_names= ''; $user_ids = array();
        for($s=0;$s<count($invitation_data);$s++)
        {
            if($s>0){ $user_names.=','; }
            $invitation_person = $this->User_model->getUserInfo(array('id' => $invitation_data[$s]['invitation_id']));
            $user_names.= ucfirst($invitation_person->first_name).' '.ucfirst($invitation_person->last_name);
            $user_ids[] =  $invitation_person->id_user;
        }
        if(!isset($data['id_meeting']))
        {
            $created_user = $this->User_model->getUserInfo(array('id' => $data['created_by']));
            $meetingTemplate = '<p class="f12"><span class="blue">'.ucfirst($created_user->first_name).' '.ucfirst($created_user->last_name).'</span> has created new meeting <a class="sky-blue" href="javascript:;" data-activity-id="'.$meeting_id.'" data-activity-type="meeting">'.ucfirst($data['meeting_name']).'</a> at <span class="blue">'.ucfirst($data['where']).'</span> on <span class="blue">'.date('d-m-Y',strtotime($data['when'])).'</span> and invited <span class="blue">'.$user_names.'</span></p>';
            $activity = array(
                'activity_name' => 'Meeting',
                'activity_type' => 'meeting',
                'activity_template' =>$meetingTemplate,
                'activity_reference_id' => $meeting_id,
                'module_type' => 'project',
                'module_id' => $data['project_id'],
                'created_by' => $data['created_by']
            );
            $notification = array(
                'sent_by' => $data['created_by'],
                'project_id' => $data['project_id'],
                'notification_type' => 'meeting',
                'notification_template' => '<p class="f12"><a class="sky-blue" href="javascript:;">'.$created_user->first_name.' '.$created_user->last_name.'</a> has been invited for '.$data['meeting_name'].' meeting on <a class="sky-blue" href="javascript:;">'.$data['when'].' </a> at <a class="sky-blue" href="javascript:;">'.$data['where'].' </a></p>'
            );

            $this->addActivity($activity);
            if(!empty($user_ids)){
                for($r=0;$r<count($user_ids);$r++){
                    $notification['sent_to'] = $user_ids[$r];
                    $this->addNotification($notification);
                }
            }
        }
        if(!isset($data['id_meeting'])){
            $suc_msg = 'Meeting scheduled successfully.';
        }
        else{
            $suc_msg = 'Meeting updated successfully.';
        }

        if($project_approval_id)
        {
            if(isset($data['id_meeting'])){ $meeting_id = $data['id_meeting']; }
            $check_meeting = $this->Activity_model->getProjectApprovalMeeting(array('project_approval_id' => $project_approval_id, 'meeting_id' => $meeting_id));
            if(empty($check_meeting)){
                $this->Activity_model->addProjectApprovalMeeting(array('project_approval_id' => $project_approval_id, 'meeting_id' => $meeting_id));
            }
        }

        //sending invitation mail to members
        $created_user = $this->User_model->getUserInfo(array('id' => $data['created_by']));
        if(isset($data['from_time']) && isset($data['to_time']))
            $template = '<p class="f12"><a class="sky-blue" href="javascript:;">'.$created_user->first_name.' '.$created_user->last_name.'</a> has been invited for '.$data['meeting_name'].' meeting on <a class="sky-blue" href="javascript:;">'.date('d-m-Y',strtotime($data['when'])).' / '.$data['from_time'].'-'.$data['to_time'].' </a> at <a class="sky-blue" href="javascript:;">'.$data['where'].' </a></p>';
        else
            $template = '<p class="f12"><a class="sky-blue" href="javascript:;">'.$created_user->first_name.' '.$created_user->last_name.'</a> has been invited for '.$data['meeting_name'].' meeting on <a class="sky-blue" href="javascript:;">'.date('d-m-Y',strtotime($data['when'])).' </a> at <a class="sky-blue" href="javascript:;">'.$data['where'].' </a></p>';

        for($s=0;$s<count($invitation_data);$s++)
        {
            $user_details = $this->User_model->getUserInfo(array('id' => $invitation_data[$s]['invitation_id']));
            sendmail($user_details->email_id,'Meeting Invitation',$template);
        }

        for($s=0;$s<count($guest_data);$s++)
        {
            sendmail($guest_data[$s]['email'],'Meeting Invitation',$template);
        }

        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>$meeting_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function meeting_delete()
    {
        $data = $this->input->get();
        $data['meeting_status'] = 'deleted';
        $result = $this->Activity_model->updateMeeting($data);

        /*if(!isset($data['id_meeting']))
        {
            $meeting_details = $this->Activity_model->getMeeting($data);
            $created_user = $this->Company_model->getCompanyUser($data['created_by']);
            $activity = array(
                'activity_type' => 'meeting',
                'activity_template' =>'<p class="f12"><a class="sky-blue" href="javascript:;">'.$created_user->first_name.' '.$created_user->last_name.'</a> has been canceled meeting<a class="sky-blue" href="javascript:;"> '.$meeting_details[0]['meeting_name'].'</a></p>',
                'activity_reference_id' => $data['id_meeting'],
                'project_id' => $data['project_id'],
                'created_by' => $data['created_by']
            );
            $notification = array(
                'sent_by' => $data['created_by'],
                'project_id' => $data['project_id'],
                'notification_type' => 'meeting',
                'notification_template' => '<p class="f12"><a class="sky-blue" href="javascript:;">'.$created_user->first_name.' '.$created_user->last_name.'</a> has been canceled meeting<a class="sky-blue" href="javascript:;"> '.$meeting_details[0]['meeting_name'].'</a></p>',
            );

            $this->addActivity($activity);
            $user_ids = explode(',',$meeting_details->invitation_id);
            if(!empty($user_ids)){
                for($r=0;$r<count($user_ids);$r++){
                    $notification['sent_to'] = $user_ids[$r];
                    $this->addNotification($notification);
                }
            }
        }*/

        $result = array('status'=>TRUE, 'message' => 'Meeting deleted successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function meeting_get()
    {
        $data = $this->input->get();
        //getting attachment with document type and meeting id join
        $document_type = $this->Crm_model->getDocumentTypeByName('meeting');
        $data['crm_document_type_id'] = $document_type->id_crm_document_type;

        $result = $this->Activity_model->getMeeting($data);
        for($s=0;$s<count($result);$s++)
        {
            $email = $this->Crm_model->getMeetingGuest(array('meeting_id' => $result->id_meeting));
            $result->guest = $email[0]['guest'];
        }
        if(isset($data['id_meeting'])){
            //echo strtotime($result->when_date.' '.$result->from_time); exit;
            $meeting_date = strtotime($result->when_date.' '.$result->from_time);
            $current_date = strtotime(date('Y-m-d h:i:s'));
            if($current_date>$meeting_date){ $result->meeting_expiry_status = 0; }
        }


        if(isset($data['id_meeting'])){
            $result->attachment = $this->Activity_model->getAttachments($data['id_meeting'],$data['crm_document_type_id']);
            for($r=0;$r<count($result->attachment);$r++){
                $result->attachment[$r]['document_source'] = getImageUrl($result->attachment[$r]['document_source']);
            }
            $invitation_id = explode(',',$result->invitation_id); $invitation_id_array = array();
            for($s=0;$s<count($invitation_id);$s++){
                $invitation_id_array[] = array('id_user' => $invitation_id[$s]);
            }
            $result->invitation_id = $invitation_id_array;
        }
        $total_records = $this->Activity_model->getTotalMeetings($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' => $result, 'total_meetings' => $total_records->total_meetings));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function activity_get()
    {
        $data = $this->input->get();
        $result = $this->Activity_model->getActivity($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function addActivity($data)
    {
        $data = $this->Activity_model->addActivity($data);
    }

    /*public function discussion_get()
    {
        $data = $this->input->get();
        $result = $this->Activity_model->getDiscussion($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }*/

    public function discussion_get()
    {
        $data = $this->input->get();
        $result = $this->Activity_model->getDiscussion($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function discussion_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $this->form_validator->add_rules('discussion_reference_id',array('required'=> 'discussion reference id required'));
        $this->form_validator->add_rules('discussion_type',array('required'=> 'discussion type required'));
        $this->form_validator->add_rules('created_by',array('required'=> 'user id required'));
        $this->form_validator->add_rules('discussion_description',array('required'=> 'discussion description required'));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $activity_discussion_id = $this->Activity_model->addDiscussion($data);
        $result = array('status'=>TRUE, 'message' => 'Discussion added successfully.', 'data'=>$activity_discussion_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function discussion_delete()
    {
        $data = $this->input->get();
        $data['discussion_status'] = 'deleted';
        $result = $this->Activity_model->updateDiscussion($data);
        $total_records = $this->Activity_model->getTotalDiscussion($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' => $result, 'total_records' => $total_records->total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function addNotification($data)
    {
        $data = $this->Activity_model->addNotification($data);
    }

    public function notification_get()
    {
        $data = $this->input->get();
        $result = $this->Activity_model->getNotification($data);
        unset($data['offset']); unset($data['limit']);
        $total_records = count($this->Activity_model->getNotification($data));
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' => $result, 'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    /*public function notification_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        $result = $this->Activity_model->updateNotification($data);
        $result = array('status'=>TRUE, 'message' => 'Notification added successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }*/

    public function notification_delete()
    {
        $data = $this->input->get();
        $data['notification_status'] = 'deleted';
        $result = $this->Activity_model->updateNotification($data);
        $result = array('status'=>TRUE, 'message' => 'Notification deleted successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approval_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo "<pre>"; print_r($data); exit;
        $this->form_validator->add_rules('project_id',array('required'=> 'Project id required'));
        $this->form_validator->add_rules('forwarded_by',array('required'=> 'Forwarded by required'));
        $this->form_validator->add_rules('forwarded_to',array('required'=> 'Forwarded to required'));
        $this->form_validator->add_rules('committee_id',array('required'=> 'Committee id required'));
        $this->form_validator->add_rules('approval_comments',array('required'=> 'Comments required'));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($data['approval_status'] == 'approved' || $data['approval_status'] == 'rejected')
        {
            $project_details = $this->Crm_model->getProject($data['project_id']);
            $approval_id = $this->Activity_model->addApproval(array('project_id' => $data['project_id'], 'forwarded_by' => $project_details[0]['assigned_to'], 'forwarded_to' => $project_details[0]['assigned_to'], 'approval_comments' => $data['approval_comments'], 'approval_status' => $data['approval_status'], 'company_approval_credit_committee_id' => $project_details[0]['committee_id']));
        }
        else
        {
            $approval_id = $this->Activity_model->addApproval(array('project_id' => $data['project_id'], 'forwarded_by' => $data['forwarded_by'], 'forwarded_to' => $data['forwarded_to'], 'approval_comments' => $data['approval_comments'], 'approval_status' => $data['approval_status'], 'company_approval_credit_committee_id' => $data['committee_id']));
        }

        if(isset($data['forwarded_to']) && $data['approval_status'] == 'forwarded')
            $this->Crm_model->updateProject(array('assigned_to' => $data['forwarded_to'], 'committee_id' => $data['committee_id']),$data['project_id']);

        if(isset($data['approval_status']) /*&& $data['approval_status']!='forwarded'*/){
            $approvalStatus = $data['approval_status'];
            if($data['approval_status'] == 'forwarded') $approvalStatus = 'pending';
            $this->Crm_model->updateProject(array('project_status' => ucfirst($approvalStatus), 'approval_comments' => $data['approval_comments']),$data['project_id']);
        }

        $project = $this->Crm_model->getProject($data['project_id']);
        if(isset($data['committee_id']))
            $committee_details = $this->Company_model->getApprovalCreditCommittee($data);

        /*echo '<pre>';
        print_r($committee_details); exit;*/

        if(isset($data['forwarded_by']))
            $forwarded_by = $this->Company_model->getCompanyUser($data['forwarded_by']);

        if(isset($data['forwarded_to']))
            $forwarded_to = $this->Company_model->getCompanyUser($data['forwarded_to']);

        $activity_template = $notification = $notification_template = '';
        $suc_msg = 'success';
        if(isset($data['approval_status']) && $data['approval_status']=='forwarded'){
            $activity_template = '<p class="f12"><span class="blue">'.ucfirst($forwarded_by->first_name).' '.ucfirst($forwarded_by->last_name).'</span> has forwarded to <span class="blue">'.$committee_details[0]['credit_committee_name'].'</span> assigned to <span class="blue">'.ucfirst($forwarded_to->first_name).' '.ucfirst($forwarded_to->last_name).'</span><p class="text-indent">" '.$data['approval_comments'].' "</p></p>';
            $notification_template = '<p class="f12"><a class="sky-blue" href="javascript:;">'.$project[0]['project_title'].'</a>project has been forwarded for approval to you</p>';
            $suc_msg = 'Approval forwarded successfully.';
        }
        else if(isset($data['approval_status']) && $data['approval_status']=='approved'){
            $activity_template = '<p class="f12"><span class="blue">'.ucfirst($forwarded_by->first_name).' '.ucfirst($forwarded_by->last_name).'</span> has <span class="blue">Approved</span> project <p class="text-indent">" '.$data['approval_comments'].' "</p></p>';
            $suc_msg = 'Approval approved successfully.';
        }
        else if(isset($data['approval_status']) && $data['approval_status']=='rejected'){
            $activity_template = '<p class="f12"><span class="blue">'.ucfirst($forwarded_by->first_name).' '.ucfirst($forwarded_by->last_name).'</span> has <span class="blue">Rejected</span> project <p class="text-indent">" '.$data['approval_comments'].' "</p></p>';
            $suc_msg = 'Approval rejected successfully.';
        }

        $activity = array(
            'activity_name' => 'Project approval',
            'activity_type' => 'approval',
            'activity_template' =>$activity_template,
            'activity_reference_id' => $approval_id,
            'module_type' => 'Project',
            'module_id' => $data['project_id'],
            'created_by' => $data['forwarded_by']
        );
        $notification = array(
            'sent_by' => $data['forwarded_by'],
            'sent_to' => $data['forwarded_to'],
            'project_id' => $data['project_id'],
            'notification_type' => 'approval',
            'notification_template' => $notification_template
        );
        if(!empty($activity))
            $this->addActivity($activity);
        if(!empty($notification) && $data['approval_status'] == 'forwarded')
            $this->addNotification($notification);

        //send mail to forwarded to user
        if(isset($data['approval_status']) && $data['approval_status']=='forwarded'){
            $user_deatils = $this->User_model->getUserInfo(array('id' => $data['forwarded_to']));
            sendmail($user_deatils->email_id,'Project Forwarded For Approval',$notification_template);
        }

        //forwarding facilities
        $project_approval = $this->Activity_model->getProjectApproval(array('project_id' => $data['project_id'], 'forwarded_to' => $data['forwarded_by']));
        if(empty($project_approval))
        {
            $facility = $this->Crm_model->getFacilities(array('crm_project_id' => $data['project_id']));
            for($s=0;$s<count($facility);$s++)
            {
                $this->Crm_model->addFacilityActivity(array('created_by' => $data['forwarded_by'], 'project_facility_id' => $facility[$s]['id_project_facility'], 'facility_amount' => $facility[$s]['loan_amount'], 'facility_status' => $facility[$s]['project_facility_status'],'facility_comments' => ''));
                $this->Crm_model->addFacilityActivity(array('created_by' => $data['forwarded_by'], 'project_facility_id' => $facility[$s]['id_project_facility'], 'project_approval_id' => $approval_id, 'facility_amount' => $facility[$s]['loan_amount'], 'facility_status' => $facility[$s]['project_facility_status'],'facility_comments' => ''));
            }
        }
        else
        {
            $facility = $this->Crm_model->getFacilityActivity(array('project_approval_id' => $project_approval[0]['id_project_approval']));
            if(empty($facility))
            {
                $facility = $this->Crm_model->getFacilities(array('crm_project_id' => $data['project_id']));
                for($s=0;$s<count($facility);$s++)
                {
                    $this->Crm_model->addFacilityActivity(array('created_by' => $data['forwarded_by'], 'project_facility_id' => $facility[$s]['id_project_facility'], 'facility_amount' => $facility[$s]['loan_amount'], 'facility_status' => $facility[$s]['project_facility_status'],'facility_comments' => ''));
                    $this->Crm_model->addFacilityActivity(array('created_by' => $data['forwarded_by'], 'project_facility_id' => $facility[$s]['id_project_facility'], 'project_approval_id' => $approval_id, 'facility_amount' => $facility[$s]['loan_amount'], 'facility_status' => $facility[$s]['project_facility_status'],'facility_comments' => ''));
                }
            }
            else
            {
                for($s=0;$s<count($facility);$s++)
                {
                    $this->Crm_model->addFacilityActivity(array('created_by' => $data['forwarded_by'], 'project_facility_id' => $facility[$s]['project_facility_id'], 'project_approval_id' => $approval_id, 'facility_amount' => $facility[$s]['facility_amount'], 'facility_status' => $facility[$s]['facility_status'],'facility_comments' => $facility[$s]['facility_comments']));
                }
            }
        }


        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

}