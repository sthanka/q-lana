<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2015-12-01
 * Time: 03:58 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
class Master extends REST_Controller
{
    public $nameRules                   = array(
                                                'required'=> 'Name is required',
                                                'max_len-100' => 'Name should be below 100',
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Master_model');
        $this->load->model('Company_model');
        //$this->load->library('common/form_validator');
    }

    public function sectorList_get()
    {
        $data = $this->input->get();
        $result = $this->Master_model->getSectorSubSectorList($data);
        $total_records=$this->Master_model->getSectorCount($data);
        if(isset($data['company_id']))
        {
            array_unshift($result,array('id_sector' => "0", 'parent_id' => "0", 'sector_name' => 'All Sectors', 'parent' => ''));
        }
        //echo "<pre>"; print_r($result); exit;
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function sector_get()
    {
        $data = $this->input->get();
        $result = $this->Master_model->getSectorsList($data);
        $total_records=$this->Master_model->getSectorCount();
        if(isset($data['company_id']))
        {
            array_unshift($result,array('id_sector' => "0", 'parent_id' => "0", 'sector_name' => 'All Sectors', 'parent' => ''));
        }
        //echo "<pre>"; print_r($result); exit;
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function sectorById_get($id)
    {
        $data['id_sector'] = $id;
        //validating data
        $idRule = array('required'=> 'id required');
        $this->form_validator->add_rules('id_sector', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->getSector($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function subSector_get()
    {
        $data = $this->input->get();
        //validating data

        $idRule = array('required'=> 'sector id required');
        $this->form_validator->add_rules('id_sector', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->getSubSector($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function sector_post()//add
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        //print_r($data);
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('sector_name', array('required'=> 'Sector name required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existSector=$this->Master_model->checkSectorExist($data);
        if($existSector){
            $result = array('status'=>FALSE,'error'=>array('sector_name' => 'Sector already exist'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertSector($data);
        $result = array('status'=>TRUE, 'message' => 'Sector added successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function sector_put()//edit
    {
        $data = $this->input->get();
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('sector_name', $this->nameRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        /*$existSector=$this->Master_model->checkSectorExist($data);
        if($existSector){
            $result = array('status'=>FALSE,'message'=>'Sector already exist','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }*/
        $result = $this->Master_model->updateSector($data);
        $result = array('status'=>TRUE, 'message' => 'Sector updated successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    /*public function sector_delete()
    {
        //$_POST = json_decode(file_get_contents("php://input"), true);
        //$data=$this->input->post();
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $idRule = array('required'=> 'id required');
        $this->form_validator->add_rules('id_sector', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->deleteSector($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }*/

    // country module
    public function country_get()
    {
        $data=$this->input->get();
        $result = $this->Master_model->getCountriesList($data);
        $total_records=$this->Master_model->getCountriesCount();
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function countryById_get($id)
    {
        $data['id_country'] = $id;
        //validating data
        $idRule = array('required'=> 'id required');
        $this->form_validator->add_rules('id_country', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->getCountry($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function country_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('country_name', $this->nameRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existCountry=$this->Master_model->checkCountryExist($data);
        if($existCountry){
            $result = array('status'=>FALSE,'error'=>array('country_name'=>'Country already exist'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertCountry($data);
        $result = array('status'=>TRUE, 'message' => 'Country added successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function country_put()
    {
        /*$data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }*/
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('country_name', $this->nameRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existCountry=$this->Master_model->checkCountryExist($data);
        if($existCountry){
            $result = array('status'=>FALSE,'error'=>array('country_name'=>'Country already exist'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updateCountry($data);
        $result = array('status'=>TRUE, 'message' => 'Country updated successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function country_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $idRule = array('required'=> 'id required');
        $this->form_validator->add_rules('id_country', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->deleteCountry($data);
        $result = array('status'=>TRUE, 'message' => 'Country deleted successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    //country end

    // branch start
    public function branch_get()
    {
        $data=$this->input->get();
        $total_records=$this->Master_model->getBranchesCount($data);
        $result = $this->Master_model->getBranchesList($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function branchTypeById_get($id)
    {
        $data['id_branch_type'] = $id;
        //validating data
        $idRule = array('required'=> 'id required');
        $this->form_validator->add_rules('id_branch_type', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->getBranchType($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function branch_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('branch_type_name', array('required'=> 'Branch Type name required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existBranch=$this->Master_model->checkBranchExist($data);
        if($existBranch){
            $result = array('status'=>FALSE,'error'=>array('branch_type_name'=>'Branch type already exist'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertBranch($data);
        $result = array('status'=>TRUE, 'message' => 'Branch type added successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function branch_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('branch_type_name', $this->nameRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existBranch=$this->Master_model->checkBranchExist($data);
        if($existBranch){
            $result = array('status'=>FALSE,'error'=>array('branch_type_name'=>'Branch type already exist'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updateBranch($data);
        $result = array('status'=>TRUE, 'message' => 'Branch type updated successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    /*public function branch_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $idRule = array('required'=> 'id required');
        $this->form_validator->add_rules('id_branch', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->deleteBranch($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }*/
    public function plansList_get()
    {
        $data=$this->input->get();
        $total_records=$this->Master_model->getPlansCount();
        $result = $this->Master_model->getPlansList($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function plans_get($id)
    {
        $result = $this->Master_model->getPlanDetails($id);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function plans_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $planNameRules=array('required'=> 'Plan name required');
        $loansRules=array('required'=> 'No of loans required','numeric'=> 'No of loans should be numeric');
        $usersRules=array('required'=> 'No of users required','numeric'=> 'No of users should be numeric');
        $diskSpaceRules=array('required'=> 'Total disk space required','numeric'=> 'Total disk space should be numeric');
        $priceLoanRules=array('required'=> 'Price per loan required','numeric'=> 'Price per loan should be numeric');

        $this->form_validator->add_rules('plan_name', $planNameRules);
        $this->form_validator->add_rules('no_of_loans', $loansRules);
        $this->form_validator->add_rules('no_of_users', $usersRules);
        $this->form_validator->add_rules('total_disk_space', $diskSpaceRules);
        $this->form_validator->add_rules('price_per_loan', $priceLoanRules);


        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updatePlan($data);
        $result = array('status'=>TRUE, 'message' => 'Plan updated successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function plans_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //echo "<pre>"; print_r($data); exit;
        $planNameRules=array('required'=> 'Plan name required');
        $noLoansRules=array('required'=> 'No of loans required','numeric'=> 'No of loans should be numeric');
        $noUsersRules=array('required'=> 'No of users required','numeric'=> 'No of users should be numeric');
        $priceLoanRules=array('required'=> 'Price per loan required','numeric'=> 'Price per loan should be numeric');
        $diskSpaceRules=array('required'=> 'Disk space required','numeric'=> 'Disk space should be numeric');

        $this->form_validator->add_rules('plan_name', $planNameRules);
        $this->form_validator->add_rules('no_of_loans', $noLoansRules);
        $this->form_validator->add_rules('no_of_users', $noUsersRules);
        $this->form_validator->add_rules('price_per_loan', $priceLoanRules);
        $this->form_validator->add_rules('total_disk_space', $diskSpaceRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertPlan($data);
        $result = array('status'=>TRUE, 'message' => 'Plan Added successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function plans_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $planRules = array('required'=> 'id required');
        $this->form_validator->add_rules('id_plan', $planRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->deletePlan($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    //risk
    public function risk_get($id)
    {
        $result = $this->Master_model->getRiskDetails($id);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function riskList_get()
    {
        $data=$this->input->get();
        $total_records=$this->Master_model->getRiskCount();
        $result = $this->Master_model->getRiskList($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function risk_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('risk_name', $this->nameRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existRisk=$this->Master_model->checkRiskExist($data);
        if($existRisk){
            $result = array('status'=>FALSE,'error'=>array('risk_name'=>'Risk already exist'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertRisk($data);
        $result = array('status'=>TRUE, 'message' => 'Risk added successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function risk_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('risk_name', $this->nameRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existRisk=$this->Master_model->checkRiskExist($data);
        if($existRisk){
            $result = array('status'=>FALSE,'error'=>array('risk_name'=>'Risk already exist'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updateRisk($data);
        $result = array('status'=>TRUE, 'message' => 'Risk updated successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    //social
    public function social_get($id)
    {
        $result = $this->Master_model->getSocialDetails($id);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function socialList_get()
    {
        $data=$this->input->get();
        $total_records=$this->Master_model->getSocialCount();
        $result = $this->Master_model->getSocialList($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function social_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('business_social_network', $this->nameRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existSocial=$this->Master_model->checkSocialExist($data);
        if($existSocial){
            $result = array('status'=>FALSE,'error'=>array('business_social_network'=>'Name already exist'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertSocial($data);
        $result = array('status'=>TRUE, 'message' => 'Network added successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function social_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('business_social_network', $this->nameRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existSocial=$this->Master_model->checkSocialExist($data);
        if($existSocial){
            $result = array('status'=>FALSE,'error'=>array('business_social_network'=>'Name already exist'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updateSocial($data);
        $result = array('status'=>TRUE, 'message' => 'Network updated successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    //contact
    public function contact_get($id)
    {
        $result = $this->Master_model->getContactDetails($id);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function contactList_get()
    {
        $data=$this->input->get();
        $total_records=$this->Master_model->getContactCount();
        $result = $this->Master_model->getContactList($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function contact_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('business_contact', $this->nameRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existContact=$this->Master_model->checkContactExist($data);
        if($existContact){
            $result = array('status'=>FALSE,'error'=>array('business_contact'=>'Name already exist'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertContact($data);
        $result = array('status'=>TRUE, 'message' => 'Contact added successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function contact_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('business_contact', $this->nameRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $existContact=$this->Master_model->checkContactExist($data);
        if($existContact){
            $result = array('status'=>FALSE,'error'=>array('business_contact'=>'Name already exist'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updateContact($data);
        $result = array('status'=>TRUE, 'message' => 'Contact updated successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }




    public function bankCategoryList_get()
    {
        $data=$this->input->get();
        $result = $this->Master_model->getBankCategoryList($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function bankCategory_get()
    {
        $data=$this->input->get();
        $result = $this->Master_model->getBankCategoryList($data);
        $total_records=$this->Master_model->getBankCategoriesCount();
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function bankCategoryById_get($id)
    {
        $result = $this->Master_model->getBankCategoryDetails($id);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function bankCategory_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //$req=array('required'=> 'name required');
        $this->form_validator->add_rules('bank_category_name', $this->nameRules);
        //$this->form_validator->add_rules('bank_category_code', $req);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updateBankCategory($data);
        $result = array('status'=>TRUE, 'message' => 'Bank category updated successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function bankCategory_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //$req=array('required'=> 'name required');
        $this->form_validator->add_rules('bank_category_name', $this->nameRules);
        //$this->form_validator->add_rules('bank_category_code', $req);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertBankCategory($data);
        $result = array('status'=>TRUE, 'message' => 'Bank category added successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function bankCategory_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $req = array('required'=> 'id required');
        $this->form_validator->add_rules('id', $req);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->deleteBankCategory($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function approvalRole_get(){
        $data=$this->input->get();
        $total_records=$this->Master_model->getApprovalRolesCount($data);
        $result = $this->Master_model->getApprovalRoles($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function approvalRoleById_get($id)
    {
        $data['id_approval_role'] = $id;
        //validating data
        $idRule = array('required'=> 'id required');
        $this->form_validator->add_rules('id_approval_role', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->getApprovalRole($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function approvalRole_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //$req=array('required'=> 'Name should be required with atleast 2 characters');
        $this->form_validator->add_rules('approval_name', $this->nameRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->insertApprovalRole($data);
        $result = array('status'=>TRUE, 'message' => 'Approval role added successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function approvalRole_put()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //$req=array('required'=> 'Name should be required with atleast 2 characters');
        $this->form_validator->add_rules('approval_name', $this->nameRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->updateApprovalRole($data);
        $result = array('status'=>TRUE, 'message' => 'Approval role updated successfully', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function approvalRole_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $req = array('required'=> 'id required');
        $this->form_validator->add_rules('id_approval_role', $req);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Master_model->deleteApprovalRole($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function currency_get()
    {
        $data = $this->input->get();
        $currency = array();
        if(!empty($data) && isset($data['company_id'])){
            $company_details = $this->Company_model->getCompany(array('id'=>$data['company_id']));
            if($company_details->currency_id!='' || $company_details->currency_id!=0)
                //$currency = array('currency_id' => $company_details->currency_id);
                $data['currency_id'] = $company_details->currency_id;
        }

        $result = $this->Master_model->getCurrency($data);
        for($s=0;$s<count($result);$s++)
        {
            if(isset($data['id_currency']) && $data['id_currency']!='')
                $result->country_flag = getImageUrl($result->country_flag,'flag');
            else
                $result[$s]['country_flag'] = getImageUrl($result[$s]['country_flag'],'flag');
        }
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function currencyList_get()
    {
        $data = $this->input->get();
        $result = $this->Master_model->getCurrency($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['country_flag'] = getImageUrl($result[$s]['country_flag'],'flag');
        }
        $total = $this->Master_model->getTotalCurrency($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>array('data' =>$result,'total_records' => $total[0]['total_records']));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectLoanType_get()
    {
        $result = $this->Master_model->getProjectLoanType();
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectPaymentType_get()
    {
        $result = $this->Master_model->getProjectPaymentType();
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function currency_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>array('error' => 'Invalid Data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else if(!isset($data['currency'])){
            $result = array('status'=>FALSE,'error'=>array('error' => 'Invalid Data'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data = $data['currency'];
        $this->form_validator->add_rules('country_id', array('required'=> 'Country required'));
        $this->form_validator->add_rules('currency_code', array('required'=> 'Currency code required'));
        $this->form_validator->add_rules('currency_symbol', array('required'=> 'Currency symbol required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        /*echo "<pre>"; print_r($data);
        echo "<pre>"; print_r($_FILES);
        exit;*/
        $path='uploads/';
        if(isset($_FILES) && !empty($_FILES['file']['name']['country_flag']))
        {
            $imageName = doUpload($_FILES['file']['tmp_name']['country_flag'],$_FILES['file']['name']['country_flag'],$path,'','image');
            if($imageName===0){
                $result = array('status' => FALSE, 'error' => 'upload only jpg,png format files only', 'data' => '');
                $this->response($result, REST_Controller::HTTP_OK); exit;
            }
            $data['country_flag'] = $imageName;
        }

        if(isset($data['id_currency']) && $data['id_currency']!='') {
            $result = $this->Master_model->updateCurrency($data);
            $msg = 'Currency updated successfully.';
        }
        else {
            $result = $this->Master_model->addCurrency($data);
            $msg = 'Currency added successfully.';
        }

        $result = array('status'=>TRUE, 'message' => $msg, 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
}
?>