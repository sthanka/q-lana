<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Dashboard extends REST_Controller
{

    public $userNameRules               = array(
                                                'required'=> 'Enter valid user name',
                                                'alpha_numeric' => 'User name should be alpha numeric',
                                                'max_len-100' => 'User name should be below 100 characters',
                                                'min_len-2' => 'User name should be minimum 2 characters'
                                                );
    public $firstNameRules               = array(
                                                'required'=> 'Enter valid first name',
                                                'max_len-100' => 'First name should be below 100 characters',
                                                'min_len-2' => 'First name should be minimum 2 characters'
                                                );
    public $lastNameRules               = array(
                                                'required'=> 'Enter valid last name',
                                                'max_len-100' => 'Last name should be below 100 characters',
                                                'min_len-2' => 'Last name should be minimum 2 characters'
                                               );
    public $companyNameRules               = array(
                                                    'required'=> 'Enter valid company name',
                                                    'max_len-100' => 'Company name should be below 100 characters',
                                                    'min_len-2' => 'Company name should be minimum 2 characters'
                                                 );
    public $emailRules                  = array(
                                                'required'=> 'Email required',
                                                'valid_email' => 'Enter valid email'
                                                );
    public $passwordRules               = array(
                                                'required'=> 'Password required',
                                                'alpha_numeric' => 'Password should be alpha numeric',
                                                'max_len-100' => 'Password should be below 100 characters'
                                               );
    public $confirmPasswordRules        = array(
                                                'required'=>'Confirm Password required',
                                                'match_field-password'=>'Password not matched'
                                               );
    public $addressRules                = array(
                                                'required'=> 'Address required',
                                               );

    public $phoneRules                  = array(
                                                'required'=> 'Phone Number required',
                                                'numeric'=>  'Phone number should be numeric',
                                                'min_len-7' => 'Phone no must be minimum 7 digits',
                                                'max_len-10' => 'Phone no must be maximum 10 digits',
                                            );
    public $companyPhoneRules                  = array(
                                                'required'=> 'Company phone Number required',
                                                'numeric'=>  'Company phone number should be numeric',
                                                'min_len-7' => 'Phone no must be minimum 7 digits',
                                                'max_len-10' => 'Phone no must be maximum 10 digits',
                                            );
    public $req                         = array(
                                                'required'=> 'Phone Number required'
                                                );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Dashboard_model');
        //$this->load->library('common/form_validator');
    }

    public function customersByCountry_get()
    {
        $data = $this->Dashboard_model->getCustomersByCountry();
        /*$cols = array(array('id' => 't', 'label' => 'topping', 'type' => 'string'),array('id' => 's', 'label' => 'Slices', 'type' => 'number'));
        $rows = array();
        for($s=0;$s<count($data);$s++){
            array_push($rows,array('c' =>array('v' => $data[$s]['country_name'], 'v' => $data[$s]['total_customers'])));
        }*/
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$data);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function customersByCategory_get()
    {
        $data = $this->Dashboard_model->getCustomersByCategory();
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$data);
        $this->response($result, REST_Controller::HTTP_OK);
    }

}