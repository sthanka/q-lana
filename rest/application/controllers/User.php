<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class User extends REST_Controller
{

    public $userNameRules               = array(
                                                'required'=> 'Enter valid user name',
                                                'max_len-100' => 'User name should be below 100 characters',
                                                );
    public $firstNameRules               = array(
                                                'required'=> 'Enter valid first name',
                                                'max_len-100' => 'First name should be below 100 characters',
                                                );
    public $lastNameRules               = array(
                                                'required'=> 'Enter valid last name',
                                                'max_len-100' => 'Last name should be below 100 characters',
                                               );
    public $companyNameRules               = array(
                                                    'required'=> 'Enter valid company name',
                                                    'max_len-100' => 'Company name should be below 100 characters',
                                                 );
    public $emailRules                  = array(
                                                'required'=> 'Email required',
                                                'valid_email' => 'Enter valid email'
                                                );
    public $passwordRules               = array(
                                                'required'=> 'Password required',
                                                'max_len-100' => 'Password should be below 100 characters'
                                               );
    public $confirmPasswordRules        = array(
                                                'required'=>'Confirm Password required',
                                                'match_field-password'=>'Password not matched'
                                               );
    public $addressRules                = array(
                                                'required'=> 'Address required',
                                               );

    public $phoneRules                  = array(
                                                'required'=> 'Phone Number required',
                                                'numeric'=>  'Phone number should be numeric',
                                                'min_len-7' => 'Phone no must be minimum 7 digits',
                                                'max_len-10' => 'Phone no must be maximum 10 digits',
                                            );
    public $companyPhoneRules           = array(
                                            'required'=> 'Company phone Number required',
                                            'numeric'=>  'Company phone number should be numeric',
                                            'min_len-7' => 'Phone no must be minimum 7 digits',
                                            'max_len-10' => 'Phone no must be maximum 10 digits',
                                        );
    public $req                         = array(
                                                'required'=> 'Phone Number required'
                                                );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        //$this->load->library('common/form_validator');
    }

    public function check_email_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('email_id', $this->emailRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->User_model->check_email($data['email_id']);
        $value = 0;
        if(empty($result)){ $value=1; }
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$value);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function loginHistory_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_ControllFer::HTTP_OK);
        }
        $result = $this->User_model->getSession($data);
        $total_records = count($this->User_model->getTotalSession($data));
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result,'total_records' => $total_records) );
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function changePassword_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $req = array(
            'required'=> 'user id required'
        );
        $this->form_validator->add_rules('user_id', $req);
        $this->form_validator->add_rules('oldpassword', array('required'=>'Old password required','alpha_numeric' => 'Old Password should be alpha numeric','max_len-100' => 'Password should be below 100 characters'));
        $this->form_validator->add_rules('password', $this->passwordRules);
        $this->form_validator->add_rules('cpassword', $this->confirmPasswordRules);
        $validated = $this->form_validator->validate($data);

        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $passwordExist=$this->User_model->passwordExist($data);
        if($passwordExist!=1)
        {
            $result = array('status'=>FALSE,'error'=>array('oldpassword'=>"Existing password doesn't match"),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->User_model->changePassword($data);
        $result = array('status'=>TRUE, 'message' => 'Password changed successfully.', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function usersList_get($type)
    {
        if(empty($type)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['type'] = $type;
        //validating data
        $this->form_validator->add_rules('type', $this->req);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->User_model->getUsersList($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }


    public function userInfo_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $idRule = array('required'=> 'id required');
        $this->form_validator->add_rules('id', $idRule);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->User_model->getUserInfo($data);
        $result->profile_image = getImageUrl($result->profile_image,'profile');

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function userInfo_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        //$this->form_validator->add_rules('username', $this->userNameRules);
        $this->form_validator->add_rules('first_name', $this->firstNameRules);
        $this->form_validator->add_rules('last_name', $this->lastNameRules);
        $this->form_validator->add_rules('email_id', $this->emailRules);
        $this->form_validator->add_rules('password', $this->passwordRules);
        $this->form_validator->add_rules('phone_number', $this->phoneRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $email_check = $this->User_model->check_email($data['email_id']);
        if(!empty($email_check)){
            $result = array('status'=>FALSE,'error'=>array('email_id'=>'Email already exists'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->User_model->createUserInfo($data);

        sendmail($data['email_id'],'Q-lana Account Created','<p>welcome to q-lana,</p><p>hello, '.$data["email_id"].'</p>');

        $result = array('status'=>TRUE, 'message' => 'User created successfully.', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function userInfo_put()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('first_name', $this->firstNameRules);
        $this->form_validator->add_rules('last_name', $this->lastNameRules);
        $this->form_validator->add_rules('phone_number', $this->phoneRules);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->User_model->updateUserInfo($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }
    public function userInfo_delete()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id', $this->req);
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->User_model->deleteUser($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

}