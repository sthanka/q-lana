<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Crm extends REST_Controller
{

    public $userNameRules               = array(
                                                'required'=> 'Enter valid user name',
                                                'max_len-100' => 'User name should be below 100 characters',
                                                );
    public $firstNameRules               = array(
                                                'required'=> 'Enter valid first name',
                                                'max_len-100' => 'First name should be below 100 characters',
                                                );
    public $lastNameRules               = array(
                                                'required'=> 'Enter valid last name',
                                                'max_len-100' => 'Last name should be below 100 characters',
                                               );
    public $companyNameRules               = array(
                                                    'required'=> 'Enter valid company name',
                                                    'max_len-100' => 'Company name should be below 100 characters',
                                                 );
    public $emailRules                  = array(
                                                'required'=> 'Email required',
                                                'valid_email' => 'Enter valid email'
                                                );
    public $passwordRules               = array(
                                                'required'=> 'Password required',
                                                'max_len-100' => 'Password should be below 100 characters'
                                               );
    public $confirmPasswordRules        = array(
                                                'required'=>'Confirm Password required',
                                                'match_field-password'=>'Password not matched'
                                               );
    public $addressRules                = array(
                                                'required'=> 'Address required',
                                               );

    public $phoneRules                  = array(
                                                'required'=> 'Phone Number required',
                                                'numeric'=>  'Phone number should be numeric',
                                                'min_len-6' => 'Phone no must be minimum 6 digits'
                                              );
    public $companyPhoneRules                  = array(
                                                'required'=> 'Company phone Number required',
                                                'numeric'=>  'Company phone number should be numeric',
                                                'min_len-6' => 'Phone no must be minimum 6 digits'
                                               );
    public $req                         = array(
                                                'required'=> 'Phone Number required'
                                                );

    public $allowed_created_by = [];
    public $allowed_company_approval_roles = [];
    public $current_user = 0;
    public $order_data = 0;


    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Crm_model');
        $this->load->model('Master_model');
        $this->load->model('Company_model');
        $this->load->model('Activity_model');
        //$this->load->library('common/form_validator');

        $user_id = 0;
        if(isset($_SERVER['HTTP_USER'])){
            $user_id = $_SERVER['HTTP_USER'];

        }


        $user_info = $this->Company_model->getcompanyUserByUserId(array('user_id' => $user_id));
        if($user_info[0]['user_role_id']==3)
        {
            /*$company_approval_roles = $this->Company_model->getLowerApprovalRoles($user_info[0]['company_id'],$user_info[0]['approval_role_id']);
            $current_approval_role = $this->Company_model->getCompanyApprovalRole(array('company_id' => $user_info[0]['company_id'], 'approval_role_id' => $user_info[0]['approval_role_id']));
            array_push($company_approval_roles,$current_approval_role[0]['id_company_approval_role']);
            $user_ids = $this->Company_model->getCompanyUsersByCompanyApprovalRoles(array('company_id' => $user_info[0]['company_id'], 'company_approval_roles' => $company_approval_roles));
            $this->allowed_created_by = array_map("reset", $user_ids);*/

            /*$this->allowed_created_by = $this->Company_model->getLowerReportingUsers($user_info[0]['company_id'],$user_id);
            array_push($this->allowed_created_by,$user_id);*/
            //print_r($this->allowed_created_by);

            $this->current_user = $user_id;
        }

    }

    public function contactList_get()
    {
        $data = $this->input->get();
        $result = $this->Crm_model->getContactList($data);
        for($s=0;$s<count($result);$s++){
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
            //$percentage = $this->Crm_model->getFieldPercentage($result[$s]['id_crm_contact'],'contact');
            //$percentage = explode(',',$percentage);
            //$result[$s]['percentage'] = ceil(($percentage[1]/$percentage[0])*100);
        }

        if(isset($data['offset'])){ unset($data['offset']); }
        if(isset($data['limit'])){ unset($data['limit']); }

        $total_records = count($this->Crm_model->getContactList($data));

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function crmContactType_get()
    {
        $contact_type = $this->Crm_model->getCrmContactType();
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$contact_type);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactInformation_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('crm_contact_id', array('required'=> 'Contact id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result=$this->Crm_model->getContact($data['crm_contact_id']);

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result[0]);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactForms_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('crm_module_id', array('required'=> 'Module id required'));
        $this->form_validator->add_rules('crm_contact_id', array('required'=> 'Contact id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $crm_contact_id = $data['crm_contact_id'];
        $contact = $this->Crm_model->getContact($data['crm_contact_id']);
        $result=$this->Crm_model->getModuleDetails($data['crm_module_id']);
        $data = array(); $section_id = array();
        $status = 0;
        for($s=0;$s<count($result);$s++)
        {
            if($contact[0]['crm_contact_type_id']!=3)
            {
                if($result[$s]['form_name']=="Applicant's Information")
                {
                    if(!in_array($result[$s]['id_section'],$section_id)){
                        array_push($section_id,$result[$s]['id_section']);
                        $data[$result[$s]['id_section']] = array(
                            'id_section' => $result[$s]['id_section'],
                            'section_name' => $result[$s]['section_name']
                        );
                        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $crm_contact_id, 'crm_module_type' => 'contact', 'reference_id' => $result[$s]['id_form'], 'reference_type' => 'form'));
                        if(empty($module_status)) $status = 0;
                        else $status = 1;
                        $data[$result[$s]['id_section']]['forms'][] = array(
                            'id_form' => $result[$s]['id_form'],
                            'form_name' => $result[$s]['form_name'],
                            'form_template' => $result[$s]['form_template'],
                            'form_class' => $result[$s]['form_class'],
                            'form_key' => $result[$s]['form_key'],
                            'status' => $status
                        );
                    }
                    else{
                        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $crm_contact_id, 'crm_module_type' => 'contact', 'reference_id' => $result[$s]['id_form'], 'reference_type' => 'form'));
                        if(empty($module_status)) $status = 0;
                        else $status = 1;
                        $data[$result[$s]['id_section']]['forms'][] = array(
                            'id_form' => $result[$s]['id_form'],
                            'form_name' => $result[$s]['form_name'],
                            'form_template' => $result[$s]['form_template'],
                            'form_class' => $result[$s]['form_class'],
                            'form_key' => $result[$s]['form_key'],
                            'status' => $status
                        );
                    }
                }
            }
            else
            {
                if(!in_array($result[$s]['id_section'],$section_id)){
                    array_push($section_id,$result[$s]['id_section']);
                    $data[$result[$s]['id_section']] = array(
                        'id_section' => $result[$s]['id_section'],
                        'section_name' => $result[$s]['section_name']
                    );
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $crm_contact_id, 'crm_module_type' => 'contact', 'reference_id' => $result[$s]['id_form'], 'reference_type' => 'form'));
                    if(empty($module_status)) $status = 0;
                    else $status = 1;
                    $data[$result[$s]['id_section']]['forms'][] = array(
                        'id_form' => $result[$s]['id_form'],
                        'form_name' => $result[$s]['form_name'],
                        'form_template' => $result[$s]['form_template'],
                        'form_class' => $result[$s]['form_class'],
                        'form_key' => $result[$s]['form_key'],
                        'status' => $status
                    );
                }
                else{
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $crm_contact_id, 'crm_module_type' => 'contact', 'reference_id' => $result[$s]['id_form'], 'reference_type' => 'form'));
                    if(empty($module_status)) $status = 0;
                    else $status = 1;
                    $data[$result[$s]['id_section']]['forms'][] = array(
                        'id_form' => $result[$s]['id_form'],
                        'form_name' => $result[$s]['form_name'],
                        'form_template' => $result[$s]['form_template'],
                        'form_class' => $result[$s]['form_class'],
                        'form_key' => $result[$s]['form_key'],
                        'status' => $status
                    );
                }
            }
        }

        $data = array_values($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$data);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contact_get($contact_id,$module_id)
    {
        $contact = $this->Crm_model->getContact($contact_id);
        $result = $this->Crm_model->getModuleDetails($module_id);
        $data = $active_forms = array(); $active_form_id = 0;
        $unique_section_names = array_values(array_unique(array_map(function ($i) { return $i['section_name']; }, $result)));
        $unique_id_sections = array_values(array_unique(array_map(function ($i) { return $i['id_section']; }, $result)));

        for($s=0;$s<count($unique_section_names);$s++)
        {
            $data[$s]['id_section'] = $unique_id_sections[$s];
            $data[$s]['section_name'] = $unique_section_names[$s];
            $form = array(); $st= $active_form_id = 0;
            for($r=0;$r<count($result);$r++){
                if($unique_section_names[$s]==$result[$r]['section_name']){
                    $form[$st]['id_form'] = $result[$r]['id_form'];
                    $form[$st]['form_name'] = $result[$r]['form_name'];
                    $form[$st]['form_template'] = $result[$r]['form_template'];
                    $st++;
                }
            }

            $data[$s]['form'] = $form;
        }
        $t=0;
        //getting fields data
        $crm_contact_type_based_fields = array('first_name','last_name','phone_number','email');
        for($s=0;$s<count($data);$s++){
            for($r=0;$r<count($data[$s]['form']);$r++){
                $form_field_data = $this->Crm_model->getContactFormData($contact_id,$data[$s]['form'][$r]['id_form']);
                $field_data = array();
                for($st=0;$st<count($form_field_data);$st++){ //echo "<pre>"; print_r($contact); exit;
                    /*if(trim($form_field_data[$st]['field_type'])=='json'){
                        $field_data[$form_field_data[$st]['field_name']] = json_decode($form_field_data[$st]['form_field_value']);
                        $field_data1[$form_field_data[$st]['field_label']] = json_decode($form_field_data[$st]['form_field_value']);
                    }
                    else{
                        $field_data[$form_field_data[$st]['field_name']] = $form_field_data[$st]['form_field_value'];
                        $field_data1[$form_field_data[$st]['field_label']] = $form_field_data[$st]['form_field_value'];
                    }*/
                    //echo "<pre>"; print_r($form_field_data[$st]);
                    if($contact[0]['crm_contact_type_id']!=3){
                        if(in_array($form_field_data[$st]['field_name'],$crm_contact_type_based_fields)){
                            if(trim($form_field_data[$st]['field_type'])=='json'){

                                $value = json_decode($form_field_data[$st]['form_field_value']);
                                for($sth=0;$sth<count($value);$sth++)
                                {
                                    if(empty($value[$sth])){
                                        $value[$sth] = (object)$value[$sth];
                                    }
                                }
                                $form_field_data[$st]['form_field_value'] = $value;

                                $field_data[] = array(
                                    'field_label' => $form_field_data[$st]['field_label'],
                                    'field_name' => $form_field_data[$st]['field_name'],
                                    'field_type' => $form_field_data[$st]['field_type'],
                                    'field_value' => $form_field_data[$st]['form_field_value']
                                );
                            }
                            else{
                                if($form_field_data[$st]['field_name']=='date_of_birth' && $form_field_data[$st]['form_field_value']!=''){
                                    $form_field_data[$st]['form_field_value'] = date('Y-m-d',strtotime($form_field_data[$st]['form_field_value']));
                                }
                                $field_data[] = array(
                                    'field_label' => $form_field_data[$st]['field_label'],
                                    'field_name' => $form_field_data[$st]['field_name'],
                                    'field_type' => $form_field_data[$st]['field_type'],
                                    'field_value' => $form_field_data[$st]['form_field_value']
                                );
                            }
                        }
                    }
                    else{
                        if(trim($form_field_data[$st]['field_type'])=='json'){

                            $value = json_decode($form_field_data[$st]['form_field_value']);
                            for($sth=0;$sth<count($value);$sth++)
                            {
                                if(empty($value[$sth])){
                                    $value[$sth] = (object)$value[$sth];
                                }
                            }
                            $form_field_data[$st]['form_field_value'] = $value;

                            $field_data[] = array(
                                'field_label' => $form_field_data[$st]['field_label'],
                                'field_name' => $form_field_data[$st]['field_name'],
                                'field_type' => $form_field_data[$st]['field_type'],
                                'field_value' => $form_field_data[$st]['form_field_value']
                            );
                        }
                        else{
                            if($form_field_data[$st]['field_name']=='date_of_birth' && $form_field_data[$st]['form_field_value']!=''){
                                $form_field_data[$st]['form_field_value'] = date('Y-m-d',strtotime($form_field_data[$st]['form_field_value']));
                            }
                            $field_data[] = array(
                                'field_label' => $form_field_data[$st]['field_label'],
                                'field_name' => $form_field_data[$st]['field_name'],
                                'field_type' => $form_field_data[$st]['field_type'],
                                'field_value' => $form_field_data[$st]['form_field_value']
                            );
                        }
                    }


                }
                $data[$s]['form'][$r]['field_data'] = $field_data;
                //getting form is filled or not
                $active_forms[$t]['id'] = $data[$s]['form'][$r]['id_form'];
                $active_forms[$t]['section_id'] = $data[$s]['id_section'];
                $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $contact_id, 'crm_module_type' => 'contact', 'reference_id' => $data[$s]['form'][$r]['id_form'], 'reference_type' => 'form'));
                if(empty($module_status)) $data[$s]['form'][$r]['status'] = 0;
                else $data[$s]['form'][$r]['status'] = 1;
                /*if(empty($module_status)) $active_forms[$t]['status'] = 0;
                else $active_forms[$t]['status'] = 1;*/
                $t++;
            }
        }
        $percentage = $this->Crm_model->getFieldPercentage($contact_id,'contact');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('details' => $contact, 'forms' => $data, 'percentage' => $percentage,'active_forms' => $active_forms));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function module_get($module_id,$crm_id = 0)
    {
        $result = $this->Crm_model->getModuleDetails($module_id);
        $data = array();
        $unique_section_names = array_values(array_unique(array_map(function ($i) { return $i['section_name']; }, $result)));
        $unique_id_sections = array_values(array_unique(array_map(function ($i) { return $i['id_section']; }, $result)));
        for($s=0;$s<count($unique_section_names);$s++)
        {
            $data[$s]['id_section'] = $unique_id_sections[$s];
            $data[$s]['section_name'] = $unique_section_names[$s];
            $form = array(); $st=0;
            for($r=0;$r<count($result);$r++){
                if($unique_section_names[$s]==$result[$r]['section_name']){
                    $form[$st]['id_form'] = $result[$r]['id_form'];
                    $form[$st]['form_name'] = $result[$r]['form_name'];
                    $form[$st]['form_template'] = $result[$r]['form_template'];
                    $form[$st]['form_class'] = $result[$r]['form_class'];
                    $percentage = $this->Crm_model->getFormPercentage($result[$r]['id_form'],'company',$crm_id);
                    $percentage = explode(',',$percentage);
                    if($percentage[0])
                        $percentage = ($percentage[1]/$percentage[0])*100;
                    else
                        $percentage = 0;
                    $form[$st]['percentage'] = $percentage;
                    $st++;
                }
            }
            $data[$s]['form'] = $form;
        }
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$data);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function formData_get($contact_id,$form_id,$type)
    {
        $form_fields = $this->Crm_model->getFormFieldsByFormId($form_id);
        $form_data = $this->Crm_model->getContactFormData($contact_id,$form_id);
        $contact_details = $this->Crm_model->getContact($contact_id);
        $created_date = '';
        if(!empty($contact_details))
            $created_date = $contact_details[0]['created_date_time'];

        $result = array(); $st=0; $crm_contact_type_based_fields = array('first_name','last_name','phone_number','email');
        $result['form_name'] = $form_fields[0]['form_name'];
        for($s=0;$s<count($form_fields);$s++){
            for($r=0;$r<count($form_data);$r++){
                if($form_fields[$s]['id_form_field']==$form_data[$r]['form_field_id']){
                    if($type=='view')
                    {
                        if($contact_details[0]['crm_contact_type_id']==1 || $contact_details[0]['crm_contact_type_id']==2){
                            if(!in_array($form_data[$r]['field_name'],$crm_contact_type_based_fields)){ break; }
                        }
                        if(trim($form_fields[$s]['field_type'])=='json'){

                            $value = json_decode($form_data[$r]['form_field_value']);
                            for($sth=0;$sth<count($value);$sth++)
                            {
                                if(empty($value[$sth])){
                                    $value[$sth] = (object)$value[$sth];
                                }
                            }
                            $form_data[$r]['form_field_value'] = $value;

                            $result['form_data'][$st] = array(
                                'field_label' => $form_data[$r]['field_label'],
                                'field_name' => $form_data[$r]['field_name'],
                                'field_type' => $form_data[$r]['field_type'],
                                'field_value' => $form_data[$r]['form_field_value']
                            );
                            $st++;
                        }
                        else{
                            if($form_data[$r]['field_name']=='date_of_birth'){
                                if(isset($form_data[$r]['form_field_value']) && $form_data[$r]['form_field_value']!='')
                                    $form_data[$r]['form_field_value'] = date('Y-m-d', strtotime($form_data[$r]['form_field_value']));
                            }
                            $result['form_data'][$st] = array(
                                'field_label' => $form_data[$r]['field_label'],
                                'field_name' => $form_data[$r]['field_name'],
                                'field_type' => $form_data[$r]['field_type'],
                                'field_value' => $form_data[$r]['form_field_value']
                            );
                            $st++;
                        }
                    }
                    else
                    {
                        if(trim($form_fields[$s]['field_type'])=='json')
                        {
                            $value = json_decode($form_data[$r]['form_field_value']);
                            for($sth=0;$sth<count($value);$sth++)
                            {
                                if(empty($value[$sth])){
                                    $value[$sth] = (object)$value[$sth];
                                }
                            }
                            $form_data[$r]['form_field_value'] = $value;
                            $result[$form_fields[$s]['field_name']] = $form_data[$r]['form_field_value']; $st++;
                        }
                        else
                        {
                            if($form_data[$r]['field_name']=='date_of_birth'){
                                if(isset($form_data[$r]['form_field_value']) && $form_data[$r]['form_field_value']!='')
                                    $form_data[$r]['form_field_value'] = date('Y-m-d', strtotime($form_data[$r]['form_field_value']));
                            }
                            $result[$form_fields[$s]['field_name']] = $form_data[$r]['form_field_value']; $st++;
                        }

                    }
                }
            }
        }

        $percentage = $this->Crm_model->getFieldPercentage($contact_id,'contact');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' => $result,'percentage' => ceil($percentage), 'contact_details' => $contact_details[0]));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function formFields_get($form_id)
    {
        $result = $this->Crm_model->getFormFieldsByFormId($form_id);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function formData_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $update_type = '0';
        if(isset($data['personal_financial_information'])){ $data['personal_financial_information'] = json_encode($data['personal_financial_information']); $update_type=1; }
        if(isset($data['other_sources_income'])){ $data['other_sources_income'] = json_encode($data['other_sources_income']); $update_type=1; }
        if(isset($data['non_business_expenditure'])){ $data['non_business_expenditure'] = json_encode($data['non_business_expenditure']); $update_type=1; }
        if(isset($data['previous_experience_institution'])){ $data['previous_experience_institution'] = json_encode($data['previous_experience_institution']); $update_type=1; }

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $data_keys = array_keys($data);
        $form_data = array(); $st=0; $type = 'insert';
        $form_fields = $this->Crm_model->getFormFieldsByFormId($data['form_id']);

        if(isset($data['crm_contact_id'])){
            $check_data = $this->Crm_model->getContactFormData($data['crm_contact_id'],$data['form_id']);
            //echo "<pre>"; print_r($check_data); exit;
            if(empty($check_data)){ $type = 'insert'; }
            else{ $type = 'update'; }
        }

        for($s=0;$s<count($form_fields);$s++){ $check_field = 0;
            for($r=0;$r<count($data_keys);$r++){
                if($form_fields[$s]['field_name']==$data_keys[$r]){
                    $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                    $form_data[$st]['form_field_value'] = $data[$data_keys[$r]];
                    $st++;
                    $check_field = 1;
                }
            }
            if($check_field==0){
                $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                $form_data[$st]['form_field_value'] = '';
                $st++;
            }
        }
		
        if(empty($form_data)){
            $result = array('status'=>FALSE, 'error' => 'invalid fields', 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($type=='insert')
        {
            $contact_data = array();
            if(!isset($data['crm_contact_id'])){
                $contact_data = array('company_id' => $data['company_id'], 'created_by' => $data['user_id']);
                if(isset($data['first_name'])){ $contact_data['first_name'] = $data['first_name']; }
                if(isset($data['last_name'])){ $contact_data['last_name'] = $data['last_name']; }
                if(isset($data['email'])){ $contact_data['email'] = $data['email']; }
                if(isset($data['phone_number'])){ $contact_data['phone_number'] = $data['phone_number']; }
                //print_r($contact_data); exit;
                $crm_contact_id = $this->Crm_model->addCrmContact($contact_data);
            }
            else{
                $crm_contact_id = $data['crm_contact_id'];
            }
            for($r=0;$r<count($form_data);$r++){
                $form_data[$r]['crm_contact_id'] = $crm_contact_id;
                $form_data[$r]['created_date_time'] = date('Y-m-d H:i:s');
            }

            if(!empty($contact_data)){
                $contact_data['updated_date_time'] = date('Y-m-d H:i:s');
                $this->Crm_model->updateCrmContact($contact_data,$crm_contact_id);
            }
            $this->Crm_model->addCrmContactData($form_data);
        }
		
        if($type=='update')
        {
            if(!empty($check_data)){
                for($s=0;$s<count($check_data);$s++){
                    for($r=0;$r<count($form_data);$r++){
                        if($check_data[$s]['form_field_id']==$form_data[$r]['form_field_id']){
                            $form_data[$r]['id_crm_contact_data'] = $check_data[$s]['id_crm_contact_data'];
                        }
                    }
                }
            }

            $new_form_fields = array();
            for($sr=0;$sr<count($form_data);$sr++)
            {
                if(!isset($form_data[$sr]['id_crm_contact_data'])){
                    $form_data[$sr]['crm_contact_id'] = $data['crm_contact_id'];
                    $new_form_fields[] = $form_data[$sr];
                    unset($form_data[$sr]);
                }
            }
            //check mail
            if(isset($data['email']) && $data['email']!=''){
                $email_check = $this->User_model->check_email($data['email'],'crm_contact',$data['company_id']);
                if(!empty($email_check)){
                    if($email_check->id_crm_contact!=$data['crm_contact_id']){
                        $result = array('status'=>FALSE,'error'=>array('email'=>'Email already exists'),'data'=>'');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }
                }
                $contact_data['email'] = $data['email'];
            }

            if(!empty($new_form_fields))
                $this->Crm_model->addCrmContactData($new_form_fields);

            $this->Crm_model->updateCrmContactData($form_data,$update_type);
            $crm_contact_id = $data['crm_contact_id'];
            $contact_data = array();
            if(isset($data['first_name'])){ $contact_data['first_name'] = $data['first_name']; }
            if(isset($data['last_name'])){ $contact_data['last_name'] = $data['last_name']; }
            if(isset($data['email']) && $data['email']!=''){
                $email_check = $this->User_model->check_email($data['email'],'crm_contact',$data['company_id']);
                //echo $crm_contact_id."<pre>"; print_r($email_check); exit;
                if(!empty($email_check)){
                    if($email_check->id_crm_contact!=$crm_contact_id){
                        $result = array('status'=>FALSE,'error'=>array('email'=>'Email already exists'),'data'=>'');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }
                }
                $contact_data['email'] = $data['email'];
            }
            if(isset($data['phone_number'])){ $contact_data['phone_number'] = $data['phone_number']; }
            if(!empty($contact_data)){
                $contact_data['updated_date_time'] = date('Y-m-d H:i:s');
                $this->Crm_model->updateCrmContact($contact_data,$crm_contact_id);
            }
        }
        $percentage = $this->Crm_model->getFieldPercentage($crm_contact_id,'contact');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;

        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'],'crm_module_type' => 'contact','reference_id' => $data['form_id'], 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_contact_id'], 'crm_module_type' => 'contact', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $data['user_id'], 'updated_date_time' => date('Y-m-d H:i:s')));
        //adding activity
        $form_info = $this->Crm_model->getFormInformation($data['form_id']);
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'],'activity_template' => 'Form information has been updated','module_type' => 'contact','module_id' => $data['crm_contact_id'], 'activity_type' => 'form','activity_reference_id' => $data['form_id'],'created_by' => $data['user_id']));

        $result = array('status'=>TRUE, 'message' => 'Information updated successfully.', 'data'=>array('crm_contact_id' => $crm_contact_id,'percentage' => ceil($percentage)));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function company_get($company_id,$module_id)
    {
        $contact = $this->Crm_model->getCompany($company_id);
        $contact['sector'] = $contact['subsector'] = '';
        $result = $this->Crm_model->getModuleDetails($module_id);
        $data = $active_forms = array();
        $unique_section_names = array_values(array_unique(array_map(function ($i) { return $i['section_name']; }, $result)));
        $unique_id_sections = array_values(array_unique(array_map(function ($i) { return $i['id_section']; }, $result)));
        for($s=0;$s<count($unique_section_names);$s++)
        {
            $data[$s]['id_section'] = $unique_id_sections[$s];
            $data[$s]['section_name'] = $unique_section_names[$s];
            $form = array(); $st=0;
            for($r=0;$r<count($result);$r++){
                if($unique_section_names[$s]==$result[$r]['section_name']){
                    $form[$st]['id_form'] = $result[$r]['id_form'];
                    $form[$st]['form_name'] = $result[$r]['form_name'];
                    $form[$st]['form_template'] = $result[$r]['form_template'];
                    $form[$st]['form_class'] = $result[$r]['form_class'];
                    $form[$st]['form_key'] = $result[$r]['form_key'];
                    $st++;
                }
            }
            $data[$s]['form'] = $form;

        }
        //getting fields data
        $t = 0;
        for($s=0;$s<count($data);$s++){
            for($r=0;$r<count($data[$s]['form']);$r++)
            {
                /*$form_field_data = $this->Crm_model->getCompanyFormData($company_id,$data[$s]['form'][$r]['id_form']);
                $field_data = array();
                for($st=0;$st<count($form_field_data);$st++)
                {
                    if(trim($form_field_data[$st]['field_type'])=='json'){
                        $field_data[$form_field_data[$st]['field_name']] = json_decode($form_field_data[$st]['form_field_value']);
                        $field_data1[$form_field_data[$st]['field_label']] = json_decode($form_field_data[$st]['form_field_value']);
                    }
                    else{
                        $field_data[$form_field_data[$st]['field_name']] = $form_field_data[$st]['form_field_value'];
                        $field_data1[$form_field_data[$st]['field_label']] = $form_field_data[$st]['form_field_value'];
                    }
                    //print_r($form_field_data); exit;
                    if(trim($form_field_data[$st]['field_type'])=='json'){

                        $value = json_decode($form_field_data[$st]['form_field_value']);
                        for($sth=0;$sth<count($value);$sth++)
                        {
                            if(empty($value[$sth])){
                                $value[$sth] = (object)$value[$sth];
                            }
                        }
                        $form_field_data[$st]['form_field_value'] = $value;

                        $field_data[] = array(
                            'field_label' => $form_field_data[$st]['field_label'],
                            'field_name' => $form_field_data[$st]['field_name'],
                            'field_type' => $form_field_data[$st]['field_type'],
                            'field_value' => $form_field_data[$st]['form_field_value']
                        );
                    }
                    else{
                        $field_data[] = array(
                            'field_label' => $form_field_data[$st]['field_label'],
                            'field_name' => $form_field_data[$st]['field_name'],
                            'field_type' => $form_field_data[$st]['field_type'],
                            'field_value' => $form_field_data[$st]['form_field_value']
                        );
                    }


                    for($sth=0;$sth<count($field_data);$sth++)
                    {
                        if($field_data[$sth]['field_name']=='sector' || $field_data[$sth]['field_name']=='subsector'){
                            $sector = $this->Master_model->getSector(array('id_sector' => $field_data[$sth]['field_value']));
                            if(!empty($sector)) {
                                $field_data[$sth]['field_value'] = $sector->sector_name;
                            }
                        }
                        else if($field_data[$sth]['field_name']=='business_start_date'){
                            if(isset($field_data[$sth]['field_name']['field_value']) && $field_data[$sth]['field_name']['field_value']!='')
                                $field_data[$sth]['field_value'] = date('Y-m-d', strtotime($field_data[$sth]['field_name']['field_value']));
                        }
                        else if($field_data[$sth]['field_name']=='operating_in_this_place'){
                            if(isset($field_data[$sth['field_name']['field_value']]) &&$field_data[$sth]['field_name']['field_value']!='')
                                $field_data[$sth]['field_value'] = date('Y-m-d', strtotime($field_data[$sth]['field_name']['field_value']));
                        }
                        if($field_data[$sth]['field_name']=='business_country'){
                            if(isset($field_data[$sth]['field_value']) && $field_data[$sth]['field_value']!=''){
                                $country = $this->Master_model->getCountry(array('id_country' =>$field_data[$sth]['field_value']));
                                if(!empty($country))
                                    $field_data[$sth]['field_value'] = $country->country_name;
                            }
                        }
                    }
                }*/
                //print_r($data[$s]['form']); exit;
                $form_fields = $this->Crm_model->getFormFieldsByFormId($data[$s]['form'][$r]['id_form']);
                $form_data = $this->Crm_model->getCompanyFormData($company_id,$data[$s]['form'][$r]['id_form']);
                $field_data = array();

                for($sr=0;$sr<count($form_fields);$sr++){
                    $field_data[] = array(
                        'id_form_field' => $form_fields[$sr]['id_form_field'],
                        'field_label' => $form_fields[$sr]['field_label'],
                        'field_name' => $form_fields[$sr]['field_name'],
                        'field_type' => $form_fields[$sr]['field_type'],
                        'field_value' => '---'
                    );
                }

                //print_r($form_fields); exit;
                $st=0;
                for($sr=0;$sr<count($form_fields);$sr++){
                    $field_data[$sr] = array(
                        'field_label' => $form_fields[$sr]['field_label'],
                        'field_name' => $form_fields[$sr]['field_name'],
                        'field_type' => $form_fields[$sr]['field_type'],
                        'field_value' => '--'
                    );
                    for($ru=0;$ru<count($form_data);$ru++){
                        if($form_fields[$sr]['id_form_field']==$form_data[$ru]['form_field_id']){

                                if(trim($form_fields[$sr]['field_type'])=='json'){

                                    $value = json_decode($form_data[$ru]['form_field_value']);
                                    for($sth=0;$sth<count($value);$sth++)
                                    {
                                        if(empty($value[$sth])){
                                            $value[$sth] = (object)$value[$sth];
                                        }
                                    }
                                    $form_data[$ru]['form_field_value'] = $value;

                                    $field_data[$sr] = array(
                                        'field_label' => $form_data[$ru]['field_label'],
                                        'field_name' => $form_data[$ru]['field_name'],
                                        'field_type' => $form_data[$ru]['field_type'],
                                        'field_value' => $form_data[$ru]['form_field_value']
                                    );
                                    $st++;
                                }
                                else{
                                    if($form_data[$ru]['field_name']=='sector' || $form_data[$ru]['field_name']=='subsector'){
                                        $sector = $this->Master_model->getSector(array('id_sector' => $form_data[$ru]['form_field_value']));
                                        //echo "<pre>"; print_r($sector); exit;
                                        if(!empty($sector))
                                            $form_data[$ru]['form_field_value'] = $sector->sector_name;
                                    }
                                    else if($form_data[$ru]['field_name']=='business_start_date'){
                                        if(isset($form_data[$ru]['form_field_value']) && $form_data[$ru]['form_field_value']!='')
                                            $form_data[$ru]['form_field_value'] = date('Y-m-d', strtotime($form_data[$ru]['form_field_value']));
                                    }
                                    else if($form_data[$ru]['field_name']=='operating_in_this_place'){
                                        if(isset($form_data[$ru]['form_field_value']) && $form_data[$ru]['form_field_value']!='')
                                            $form_data[$ru]['form_field_value'] = date('Y-m-d', strtotime($form_data[$ru]['form_field_value']));
                                    }
                                    else if($form_data[$ru]['field_name']=='business_country'){
                                        if(isset($form_data[$ru]['form_field_value']) && $form_data[$ru]['form_field_value']!='') {
                                            $country = $this->Master_model->getCountry(array('id_country' => $form_data[$ru]['form_field_value']));
                                            $form_data[$ru]['form_field_value'] = $country->country_name;
                                        }
                                    }

                                    $field_data[$sr] = array(
                                        'field_label' => $form_data[$ru]['field_label'],
                                        'field_name' => $form_data[$ru]['field_name'],
                                        'field_type' => $form_data[$ru]['field_type'],
                                        'field_value' => $form_data[$ru]['form_field_value']
                                    );
                                    $st++;
                                }
                        }
                    }
                }

                $data[$s]['form'][$r]['field_data'] = $field_data;
                //getting form is filled or not
                /*$active_forms[$t]['id'] = $data[$s]['form'][$r]['id_form'];
                $active_forms[$t]['section_id'] = $data[$s]['id_section'];*/
                $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $company_id, 'crm_module_type' => 'company', 'reference_id' => $data[$s]['form'][$r]['id_form'], 'reference_type' => 'form'));
                if(empty($module_status)) $data[$s]['form'][$r]['status'] = 0;
                else $data[$s]['form'][$r]['status'] = 1; $t++;
                /*if(empty($module_status)) $active_forms[$t]['status'] = 0;
                else $active_forms[$t]['status'] = 1; $t++;*/
            }
        }
        $percentage = $this->Crm_model->getFieldPercentage($company_id,'company');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('details' => $contact, 'forms' => $data, 'percentage' => ceil($percentage), 'active_forms' => $active_forms));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyList_get()
    {
        $data = $this->input->get();

        if(isset($data['crm_project_id'])){
            $project = $this->Crm_model->getProject($data['crm_project_id']);
            $data['company_id'] = $project[0]['company_id'];
        }
        $result = $this->Crm_model->getCompanyList($data);
        /*for($s=0;$s<count($result);$s++){
            $percentage = $this->Crm_model->getFieldPercentage($result[$s]['id_crm_company'],'company');
            $percentage = explode(',',$percentage);
            $result[$s]['percentage'] = ceil(($percentage[1]/$percentage[0])*100);
        }*/
        if(isset($data['offset'])){ unset($data['offset']); }
        if(isset($data['limit'])){ unset($data['limit']); }
        $total_records = count($this->Crm_model->getCompanyList($data));
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyForm_get($company_id,$form_id,$type)
    {
        $form_fields = $this->Crm_model->getFormFieldsByFormId($form_id);
        $form_data = $this->Crm_model->getCompanyFormData($company_id,$form_id);
        $company_details = $this->Crm_model->getCompany($company_id);
        $created_date = '';

        $result = array(); $st=0;
        $result['form_name'] = $form_fields[0]['form_name'];
        for($s=0;$s<count($form_fields);$s++){
            for($r=0;$r<count($form_data);$r++){
                if($form_fields[$s]['id_form_field']==$form_data[$r]['form_field_id']){
                    if($type=='view')
                    {
                        if(trim($form_fields[$s]['field_type'])=='json'){

                            $value = json_decode($form_data[$r]['form_field_value']);
                            for($sth=0;$sth<count($value);$sth++)
                            {
                                if(empty($value[$sth])){
                                    $value[$sth] = (object)$value[$sth];
                                }
                            }
                            $form_data[$r]['form_field_value'] = $value;

                            $result['form_data'][$st] = array(
                                'field_label' => $form_data[$r]['field_label'],
                                'field_name' => $form_data[$r]['field_name'],
                                'field_type' => $form_data[$r]['field_type'],
                                'field_value' => $form_data[$r]['form_field_value']
                            );
                            $st++;
                        }
                        else{
                            if($form_data[$r]['field_name']=='sector' || $form_data[$r]['field_name']=='subsector'){
                                $sector = $this->Master_model->getSector(array('id_sector' => $form_data[$r]['form_field_value']));
                                //echo "<pre>"; print_r($sector); exit;
                                if(!empty($sector))
                                    $form_data[$r]['form_field_value'] = $sector->sector_name;
                            }
                            else if($form_data[$r]['field_name']=='business_start_date'){
                                if(isset($form_data[$r]['form_field_value']) && $form_data[$r]['form_field_value']!='')
                                    $form_data[$r]['form_field_value'] = date('Y-m-d', strtotime($form_data[$r]['form_field_value']));
                            }
                            else if($form_data[$r]['field_name']=='operating_in_this_place'){
                                if(isset($form_data[$r]['form_field_value']) && $form_data[$r]['form_field_value']!='')
                                    $form_data[$r]['form_field_value'] = date('Y-m-d', strtotime($form_data[$r]['form_field_value']));
                            }
                            else if($form_data[$r]['field_name']=='business_country'){
                                if(isset($form_data[$r]['form_field_value']) && $form_data[$r]['form_field_value']!='') {
                                    $country = $this->Master_model->getCountry(array('id_country' => $form_data[$r]['form_field_value']));
                                    $form_data[$r]['form_field_value'] = $country->country_name;
                                }
                            }
                            $result['form_data'][$st] = array(
                                'field_label' => $form_data[$r]['field_label'],
                                'field_name' => $form_data[$r]['field_name'],
                                'field_type' => $form_data[$r]['field_type'],
                                'field_value' => $form_data[$r]['form_field_value']
                            );
                            $st++;
                        }
                    }
                    else
                    {
                        if(trim($form_fields[$s]['field_type'])=='json'){

                            $value = json_decode($form_data[$r]['form_field_value']);
                            for($sth=0;$sth<count($value);$sth++)
                            {
                                if(empty($value[$sth])){
                                    $value[$sth] = (object)$value[$sth];
                                }
                            }
                            $form_data[$r]['form_field_value'] = $value;

                            $result[$form_fields[$s]['field_name']] = $form_data[$r]['form_field_value']; $st++;
                        }
                        else{
                            if($form_data[$r]['field_name']=='business_start_date'){
                                if(isset($form_data[$r]['form_field_value']) && $form_data[$r]['form_field_value']!='')
                                    $form_data[$r]['form_field_value'] = date('Y-m-d', strtotime($form_data[$r]['form_field_value']));
                            }
                            else if($form_data[$r]['field_name']=='operating_in_this_place'){
                                if(isset($form_data[$r]['form_field_value']) && $form_data[$r]['form_field_value']!='')
                                    $form_data[$r]['form_field_value'] = date('Y-m-d', strtotime($form_data[$r]['form_field_value']));
                            }

                            $result[$form_fields[$s]['field_name']] = $form_data[$r]['form_field_value']; $st++;
                        }
                    }

                    /**/
                }
            }
        }
        $percentage = $this->Crm_model->getFieldPercentage($company_id,'company');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' => $result,'percentage' => ceil($percentage), 'company_details' => $company_details[0]));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function company_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $update_type = '0';

        if(isset($data['assessment_questions'])){ $data['assessment_questions'] = json_encode($data['assessment_questions']); $update_type=1; }
        if(isset($data['risks_from_suppliers'])){ $data['risks_from_suppliers'] = json_encode($data['risks_from_suppliers']); $update_type=1; }
        if(isset($data['bargaining_buyers'])){ $data['bargaining_buyers'] = json_encode($data['bargaining_buyers']); $update_type=1; }
        if(isset($data['threats_new_entrants'])){ $data['threats_new_entrants'] = json_encode($data['threats_new_entrants']); $update_type=1; }
        if(isset($data['cash_inflow'])){ $data['cash_inflow'] = json_encode($data['cash_inflow']); $update_type=1; }
        if(isset($data['cash_outflow'])){ $data['cash_outflow'] = json_encode($data['cash_outflow']); $update_type=1; }
        if(isset($data['financial_indicators'])){ $data['financial_indicators'] = json_encode($data['financial_indicators']); $update_type=1; }
        if(isset($data['threats_from_subsitutes'])){ $data['threats_from_subsitutes'] = json_encode($data['threats_from_subsitutes']); $update_type=1; }
        if(isset($data['ownership_details'])){ $data['ownership_details'] = json_encode($data['ownership_details']);  }
        if(isset($data['business_social_networks'])){ $data['business_social_networks'] = json_encode($data['business_social_networks']);  }

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $data_keys = array_keys($data);
        $form_data = array(); $st=0; $type = 'insert';
        $form_fields = $this->Crm_model->getFormFieldsByFormId($data['form_id']);

        if(isset($data['crm_company_id'])){
            $check_data = $this->Crm_model->getCompanyFormData($data['crm_company_id'],$data['form_id']);
            //echo "<pre>"; print_r($check_data); exit;
            if(empty($check_data)){ $type = 'insert'; }
            else{ $type = 'update'; }
        }

        for($s=0;$s<count($form_fields);$s++){ $check_field = 0;
            for($r=0;$r<count($data_keys);$r++){
                if($form_fields[$s]['field_name']==$data_keys[$r]){
                    $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                    $form_data[$st]['form_field_value'] = $data[$data_keys[$r]];
                    $st++;
                    $check_field = 1;
                }
            }
            if($check_field==0){
                $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                $form_data[$st]['form_field_value'] = '';
                $st++;
            }
        }
        if(empty($form_data)){
            $result = array('status'=>FALSE, 'error' => 'invalid fields', 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($type=='insert')
        {
            if(!isset($data['crm_company_id'])){
                $company_data = array('company_id' => $data['company_id'], 'created_by' => $data['user_id']);
                if(isset($data['business_name']) && $data['business_name']!=''){
                    $company_data['company_name'] = $data['business_name'];
                }
                if(isset($data['sector'])){
                    $company_data['sector_id'] = $data['sector'];
                }
                if(isset($data['subsector'])){
                    $company_data['sub_sector_id'] = $data['subsector'];
                }
                if(isset($data['business_description'])){
                    $company_data['company_description'] = $data['business_description'];
                }
                if(isset($data['business_phone_number'])){
                    $company_data['phone_number'] = $data['business_phone_number'];
                }
                $crm_company_id = $this->Crm_model->addCrmCompany($company_data);
            }
            else{
                $crm_company_id = $data['crm_company_id'];
                $company_data = array();
                if(isset($data['business_name']) && $data['business_name']!=''){
                    $company_data['company_name'] = $data['business_name'];
                }
                if(isset($data['sector'])){
                    $company_data['sector_id'] = $data['sector'];
                }
                if(isset($data['subsector'])){
                    $company_data['sub_sector_id'] = $data['subsector'];
                }
                if(isset($data['business_description'])){
                    $company_data['company_description'] = $data['business_description'];
                }
                if(isset($data['business_phone_number'])){
                    $company_data['phone_number'] = $data['business_phone_number'];
                }
                if(isset($data['business_email']) && $data['business_email']!=''){
                    $email_check = $this->User_model->check_email($data['business_email'],'crm_company',$data['company_id']);
                    if(!empty($email_check)){
                        if($email_check->id_crm_company!=$data['crm_company_id']){
                            $result = array('status'=>FALSE,'error'=>array('business_email'=>'Email already exists'),'data'=>'');
                            $this->response($result, REST_Controller::HTTP_OK);
                        }
                    }
                    $company_data['email'] = $data['business_email'];
                }
            }
            for($r=0;$r<count($form_data);$r++){
                $form_data[$r]['crm_company_id'] = $crm_company_id;
            }

            $this->Crm_model->addCrmCompanyData($form_data);
        }
        if($type=='update')
        {
            if(!empty($check_data)){
                for($s=0;$s<count($check_data);$s++){
                    for($r=0;$r<count($form_data);$r++){
                        if($check_data[$s]['form_field_id']==$form_data[$r]['form_field_id']){
                            $form_data[$r]['id_crm_company_data'] = $check_data[$s]['id_crm_company_data'];
                        }
                    }
                }
            }
            $new_form_fields = array();
            for($sr=0;$sr<count($form_data);$sr++)
            {
                if(!isset($form_data[$sr]['id_crm_company_data'])){
                    $form_data[$sr]['crm_company_id'] = $data['crm_company_id'];
                    $new_form_fields[] = $form_data[$sr];
                    unset($form_data[$sr]);
                }
            }

            if(!empty($new_form_fields))
                $this->Crm_model->addCrmCompanyData($new_form_fields);

            $this->Crm_model->updateCrmCompanyData($form_data,$update_type);
            $crm_company_id = $data['crm_company_id'];
            $company_data = array();
            if(isset($data['business_name']) && $data['business_name']!=''){
                $company_data['company_name'] = $data['business_name'];
            }
            if(isset($data['sector'])){
                $company_data['sector_id'] = $data['sector'];
            }
            if(isset($data['subsector'])){
                $company_data['sub_sector_id'] = $data['subsector'];
            }
            if(isset($data['business_description'])){
                $company_data['company_description'] = $data['business_description'];
            }
            if(isset($data['business_phone_number'])){
                $company_data['phone_number'] = $data['business_phone_number'];
            }
            if(isset($data['business_email']) && $data['business_email']!=''){
                $email_check = $this->User_model->check_email($data['business_email'],'crm_company',$data['company_id']);
                if(!empty($email_check)){
                    if($email_check->id_crm_company!=$data['crm_company_id']){
                        $result = array('status'=>FALSE,'error'=>array('business_email'=>'Email already exists'),'data'=>'');
                        $this->response($result, REST_Controller::HTTP_OK);
                    }
                }
                $company_data['email'] = $data['business_email'];
            }
        }
        if(!empty($company_data)){
            $this->Crm_model->updateCrmcompany($company_data,$crm_company_id);
        }
        $percentage = $this->Crm_model->getFieldPercentage($crm_company_id,'company');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;

        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $data['form_id'], 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_company_id'], 'crm_module_type' => 'company', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $data['user_id'], 'updated_date_time' => date('Y-m-d H:i:s')));

        //adding activity
        $form_info = $this->Crm_model->getFormInformation($data['form_id']);
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'],'activity_template' => 'information has been updated','module_type' => 'company','module_id' =>  $data['crm_company_id'], 'activity_type' => 'form','activity_reference_id' => $data['form_id'],'created_by' => $data['user_id']));

        $result = array('status'=>TRUE, 'message' => 'Information updated successfully.', 'data'=>array('crm_company_id' => $crm_company_id,'percentage' => ceil($percentage)));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    function contact_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('id_crm_contact',array('required'=> 'Contact id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $crm_contact_id = $data['id_crm_contact'];
        $this->Crm_model->updateCrmContact($data,$crm_contact_id);
        $result = array('status'=>TRUE, 'message' => 'Contact updated successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }


    public function quickContact_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        //echo "<pre>"; print_r($data); exit;
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        /*unset($data['designation_id']);*/
        $this->form_validator->add_rules('first_name',$this->firstNameRules);
        //$this->form_validator->add_rules('last_name', $this->lastNameRules);
        $this->form_validator->add_rules('email', $this->emailRules);
        //$this->form_validator->add_rules('phone_number', $this->phoneRules);
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $this->form_validator->add_rules('crm_contact_type_id', array('required'=> 'Contact Type required'));
        $this->form_validator->add_rules('created_by', array('required'=> 'User id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $email_check = $this->User_model->check_email($data['email'],'crm_contact',$data['company_id']);
        if(!empty($email_check)){
            $result = array('status'=>FALSE,'error'=>array('email'=>'Email already exists'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($_FILES) && !empty($_FILES['file']['name']['profile_image']))
        {
            if(!file_exists('uploads/'.$data['company_id'])){
                mkdir('uploads/'.$data['company_id']);
            }
            $folder = $data['company_id'];
            $path='uploads/';

            $imageName = doUpload($_FILES['file']['tmp_name']['profile_image'],$_FILES['file']['name']['profile_image'],$path,$folder,'image');
            if($imageName===0){
                $result = array('status' => FALSE, 'error' => 'upload only jpg,png format files only', 'data' => '');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            $data['profile_image'] = $imageName;
        }
        else
        {
            $data['profile_image'] = '';
        }
        //adding new company
        if(isset($data['crm_company_name']) && $data['crm_company_name']!=''){
            $company = array(
                'company_id'   => $data['company_id'],
                'created_by'   => $data['created_by'],
                'company_name' => $data['crm_company_name']
            );
            $data['crm_company_id'] = $this->Crm_model->addCrmCompany($company);
            unset($data['crm_company_name']);
        }

        $map_data = array();
        if(isset($data['crm_company_id']) && $data['crm_company_id']!='' && isset($data['crm_company_designation_id']) && $data['crm_company_designation_id']!='')
        {
            $map_data = array(
                'crm_company_id'                => $data['crm_company_id'],
                'crm_company_designation_id'    => $data['crm_company_designation_id'],
                'created_by'                    => $data['created_by'],
                'is_primary_company'            => '1'
            );

            unset($data['crm_company_id']);
            unset($data['crm_company_designation_id']);
        }
        $data['updated_date_time'] = date('Y-m-d H:i:s');
        $crm_contact_id = $this->Crm_model->addQuickContact($data);

        if(!empty($map_data))
        {
            $map_data['crm_contact_id'] = $crm_contact_id;
            //echo "<pre>"; print_r($map_data); exit;
            $this->Crm_model->mapModule($map_data);
        }

        $data_keys = array_keys($data);
        $forms = $this->Crm_model->getModuleDetails(1);
        $st=0; $form_data = array();
        $form_fields = $this->Crm_model->getFormFieldsByFormId($forms[0]['id_form']);
        for($s=0;$s<count($form_fields);$s++){ $check_field = 0;
            for($r=0;$r<count($data_keys);$r++){
                if($form_fields[$s]['field_name']==$data_keys[$r]){
                    $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                    $form_data[$st]['form_field_value'] = $data[$data_keys[$r]];
                    $st++;
                    $check_field = 1;
                }
            }
            if($check_field==0){
                $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                $form_data[$st]['form_field_value'] = '';
                $st++;
            }
        }
        for($r=0;$r<count($form_data);$r++){
            $form_data[$r]['crm_contact_id'] = $crm_contact_id;
            $form_data[$r]['created_date_time'] = date('Y-m-d H:i:s');
        }
        $this->Crm_model->addCrmContactData($form_data);
        //adding activity
        $this->Activity_model->addActivity(array('activity_name' => 'Contact','activity_template' => 'Contact created','module_type' => 'contact','module_id' => $crm_contact_id, 'activity_type' => 'created','activity_reference_id' => $crm_contact_id,'created_by' => $data['created_by']));

        $result = array('status'=>TRUE, 'message' => 'Contact added successfully.', 'data'=>array('crm_contact_id' => $crm_contact_id));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function quickCompany_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_name',array('required'=> 'Company name required'));
        $this->form_validator->add_rules('email', $this->emailRules);
        $this->form_validator->add_rules('phone_number', $this->phoneRules);
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $this->form_validator->add_rules('created_by', array('required'=> 'User id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $email_check = $this->User_model->check_email($data['email'],'crm_company',$data['company_id']);
        if(!empty($email_check)){
            $result = array('status'=>FALSE,'error'=>array('email'=>'Email already exists'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $crm_company_id = $this->Crm_model->addQuickCompany($data);

        if(isset($data['company_name'])){
            $data['business_name'] = $data['company_name'];
        }
        $data_keys = array_keys($data);
        $forms = $this->Crm_model->getModuleDetails(2);
        $st=0; $form_data = array();
        $form_fields = $this->Crm_model->getFormFieldsByFormId($forms[0]['id_form']);
        for($s=0;$s<count($form_fields);$s++){ $check_field = 0;
            for($r=0;$r<count($data_keys);$r++){
                if($form_fields[$s]['field_name']==$data_keys[$r]){
                    $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                    $form_data[$st]['form_field_value'] = $data[$data_keys[$r]];
                    $st++;
                    $check_field = 1;
                }
            }
            if($check_field==0){
                $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                $form_data[$st]['form_field_value'] = '';
                $st++;
            }
        }
        for($r=0;$r<count($form_data);$r++){
            $form_data[$r]['crm_company_id'] = $crm_company_id;
        }
        $this->Crm_model->addCrmCompanyData($form_data);

        //adding activity
        $this->Activity_model->addActivity(array('activity_name' => 'Company','activity_template' => 'Company created','module_type' => 'company','module_id' => $crm_company_id, 'activity_type' => 'created','activity_reference_id' => $crm_company_id,'created_by' => $data['created_by']));

        $result = array('status'=>TRUE, 'message' => 'Company added successfully.', 'data'=>array('crm_company_id' => $crm_company_id));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function document_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $media_id = '';
        $company_id = $this->Crm_model->getCrmCompanyIdByModuleId($data['group_id'],$data['module_id']);
        $company_id = $company_id->company_id;
        if(!file_exists('uploads/'.$company_id)){
            mkdir('uploads/'.$company_id);
        }


        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $folder_structure = array($company_id,$data['group_id']);
        if(isset($_FILES) && !empty($_FILES['file']['name']['attachment']))
        {
                $file_name = $_FILES['file']['name']['attachment'];
                $mediaType = $_FILES['file']['type']['attachment'];
                $imageName = doUpload($_FILES['file']['tmp_name']['attachment'],$_FILES['file']['name']['attachment'],'uploads/',$company_id);

                if($imageName===0){
                    $result = array('status' => FALSE, 'error' => 'upload attachment error occurred', 'data' => '');
                    $this->response($result, REST_Controller::HTTP_OK);
                }
                $document = array('document_source' => $imageName, 'document_name' => $file_name, 'crm_document_type_id' => $data['document_type'], 'uploaded_from_id' => $data['group_id'], 'document_description' => $data['description'], 'document_mime_type' => $mediaType, 'uploaded_by' => $data['user_id']);
                $document_id = $this->Crm_model->addDocument($document);

                //saving activity for activity table --start
                $module = $this->Crm_model->getModuleByDocumentType($data['document_type']);
                if(!empty($module)){
                    if($data['module_id']==1){ $module_type = 'contact'; }
                    else if($data['module_id']==2){ $module_type = 'company'; }
                    else if($data['module_id']==3){ $module_type = 'project'; }
                    $uploaded_user = $this->Company_model->getCompanyUser($data['user_id']);
                    $activityTemplate = '<p class="f12"><span class="blue">'.ucfirst($uploaded_user->first_name).' '.ucfirst($uploaded_user->last_name).'</span> has attached new document <a class="sky-blue" href="javascript:;" data-activity-type="file" data-activity-id="'.$document_id.'">'.ucfirst($file_name).'</a></p>';
                    $activity = array(
                        'activity_name' => 'Document',
                        'activity_type' => 'attachment',
                        'activity_template' =>$activityTemplate,
                        'activity_reference_id' => $document_id,
                        'module_type' => $module_type,
                        'module_id' => $data['group_id'],
                        'created_by' => $data['user_id']
                    );
                    $this->Activity_model->addActivity($activity);
                }
                //saving activity --end
        }

        //adding activity
        /*if($data['module_id']==1){ $module_type = 'contact'; }
        else if($data['module_id']==2){ $module_type = 'company'; }
        else if($data['module_id']==3){ $module_type = 'project'; }
        $this->Activity_model->addActivity(array('activity_name' => 'Document','activity_template' => 'Attachment added for '.$module_type,'module_type' => $module_type,'module_id' => $data['group_id'], 'activity_type' => 'attachment','activity_reference_id' => $document_id,'created_by' => $data['user_id']));*/

        $result = array('status'=>TRUE, 'message' => 'Documents added successfully.', 'data'=>array('document_id' => $document_id));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function document_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //validating data
        $this->form_validator->add_rules('module_id', array('required'=> 'module id required'));
        $this->form_validator->add_rules('group_id', array('required'=> 'group id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result=$this->Crm_model->getDocument($data);
        for($m=0;$m<count($result);$m++)
        {
            $result[$m]['full_path']=getImageUrl($result[$m]['document_source'],'');
            $result[$m]['size']=get_file_info('uploads/'.$result[$m]['document_source'], 'size');
        }
        //print_r($mediaResult);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function documentType_get($module_id)
    {
        $documents = $this->Crm_model->getDocumentType($module_id);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$documents);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function quickProject_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('project_title',array('required'=> 'Project Title required'));
        $this->form_validator->add_rules('project_description', array('required'=> 'Project description required'));
        $this->form_validator->add_rules('project_main_sector_id', array('required'=> 'Sector required'));
        //$this->form_validator->add_rules('project_sub_sector_id', array('required'=> 'Sub Sector id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data['assigned_to'] = $data['created_by'];
        $crm_project_id = $this->Crm_model->addQuickProject($data);

        $data_keys = array_keys($data);
        $forms = $this->Crm_model->getModuleDetails(3);
        $st=0; $form_data = array();
        $form_fields = $this->Crm_model->getFormFieldsByFormId($forms[0]['id_form']);
        for($s=0;$s<count($form_fields);$s++){ $check_field = 0;
            for($r=0;$r<count($data_keys);$r++){
                if($form_fields[$s]['field_name']==$data_keys[$r]){
                    $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                    $form_data[$st]['form_field_value'] = $data[$data_keys[$r]];
                    $st++;
                    $check_field = 1;
                }
            }
            if($check_field==0){
                $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                $form_data[$st]['form_field_value'] = '';
                $st++;
            }
        }
        for($r=0;$r<count($form_data);$r++){
            $form_data[$r]['crm_project_id'] = $crm_project_id;
        }
        $this->Crm_model->addCrmProjectData($form_data);
        //adding activity
        $this->Activity_model->addActivity(array('activity_name' => 'Project','activity_template' => 'Project created','module_type' => 'project','module_id' => $crm_project_id, 'activity_type' => 'created','activity_reference_id' => $crm_project_id,'created_by' => $data['created_by']));

        $result = array('status'=>TRUE, 'message' => 'Project added successfully..', 'data'=>array('crm_project_id' => $crm_project_id));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function project_get($project_id,$module_id,$current_user)
    {
        $project = $this->Crm_model->getProject($project_id);
//echo '<pre>'; print_r($project); exit;
        $project_company = $this->Crm_model->getProjectCompany(array('crm_project_id' => $project_id));
        $project_company = array_map(function($key){ return $key['company_name']; }, $project_company);
        $project[0]['company_name'] = implode(',',$project_company);

        $project_contact = $this->Crm_model->getProjectContact(array('crm_project_id' => $project_id));
        $project_contact = array_map(function($key){ return $key['first_name']; }, $project_contact);
        $project[0]['contact_name'] = implode(',',$project_contact);

        $project[0]['loan_amount'] = 0;
        $loan = $this->Crm_model->getFacilityLoanAmountByProjectId($project[0]['id_crm_project']);
        if(!empty($loan)){ $project[0]['loan_amount'] = $loan; }
        $project[0]['no_of_loans'] = 0;
        $count = $this->Crm_model->getFacilitiesCount(array('crm_project_id' => $project[0]['id_crm_project']));
        if($count[0]['total']!='')
            $project[0]['no_of_loans']  = $count[0]['total'];
        $result = $this->Crm_model->getModuleDetails($module_id);
        $data = $active_forms = array(); $active_form_id = 0;
        $unique_section_names = array_values(array_unique(array_map(function ($i) { return $i['section_name']; }, $result)));
        $unique_id_sections = array_values(array_unique(array_map(function ($i) { return $i['id_section']; }, $result)));

        for($s=0;$s<count($unique_section_names);$s++)
        {
            $data[$s]['id_section'] = $unique_id_sections[$s];

            $data[$s]['section_name'] = $unique_section_names[$s];
            $form = array(); $st= $active_form_id = 0;
            for($r=0;$r<count($result);$r++){
                if($unique_section_names[$s]==$result[$r]['section_name']){
                    $form[$st]['id_form'] = $result[$r]['id_form'];
                    $form[$st]['form_name'] = $result[$r]['form_name'];
                    $form[$st]['form_template'] = $result[$r]['form_template'];
                    $form[$st]['form_class'] = $result[$r]['form_class'];
                    $st++;
                }
            }

            $data[$s]['form'] = $form;
        }
        //echo "<pre>"; print_r($data); exit;
        $t=0;
        //getting fields data


        for($s=0;$s<count($data);$s++){
            for($r=0;$r<count($data[$s]['form']);$r++){
                    //if($data[$s]['form'][$r]['form_name'] == 'Facility Information'){ break; }
                $form_fields = $this->Crm_model->getFormFieldsByFormId($data[$s]['form'][$r]['id_form']);
                $form_field_data = $this->Crm_model->getProjectFormData($project_id,$data[$s]['form'][$r]['id_form']);
                $field_data = array();

                for($sr=0;$sr<count($form_fields);$sr++)
                {
                    $field_data[$sr] = array(
                        'field_label' => $form_fields[$sr]['field_label'],
                        'field_name' => $form_fields[$sr]['field_name'],
                        'field_type' => $form_fields[$sr]['field_type'],
                        'field_value' => '---'
                    );

                    for($st=0;$st<count($form_field_data);$st++){

                        if($form_fields[$sr]['id_form_field']==$form_field_data[$st]['form_field_id']) {

                            if (trim($form_field_data[$st]['field_type']) == 'json') {

                                $value = json_decode($form_field_data[$st]['form_field_value']);
                                for ($sth = 0; $sth < count($value); $sth++) {
                                    if (empty($value[$sth])) {
                                        $value[$sth] = (object)$value[$sth];
                                    }
                                }
                                $form_field_data[$st]['form_field_value'] = $value;

                                $field_data[$sr] = array(
                                    'field_label' => $form_field_data[$st]['field_label'],
                                    'field_name' => $form_field_data[$st]['field_name'],
                                    'field_type' => $form_field_data[$st]['field_type'],
                                    'field_value' => $form_field_data[$st]['form_field_value']
                                );
                            }
                            else {
                                if ($form_field_data[$st]['field_name'] == 'project_main_sector_id' || $form_field_data[$st]['field_name'] == 'project_sub_sector_id') {
                                    $sector = $this->Master_model->getSector(array('id_sector' => $form_field_data[$st]['form_field_value']));
                                    if (!empty($sector))
                                        $form_field_data[$st]['form_field_value'] = $sector->sector_name;
                                }
                                $field_data[$sr] = array(
                                    'field_label' => $form_field_data[$st]['field_label'],
                                    'field_name' => $form_field_data[$st]['field_name'],
                                    'field_type' => $form_field_data[$st]['field_type'],
                                    'field_value' => $form_field_data[$st]['form_field_value']
                                );
                            }

                        }
                    }
                }


                $data[$s]['form'][$r]['field_data'] = $field_data;
                //getting form is filled or not
                /*$active_forms[$t]['id'] = $data[$s]['form'][$r]['id_form'];
                $active_forms[$t]['section_id'] = $data[$s]['id_section'];*/
                if($data[$s]['form'][$r]['form_name']=='Facility Information'){
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $project_id, 'crm_module_type' => 'project', 'reference_id' => 0, 'reference_type' => 'facility'));
                    if(empty($module_status)) $data[$s]['form'][$r]['status'] = 0;
                    else $data[$s]['form'][$r]['status'] = 1; $t++;
                    /*if(empty($module_status)) $active_forms[$t]['status'] = 0;
                    else $active_forms[$t]['status'] = 1; $t++;*/
                }
                else{
                    $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $project_id, 'crm_module_type' => 'project', 'reference_id' => $data[$s]['form'][$r]['id_form'], 'reference_type' => 'form'));
                    if(empty($module_status)) $data[$s]['form'][$r]['status'] = 0;
                    else $data[$s]['form'][$r]['status'] = 1; $t++;
                    /*if(empty($module_status)) $active_forms[$t]['status'] = 0;
                    else $active_forms[$t]['status'] = 1; $t++;*/
                }

            }
        }
        /*$percentage = $this->Crm_model->getFieldPercentage($project_id,'contact');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;*/

        $current_user_details = $this->Company_model->getCompanyUser($current_user);
        $reporting_user_id = $current_user_details->reporting_user_id;
        $reporting_user_name = $current_user_details->reporting_user_name;

        $project[0]['approval'] = 0;
        if($project[0]['committee_id']!=0)
        {
            $approval_limit = $this->Company_model->getApprovalLimit(array('company_approval_credit_committee_id' => $project[0]['committee_id'],'sector_id' => $project[0]['project_sub_sector_id'], 'product_id' => $project[0]['product_id']));
            //echo "<pre>"; print_r($approval_limit);
            if(!empty($approval_limit)){
                if($project[0]['amount']<=$approval_limit[0]['amount']){ $project[0]['approval']=1; }
            }
            else
            {
                $approval_limit = $this->Company_model->getApprovalLimit(array('company_approval_credit_committee_id' => $project[0]['committee_id'],'sector_id' => $project[0]['project_main_sector_id'], 'product_id' => $project[0]['product_id']));
                //echo "<pre>"; print_r($approval_limit);
                if(!empty($approval_limit)){
                    if($project[0]['amount']<=$approval_limit[0]['amount']){ $project[0]['approval']=1; }
                }
                else
                {
                    $approval_limit = $this->Company_model->getApprovalLimit(array('company_approval_credit_committee_id' => $project[0]['committee_id'],'sector_id' => 0, 'product_id' => $project[0]['product_id']));
                    //echo "<pre>"; print_r($approval_limit);
                    if(!empty($approval_limit)){
                        if($project[0]['amount']<=$approval_limit[0]['amount']){ $project[0]['approval']=1; }
                    }
                }
            }

        }

        $rating = $this->getCrmProjectRiskRating($project_id,$project[0]['company_id']);
        $rating = explode('@@@',$rating);
        if($rating[1]){ $project[0]['rating'] = (float)$rating[0]; }
        else{ $project[0]['rating'] = '---'; }
        $project[0]['grade'] = $this->projectRiskGradeCalculation($project[0]['rating']);



        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('details' => $project, 'forms' => $data, 'percentage' => '','active_forms' => $active_forms,'reporting' =>array('reporting_user_id' => $reporting_user_id,'reporting_user_name' => $reporting_user_name)));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectList_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $current_user_id = '';
        if(isset($_SERVER['HTTP_USER'])){
            $current_user_id = $_SERVER['HTTP_USER'];
        }

        $result = $this->Crm_model->getProjectList($data);
        $user_access = '';
        for($s=0;$s<count($result);$s++){
            /*$percentage = $this->Crm_model->getFieldPercentage($result[$s]['id_crm_project'],'project');
            $percentage = explode(',',$percentage);
            $result[$s]['percentage'] = ceil(($percentage[1]/$percentage[0])*100);*/

            if($current_user_id==$result[$s]['created_by']){
                $user_access = 'edit';
            }
            else
            {
                $user_in_project = $this->Activity_model->getProjectApproval(array('project_id' => $result[$s]['id_crm_project'], 'user_id' => $current_user_id));
                if(!empty($user_in_project)){
                    $user_access = 'status_flow';
                }
                else{
                    $user_access = 'overview';
                }
            }
            $result[$s]['user_access'] = $user_access;

            $result[$s]['loan_amount'] = $this->Crm_model->getFacilityLoanAmountByProjectId($result[$s]['id_crm_project']);

        }
        if(isset($data['offset'])){ unset($data['offset']); }
        if(isset($data['limit'])){ unset($data['limit']); }
        $total_records = count($this->Crm_model->getProjectList($data));
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectForm_get($project_id,$form_id,$type)
    {
        $form_fields = $this->Crm_model->getFormFieldsByFormId($form_id);
        $form_data = $this->Crm_model->getProjectFormData($project_id,$form_id);
        $project_details = $this->Crm_model->getProject($project_id);
        $created_date = '';

        $result = array(); $st=0;
        $result['form_name'] = $form_fields[0]['form_name'];
        $form_field_data = $this->Crm_model->getProjectFormData($project_id,$form_id);
        $field_data = array();
        for($st=0;$st<count($form_field_data);$st++){
            if($type=='view')
            {
                if(trim($form_field_data[$st]['field_type'])=='json'){

                    $value = json_decode($form_field_data[$st]['form_field_value']);
                    for($sth=0;$sth<count($value);$sth++)
                    {
                        if(empty($value[$sth])){
                            $value[$sth] = (object)$value[$sth];
                        }
                    }
                    $form_field_data[$st]['form_field_value'] = $value;

                    $field_data[] = array(
                        'field_label' => $form_field_data[$st]['field_label'],
                        'field_name' => $form_field_data[$st]['field_name'],
                        'field_type' => $form_field_data[$st]['field_type'],
                        'field_value' => $form_field_data[$st]['form_field_value']
                    );
                }
                else{
                    if($form_field_data[$st]['field_name']=='project_main_sector_id' || $form_field_data[$st]['field_name']=='project_sub_sector_id'){
                        $sector = $this->Master_model->getSector(array('id_sector' => $form_field_data[$st]['form_field_value']));
                        if(!empty($sector))
                            $form_field_data[$st]['form_field_value'] = $sector->sector_name;
                    }
                    $field_data[] = array(
                        'field_label' => $form_field_data[$st]['field_label'],
                        'field_name' => $form_field_data[$st]['field_name'],
                        'field_type' => $form_field_data[$st]['field_type'],
                        'field_value' => $form_field_data[$st]['form_field_value']
                    );
                }
            }
            else
            {

                if(trim($form_field_data[$st]['field_type'])=='json'){

                    $value = json_decode($form_field_data[$st]['form_field_value']);
                    for($sth=0;$sth<count($value);$sth++)
                    {
                        if(empty($value[$sth])){
                            $value[$sth] = (object)$value[$sth];
                        }
                    }
                    $form_field_data[$st]['form_field_value'] = $value;

                    $result[$form_field_data[$st]['field_name']] = $form_field_data[$st]['form_field_value'];
                }
                else{
                    $value = '';
                    if(isset($form_field_data[$st]['form_field_value'])){ $value = $form_field_data[$st]['form_field_value']; }
                    $result[$form_field_data[$st]['field_name']] = $value;
                }
            }
        }

        if($type=='view') $result['form_data'] = $field_data;
        /*for($s=0;$s<count($form_fields);$s++){
            for($r=0;$r<count($form_data);$r++){
                if($form_fields[$s]['id_form_field']==$form_data[$r]['form_field_id'])
                {
                    if($type=='view')
                    {
                        if(trim($form_fields[$s]['field_type'])=='json'){
                            $result['form_data'][$st] = array(
                                'field_label' => $form_data[$r]['field_label'],
                                'field_name' => $form_data[$r]['field_name'],
                                'field_type' => $form_data[$r]['field_type'],
                                'field_value' => json_decode($form_data[$r]['form_field_value'])
                            );
                            $st++;
                        }
                        else{
                            if($form_data[$r]['field_name']=='project_main_sector_id' || $form_data[$r]['field_name']=='project_sub_sector_id'){
                                $sector = $this->Master_model->getSector(array('id_sector' => $form_data[$r]['form_field_value']));
                                if(!empty($sector))
                                    $form_data[$r]['form_field_value'] = $sector->sector_name;
                            }
                            $result['form_data'][$st] = array(
                                'field_label' => $form_data[$r]['field_label'],
                                'field_name' => $form_data[$r]['field_name'],
                                'field_type' => $form_data[$r]['field_type'],
                                'field_value' => $form_data[$r]['form_field_value']
                            );
                            $st++;
                        }
                    }
                    else
                    {
                        if(trim($form_fields[$s]['field_type'])=='json'){
                            $result[$form_fields[$s]['field_name']] = json_decode($form_data[$r]['form_field_value']); $st++;
                        }
                        else{
                            $result[$form_fields[$s]['field_name']] = $form_data[$r]['form_field_value']; $st++;
                        }
                    }
                }
            }
        }*/
        $percentage = $this->Crm_model->getFieldPercentage($project_id,'project');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' => $result,'percentage' => ceil($percentage), 'project_details' => $project_details[0]));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function project_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        //echo "<pre>"; print_r($data); exit;
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $update_type = '0';

        if(isset($data['project_cost'])){ $data['project_cost'] = json_encode($data['project_cost']); $update_type=0; }
        if(isset($data['loan_information'])){ $data['loan_information'] = json_encode($data['loan_information']); $update_type=0; }
        if(isset($data['facility_details'])){ $data['facility_details'] = json_encode($data['facility_details']); $update_type=1; }
        if(isset($data['risk_assessment'])){ $data['risk_assessment'] = json_encode($data['risk_assessment']); $update_type=0; }
        if(isset($data['recommended_terms'])){ $data['recommended_terms'] = json_encode($data['recommended_terms']); $update_type=0; }

        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $data_keys = array_keys($data);
        $form_data = array(); $st=0; $type = 'insert';
        $form_fields = $this->Crm_model->getFormFieldsByFormId($data['form_id']);

        if(isset($data['crm_project_id'])){
            $check_data = $this->Crm_model->getProjectFormData($data['crm_project_id'],$data['form_id']);
            //echo "<pre>"; print_r($check_data); exit;
            if(empty($check_data)){ $type = 'insert'; }
            else{ $type = 'update'; }
        }

        for($s=0;$s<count($form_fields);$s++){ $check_field = 0;
            for($r=0;$r<count($data_keys);$r++){
                if($form_fields[$s]['field_name']==$data_keys[$r]){
                    $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                    $form_data[$st]['form_field_value'] = $data[$data_keys[$r]];
                    $st++;
                    $check_field = 1;
                }
            }
            if($check_field==0){
                $form_data[$st]['form_field_id'] = $form_fields[$s]['id_form_field'];
                $form_data[$st]['form_field_value'] = '';
                $st++;
            }
        }
        if(empty($form_data)){
            $result = array('status'=>FALSE, 'error' => 'invalid fields', 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($type=='insert')
        {
            if(!isset($data['crm_project_id'])){
                $project_data = array('company_id' => $data['company_id'], 'created_by' => $data['user_id']);
                if(isset($data['project_title']) && $data['project_title']!=''){
                    $project_data['project_title'] = $data['project_title'];
                }
                if(isset($data['project_main_sector_id'])){
                    $project_data['project_main_sector_id'] = $data['project_main_sector_id'];
                }
                if(isset($data['project_sub_sector_id'])){
                    $project_data['project_sub_sector_id'] = $data['project_sub_sector_id'];
                }
                if(isset($data['project_description'])){
                    $project_data['project_description'] = $data['project_description'];
                }
                $crm_project_id = $this->Crm_model->addCrmProject($project_data);
            }
            else{
                $crm_project_id = $data['crm_project_id'];
                $project_data = array();
                if(isset($data['project_title']) && $data['project_title']!=''){
                    $project_data['project_title'] = $data['project_title'];
                }
                if(isset($data['project_main_sector_id'])){
                    $project_data['project_main_sector_id'] = $data['project_main_sector_id'];
                }
                if(isset($data['project_sub_sector_id'])){
                    $project_data['project_sub_sector_id'] = $data['project_sub_sector_id'];
                }
                if(isset($data['project_description'])){
                    $project_data['project_description'] = $data['project_description'];
                }
            }
            for($r=0;$r<count($form_data);$r++){
                $form_data[$r]['crm_project_id'] = $crm_project_id;
            }

            $this->Crm_model->addCrmProjectData($form_data);

            if(!empty($project_data)){
                $this->Crm_model->updateCrmProject($project_data,$crm_project_id);
            }
        }
        if($type=='update')
        {
            if(!empty($check_data)){
                for($s=0;$s<count($check_data);$s++){
                    for($r=0;$r<count($form_data);$r++){
                        if($check_data[$s]['form_field_id']==$form_data[$r]['form_field_id']){
                            $form_data[$r]['id_crm_project_data'] = $check_data[$s]['id_crm_project_data'];
                        }
                    }
                }
            }
            $this->Crm_model->updateCrmProjectData($form_data,$update_type);
            $crm_project_id = $data['crm_project_id'];
            $project_data = array();
            if(isset($data['project_title']) && $data['project_title']!=''){
                $project_data['project_title'] = $data['project_title'];
            }
            if(isset($data['project_main_sector_id'])){
                $project_data['project_main_sector_id'] = $data['project_main_sector_id'];
            }
            if(isset($data['project_sub_sector_id'])){
                $project_data['project_sub_sector_id'] = $data['project_sub_sector_id'];
            }
            if(isset($data['project_description'])){
                $project_data['project_description'] = $data['project_description'];
            }
        }
        if(!empty($project_data)){
            $this->Crm_model->updateCrmProject($project_data,$crm_project_id);
        }

        $this->Crm_model->updateCrmProject(array('updated_date_time' => date('Y-m-d H:i:s')),$crm_project_id);

        $percentage = $this->Crm_model->getFieldPercentage($crm_project_id,'project');
        $percentage = explode(',',$percentage);
        $percentage = ($percentage[1]/$percentage[0])*100;

        //adding status for each form fill or not
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $data['form_id'], 'reference_type' => 'form'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $data['form_id'], 'reference_type' => 'form', 'created_by' => $data['user_id'], 'updated_date_time' => date('Y-m-d H:i:s')));

        //adding activity
        $form_info = $this->Crm_model->getFormInformation($data['form_id']);
        $this->Activity_model->addActivity(array('activity_name' => $form_info[0]['form_name'],'activity_template' => 'information has been updated','module_type' => 'project','module_id' =>  $data['crm_project_id'], 'activity_type' => 'form','activity_reference_id' => $data['form_id'],'created_by' => $data['user_id']));

        $result = array('status'=>TRUE, 'message' => 'Information updated successfully.', 'data'=>array('crm_project_id' => $crm_project_id,'percentage' => ceil($percentage)));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function crmListForMapping_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_contact_id',array('required'=> 'Contact id or company id required'));
        $this->form_validator->add_rules('search_key',array('required'=> 'Search_key required'));
        $validated = $this->form_validator->validate($data);
        $this->form_validator->clearRules();
        $this->form_validator->add_rules('crm_company_id',array('required'=> 'Contact id or company id required'));
        $validated1 = $this->form_validator->validate($data);

        if($validated != 1 && $validated1 != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getCrmListForMapping($data);
        //echo "<pre>"; print_r($result); exit;
        if(isset($data['crm_company_id']))
        {
            for($s=0;$s<count($result);$s++)
            {
                $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
            }
        }

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyDesignation_get()
    {
        $result = $this->Crm_model->getCompanyDesignation();
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function mapModule_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id', array('required'=> 'Company id required'));
        $this->form_validator->add_rules('crm_contact_id', array('required'=> 'Contact id required'));
        $this->form_validator->add_rules('crm_company_designation_id', array('required'=> 'Company designation required'));
        $this->form_validator->add_rules('created_by', array('required'=> 'User id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $type = $data['type'];
        unset($data['type']);
        if(isset($data['is_primary_company'])){
            $update = array('is_primary_company' => '');
            $this->Crm_model->updateCompanyContactPrimary($data['crm_contact_id'],$update);
        }

        $check_exists = $this->Crm_model->checkCompanyContactExists($data);
        if(!empty($check_exists)){
            $update = array('crm_company_contact_status' => '1');
            if(isset($data['is_primary_company'])){ $update['is_primary_company'] = 1; }
            $this->Crm_model->updateCompanyContact($check_exists->id_crm_company_contact,$update);
            $update = array('crm_company_designation_id' => $data['crm_company_designation_id']);
            $this->Crm_model->updateCompanyContact($check_exists->id_crm_company_contact,$update);
            $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$check_exists->id_crm_company_contact);
            $this->response($result, REST_Controller::HTTP_OK); exit;
        }

        $map_id = $this->Crm_model->mapModule($data);
        $activity_template = '';
        if($type=='company'){ $suc_msg = 'Contact added to company successfully.'; $activity_template='Contact added to company'; $activity_type = 'contact_add_to_company'; }
        else if($type=='contact'){ $suc_msg = 'Company added to contact successfully.'; $activity_template='Company added to contact'; $activity_type = 'company_add_to_contact'; }

        //adding activity
        $this->Activity_model->addActivity(array('activity_name' => 'Contact','activity_template' => $activity_template,'module_type' => 'contact','module_id' => $data['crm_contact_id'], 'activity_type' => $activity_type,'activity_reference_id' => $map_id,'created_by' => $data['created_by']));

        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>$map_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyContact_delete($crm_company_contact_id)
    {
        $this->Crm_model->deleteCompanyContact($crm_company_contact_id);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function mapModule_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_company_id',array('required'=> 'Company or contact id required'));
        $validated = $this->form_validator->validate($data);
        $this->form_validator->clearRules();
        $this->form_validator->add_rules('crm_contact_id',array('required'=> 'Company or contact id required'));
        $validated1 = $this->form_validator->validate($data);

        if($validated != 1 && $validated1 != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getMapModule($data);
        for($s=0;$s<count($result);$s++){
            if(isset($data['crm_company_id']))
                $percentage = $this->Crm_model->getFieldPercentage($result[$s]['id_crm_contact'],'contact');
            else if(isset($data['crm_contact_id']))
                $percentage = $this->Crm_model->getFieldPercentage($result[$s]['id_crm_company'],'company');
            $percentage = explode(',',$percentage);
            $result[$s]['percentage'] = ceil(($percentage[1]/$percentage[0])*100);
        }
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function contactType_get()
    {
        $result = $this->Crm_model->getContactType();
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectContact_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id',array('required'=> 'Project id required'));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getProjectContact($data);
        for($s=0;$s<count($result);$s++){
            $percentage = $this->Crm_model->getFieldPercentage($result[$s]['id_crm_contact'],'contact');
            $percentage = explode(',',$percentage);
            $result[$s]['percentage'] = ceil(($percentage[1]/$percentage[0])*100);
        }
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectContact_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id', array('required'=> 'Project id required'));
        $this->form_validator->add_rules('crm_contact_id', array('required'=> 'Contact id required'));
        $this->form_validator->add_rules('created_by', array('required'=> 'User id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['is_primary_contact'])){
            $update = array('is_primary_contact' => '');
            $this->Crm_model->updateProjectContactPrimary($data['crm_project_id'],$update);
        }

        $check_exists = $this->Crm_model->checkProjectContactExists($data);
        if(!empty($check_exists)){
            $update = array('project_contact_status' => '1');
            if(isset($data['is_primary_contact'])){ $update['is_primary_contact'] = 1; }
            $this->Crm_model->updateProjectContact($check_exists->id_project_contact,$update);
            $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$check_exists->id_project_contact);
            $this->response($result, REST_Controller::HTTP_OK); exit;
        }

        $map_id = $this->Crm_model->addProjectContact($data);

        //adding activity
        $this->Activity_model->addActivity(array('activity_name' => 'Project','activity_template' => 'Contact added to project.','module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'contact_add_to_project','activity_reference_id' => $map_id,'created_by' => $data['created_by']));

        $result = array('status'=>TRUE, 'message' => 'Contact added to project successfully', 'data'=>$map_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectContact_delete($crm_project_contact_id)
    {
        $this->Crm_model->deleteProjectContact($crm_project_contact_id);
        $result = array('status'=>TRUE, 'message' => 'Contact deleted successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function companyType_get()
    {
        $result = $this->Crm_model->getCompanyType();
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectCompany_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id',array('required'=> 'Project id required'));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getProjectCompany($data);
        for($s=0;$s<count($result);$s++){
            $percentage = $this->Crm_model->getFieldPercentage($result[$s]['id_crm_company'],'company');
            $percentage = explode(',',$percentage);
            $result[$s]['percentage'] = ceil(($percentage[1]/$percentage[0])*100);
        }
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectCompany_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id', array('required'=> 'Project id required'));
        $this->form_validator->add_rules('crm_company_id', array('required'=> 'Company id required'));
        $this->form_validator->add_rules('company_type_id', array('required'=> 'Company type id required'));
        $this->form_validator->add_rules('created_by', array('required'=> 'User id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $check_exists = $this->Crm_model->checkProjectCompanyExists($data);
        if(!empty($check_exists)){
            $update = array('project_company_status' => '1');
            $this->Crm_model->updateProjectCompany($check_exists->id_project_company,$update);
            $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$check_exists->id_project_company);
            $this->response($result, REST_Controller::HTTP_OK); exit;
        }

        $map_id = $this->Crm_model->addProjectCompany($data);

        //adding activity
        $this->Activity_model->addActivity(array('activity_name' => 'Project','activity_template' => 'Company added to project.','module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'company_add_to_project','activity_reference_id' => $map_id,'created_by' => $data['created_by']));

        $result = array('status'=>TRUE, 'message' => 'Company added to project successfully', 'data'=>$map_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectCompany_delete($crm_project_company_id)
    {
        $this->Crm_model->deleteProjectCompany($crm_project_company_id);
        $result = array('status'=>TRUE, 'message' => 'Company deleted successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function touchPointType_get()
    {
        $result = $this->Crm_model->getTouchPointType();
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function touchPoints_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $results = array();
        $result = $this->Crm_model->getTouchPoints($data);
        $reminder_count = count($this->Crm_model->getTouchPointReminder($data));
        //echo "<pre>"; print_r($result); exit;
        //$reminder = $this->Crm_model->getTouchPointReminder($data);
        /*$data = $touch_point_type_id_array = $touch_point_type_name_array = $touch_point_description_array = $reminder_date_array = $reminder_time_array = array();*/

        $res = array();
        for($s=0;$s<count($result);$s++)
        {
            $touch_point_type_id = explode('@@@',$result[$s]['touch_point_type_id']);
            $touch_point_type_name = explode('@@@',$result[$s]['touch_point_type_name']);
            $touch_point_description = explode('@@@',$result[$s]['touch_point_description']);
            $reminder_date = explode('@@@',$result[$s]['reminder_date']);
            $reminder_time = explode('@@@',$result[$s]['reminder_time']);
            $reminder = explode('@@@',$result[$s]['reminder']);
            $user_name = explode('@@@',$result[$s]['user_name']);
            $created_date = explode('@@@',$result[$s]['created_date']);

            for($r=0;$r<count($touch_point_type_id);$r++)
            {
                /*$touch_point_type_id_array[] = $touch_point_type_id[$r];
                $touch_point_type_name_array[] = $touch_point_type_name[$r];
                $touch_point_description_array[] = $touch_point_description[$r];
                $reminder_date_array[] = $reminder_date[$r];
                $reminder_time_array[] = $reminder_time[$r];*/
                $reminder_date_new = $reminder_new = $user_name_new = $reminder_time_new = $created_date_new = '';
                if(isset($reminder_date[$r])){ $reminder_date_new = $reminder_date[$r]; }
                if(isset($reminder_time[$r])){ $reminder_time_new = $reminder_time[$r]; }
                if(isset($reminder[$r])){ $reminder_new = $reminder[$r]; }
                if(isset($user_name[$r])){ $user_name_new = $user_name[$r]; }
                if(isset($created_date[$r])){ $created_date_new = $created_date[$r]; }
                $res[$s][] = array(
                            'touch_point_type_id' => $touch_point_type_id[$r],
                            'touch_point_type_name' => $touch_point_type_name[$r],
                            'touch_point_description' => $touch_point_description[$r],
                            'reminder_date' => $reminder_date_new,
                            'reminder_time' => $reminder_time_new,
                            'reminder' => $reminder_new,
                            'user_name' => $user_name_new,
                            'created_date' => $created_date_new
                        );
            }

            $results[] = array(
                            'touch_point' => $res[$s],
                            'date' => $result[$s]['date']
                        );
        }
        $total_records = count($this->Crm_model->getTotalTouchPoints($data));
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$results,'total_records' => $total_records,'reminder' => $reminder_count));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function touchPointReminder_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getTouchPointReminder($data);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function touchPoints_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_module_id', array('required'=> 'Module id required'));
        $this->form_validator->add_rules('crm_touch_point_type_id', array('required'=> 'Touch point type required'));
        $this->form_validator->add_rules('crm_module_type', array('required'=> 'Module type required'));
        $this->form_validator->add_rules('touch_point_description', array('required'=> 'Description required'));
        $this->form_validator->add_rules('created_by', array('required'=> 'User id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(isset($data['reminder'])){
             $data['is_reminder'] = $data['reminder'];
             unset($data['reminder']);
        }

        $data['touch_point_from_id'] = $data['crm_module_id'];
        if($data['crm_module_type']=='contact'){ $data['crm_module_id'] = 1; }
        else if($data['crm_module_type']=='company'){ $data['crm_module_id'] = 2; }
        else if($data['crm_module_type']=='project'){ $data['crm_module_id'] = 3; }
        $module_type = $data['crm_module_type'];
        unset($data['crm_module_type']);
        $result = $touch_point_id = $this->Crm_model->addTouchPoint($data);
        //adding activity

        /*if($data['crm_module_id']==1){ $module_type = 'contact'; }
        else if($data['crm_module_id']==2){ $module_type = 'company'; }
        else if($data['crm_module_id']==3){ $module_type = 'project'; }*/

        $this->Activity_model->addActivity(array('activity_name' => 'Touch Point','activity_template' => $data['touch_point_description'],'module_type' => $module_type,'module_id' => $data['touch_point_from_id'], 'activity_type' => 'touch_point','activity_reference_id' => $touch_point_id,'created_by' => $data['created_by']));
        $result = array('status'=>TRUE, 'message' => 'Touch point added successfully.', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function product_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $product = $this->Crm_model->getProduct($data);
        unset($data['offset']); unset($data['limit']);
        $total_records = count($this->Crm_model->getProduct($data));

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' => $product,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function product_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $this->form_validator->add_rules('product_name', array('required'=> 'Product name required'));
        $this->form_validator->add_rules('company_approval_structure_id', array('required'=> 'Approval structure required'));
        $this->form_validator->add_rules('created_by', array('required'=> 'User id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['id_product']))
        {
            $product = array(
                'company_id' => $data['company_id'],
                'product_name' => $data['product_name'],
                'company_approval_structure_id' => $data['company_approval_structure_id'],
                'created_by' => $data['created_by']
            );
            $product_id = $this->Crm_model->addProduct($product);
            $suc_msg =  'Product added successfully.';
        }
        else
        {
            $product = array(
                'company_id' => $data['company_id'],
                'product_name' => $data['product_name'],
                'company_approval_structure_id' => $data['company_approval_structure_id'],
                'created_by' => $data['created_by'],
                'id_product' => $data['id_product']
            );
            $product_id = $data['id_product'];
            $this->Crm_model->updateProduct($product);
            $suc_msg =  'Product updated successfully.';
        }


        $result = array('status'=>TRUE, 'message' =>$suc_msg, 'data'=>$product_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectTeam_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id',array('required'=> 'Project id required'));
        $validated = $this->form_validator->validate($data);

        if($validated != 1){
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getProjectTeam($data);
        for($s=0;$s<count($result);$s++){
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
        }
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectTeam_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id', array('required'=> 'Project id required'));
        $this->form_validator->add_rules('team_member_id', array('required'=> 'Team member id required'));
        $this->form_validator->add_rules('created_by', array('required'=> 'User id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $check_exists = $this->Crm_model->checkProjectTeamExists($data);
        if(!empty($check_exists)){
            $update = array('team_member_status' => '1');
            $this->Crm_model->updateProjectTeam($check_exists->id_project_team,$update);
            $result = array('status'=>TRUE, 'message' => 'Team added successfully.', 'data'=>$check_exists->id_project_team);
            $this->response($result, REST_Controller::HTTP_OK); exit;
        }

        $map_id = $this->Crm_model->addProjectTeam($data);
        $result = array('status'=>TRUE, 'message' => 'Team added successfully.', 'data'=>$map_id);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectTeam_delete($project_team_id)
    {
        $this->Crm_model->deleteProjectTeam($project_team_id);
        $result = array('status'=>TRUE, 'message' => 'Team deleted successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function intelligence_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('type', array('required'=> 'type required'));
        $this->form_validator->add_rules('sector_id', array('required'=> 'Sector id required'));
        $this->form_validator->add_rules('sub_sector_id', array('required'=> 'Sub sector id required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else if($data['type']!='crm_company' && $data['type']!='crm_project')
        {
            $result = array('status'=>FALSE,'error'=>array('type' => 'Invalid type'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->getIntelligence($data);
        $list = $user_list = $users = array();
        for($s=0;$s<count($result);$s++)
        {
            if($data['type']=='crm_company'){
                $list[] = array(
                    'id_crm_company' => $result[$s]['id_crm_company'],
                    'company_id' => $result[$s]['company_id'],
                    'company_name' => $result[$s]['company_name'],
                    'company_description' => $result[$s]['company_description']
                );
            }
            else if($data['type']=='crm_project')
            {
                $list[] = array(
                    'id_crm_project' => $result[$s]['id_crm_project'],
                    'company_id' => $result[$s]['company_id'],
                    'project_title' => $result[$s]['project_title'],
                    'project_description' => $result[$s]['project_description']
                );
            }

            if(!in_array($result[$s]['id_user'],$users)){
                array_push($users,$result[$s]['id_user']);
                $user_list[] = array(
                    'username' => $result[$s]['username'],
                    'profile_image' => getImageUrl($result[$s]['profile_image'],'profile'),
                    'approval_name' => $result[$s]['approval_name']
                );
            }
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
        }

        $result = array('status'=>TRUE, 'message' => '', 'data'=>array('list' => $list, 'user_list' => $user_list));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledge_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('type', array('required'=> 'type required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else if($data['type']!='crm_company' && $data['type']!='crm_project')
        {
            $result = array('status'=>FALSE,'error'=>array('type' => 'Invalid type'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['tags']))
            $tag = $data['tags'];
        else $tag = 0;

        $data['tags'] = array();

        if($tag)
            $data['tags'][] = $tag;

        if($data['type']=='crm_company'){
            $this->form_validator->add_rules('crm_company_id', array('required'=> 'Crm company id required'));
            $validated = $this->form_validator->validate($data);
            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            else{
                $crm_details = $this->Crm_model->getCompany($data['crm_company_id']);
                $data['tags'][] =  $crm_details[0]['sector'];
                $data['tags'][] =  $crm_details[0]['sub_sector'];
            }
        }
        else if($data['type']=='crm_project'){
            $this->form_validator->add_rules('crm_project_id', array('required'=> 'Crm project id required'));
            $validated = $this->form_validator->validate($data);
            if($validated != 1)
            {
                $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
            else{
                $crm_details = $this->Crm_model->getProject($data['crm_project_id']);
                $data['tags'][] =  $crm_details[0]['sector'];
                $data['tags'][] =  $crm_details[0]['sub_sector'];
            }
        }
        $tags = '(';
        for($s=0;$s<count($data['tags']);$s++)
        {   if($s>0){ $tags .=','; }
            $tags .='"'.$data['tags'][$s].'"';
        }
        $tags .= ')';
        $data['tags'] = $tags;
        $result = $this->Crm_model->getKnowledgeDocuments(array('search_key' => $data['tags'], 'company_id' => $data['company_id'], 'knowledge_document_status' => 1));
        for($s=0;$s<count($result);$s++)
        {
            if($result[$s]['document_type']=='document'){
                $result[$s]['document_source'] = getImageUrl($result[$s]['document_source'],'file');
            }
        }
        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledgeCount_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('id_knowledge_document', array('required'=> 'Knowledge document id required'));
        //$this->form_validator->add_rules('user_id', array('required'=> 'user id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->addKnowledgeDocumentViewCount($data);
        //$this->Crm_model->adddKnowledgeDocumentView(array('knowledge_document_id',$data['id_knowledge_document'], 'viewed_by' => $data['user_id']));

        $result = array('status'=>TRUE, 'message' => '', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    /*public function knowledgeComments_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('knowledge_document_id', array('required'=> 'Knowledge document id required'));
        $this->form_validator->add_rules('user_id', array('required'=> 'Commented by required'));
        $this->form_validator->add_rules('comment', array('required'=> 'Comment required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Crm_model->addKnowledgeDocumentViewCount($data);
        //$this->Crm_model->adddKnowledgeDocumentView(array('knowledge_document_id',$data['id_knowledge_document'], 'viewed_by' => $data['user_id']));

        $result = array('status'=>TRUE, 'message' => '', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }*/

    public function projectTerms_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id', array('required'=> 'crm project id required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_details = $this->Crm_model->getProject($data['crm_project_id']);
        $result = $this->Company_model->getProductTerms($data);

        for($sr=0;$sr<count($result);$sr++)
        {
            if($result[$sr]['product_term_type']=='item' || $result[$sr]['product_term_type']=='item')
            {
                $result[$sr]['data'] = $this->Company_model->getProductTermItems(array('product_term_id' => $result[$sr]['id_product_term']));
				
                for($s=0;$s<count($result[$sr]['data']);$s++)
                {
                    if(trim(strtolower($result[$sr]['data'][$s]['product_term_item_name']))=='facility information')
                    {
                        $form = $this->Crm_model->getFormFieldValueByFieldName(array('form_name' => trim($result[$sr]['data'][$s]['product_term_item_name']), 'crm_project_id' => $data['crm_project_id']));
                        if(!empty($form)) $result[$sr]['data'][$s]['answer'] = json_decode($form[0]['form_field_value']);
                        else  $result[$sr]['data'][$s]['answer'] = '';
						
                    }
                    else
                    {
                        $answer = $this->Crm_model->getProductTermItemData(array('crm_project_id' => $data['crm_project_id'], 'product_term_item_id' => $result[$sr]['data'][$s]['id_product_term_item']));
                        if(empty($answer)){ $result[$sr]['data'][$s]['answer'] = ''; }
                        else { $result[$sr]['data'][$s]['answer'] = $answer[0]['item_data']; }
                    }
					
					if(trim($result[$sr]['data'][$s]['product_term_item_name'])=="Expiry"){
						if(isset($result[$sr]['data'][$s]['answer']) && !empty($result[$sr]['data'][$s]['answer'])){
							$result[$sr]['data'][$s]['answer']=date("Y-m-d",strtotime($result[$sr]['data'][$s]['answer']));
						}else{
							$result[$sr]['data'][$s]['answer']="";
						}
					}
					
					
                }
				
            }
            else if($result[$sr]['product_term_type']==''){
                $result[$sr]['data'] = array();
            }
            else if($result[$sr]['product_term_type']=='question')
            {
                $result[$sr]['data'] = $this->Company_model->getAssessmentQuestionType(array('assessment_question_type_key' => 'pre_disbursement_checklist'));
                for($s=0;$s<count($result[$sr]['data']);$s++)
                {
                    $result[$sr]['data'][$s]['category'] = $this->Company_model->getAssessmentQuestionCategory(array('assessment_question_type_id' => $result[$sr]['data'][$s]['id_assessment_question_type'], 'company_id' => $data['company_id'], 'assessment_question_category_status' => 1, 'sector_id' => $project_details[0]['project_main_sector_id'], 'sub_sector_id' => $project_details[0]['project_sub_sector_id'], 'product_id' => $project_details[0]['product_id'] ));
                    for($r=0;$r<count($result[$sr]['data'][$s]['category']);$r++)
                    {    
                        $result[$sr]['data'][$s]['category'][$r]['question'] = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $result[$sr]['data'][$s]['category'][$r]['id_assessment_question_category'], 'question_status' => 1));
                        for($sru=0;$sru<count($result[$sr]['data'][$s]['category'][$r]['question']);$sru++)
                        {
                            if(isset($result[$sr]['data'][$s]['category'][$r]['question'][$sru]['question_option']) && $result[$sr]['data'][$s]['category'][$r]['question'][$sru]['question_option']!=''){
                                $result[$sr]['data'][$s]['category'][$r]['question'][$sru]['question_option'] = json_decode($result[$sr]['data'][$s]['category'][$r]['question'][$sru]['question_option']);
                            }
                            $result[$sr]['data'][$s]['category'][$r]['question'][$sru]['answer'] = $this->Company_model->getAssessmentQuestionAnswer(array('assessment_question_id' => $result[$sr]['data'][$s]['category'][$r]['question'][$sru]['id_assessment_question'], 'crm_project_id' => $data['crm_project_id']));

                            if(isset($result[$sr]['data'][$s]['category'][$r]['question'][$sru]['answer']->assessment_answer) && $result[$sr]['data'][$s]['category'][$r]['question'][$sru]['answer']->assessment_answer!=''){
                                if($result[$sr]['data'][$s]['category'][$r]['question'][$sru]['question_type']=='checkbox')
                                    $result[$sr]['data'][$s]['category'][$r]['question'][$sru]['answer']->assessment_answer = json_decode($result[$sr]['data'][$s]['category'][$r]['question'][$sru]['answer']->assessment_answer);
                                else if($result[$sr]['data'][$s]['category'][$r]['question'][$sru]['question_type']=='file')
                                    $result[$sr]['data'][$s]['category'][$r]['question'][$sru]['answer']->assessment_answer = getImageUrl($result[$sr]['data'][$s]['category'][$r]['question'][$sru]['answer']->assessment_answer,'file');
                            }
                        }
                    }
                }
            }
            else if($result[$sr]['product_term_type']=='risk')
            {
                $result[$sr]['data'] = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => 0));
                for($s=0;$s<count($result[$sr]['data']);$s++)
                {
                    $result[$sr]['data'][$s]['sub_category'] = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => $result[$sr]['data'][$s]['id_risk_category']));
                    $sub_category = array();
                    for($r=0;$r<count($result[$sr]['data'][$s]['sub_category']);$r++)
                    {
                        $result[$sr]['data'][$s]['sub_category'][$r]['attributes'] = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => $result[$sr]['data'][$s]['sub_category'][$r]['id_risk_category']));
                        $attributes = array();
                        for($u=0;$u<count($result[$sr]['data'][$s]['sub_category'][$r]['attributes']); $u++)
                        {
                            $crm_project_details = $this->Crm_model->getProject($data['crm_project_id']);
                            $result[$sr]['data'][$s]['sub_category'][$r]['attributes'][$u]['items'] = $this->Crm_model->getRiskCategoryItemsWithProjectItems(array('crm_project_id'=>$data['crm_project_id'],'risk_category_id' => $result[$sr]['data'][$s]['sub_category'][$r]['attributes'][$u]['id_risk_category'], 'sector_id' => $crm_project_details[0]['project_main_sector_id'],'sub_sector_id' => $crm_project_details[0]['project_sub_sector_id']));


                            $result[$sr]['data'][$s]['sub_category'][$r]['attributes'][$u]['project_grade'] = $this->Crm_model->getCrmProjectRiskCategoryAttributesItemsGrade(array('crm_project_id'=>$data['crm_project_id'],'risk_category_id' => $result[$sr]['data'][$s]['sub_category'][$r]['attributes'][$u]['id_risk_category'], 'sector_id' => $crm_project_details[0]['project_main_sector_id'],'sub_sector_id' => $crm_project_details[0]['project_sub_sector_id']));
                            //echo "<pre>"; print_r($result[$s]['sub_category'][$r]['attributes'][$u]); exit;
                            if(isset($result[$sr]['data'][$s]['sub_category'][$r]['attributes'][$u]['project_grade'][0]['project_risk_category_item_grade']))
                                $result[$sr]['data'][$s]['sub_category'][$r]['attributes'][$u]['project_grade'] = $result[$sr]['data'][$s]['sub_category'][$r]['attributes'][$u]['project_grade'][0]['project_risk_category_item_grade'];
                            else
                                $result[$sr]['data'][$s]['sub_category'][$r]['attributes'][$u]['project_grade'] = 0;
                        }

                        $attributes = $result[$sr]['data'][$s]['sub_category'][$r]['attributes'];
                        $attributes = array_map(function ($i) { return $i['project_grade']; }, $attributes);
                        $remove = array(0);
                        $attributes = array_diff($attributes, $remove);
                        if(count($attributes)>0)
                            $attributes = (array_sum($attributes)/count($attributes));
                        else $attributes = 0;
                        $result[$sr]['data'][$s]['sub_category'][$r]['final_grade'] =  number_format((($attributes*$result[$sr]['data'][$s]['sub_category'][$r]['risk_percentage'])/100),2);
                    }

                    $sub_category = $result[$sr]['data'][$s]['sub_category'];
                    $sub_category = array_map(function ($i) { return $i['final_grade']; }, $sub_category);
                    $sub_category = array_sum($sub_category);
                    $result[$sr]['data'][$s]['final_grade'] =  number_format((($sub_category*$result[$sr]['data'][$s]['risk_percentage'])/100),2);
                }
            }
        }

        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function terms_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->form_validator->add_rules('crm_project_id', array('required'=> 'crm project id required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        //$this->form_validator->add_rules('product_term_id', array('required'=> 'Term id required'));
        $this->form_validator->add_rules('product_term_key', array('required'=> 'Term key required'));
        $this->form_validator->add_rules('product_term_type', array('required'=> 'Term Type required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = array();
        $project_details = $this->Crm_model->getProject($data['crm_project_id']);

        if(!isset($data['product_term_id']))
        {
            $product_term_details = $this->Company_model->getProductTerms($data);
            $data['product_term_id'] = $product_term_details[0]['id_product_term'];
        }

        if($data['product_term_type']=='item' || $data['product_term_type']=='item')
        {
            $result = $this->Company_model->getProductTermItems($data);
            for($s=0;$s<count($result);$s++)
            {
					
                if(trim(strtolower($result[$s]['product_term_item_name']))=='facility information')
                {
                    $form = $this->Crm_model->getFormFieldValueByFieldName(array('form_name' => trim($result[$s]['product_term_item_name']), 'crm_project_id' => $data['crm_project_id']));
                    if(!empty($form)) $result[$s]['answer'] = json_decode($form[0]['form_field_value']);
                    else  $result[$s]['answer'] = '';
                }
                else
                {
                    $result[$s]['answer'] = $this->Crm_model->getProductTermItemData(array('crm_project_id' => $data['crm_project_id'], 'product_term_item_id' => $result[$s]['id_product_term_item']));
                    if(!empty($result[$s]['answer'])){ $result[$s]['answer'] = $result[$s]['answer'][0]['item_data']; }
                    else { $result[$s]['answer'] = ''; }
                }
            }
        }
        else if($data['product_term_type']=='question')
        {
            $result = $this->Company_model->getAssessmentQuestionType(array('assessment_question_type_key' => $data['product_term_key']));
            for($s=0;$s<count($result);$s++)
            {
                if(isset($data['product_term_key']) && ($data['product_term_key'] =='pre_disbursement_checklist' || $data['product_term_key'] =='restriction_limit')){
                    $data['product_id'] = $project_details[0]['product_id'];
                }
                else{ $data['product_id'] = 0; }
                $order = 'DESC';
                if($data['product_term_key']=='pre_disbursement_checklist'){ $order = 'ASC'; }
                $result[$s]['category'] = $this->Company_model->getAssessmentQuestionCategory(array('assessment_question_type_id' => $result[$s]['id_assessment_question_type'], 'company_id' => $data['company_id'], 'assessment_question_category_status' => 1, 'sector_id' => $project_details[0]['project_main_sector_id'], 'sub_sector_id' => $project_details[0]['project_sub_sector_id'], 'product_id' => $data['product_id'], 'order' => $order));
                //echo "<pre>"; print_r($result[$s]['category']); exit;
                for($r=0;$r<count($result[$s]['category']);$r++)
                {
                    $result[$s]['category'][$r]['question'] = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $result[$s]['category'][$r]['id_assessment_question_category'], 'question_status' => 1));
                    for($sr=0;$sr<count($result[$s]['category'][$r]['question']);$sr++)
                    {
                        if(isset($result[$s]['category'][$r]['question'][$sr]['question_option']) && $result[$s]['category'][$r]['question'][$sr]['question_option']!=''){
                            $result[$s]['category'][$r]['question'][$sr]['question_option'] = json_decode($result[$s]['category'][$r]['question'][$sr]['question_option']);
                        }
                        $result[$s]['category'][$r]['question'][$sr]['answer'] = $this->Company_model->getAssessmentQuestionAnswer(array('assessment_question_id' => $result[$s]['category'][$r]['question'][$sr]['id_assessment_question'], 'crm_project_id' => $data['crm_project_id']));

                        if(isset($result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer) && $result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer!=''){
                            if($result[$s]['category'][$r]['question'][$sr]['question_type']=='checkbox')
                                $result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer = json_decode($result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer);
                            else if($result[$s]['category'][$r]['question'][$sr]['question_type']=='file')
                                $result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer = getImageUrl($result[$s]['category'][$r]['question'][$sr]['answer']->assessment_answer,'file');
                        }
                        if(isset($result[$s]['category'][$r]['question'][$sr]['answer']->comment_file) && $result[$s]['category'][$r]['question'][$sr]['answer']->comment_file!=''){
                            $result[$s]['category'][$r]['question'][$sr]['answer']->comment_file = getImageUrl($result[$s]['category'][$r]['question'][$sr]['answer']->comment_file,'file');
                        }
                    }
                }
            }
        }


        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function productTermItemData_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
		
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
		
        $this->form_validator->add_rules('crm_project_id', array('required'=> 'Project id required'));
        $this->form_validator->add_rules('created_by', array('required'=> 'Created by required'));
        $this->form_validator->add_rules('product_term_item_id', array('required'=> 'Product term item id required'));
        $this->form_validator->add_rules('item_data', array('required'=> 'Data required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $check_item_data = $this->Crm_model->getProductTermItemData(array('crm_project_id' => $data['crm_project_id'], 'product_term_item_id' => $data['product_term_item_id']));

        if(!empty($check_item_data))
        {
            $this->Crm_model->updateProductTermItemData($data);
            $suc_msg = 'Information updated successfully.';
        }
        else
        {
            $this->Crm_model->addProductTermItemData($data);
            $suc_msg = 'Information added successfully.';
        }

        //adding module status for financial sheets
        $term = $this->Company_model->getProductTermItems(array('id_product_term_item' => $data['product_term_item_id']));
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $term[0]['product_term_id'], 'reference_type' => 'term'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => $term[0]['product_term_id'], 'reference_type' => 'term', 'created_by' => $data['created_by'], 'updated_date_time' => date('Y-m-d H:i:s')));

        $this->Activity_model->addActivity(array('activity_name' => 'Project Terms','activity_template' => 'Information updated','module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'term','activity_reference_id' => $term[0]['product_term_id'],'created_by' => $data['created_by']));


        $result = array('status'=>TRUE, 'message' => $suc_msg, 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function riskCategory_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $this->form_validator->add_rules('crm_project_id', array('required'=> 'Crm project id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => 0));
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['sub_category'] = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => $result[$s]['id_risk_category']));
            for($r=0;$r<count($result[$s]['sub_category']);$r++)
            {
                $result[$s]['sub_category'][$r]['attributes'] = $this->Company_model->getRiskCategories(array('company_id' => $data['company_id'], 'parent_risk_category_id' => $result[$s]['sub_category'][$r]['id_risk_category']));
                for($u=0;$u<count($result[$s]['sub_category'][$r]['attributes']); $u++)
                {
                    $crm_project_details = $this->Crm_model->getProject($data['crm_project_id']);
                    $result[$s]['sub_category'][$r]['attributes'][$u]['items'] = $this->Crm_model->getRiskCategoryItemsWithProjectItems(array('crm_project_id'=>$data['crm_project_id'],'risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['id_risk_category'], 'sector_id' => $crm_project_details[0]['project_main_sector_id'],'sub_sector_id' => $crm_project_details[0]['project_sub_sector_id']));
                    /*$result[$s]['sub_category'][$r]['attributes'][$u]['sector_list'] = $this->Company_model->getRiskCategoryItemsSectors(array('risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['id_risk_category']));
                    for($h=0;$h<count($result[$s]['sub_category'][$r]['attributes'][$u]['sector_list']);$h++)
                    {

                    }*/
                }
            }
        }
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function projectRiskCategory_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_project_id', array('required'=> 'Project id required'));
        $this->form_validator->add_rules('user_id', array('required'=> 'User id required'));

        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $project_items = $data['data']; $add = $update = $risk_category_ids = array();
        for($s=0;$s<count($project_items);$s++)
        {
            if(!in_array($project_items[$s]['id_risk_category'],$risk_category_ids)){ array_push($risk_category_ids,$project_items[$s]['id_risk_category']); }
            $check_data = $this->Crm_model->getProjectRiskCategoryItem(array('risk_category_item_id' => $project_items[$s]['id_risk_category_item'],'crm_project_id' => $data['crm_project_id']));
            if(empty($check_data))
            {
                $add[] = array(
                    'risk_category_id' => $project_items[$s]['id_risk_category'],
                    'risk_category_item_id' => $project_items[$s]['id_risk_category_item'],
                    'project_risk_category_item_grade' => $project_items[$s]['grade'],
                    'user_id' => $data['user_id'],
                    'crm_project_id' => $data['crm_project_id'],
                    'is_active' => 1
                );
            }
            else{
                $update[] = array(
                    'id_project_risk_category_item' => $check_data[0]['id_project_risk_category_item'],
                    'risk_category_item_id' => $project_items[$s]['id_risk_category_item'],
                    'project_risk_category_item_grade' => $project_items[$s]['grade'],
                    'is_active' => 1
                );
            }
        }
        //update all project risk category items as is_active=0
        for($s=0;$s<count($risk_category_ids);$s++)
        {
            $this->Crm_model->updateProjectRiskCategory(array('is_active' => 0, 'risk_category_id' => $risk_category_ids[$s]));
        }
        //echo "<pre>"; print_r($add); print_r($update); exit;
        if(!empty($add)){ $this->Crm_model->addProjectRiskCategory($add); }
        if(!empty($update)){ $this->Crm_model->updateProjectRiskCategory($update); }

        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' =>'', 'reference_type' => 'risk'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' =>'', 'reference_type' => 'risk', 'created_by' => $data['user_id'], 'updated_date_time' => date('Y-m-d H:i:s')));
        $this->Activity_model->addActivity(array('activity_name' => 'Risk Assessment','activity_template' => 'Risk assessment information updated','module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'risk','activity_reference_id' => '','created_by' => $data['user_id']));

        $result = array('status'=>TRUE, 'message' =>'Risk Assessment updated successfully.', 'data'=>'');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getCrmProjectRiskRating($crm_project_id,$company_id)
    {
        $check_project_rating = $this->Crm_model->checkProjectContainsProjectRiskAssessment($crm_project_id);
        if(empty($check_project_rating))
        {
            $rating = 0;
            $status = 0;
        }
        else
        {
            $result = $this->Company_model->getRiskCategories(array('company_id' => $company_id, 'parent_risk_category_id' => 0));
            $rating = array();
            for($s=0;$s<count($result);$s++)
            {
                $result[$s]['sub_category'] = $this->Company_model->getRiskCategories(array('company_id' => $company_id, 'parent_risk_category_id' => $result[$s]['id_risk_category']));
                $sub_category = array();
                for($r=0;$r<count($result[$s]['sub_category']);$r++)
                {
                    $result[$s]['sub_category'][$r]['attributes'] = $this->Company_model->getRiskCategories(array('company_id' => $company_id, 'parent_risk_category_id' => $result[$s]['sub_category'][$r]['id_risk_category']));
                    $attributes = array();
                    for($u=0;$u<count($result[$s]['sub_category'][$r]['attributes']); $u++)
                    {
                        $crm_project_details = $this->Crm_model->getProject($crm_project_id);
                        $result[$s]['sub_category'][$r]['attributes'][$u]['project_grade'] = $this->Crm_model->getCrmProjectRiskCategoryAttributesItemsGrade(array('crm_project_id'=>$crm_project_id,'risk_category_id' => $result[$s]['sub_category'][$r]['attributes'][$u]['id_risk_category'], 'sector_id' => $crm_project_details[0]['project_main_sector_id'],'sub_sector_id' => $crm_project_details[0]['project_sub_sector_id']));
                        //echo "<pre>"; print_r($result[$s]['sub_category'][$r]['attributes'][$u]); exit;
                        if(isset($result[$s]['sub_category'][$r]['attributes'][$u]['project_grade'][0]['project_risk_category_item_grade']))
                            $result[$s]['sub_category'][$r]['attributes'][$u]['project_grade'] = $result[$s]['sub_category'][$r]['attributes'][$u]['project_grade'][0]['project_risk_category_item_grade'];
                        else
                            $result[$s]['sub_category'][$r]['attributes'][$u]['project_grade'] = 0;
                    }

                    $attributes = $result[$s]['sub_category'][$r]['attributes'];
                    $attributes = array_map(function ($i) { return $i['project_grade']; }, $attributes);
                    $remove = array(0);
                    $attributes = array_diff($attributes, $remove);
                    if(count($attributes)>0)
                        $attributes = (array_sum($attributes)/count($attributes));
                    else $attributes = 0;
                    $result[$s]['sub_category'][$r]['final_grade'] =  (($attributes*$result[$s]['sub_category'][$r]['risk_percentage'])/100);

                }
                $sub_category = $result[$s]['sub_category'];
                $sub_category = array_map(function ($i) { return $i['final_grade']; }, $sub_category);
                $sub_category = array_sum($sub_category);
                /*$remove = array(0);
                $sub_category = array_diff($sub_category, $remove);
                if(count($sub_category)>0)
                    $sub_category = (array_sum($sub_category)/count($sub_category));
                else $sub_category = 0;*/
                $result[$s]['final_grade'] =  (($sub_category*$result[$s]['risk_percentage'])/100);

            }
            $rating = $result;
            $rating = array_map(function ($i) { return $i['final_grade']; }, $rating);
            /*$rating = number_format((array_sum($rating)/count($rating)),2);*/
            $rating = array_sum($rating);
            $rating = number_format($rating,2);
            $status = 1;
        }

        return $rating.'@@@'.$status;
    }

    public function projectRiskGradeCalculation($rating)
    {
        $grade = '';

        if(!is_numeric($rating) && !is_float($rating)){ $grade = '---'; }
        else if(0<=$rating && $rating<1.25){ $grade = 'A'; }
        else if(1.25<=$rating && $rating<2.25){ $grade = 'B'; }
        else if(2.25<=$rating && $rating<3.25){ $grade = 'C'; }
        else if(3.25<=$rating && $rating<4.25){ $grade = 'D'; }
        else if(4.25<=$rating && $rating<5.25){ $grade = 'E'; }
        else if(5.25<=$rating && $rating<6.25){ $grade = 'F'; }
        else if(6.25<=$rating){ $grade = 'G'; }

        return $grade;
    }

    public function statusFlow_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $this->form_validator->add_rules('crm_project_id', array('required'=> 'Crm project id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project_details = $this->Crm_model->getProjectList(array('crm_project_id' => $data['crm_project_id'], 'company_id' => $data['company_id']));
        $result = $this->Crm_model->statusFlow($data);
        //echo "<pre>"; print_r($result); exit;

        if(empty($result))
        {
            $result[0] = array(
                'assigned_to' => 0,
                'id_project_approval' => 0,
                'legal_name' => $project_details[0]['legal_name'],
                'approval_status' => $project_details[0]['project_status'],
                'date' => date('d M Y',strtotime($project_details[0]['created_date_time'])),
                'credit_committee_id' => '',
                'credit_committee_name' => $project_details[0]['username'],
                'username' => '---'
                );

            $result[0]['facilities'] = $this->Crm_model->getFacilityActivityInitial(array('crm_project_id' => $data['crm_project_id']));

        }
        else
        {
            //$facilities = $this->Crm_model->getFacilities(array('crm_project_id' => $data['crm_project_id']));
            array_push($result,array(
                'assigned_to' => 0,
                'id_project_approval' => 0,
                'legal_name' => $project_details[0]['legal_name'],
                'approval_status' => 'Pending',
                'date' => date('d M Y',strtotime($project_details[0]['created_date_time'])),
                'credit_committee_id' => '',
                'credit_committee_name' => $project_details[0]['username'],
                'username' => $result[(count($result)-1)]['date']
            ));

            for($s=0;$s<count($result);$s++)
            {
                $meeting = $this->Crm_model->projectApprovalMeeting(array('project_approval_id' => $result[$s]['id_project_approval']));
                for($st=0;$st<count($meeting);$st++){
                    if($meeting[$st]['from_time']!=0 && $meeting[$st]['to_time']!=0)
                        $meeting[$st]['meeting_date'] = $meeting[$st]['meeting_date'].' '.$meeting[$st]['meeting_time'];
                    else if($meeting[$st]['from_time']!=0 && $meeting[$st]['to_time']==0)
                        $meeting[$st]['meeting_date'] = $meeting[$st]['meeting_date'].' '.$meeting[$st]['from_time'];
                    else if($meeting[$st]['from_time']==0 && $meeting[$st]['to_time']!=0)
                        $meeting[$st]['meeting_date'] = $meeting[$st]['meeting_date'].' '.$meeting[$st]['to_time'];

                    $email = $this->Crm_model->getMeetingGuest(array('meeting_id' => $meeting[$st]['id_meeting']));
                    $meeting[$st]['guest'] = $email[0]['guest'];
                }
                if(!empty($meeting)){
                    $result[$s]['meeting'] = $meeting;
                }
                else{
                    $result[$s]['meeting'] = array();
                }

                //getting facilities
                if($result[$s]['id_project_approval']==0) {
                    $result[$s]['facilities'] = $this->Crm_model->getFacilityActivityInitial(array('crm_project_id' => $data['crm_project_id']));
                }
                else {
                    $result[$s]['facilities'] = $this->Crm_model->getFacilityActivity(array('project_approval_id' => $result[$s]['id_project_approval']));
                    /*if(empty($result[$s]['facilities'])){
                        $result[$s]['facilities'] = $facility = $this->Crm_model->getFacilities(array('crm_project_id' => $data['crm_project_id']));
                    }*/
                }
            }
        }
        //echo "<pre>"; print_r($result); exit;

        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facility_get()
    {
        $data = $this->input->get();
        //$this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $this->form_validator->add_rules('crm_project_id', array('required'=> 'Crm project id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Crm_model->getFacilities($data);
        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function facility_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('crm_project_id', array('required'=> 'Crm project id required'));
        $this->form_validator->add_rules('facility_name', array('required'=> 'Facility name required'));
        $this->form_validator->add_rules('currency_id', array('required'=> 'Currency required'));
        $this->form_validator->add_rules('loan_amount', array('required'=> 'Loan amount required','float' => 'Invalid loan amount'));
        $this->form_validator->add_rules('expected_start_date', array('required'=> 'Start date required'));
        $this->form_validator->add_rules('expected_maturity', array('required'=> 'Maturity required'));
        $this->form_validator->add_rules('expected_maturity_type', array('required'=> 'Maturity type required'));
        $this->form_validator->add_rules('expected_interest_rate', array('required'=> 'Interest rate required','float' => 'Invalid interest rate'));
        $this->form_validator->add_rules('project_loan_type_id', array('required'=> 'Loan type required'));
        $this->form_validator->add_rules('project_payment_type_id', array('required'=> 'Payment type required'));
        $this->form_validator->add_rules('created_by', array('required'=> 'User id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $facility_comments = ''; $project_approval_id = 0;
        if(!isset($data['project_facility_status'])){ $data['project_facility_status'] = 'Inactive'; }


        $project_details = $this->Crm_model->getProject($data['crm_project_id']);

        $company_details = $this->Company_model->getCompany(array('id_company' => $project_details[0]['company_id']));


        $currency_value = $this->Crm_model->getCompanyCurrencyValue(array('company_id' => $project_details[0]['company_id'], 'currency_id' => $data['currency_id']));



        if(isset($data['project_approval_id']))
        {
            if($project_details[0]['currency_id']!='' || $project_details[0]['currency_id']!=0){ $company_details->currency_id = $project_details[0]['currency_id']; }
            else
            {
                $this->Crm_model->updateProject(array('currency_id' => $company_details->currency_id),$data['crm_project_id']);
            }

            if(isset($data['project_approval_id'])){ $project_approval_id = $data['project_approval_id']; unset($data['project_approval_id']); }
            if(isset($data['facility_comments'])){ $facility_comments = $data['facility_comments']; unset($data['facility_comments']); }
            $prev_facility = $this->Crm_model->getFacilities(array('crm_project_id' => $data['crm_project_id'], 'id_project_facility' => $data['id_project_facility']));
            $new_currency_value = $prev_facility[0]['currency_value'];
            $result = array();
            if(isset($data['id_project_facility']))
            {
                $result = $this->Crm_model->updateFacilities(array('id_project_facility' => $data['id_project_facility'], 'loan_amount' => $data['loan_amount'], 'project_facility_status' => $data['project_facility_status']));
                $this->Crm_model->addProjectFacilityHistory(array('project_facility_id' => $data['id_project_facility'],'created_by' => $data['created_by'], 'type' => 'update','currency_id' => $data['currency_id'], 'amount' => $data['loan_amount'], 'curreny_value' => $new_currency_value));

                $this->Crm_model->addFacilityActivity(array('created_by' => $data['created_by'], 'project_facility_id' => $data['id_project_facility'], 'project_approval_id' => $project_approval_id, 'facility_amount' => $data['loan_amount'], 'facility_status' => $data['project_facility_status'],'facility_comments' => $facility_comments));
            }

            $final_amount = 0;
            $facility_list = $this->Crm_model->getFacilities(array('crm_project_id' => $data['crm_project_id']));

            for($s=0;$s<count($facility_list);$s++)
            {
                if($facility_list[$s]['currency_id']==$company_details->currency_id)
                {
                    $final_amount = $final_amount + $facility_list[$s]['loan_amount'];
                }
                else
                {
                    $final_amount = $final_amount + ($facility_list[$s]['loan_amount']*$facility_list[$s]['currency_value']);
                }
            }
            $this->Crm_model->updateProject(array('amount' => $final_amount),$data['crm_project_id']);

            $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>$result);
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if($company_details->currency_id!=$data['currency_id']){
            if(empty($currency_value)){
                $result = array('status'=>FALSE,'error'=>array('config' => 'Please configure currency and try again'),'data'=>'');
                $this->response($result, REST_Controller::HTTP_OK);
            }
        }

        if($project_details[0]['currency_id']!='' || $project_details[0]['currency_id']!=0){ $company_details->currency_id = $project_details[0]['currency_id']; }
        else
        {
            $this->Crm_model->updateProject(array('currency_id' => $company_details->currency_id),$data['crm_project_id']);
        }

        //echo "<pre>"; print_r($company_details); exit;
        $amount = $result = '';



        if(isset($data['id_project_facility']))
        {
            $prev_facility = $this->Crm_model->getFacilities(array('crm_project_id' => $data['crm_project_id'], 'id_project_facility' => $data['id_project_facility']));
            //echo "<pre>"; print_r($prev_facility);
            if($company_details->currency_id==$data['currency_id']){
                $amount = $project_details[0]['amount']+$data['loan_amount']-$prev_facility[0]['loan_amount'];
                $new_currency_value = $prev_facility[0]['currency_value'];
                $data['currency_value'] = 0;
            }
            else{
                if($prev_facility[0]['currency_id']==$data['currency_id'])
                {
                    $new_currency_value = $prev_facility[0]['currency_value'];
                    $amount = ($data['loan_amount']*$prev_facility[0]['currency_value']);
                    $amount = $project_details[0]['amount']+$amount-$prev_facility[0]['loan_amount'];
                }
                else
                {
                    $company_currency = $this->Crm_model->getCompanyCurrencyValue(array('company_id' => $project_details[0]['company_id'], 'currency_id' => $data['currency_id']));
                    if(!empty($company_currency))
                    {
                        $amount = ($data['loan_amount']*$company_currency[0]['company_currency_value']);
                        $amount = $project_details[0]['amount']+$amount-$prev_facility[0]['loan_amount'];
                        $new_currency_value = $company_currency[0]['company_currency_value'];
                    }
                    $data['currency_value'] = $company_currency[0]['company_currency_value'];
                }

            }
            $result = $this->Crm_model->updateFacilities($data);

            //$this->Crm_model->updateProject(array('amount' => $amount),$data['crm_project_id']);

            $this->Crm_model->addProjectFacilityHistory(array('project_facility_id' => $data['id_project_facility'],'created_by' => $data['created_by'], 'type' => 'update','currency_id' => $data['currency_id'], 'amount' => $data['loan_amount'], 'curreny_value' => $new_currency_value));
        }
        else
        {
            if($company_details->currency_id==$data['currency_id']){
                $amount = $project_details[0]['amount']+$data['loan_amount'];
                $data['currency_value'] = 0;
            }
            else{
                $currency_value = $this->Crm_model->getCompanyCurrencyValue(array('company_id' => $project_details[0]['company_id'], 'currency_id' => $data['currency_id']));

                if(!empty($currency_value)){
                    $amount = ($data['loan_amount']*$currency_value[0]['company_currency_value']+$project_details[0]['amount']);
                }
                if(isset($currency_value[0]['company_currency_value']))
                    $data['currency_value'] = $currency_value[0]['company_currency_value'];
                else
                    $data['currency_value'] = 0;
            }
            //echo "<pre>"; print_r($data); exit;
            $id_project_facility = $this->Crm_model->addFacilities($data);
            //$this->Crm_model->updateProject(array('amount' => $amount),$data['crm_project_id']);
            $company_currency_value = 0;
            if(isset($currency_value[0]['company_currency_value'])){ $company_currency_value = $currency_value[0]['company_currency_value']; }
            $this->Crm_model->addProjectFacilityHistory(array('project_facility_id' => $id_project_facility,'created_by' => $data['created_by'], 'type' => 'add', 'currency_id' => $data['currency_id'], 'amount' => $data['loan_amount'], 'curreny_value' => $company_currency_value));
        }

        $final_amount = 0;
        $facility_list = $this->Crm_model->getFacilities(array('crm_project_id' => $data['crm_project_id']));
        //echo "<pre>"; print_r($facility_list); exit;
        for($s=0;$s<count($facility_list);$s++)
        {
            if($facility_list[$s]['currency_id']==$company_details->currency_id)
            {
                $final_amount = $final_amount + $facility_list[$s]['loan_amount'];
            }
            else
            {
                //$company_currency = $this->Crm_model->getCompanyCurrencyValue(array('company_id' => $project_details[0]['company_id'], 'currency_id' => $facility_list[$s]['currency_id']));
                //if(isset($company_currency[0]['company_currency_value']))
                $final_amount = $final_amount + ($facility_list[$s]['loan_amount']*$facility_list[$s]['currency_value']);
            }
        }
        //$final_amount; exit;
        $this->Crm_model->updateProject(array('amount' => $final_amount),$data['crm_project_id']);

        //sdding module status for financial sheets
        $module_status = $this->Crm_model->getCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => '', 'reference_type' => 'facility'));
        if(empty($module_status))
            $this->Crm_model->addCrmModuleStatus(array('crm_module_id' => $data['crm_project_id'], 'crm_module_type' => 'project', 'reference_id' => '', 'reference_type' => 'facility', 'created_by' => $data['created_by'], 'updated_date_time' => date('Y-m-d H:i:s')));

        $this->Activity_model->addActivity(array('activity_name' => 'Facility','activity_template' => 'Facility added','module_type' => 'project','module_id' => $data['crm_project_id'], 'activity_type' => 'facility','activity_reference_id' => '','created_by' => $data['created_by']));


        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledgeDocumentComment_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('knowledge_document_id', array('required'=> 'Document id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Crm_model->getKnowledgeDocumentComments($data);
        for($s=0;$s<count($result);$s++)
        {
            $result[$s]['profile_image'] = getImageUrl($result[$s]['profile_image'],'profile');
        }
        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledgeDocumentComment_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('knowledge_document_id', array('required'=> 'Document id required'));
        $this->form_validator->add_rules('commented_by', array('required'=> 'User id required'));
        $this->form_validator->add_rules('comment', array('required'=> 'comment required'));
        //$this->form_validator->add_rules('comment_status', array('required'=> 'comment_status required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(isset($data['id_knowledge_document_comment']))
            $result = $this->Crm_model->updateKnowledgeDocumentComments($data);
        else
            $result = $this->Crm_model->addKnowledgeDocumentComments($data);

        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledgeDocumentDetails_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('knowledge_document_id', array('required'=> 'Document id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $result = $this->Company_model->getKnowledgeDocuments(array('knowledge_document_id' => $data['knowledge_document_id']));
        for($s=0;$s<count($result);$s++)
        {
            $file = explode('/',$result[$s]['document_source']);
            if($result[$s]['document_type']=='document'){
                //echo "<pre>"; print_r(filesize('uploads'.$result[$s]['document_source'])); exit;
                $result[$s]['document_source'] = getImageUrl($result[$s]['document_source'],'file');
                $result[$s]['size']=get_file_info($result[$s]['document_source'], 'size');
            }
            $file_name = '';
            if(isset($file[1])){ $file_name = $file[1]; }
            $result[$s]['file_name'] = $file_name;
            $result[$s]['comments'] = $this->Crm_model->getKnowledgeDocumentComments(array('knowledge_document_id' => $data['knowledge_document_id']));
        }
        if(!empty($result[0]))
            $result = $result[0];
        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function knowledgePublish_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        $this->form_validator->add_rules('knowledge_document_id', array('required'=> 'Document id required'));
        $this->form_validator->add_rules('knowledge_document_status', array('required'=> 'status required'));
        //$this->form_validator->add_rules('comment_status', array('required'=> 'comment_status required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Company_model->updateKnowledgeDocument(array('id_knowledge_document' => $data['knowledge_document_id'], 'knowledge_document_status' => $data['knowledge_document_status']));

        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function currency_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $company_details = $this->Company_model->getCompanyById($data['company_id']);
        $result = $this->Crm_model->getCurrency($data);
        array_push($result,array('id_currency' => $company_details->currency_id, 'currency_code' => $company_details->currency_code));
        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function touchPoint_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('crm_module_id', array('required'=> 'Module id required'));
        $this->form_validator->add_rules('crm_module_type', array('required'=> 'Module type required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'Company id required'));
        $this->form_validator->add_rules('offset', array('required'=> 'offset required'));
        $this->form_validator->add_rules('limit', array('required'=> 'limit required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else if(($data['crm_module_type']!='contact') && ($data['crm_module_type']!='company') && ($data['crm_module_type']!='project'))
        {
            $result = array('status'=>FALSE,'error'=>array('module_type' => 'Invalid module type'),'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        if(!isset($data['offset'])){ $data['offset'] = 0; }
        if(!isset($data['limit'])){ $data['limit'] = 5; }

        if($data['crm_module_type']=='project')
        {
            $project_details = $this->Crm_model->getProject($data['crm_module_id']);

            if($project_details[0]['sub_sector']!=''){ $data['sector'] = $project_details[0]['sub_sector']; }
            else if($project_details[0]['sector']!=''){ $data['sector'] = $project_details[0]['sector']; }
            else{ $data['sector']=''; }
        }
        else if($data['crm_module_type']=='company')
        {
            $company_details = $this->Crm_model->getCompany($data['crm_module_id']);
            if($company_details[0]['sub_sector']!=''){ $data['sector'] = $company_details[0]['sub_sector']; }
            else if($company_details[0]['sector']!=''){ $data['sector'] = $company_details[0]['sector']; }
            else{ $data['sector']=''; }
        }
        else if($data['crm_module_type']=='contact')
        {
            $data['sector']='';
        }

        $result = $this->Company_model->getTouchPoints($data);
        $result = array('status'=>TRUE, 'message' =>'', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateral_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $data = $this->input->post();
        $this->form_validator->add_rules('project_facility_id', array('required'=> 'Facility required'));
        $this->form_validator->add_rules('collateral_type_id', array('required'=> 'Collateral type required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if(!isset($data['id_collateral']))
        {
            $result = $this->Crm_model->addCollateral($data);
            $message = 'Collateral added successfully';
        }
        else
        {
            $result = $this->Crm_model->updateCollateral($data);
            $message = 'Collateral updated successfully';
        }
        $result = array('status'=>TRUE, 'message' => $message, 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateral_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('id_collateral', array('required'=> 'Collateral required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $collateral_details = $this->Crm_model->getCollateralDetails($data['id_collateral']);
        if(!empty($collateral_details)) $collateral_details = $collateral_details[0];
        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=>$collateral_details);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralTypeFormData_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('collateral_id', array('required'=> 'Collateral required'));
        $this->form_validator->add_rules('collateral_type_id', array('required'=> 'Collateral type required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $collateral_type_form_data = $this->Crm_model->getCollateralTypeFieldData($data);
        $collateral_type_field_data = [];
        for($ctf = 0; $ctf < count($collateral_type_form_data); $ctf++)
        {
            $collateral_type_field_data[$collateral_type_form_data[$ctf]['collateral_type_filed_name']] = $collateral_type_form_data[$ctf]['collateral_type_field_value'];
        }
        $collateral_details = $this->Crm_model->getCollateralTypeDetails($data);
        if(!empty($collateral_details)) $collateral_details = $collateral_details[0];
        $result = array('status'=>TRUE, 'message' =>'Success', 'data'=> ['collateral_type_details' => $collateral_details, 'collateral_type_field_data' => $collateral_type_field_data]);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralStageFormData_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('collateral_id', array('required'=> 'Collateral required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'Company required'));
        $this->form_validator->add_rules('collateral_type_id', array('required'=> 'Collateral type required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $collateral_stage = $this->Crm_model->getCollateralTypeStages($data);
        $dates = array();
        for($cs = 0; $cs < count($collateral_stage); $cs++)
        {
            $data['collateral_stage_id'] = $collateral_stage[$cs]['id_collateral_stage'];
            /*$collateral_stage_fields = $this->Crm_model->getCollateralStageFields($data);
            $collateral_stage_field_data = '';
            for($csf=0; $csf < count($collateral_stage_fields); $csf++)
            {
                $collateral_stage_field_data[$collateral_stage_fields[$csf]['collateral_stage_filed_name']] = $collateral_stage_fields[$csf]['collateral_stage_field_value'];
            }
            $collateral_stage[$cs]['form_data'] = $collateral_stage_field_data;*/

            $date_fields = array('external_date_of_check','perfection_confirmation_date');

            $dates = $this->Crm_model->getCollateralStageFieldsDataDates($data);
            $collateral_stage[$cs]['form_data'] = array();
            for($s=0;$s<count($dates);$s++)
            {
                //echo $dates[$s]['date']; exit;
                $data['date'] = $dates[$s]['date'];
                $collateral_stage_fields = $this->Crm_model->getCollateralStageFieldsByDate($data);
                $collateral_stage_field_data = '';
                for($csf=0; $csf < count($collateral_stage_fields); $csf++)
                {
                    /*if($collateral_stage_fields[$csf]['collateral_stage_filed_name']!='' && in_array($collateral_stage_fields[$csf]['collateral_stage_filed_name'],$date_fields)){
                        $collateral_stage_fields[$csf]['collateral_stage_field_value'] = date('Y-m-d',strtotime($collateral_stage_fields[$csf]['collateral_stage_field_value']));
                    }*/
                    $collateral_stage_field_data[$collateral_stage_fields[$csf]['collateral_stage_filed_name']] = $collateral_stage_fields[$csf]['collateral_stage_field_value'];
                }
                if($dates[$s]['date']!=''){ $dates[$s]['date'] = date('d M, Y',strtotime($dates[$s]['date'])); }
                $collateral_stage[$cs]['form_data'][] = array('date' => $dates[$s]['date'], 'data' => $collateral_stage_field_data);
            }
            //$collateral_stage[$cs]['dates'] = $dates;
        }

        $result = array('status'=>TRUE, 'message' => '', 'data'=> $collateral_stage);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralStageFormData_delete()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('date', array('required'=> 'Date required'));
        $this->form_validator->add_rules('collateral_id', array('required'=> 'Collateral id required'));
        $this->form_validator->add_rules('collateral_stage_id', array('required'=> 'Cpllateral stage id required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->Crm_model->deleteCollateralStageFormData($data);
        $result = array('status'=>TRUE, 'message' => 'Success', 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralList_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> 'Company required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Crm_model->getCollateralList($data);
        if(isset($data['offset'])){ unset($data['offset']); }
        if(isset($data['limit'])){ unset($data['limit']); }
        $total_records = count($this->Crm_model->getCollateralList($data));

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>array('data' =>$result,'total_records' => $total_records));
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralTypeFormData_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required'=> 'Collateral required'));
        $this->form_validator->add_rules('collateral_type_id', array('required'=> 'Collateral type required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $form_data = array(); $st=0; $type = 'insert';
        $form_fields = $this->Crm_model->getCollateralTypeFields($data);
        $collateral_type_form_data = $this->Crm_model->getCollateralTypeFormData($data);
        if(empty($collateral_type_form_data)) $type = 'insert'; else $type = 'update';
        $collateralId = $data['collateral_id'];
        unset($data['collateral_id']);
        $collateralTypeId = $data['collateral_type_id'];
        unset($data['collateral_type_id']);
        $data_keys = array_keys($data);
        for($s=0;$s<count($form_fields);$s++){
            $check_field = 0;
            $form_data[$st]['collateral_id'] = $collateralId;
            $form_data[$st]['collateral_type_id'] = $collateralTypeId;
            $form_data[$st]['collateral_type_field_id'] = $form_fields[$s]['id_collateral_type_field'];
            for($r=0;$r<count($data_keys);$r++){
                if($form_fields[$s]['collateral_type_filed_name']==$data_keys[$r]){
                    $form_data[$st]['collateral_type_field_value'] = $data[$data_keys[$r]];
                    $st++;
                    $check_field = 1;
                }
            }
            if($check_field==0){
                $form_data[$st]['collateral_type_field_value'] = '';
                $st++;
            }
        }
        if(empty($form_data)){
            $result = array('status'=>FALSE, 'error' => 'invalid fields', 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        if($type=='insert')
        {
            $this->Crm_model->addCollateralTypeFormData($form_data);
        }
        else
        {
            if(!empty($collateral_type_form_data)){
                for($s=0;$s<count($collateral_type_form_data);$s++){
                    for($r=0;$r<count($form_data);$r++){
                        if($collateral_type_form_data[$s]['collateral_type_field_id']==$form_data[$r]['collateral_type_field_id']){
                            $form_data[$r]['id_collateral_type_field_data'] = $collateral_type_form_data[$s]['id_collateral_type_field_data'];
                        }
                    }
                }
            }
            $new_form_fields = array();
            for($sr=0;$sr<count($form_data);$sr++)
            {
                if(!isset($form_data[$sr]['id_collateral_type_field_data'])){
                    $new_form_fields[] = $form_data[$sr];
                    unset($form_data[$sr]);
                }
            }
            if(!empty($new_form_fields))
                $this->Crm_model->addCollateralTypeFormData($new_form_fields);
            $this->Crm_model->updateCollateralTypeFormData($form_data);

        }
        $result = array('status'=>TRUE, 'message' => 'Information updated successfully', 'data'=> []);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralTypeDocuments_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('collateral_type_id', array('required'=> 'Collateral type required'));
        $this->form_validator->add_rules('collateral_id', array('required'=> 'Collateral required'));
        $this->form_validator->add_rules('company_id', array('required'=> 'Company required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $result = $this->Crm_model->getCollateralTypeCategories($data);
        for($r=0;$r<count($result);$r++)
        {
            $result[$r]['question'] = $this->Company_model->getAssessmentQuestion(array('assessment_question_category_id' => $result[$r]['id_assessment_question_category'], 'question_status' => 1));
            for($sr=0;$sr<count($result[$r]['question']);$sr++)
            {
                if(isset($result[$r]['question'][$sr]['question_option']) && $result[$r]['question'][$sr]['question_option']!=''){
                    $result[$r]['question'][$sr]['question_option'] = json_decode($result[$r]['question'][$sr]['question_option']);
                }
                $result[$r]['question'][$sr]['answer'] = $this->Company_model->getAssessmentQuestionAnswer(array('assessment_question_id' => $result[$r]['question'][$sr]['id_assessment_question'], 'collateral_id' => $data['collateral_id']));
                if(isset($result[$r]['question'][$sr]['answer']->assessment_answer) && $result[$r]['question'][$sr]['answer']->assessment_answer!=''){
                    if($result[$r]['question'][$sr]['question_type']=='checkbox')
                        $result[$r]['question'][$sr]['answer']->assessment_answer = json_decode($result[$r]['question'][$sr]['answer']->assessment_answer);
                    else if($result[$r]['question'][$sr]['question_type']=='file')
                        $result[$r]['question'][$sr]['answer']->assessment_answer = getImageUrl($result[$r]['question'][$sr]['answer']->assessment_answer,'file');
                }
                if(isset($result[$r]['question'][$sr]['answer']->comment_file) && $result[$r]['question'][$sr]['answer']->comment_file!=''){
                    $result[$r]['question'][$sr]['answer']->comment_file = getImageUrl($result[$r]['question'][$sr]['answer']->comment_file,'file');
                }
            }
        }
        $result = array('status'=>TRUE, 'message' => '', 'data'=>$result);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function collateralStageFormData_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('collateral_id', array('required'=> 'Collateral required'));
        $this->form_validator->add_rules('collateral_stage_id', array('required'=> 'Collateral stage required'));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $form_data = array(); $st=0; $type = 'insert';
        $form_fields = $this->Crm_model->getCollateralStageFormFields($data);
        $collateral_stage_form_data = $this->Crm_model->getCollateralStageFormData($data);
        if(empty($collateral_stage_form_data)) $type = 'insert'; else $type = 'update';
        $collateralId = $data['collateral_id'];
        unset($data['collateral_id']);
        $collateralStageId = $data['collateral_stage_id'];
        unset($data['collateral_stage_id']);
        $data_keys = array_keys($data);
        for($s=0;$s<count($form_fields);$s++){
            $check_field = 0;
            $form_data[$st]['collateral_id'] = $collateralId;
            $form_data[$st]['collateral_stage_id'] = $collateralStageId;
            $form_data[$st]['collateral_stage_field_id'] = $form_fields[$s]['id_collateral_stage_field'];
            for($r=0;$r<count($data_keys);$r++){
                if($form_fields[$s]['collateral_stage_filed_name']==$data_keys[$r]){
                    $form_data[$st]['collateral_stage_field_value'] = $data[$data_keys[$r]];
                    $st++;
                    $check_field = 1;
                }
            }
            if($check_field==0){
                $form_data[$st]['collateral_stage_field_value'] = '';
                $st++;
            }
        }
        if(empty($form_data)){
            $result = array('status'=>FALSE, 'error' => 'invalid fields', 'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        //echo "<pre>"; print_r($form_data); exit;
        $this->Crm_model->addCollateralStageFormData($form_data);
        /*if($type=='insert')
        {
            $this->Crm_model->addCollateralStageFormData($form_data);
        }
        else
        {
            if(!empty($collateral_stage_form_data)){
                for($s=0;$s<count($collateral_stage_form_data);$s++){
                    for($r=0;$r<count($form_data);$r++){
                        if($collateral_stage_form_data[$s]['collateral_stage_field_id']==$form_data[$r]['collateral_stage_field_id']){
                            $form_data[$r]['id_collateral_stage_field_data'] = $collateral_stage_form_data[$s]['id_collateral_stage_field_data'];
                        }
                    }
                }
            }
            $new_form_fields = array();
            for($sr=0;$sr<count($form_data);$sr++)
            {
                if(!isset($form_data[$sr]['id_collateral_stage_field_data'])){
                    $new_form_fields[] = $form_data[$sr];
                    unset($form_data[$sr]);
                }
            }
            if(!empty($new_form_fields))
                $this->Crm_model->addCollateralStageFormData($new_form_fields);
            $this->Crm_model->updateCollateralStageFormData($form_data);

        }*/
        $result = array('status'=>TRUE, 'message' => 'Information updated successfully', 'data'=> []);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenant_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('covenant_type_key', array('required'=> $this->lang->line('covenant_type_key_req')));
        $this->form_validator->add_rules('covenant_category_id', array('required'=> $this->lang->line('covenant_category_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $project = $this->Crm_model->getProject($data['crm_project_id']);
        $data['sector_id']=0;
        if($project[0]['project_sub_sector_id']!='' && $project[0]['project_sub_sector_id']!=0){ $data['sector_id'] = $project[0]['project_sub_sector_id']; }
        else { $data['sector_id'] = $project[0]['project_main_sector_id']; }

        $covenant = $this->Crm_model->getProjectCovenant($data);
        $result = array();
        //$covenant_category_id = array_values(array_unique(array_map(function ($i) { return $i['id_covenant']; }, $covenant)));
        $covenant_category_id = $covenant_id = $project_covenant_id = array();
        for($s=0;$s<count($covenant);$s++)
        {
            if(!in_array($covenant[$s]['id_covenant_category'],$covenant_category_id))
            {
                //getting previous category covenants like objects
                if(isset($covenant[$s-1])){
                    $result[$covenant[$s-1]['id_covenant_category']]['covenant'] = array_values($result[$covenant[$s-1]['id_covenant_category']]['covenant']);
                }

                array_push($covenant_category_id,$covenant[$s]['id_covenant_category']);
                $result[$covenant[$s]['id_covenant_category']] = array(
                    'id_covenant_category' => $covenant[$s]['id_covenant_category'],
                    'covenant_category' => $covenant[$s]['covenant_category'],
                    'covenant' => array()
                );

                if($covenant[$s]['id_covenant']!='')
                {
                    $result[$covenant[$s]['id_covenant_category']]['covenant'][$covenant[$s]['id_covenant']] = array(
                        'id_covenant' => $covenant[$s]['id_covenant'],
                        'covenant_name' => $covenant[$s]['covenant_name'],
                        'created_by' => $covenant[$s]['created_by'],
                        'is_checked' => $covenant[$s]['is_checked']
                    );
                }
            }
            else
            {
                if($covenant[$s]['id_covenant']!=''){
                    $result[$covenant[$s]['id_covenant_category']]['covenant'][$covenant[$s]['id_covenant']] = array(
                        'id_covenant' => $covenant[$s]['id_covenant'],
                        'covenant_name' => $covenant[$s]['covenant_name'],
                        'created_by' => $covenant[$s]['created_by'],
                        'is_checked' => $covenant[$s]['is_checked']
                    );
                }
            }
        }

        if(isset($covenant[count($covenant)-1])){
            $result[$covenant[count($covenant)-1]['id_covenant_category']]['covenant'] = array_values($result[$covenant[count($covenant)-1]['id_covenant_category']]['covenant']);
        }

        //getting covenant category like object
        $result = array_values($result);
        $result = array('status'=>TRUE, 'message' => 'Success', 'data'=> $result);
        $this->response($result, REST_Controller::HTTP_OK);
    }



    public function projectCovenant_get()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('covenant_type_key', array('required'=> $this->lang->line('covenant_type_key_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $categories = $this->Company_model->getCovenantCategoryList($data);
        for($s=0;$s<count($categories);$s++)
        {
            $data['covenant_category_id'] = $categories[$s]['id_covenant_category'];
            $covenant[] = array(
                'id_covenant_category' => $categories[$s]['id_covenant_category'],
                'covenant_category' => $categories[$s]['covenant_category'],
                'covenant_type_id' => $categories[$s]['covenant_type_id'],
                'sector' => $categories[$s]['sector'],
                'company_id' => $categories[$s]['company_id'],
                'covenant' => $this->Crm_model->getSelectedProjectCovenants($data)
            );

        }

        /*$result = array();
        //$covenant_category_id = array_values(array_unique(array_map(function ($i) { return $i['id_covenant']; }, $covenant)));
        $covenant_category_id = $covenant_id = $project_covenant_id = array();
        for($s=0;$s<count($covenant);$s++)
        {
            if(!in_array($covenant[$s]['id_covenant_category'],$covenant_category_id))
            {
                //getting previous category covenants like objects
                if(isset($covenant[$s-1])){
                    $result[$covenant[$s-1]['id_covenant_category']]['covenant'] = array_values($result[$covenant[$s-1]['id_covenant_category']]['covenant']);
                }

                array_push($covenant_category_id,$covenant[$s]['id_covenant_category']);
                $result[$covenant[$s]['id_covenant_category']] = array(
                    'id_covenant_category' => $covenant[$s]['id_covenant_category'],
                    'covenant_category' => $covenant[$s]['covenant_category'],
                    'covenant' => array()
                );

                if($covenant[$s]['id_covenant']!='')
                {
                    $result[$covenant[$s]['id_covenant_category']]['covenant'][$covenant[$s]['id_covenant']] = array(
                        'id_covenant' => $covenant[$s]['id_covenant'],
                        'covenant_name' => $covenant[$s]['covenant_name'],
                        'created_by' => $covenant[$s]['created_by']
                    );
                }
            }
            else
            {
                if($covenant[$s]['id_covenant']!=''){
                    $result[$covenant[$s]['id_covenant_category']]['covenant'][$covenant[$s]['id_covenant']] = array(
                        'id_covenant' => $covenant[$s]['id_covenant'],
                        'covenant_name' => $covenant[$s]['covenant_name'],
                        'created_by' => $covenant[$s]['created_by']
                    );
                }
            }
        }

        if(isset($covenant[count($covenant)-1])){
            $result[$covenant[count($covenant)-1]['id_covenant_category']]['covenant'] = array_values($result[$covenant[count($covenant)-1]['id_covenant_category']]['covenant']);
        }

        //getting covenant category like object
        $result = array_values($result);*/
        $result = array('status'=>TRUE, 'message' => 'Success', 'data'=> $covenant);
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenant_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $data = $this->input->post();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $this->form_validator->add_rules('id_covenant_category', array('required'=> $this->lang->line('covenant_category_id_req')));
        //$this->form_validator->add_rules('covenant_ids', array('required'=> $this->lang->line('covenant_ids_req')));
        $this->form_validator->add_rules('created_by', array('required'=> $this->lang->line('created_by_req')));
        $this->form_validator->add_rules('covenant_type_key', array('required'=> $this->lang->line('covenant_type_key_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $covenant_ids = array();
        if(isset($data['covenant_ids']) && $data['covenant_ids']!='')
            $covenant_ids = explode(',',$data['covenant_ids']);



        $add = $update = $delete = array();
        $prev_data = $this->Crm_model->getProjectCovenantByProjectId(array('crm_project_id' => $data['crm_project_id'],'covenant_type_key' => $data['covenant_type_key'],'covenant_category_id' => $data['id_covenant_category']));
        if(!empty($prev_data))
            $prev_covenant_id = array_values(array_unique(array_map(function ($i) { return $i['covenant_id']; }, $prev_data)));
        else $prev_covenant_id = array();

        if(!empty($covenant_ids))
        for($s=0;$s<count($covenant_ids);$s++)
        {
            if(!in_array($covenant_ids[$s],$prev_covenant_id)){
                $add[] = array(
                    'project_id' => $data['crm_project_id'],
                    'covenant_id' => $covenant_ids[$s],
                    'created_by' => $data['created_by']
                );
            }
        }

        for($s=0;$s<count($prev_data);$s++)
        {
            if(!in_array($prev_data[$s]['covenant_id'],$covenant_ids)){
                $this->Crm_model->deleteProjectCovenant(array('covenant_id' => $prev_data[$s]['covenant_id'],'project_id' => $data['crm_project_id']));
            }
        }
        //echo "<pre>"; print_r($add); exit;
        if(!empty($add)){ $this->Crm_model->addProjectCovenant($add); }

        $result = array('status'=>TRUE, 'message' => $this->lang->line('covenant_update'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function covenant_delete()
    {
        $data = $this->input->get();
        if(empty($data)){
            $result = array('status'=>FALSE,'error'=>'Invalid Data','data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }
        $this->form_validator->add_rules('covenant_id', array('required'=> $this->lang->line('covenant_ids_req')));
        $this->form_validator->add_rules('crm_project_id', array('required'=> $this->lang->line('crm_project_id_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $this->Crm_model->deleteProjectCovenant(array('covenant_id' => $data['covenant_id'],'project_id' => $data['crm_project_id']));
        $result = array('status'=>TRUE, 'message' => $this->lang->line('covenant_delete'), 'data'=> '');
        $this->response($result, REST_Controller::HTTP_OK);
    }

    public function getChildNodes($data,$parent_id)
    {
        for($s=0;$s<count($data);$s++){

                if($data[$s]['parent_module_id']==$parent_id){
                    if( $data[$s]['sub_module']==0){
                        for($st=0;$st<count($data);$st++){
                            if($data[$s]['id_module']==$data[$st]['module_id']){
                                $this->order_data[] = array(
                                    $data[$st]['action_key'] => ($data[$st]['checked']==1)? true : false
                                );
                                //array_push($array,$value);
                            }
                        }
                    }
                    $this->getChildNodes($data,$data[$s]['id_module']);
                }

        }

        return $this->order_data;
    }

    public function moduleAccess_get()
    {
        $data = $this->input->get();
        $this->form_validator->add_rules('company_id', array('required'=> $this->lang->line('company_id_req')));
        $this->form_validator->add_rules('module_key', array('required'=> $this->lang->line('module_key_req')));
        $this->form_validator->add_rules('user_id', array('required'=> $this->lang->line('user_req')));
        $validated = $this->form_validator->validate($data);
        if($validated != 1)
        {
            $result = array('status'=>FALSE,'error'=>$validated,'data'=>'');
            $this->response($result, REST_Controller::HTTP_OK);
        }

        $applicationUserRole = $this->Company_model->getApplicationUserRole($data);
        //echo "<pre>"; print_r($applicationUserRole); exit;
        //$data['application_role_id'] = 0;
        if(!empty($applicationUserRole)){
            $data['application_role_id'] = $applicationUserRole[0]['application_role_id'];
        }
        else{

            $company_approval_role = $this->Company_model->getCompanyUsers(array('company_id' => $data['company_id'],'user_id' => $data['user_id']));
            //echo "<pre>"; print_r($company_approval_role); exit;
            $data['company_approval_role_id'] = $company_approval_role[0]['company_approval_role_id'];
            unset($data['user_id']);
            $applicationUserRole = $this->Company_model->getApplicationUserRole($data);
            //echo "<pre>"; print_r($applicationUserRole); exit;
            if(!empty($applicationUserRole))
                $data['application_role_id'] = $applicationUserRole[0]['application_role_id'];
            else{
                $data['application_role_id'] = 0;
            }
        }
        $result = $this->Company_model->getModules($data);
        $module_data = $this->order_data = array();
        for($s=0;$s<count($result);$s++)
        {
            if($result[$s]['module_key']==$data['module_key'])
            {
               if( $result[$s]['sub_module']==0){
                    for($st=0;$st<count($result);$st++){
                        if($result[$s]['id_module']==$result[$st]['module_id']){
                            $this->order_data[] = array(
                                $result[$st]['action_key'] => ($result[$st]['checked']==1)? true : false
                            );
                        }
                    }
                }
                $this->getChildNodes($result,$result[$s]['id_module']);
                break;
            }
        }

        //$module_data = array_values($module_data);

        $result = array('status'=>TRUE, 'message' =>'success', 'data'=>$this->order_data);
        $this->response($result, REST_Controller::HTTP_OK);
    }

}