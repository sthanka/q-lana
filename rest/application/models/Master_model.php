<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2015-12-01
 * Time: 04:09 PM
 */
class Master_model extends CI_Model
{
    public function checkSectorExist($sector){
        $this->db->where("sector_name",$sector['sector_name']);
        //$this->db->where("status!=",'inactive');
        if(isset($sector['id_sector']))
            $sectorId=$sector['id_sector'];
        if(isset($sectorId) && $sectorId>0){
            $this->db->where("id_sector !=",$sectorId);
        }
        $query=$this->db->get("sector");
        if($query->num_rows()>0){
            $result = true;
        }else {
            $result = false;
        }
        return $result;
    }
    public function getSectorCount()
    {
        $this->db->select('(case when c.sector_name is not null then c.sector_name else p.sector_name end) as parent,(case when c.sector_name is not null then p.sector_name else null end) as sector_name');
        $this->db->from('sector p');
        $this->db->join('sector c','c.id_sector = p.parent_sector_id','left');

        $query = $this->db->get();
        return $query->num_rows();

    }
    public function getSectorsList($data)
    {
        /*echo "<pre>"; print_r($data);*/
        $this->db->select('s.id_sector,s1.id_sector as parent_id,s.sector_name,s1.sector_name as parent');
        $this->db->from('sector s');
        $this->db->join('sector s1','s.parent_sector_id=s1.id_sector','left');
        $this->db->order_by('s.sector_name','asc');

       /* $this->db->select('s1.id_sector,s.id_sector as parent_id,s1.sector_name,s.sector_name as parent');
        $this->db->from('sector s');
        $this->db->join('sector s1','s.id_sector=s1.parent_sector_id','left');*/

        if(isset($data['parent_sector_id']))
            $this->db->where('s.parent_sector_id',$data['parent_sector_id']);

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getSectorSubSectorList($data)
    {
        /*echo "<pre>"; print_r($data);*/
        /*$this->db->select('s.id_sector,s1.id_sector as parent_id,s.sector_name,s1.sector_name as parent');
        $this->db->from('sector s');
        $this->db->join('sector s1','s.parent_sector_id=s1.id_sector','left');
        $this->db->order_by('s.sector_name','asc');*/

        $this->db->select('p.id_sector,(case when c.sector_name is not null then c.sector_name else p.sector_name end) as parent,(case when c.sector_name is not null then p.sector_name else null end) as sector_name');
        $this->db->from('sector p');
        $this->db->join('sector c','c.id_sector = p.parent_sector_id','left');
        //$this->db->where('s.parent_sector_id',0);

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }
    public function getSector($data)
    {
        $this->db->select('s.id_sector,s1.id_sector as parent_sector_id,s.sector_name,s1.sector_name as parent');
        $this->db->from('sector s');
        $this->db->join('sector s1','s.parent_sector_id=s1.id_sector','left');
        $this->db->where('s.id_sector',$data['id_sector']);
        $query = $this->db->get();
        return $query->row();
    }

    public function getSubSector($data)
    {
        $this->db->select('*');
        $this->db->from('sector');
        $this->db->where('parent_sector_id',$data['id_sector']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function insertSector($data)
    {
        $sectorParent=$data['parent_sector_id'];
        if(isset($sectorParent))
            $sectorParent=$sectorParent;
        else
            $sectorParent='';
        $insert=array(
            'sector_name'=>$data['sector_name'],
            'parent_sector_id'=>$sectorParent,
            'created_date_time'=>date('Y-m-d'),
            //'status'=>'active'
        );
        $this->db->insert('sector',$insert);
        //return 1;
        return $this->db->insert_id();
    }
    public function updateSector($data)
    {
        $sectorParent=$data['parent_sector_id'];
        if(isset($sectorParent))
            $sectorParent=$sectorParent;
        else
            $sectorParent='';
        $update=array(
            'sector_name'=>$data['sector_name'],
            'parent_sector_id'=>$sectorParent
        );
        $this->db->where('id_sector', $data['id_sector']);
        $this->db->update('sector',$update);
        return 1;
    }
    /*public function deleteSector($data)
    {
        $delete=array(
            'status' => 'inactive'
        );
        $this->db->where('id_sector',$data['id_sector']);
        $this->db->update('sector',$delete);
    }*/
    // countries
    public function checkCountryExist($country){
        $this->db->where("country_name",$country['country_name']);
        //$this->db->where("status!=",'inactive');
        if(isset($country['id_country']))
            $countryId=$country['id_country'];
        if(isset($countryId) && $countryId>0){
            $this->db->where("id_country !=",$countryId);
        }
        $query=$this->db->get("country");
        if($query->num_rows()>0){
            $result = true;
        }else {
            $result = false;
        }
        return $result;
    }
    public function getCountriesCount()
    {
        $this->db->select('id_country,country_name');
        $query = $this->db->get('country');
        return $query->num_rows();
    }
    public function getCountriesList($data)
    {
        $this->db->select('id_country,country_name,country_code');
        $this->db->order_by('country_name','asc');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get('country');
        return $query->result_array();
    }
    public function getCountry($data)
    {
        $this->db->select('id_country,country_name,country_code');
        $this->db->where('id_country',$data['id_country']);
        $query = $this->db->get('country');
        return $query->row();
    }
    public function insertCountry($data)
    {
        $insert=array(
            'country_name'=>$data['country_name'],
            'country_code'=>$data['country_code'],
            //'status' =>'active'
        );
        $this->db->insert('country',$insert);
        return $this->db->insert_id();
    }
    public function updateCountry($data)
    {
        $update=array(
            'country_name'=>$data['country_name'],
            'country_code'=>$data['country_code']
        );
        $this->db->where('id_country', $data['id_country']);
        $this->db->update('country',$update);
        return 1;
    }
    /*public function deleteCountry($data)
    {
        $delete=array(
            'status' => 'inactive'
        );
        $this->db->where('id_country',$data['id_country']);
        $this->db->update('country',$delete);
    }*/
    public function checkBranchExist($branch)
    {
        $this->db->where("branch_type_name",$branch['branch_type_name']);
        //$this->db->where("status!=",'inactive');
        if(isset($branch['id_branch_type']))
            $branchId=$branch['id_branch_type'];
        if(isset($branchId) && $branchId>0){
            $this->db->where("id_branch_type !=",$branchId);
        }
        $query=$this->db->get("branch_type");
        if($query->num_rows()>0){
            $result = true;
        }else {
            $result = false;
        }
        return $result;
    }
    public function getBranchesCount($data)
    {
        $this->db->select('id_branch_type,branch_type_name,branch_type_code');
        if(isset($data['company_id']))
            $this->db->where('company_id',$data['company_id']);
        $query = $this->db->get('branch_type');
        return $query->num_rows();
    }
    public function getBranchesList($data)
    {
        $this->db->select('id_branch_type,branch_type_name,branch_type_code');

        if(isset($data['company_id']))
            $this->db->where('company_id',$data['company_id']);

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get('branch_type');
        return $query->result_array();
    }
    public function getBranchType($data)
    {
        $this->db->select('id_branch_type,branch_type_name,branch_type_code,description');
        $this->db->where('id_branch_type',$data['id_branch_type']);
        $query = $this->db->get('branch_type');
        return $query->row();
    }
    public function insertBranch($data)
    {
        if(!isset($data['description'])){ $data['description']=''; }
        $insert=array(
            'branch_type_name'=>$data['branch_type_name'],
            'branch_type_code'=>$data['branch_type_code'],
            'description'=>$data['description']
            //'status' =>'active'
        );
        $this->db->insert('branch_type',$insert);
        return $this->db->insert_id();
    }
    public function updateBranch($data)
    {
        $update=array(
            'branch_type_name'=>$data['branch_type_name'],
            'branch_type_code'=>$data['branch_type_code'],
            'description'=>$data['description']
        );
        $this->db->where('id_branch_type', $data['id_branch_type']);
        $this->db->update('branch_type',$update);
        return 1;
    }
    /*public function deleteBranch($data)
    {
        $delete=array(
            'status' => 'inactive'
        );
        $this->db->where('id_branch_type',$data['id_branch']);
        $this->db->update('branch_type',$delete);
    }*/
    //plans
    public function getPlansCount()
    {
        $query = $this->db->get('plan');
        return $query->num_rows();
    }
    public function getPlansList($data)
    {
        $this->db->order_by('plan_name','asc');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get('plan');
        return $query->result_array();
    }
    public function getPlanDetails($id)
    {
        $query = $this->db->get_where('plan',array('id_plan' => $id));
        return $query->row();
    }
    public function insertPlan($data)
    {
        $insert=array(
            'plan_name' =>$data['plan_name'],
            'no_of_loans' => $data['no_of_loans'],
            'price_per_loan' => $data['price_per_loan'],
            'no_of_users' => $data['no_of_users'],
            'total_disk_space' => $data['total_disk_space']
        );
        $this->db->insert('plan',$insert);
        return $this->db->insert_id();
    }
    public function updatePlan($data)
    {
        $update=array(
            'plan_name' =>$data['plan_name'],
            'no_of_loans' => $data['no_of_loans'],
            'price_per_loan' => $data['price_per_loan'],
            'no_of_users' => $data['no_of_users'],
            'total_disk_space' => $data['total_disk_space']
        );
        $this->db->where('id_plan',$data['id_plan']);
        $this->db->update('plan',$update);
    }
    public function deletePlan($data)
    {
        $delete=array(
            'status' => 'inactive'
        );
        $this->db->where('id_plan',$data['id_plan']);
        $this->db->update('plan',$delete);
    }

    //risk
    public function checkRiskExist($risk){
        $this->db->where("risk_name",$risk['risk_name']);
        //$this->db->where("status!=",'inactive');
        if(isset($risk['id_risk_type']))
            $riskId=$risk['id_risk_type'];
        if(isset($riskId) && $riskId>0){
            $this->db->where("id_risk_type !=",$riskId);
        }
        $query=$this->db->get("risk_type");
        if($query->num_rows()>0){
            $result = true;
        }else {
            $result = false;
        }
        return $result;
    }
    public function getRiskDetails($id)
    {
        $query = $this->db->get_where('risk_type',array('id_risk_type' => $id));
        return $query->row();
    }
    public function getRiskCount()
    {
        $query = $this->db->get('risk_type');
        return $query->num_rows();
    }
    public function getRiskList($data)
    {
        $this->db->order_by('risk_name','asc');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get('risk_type');
        return $query->result_array();
    }
    public function insertRisk($data){
        $insert=array(
            'risk_name' =>$data['risk_name'],
            'risk_description' => $data['risk_description']
        );
        $this->db->insert('risk_type',$insert);
        return $this->db->insert_id();
    }
    public function updateRisk($data){
        $update=array(
            'risk_name' =>$data['risk_name'],
            'risk_description' => $data['risk_description']
        );
        $this->db->where('id_risk_type',$data['id_risk_type']);
        $this->db->update('risk_type',$update);
    }

    //social
    public function checkSocialExist($social){
        $this->db->where("business_social_network",$social['business_social_network']);
        //$this->db->where("status!=",'inactive');
        if(isset($social['id_business_social_network_type']))
            $socialId=$social['id_business_social_network_type'];
        if(isset($socialId) && $socialId>0){
            $this->db->where("id_business_social_network_type !=",$socialId);
        }
        $query=$this->db->get("business_social_network_type");
        if($query->num_rows()>0){
            $result = true;
        }else {
            $result = false;
        }
        return $result;
    }
    public function getSocialDetails($id)
    {
        $query = $this->db->get_where('business_social_network_type',array('id_business_social_network_type' => $id));
        return $query->row();
    }
    public function getSocialCount()
    {
        $query = $this->db->get('business_social_network_type');
        return $query->num_rows();
    }
    public function getSocialList($data)
    {
        $this->db->order_by('business_social_network','asc');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get('business_social_network_type');
        return $query->result_array();
    }
    public function insertSocial($data){
        $insert=array(
            'business_social_network' =>$data['business_social_network'],
            'business_social_network_description' => $data['business_social_network_description']
        );
        $this->db->insert('business_social_network_type',$insert);
        return $this->db->insert_id();
    }
    public function updateSocial($data){
        $update=array(
            'business_social_network' =>$data['business_social_network'],
            'business_social_network_description' => $data['business_social_network_description']
        );
        $this->db->where('id_business_social_network_type',$data['id_business_social_network_type']);
        $this->db->update('business_social_network_type',$update);
    }

    //contact
    public function checkContactExist($contact){
        $this->db->where("business_contact",$contact['business_contact']);
        //$this->db->where("status!=",'inactive');
        if(isset($contact['id_business_contact_type']))
            $contactId=$contact['id_business_contact_type'];
        if(isset($contactId) && $contactId>0){
            $this->db->where("id_business_contact_type !=",$contactId);
        }
        $query=$this->db->get("business_contact_type");
        if($query->num_rows()>0){
            $result = true;
        }else {
            $result = false;
        }
        return $result;
    }
    public function getContactDetails($id)
    {
        $query = $this->db->get_where('business_contact_type',array('id_business_contact_type' => $id));
        return $query->row();
    }
    public function getContactCount()
    {
        $query = $this->db->get('business_contact_type');
        return $query->num_rows();
    }
    public function getContactList($data)
    {
        $this->db->order_by('business_contact','asc');
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get('business_contact_type');
        return $query->result_array();
    }
    public function insertContact($data){
        $insert=array(
            'business_contact' =>$data['business_contact'],
            'business_contact_description' => $data['business_contact_description']
        );
        $this->db->insert('business_contact_type',$insert);
        return $this->db->insert_id();
    }
    public function updateContact($data){
        $update=array(
            'business_contact' =>$data['business_contact'],
            'business_contact_description' => $data['business_contact_description']
        );
        $this->db->where('id_business_contact_type',$data['id_business_contact_type']);
        $this->db->update('business_contact_type',$update);
    }


    public function getBankCategoriesCount()
    {
        $query = $this->db->get('bank_category');
        return $query->num_rows();
    }
    public function getBankCategoryList($data)
    {
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get('bank_category');
        return $query->result_array();
    }
    public function getBankCategoryDetails($id)
    {
        $query = $this->db->get_where('bank_category',array('id_bank_category' => $id));
        return $query->row();
    }
    public function insertBankCategory($data)
    {
        if(!isset($data['bank_category_code'])){ $data['bank_category_code']=''; }
        $insert=array(
            'bank_category_name' =>$data['bank_category_name'],
            'bank_category_code' => $data['bank_category_code']
        );
        $this->db->insert('bank_category',$insert);
        return $this->db->insert_id();
    }
    public function updateBankCategory($data)
    {
        $update=array(
            'bank_category_name' =>$data['bank_category_name'],
            'bank_category_code' => $data['bank_category_code']
        );
        $this->db->where('id_bank_category',$data['id_bank_category']);
        $this->db->update('bank_category',$update);
    }
    public function deleteBankCategory($data)
    {
        $this->db->where('id_bank_category', $data['id']);
        $this->db->delete('bank_category');
    }

    // Approval roles
    public function getApprovalRolesCount($data)
    {
        $this->db->select('id_approval_role,approval_name,approval_role_code');
        if(isset($data['company_id']))
            $this->db->where('company_id',$data['company_id']);
        $query = $this->db->get('approval_role');
        return $query->num_rows();
    }
    public function getApprovalRoles($data)
    {
        $this->db->select('id_approval_role,approval_name,approval_role_code');
        if(isset($data['company_id']))
            $this->db->where('company_id',$data['company_id']);

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get('approval_role');
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }
    public function getApprovalRole($data)
    {
        $this->db->select('id_approval_role,approval_name,description,approval_role_code');
        $this->db->where('id_approval_role',$data['id_approval_role']);
        $query = $this->db->get('approval_role');
        return $query->row();
    }
    public function insertApprovalRole($data)
    {
        if(!isset($data['approval_role_code'])){ $data['approval_role_code']=''; }
        if(!isset($data['description'])){ $data['description']=''; }

        $insert=array(
            'approval_name' =>$data['approval_name'],
            'approval_role_code' =>$data['approval_role_code'],
            'description'=>$data['description']
        );
        $this->db->insert('approval_role',$insert);
        return $this->db->insert_id();
    }
    public function updateApprovalRole($data)
    {
        $update=array(
            'approval_name' =>$data['approval_name'],
            'approval_role_code' =>$data['approval_role_code'],
            'description'=>$data['description']
        );
        $this->db->where('id_approval_role',$data['id_approval_role']);
        $this->db->update('approval_role',$update);
    }
    public function deleteApprovalRole($data)
    {
        $this->db->where('id_approval_role', $data['id_approval_role']);
        $this->db->delete('approval_role');
    }

    public function getCurrency($data)
    {
        $this->db->select('*');
        $this->db->from('currency c');
        $this->db->join('country cn','c.country_id=cn.id_country','left');
        if(isset($data['currency_id']) && $data['currency_id']!='')
            $this->db->where('c.id_currency!=',$data['currency_id']);
        if(isset($data['id_currency']) && $data['id_currency']!='')
            $this->db->where('c.id_currency',$data['id_currency']);
        if(isset($data['country_id']))
            $this->db->where('c.country_id',$data['country_id']);

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        $query = $this->db->get();
        //echo $this->db->last_query();  exit;
        if(isset($data['id_currency']) && $data['id_currency']!='')
            return $query->row();
        else
            return $query->result_array();
    }

    public function getTotalCurrency($data)
    {
        $this->db->select('count(*) as total_records');
        $this->db->from('currency');
        if(isset($data['currency_id']) && $data['currency_id']!='')
            $this->db->where('id_currency!=',$data['currency_id']);
        if(isset($data['id_currency']) && $data['id_currency']!='')
            $this->db->where('id_currency',$data['id_currency']);
        if(isset($data['country_id']))
            $this->db->where('country_id',$data['country_id']);


        $query = $this->db->get();
        //echo $this->db->last_query();  exit;
        return $query->result_array();
    }

    public function getProjectLoanType()
    {
        $this->db->select('*');
        $this->db->from('project_loan_type');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectPaymentType()
    {
        $this->db->select('*');
        $this->db->from('project_payment_type');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCurrency($data)
    {
        $this->db->insert('currency',$data);
        return $this->db->insert_id();
    }

    public function updateCurrency($data)
    {
        $this->db->where('id_currency', $data['id_currency']);
        $this->db->update('currency',$data);
    }
}
