<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    public function getCustomersByCountry()
    {
        $this->db->select('count(*) as total_customers,cn.id_country,cn.country_name');
        $this->db->join('country cn','cn.id_country=c.country_id','left');
        $this->db->from('company c');
        $this->db->group_by('c.country_id');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCustomersByCategory()
    {
        $this->db->select('count(*) as total_companies,bc.id_bank_category,bc.bank_category_name,bc.bank_category_code');
        $this->db->from('company c');
        $this->db->join('bank_category bc','bc.id_bank_category=c.bank_category_id','left');
        $this->db->group_by('c.bank_category_id');
        $query = $this->db->get();
        return $query->result_array();
    }
}