<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Company_model extends CI_Model
{


    public $branch_users = [];
    public $reporting_branch_type = [];
    public $reporting_user = [];
    public $reporting_branch = [];
    public $reporting_company_approval_roles = [];

    public function __construct(){
        parent::__construct();
        $this->load->model('Mcommon');
    }

    function next_result()
    {
        if (is_object($this->conn_id))
        {
            return mysqli_next_result($this->conn_id);
        }
    }

    public function companyList($data)
    {
        $this->db->select('c.*,cn.country_name,cn.country_code,p.*,u.user_role_id,u.first_name,u.last_name,u.email_id,u.phone_number,u.address,u.city,u.state,u.profile_image,u.user_status');
        $this->db->from('company c');
        $this->db->join('user u', 'u.id_user = c.user_id', 'left');
        $this->db->join('plan p', 'p.id_plan = c.plan_id', 'left');
        $this->db->join('country cn', 'cn.id_country = c.country_id', 'left');
        if(isset($data['from']) && $data['from']!='' && isset($data['to']) && $data['to']!='')
            $this->db->limit($data['from'],$data['to']);
        $this->db->order_by('c.id_company','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function totalCompanyList()
    {
        $this->db->select('count(*)');
        $this->db->from('company');
        $this->db->join('user', 'user.id_user = company.user_id', 'left');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompany($data)
    {
        $this->db->select('c.*,cc.currency_code,cc.currency_symbol,cn.id_country,cn.country_name,cn.country_code,u.id_user,u.user_role_id,u.first_name,u.last_name,u.email_id,u.phone_number,u.address,u.city,u.state,u.country_id,u.profile_image,u.user_status');
        $this->db->from('company c');
        $this->db->join('user u', 'u.id_user = c.user_id', 'left');
        $this->db->join('country cn', 'cn.id_country = u.country_id', 'left');//no_of_loans
        $this->db->join('currency cc', 'cc.id_currency = c.currency_id', 'left');
        if(isset($data['id']) && $data['id']!=0)
            $this->db->where('c.id_company!=',$data['id']);

        if(isset($data['company_name']))
            $this->db->where('c.company_name',$data['company_name']);

        if(isset($data['id_company']))
            $this->db->where('c.id_company',$data['id_company']);

        $query = $this->db->get();

        return $query->row();
    }

    public function getCompanyById($id)
    {
        $this->db->select('c.*,cc.currency_code,cc.currency_symbol,cn.id_country,cn.country_name,cn.country_code,u.id_user,u.user_role_id,u.first_name,u.last_name,u.email_id,u.phone_number,u.address,u.city,u.state,u.country_id,u.profile_image,u.user_status,(select count(*) from company_user where company_id='.$id.' and company_approval_role_id!="") as users,(select count(*) from company_branch where company_id='.$id.') as branches,(select count(*) from company_approval_role where company_id='.$id.') as approval_roles,(select count(*) from company_branch_type where company_id='.$id.') as branch_types,cn1.id_country as company_country_id');
        $this->db->from('company c');
        $this->db->join('user u', 'u.id_user = c.user_id', 'left');
        $this->db->join('country cn', 'cn.id_country = u.country_id', 'left');//no_of_loans
        $this->db->join('country cn1', 'cn1.id_country = c.country_id', 'left');//no_of_loans
        $this->db->join('currency cc', 'cc.id_currency = c.currency_id', 'left');

        $this->db->where('c.id_company',$id);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->row();
    }

    public function createCompany($data)
    {
        $this->db->insert('company', $data);
        return $this->db->insert_id();
    }

    public function updateCompanyInfo($data)
    {
        $this->db->where('id_company', $data['id_company']);
        $this->db->update('company', $data);
        return 1;
    }

    public function updateUser($data)
    {
        $this->db->where('id_user', $data['id_user']);
        $this->db->update('user', $data);
        return 1;
    }

    public function updateCompany($data)
    {
        $update = array(
            'company_name' => addslashes($data['company_name']),
            'company_address' => addslashes($data['company_address']),
            'company_city' => addslashes($data['company_city']),
            'company_state' => addslashes($data['company_state']),
            'user_id' => $data['user_id'],
            'status' => $data['status']
        );

        $this->db->where('id', $data['id']);
        $this->db->update('company', $update);
        return 1;
    }

    public function updateCompanyData($data,$id)
    {
        $this->db->where('id_company', $id);
        $this->db->update('company', $data);
        return 1;
    }

    public function deleteCompany($data)
    {
        $update = array(
            'status' => 'inactive'
        );
        $this->db->where('id_company', $data['id']);
        $this->db->update('company', $update);
        return 1;
    }

    public function createCompanyUser($data)
    {
        $this->db->insert('company_user', $data);
        return $this->db->insert_id();
    }

    public function updateCompanyUser($data)
    {
        $id = $data['id_company_user'];
        unset($data['id_company_user']);
        $this->db->where('id_company_user', $id);
        $this->db->update('company_user', $data);
        return 1;
    }

    public function getCompanyUsers($data)
    {
        //echo "<pre>"; print_r($data);
        $this->db->select('u.user_role_id,u.id_user,u.first_name,u.last_name,u.email_id,u.phone_number,u.address,u.city,u.state,c.country_name,u.zip_code,u.profile_image,u.created_date_time,u.user_status,cu.id_company_user,cu.company_id,cu.branch_id,cu.company_approval_role_id,cu.reporting_user_id,cb.legal_name as branch_name,cb.branch_type_id,ar.approval_name as role_name');
        $this->db->from('company_user cu');
        $this->db->join('user u', 'u.id_user = cu.user_id', 'left');
        $this->db->join('company_branch cb', 'cb.id_branch = cu.branch_id', 'left');
        $this->db->join('company_approval_role car', 'car.id_company_approval_role = cu.company_approval_role_id', 'left');
        $this->db->join('approval_role ar', 'ar.id_approval_role = car.approval_role_id', 'left');
        $this->db->join('country c', 'u.country_id = c.id_country', 'left');

        if(isset($data['current_user_id']))
            $this->db->where('cu.user_id NOT IN ('.$data['current_user_id'].')');

        if(isset($data['company_id']))
            $this->db->where('cu.company_id', $data['company_id']);

        if(isset($data['user_id']))
            $this->db->where('cu.user_id', $data['user_id']);

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        else if(isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit']);
        //$this->db->where(array('cu.company_id' => $data['company_id'],'u.user_role_id' => 3, 'user_status' => 'active'));
        if(!isset($data['user_type']))
            $this->db->where(array('cu.company_id' => $data['company_id'],'u.user_role_id' => 3));

        if(isset($data['allowed_approval_role_ids']) && $data['allowed_approval_role_ids']!=0 && $data['allowed_approval_role_ids']!=''){
            $this->db->where('cu.company_approval_role_id IN '.$data['allowed_approval_role_ids'], NULL, FALSE);
        }

        if(isset($data['allowed_approval_role_id']) && $data['allowed_approval_role_id']!='' && $data['allowed_approval_role_id']!=0){
            $this->db->where('cu.company_approval_role_id',$data['allowed_approval_role_id']);
        }

        if(isset($data['allowed_branch_ids']) && !empty($data['allowed_branch_ids'])){
           $this->db->where_in('cb.id_branch ',$data['allowed_branch_ids']);
        }

        if(isset($data['branch_id']))
            $this->db->where('cb.id_branch ',$data['branch_id']);

        if(isset($data['crm_project_id']) && $data['crm_project_id']!='')
            $this->db->where('id_company_user NOT IN (SELECT team_member_id FROM project_team where crm_project_id='.$data['crm_project_id'].')', NULL, FALSE);

        if(isset($data['project_id']) && $data['project_id']!='')
            $this->db->where('id_company_user NOT IN (SELECT invitation_id FROM meeting_invitation i left join meeting m on m.id_meeting=i.meeting_id where m.project_id='.$data['project_id'].')', NULL, FALSE);

        $this->db->order_by('cu.id_company_user','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCompanyUsersByRole($data)
    {

        $this->db->select('u.user_role_id,u.id_user,u.first_name,u.last_name,u.email_id,u.phone_number,u.address,u.city,u.state,c.country_name,u.zip_code,u.profile_image,u.created_date_time,u.user_status,cu.id_company_user,cu.company_id,cu.branch_id,cu.company_approval_role_id,cu.reporting_user_id,cb.legal_name as branch_name,cb.branch_type_id,ar.approval_name as role_name');
        $this->db->from('company_user cu');
        $this->db->join('user u', 'u.id_user = cu.user_id', 'left');
        $this->db->join('company_branch cb', 'cb.id_branch = cu.branch_id', 'left');
        $this->db->join('company_approval_role car', 'car.id_company_approval_role = cu.company_approval_role_id', 'left');
        $this->db->join('approval_role ar', 'ar.id_approval_role = car.approval_role_id', 'left');
        $this->db->join('country c', 'u.country_id = c.id_country', 'left');

        if(isset($data['company_id']))
            $this->db->where('cu.company_id', $data['company_id']);

        if(isset($data['id_branch']))
            $this->db->where('cb.id_branch', $data['id_branch']);

        if(isset($data['company_approval_role_id'])){
            $this->db->where('cu.company_approval_role_id', $data['company_approval_role_id']);
        }

        $this->db->order_by('cu.id_company_user','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTotalCompanyUsers($data)
    {
        /*$this->db->select('count(*) as total_records');
        $this->db->from('company_user cu');
        $this->db->join('user u', 'u.id_user = cu.user_id', 'left');
        $this->db->join('company_branch cb', 'cb.id_branch = cu.branch_id', 'left');
        $this->db->join('company_approval_role car', 'car.id_company_approval_role = cu.company_approval_role_id', 'left');
        $this->db->join('approval_role ar', 'ar.id_approval_role = car.approval_role_id', 'left');
        $this->db->where(array('cu.company_id' => $data['company_id'],'u.user_role_id' => 3, 'user_status' => 'active'));
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->row();*/

        $this->db->select('count(*) as total_records');
        $this->db->from('company_user cu');
        $this->db->join('user u', 'u.id_user = cu.user_id', 'left');
        $this->db->join('company_branch cb', 'cb.id_branch = cu.branch_id', 'left');
        $this->db->join('company_approval_role car', 'car.id_company_approval_role = cu.company_approval_role_id', 'left');
        $this->db->join('approval_role ar', 'ar.id_approval_role = car.approval_role_id', 'left');
        $this->db->join('country c', 'u.country_id = c.id_country', 'left');

        if(isset($data['current_user_id']))
            $this->db->where('cu.user_id NOT IN ('.$data['current_user_id'].')');

        if(isset($data['company_id']))
            $this->db->where('cu.company_id', $data['company_id']);

        if(isset($data['user_id']))
            $this->db->where('cu.user_id', $data['user_id']);

        if(!isset($data['user_type']))
            $this->db->where(array('cu.company_id' => $data['company_id'],'u.user_role_id' => 3));

        if(isset($data['allowed_approval_role_ids']) && $data['allowed_approval_role_ids']!=0 && $data['allowed_approval_role_ids']!=''){ echo "456123";
            $this->db->where('cu.company_approval_role_id IN '.$data['allowed_approval_role_ids'], NULL, FALSE);
        }

        if(isset($data['allowed_approval_role_id']) && $data['allowed_approval_role_id']!='' && $data['allowed_approval_role_id']!=0){
            $this->db->where('cu.company_approval_role_id',$data['allowed_approval_role_id']);
        }

        if(isset($data['allowed_branch_ids']) && !empty($data['allowed_branch_ids'])){
            $this->db->where_in('cb.id_branch ',$data['allowed_branch_ids']);
        }

        if(isset($data['branch_id']))
            $this->db->where('cb.id_branch ',$data['branch_id']);

        if(isset($data['crm_project_id']) && $data['crm_project_id']!='')
            $this->db->where('id_company_user NOT IN (SELECT team_member_id FROM project_team where crm_project_id='.$data['crm_project_id'].')', NULL, FALSE);

        if(isset($data['project_id']) && $data['project_id']!='')
            $this->db->where('id_company_user NOT IN (SELECT invitation_id FROM meeting_invitation i left join meeting m on m.id_meeting=i.meeting_id where m.project_id='.$data['project_id'].')', NULL, FALSE);

        $this->db->order_by('cu.id_company_user','DESC');
        $query = $this->db->get();
        return $query->row();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCompanyUser($id)
    {
        $this->db->select('u.id_user,u.first_name,u.last_name,u.email_id,u.phone_number,u.address,u.city,u.state,u.country_id,u.profile_image,u.user_status,u.zip_code,cu.id_company_user,cu.company_id,cu.company_approval_role_id as user_role_id,cu.branch_id,cu.reporting_user_id,CONCAT(us.first_name," ",us.last_name) as reporting_user_name,cb.id_branch,cb.branch_type_id,cb.legal_name as branch_name,car.id_company_approval_role,ar.approval_name');
        $this->db->from('company_user cu');
        $this->db->join('company_branch cb', 'cu.branch_id = cb.id_branch', 'left');
        $this->db->join('company_approval_role car', 'cu.company_approval_role_id = car.id_company_approval_role', 'left');
        $this->db->join('approval_role ar', 'car.approval_role_id = ar.id_approval_role', 'left');
        $this->db->join('user u', 'u.id_user = cu.user_id', 'left');
        $this->db->join('user us', 'us.id_user = cu.reporting_user_id', 'left');
        $this->db->where('u.id_user', $id);
        $query = $this->db->get();
        return $query->row();
    }

    public function getReportingUser($user_id)
    {
        $this->db->select('CONCAT(u.first_name,u.last_name) as reporting_user_name,cb.legal_name as reporting_branch_name,ar.approval_name as reporting_designation');
        $this->db->from('company_user cu');
        $this->db->join('user u', 'u.id_user = cu.user_id', 'left');
        $this->db->join('company_branch cb', 'cb.id_branch = cu.branch_id', 'left');
        $this->db->join('company_approval_role car', 'car.id_company_approval_role = cu.company_approval_role_id', 'left');
        $this->db->join('approval_role ar', 'ar.id_approval_role = car.approval_role_id', 'left');
        $this->db->where('cu.user_id', $user_id);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->row();
    }

    public function getBranchUsersByBrachId($branchId)
    {
        $this->db->select('CONCAT(u.first_name," ",u.last_name) as user_name,ar.approval_name,cb.reporting_branch_id,u.profile_image,u.email_id,u.phone_number,cb.legal_name');
        $this->db->from('company_user cu');
        $this->db->join('user u','cu.user_id=u.id_user','left');
        $this->db->join('company_branch cb','cb.id_branch=cu.branch_id','left');
        $this->db->join('company_approval_role car','car.id_company_approval_role=cu.company_approval_role_id','left');
        $this->db->join('approval_role ar','ar.id_approval_role=car.approval_role_id','left');
        $this->db->where('cu.branch_id',$branchId);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getBranchTypeByCompanyId($company_id,$branch_type=0)
    {
        $this->db->select('b.branch_type_name,b.branch_type_code,c.branch_type_id,c.company_id,c.branch_type_id,c.reporting_branch_type_id');
        $this->db->from('company_branch_type c');
        $this->db->join('branch_type b', 'c.branch_type_id=b.id_branch_type', 'left');
        $this->db->where('c.company_id', $company_id);
        if($branch_type)
            $this->db->where('c.branch_type_id', $branch_type);
        $this->db->order_by('c.id_company_branch_type','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getLastNodeOfCompanyBranchType($company_id,$data)
    {
        $this->db->select('*');
        $this->db->from('company_branch_type');
        $this->db->where('company_id',$company_id);
        if(!empty($data))
            $this->db->where('branch_type_id NOT IN '.$data, NULL, FALSE);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getLastNodeOfCompanyApprovalRole($company_id,$data)
    {
        $this->db->select('*');
        $this->db->from('company_approval_role');
        $this->db->where('company_id',$company_id);
        if(!empty($data))
            $this->db->where('approval_role_id NOT IN '.$data, NULL, FALSE);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCompanyBranchesBybranchId($branch_type_id,$company_id)
    {
        $this->db->select('cb.*,cb1.legal_name as reporting_branch_name');
        $this->db->from('company_branch cb');
        $this->db->join('company_branch cb1','cb1.id_branch=cb.reporting_branch_id','left');
        $this->db->join('country ct', 'cb.country_id=ct.id_country', 'left');
        $this->db->where(array('cb.branch_type_id' => $branch_type_id,'cb.company_id' => $company_id));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getBranchDetails($data)
    {
        $this->db->select('cb.*,cb1.legal_name as reporting_branch_name,ct.country_name,bt.branch_type_name');
        $this->db->from('company_branch cb');
        $this->db->join('branch_type bt','cb.branch_type_id=bt.id_branch_type','left');
        $this->db->join('company_branch cb1','cb1.id_branch=cb.reporting_branch_id','left');
        $this->db->join('country ct', 'cb.country_id=ct.id_country', 'left');
        if(isset($data['company_id']))
            $this->db->where('cb.company_id', $data['company_id']);

        if(isset($data['branch_id']))
            $this->db->where('cb.id_branch', $data['branch_id']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getBranchName($data)
    {
        $this->db->select('id_branch,legal_name');
        $this->db->where('branch_status',1);
        if(isset($data['company_id']))
            $this->db->where('company_id',$data['company_id']);
        $query=$this->db->get('company_branch');
        //echo $this->db->last_query(); exit;
        return $query->result();
    }
    public function getApprovalRole($data)
    {
        $this->db->select('c.id_company_approval_role,a.approval_name');
        $this->db->from('company_approval_role c');
        $this->db->join('approval_role a', 'a.id_approval_role=c.id_company_approval_role', 'left');
        if(isset($data['company_id']))
            $this->db->where('c.company_id',$data['company_id']);
        $query=$this->db->get();
        return $query->result();
    }

    public function getCompanyBranchType($data)
    {
        //echo "<pre>"; print_r($data); exit;
        $this->db->select('b.*');
        $this->db->from('branch_type b');
        $this->db->where('b.company_id IN ('.$data['company_id'].',0)');
        $query=$this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result();
    }

    public function getPrimaryCompanyBranchType($data)
    {
        $this->db->select('*');
        $this->db->from('company_branch_type');
        $this->db->where('company_id', $data['company_id']);
        $this->db->where('reporting_branch_type_id','0');
        $query=$this->db->get();
        return $query->result_array();
    }

    public function getPrimaryCompanyApprovalRole($data)
    {
        $this->db->select('*');
        $this->db->from('company_approval_role');
        $this->db->where('company_id', $data['company_id']);
        $this->db->where('reporting_role_id','0');
        $query=$this->db->get();
        return $query->result_array();
    }

    public function getBranchesByCompanyId($id)
    {
        $this->db->select('bt.branch_type_name,c.*,cb.*,cbt.*');
        $this->db->from('company c');
        $this->db->join('company_branch cb', 'c.id_company=cb.company_id', 'left');
        $this->db->join('company_branch_type cbt', 'cb.branch_type_id=cbt.branch_type_id', 'left');
        $this->db->join('branch_type bt', 'cb.branch_type_id=bt.id_branch_type', 'left');
        $this->db->where('c.id_company', $id);
        $this->db->order_by('cbt.id_company_branch_type','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyBranchTypeByBranchId($company_id,$branch_type_id)
    {
        $query = $this->db->get_where('company_branch_type',array('company_id' => $company_id, 'branch_type_id' => $branch_type_id));
        return $query->result_array();
    }

    public function getCompanyApprovalRoleByApprovalId($company_id,$approval_role_id)
    {
        $query = $this->db->get_where('company_approval_role',array('company_id' => $company_id, 'approval_role_id' => $approval_role_id));
        //echo $this->db->last_query();
        return $query->result_array();
    }


    public function getCompanyApprovalRoleByApprovalRoleId($company_id,$approval_role_id)
    {
        $query = $this->db->get_where('company_approval_role',array('company_id' => $company_id, 'approval_role_id' => $approval_role_id));
        return $query->result_array();
    }

    public function getCompanyBranchTypeAsReportingBranchType($company_id,$branch_type_id)
    {
        $query = $this->db->get_where('company_branch_type',array('company_id' => $company_id, 'reporting_branch_type_id' => $branch_type_id));
        return $query->result_array();
    }

    public function getCompanyApprovalRoleAsReportingApprovalRole($company_id,$approval_role_id)
    {
        $query = $this->db->get_where('company_approval_role',array('company_id' => $company_id, 'reporting_role_id' => $approval_role_id));
        return $query->result_array();
    }

    public function getCompanyApprovalCreditCommitteeAsReportingCreditCommittee($company_id,$credit_committee_id)
    {
        $query = $this->db->get_where('company_approval_credit_committee',array('company_id' => $company_id, 'credit_committee_forward_to' => $credit_committee_id));
        return $query->result_array();
    }


    public function getBranchUsers($branchId)
    {
        if($branchId)
        {
            //echo $branchId;
            $branch_users =  $this->getBranchUsersByBrachId($branchId);
            //echo "<pre>"; print_r($branch_users);
            if(!empty($branch_users))
            {
                $this->branch_users[] = $branch_users;

                if($branchId==$branch_users[0]['reporting_branch_id'])
                {
                    $parentBranchId=0;
                }
                else
                {
                    if($branchId!=$branch_users[0]['reporting_branch_id'])//condition_for same branch id getting reporting branch
                    {
                        if(isset($branch_users[0]['reporting_branch_id']))
                            $parentBranchId = $branch_users[0]['reporting_branch_id'];
                        else
                            $parentBranchId=0;
                    }
                    else
                    {
                        $parentBranchId=0;
                    }
                }
            }
            else
            {
                $parentBranchId=0;
            }

            $this->getBranchUsers($parentBranchId);
        }

        return $this->branch_users;
    }

    public function getReportingApprovalRoles($company_id,$approvalRoleId)
    {
        if($approvalRoleId)
        {
            //echo $approvalRoleId;
            $approvalRoles =  $this->getCompanyApprovalRole(array('company_id' => $company_id, 'approval_role_id' => $approvalRoleId));
            //echo "<pre>"; print_r($approvalRoles); exit;
            if(!empty($approvalRoles))
            {
                if( $approvalRoles[0]['reporting_role_id']!=0){
                    $reporting_role_details = $this->getCompanyApprovalRole(array('company_id' => $company_id, 'approval_role_id' => $approvalRoles[0]['reporting_role_id']));
                    $this->reporting_company_approval_roles[] = $reporting_role_details[0]['id_company_approval_role'];
                }


                if($approvalRoleId==$approvalRoles[0]['reporting_role_id'])
                {
                    $reportingApprovalRoleId=0;
                }
                else
                {
                    $reportingApprovalRoleId = $approvalRoles[0]['reporting_role_id'];
                }
            }
            else
            {
                $reportingApprovalRoleId=0;
            }

            $this->getReportingApprovalRoles($company_id,$reportingApprovalRoleId);
        }

        return $this->reporting_company_approval_roles;

    }

    public function getLowerApprovalRoles($company_id,$reportingRoleId)
    {
        if($reportingRoleId)
        {
            $approvalRoles =  $this->getCompanyApprovalRolesByReportingRole(array('company_id' => $company_id, 'reporting_role_id' => $reportingRoleId));
            if(!empty($approvalRoles))
            {
                $approvalRoleId = $approvalRoles[0]['approval_role_id'];
                $this->reporting_company_approval_roles[] = $approvalRoles[0]['id_company_approval_role'];
                $this->getLowerApprovalRoles($company_id,$approvalRoleId);
            }
        }
        return $this->reporting_company_approval_roles;
    }

    public function getReportingBranchTypes($company_id,$branch_type_id)
    {
        if($branch_type_id)
        {
            //echo $branchId;
            $branch_type =  $this->getCompanyBranchTypeByBranchId($company_id,$branch_type_id);

            if(!empty($branch_type))
            {
                if( $branch_type[0]['reporting_branch_type_id']!=0)
                    $this->reporting_branch_type[] = $branch_type[0]['reporting_branch_type_id'];

                if($branch_type_id==$branch_type[0]['reporting_branch_type_id'])
                {
                    $branch_type_id=0;
                }
                else
                {
                    $branch_type_id = $branch_type[0]['reporting_branch_type_id'];
                }
            }
            else
            {
                $branch_type_id=0;
            }

            $this->getReportingBranchTypes($company_id,$branch_type_id);
        }

        return $this->reporting_branch_type;
    }

    public function getLowerReportingUsers($company_id,$user_id)
    {
        if($user_id)
        {
            $user =  $this->getCompanyUserByReportingUser(array('company_id' => $company_id, 'reporting_user_id' => $user_id));
            if(!empty($user))
            {
                for($s=0;$s<count($user);$s++)
                {
                    $user_id = $user[$s]['user_id'];
                    $this->reporting_user[] = $user[$s]['user_id'];
                    $this->getLowerReportingUsers($company_id,$user_id);
                }
            }
            else
            {
                $user_id=0;
            }
        }

        return $this->reporting_user;
    }

    public function getCompanyUserByReportingUser($data)
    {
        $this->db->select('*');
        $this->db->from('company_user');
        if(isset($data['company_id']))
            $this->db->where('company_id', $data['company_id']);
        if(isset($data['reporting_user_id']))
            $this->db->where('reporting_user_id', $data['reporting_user_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getReportingBranch($branch_id)
    {
        if($branch_id)
        {
            //echo $branchId;
            $branch =  $this->getCompanyBranch($branch_id);
            if(!empty($branch))
            {
                if($branch->reporting_branch_id!=0)
                    $this->reporting_branch[] = $branch->reporting_branch_id;

                if($branch_id==$branch->reporting_branch_id)
                {
                    $branch_id=0;
                }
                else
                {
                    $branch_id = $branch->reporting_branch_id;
                }
            }
            else
            {
                $branch_id=0;
            }

            $this->getReportingBranch($branch_id);
        }

        return $this->reporting_branch;
    }

    public function getCompanyBranchByBranchTypeId($data)
    {
        $this->db->select('*');
        $this->db->from('company_branch');
        $this->db->where('company_id', $data['company_id']);
        $this->db->where('branch_type_id', $data['branch_type_id']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCompanyBranchByCompanyId($company_id,$branch_id='',$allowed_branch_type_ids = 0)
    {
        $this->db->select('cb.*');
        $this->db->from('company_branch cb');
        $this->db->where('company_id', $company_id);

        if($branch_id!='' || $branch_id!=0){
            $this->db->where('id_branch !=', $branch_id);
        }

        if($allowed_branch_type_ids){
            $this->db->where('branch_type_id IN '.$allowed_branch_type_ids.'');
        }

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }



    public function getCompanyBranchTypeByCompanyId($company_id,$allowed_branch_type_ids=0)
    {
        $this->db->select('cbt.*,bt.branch_type_name');
        $this->db->from('company_branch_type cbt');
        $this->db->join('branch_type bt','cbt.branch_type_id=bt.id_branch_type','left');
        $this->db->where('cbt.company_id', $company_id);
        if($allowed_branch_type_ids){
            $this->db->where('cbt.branch_type_id IN '.$allowed_branch_type_ids.'');
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyBranchListByCompanyId($data)
    {

        $this->db->select('cb.*,bt.branch_type_name,bt.branch_type_code');
        $this->db->from('company_branch cb');
        $this->db->join('branch_type bt', 'cb.branch_type_id=bt.id_branch_type', 'left');
        $this->db->where('cb.company_id', $data['company_id']);
        if(isset($data['branch_type_id']) && $data['branch_type_id'])
            $this->db->where('cb.branch_type_id', $data['branch_type_id']);
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        else if(isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit']);




        /*$this->db->select('tmp.*,count(*) as users');
        $this->db->from('company_user cu');
        $sub = $this->subquery->start_subquery('join', 'left', 'tmp.id_branch = cu.branch_id');
        $this->db->select('cb.*,bt.branch_type_name,bt.branch_type_code');
        $this->db->from('company_branch cb');
        $this->db->join('branch_type bt', 'cb.branch_type_id=bt.id_branch_type', 'left');
        $this->db->where('cb.company_id', $data['company_id']);
        if(isset($data['branch_type_id']) && $data['branch_type_id'])
            $this->db->where('cb.branch_type_id', $data['branch_type_id']);
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        else if(isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit']);
        $this->subquery->end_subquery('tmp');*/

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTotalCompanyBranchByCompanyId($data)
    {
        $this->db->select('count(*) as total_records');
        $this->db->from('company_branch');
        $this->db->where('company_id', $data['company_id']);
        $query = $this->db->get();
        return $query->row();
    }

    public function getCompanyBranchTypeStructure($company_id)
    {
        $this->db->select('bt.*,bt.company_id as created_by,cbt.branch_type_id,cbt.company_id,cbt.reporting_branch_type_id,bta.branch_type_name as reporting_branch_type_name,bta.branch_type_code as reporting_branch_type_code');
        $this->db->from('branch_type bt');
        $this->db->join('company_branch_type cbt', 'cbt.branch_type_id=bt.id_branch_type', 'left');
        $this->db->join('branch_type bta', 'cbt.reporting_branch_type_id=bta.id_branch_type', 'left');
        $this->db->where('cbt.company_id', $company_id);
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getCompanyBranch($id)
    {
        $this->db->select('bt.branch_type_name,c.*,cb.*');
        $this->db->from('company c');
        $this->db->join('company_branch cb', 'c.id_company=cb.company_id', 'left');
        //$this->db->join('company_branch_type cbt', 'cb.branch_type_id=cbt.branch_type_id', 'left');
        $this->db->join('branch_type bt', 'cb.branch_type_id=bt.id_branch_type', 'left');
        $this->db->where('cb.id_branch', $id);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->row();
    }

    public function createCompanyBranch($data)
    {
        $this->db->insert('company_branch', $data);
        return $this->db->insert_id();
    }

    public function updateCompanyBranch($data,$branch_id)
    {
        $this->db->where('id_branch', $branch_id);
        $this->db->update('company_branch', $data);
    }

    public function createCompanyBranchType($data)
    {
        $this->db->insert('company_branch_type', $data);
        return $this->db->insert_id();
    }

    public function createCompanyApprovalRole($data)
    {
        $this->db->insert('company_approval_role', $data);
        return $this->db->insert_id();
    }

    public function updateCompanyApprovalRole($data)
    {
        $this->db->where('id_company_approval_role', $data['id_company_approval_role']);
        $this->db->update('company_approval_role', $data);
    }

    public function updateCompanyApprovalRoleByRoleId($data)
    {
        $this->db->where(array('company_id' => $data['company_id'], 'approval_role_id' => $data['approval_role_id']));
        $this->db->update('company_approval_role', $data);
    }

    public function getApprovalRoleByCompanyId($company_id)
    {
        $this->db->select('ar.*,cpr.id_company_approval_role as company_approval_role_id,cpr.reporting_role_id,cpr.approval_role_id');
        $this->db->from('approval_role ar');
        $this->db->join('company_approval_role cpr', 'ar.id_approval_role=cpr.approval_role_id','left');
        $this->db->where('cpr.company_id', $company_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyUsersByApprovalRole($approval_role_id,$company_id)
    {
        $this->db->select('cu.*,u.user_role_id,u.first_name,u.last_name,u.email_id,u.profile_image,u.phone_number,u.address,u.city,u.state,u.country_id,u.profile_image,u.user_status,cb.legal_name');
        $this->db->from('company_user cu');
        $this->db->join('company_approval_role cpr', 'cu.company_approval_role_id=cpr.id_company_approval_role','left');
        $this->db->join('user u', 'cu.user_id=u.id_user','left');
        $this->db->join('company_branch cb', 'cu.branch_id=cb.id_branch','left');
        $this->db->where(array('cu.company_approval_role_id' => $approval_role_id,'cu.company_id' => $company_id));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCountryByName($country_name)
    {
        $query = $this->db->get_where('country',array('country_name' => $country_name));
        return $query->row();
    }

    public function getCompanyBranchByName($branch_name)
    {
        $query = $this->db->get_where('company_branch',array('legal_name' => $branch_name));
        return $query->row();
    }

    public function getBranchTypeById($branch_type_id)
    {
        $query = $this->db->get_where('branch_type',array('id_branch_type' => $branch_type_id));
        return $query->row();
    }

    public function getBranchTypeByName($branch_type)
    {
        $query = $this->db->get_where('branch_type',array('branch_type_name' => $branch_type));
        return $query->row();
    }

    public function addBranchType($data)
    {
        $this->db->insert('branch_type', $data);
        return $this->db->insert_id();
    }

    public function addApprovalRole($data)
    {
        $this->db->insert('approval_role', $data);
        return $this->db->insert_id();
    }

    public function updateBranchType($data)
    {
        $this->db->where('id_branch_type', $data['id_branch_type']);
        $this->db->update('branch_type', $data);
        return 1;
    }

    public function updateApprovalRoleByRoleId($data)
    {
        $this->db->where(array('company_id' => $data['company_id'], 'id_approval_role' => $data['approval_role_id']));
        $this->db->update('approval_role', $data);
        return 1;
    }
    public function updateApprovalRole($data)
    {
        $this->db->where('id_approval_role', $data['id_approval_role']);
        $this->db->update('approval_role', $data);
        return 1;
    }

    public function updateCompanyBranchType($data)
    {
        $this->db->where(array('company_id' => $data['company_id'], 'branch_type_id' => $data['branch_type_id']));
        $this->db->update('company_branch_type', $data);
        return 1;
    }

    public function updateCompanyBranchTypeByCompanyBranchTypeId($data)
    {
        $this->db->where('id_company_branch_type', $data['id_company_branch_type']);
        $this->db->update('company_branch_type', $data);
        return 1;
    }

    public function getCompanyBranchTypeByName($branch_name,$company_id)
    {
        $this->db->select('*');
        $this->db->from('company_branch_type c');
        $this->db->join('branch_type b','b.id_branch_type=c.branch_type_id','left');
        $this->db->where(array('b.branch_type_name' => $branch_name,'c.company_id' => $company_id));
        $query = $this->db->get();
        return $query->row();
    }

    public function getApprovalRoleByName($approval_name)
    {
        $query = $this->db->get_where('approval_role`',array('approval_name' => $approval_name));
        return $query->row();
    }

    public function getCompanyApprovalRole($data)
    {
        $this->db->select('*');
        $this->db->from('company_approval_role car');
        $this->db->join('approval_role ar','car.approval_role_id=ar.id_approval_role','left');
        if(isset($data['id_company_approval_role']))
            $this->db->where(array('car.id_company_approval_role' => $data['id_company_approval_role']));
        if(isset($data['company_id']))
            $this->db->where(array('car.company_id' => $data['company_id']));
        if(isset($data['branch_type_id']))
            $this->db->where(array('car.branch_type_id' => $data['branch_type_id']));
        if(isset($data['approval_role_id']))
            $this->db->where(array('car.approval_role_id' => $data['approval_role_id']));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyApprovalRolesByCompanyId($company_id)
    {
        $this->db->select('*');
        $this->db->from('company_approval_role car');
        $this->db->where(array('car.company_id' => $company_id));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyApprovalRolesByReportingRole($data)
    {
        $this->db->select('*');
        $this->db->from('company_approval_role car');
        if(isset($data['company_id']))
            $this->db->where(array('car.company_id' => $data['company_id']));
        if(isset($data['reporting_role_id']))
            $this->db->where(array('car.reporting_role_id' => $data['reporting_role_id']));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyApprovalRoleByName($approval_name,$company_id)
    {
        $this->db->select('*');
        $this->db->from('company_approval_role car');
        $this->db->join('approval_role ar','car.approval_role_id=ar.id_approval_role','join');
        $this->db->where(array('ar.approval_name' => $approval_name,'car.company_id' => $company_id));
        $query = $this->db->get();
        return $query->row();
    }

    public function getCompanyApprovalTypeStructure($company_id)
    {
        //$this->db->select('bt.id_branch_type,bt.branch_type_name,ar.id_approval_role,ar.approval_name,ar.approval_role_code,cpr.id_company_approval_role,cpr.company_id,ara.id_approval_role as reporting_id,ara.approval_name as reporting_approval_name,ara.approval_role_code as reporting_approval_code,cpr.approval_role_id,cpr.reporting_role_id,IF(ar.company_id=0,0,1) as is_edit');
        $this->db->select('bt.id_branch_type,bt.branch_type_name,ar.id_approval_role,ar.approval_name,ar.approval_role_code,ar.description,cpr.id_company_approval_role,cpr.company_id,ara.id_approval_role as reporting_id,ara.approval_name as reporting_approval_name,ara.approval_role_code as reporting_approval_code,cpr.approval_role_id,cpr.reporting_role_id, ar.company_id as is_edit');
        $this->db->from('approval_role ar');
        $this->db->join('company_approval_role cpr', 'cpr.approval_role_id=ar.id_approval_role', 'left');
        $this->db->join('approval_role ara', 'cpr.reporting_role_id=ara.id_approval_role', 'left');
        $this->db->join('branch_type bt', 'cpr.branch_type_id=bt.id_branch_type', 'left');
        $this->db->where('cpr.company_id', $company_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyBranchUsersByBranchId($branch_id,$company_id)
    {
        $this->db->select('count(*) as total_users');
        $query = $this->db->get_where('company_user',array('branch_id' => $branch_id,'company_id' => $company_id));
        return $query->row();
    }

    public function getApprovalStructure($data)
    {
        $this->db->select('s.*,DATE(s.created_date_time) as date,IFNULL(group_concat(product_name),"---") as products');
        $this->db->from('company_approval_structure s');
        $this->db->join('product p','s.id_company_approval_structure=p.company_approval_structure_id','left');
        if(isset($data['company_id'])){
            $this->db->where('s.company_id',$data['company_id']);
        }
        if(isset($data['approval_structure_name'])){
            $this->db->where('s.approval_structure_name',$data['approval_structure_name']);
        }
        if(isset($data['id_company_approval_structure'])){
            $this->db->where('s.id_company_approval_structure',$data['id_company_approval_structure']);
        }
        if(isset($data['id_company_approval_structure_not'])){
            $this->db->where('s.id_company_approval_structure!=',$data['id_company_approval_structure_not']);
        }
        $this->db->group_by('s.id_company_approval_structure');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addCompanyApprovalStructure($data)
    {
        $this->db->insert('company_approval_structure', $data);
        return $this->db->insert_id();
    }

    public function updateCompanyApprovalStructure($data)
    {
        $this->db->where('id_company_approval_structure', $data['id_company_approval_structure']);
        $this->db->update('company_approval_structure', $data);
        return 1;
    }

    public function getApprovalCreditCommitteeList($data)
    {
        $this->db->select('c.*');
        $this->db->from('company_approval_credit_committee c');
        /*$this->db->join('company_approval_credit_committee_structure cs','c.id_company_approval_credit_committee=cs.company_approval_credit_committee_id','left');
        $this->db->join('branch_type_role btr','btr.id_branch_type_role=cs.branch_type_role_id','left');
        $this->db->join('approval_role ar','ar.id_approval_role=cs.approval_role_id','left');*/
        if(isset($data['id_company_approval_credit_committee'])){
            $this->db->where('c.id_company_approval_credit_committee!=',$data['id_company_approval_credit_committee']);
        }
        if(isset($data['company_id'])){
            $this->db->where('c.company_id',$data['company_id']);
        }
        if(isset($data['branch_type_id'])){
            $this->db->where('c.branch_type_id',$data['branch_type_id']);
        }
        if(isset($data['id_company_approval_structure'])){
            $this->db->where('c.company_approval_structure_id',$data['id_company_approval_structure']);
        }
        $this->db->order_by('c.credit_committee_forward_to');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getApprovalCreditCommittee($data)
    {
        //echo "<pre>"; print_r($data); exit;
        $this->db->select('c.*');
        $this->db->from('company_approval_credit_committee c');
        /*$this->db->join('company_approval_credit_committee_structure cs','c.id_company_approval_credit_committee=cs.company_approval_credit_committee_id','left');
        $this->db->join('branch_type_role btr','btr.id_branch_type_role=cs.branch_type_role_id','left');
        $this->db->join('approval_role ar','ar.id_approval_role=cs.approval_role_id','left');*/
        if(isset($data['id_company_approval_credit_committee'])){
            $this->db->where('c.id_company_approval_credit_committee',$data['id_company_approval_credit_committee']);
        }
        if(isset($data['committee_id'])){
            $this->db->where('c.id_company_approval_credit_committee',$data['committee_id']);
        }
        if(isset($data['company_id'])){
            $this->db->where('c.company_id',$data['company_id']);
        }
        if(isset($data['branch_type_id'])){
            $this->db->where('c.branch_type_id',$data['branch_type_id']);
        }
        if(isset($data['id_company_approval_structure'])){
            $this->db->where('c.company_approval_structure_id',$data['id_company_approval_structure']);
        }
        if(isset($data['credit_committee_name'])){
            $this->db->where('c.credit_committee_name',$data['credit_committee_name']);
        }
        if(isset($data['id_company_approval_credit_committee_not']) && $data['id_company_approval_credit_committee_not']!=0){
            $this->db->where('c.id_company_approval_credit_committee!=',$data['id_company_approval_credit_committee_not']);
        }

        $this->db->order_by('c.credit_committee_forward_to');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getApprovalCreditCommitteeIds($data)
    {
        //echo "<pre>"; print_r($data); exit;
        $this->db->select('c.credit_committee_forward_to as id');
        $this->db->from('company_approval_credit_committee c');
        /*$this->db->join('company_approval_credit_committee_structure cs','c.id_company_approval_credit_committee=cs.company_approval_credit_committee_id','left');
        $this->db->join('branch_type_role btr','btr.id_branch_type_role=cs.branch_type_role_id','left');
        $this->db->join('approval_role ar','ar.id_approval_role=cs.approval_role_id','left');*/
        if(isset($data['id_company_approval_credit_committee'])){
            $this->db->where('c.id_company_approval_credit_committee',$data['id_company_approval_credit_committee']);
        }

        if(isset($data['company_id'])){
            $this->db->where('c.company_id',$data['company_id']);
        }
        if(isset($data['branch_type_id'])){
            $this->db->where('c.branch_type_id',$data['branch_type_id']);
        }
        if(isset($data['id_company_approval_structure'])){
            $this->db->where('c.company_approval_structure_id',$data['id_company_approval_structure']);
        }
        $this->db->order_by('c.credit_committee_forward_to');
        $query = $this->db->get();
        //echo "<br>".$this->db->last_query();
        return $query->result_array();
    }

    public function getLastCompanyApprovalCreditCommittee($data)
    {
        //echo "<pre>"; print_r($data); exit;
        $this->db->select('c.*');
        $this->db->from('company_approval_credit_committee c');
        /*$this->db->join('company_approval_credit_committee_structure cs','c.id_company_approval_credit_committee=cs.company_approval_credit_committee_id','left');
        $this->db->join('branch_type_role btr','btr.id_branch_type_role=cs.branch_type_role_id','left');
        $this->db->join('approval_role ar','ar.id_approval_role=cs.approval_role_id','left');*/
        if(isset($data['committee_ids'])){
            if($data['committee_ids'])
                $this->db->where('c.id_company_approval_credit_committee NOT IN '.$data['committee_ids'], NULL, FALSE);
        }

        if(isset($data['company_id'])){
            $this->db->where('c.company_id',$data['company_id']);
        }
        if(isset($data['branch_type_id'])){
            $this->db->where('c.branch_type_id',$data['branch_type_id']);
        }
        if(isset($data['id_company_approval_structure'])){
            $this->db->where('c.company_approval_structure_id',$data['id_company_approval_structure']);
        }
        $this->db->order_by('c.credit_committee_forward_to');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function checkCommitteesForwardTo($data,$company_approval_structure_id=0)
    {
        //echo "<pre>"; print_r($data);
        $this->db->select('c.*,cc.credit_committee_name as forward_committee_name');
        $this->db->from('company_approval_credit_committee c');
        $this->db->join('company_approval_credit_committee cc','c.credit_committee_forward_to=cc.id_company_approval_credit_committee','left');
        $this->db->where(array('c.credit_committee_forward_to' => $data['credit_committee_forward_to'], 'c.company_id' => $data['company_id']));
        if($company_approval_structure_id)
            $this->db->where('c.id_company_approval_credit_committee!=', $company_approval_structure_id);
        if(isset($data['branch_type_id']))
            $this->db->where('c.branch_type_id=', $data['branch_type_id']);
        if(isset($data['company_approval_structure_id']))
            $this->db->where('c.company_approval_structure_id=', $data['company_approval_structure_id']);
        if(isset($data['company_id']))
            $this->db->where('c.company_id=', $data['company_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function checkPrimaryCreditCommittees($data,$company_approval_credit_committee=0)
    {
        $this->db->select('*');
        $this->db->from('company_approval_credit_committee');
        $this->db->where(array('credit_committee_forward_to' => '0', 'company_id' => $data['company_id']));
        if(isset($data['company_approval_structure_id']))
            $this->db->where('company_approval_structure_id=', $data['company_approval_structure_id']);
        if(isset($data['branch_type_id']))
            $this->db->where('branch_type_id=', $data['branch_type_id']);
        if($company_approval_credit_committee)
            $this->db->where('id_company_approval_credit_committee!=', $company_approval_credit_committee);
        if(isset($data['company_id']))
            $this->db->where('company_id=', $data['company_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addApprovalCreditCommittee($data)
    {
        $this->db->insert('company_approval_credit_committee', $data);
        return $this->db->insert_id();
    }

    public function updateApprovalCreditCommittee($data)
    {
        $this->db->where('id_company_approval_credit_committee', $data['id_company_approval_credit_committee']);
        $this->db->update('company_approval_credit_committee', $data);
        return 1;
    }

    public function checkApprovalCreditCommitteeStructure($data)
    {
        $this->db->select('cs.*');
        $this->db->from('company_approval_credit_committee_structure cs');

        if(isset($data['id_company_approval_credit_committee_structure'])){
            $this->db->where('cs.id_company_approval_credit_committee_structure',$data['id_company_approval_credit_committee_structure']);
        }

        if(isset($data['id_company_approval_credit_committee'])){
            $this->db->where('cs.company_approval_credit_committee_id',$data['id_company_approval_credit_committee']);
        }
        if(isset($data['branch_type_role_id'])){
            $this->db->where('cs.branch_type_role_id',$data['branch_type_role_id']);
        }
        if(isset($data['approval_role_id'])){
            $this->db->where('cs.approval_role_id',$data['approval_role_id']);
        }


        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getApprovalCreditCommitteeStructure($data)
    {
        $this->db->select('cs.*,cs.approval_role_id as company_approval_role_id,btr.branch_type_role_name,ar.approval_name');
        $this->db->from('company_approval_credit_committee_structure cs');
        $this->db->join('branch_type_role btr','btr.id_branch_type_role=cs.branch_type_role_id','left');
        $this->db->join('company_approval_role car','car.id_company_approval_role=cs.approval_role_id','left');
        $this->db->join('approval_role ar','ar.id_approval_role=car.approval_role_id','left');
        if(isset($data['id_company_approval_credit_committee_structure'])){
            $this->db->where('cs.id_company_approval_credit_committee_structure',$data['id_company_approval_credit_committee_structure']);
        }

        if(isset($data['id_company_approval_credit_committee'])){
            $this->db->where('cs.company_approval_credit_committee_id',$data['id_company_approval_credit_committee']);
        }
        if(isset($data['branch_type_role_id'])){
            $this->db->where('cs.branch_type_role_id',$data['branch_type_role_id']);
        }
        if(isset($data['approval_role_id'])){
            $this->db->where('cs.approval_role_id',$data['approval_role_id']);
        }

        $this->db->where('cs.status',1);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addApprovalCreditCommitteeStructure($data)
    {
        /*$this->db->insert('company_approval_credit_committee_structure', $data);
        return $this->db->insert_id();*/
        $this->db->insert_batch('company_approval_credit_committee_structure', $data);
        return $this->db->insert_id();
    }

    public function updateApprovalCreditCommitteeStructure($data)
    {
        /*$this->db->where('id_company_approval_credit_committee_structure', $data['id_company_approval_credit_committee']);
        $this->db->update('company_approval_credit_committee_structure', $data);*/
        $this->db->update_batch('company_approval_credit_committee_structure', $data, 'id_company_approval_credit_committee_structure');
        return 1;
    }

    public function getBranchTypeRole($data)
    {
        $this->db->select('*');
        $this->db->from('branch_type_role');
        if(isset($data['id_branch_type_role']))
            $this->db->where('id_branch_type_role !=',$data['id_branch_type_role']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getApprovalLimitExceptStatus($data)
    {
        $this->db->select('pal.id_product_approval_limit,pal.amount,pal.sector_id as id_sector,s.sector_name');
        $this->db->from('product_approval_limit pal');
        //$this->db->join('product_approval_limit pal','cacc.id_company_approval_credit_committee=pal.company_approval_credit_committee_id','left');
        $this->db->join('product p','p.id_product=pal.product_id','left');
        $this->db->join('sector s','s.id_sector=pal.sector_id','left');
        if(isset($data['company_approval_credit_committee_id']))
            $this->db->where('pal.company_approval_credit_committee_id =',$data['company_approval_credit_committee_id']);
        /*if(isset($data['branch_type_id']))
            $this->db->where('cacc.branch_type_id =',$data['branch_type_id']);*/
        if(isset($data['id_company_approval_structure']))
            $this->db->where('p.company_approval_structure_id =',$data['id_company_approval_structure']);
        if(isset($data['product_id']))
            $this->db->where('pal.product_id =',$data['product_id']);
        if(isset($data['company_id']))
            $this->db->where('pal.company_id =',$data['company_id']);
        if(isset($data['sector_id']))
            $this->db->where('pal.sector_id =',$data['sector_id']);

        if(isset($data['committees'])){
            $this->db->where('company_approval_credit_committee_id IN '.$data['committees'], NULL, FALSE);
        }

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getApprovalLimit($data)
    {
        $this->db->select('pal.id_product_approval_limit,pal.amount,pal.sector_id as id_sector,s.sector_name');
        $this->db->from('product_approval_limit pal');
        //$this->db->join('product_approval_limit pal','cacc.id_company_approval_credit_committee=pal.company_approval_credit_committee_id','left');
        $this->db->join('sector s','s.id_sector=pal.sector_id','left');
        if(isset($data['company_approval_credit_committee_id']))
            $this->db->where('pal.company_approval_credit_committee_id =',$data['company_approval_credit_committee_id']);
        /*if(isset($data['branch_type_id']))
            $this->db->where('cacc.branch_type_id =',$data['branch_type_id']);*/
        if(isset($data['product_id']))
            $this->db->where('pal.product_id =',$data['product_id']);
        if(isset($data['company_id']))
            $this->db->where('pal.company_id =',$data['company_id']);
        if(isset($data['sector_id']))
            $this->db->where('pal.sector_id =',$data['sector_id']);

        if(isset($data['company_approval_credit_committee_id']))
            $this->db->where('pal.company_approval_credit_committee_id =',$data['company_approval_credit_committee_id']);

        $this->db->where('pal.product_approval_limit_status =',1);
        $this->db->order_by('DATE(pal.created_date)','ASC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getApprovalLimitSectors($data)
    {
        $this->db->select('pal.sector_id as id_sector,IFNULL(s.sector_name, "All sectors") as sector_name');
        $this->db->from('product_approval_limit pal');
        $this->db->join('sector s','s.id_sector=pal.sector_id','left');
        if(isset($data['company_approval_credit_committee_id']))
            $this->db->where('pal.company_approval_credit_committee_id =',$data['company_approval_credit_committee_id']);
        /*if(isset($data['branch_type_id']))
            $this->db->where('cacc.branch_type_id =',$data['branch_type_id']);*/
        if(isset($data['product_id']))
            $this->db->where('pal.product_id =',$data['product_id']);
        if(isset($data['company_id']))
            $this->db->where('pal.company_id =',$data['company_id']);

        if(isset($data['committees'])){
            $this->db->where('company_approval_credit_committee_id IN '.$data['committees'], NULL, FALSE);
        }

        $this->db->where('pal.product_approval_limit_status','1');
        $this->db->group_by('pal.sector_id');
        $this->db->order_by('pal.id_product_approval_limit','ASC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        //echo "1<pre>"; print_r($query->result_array()); exit;
        return $query->result_array();
    }

    public function addApprovalLimit($data)
    {
        $this->db->insert_batch('product_approval_limit', $data);
        return $this->db->insert_id();
    }

    public function updateApprovalLimitBySector($data,$status)
    {
        $this->db->where(array('company_id' => $data['company_id'], 'sector_id' => $data['sector_id'], 'product_id' => $data['product_id']));
        $this->db->update('product_approval_limit', array('product_approval_limit_status' => $status,'updated_date' => date('y-m-d H:i:s')));
        return 1;
    }

    public function updateApprovalLimit($data)
    {
        $this->db->update_batch('product_approval_limit', $data, 'id_product_approval_limit');
        return 1;
    }

    public function ChangeApprovalLimitStatus($data)
    {
        $committee_id = 0;
        if(isset($data['company_approval_credit_committee_id'])){
            $committee_id = $data['company_approval_credit_committee_id'];
            unset($data['company_approval_credit_committee_id']);
        }
        $this->db->where('product_id', $data['product_id']);
        if($committee_id!='' && $committee_id!=0){
            $this->db->where('company_approval_credit_committee_id IN '.$committee_id, NULL, FALSE);
        }
        $this->db->update('product_approval_limit', $data);
        return 1;
    }

    public function getFinancialStatements($data)
    {
        $this->db->select('*');
        $this->db->from('financial_statement');
        if(isset($data['id_financial_statement']))
            $this->db->where('id_financial_statement', $data['id_financial_statement']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyFinancialStatement($data)
    {
        $this->db->select('*');
        $this->db->from('company_financial_statement cf');
        $this->db->join('financial_statement f','f.id_financial_statement=cf.financial_statement_id','left');
        if(isset($data['financial_statement_id']))
            $this->db->where('cf.financial_statement_id', $data['financial_statement_id']);

        if(isset($data['crm_company_id']))
            $this->db->where('cf.crm_company_id', $data['crm_company_id']);

        $this->db->order_by('cf.id_company_financial_statement','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCompanyFinancialStatement($data)
    {
        $this->db->insert('company_financial_statement', $data);
        return $this->db->insert_id();
    }

    public function addFinancialStatementHeader($data)
    {
        $this->db->insert('financial_statement_header', $data);
        return $this->db->insert_id();
    }

    public function addFinancialStatementHeaderItem($data)
    {
        $this->db->insert('financial_statement_header_item', $data);
        return $this->db->insert_id();
    }

    public function getFinancialStatementHeaderItemByHeaderItemName($header_item_name)
    {
        $query = $this->db->get_where('financial_statement_header_item',array('header_item_name' => $header_item_name));
        return $query->result_array();
    }

    public function addFinancialItemData($data)
    {
        $this->db->insert('financial_statement_item_data', $data);
        //echo $this->db->insert_id(); exit;
        return $this->db->insert_id();
    }

    public function getAssessmentQuestionType($data)
    {
        $this->db->select('*');
        $this->db->from('assessment_question_type');
        if(isset($data['id_assessment_question_type']))
            $this->db->where('id_assessment_question_type', $data['id_assessment_question_type']);

        if(isset($data['assessment_question_type_key']))
            $this->db->where('assessment_question_type_key', $data['assessment_question_type_key']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAssessmentQuestionCategory($data)
    {
        $this->db->select('*');
        $this->db->from('assessment_question_category');

        if(isset($data['assessment_question_type_id']))
            $this->db->where('assessment_question_type_id', $data['assessment_question_type_id']);
        if(isset($data['assessment_question_category_name']))
            $this->db->where('assessment_question_category_name', $data['assessment_question_category_name']);
        if(isset($data['company_id']))
            $this->db->where('company_id', $data['company_id']);
        if(isset($data['product_id']) && $data['product_id']!=0)
            $this->db->where('product_id', $data['product_id']);
        if(isset($data['collateral_type_id']) && $data['collateral_type_id']!=0)
            $this->db->where('collateral_type_id', $data['collateral_type_id']);
        if(isset($data['id_assessment_question_category']))
            $this->db->where('id_assessment_question_category', $data['id_assessment_question_category']);
        if(isset($data['id_assessment_question_category_not']))
            $this->db->where('id_assessment_question_category!=', $data['id_assessment_question_category_not']);
        if(isset($data['assessment_question_category_status']))
            $this->db->where('assessment_question_category_status', $data['assessment_question_category_status']);

        if(isset($data['collateral_type_id']) && $data['collateral_type_id']!=0)
            $this->db->where('collateral_type_id', $data['collateral_type_id']);

        if(isset($data['sector_id']) && (isset($data['sub_sector_id'])))
            $this->db->where('(FIND_IN_SET('.$data['sector_id'].',sector_id)>0 OR FIND_IN_SET('.$data['sub_sector_id'].',sector_id)>0)');
        /*if(isset($data['sector_id']))
            $this->db->where('FIND_IN_SET('.$data['sector_id'].',sector_id)>0', NULL, FALSE);
        if(isset($data['sub_sector_id']) && $data['sub_sector_id']!=0)
            $this->db->where('FIND_IN_SET('.$data['sub_sector_id'].',sector_id)>0', NULL, FALSE);*/
        if(isset($data['order']))
            $this->db->order_by('id_assessment_question_category',$data['order']);
        else
            $this->db->order_by('id_assessment_question_category','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getAssessmentQuestion($data)
    {
        $this->db->select('*');
        $this->db->from('assessment_question');
        if(isset($data['assessment_question_category_id']))
            $this->db->where('assessment_question_category_id', $data['assessment_question_category_id']);
        if(isset($data['id_assessment_question']))
            $this->db->where('id_assessment_question', $data['id_assessment_question']);

        if(isset($data['id_assessment_question_not']))
            $this->db->where('id_assessment_question!=', $data['id_assessment_question_not']);
        if(isset($data['assessment_question']))
                    $this->db->where('assessment_question', $data['assessment_question']);

        if(isset($data['question_status']))
            $this->db->where('question_status', $data['question_status']);
        $this->db->order_by('id_assessment_question','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAssessmentQuestionAnswer($data)
    {
        $this->db->select('*');
        $this->db->from('assessment_answer');
        if(isset($data['assessment_question_id']))
            $this->db->where('assessment_question_id', $data['assessment_question_id']);
        if(isset($data['crm_company_id']))
            $this->db->where('crm_reference_id', $data['crm_company_id']);
        if(isset($data['crm_project_id']))
            $this->db->where('crm_reference_id', $data['crm_project_id']);
        if(isset($data['collateral_id']))
            $this->db->where('crm_reference_id', $data['collateral_id']);
        if(isset($data['crm_reference_id']))
                    $this->db->where('crm_reference_id', $data['crm_reference_id']);

        $query = $this->db->get();
        return $query->row();
    }

    public function addAssessmentQuestionCategory($data)
    {
        $this->db->insert('assessment_question_category', $data);
        return $this->db->insert_id();
    }

    public function updateAssessmentQuestionCategory($data)
    {
        $this->db->where('id_assessment_question_category', $data['id_assessment_question_category']);
        $this->db->update('assessment_question_category', $data);
        return 1;
    }

    public function addAssessmentQuestion($data)
    {
        $this->db->insert('assessment_question', $data);
        return $this->db->insert_id();
    }

    public function updateAssessmentQuestion($data)
    {
        $this->db->where('id_assessment_question', $data['id_assessment_question']);
        $this->db->update('assessment_question', $data);
        return 1;
    }

    public function getAssessment()
    {
        $query = $this->db->get('assessment');
        return $query->result_array();
    }

    public function getAssessmentForCompany($data)
    {
        $this->db->select('a.*,IFNULL(c.id_company_assessment,0) as checked');
        $this->db->from('assessment a');
        $this->db->join('company_assessment c','a.id_assessment=c.assessment_id and company_id='.$data['company_id'].' and company_assessment_status !=0'  ,'left');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyAssessment($data)
    {
        $this->db->select('*');
        $this->db->from('company_assessment c');
        $this->db->join('assessment a','c.assessment_id=a.id_assessment','left');
        /*if(isset($data['crm_company_id']))
            $this->db->where('c.crm_company_id', $data['crm_company_id']);*/
        if(isset($data['company_id']))
            $this->db->where('c.company_id', $data['company_id']);
        if(isset($data['assessment_id']))
            $this->db->where('c.assessment_id', $data['assessment_id']);

        if(!isset($data['company_assessment_status']))
            $this->db->where('c.company_assessment_status', 1);

        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function addCompanyAssessment($data)
    {
        //$this->db->insert('company_assessment', $data);
        $this->db->insert_batch('company_assessment', $data);
        return $this->db->insert_id();
    }

    public function updateCompanyAssessment($data)
    {
        $this->db->update_batch('company_assessment', $data, 'id_company_assessment');
        return 1;
    }

    public function updateCompanyAssessmentByAssessmentId($data)
    {
        $this->db->where(array('company_id' => $data['company_id'], 'assessment_id' => $data['assessment_id']));
        $this->db->update('company_assessment', $data);
        return 1;
    }

    public function getAssessmentItem($data)
    {
        $this->db->select('*');
        $this->db->from('assessment_item');
        if(isset($data['assessment_id']))
            $this->db->where('assessment_id',$data['assessment_id']);
        if(isset($data['id_assessment_item']))
            $this->db->where('id_assessment_item',$data['id_assessment_item']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyAssessmentItemStep($data)
    {
        $this->db->select('*');
        $this->db->from('company_assessment_item_step');
        if(isset($data['crm_company_id']))
            $this->db->where('crm_company_id',$data['crm_company_id']);
        if(isset($data['assessment_item_id']))
            $this->db->where('assessment_item_id',$data['assessment_item_id']);

        $this->db->order_by('id_company_assessment_item_step','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCompanyAssessmentItemStep($data)
    {
        $this->db->insert('company_assessment_item_step', $data);
        return $this->db->insert_id();
    }

    public function deleteCompanyAssessmentItemStep($data)
    {
        $this->db->where('id_company_assessment_item_step',$data['id_company_assessment_item_step'])->delete('company_assessment_item_step');
        return 1;
    }

    public function addAssessmentAnswer($data)
    {
        $this->db->insert('assessment_answer', $data);
        return $this->db->insert_id();
    }

    public function updateAssessmentAnswer($data)
    {
        $this->db->where('id_assessment_answer',$data['id_assessment_answer']);
        $this->db->update('assessment_answer', $data);
        return 1;
    }

    public function deleteAssessmentAnswer($data)
    {
        $this->db->where('id_assessment_answer',$data['id_assessment_answer'])->delete('assessment_answer');
        return 1;
    }

    public function getFinancialHeaders($data)
    {
        $this->db->select('fsh.header_name,fsh.parent_header_id,count(fshi.header_item_name) as loop_count');
        $this->db->from('company_financial_statement cfs');
        $this->db->join('financial_statement_header fsh','cfs.id_company_financial_statement=fsh.company_financial_statement_id','left');
        $this->db->join('financial_statement_header fsh1','fsh.parent_header_id=fsh1.id_financial_statement_header','left');
        $this->db->join('financial_statement_header_item fshi','fsh.id_financial_statement_header=fshi.financial_statement_header_id','left');
        $this->db->where('cfs.id_company_financial_statement',$data['id_company_financial_statement']);
        $this->db->group_by('fsh.id_financial_statement_header');
        if(mysqli_more_results($this->db->conn_id) && mysqli_next_result($this->db->conn_id)){
            mysqli_next_result( $this->db->conn_id );
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAllCompanyFinancialStatements($crm_company_id)
    {
        $this->db->select('max(c.id_company_financial_statement) as id_company_financial_statement, c.financial_statement_id, f.statement_name');
        $this->db->from('company_financial_statement c');
        $this->db->join('financial_statement f','c.financial_statement_id=f.id_financial_statement','left');
        $this->db->where('c.crm_company_id',$crm_company_id);
        $this->db->group_by('c.financial_statement_id');
        $this->db->order_by('c.financial_statement_id','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFinancialInformation($data)
    {
        //echo "call getFinancialInformation(".$data['id_company_financial_statement'].")"; exit;
        $query = $this->db->query("call getFinancialInformation(".$data['id_company_financial_statement'].")");
        return $query->result_array();
    }

    public function getProductTerms($data)
    {
        $this->db->select('*');
        $this->db->from('product_term');
        if(isset($data['product_term_key']))
            $this->db->where('product_term_key',$data['product_term_key']);
        $this->db->where('product_term_status',1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProductTermItems($data)
    {
        $this->db->select('*');
        $this->db->from('product_term_item');
        if(isset($data['product_term_id']))
            $this->db->where('product_term_id',$data['product_term_id']);

        if(isset($data['id_product_term_item']))
            $this->db->where('id_product_term_item',$data['id_product_term_item']);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function getRiskCategories($data)
    {
        $this->db->select('*');
        $this->db->from('risk_category r');
        if(isset($data['parent_risk_category_id']))
            $this->db->where('r.parent_risk_category_id',$data['parent_risk_category_id']);
        if(isset($data['risk_category_id']))
            $this->db->where('r.id_risk_category',$data['risk_category_id']);
        if(isset($data['company_id']))
            $this->db->where('r.company_id',$data['company_id']);
        if(isset($data['risk_category_name']))
            $this->db->where('r.risk_category_name',$data['risk_category_name']);
        if(isset($data['risk_category_id_not']))
            $this->db->where('r.id_risk_category!=',$data['risk_category_id_not']);

        if(isset($data['array_parent_risk_category_id']))
            $this->db->where_in('r.parent_risk_category_id',$data['array_parent_risk_category_id']);

        $this->db->where('r.risk_category_status',1);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function getRiskCategoryItemsSectors($data)
    {
        $this->db->select('s.id_sector,s.sector_name,r.risk_category_id');
        $this->db->from('risk_category_item r');
        $this->db->join('sector s','r.sector_id=s.id_sector','left');
        if(isset($data['risk_category_id']))
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
        $this->db->group_by('r.sector_id');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getRiskCategoryItems($data)
    {
        $this->db->select('*');
        $this->db->from('risk_category_item r');
        if(isset($data['sector_id']))
            $this->db->where('r.sector_id',$data['sector_id']);
        if(isset($data['risk_category_id']))
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
        if(isset($data['risk_category_item_name']))
            $this->db->where('r.risk_category_item_name',$data['risk_category_item_name']);
        if(isset($data['risk_category_item_id']))
            $this->db->where('r.id_risk_category_item',$data['risk_category_item_id']);
        if(isset($data['id_risk_category_item_not']))
            $this->db->where('r.id_risk_category_item!=',$data['id_risk_category_item_not']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function deleteRiskCategory($data)
    {
        if(isset($data['risk_category_id']))
            $this->db->where('id_risk_category', $data['id_risk_category']);
        if(isset($data['array_risk_category_id']))
            $this->db->where_in('id_risk_category', $data['array_risk_category_id']);
        if(isset($data['parent_risk_category_id']))
            $this->db->where('parent_risk_category_id', $data['parent_risk_category_id']);
        if(isset($data['array_parent_risk_category_id']))
            $this->db->where('parent_risk_category_id', $data['array_parent_risk_category_id']);

        $this->db->delete('risk_category');
    }

    public function deleteRiskCategoryItem($data)
    {
        if(isset($data['id_risk_category_item']))
            $this->db->where('id_risk_category_item', $data['id_risk_category_item']);
        if(isset($data['risk_category_id']))
            $this->db->where('risk_category_id', $data['risk_category_id']);
        if(isset($data['sector_id']))
            $this->db->where('sector_id', $data['sector_id']);
        if(isset($data['array_risk_category_id']))
            $this->db->where_in('risk_category_id', $data['array_risk_category_id']);

        $this->db->delete('risk_category_item');
    }

    public function getTotalRiskCategoryPercentage($data)
    {
        $this->db->select('sum(risk_percentage) as risk_percentage');
        $this->db->from('risk_category');
        if(isset($data['parent_risk_category_id']))
            $this->db->where('parent_risk_category_id', $data['parent_risk_category_id']);
        if(isset($data['id_risk_category']))
            $this->db->where('id_risk_category!='.$data['id_risk_category'].'');
        if(isset($data['company_id']))
            $this->db->where('company_id',$data['company_id']);
        $this->db->where('risk_category_status',1);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addRiskCategory($data)
    {
        $this->db->insert('risk_category', $data);
        return $this->db->insert_id();
    }

    public function updateRiskCategory($data)
    {
        $this->db->where('id_risk_category',$data['id_risk_category']);
        $this->db->update('risk_category', $data);
        return 1;
    }

    public function addRiskCategoryItem($data)
    {
        $this->db->insert('risk_category_item', $data);
        return $this->db->insert_id();
    }

    public function updateRiskCategoryItem($data)
    {
        //echo "<pre>"; print_r($data); exit;
        $this->db->where('id_risk_category_item',$data['id_risk_category_item']);
        $this->db->update('risk_category_item', $data);
        return 1;
    }

    public function addKnowledgeDocument($data)
    {
        $this->db->insert('knowledge_document', $data);
        return $this->db->insert_id();
    }

    public function updateKnowledgeDocument($data)
    {
        $this->db->where('id_knowledge_document',$data['id_knowledge_document']);
        $this->db->update('knowledge_document', $data);
        return 1;
    }

    public function getTotalKnowledgeDocuments($data)
    {
        //echo "<pre>"; print_r($data); exit;
        /*$this->db->select('k.*,group_concat(t.tag_name),concat(u.first_name," ",u.last_name) as user_name,DATE_FORMAT(k.created_date_time,"%d %b %Y") as date');
        $this->db->from('knowledge_document k');
        $this->db->join('knowledge_document_tag t','k.id_knowledge_document=t.knowledge_document_id','left');
        $this->db->join('user u','k.uploaded_by=u.id_user','left');
        if(isset($data['company_id']))
            $this->db->where('k.company_id',$data['company_id']);
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        else if(isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit']);

        $this->db->group_by('k.id_knowledge_document');
        $this->db->order_by('k.id_knowledge_document','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();*/

        if(!isset($data['created_by']))
        {
            $this->db->select('k.*,group_concat(t.tag_name),concat(u.first_name," ",u.last_name) as user_name,DATE_FORMAT(k.created_date_time,"%d %b %Y") as date');
            $this->db->from('knowledge_document k');
            $this->db->join('knowledge_document_tag t','k.id_knowledge_document=t.knowledge_document_id','left');
            $this->db->join('user u','k.uploaded_by=u.id_user','left');
            if(isset($data['company_id']))
                $this->db->where('k.company_id',$data['company_id']);
            if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
                $this->db->limit($data['limit'],$data['offset']);
            else if(isset($data['limit']) && $data['limit']!='')
                $this->db->limit($data['limit']);

            $this->db->group_by('k.id_knowledge_document');
            $this->db->order_by('k.id_knowledge_document','DESC');
            $query = $this->db->get();
            //echo $this->db->last_query(); exit;
            return $query->result_array();
        }
        else
        {

            $sql = 'select * from (';
            $sql.= 'select k.*,group_concat(t.tag_name),concat(u.first_name," ",u.last_name) as user_name,DATE_FORMAT(k.created_date_time,"%d %b %Y") as date
                    from knowledge_document k left join knowledge_document_tag t on k.id_knowledge_document=t.knowledge_document_id left join user u
                    on k.uploaded_by=u.id_user';
            if(isset($data['company_id'])){
                $sql.=' where k.company_id='.$data['company_id'].' ';
                $sql.=' and k.knowledge_document_status =1';
            }
            else{
                $sql.=' where k.knowledge_document_status =1';
            }
            $sql.=' group by k.id_knowledge_document ';
            $sql.=' order by id_knowledge_document desc ) dum1';
            //$sql.=' group by k.id_knowledge_document and order by k.id_knowledge_document DESC';

            $sql.= ' UNION All ';

            $sql.= 'select * from (';

            $sql.= 'select k.*,group_concat(t.tag_name),concat(u.first_name," ",u.last_name) as user_name,DATE_FORMAT(k.created_date_time,"%d %b %Y") as date
                    from knowledge_document k left join knowledge_document_tag t on k.id_knowledge_document=t.knowledge_document_id left join user u
                    on k.uploaded_by=u.id_user';
            if(isset($data['company_id'])){
                $sql.=' where k.company_id='.$data['company_id'].' ';
                $sql.=' and k.knowledge_document_status = 0';
            }
            else{
                $sql.=' where k.knowledge_document_status = 0';
            }
            $sql.=' and k.uploaded_by='.$data['created_by'].' ';
            //$sql.=' group by k.id_knowledge_document and order by k.id_knowledge_document DESC';
            $sql.=' group by k.id_knowledge_document ';

            $sql.=' order by k.id_knowledge_document desc ) dum2';

            if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
                $sql.=' limit '.$data['offset'].', '.$data['limit'].'';
            else if(isset($data['limit']) && $data['limit']!='')
                $sql.=' limit '.$data['limit'].' ';

            //echo $sql; exit;

            $query = $this->db->query($sql);
            return $query->result_array();
        }
    }

    public function getDocumentTypeKnowledgeDocuments($data)
    {
        $this->db->select('document_type,count(id_knowledge_document) as total');
        $this->db->from('knowledge_document');
        if(isset($data['company_id']))
            $this->db->where('company_id',$data['company_id']);
        $this->db->group_by('document_type');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getKnowledgeDocumentsUsers($data)
    {
        $this->db->select('concat(u.first_name," ",u.last_name) as user_name,profile_image,ar.approval_name,count(k.id_knowledge_document) as total');
        $this->db->from('knowledge_document k');
        $this->db->join('user u','k.uploaded_by=u.id_user','left');
        $this->db->join('company_user cu','u.id_user=cu.user_id','left');
        $this->db->join('company_approval_role cpr','cu.company_approval_role_id=cpr.id_company_approval_role','left');
        $this->db->join('approval_role ar','cpr.approval_role_id=ar.id_approval_role','left');
        if(isset($data['company_id']))
            $this->db->where('k.company_id',$data['company_id']);
        if(isset($data['knowledge_document_status']))
            $this->db->where('k.knowledge_document_status',$data['knowledge_document_status']);
        $this->db->group_by('k.uploaded_by');
        $this->db->order_by('total','DESC');
        $this->db->limit('10');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getKnowledgeDocuments($data)
    {

        $this->db->select('k.*,concat(u.first_name," ",u.last_name) as user_name,profile_image,group_concat(t.tag_name SEPARATOR "$$") as tags,DATE_FORMAT(k.created_date_time,"%d %b %Y") as date');
        $this->db->from('knowledge_document k');
        $this->db->join('user u','k.uploaded_by=u.id_user','left');
        $this->db->join('knowledge_document_tag t','k.id_knowledge_document=t.knowledge_document_id','left');

        if(isset($data['company_id']))
            $this->db->where('k.company_id',$data['company_id']);

        if(isset($data['search_key']) && $data['search_key']!='' && !empty($data['search_key']))
            $this->db->where('t.tag_name in '.$data['search_key']);

        if(isset($data['knowledge_document_id']))
            $this->db->where('k.id_knowledge_document',$data['knowledge_document_id']);

        $this->db->group_by('k.id_knowledge_document');
        $this->db->order_by('k.id_knowledge_document','DESC');

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        else if(isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addKnowledgeDocumentTags($data)
    {
        $this->db->insert_batch('knowledge_document_tag', $data);
        return $this->db->insert_id();
    }

    public function deleteKnowledgeTags($data)
    {
        if(isset($data['knowledge_document_id']))
            $this->db->where('knowledge_document_id', $data['knowledge_document_id']);
        else
            $this->db->where_in('id_knowledge_document_tag', $data);
        $this->db->delete('knowledge_document_tag');
    }

    public function getKnowledgeViewsByMonth($data)
    {

        $sql = "SELECT qb.dy as date_time,IFNULL(sum(number_of_views),0) as views, UNIX_TIMESTAMP(qb.dy) as date
		        FROM `knowledge_document` k
			    right join (
                    select curdate() as dy    union
                    select DATE_SUB(curdate(), INTERVAL 1 day) as dy     union
                    select DATE_SUB(curdate(), INTERVAL 2 day) as dy     union
                    select DATE_SUB(curdate(), INTERVAL 3 day) as dy     union
                    select DATE_SUB(curdate(), INTERVAL 4 day) as dy     union
                    select DATE_SUB(curdate(), INTERVAL 5 day) as dy     union
                    select DATE_SUB(curdate(), INTERVAL 6 day) as dy
                ) as qb
                on date(k.created_date_time) = qb.dy and k.company_id=" . $data['company_id'] . "
                GROUP BY DATE(qb.dy)
                ORDER BY qb.dy ASC";
        $query = $this->db->query($sql);
        return $query->result_array();

    }

    public function getKnowledgeUploadsByMonth($data)
    {
        $sql = "SELECT qb.dy as date_time,IFNULL(count(id_knowledge_document), 0) as uploads, UNIX_TIMESTAMP(qb.dy) as date
				FROM `knowledge_document` k
				right join (
				   select curdate() as dy    union
				   select DATE_SUB(curdate(), INTERVAL 1 day) as dy     union
				   select DATE_SUB(curdate(), INTERVAL 2 day) as dy     union
				   select DATE_SUB(curdate(), INTERVAL 3 day) as dy     union
				   select DATE_SUB(curdate(), INTERVAL 4 day) as dy     union
				   select DATE_SUB(curdate(), INTERVAL 5 day) as dy     union
				   select DATE_SUB(curdate(), INTERVAL 6 day) as dy
				   ) as qb 
				on date(k.created_date_time) = qb.dy and k.company_id=" . $data['company_id'] . "
				GROUP BY DATE(qb.dy)
				ORDER BY qb.dy ASC";
        $query = $this->db->query($sql);
        return $query->result_array();

    }


    public function getKnowledgeDocumentTags($data)
    {
        $this->db->select('*');
        $this->db->from('knowledge_document_tag');
        if(isset($data['knowledge_document_id']))
            $this->db->where('knowledge_document_id',$data['knowledge_document_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getcompanyUserByUserId($data)
    {
        $this->db->select('*');
        $this->db->from('company_user cu');
        $this->db->join('user u','cu.user_id=u.id_user','left');
        $this->db->join('company_approval_role cpr','cu.company_approval_role_id=cpr.id_company_approval_role','left');
        if(isset($data['user_id']))
            $this->db->where('cu.user_id',$data['user_id']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyUsersByCompanyApprovalRoles($data)
    {
        $this->db->select('cu.user_id');
        $this->db->from('company_user cu');
        $this->db->join('company_approval_role cpr', 'cu.company_approval_role_id=cpr.id_company_approval_role','left');
        $this->db->join('user u', 'cu.user_id=u.id_user','left');
        $this->db->join('company_branch cb', 'cu.branch_id=cb.id_branch','left');
        if(isset($data['company_id']))
            $this->db->where('cu.company_id', $data['company_id']);
        if(isset($data['company_approval_roles']) && !empty($data['company_approval_roles']))
            $this->db->where_in('cu.company_approval_role_id', $data['company_approval_roles']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyCurrencyDetails($data)
    {
        $this->db->select('cc.id_company_currency,cc.currency_id,c.*,ccv.company_currency_value,ccv.company_currency_value as currency_value');
        $this->db->from('company_currency cc');
        $this->db->join('currency c','cc.currency_id=c.id_currency','left');
        $this->db->join('company_currency_value ccv','cc.id_company_currency=ccv.id_company_currency_value','left');

        if(isset($data['company_id']))
            $this->db->where('cc.company_id',$data['company_id']);

        if(isset($data['currency_id']))
            $this->db->where('cc.currency_id',$data['currency_id']);

        if(isset($data['except_currency_id']))
            $this->db->where('cc.currency_id!=',$data['except_currency_id']);

        if(isset($data['company_currency_id']))
            $this->db->where('ccv.company_currency_id',$data['company_currency_id']);

        if(isset($data['company_currency_status']))
            $this->db->where('cc.company_currency_status',$data['company_currency_status']);

        $this->db->order_by('id_company_currency_value','DESC'); // using in facility adding
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCompanyCurrencySelected($data)
    {
        $this->db->select('c.*,IFNULL(cc.id_company_currency,0) as is_selected');
        $this->db->from('currency c');
        $this->db->join('company_currency cc','c.id_currency=cc.currency_id and company_id='.$data['company_id'].' and cc.company_currency_status=1 ','left');

        if(isset($data['except_currency_id']))
            $this->db->where('c.id_currency!=',$data['except_currency_id']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyCurrency($data)
    {
        /*$this->db->select('cc.*,c.currency_code');
        $this->db->from('company_currency cc');
        $this->db->join('currency c','cc.currency_id=c.id_currency','left');
        if(isset($data['comapny_id']))
            $this->db->where('cc.comapny_id',$data['comapny_id']);

        $query = $this->db->get();
        return $query->result_array();*/
        $sql = 'call getCompanyCurrency('.$data['company_id'].')';
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    public function addCompanyCurrency($data)
    {
        $this->db->insert('company_currency', $data);
        return $this->db->insert_id();
    }

    public function updateCompanyCurrency($data)
    {
        $this->db->where('id_company_currency',$data['id_company_currency']);
        $this->db->update('company_currency', $data);
        return 1;
    }

    public function addCompanyCurrencyBatch($data)
    {
        $this->db->insert_batch('company_currency', $data);
        return $this->db->insert_id();
    }

    public function updateCompanyCurrencyBatch($data)
    {
        $this->db->update_batch('company_currency', $data, 'id_company_currency');
        return 1;
    }



    public function addCompanyCurrencyValue($data)
    {
        $this->db->insert_batch('company_currency_value', $data);
        return $this->db->insert_id();
    }

    public function updateCompanyCurrencyValue($data)
    {
        $this->db->where('id_company_currency_value',$data['id_company_currency_value']);
        $this->db->update('company_currency_value', $data);
        return 1;
    }

    public function getForms()
    {
        $this->db->select('*');
        $this->db->from('form f');
        $query = $this->db->get();
        return $query->result_array();
    }

    /*public function getContactTouchPoints($data)
    {
        $sql = 'call getContactTouchPoints('.$data['crm_contact_id'].','.$data['company_id'].')';
        $query = $this->db->query($sql);
        $data =  $query->result_array();
        $this->Mcommon->clean_mysqli_connection($this->db->conn_id);
        return $data;
    }

    public function getCompanyTouchPoints($data)
    {
        $sql = 'call getCompanyTouchpoints('.$data['crm_company_id'].',"'.$data['sector'].'",'.$data['company_id'].')';
        $query = $this->db->query($sql);
        $data =  $query->result_array();
        $this->Mcommon->clean_mysqli_connection($this->db->conn_id);
        return $data;
    }

    public function getProjectTouchPoints($data)
    {
        $sql = 'call getProjectTouchpoints('.$data['crm_project_id'].',"'.$data['sector'].'",'.$data['company_id'].')';
        $query = $this->db->query($sql);
        $data =  $query->result_array();
        $this->Mcommon->clean_mysqli_connection($this->db->conn_id);
        return $data;
    }*/

    public function getTouchPoints($data)
    {
        $sql = 'call getTouchPoints('.$data['crm_module_id'].',"'.$data['sector'].'",'.$data['company_id'].',"'.$data['crm_module_type'].'","'.$data['offset'].'","'.$data['limit'].'")';
        $query = $this->db->query($sql);
        $data =  $query->result_array();
        $this->Mcommon->clean_mysqli_connection($this->db->conn_id);
        return $data;
    }

    public function getChildUsers($user_id)
    {
        $sql = 'call getChildUsers('.$user_id.')';
        $query = $this->db->query($sql);
        $data =  $query->result_array();
        $this->Mcommon->clean_mysqli_connection($this->db->conn_id);
        return $data;
    }

    public function updatePrimaryCurrency($data)
    {
        $this->db->where('id_company', $data['id_company']);
        $this->db->update('company', $data);
        return 1;
    }

    public function getCollateralType()
    {
        $this->db->select('t.*,IFNULL(a.id_assessment_question_category,0) as document_status,IFNULL(cts.id_collateral_type_stage,0) as checklist_status');
        $this->db->from('collateral_type t ');
        $this->db->join('assessment_question_category a','t.id_collateral_type=a.collateral_type_id','left');
        $this->db->join('collateral_type_stage cts','t.id_collateral_type=cts.collateral_type_id','left');
        $this->db->group_by('t.id_collateral_type');
        $this->db->order_by('t.id_collateral_type','asc');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCollateralStage()
    {
        $this->db->select('*');
        $this->db->from('collateral_stage');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralTypeFields($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_type_field');
        if(isset($data['collateral_type_id']))
            $this->db->where('collateral_type_id', $data['collateral_type_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralTypeStage($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_type_stage');
        if(isset($data['company_id']))
            $this->db->where('company_id', $data['company_id']);
        if(isset($data['collateral_type_id']))
            $this->db->where('collateral_type_id', $data['collateral_type_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralStageFields($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_stage_field');
        if(isset($data['collateral_stage_id']))
            $this->db->where('collateral_stage_id', $data['collateral_stage_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralStageForCompany($data)
    {
        $this->db->select('s.*,IFNULL(ts.status,0) as status');
        $this->db->from('collateral_stage s');
        $this->db->join('collateral_type_stage ts','s.id_collateral_stage=ts.collateral_stage_id and company_id="'.$data["company_id"].'" and collateral_type_id = "'.$data["collateral_type_id"].'"','left');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCollateralTypeStage($data)
    {
       $this->db->insert_batch('collateral_type_stage', $data);
       return $this->db->insert_id();
    }

    public function updateCollateralTypeStage($data)
    {
        $this->db->where('id_collateral_type_stage',$data['id_collateral_type_stage']);
        $this->db->update('collateral_type_stage', $data);
        return 1;
    }

    public function getCovenantType($data)
    {
        $this->db->select('*');
        $this->db->from('covenant_type');
        if(isset($data['covenant_type_key']))
            $this->db->where('covenant_type_key',$data['covenant_type_key']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCovenantCategoryList($data)
    {
        $this->db->select('*');
        $this->db->from('covenant_category cc');
        $this->db->join('covenant_type ct','cc.covenant_type_id=ct.id_covenant_type','left');
        if(isset($data['company_id']))
            $this->db->where('cc.company_id',$data['company_id']);
        if(isset($data['covenant_type_key']))
            $this->db->where('ct.covenant_type_key',$data['covenant_type_key']);
        if(isset($data['covenant_category']))
            $this->db->where('cc.covenant_category',$data['covenant_category']);
        if(isset($data['covenant_type_id']))
            $this->db->where('cc.covenant_type_id',$data['covenant_type_id']);
        if(isset($data['covenant_category_id_not']) && $data['covenant_category_id_not']!=0)
            $this->db->where('cc.id_covenant_category!=',$data['covenant_category_id_not']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCovenantCategory($data)
    {
        $this->db->select('*');
        $this->db->from('covenant_category cc');
        $this->db->join('covenant_type ct','cc.covenant_type_id=ct.id_covenant_type','left');
        $this->db->join('covenant c','cc.id_covenant_category=c.covenant_category_id','left');
        if(isset($data['company_id']))
            $this->db->where('cc.company_id',$data['company_id']);
        if(isset($data['covenant_type_key']))
            $this->db->where('ct.covenant_type_key',$data['covenant_type_key']);
        if(isset($data['covenant_category']))
            $this->db->where('cc.covenant_category',$data['covenant_category']);
        if(isset($data['covenant_type_id']))
            $this->db->where('cc.covenant_type_id',$data['covenant_type_id']);
        if(isset($data['covenant_category_id_not']) && $data['covenant_category_id_not']!=0)
            $this->db->where('cc.id_covenant_category!=',$data['covenant_category_id_not']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addCovenantCategory($data)
    {
        $this->db->insert('covenant_category', $data);
        return $this->db->insert_id();
    }

    public function updateCovenantCategory($data)
    {
        $this->db->where('id_covenant_category',$data['id_covenant_category']);
        $this->db->update('covenant_category', $data);
        return 1;
    }

    public function getCovenant($data)
    {
        $this->db->select('*');
        $this->db->from('covenant');
        if(isset($data['covenant_category_id']))
            $this->db->where('covenant_category_id',$data['covenant_category_id']);
        if(isset($data['covenant_name']))
            $this->db->where('covenant_name',$data['covenant_name']);
        if(isset($data['covenant_id_not']) && $data['covenant_id_not']!=0)
            $this->db->where('id_covenant!=',$data['covenant_id_not']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCovenant($data)
    {
        $this->db->insert('covenant', $data);
        return $this->db->insert_id();
    }

    public function updateCovenant($data)
    {
        $this->db->where('id_covenant',$data['id_covenant']);
        $this->db->update('covenant', $data);
        return 1;
    }

    /*public function getModules($data)
    {
        if(isset($data['type']) && $data['type']==1)
        {
            $this->db->select('m.*,mp.*,pa.*,prc.application_role_id,IF(prc.id_application_role_access,1,0) as checked');
            $this->db->from('module m');
            $this->db->join('module_page mp','m.id_module=mp.module_id','left');
            $this->db->join('page_action pa','mp.id_module_page=pa.module_page_id','left');
            $this->db->join('application_role_access prc','pa.id_page_action=prc.page_action_id and `prc`.`application_role_id` = '.$data['application_role_id'].'','left');

            if(isset($data['module_id'])){
                $this->db->where('m.id_module',$data['module_id']);
            }

        }
        else
        {
            $this->db->select('*');
            $this->db->from('module');
            if(isset($data['parent_module_id']))
                $this->db->where('parent_module_id',$data['parent_module_id']);
        }

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }*/

    public function getModules($data)
    {
        $this->db->select('m.*,mc.*,mcc.application_role_id,IF(mcc.id_module_access,1,0) as checked');
        $this->db->from('module m');
        $this->db->join('module_action mc','mc.module_id=m.id_module','left');
        $this->db->join('module_access mcc','mcc.module_action_id=mc.id_module_action and `mcc`.`application_role_id` = '.$data['application_role_id'].' and mcc.module_access_status=1','left');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getModulePage($data)
    {
        $this->db->select('*');
        $this->db->from('module_page');
        if(isset($data['module_id']))
            $this->db->where('module_id',$data['module_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getModuleAccess($data)
    {
        $this->db->select('*');
        $this->db->from('module_access');
        if(isset($data['module_access_id']))
            $this->db->where('id_module_access',$data['module_access_id']);
        if(isset($data['application_role_id']))
            $this->db->where('application_role_id',$data['application_role_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addModuleAccess($data)
    {
        $this->db->insert('module_access', $data);
        return $this->db->insert_id();
    }

    public function updateModuleAccess($data){
        $this->db->where(array('module_action_id' => $data['module_action_id'],'application_role_id' => $data['application_role_id']));
        $this->db->update('module_access', $data);
        return 1;
    }

    public function getApplicationRole($data)
    {
        $this->db->select('*');
        $this->db->from('application_role ar');
        //$this->db->join('application_user_role aur','ar.id_application_role=aur.application_role_id','left');
        if(isset($data['company_id']))
            $this->db->where('ar.company_id',$data['company_id']);
        if(isset($data['application_role_name']))
            $this->db->where('ar.application_role_name',$data['application_role_name']);
        if(isset($data['id_application_role_not']) && $data['id_application_role_not']!=0)
            $this->db->where('ar.id_application_role!=',$data['id_application_role_not']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addApplicationRole($data)
    {
        $this->db->insert('application_role', $data);
        return $this->db->insert_id();
    }

    public function updateApplicationRole($data)
    {
        $this->db->where('id_application_role',$data['id_application_role']);
        $this->db->update('application_role', $data);
        return 1;
    }

    public function getApplicationUserRole($data)
    {
        $this->db->select('ar.application_role_name,aur.*,apr.*,u.email_id,u.profile_image,CONCAT(u.first_name," ",u.last_name) as user_name');
        $this->db->from('application_user_role aur');
        $this->db->join('application_role ar','aur.application_role_id=ar.id_application_role','left');
        $this->db->join('company_approval_role car','aur.company_approval_role_id=car.id_company_approval_role','left');
        $this->db->join('approval_role apr','car.approval_role_id=apr.id_approval_role','left');
        $this->db->join('user u ','aur.user_id=u.id_user','left');
        if(isset($data['company_id']))
            $this->db->where('ar.company_id',$data['company_id']);
        if(isset($data['application_role_id']))
            $this->db->where('aur.application_role_id',$data['application_role_id']);
        if(isset($data['company_approval_role_id']))
            $this->db->where('aur.company_approval_role_id',$data['company_approval_role_id']);
        if(isset($data['user_id']))
            $this->db->where('aur.user_id',$data['user_id']);
        if(isset($data['id_application_user_role']))
            $this->db->where('aur.id_application_user_role',$data['id_application_user_role']);
        if(isset($data['type']) && $data['type']=='user')
            $this->db->where('aur.company_approval_role_id',0);
        else if(isset($data['type']) && $data['type']=='approval_role')
            $this->db->where('aur.user_id','');

        if(isset($data['status']) && $data['status']=='all'){  }
        else
            $this->db->where('aur.status',1);

        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function getApplicationUserRoleApprovalRole($data)
    {
        $this->db->select('company_approval_role_id');
        $this->db->from('application_user_role');
        if(isset($data['application_role_id']))
            $this->db->where('application_role_id',$data['application_role_id']);
        if(isset($data['user_id']))
            $this->db->where('user_id',$data['user_id']);
        if(isset($data['status']))
            $this->db->where('status',$data['status']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addApplicationUserRole($data)
    {
        $this->db->insert('application_user_role', $data);
        return $this->db->insert_id();
    }

    public function addApplicationUserRoleBatch($data)
    {
        $this->db->insert_batch('application_user_role', $data);
        return $this->db->insert_id();
    }

    public function updateApplicationUserRole($data)
    {
        $this->db->where('id_application_user_role',$data['id_application_user_role']);
        $this->db->update('application_user_role', $data);
        return 1;
    }

    /*public function updateApplicationUserRole($data)
    {
        $this->db->where(array('application_role_id' => $data['application_role_id'],'company_approval_role_id' => $data['company_approval_role_id']));
        $this->db->update('application_user_role', $data);
        return 1;
    }*/

    public function getApplicationRoleAccess($data)
    {
        $this->db->select('*');
        $this->db->from('application_role_access');
        if(isset($data['company_id']))
            $this->db->where('company_id',$data['company_id']);
        if(isset($data['application_role_id']))
            $this->db->where('application_role_id',$data['application_role_id']);
        if(isset($data['module_id']))
            $this->db->where('module_id',$data['module_id']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function addApplicationRoleAccess($data)
    {
        $this->db->insert('application_role_access', $data);
        return $this->db->insert_id();
    }

    public function updateApplicationRoleAccess($data)
    {
        $this->db->where(array('application_role_id' => $data['application_role_id'], 'page_action_id' => $data['page_action_id'], 'company_id' => $data['company_id']));
        $this->db->update('application_role_access', $data);
        return 1;
    }

    public function getUserListForApplicationRole($data)
    {
        $this->db->select('u.user_role_id,u.id_user,u.first_name,u.last_name,u.email_id,u.phone_number,u.address,u.city,u.state,u.zip_code,u.profile_image,u.created_date_time');
        $this->db->from('company_user cu');
        $this->db->join('user u', 'u.id_user = cu.user_id', 'left');


        if(isset($data['company_id']))
            $this->db->where(array('cu.company_id' => $data['company_id'],'u.user_role_id' => 3));

        $this->db->where('cu.user_id NOT IN (select user_id from application_user_role where application_role_id='.$data['application_role_id'].')');

        $this->db->order_by('cu.id_company_user','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getApprovalRoleListForApplicationRole($data)
    {
        $this->db->select('car.id_company_approval_role,ar.approval_name');
        $this->db->from('company_approval_role car');
        $this->db->join('approval_role ar','car.approval_role_id=ar.id_approval_role','left');
        if(isset($data['company_id']))
            $this->db->where(array('car.company_id' => $data['company_id']));

        $this->db->where('car.id_company_approval_role NOT IN (select company_approval_role_id from application_user_role where application_role_id='.$data['application_role_id'].')');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function deleteApplicationUserRole($data)
    {
        $this->db->where('id_application_user_role',$data['id_application_user_role'])->delete('application_user_role');
        return 1;
    }

    public function addProjectCovenant($data)
    {
        $this->db->insert('project_covenant', $data);
        return $this->db->insert_id();
    }

}