<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Crm_model extends CI_Model
{

    public function getContactList($data)
    {
        //$created_by = $this->allowed_created_by;
        //$allowed_company_approval_roles = $this->allowed_company_approval_roles;
        if($this->current_user)
        {
            $user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);
        }

        $this->db->select('c.*,cc.company_name,ccd.designation_name as designation,CONCAT(u.first_name,\' \',u.last_name) as username,cb.legal_name');
        $this->db->from('crm_contact c');
        $this->db->join('crm_company_contact ccc','c.id_crm_contact=ccc.crm_contact_id and is_primary_company=1 and crm_company_contact_status=1','left');
        $this->db->join('crm_company cc','cc.id_crm_company=ccc.crm_company_id','left');
        $this->db->join('crm_company_designation ccd','ccd.id_crm_company_designation=ccc.crm_company_designation_id','left');
        $this->db->join('company_user cu','cu.user_id=c.created_by','left');
        $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
        //$this->db->join('company_approval_role car','car.id_company_approval_role=cu.company_approval_role_id','left');
        //$this->db->join('approval_role ar','ar.id_approval_role=car.approval_role_id','left');
        $this->db->join('user u','u.id_user=c.created_by','left');
        $this->db->where('c.company_id',$data['company_id']);
        //$this->db->where('ccc.is_primary_company','1');
        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(c.first_name like "%'.$data['search_key'].'%" or c.last_name like "%'.$data['search_key'].'%" or c.email like "%'.$data['search_key'].'%" or c.phone_number like "%'.$data['search_key'].'%")');
        }
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        if(isset($data['crm_project_id'])){
            $this->db->where('id_crm_contact NOT IN (SELECT crm_contact_id FROM project_contact where crm_project_id='.$data['crm_project_id'].' and project_contact_status=1)', NULL, FALSE);
            $this->db->where('c.crm_contact_type_id !=',1);
        }

        /*if(!empty($created_by))
            $this->db->where_in('c.created_by ',$this->allowed_created_by);*/
        if(!isset($data['crm_project_id'])) {
            if ($this->current_user)
                $this->db->where_in('c.created_by ', $user_list);
        }

        /*if(!empty($allowed_company_approval_roles))
            $this->db->where_in('cu.company_approval_role_id',$this->allowed_company_approval_roles);*/


        $this->db->group_by('id_crm_contact');
        $this->db->order_by('id_crm_contact','DESC');



        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function totalContactList($data)
    {
        //print_r($data);
        $this->db->select('c.*,cc.company_name,ccd.designation_name as designation');
        $this->db->from('crm_contact c');
        $this->db->join('crm_company_contact ccc','c.id_crm_contact=ccc.crm_contact_id and is_primary_company=1 and crm_company_contact_status=1','left');
        $this->db->join('crm_company cc','cc.id_crm_company=ccc.crm_company_id','left');
        $this->db->join('crm_company_designation ccd','ccd.id_crm_company_designation=ccc.crm_company_designation_id','left');
        $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('(c.first_name like "%'.$data['search_key'].'%" or c.last_name like "%'.$data['search_key'].'%" or c.email like "%'.$data['search_key'].'%" or c.phone_number like "%'.$data['search_key'].'%")');
        }
        if(isset($data['crm_project_id'])){
            $this->db->where('id_crm_contact NOT IN (SELECT crm_contact_id FROM project_contact where crm_project_id='.$data['crm_project_id'].' and project_contact_status=1)', NULL, FALSE);
            $this->db->where('c.crm_contact_type_id !=',1);
        }

        $this->db->group_by('id_crm_contact');
        $this->db->order_by('id_crm_contact','DESC');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getContact($contact_id)
    {
        $this->db->select('c.*,group_concat(cc.company_name) as company_name');
        $this->db->from('crm_contact c');
        $this->db->join('crm_company_contact ccc','c.id_crm_contact=ccc.crm_contact_id and ccc.crm_company_contact_status=1','left');
        $this->db->join('crm_company cc','ccc.crm_company_id=cc.id_crm_company','left');
        $this->db->where('id_crm_contact',$contact_id);
        $this->db->Group_by('id_crm_contact');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getModuleDetails($module_id)
    {
        $this->db->select('*');
        $this->db->from('crm_module m');
        $this->db->join('section s','s.crm_module_id=m.id_crm_module','left');
        $this->db->join('form f','f.section_id=s.id_section','left');
        $this->db->where('m.id_crm_module',$module_id);
        $this->db->order_by('s.section_order','asc');
        $this->db->order_by('f.form_order','asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSectionsByModuleId($module_id)
    {
        $this->db->get_where('section',array('crm_module_id' => $module_id));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getContactFormData($contact_id,$form_id)
    {
        $this->db->select('*');
        $this->db->from('crm_contact_data c');
        //$this->db->join('crm_contact cc','cc.id_crm_contact=c.crm_contact_id','left');
        $this->db->join('form_field f','c.form_field_id=f.id_form_field','left');
        $this->db->where(array('c.crm_contact_id' => $contact_id,'f.form_id' => $form_id));
        $this->db->order_by('f.order','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFormInformation($form_id)
    {
        $this->db->select('*');
        $this->db->from('form f');
        $this->db->where('f.id_form', $form_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFormFieldsByFormId($form_id)
    {
        $this->db->select('*');
        $this->db->from('form f');
        $this->db->join('form_field ff','ff.form_id=f.id_form','left');
        $this->db->where('form_id', $form_id);
        $this->db->order_by('ff.order','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCrmContact($data)
    {
        $this->db->insert('crm_contact', $data);
        return $this->db->insert_id();
    }

    public function updateCrmContact($data,$crm_contact_id)
    {
        $this->db->where('id_crm_contact', $crm_contact_id);
        $this->db->update('crm_contact', $data);
    }

    public function addCrmContactData($data)
    {
        $this->db->insert_batch('crm_contact_data', $data);
        return $this->db->insert_id();
    }

    public function updateCrmContactData($data,$type)
    {
        if($type==1)
        {
            $id_crm_contact_data = $data[0]['id_crm_contact_data'];
            unset($data[0]['id_crm_contact_data']);
            $this->db->where('id_crm_contact_data', $id_crm_contact_data);
            $this->db->update('crm_contact_data', $data[0]);
        }
        else{
            $this->db->update_batch('crm_contact_data', $data, 'id_crm_contact_data');
        }

        return 1;
    }

    public function getFieldPercentage($crm_id,$type)
    {
        $this->db->select('count(*) as count');
        $this->db->from('form_field ff');
        $this->db->join('form f','f.id_form=ff.form_id','left');
        $this->db->join('section s','s.id_section=f.section_id','left');
        $this->db->join('crm_module m','m.id_crm_module=s.crm_module_id','left');
        $this->db->where('m.crm_module_name', $type);
        $query = $this->db->get();
        $total_field_count = $query->result_array();

        $table = 'crm_'.$type.'_data';
        $id = 'crm_'.$type.'_id';

        $this->db->select('count(*) as count');
        $this->db->from($table);
        $this->db->where(array($id => $crm_id,'form_field_value!=' => ''));
        $query = $this->db->get();
        $total_data_count = $query->result_array();
        return $total_field_count[0]['count'].','.$total_data_count[0]['count'];
    }

    public function getFormPercentage($form_id,$type,$crm_id)
    {
        $this->db->select('count(*) as count');
        $this->db->from('form_field ff');
        $this->db->join('form f','f.id_form=ff.form_id','left');
        $this->db->where('f.id_form', $form_id);
        $query = $this->db->get();
        $total_field_count = $query->result_array();

        $table = 'crm_'.$type.'_data';
        $id = 'crm_'.$type.'_id';
        $this->db->select('count(*) as count');
        $this->db->from($table.' d');
        $this->db->join('form_field ff','d.form_field_id=ff.id_form_field','left');
        $this->db->where(array('ff.form_id' => $form_id,'d.form_field_value !=' => '', $id => $crm_id));
        $query = $this->db->get();
        $total_data_count = $query->result_array();
        return $total_field_count[0]['count'].','.$total_data_count[0]['count'];
    }

    public function getCompany($crm_company_id)
    {
        $this->db->select('c.*,s.sector_name as sector,s1.sector_name as sub_sector,group_concat(cc.first_name) as contact_name');
        $this->db->from('crm_company c');
        $this->db->join('sector s','c.sector_id=s.id_sector','left');
        $this->db->join('sector s1','c.sub_sector_id=s1.id_sector','left');
        $this->db->join('crm_company_contact ccc','c.id_crm_company=ccc.crm_company_id and crm_company_contact_status=1','left');
        $this->db->join('crm_contact cc','ccc.crm_contact_id=cc.id_crm_contact','left');
        $this->db->where('id_crm_company',$crm_company_id);
        $this->db->group_by('id_crm_company');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyList($data)
    {
        //$created_by = $this->allowed_created_by;
        //$allowed_company_approval_roles = $this->allowed_company_approval_roles;


        if($this->current_user)
        {
            $user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);
        }

        $this->db->select('c.*,CONCAT(u.first_name,\' \',u.last_name) as username,cb.legal_name');
        $this->db->from('crm_company c');
        $this->db->join('company_user cu','cu.user_id=c.created_by','left');
        $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
        //$this->db->join('company_approval_role car','car.id_company_approval_role=cu.company_approval_role_id','left');
        //$this->db->join('approval_role ar','ar.id_approval_role=car.approval_role_id','left');
        $this->db->join('user u','u.id_user=c.created_by','left');
        $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('company_name like "%'.$data['search_key'].'%"');
        }
        if(isset($data['crm_project_id'])){
            //$this->db->where('id_crm_company IN (SELECT crm_company_id FROM crm_company_contact ccc left join project_contact pc on ccc.crm_contact_id=pc.crm_contact_id where pc.crm_project_id='.$data['crm_project_id'].' and crm_company_contact_status=1 and pc.project_contact_status)', NULL, FALSE);
            $this->db->where('id_crm_company NOT IN (SELECT crm_company_id FROM project_company where crm_project_id='.$data['crm_project_id'].' and project_company_status=1)', NULL, FALSE);
        }

        /*if(!empty($created_by))
            $this->db->where_in('c.created_by',$this->allowed_created_by);*/
        /*if(!empty($allowed_company_approval_roles))
            $this->db->where_in('cu.company_approval_role_id',$this->allowed_company_approval_roles);*/

        if(!isset($data['crm_project_id'])){
            if($this->current_user)
                $this->db->where_in('c.created_by ',$user_list);
        }


        $this->db->order_by('id_crm_company','DESC');

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function totalCompanyList($data)
    {
        $this->db->select('*');
        $this->db->from('crm_company c');
        $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('company_name like "%'.$data['search_key'].'%"');
        }

        if(isset($data['crm_project_id'])){
            $this->db->where('id_crm_company IN (SELECT crm_company_id FROM crm_company_contact ccc left join project_contact pc on ccc.crm_contact_id=pc.crm_contact_id where pc.crm_project_id='.$data['crm_project_id'].' and crm_company_contact_status=1 and pc.project_contact_status)', NULL, FALSE);
            $this->db->where('id_crm_company NOT IN (SELECT crm_company_id FROM project_company where crm_project_id='.$data['crm_project_id'].' and project_company_status=1)', NULL, FALSE);
        }

        $this->db->order_by('id_crm_company','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCompanyFormData($crm_company_id,$form_id)
    {
        $this->db->select('c.*,f.*');
        //$this->db->from('crm_company cc');
        $this->db->from('crm_company_data c');
        $this->db->join('form_field f','c.form_field_id=f.id_form_field','left');
        $this->db->where(array('c.crm_company_id' => $crm_company_id,'f.form_id' => $form_id));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCrmCompany($data)
    {
        $this->db->insert('crm_company', $data);
        return $this->db->insert_id();
    }

    public function addCrmCompanyData($data)
    {
        $this->db->insert_batch('crm_company_data', $data);
        return $this->db->insert_id();
    }

    public function updateCrmCompanyData($data,$type)
    {
        //echo "<pre>"; print_r($data); exit;
        if($type==1)
        {
            $id_crm_company_data = $data[0]['id_crm_company_data'];
            unset($data[0]['id_crm_company_data']);
            $this->db->where('id_crm_company_data', $id_crm_company_data);
            $this->db->update('crm_company_data', $data[0]);
        }
        else{
            $this->db->update_batch('crm_company_data', $data, 'id_crm_company_data');
        }
        return 1;
    }

    public function updateCrmCompany($data,$crm_company_id)
    {
        $this->db->where('id_crm_company', $crm_company_id);
        $this->db->update('crm_company', $data);
    }

    public function addQuickContact($data)
    {
        $this->db->insert('crm_contact', $data);
        return $this->db->insert_id();
    }

    public function addQuickCompany($data)
    {
        $this->db->insert('crm_company', $data);
        return $this->db->insert_id();
    }

    public function addQuickProject($data)
    {
        $data['updated_date_time'] = date('Y-m-d H:i:s');
        $this->db->insert('crm_project', $data);
        return $this->db->insert_id();
    }

    public function addDocument($data)
    {
        $this->db->insert('crm_document', $data);
        return $this->db->insert_id();
    }

    public function getModuleByDocumentType($document_type)
    {
        $this->db->select('*');
        $this->db->from('crm_document cd');
        $this->db->join('crm_document_type cdt','cd.crm_document_type_id=cdt.id_crm_document_type','left');
        $this->db->where('cd.crm_document_type_id',$document_type);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDocumentTypeByName($document_type_name)
    {
        $query = $this->db->get_where('crm_document_type',array('document_type_name' => $document_type_name));
        /*$this->db->select('*');
        $this->db->from('crm_document_type');
        $this->db->where('crm_document_type',array('document_type_name' => $document_type_name));
        $query = $this->db->get();*/
        return $query->row();
    }

    public function getDocument($data){
        $this->db->select('c.*,cd.document_type_name,DATE(c.created_date_time) as date');
        $this->db->from('crm_document c');
        $this->db->join('crm_document_type cd','cd.id_crm_document_type=c.crm_document_type_id','left');
        $this->db->where('cd.crm_module_id',$data['module_id']);
        $this->db->where('c.uploaded_from_id',$data['group_id']);
        if(isset($data['type']))
        {
            if($data['type']=='document'){
                $this->db->where('c.id_crm_document',$data['crm_document_id']);
            }
            else if($data['type']=='document_type'){
                $this->db->where('c.crm_document_type_id',$data['crm_document_type_id']);
            }
        }
        $this->db->order_by('c.id_crm_document','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDocumentType($module_id)
    {
        $query = $this->db->get_where('crm_document_type',array('crm_module_id' => $module_id));
        return $query->result_array();
    }

    public function getProject($crm_project_id)
    {
        $this->db->select('c.*,DATE_FORMAT(c.updated_date_time,"%b %d %Y") as updated_date,p.product_name,s.sector_name as sector,s1.sector_name as sub_sector,cr.currency_code');
        $this->db->from('crm_project c');
        $this->db->join('company cm','c.company_id=cm.id_company','left');
        $this->db->join('currency cr','cm.currency_id=cr.id_currency','left');
        $this->db->join('product p','c.product_id=p.id_product','left');
        $this->db->join('sector s','c.project_main_sector_id=s.id_sector','left');
        $this->db->join('sector s1','c.project_sub_sector_id=s1.id_sector','left');
        $this->db->where('id_crm_project',$crm_project_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectList($data)
    {
        //$created_by = $this->allowed_created_by;
        //$allowed_company_approval_roles = $this->allowed_company_approval_roles;

        if($this->current_user)
        {
            $user_list = $this->Company_model->getChildUsers($this->current_user);
            $user_list = array_map("reset", $user_list);
            array_push($user_list,$this->current_user);
        }

        $this->db->select('c.*,s.sector_name as main_sector_name,ss.sector_name as sub_sector_name,CONCAT(u.first_name,\' \',u.last_name) as username,cb.legal_name,cur.currency_code');
        $this->db->from('crm_project c');
        $this->db->join('sector s','s.id_sector=c.project_main_sector_id','left');
        $this->db->join('sector ss','ss.id_sector=c.project_sub_sector_id','left');
        $this->db->join('company_user cu','cu.user_id=c.created_by','left');
        $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
        $this->db->join('currency cur','c.currency_id=cur.id_currency','left');
        //$this->db->join('company_approval_role car','car.id_company_approval_role=cu.company_approval_role_id','left');
        //$this->db->join('approval_role ar','ar.id_approval_role=car.approval_role_id','left');
        $this->db->join('user u','u.id_user=c.created_by','left');

        $this->db->where('c.company_id',$data['company_id']);

        if(isset($data['crm_project_id']) && $data['crm_project_id']!='' && $data['crm_project_id']!=0){
            $this->db->where('c.id_crm_project',$data['crm_project_id']);
            $created_by = array();
        }

        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('project_title like "%'.$data['search_key'].'%"');
        }


        /*if(!empty($created_by)) {
            $this->db->where_in('c.created_by', $this->allowed_created_by);
            $this->db->or_where_in('c.assigned_to', $this->allowed_created_by);
        }*/

        if($this->current_user) {
            /*$this->db->where_in('c.created_by ', $user_list);
            $this->db->or_where_in('c.assigned_to', $this->current_user);*/
            if(is_array($user_list)){ $user_list = implode(',',$user_list); }
            if(is_array($this->current_user)){ $this->current_user = implode(',',$this->current_user); }
            $this->db->where('(c.created_by in ('.$user_list.') or c.assigned_to in ('.$this->current_user.'))');
        }

        /*if(!empty($allowed_company_approval_roles))
        {
            $this->db->where_in('cu.company_approval_role_id',$this->allowed_company_approval_roles);
            if($this->current_user)
                $this->db->or_where_in('c.assigned_to', $this->current_user);
        }*/



        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $this->db->order_by('id_crm_project','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function totalProjectList($data)
    {
        $this->db->select('c.*,s.sector_name as main_sector_name,ss.sector_name as sub_sector_name');
        $this->db->from('crm_project c');
        $this->db->where('c.company_id',$data['company_id']);
        $this->db->join('sector s','s.id_sector=c.project_main_sector_id','left');
        $this->db->join('sector ss','ss.id_sector=c.project_sub_sector_id','left');
        if(isset($data['search_key']) && $data['search_key']!=''){
            $this->db->where('project_title like "%'.$data['search_key'].'%"');
        }

        $this->db->order_by('id_crm_project','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectFormData($crm_project_id,$form_id)
    {
        $this->db->select('c.*,f.*');
        //$this->db->from('crm_company cc');
        $this->db->from('crm_project_data c');
        $this->db->join('form_field f','c.form_field_id=f.id_form_field','left');
        $this->db->where(array('c.crm_project_id' => $crm_project_id,'f.form_id' => $form_id));
        $this->db->order_by('f.order','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCrmProject($data)
    {
        $this->db->insert('crm_project', $data);
        return $this->db->insert_id();
    }


    public function addCrmProjectData($data)
    {
        $this->db->insert_batch('crm_project_data', $data);
        return $this->db->insert_id();
    }

    public function updateCrmProjectData($data,$type)
    {
        //echo $type."<pre>"; print_r($data); exit;
        if($type==1)
        {
            $id_crm_company_data = $data[0]['id_crm_project_data'];
            unset($data[0]['id_crm_project_data']);
            $this->db->where('id_crm_project_data', $id_crm_company_data);
            $this->db->update('crm_project_data', $data[0]);
        }
        else{
            $this->db->update_batch('crm_project_data', $data, 'id_crm_project_data');
        }
        return 1;
    }

    public function updateCrmProject($data,$crm_project_id)
    {
        $this->db->where('id_crm_project', $crm_project_id);
        $this->db->update('crm_project', $data);
    }

    public function getCrmCompanyIdByModuleId($group_id,$document_type_id)
    {
        $table = ''; $where = '';
        if($document_type_id==1){ $table = 'crm_contact'; $where = array('id_crm_contact' => $group_id); }
        else if($document_type_id==2){ $table = 'crm_company'; $where = array('id_crm_company' => $group_id); }
        else if($document_type_id==3){ $table = 'crm_project'; $where = array('id_crm_project' => $group_id); }

        $query = $this->db->get_where($table,$where);
        //echo $this->db->last_query();
        return $query->row();
    }

    public function getCompanyDesignation()
    {
        $query = $this->db->get('crm_company_designation');
        return $query->result_array();
    }

    public function getCrmListForMapping($data)
    {
        if(isset($data['crm_contact_id']))
        {
            $crm_company_id = '';
            if($data['crm_contact_id']!=0){
                $contact_details = $this->getContact($data['crm_contact_id']);
                $crm_company_id = $contact_details[0]['company_id'];
            }

            /*$this->db->select('*');
            $this->db->from('crm_company_contact ccc');
            $this->db->join('crm_company cc','ccc.crm_company_id=cc.id_crm_company','right');
            $this->db->where('ccc.crm_contact_id',$data['crm_contact_id']);*/
            $this->db->select('*');
            $this->db->from('crm_company cc');
            $this->db->where('cc.company_id',$data['company_id']);
            if($data['crm_contact_id']!=0)
                $this->db->where('id_crm_company NOT IN (SELECT crm_company_id FROM crm_company_contact where crm_contact_id='.$data['crm_contact_id'].' and crm_company_contact_status=1)', NULL, FALSE);
            $this->db->where('company_name like "%'.$data['search_key'].'%"');
            if($data['crm_contact_id']!=0)
                $this->db->where('cc.company_id=',$crm_company_id);
        }
        else if(isset($data['crm_company_id']))
        {
            if($data['crm_company_id']!=0){
                $company_details = $this->getCompany($data['crm_company_id']);
                $crm_company_id = $company_details[0]['company_id'];
            }

            /*$this->db->select('*');
            $this->db->from('crm_company_contact ccc');
            $this->db->join('crm_contact cc','ccc.crm_contact_id=cc.id_crm_contact','right');
            $this->db->where('ccc.crm_company_id',$data['crm_company_id']);*/
            $this->db->select('*');
            $this->db->from('crm_contact cc');
            $this->db->where('cc.company_id',$data['company_id']);
            $this->db->where('cc.crm_contact_type_id !=',1);
            if($data['crm_company_id']!=0)
                $this->db->where('id_crm_contact NOT IN (SELECT crm_contact_id FROM crm_company_contact where crm_company_id='.$data['crm_company_id'].'  and crm_company_contact_status=1)', NULL, FALSE);
            $this->db->where('(first_name like "%'.$data['search_key'].'%" or last_name like "%'.$data['search_key'].'%" or email like "%'.$data['search_key'].'%")');
            if($data['crm_company_id']!=0)
                $this->db->where('cc.company_id=',$crm_company_id);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    public function mapModule($data)
    {
        $this->db->insert('crm_company_contact', $data);
        return $this->db->insert_id();
    }

    public function checkCompanyContactExists($data)
    {
        $this->db->select('*');
        $this->db->from('crm_company_contact');
        $this->db->where(array('crm_contact_id' => $data['crm_contact_id'], 'crm_company_id' => $data['crm_company_id']));
        $query = $this->db->get();
        return $query->row();
    }

    public function updateCompanyContactPrimary($crm_contact_id,$data)
    {
        $this->db->where('crm_contact_id', $crm_contact_id);
        $this->db->update('crm_company_contact', $data);
        return 1;
    }

    public function updateCompanyContact($crm_company_contact_id,$data)
    {
        $this->db->where('id_crm_company_contact',$crm_company_contact_id);
        $this->db->update('crm_company_contact', $data);
        return 1;
    }

    public function deleteCompanyContact($crm_company_contact_id)
    {
        $this->db->where('id_crm_company_contact', $crm_company_contact_id);
        $this->db->update('crm_company_contact', array('crm_company_contact_status' => '0'));
        return 1;
    }

    public function getMapModule($data)
    {
        if(isset($data['crm_contact_id']))
        {
            $this->db->select('*');
            $this->db->from('crm_company_contact m');
            $this->db->join('crm_company c','c.id_crm_company=m.crm_company_id','left');
            $this->db->join('crm_company_designation d','d.id_crm_company_designation=m.crm_company_designation_id','left');
            $this->db->where('m.crm_contact_id',$data['crm_contact_id']);
            $this->db->where('m.crm_company_contact_status','1');
            $this->db->order_by('m.id_crm_company_contact','DESC');
        }
        else if(isset($data['crm_company_id']))
        {
            $this->db->select('*');
            $this->db->from('crm_company_contact m');
            $this->db->join('crm_contact c','c.id_crm_contact=m.crm_contact_id','left');
            $this->db->join('crm_company_designation d','d.id_crm_company_designation=m.crm_company_designation_id','left');
            $this->db->where('m.crm_company_id',$data['crm_company_id']);
            $this->db->where('m.crm_company_contact_status','1');
            $this->db->order_by('m.id_crm_company_contact','DESC');
        }
        else if(isset($data['crm_project_id']))
        {

        }

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getContactType()
    {
        $query = $this->db->get('contact_type');
        return $query->result_array();
    }

    public function getCrmContactType()
    {
        $query = $this->db->get('crm_contact_type');
        return $query->result_array();
    }

    public function getProjectContact($data)
    {
        $this->db->select('*');
        $this->db->from('project_contact m');
        $this->db->join('crm_contact c','c.id_crm_contact=m.crm_contact_id','left');
        $this->db->where('m.crm_project_id',$data['crm_project_id']);
        $this->db->where('m.project_contact_status','1');
        $this->db->order_by('m.id_project_contact','DESC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function addProjectContact($data)
    {
        $this->db->insert('project_contact', $data);
        return $this->db->insert_id();
    }

    public function checkProjectContactExists($data)
    {
        $this->db->select('*');
        $this->db->from('project_contact');
        $this->db->where(array('crm_contact_id' => $data['crm_contact_id'], 'crm_project_id' => $data['crm_project_id']));
        $query = $this->db->get();
        return $query->row();
    }

    public function updateProjectContactPrimary($crm_project_id,$data)
    {
        $this->db->where('crm_project_id', $crm_project_id);
        $this->db->update('project_contact', $data);

        return 1;
    }

    public function updateProjectContact($project_contact_id,$data)
    {
        $this->db->where('id_project_contact',$project_contact_id);
        $this->db->update('project_contact', $data);
        return 1;
    }

    public function deleteProjectContact($project_contact_id)
    {
        $this->db->where('id_project_contact', $project_contact_id);
        $this->db->update('project_contact', array('project_contact_status' => '0'));
        return 1;
    }

    public function getCompanyType()
    {
        $query = $this->db->get('company_type');
        return $query->result_array();
    }

    public function getProjectCompany($data)
    {
        $this->db->select('*');
        $this->db->from('project_company m');
        $this->db->join('crm_company c','c.id_crm_company=m.crm_company_id','left');
        $this->db->where('m.crm_project_id',$data['crm_project_id']);
        $this->db->where('m.project_company_status','1');
        $this->db->order_by('m.id_project_company','DESC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function addProjectCompany($data)
    {
        $this->db->insert('project_company', $data);
        return $this->db->insert_id();
    }

    public function checkProjectCompanyExists($data)
    {
        $this->db->select('*');
        $this->db->from('project_company');
        $this->db->where(array('crm_company_id' => $data['crm_company_id'], 'crm_project_id' => $data['crm_project_id']));
        $query = $this->db->get();
        return $query->row();
    }

    public function updateProjectCompany($project_company_id,$data)
    {
        $this->db->where('id_project_company',$project_company_id);
        $this->db->update('project_company', $data);
        return 1;
    }

    public function deleteProjectCompany($project_company_id)
    {
        $this->db->where('id_project_company', $project_company_id);
        $this->db->update('project_company', array('project_company_status' => '0'));
        return 1;
    }

    public function getTouchPointType()
    {
        $this->db->select('*');
        $this->db->from('crm_touch_point_type');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTouchPoints($data)
    {
        /*$this->db->select('*');*/

        $this->db->select("GROUP_CONCAT(crm_touch_point_type_id SEPARATOR  '@@@') as touch_point_type_id,GROUP_CONCAT(crm_touch_point_type SEPARATOR  '@@@') as touch_point_type_name,GROUP_CONCAT(touch_point_description SEPARATOR  '@@@') as touch_point_description,GROUP_CONCAT(DATE(p.created_date_time) SEPARATOR  '@@@') as created_date,GROUP_CONCAT(reminder_date SEPARATOR  '@@@') as reminder_date,GROUP_CONCAT(reminder_time SEPARATOR  '@@@') as reminder_time,GROUP_CONCAT(is_reminder SEPARATOR  '@@@') as reminder,DATE(p.reminder_date) as date,GROUP_CONCAT(CONCAT(u.first_name,' ',u.last_name) SEPARATOR  '@@@') as user_name");
        $this->db->from('crm_touch_point p');
        $this->db->join('user u','p.created_by=u.id_user','left');
        $this->db->join('crm_touch_point_type pt','p.crm_touch_point_type_id=pt.id_crm_touch_point_type','left');

        if(isset($data['crm_module_id']))
            $this->db->where('crm_module_id',$data['crm_module_id']);

        if(isset($data['from_id']))
            $this->db->where('touch_point_from_id',$data['from_id']);

        if(isset($data['is_reminder'])){ //results for getting only feature reminders
            $this->db->where('p.is_reminder=','1');
            $this->db->where('DATE(p.reminder_date)>=NOW()');
        }

        if(isset($data['crm_touch_point_type_id']) && $data['crm_touch_point_type_id']!='' && $data['crm_touch_point_type_id']!='0' && $data['crm_touch_point_type_id']!='-1')
            $this->db->where('crm_touch_point_type_id',$data['crm_touch_point_type_id']);

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        if(!isset($data['is_all']) && !isset($data['is_reminder']))
            $this->db->where('DATE(p.reminder_date)<=NOW()');


        $this->db->group_by('DATE(p.reminder_date)');
        $this->db->order_by('p.reminder_date','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTotalTouchPoints($data)
    {
        $this->db->select('*');
        $this->db->from('crm_touch_point');
        if(isset($data['crm_module_id']))
            $this->db->where('crm_module_id',$data['crm_module_id']);
        if(isset($data['from_id']))
            $this->db->where('touch_point_from_id',$data['from_id']);
        if(isset($data['is_reminder'])){ //results for getting only feature reminders
            $this->db->where('is_reminder=','1');
            $this->db->where('DATE(reminder_date)>=NOW()');
        }
        else if(isset($data['crm_touch_point_type_id']) && $data['crm_touch_point_type_id']!='' && $data['crm_touch_point_type_id']!='0' && $data['crm_touch_point_type_id']!='-1')
            $this->db->where('crm_touch_point_type_id',$data['crm_touch_point_type_id']);

        if(!isset($data['is_all']) && isset($data['is_all'])==true)
            $this->db->where('DATE(p.reminder_date)<=NOW()');


        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getTouchPointReminder($data)
    {
        $this->db->select('*');
        $this->db->from('crm_touch_point');
        if(isset($data['crm_module_id']))
            $this->db->where('crm_module_id',$data['crm_module_id']);
        if(isset($data['from_id']))
            $this->db->where('touch_point_from_id',$data['from_id']);

        $this->db->where('is_reminder=','1');
        $this->db->where('DATE(reminder_date)>=NOW()');
        $this->db->order_by('DATE(reminder_date)','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addTouchPoint($data)
    {
        $this->db->insert('crm_touch_point', $data);
        return $this->db->insert_id();
    }

    public function getProduct($data)
    {
        $this->db->select('*,DATE(p.created_date) as date');
        $this->db->from('product p');
        $this->db->join('company_approval_structure cas','p.company_approval_structure_id=cas.id_company_approval_structure','left');

        if(isset($data['company_id']))
            $this->db->where('p.company_id',$data['company_id']);
        if(isset($data['company_approval_structure_id']))
            $this->db->where('p.company_approval_structure_id',$data['company_approval_structure_id']);
        if(isset($data['id_product']))
            $this->db->where('p.id_product',$data['id_product']);

        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);

        //$this->db->group_by('p.id_product');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addProduct($data)
    {
        $this->db->insert('product', $data);
        return $this->db->insert_id();
    }

    public function updateProduct($data)
    {
        $this->db->where('id_product',$data['id_product']);
        $this->db->update('product', $data);
        return 1;
    }

    public function getProjectTeam($data)
    {
        $this->db->select('t.*,cu.*,u.id_user,u.user_role_id,u.first_name,u.last_name,u.email_id,u.phone_number,u.profile_image');
        $this->db->from('project_team t');
        $this->db->join('company_user cu','t.team_member_id=cu.id_company_user','left');
        $this->db->join('user u','cu.user_id=u.id_user','left');
        $this->db->where('t.crm_project_id',$data['crm_project_id']);
        $this->db->order_by('t.id_project_team','DESC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function checkProjectTeamExists($data)
    {
        $this->db->select('*');
        $this->db->from('project_team');
        $this->db->where(array('crm_project_id' => $data['crm_project_id'], 'team_member_id' => $data['team_member_id']));
        $query = $this->db->get();
        return $query->row();
    }

    public function updateProjectTeam($project_team_id,$data)
    {
        $this->db->where('id_project_team',$project_team_id);
        $this->db->update('project_team', $data);
        return 1;
    }

    public function addProjectTeam($data)
    {
        $this->db->insert('project_team', $data);
        return $this->db->insert_id();
    }

    public function deleteProjectTeam($project_team_id)
    {
        $this->db->where('id_project_team', $project_team_id);
        $this->db->update('project_team', array('team_member_status' => '0'));
        return 1;
    }

    public function updateProject($data,$crm_project_id)
    {
        $this->db->where('id_crm_project',$crm_project_id);
        $this->db->update('crm_project', $data);
        //echo $this->db->last_query();
        return 1;
    }

    public function getIntelligence($data)
    {
        if($data['type']=='crm_company')
        {
            $this->db->select('c.*,CONCAT(u.first_name,\' \',u.last_name) as username,profile_image,ar.approval_name,id_user');
            $this->db->from('crm_company c');
            $this->db->join('user u','c.created_by=u.id_user','left');
            $this->db->join('company_user cu','u.id_user=cu.user_id','left');
            $this->db->join('company_approval_role cpr','cu.company_approval_role_id=cpr.id_company_approval_role','left');
            $this->db->join('approval_role ar','cpr.approval_role_id=ar.id_approval_role','left');
            $this->db->where(array('c.company_id' => $data['company_id'], 'c.sector_id' => $data['sector_id'], 'c.sub_sector_id' => $data['sub_sector_id']));
            if(isset($data['crm_company_id']))
                $this->db->where('id_crm_company !='.$data['crm_company_id'], NULL, FALSE);
            if(isset($data['search_key']) && $data['search_key']!=''){
                $this->db->where('(c.company_name like "%'.$data['search_key'].'%")');
            }
        }
        else if($data['type']=='crm_project')
        {
            $this->db->select('p.*,CONCAT(u.first_name,\' \',u.last_name) as username,profile_image,ar.approval_name,id_user');
            $this->db->from('crm_project p');
            $this->db->join('user u','p.created_by=u.id_user','left');
            $this->db->join('company_user cu','u.id_user=cu.user_id','left');
            $this->db->join('company_approval_role cpr','cu.company_approval_role_id=cpr.id_company_approval_role','left');
            $this->db->join('approval_role ar','cpr.approval_role_id=ar.id_approval_role','left');
            $this->db->where(array('p.company_id' => $data['company_id'], 'p.project_main_sector_id' => $data['sector_id'], 'p.project_sub_sector_id' => $data['sub_sector_id']));
            if(isset($data['crm_project_id']))
                $this->db->where('id_crm_project !='.$data['crm_project_id'], NULL, FALSE);
            if(isset($data['search_key']) && $data['search_key']!=''){
                $this->db->where('(p.project_title like "%'.$data['search_key'].'%")');
            }
        }
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProductTermItemData($data)
    {
        $this->db->select('*');
        $this->db->from('product_term_item_data');
        if(isset($data['crm_project_id']))
            $this->db->where('crm_project_id',$data['crm_project_id']);
        if(isset($data['product_term_item_id']))
            $this->db->where('product_term_item_id',$data['product_term_item_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addProductTermItemData($data)
    {
        $this->db->insert('product_term_item_data', $data);
        return $this->db->insert_id();
    }

    public function updateProductTermItemData($data)
    {
        $this->db->where(array('crm_project_id' => $data['crm_project_id'], 'product_term_item_id' => $data['product_term_item_id']));
        $this->db->update('product_term_item_data', $data);
        return 1;
    }

    public function getFormFieldValueByFieldName($data)
    {
        $this->db->select('c.*');
        $this->db->from('crm_project_data c');
        $this->db->join('form_field ff','c.form_field_id=ff.id_form_field');
        $this->db->join('form f','ff.form_id=f.id_form');
        if(isset($data['form_name']))
            $this->db->where('f.form_name',$data['form_name']);
        if(isset($data['crm_project_id']))
            $this->db->where('crm_project_id',$data['crm_project_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getRiskCategoryItemsWithProjectItems($data)
    {
        /*$this->db->select('r.*,IFNULL(pi.project_risk_category_item_grade,r.risk_category_item_grade) as project_risk_category_item_grade');
        $this->db->from('risk_category_item r');
        $this->db->join('project_risk_category_item pi','r.id_risk_category_item=pi.risk_category_item_id and pi.crm_project_id='.$data['crm_project_id'].'','left');
        if(isset($data['risk_category_id']))
            $this->db->where('r.risk_category_id',$data['risk_category_id']);
        if(isset($data['risk_category_item_id']))
            $this->db->where('r.id_risk_category_item',$data['risk_category_item_id']);
        if(isset($data['sector_id']) && isset($data['sub_sector_id']))
            $this->db->where('(r.sector_id ='.$data['sub_sector_id'].' or r.sector_id ='.$data['sector_id'].')');*/

        if($data['sub_sector_id']!='' && $data['sub_sector_id']!=0){
            $sql = 'SELECT SQL_CALC_FOUND_ROWS `r`.*, IFNULL(pi.project_risk_category_item_grade, r.risk_category_item_grade) as project_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected
                    FROM `risk_category_item` `r`
                    LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id` and `pi`.`crm_project_id`='.$data['crm_project_id'].'
                    WHERE `r`.`risk_category_id` = '.$data['risk_category_id'].'
                    AND `r`.`sector_id` = '.$data['sub_sector_id'].'
                    UNION All
                    SELECT `r`.*, IFNULL(pi.project_risk_category_item_grade, r.risk_category_item_grade) as project_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected
                    FROM `risk_category_item` `r`
                    LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id` and `pi`.`crm_project_id`='.$data['crm_project_id'].'
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].' AND FOUND_ROWS() = 0
                    AND `r`.`sector_id` = '.$data['sector_id'].'
                    UNION All
                    SELECT `r`.*, IFNULL(pi.project_risk_category_item_grade, r.risk_category_item_grade) as project_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected
                    FROM `risk_category_item` `r`
                    LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id` and `pi`.`crm_project_id`='.$data['crm_project_id'].'
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].' AND FOUND_ROWS() = 0 AND `r`.`sector_id` = 0';
        }
        else
        {
            $sql = 'SELECT  SQL_CALC_FOUND_ROWS `r`.*, IFNULL(pi.project_risk_category_item_grade, r.risk_category_item_grade) as project_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected
                FROM `risk_category_item` `r`
                LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id` and `pi`.`crm_project_id`='.$data['crm_project_id'].'
                WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].'
                AND `r`.`sector_id` = '.$data['sector_id'].'
                UNION All
                SELECT `r`.*, IFNULL(pi.project_risk_category_item_grade, r.risk_category_item_grade) as project_risk_category_item_grade, IFNULL(pi.is_active, 0) as selected
                FROM `risk_category_item` `r`
                LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id` and `pi`.`crm_project_id`='.$data['crm_project_id'].'
                WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].' AND FOUND_ROWS() = 0 AND `r`.`sector_id` = 0';
        }


        $query = $this->db->query($sql);


        //$query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addKnowledgeDocumentViewCount($data)
    {
        $this->db->set('number_of_views', 'number_of_views+1', FALSE);
        $this->db->where('id_knowledge_document', $data['id_knowledge_document']);
        $this->db->update('knowledge_document');
        return 1;
    }

    public function getProjectRiskCategoryItem($data)
    {
        $this->db->select('*');
        $this->db->from('project_risk_category_item');
        if(isset($data['risk_category_item_id']))
            $this->db->where('risk_category_item_id',$data['risk_category_item_id']);
        if(isset($data['crm_project_id']))
            $this->db->where('crm_project_id',$data['crm_project_id']);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function addProjectRiskCategory($data)
    {
        $this->db->insert_batch('project_risk_category_item', $data);
        return $this->db->insert_id();
    }

    public function updateProjectRiskCategory($data)
    {
        if(isset($data['risk_category_id'])) {
            $this->db->where('risk_category_id',$data['risk_category_id']);
            $this->db->update('project_risk_category_item',$data);
        }
        else
            $this->db->update_batch('project_risk_category_item', $data, 'id_project_risk_category_item');
        return 1;
    }

    public function checkProjectContainsProjectRiskAssessment($crm_project_id)
    {
        $query = $this->db->get_where('project_risk_category_item',array('crm_project_id' => $crm_project_id));
        return $query->result_array();
    }

    public function getCrmProjectRiskCategoryAttributesItemsGrade($data)
    {
        if($data['sub_sector_id']!='' && $data['sub_sector_id']!=0){
            $sql = 'SELECT project_risk_category_item_grade
                    FROM `risk_category_item` `r`
                    LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id`
                    WHERE `r`.`risk_category_id` = '.$data['risk_category_id'].'  and `pi`.`crm_project_id`='.$data['crm_project_id'].' and pi.is_active=1
                    AND `r`.`sector_id` = '.$data['sub_sector_id'].'
                    UNION All
                    SELECT project_risk_category_item_grade
                    FROM `risk_category_item` `r`
                    LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id`
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].'  and `pi`.`crm_project_id`='.$data['crm_project_id'].' and pi.is_active=1
                    AND `r`.`sector_id` = '.$data['sector_id'].'
                    UNION All
                    SELECT project_risk_category_item_grade
                    FROM `risk_category_item` `r`
                    LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id`
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].'  and `pi`.`crm_project_id`='.$data['crm_project_id'].' and pi.is_active=1
                    AND `r`.`sector_id` = 0';
        }
        else
        {
            $sql = 'SELECT project_risk_category_item_grade
                    FROM `risk_category_item` `r`
                    LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id`
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].'  and `pi`.`crm_project_id`='.$data['crm_project_id'].' and pi.is_active=1
                    AND `r`.`sector_id` = '.$data['sector_id'].'
                    UNION All
                    SELECT project_risk_category_item_grade
                    FROM `risk_category_item` `r`
                    LEFT JOIN `project_risk_category_item` `pi` ON `r`.`id_risk_category_item`=`pi`.`risk_category_item_id`
                    WHERE `r`.`risk_category_id` ='.$data['risk_category_id'].'  and `pi`.`crm_project_id`='.$data['crm_project_id'].' and pi.is_active=1
                    AND `r`.`sector_id` = 0';
        }

        //echo $sql; exit;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function statusFlow($data)
    {
        $this->db->select('pa.forwarded_to as assigned_to,pa.id_project_approval,cb.legal_name,pa.approval_status,DATE_FORMAT(pa.created_date_time, "%d %b %Y") as date,c.id_company_approval_credit_committee as credit_committee_id,c.credit_committee_name,CONCAT(u.first_name,\' \',u.last_name) as username');
        $this->db->from('project_approval pa');
        $this->db->join('company_approval_credit_committee c','pa.company_approval_credit_committee_id=c.id_company_approval_credit_committee','left');
        $this->db->join('user u','pa.forwarded_to=u.id_user','left');
        $this->db->join('company_user cu','pa.forwarded_to=cu.user_id','left');
        $this->db->join('company_branch cb','cu.branch_id=cb.id_branch','left');
        if(isset($data['crm_project_id']))
            $this->db->where('pa.project_id',$data['crm_project_id']);
        $this->db->order_by('pa.id_project_approval', 'DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function projectApprovalMeeting($data)
    {
        $this->db->select('m.id_meeting,m.meeting_name,m.created_by as created_by_id,IFNULL(m.from_time,0) as from_time,IFNULL(m.to_time,0) as to_time,CONCAT(m.`when`) as meeting_date, concat(m.from_time,\'/\',m.to_time) as meeting_time,GROUP_CONCAT(CONCAT(u1.first_name,\' \',u1.last_name)) as members,CONCAT(u.first_name," ",u.last_name) as created_by,m.agenda');
        $this->db->from('project_approval_meeting pam');
        $this->db->join('meeting m','pam.meeting_id=m.id_meeting','left');
        //$this->db->join('meeting_guest mg','mg.meeting_id=m.id_meeting','left');
        $this->db->join('meeting_invitation mi','m.id_meeting=mi.meeting_id','left');
        $this->db->join('user u','m.created_by=u.id_user','left');
        $this->db->join('user u1','mi.invitation_id=u1.id_user','left');

        if(isset($data['project_approval_id']))
            $this->db->where('pam.project_approval_id',$data['project_approval_id']);
        $this->db->group_by('m.id_meeting');
        $this->db->order_by('m.id_meeting','desc');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getMeetingGuest($data)
    {
        $this->db->select('group_concat(email) as guest');
        $this->db->from('meeting_guest');
        if(isset($data['meeting_id']))
            $this->db->where('meeting_id',$data['meeting_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function adddKnowledgeDocumentView($data)
    {
        $this->db->insert('knowledge_document_view', $data);
        return $this->db->insert_id();
    }

    public function getFacilities($data)
    {
        $this->db->select('pf.id_project_facility,pf.facility_name,pf.currency_value,pf.crm_project_id,pf.currency_id,pf.loan_amount,DATE(pf.expected_start_date) as expected_start_date,pf.expected_maturity,pf.expected_maturity_type,pf.expected_interest_rate,pf.project_loan_type_id,pf.project_payment_type_id,pf.project_facility_status,DATE(pf.created_date_time) as created_date_time,ppt.project_payment_type,plt.project_loan_type,c.currency_code,c.currency_symbol');
        $this->db->from('project_facility pf');
        $this->db->join('project_payment_type ppt','pf.project_payment_type_id=ppt.id_project_payment_type','left');
        $this->db->join('project_loan_type plt','pf.project_loan_type_id=plt.id_project_loan_type','left');
        $this->db->join('currency c','pf.currency_id=c.id_currency','left');

        if(isset($data['crm_project_id']))
            $this->db->where('pf.crm_project_id',$data['crm_project_id']);

        if(isset($data['id_project_facility']))
            $this->db->where('pf.id_project_facility',$data['id_project_facility']);


        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getFacilityActivity($data)
    {
        if(isset($data['project_approval_id'])) {
            $this->db->select('max(id_facility_activity)');
            $this->db->from('facility_activity');
            $this->db->where('project_approval_id', $data['project_approval_id']);
            $this->db->group_by('project_facility_id');
            $this->db->order_by('id_facility_activity', 'DESC');
            $subQuery = $this->db->get_compiled_select();
        }


        $this->db->select('fa.id_facility_activity,pf.id_project_facility,fa.project_facility_id,pf.facility_name,pf.expected_start_date,c.currency_code,CONCAT(pf.expected_maturity,\' \',pf.expected_maturity_type),fa.facility_amount,fa.facility_amount as loan_amount,pf.expected_maturity,pf.expected_maturity_type,fa.facility_status,fa.facility_status as project_facility_status,DATE(fa.created_date_time) as created_date_time,pf.currency_id,pf.expected_interest_rate,pf.project_loan_type_id,pf.project_payment_type_id,ppt.project_payment_type,fa.facility_comments');
        $this->db->from('facility_activity fa');
        $this->db->join('project_facility pf','fa.project_facility_id=pf.id_project_facility','left');
        $this->db->join('currency c','pf.currency_id=c.id_currency','left');
        $this->db->join('project_payment_type ppt','pf.project_payment_type_id=ppt.id_project_payment_type','left');
        if(isset($data['project_approval_id']))
            $this->db->where('id_facility_activity IN ('.$subQuery.')', NULL, FALSE);

        $this->db->order_by('fa.id_facility_activity','DESC');
        $this->db->group_by('fa.project_facility_id');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getFacilityActivityInitial($data)
    {
        $this->db->select('fa.id_facility_activity,pf.facility_name,pf.expected_start_date,c.currency_code,CONCAT(pf.expected_maturity,\' \',pf.expected_maturity_type),fa.facility_amount as loan_amount,pf.expected_maturity,pf.expected_maturity_type,fa.facility_status as project_facility_status,DATE(fa.created_date_time) as created_date_time,pf.currency_id,pf.expected_interest_rate,pf.project_loan_type_id,pf.project_payment_type_id,ppt.project_payment_type,fa.facility_comments');
        $this->db->from('facility_activity fa');
        $this->db->join('project_facility pf','fa.project_facility_id=pf.id_project_facility','left');
        $this->db->join('currency c','pf.currency_id=c.id_currency','left');
        $this->db->join('project_payment_type ppt','pf.project_payment_type_id=ppt.id_project_payment_type','left');
        if(isset($data['crm_project_id']))
            $this->db->where('pf.crm_project_id',$data['crm_project_id']);

        $this->db->where('fa.project_approval_id IS NULL');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getFacilitiesCount($data)
    {
        $this->db->select('count(*) as total');
        $this->db->from('project_facility');

        if(isset($data['crm_project_id']))
            $this->db->where('crm_project_id',$data['crm_project_id']);


        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addFacilities($data)
    {
        $this->db->insert('project_facility', $data);
        return $this->db->insert_id();
    }

    public function updateFacilities($data)
    {
        $this->db->where('id_project_facility',$data['id_project_facility']);
        $this->db->update('project_facility',$data);
        return 1;
    }

    public function addCollateral($data)
    {
        $this->db->insert('collateral', $data);
        return $this->db->insert_id();
    }

    public function updateCollateral($data)
    {
        if(isset($data['id_collateral']))
            $this->db->where('id_collateral',$data['id_collateral']);
        $this->db->update('collateral',$data);
        return 1;
    }

    public function getCollateralDetails($collateral_id)
    {
        $this->db->select('*');
        $this->db->from('collateral c');
        $this->db->join('collateral_type ct','ct.id_collateral_type=c.collateral_type_id');
        $this->db->where('c.id_collateral',$collateral_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralTypeDetails($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_type');
        if(isset($data['collateral_type_id']))
            $this->db->where('id_collateral_type',$data['collateral_type_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralList($data)
    {
        $this->db->select('c.id_collateral,c.collateral_code,pf.facility_name,pf.loan_amount,cp.project_title,u.first_name as username');
        $this->db->from('collateral c');
        $this->db->join('project_facility pf','pf.id_project_facility=c.project_facility_id');
        $this->db->join('crm_project cp','cp.id_crm_project=pf.crm_project_id');
        $this->db->join('user u','u.id_user=c.created_by');
        if(isset($data['company_id']))
            $this->db->where('cp.company_id', $data['company_id']);
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        $this->db->order_by('c.id_collateral','DESC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralTypeFieldData($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_type_field ctf');
        $this->db->join('collateral_type_field_data ctd','ctf.id_collateral_type_field=ctd.collateral_type_field_id','left');
        if(isset($data['id_collateral']))
            $this->db->where('ctd.collateral_id', $data['id_collateral']);
        if(isset($data['collateral_type_id']))
            $this->db->where('ctf.collateral_type_id', $data['collateral_type_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralTypeFields($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_type_field ctf');
        if(isset($data['collateral_type_id']))
            $this->db->where('collateral_type_id', $data['collateral_type_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralStageFormFields($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_stage_field');
        if(isset($data['collateral_stage_id']))
            $this->db->where('collateral_stage_id', $data['collateral_stage_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralTypeFormData($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_type_field_data');
        if(isset($data['collateral_id']))
            $this->db->where('collateral_id', $data['collateral_id']);
        if(isset($data['collateral_type_id']))
            $this->db->where('collateral_type_id', $data['collateral_type_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralStageFormData($data)
    {
        $this->db->select('*');
        $this->db->from('collateral_stage_field_data');
        if(isset($data['collateral_id']))
            $this->db->where('collateral_id', $data['collateral_id']);
        if(isset($data['collateral_stage_id']))
            $this->db->where('collateral_stage_id', $data['collateral_stage_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addCollateralTypeFormData($data)
    {
        $this->db->insert_batch('collateral_type_field_data', $data);
        return $this->db->insert_id();
    }

    public function addCollateralStageFormData($data)
    {
        $this->db->insert_batch('collateral_stage_field_data', $data);
        return $this->db->insert_id();
    }

    public function updateCollateralTypeFormData($data)
    {
        $this->db->update_batch('collateral_type_field_data', $data, 'id_collateral_type_field_data');
        return true;
    }

    public function updateCollateralStageFormData($data)
    {
        $this->db->update_batch('collateral_stage_field_data', $data, 'id_collateral_stage_field_data');
        return true;
    }

    public function getCollateralTypeStages($data)
    {
        $this->db->select('cs.*');
        $this->db->from('collateral_type_stage ctp');
        $this->db->join('collateral_stage cs','cs.id_collateral_stage=ctp.collateral_stage_id');
        if(isset($data['collateral_type_id']))
            $this->db->where('ctp.collateral_type_id', $data['collateral_type_id']);
        if(isset($data['company_id']))
            $this->db->where('ctp.company_id', $data['company_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCollateralStageFields($data)
    {
        $collateralId = '';
        $this->db->select('ctf.*,csd.collateral_stage_field_value');
        $this->db->from('collateral_stage_field ctf');
        if(isset($data['collateral_id']))
            $collateralId = ' and csd.collateral_id ='.$data['collateral_id'];
        $this->db->join('collateral_stage_field_data csd','csd.collateral_stage_field_id=ctf.id_collateral_stage_field'.$collateralId,'left');
        if(isset($data['collateral_stage_id']))
            $this->db->where('ctf.collateral_stage_id', $data['collateral_stage_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCollateralStageFieldsDataDates($data)
    {
        $sql = 'SELECT DISTINCT(DATE_FORMAT(csd.created_date_time,"%d-%m-%Y")) as `date`
                FROM `collateral_stage_field` `ctf`
                LEFT JOIN `collateral_stage_field_data` `csd` ON `csd`.`collateral_stage_field_id`=`ctf`.`id_collateral_stage_field` and `csd`.`collateral_id` ='.$data['collateral_id'].'
                WHERE `ctf`.`collateral_stage_id` = '.$data['collateral_stage_id'].' GROUP BY csd.created_date_time order by csd.created_date_time desc';
        //echo $sql; exit;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getCollateralStageFieldsByDate($data)
    {
        $sql = 'select * from (SELECT `ctf`.*, csd.collateral_stage_field_id,csd.collateral_stage_field_value,DATE_FORMAT(csd.created_date_time,"%d-%m-%Y") as val_date
                FROM `collateral_stage_field` `ctf`
                LEFT JOIN `collateral_stage_field_data` `csd` ON `csd`.`collateral_stage_field_id`=`ctf`.`id_collateral_stage_field` and `csd`.`collateral_id` ='.$data['collateral_id'].' and  DATE_FORMAT(csd.created_date_time,"%Y-%m-%d")="'.date('Y-m-d',strtotime($data["date"])).'"
                WHERE `ctf`.`collateral_stage_id` = '.$data['collateral_stage_id'].'  ORDER BY csd.id_collateral_stage_field_data DESC)tmp GROUP BY collateral_stage_field_id';
        //echo $sql; exit;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function deleteCollateralStageFormData($data)
    {
        $this->db->where(array('DATE_FORMAT(created_date_time,"%Y-%m-%d")' => date('Y-m-d',strtotime($data["date"])),'collateral_id' => $data['collateral_id'],'collateral_stage_id' => $data['collateral_stage_id']));
        $this->db->delete('collateral_stage_field_data');
    }

    public function getCollateralTypeCategories($data)
    {
        $this->db->select('*');
        $this->db->from('assessment_question_category');
        if(isset($data['collateral_type_id']))
            $this->db->where('collateral_type_id', $data['collateral_type_id']);
        if(isset($data['company_id']))
            $this->db->where('company_id', $data['company_id']);
        $query = $this->db->get();
      /*  echo $this->db->last_query(); exit;*/
        return $query->result_array();

    }

    public function addFacilityActivity($data)
    {
        $this->db->insert('facility_activity', $data);
        return $this->db->insert_id();
    }

    public function getFacilityLoanAmountByProjectId($crm_project_id)
    {
        $this->db->select('c.currency_code,sum(loan_amount) as amount');
        $this->db->from('project_facility pf');
        $this->db->join('currency c','pf.currency_id=c.id_currency','left');
        $this->db->where('crm_project_id',$crm_project_id);
        $this->db->group_by('pf.currency_id');

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getKnowledgeDocuments($data)
    {

        $this->db->select('k.*,concat(u.first_name," ",u.last_name) as user_name,profile_image,group_concat(t.tag_name SEPARATOR "$$") as tags');
        $this->db->from('knowledge_document k');
        $this->db->join('user u','k.uploaded_by=u.id_user','left');
        $this->db->join('knowledge_document_tag t','k.id_knowledge_document=t.knowledge_document_id','left');
        if(isset($data['company_id']))
            $this->db->where('k.company_id',$data['company_id']);
        if(isset($data['search_key']) && $data['search_key']!='' && !empty($data['search_key']))
            $this->db->where('t.tag_name in '.$data['search_key']);
        if(isset($data['knowledge_document_id']))
            $this->db->where('k.id_knowledge_document',$data['knowledge_document_id']);
        $this->db->group_by('k.id_knowledge_document');
        $this->db->order_by('k.id_knowledge_document','DESC');
        $this->db->where('knowledge_document_status',1);
        if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit'],$data['offset']);
        else if(isset($data['limit']) && $data['limit']!='')
            $this->db->limit($data['limit']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();

        /*if(!isset($data['created_by']))
        {
            $this->db->select('k.*,concat(u.first_name," ",u.last_name) as user_name,profile_image,group_concat(t.tag_name SEPARATOR "$$") as tags');
            $this->db->from('knowledge_document k');
            $this->db->join('user u','k.uploaded_by=u.id_user','left');
            $this->db->join('knowledge_document_tag t','k.id_knowledge_document=t.knowledge_document_id','left');
            if(isset($data['company_id']))
                $this->db->where('k.company_id',$data['company_id']);
            if(isset($data['search_key']) && $data['search_key']!='' && !empty($data['search_key']))
                $this->db->where('t.tag_name in '.$data['search_key']);
            if(isset($data['knowledge_document_id']))
                $this->db->where('k.id_knowledge_document',$data['knowledge_document_id']);
            $this->db->group_by('k.id_knowledge_document');
            $this->db->order_by('k.id_knowledge_document','DESC');
            $this->db->where('knowledge_document_status',1);
            if(isset($data['offset']) && $data['offset']!='' && isset($data['limit']) && $data['limit']!='')
                $this->db->limit($data['limit'],$data['offset']);
            else if(isset($data['limit']) && $data['limit']!='')
                $this->db->limit($data['limit']);
            $query = $this->db->get();
            //echo $this->db->last_query(); exit;
            return $query->result_array();
        }
        else
        {
            // #1 SubQueries no.1 -------------------------------------------
            $this->db->select('k.*,concat(u.first_name," ",u.last_name) as user_name,profile_image,group_concat(t.tag_name SEPARATOR "$$") as tags');
            $this->db->from('knowledge_document k');
            $this->db->join('user u','k.uploaded_by=u.id_user','left');
            $this->db->join('knowledge_document_tag t','k.id_knowledge_document=t.knowledge_document_id','left');
            if(isset($data['company_id']))
                $this->db->where('k.company_id',$data['company_id']);
            if(isset($data['search_key']) && $data['search_key']!='' && !empty($data['search_key']))
                $this->db->where('t.tag_name in '.$data['search_key']);
            if(isset($data['knowledge_document_id']))
                $this->db->where('k.id_knowledge_document',$data['knowledge_document_id']);
            $this->db->group_by('k.id_knowledge_document');
            $this->db->order_by('k.id_knowledge_document','DESC');
            $this->db->where('knowledge_document_status',1);
            $query = $this->db->get();
            $subQuery1 = $this->db->_compile_select();
            $this->db->_reset_select();

            // #2 SubQueries no.2 -------------------------------------------
            $this->db->select('k.*,concat(u.first_name," ",u.last_name) as user_name,profile_image,group_concat(t.tag_name SEPARATOR "$$") as tags');
            $this->db->from('knowledge_document k');
            $this->db->join('user u','k.uploaded_by=u.id_user','left');
            $this->db->join('knowledge_document_tag t','k.id_knowledge_document=t.knowledge_document_id','left');
            if(isset($data['company_id']))
                $this->db->where('k.company_id',$data['company_id']);
            if(isset($data['search_key']) && $data['search_key']!='' && !empty($data['search_key']))
                $this->db->where('t.tag_name in '.$data['search_key']);
            if(isset($data['knowledge_document_id']))
                $this->db->where('k.id_knowledge_document',$data['knowledge_document_id']);
            $this->db->group_by('k.id_knowledge_document');
            $this->db->order_by('k.id_knowledge_document','DESC');
            $this->db->where(array('knowledge_document_status' => 1,'uploaded_by' => $data['created_by']));
            $query = $this->db->get();
            $subQuery2 = $this->db->_compile_select();
            $this->db->_reset_select();

            // #3 Union with Simple Manual Queries --------------------------
            $this->db->query("select * from ($subQuery1 UNION $subQuery2) as unionTable");

            // #3 (alternative) Union with another Active Record ------------
            $this->db->from("($subQuery1 UNION $subQuery2)");
            $this->db->get();
            echo $this->db->last_query(); exit;
            return $query->result_array();
        }*/


    }

    public function getKnowledgeDocumentComments($data)
    {
        $this->db->select('k.comment,k.comment_status,CONCAT(u.first_name,\' \',u.last_name) as user_name,u.profile_image,DATE_FORMAT(k.created_date_time,"%d %b %Y") as date,ar.approval_name');
        $this->db->from('knowledge_document_comment k');
        $this->db->join('user u','k.commented_by=u.id_user','left');
        $this->db->join('company_user cu','k.commented_by=cu.user_id','left');
        $this->db->join('company_approval_role car', 'car.id_company_approval_role = cu.company_approval_role_id', 'left');
        $this->db->join('approval_role ar', 'ar.id_approval_role = car.approval_role_id', 'left');
        if(isset($data['knowledge_document_id']))
            $this->db->where('knowledge_document_id',$data['knowledge_document_id']);

        $this->db->order_by('k.id_knowledge_document_comment','DESC');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addKnowledgeDocumentComments($data)
    {
        $this->db->insert('knowledge_document_comment', $data);
        return $this->db->insert_id();
    }

    public function updateKnowledgeDocumentComments($data)
    {
        $this->db->where('id_knowledge_document_comment',$data['id_knowledge_document_comment']);
        $this->db->update('knowledge_document_comment',$data);
        return 1;
    }

    public function addProjectFacilityHistory($data)
    {
        $this->db->insert('project_facility_history',$data);
        return $this->db->insert_id();
    }

    public function getCrmModuleStatus($data)
    {
        $this->db->select('*');
        $this->db->from('crm_module_status');
        if(isset($data['crm_module_id']))
            $this->db->where('crm_module_id',$data['crm_module_id']);
        if(isset($data['crm_module_type']))
            $this->db->where('crm_module_type',$data['crm_module_type']);
        if(isset($data['reference_id']))
            $this->db->where('reference_id',$data['reference_id']);
        if(isset($data['reference_type']))
        $this->db->where('reference_type',$data['reference_type']);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addCrmModuleStatus($data)
    {
        $this->db->insert('crm_module_status',$data);
        return $this->db->insert_id();
    }

    public function getCurrency($data)
    {
        $this->db->select('*');
        $this->db->from('company_currency cc');
        $this->db->join('currency c','cc.currency_id=c.id_currency','left');
        //$this->db->join('company cm','cm.currency_id=c.id_currency','left');

        if(isset($data['company_id']))
            $this->db->where('cc.company_id',$data['company_id']);

        if(isset($data['currency_id']))
            $this->db->where('cc.currency_id',$data['currency_id']);

        $this->db->where('cc.company_currency_status',1);
        //$this->db->group_by('c.id_currency');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getCompanyCurrencyValue($data)
    {
        $this->db->select('*');
        $this->db->from('company_currency_value ccv');
        $this->db->join('company_currency cc','ccv.company_currency_id=cc.id_company_currency','left');
        $this->db->join('currency c','cc.currency_id=c.id_currency ','left');
        //$this->db->join('company cm','cm.currency_id=c.id_currency','left');

        if(isset($data['company_id']))
            $this->db->where('cc.company_id',$data['company_id']);

        if(isset($data['currency_id']))
            $this->db->where('cc.currency_id',$data['currency_id']);

        $this->db->where('cc.company_currency_status',1);
        $this->db->order_by('ccv.id_company_currency_value', 'DESC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProjectCovenant($data)
    {
        $this->db->select('cc.*,c.*,IFNULL(pc.covenant_id,0) as is_checked');
        $this->db->from('covenant_category cc');
        $this->db->join('covenant_type ct','cc.covenant_type_id=ct.id_covenant_type','left');
        $this->db->join('covenant c','cc.id_covenant_category=c.covenant_category_id','left');
        $this->db->join('project_covenant pc','c.id_covenant=pc.covenant_id and pc.project_id='.$data['crm_project_id'],'left');
        if(isset($data['company_id']))
            $this->db->where('cc.company_id',$data['company_id']);

        if(isset($data['covenant_type_key']))
            $this->db->where('ct.covenant_type_key', $data['covenant_type_key']);
        if(isset($data['covenant_category_id']))
            $this->db->where('c.covenant_category_id', $data['covenant_category_id']);
        if(isset($data['sector_id']))
            $this->db->where('((FIND_IN_SET('.$data['sector_id'].',sector)>0) or (FIND_IN_SET(0,sector)>0))');
        else $this->db->where('(FIND_IN_SET(0,sector)>0)');

        $this->db->where('c.covenant_status',1);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getSelectedProjectCovenants($data)
    {
        $this->db->select('c.*');
        $this->db->from('covenant c');
        $this->db->join('project_covenant pc','c.id_covenant=pc.covenant_id','left');
        if(isset($data['crm_project_id']))
            $this->db->where('pc.project_id',$data['crm_project_id']);
        if(isset($data['covenant_category_id']))
            $this->db->where('c.covenant_category_id',$data['covenant_category_id']);

        $this->db->where('c.covenant_status',1);

        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function getProjectCovenantByProjectId($data)
    {
        $this->db->select('p.*');
        $this->db->from('project_covenant p');
        $this->db->join('covenant c','p.covenant_id=c.id_covenant','left');
        $this->db->join('covenant_category cc','c.covenant_category_id=cc.id_covenant_category','left');
        $this->db->join('covenant_type ct','cc.covenant_type_id=ct.id_covenant_type','left');
        if(isset($data['crm_project_id']))
            $this->db->where('p.project_id',$data['crm_project_id']);
        if(isset($data['covenant_category_id']))
            $this->db->where('c.covenant_category_id',$data['covenant_category_id']);
        if(isset($data['covenant_type_key']))
            $this->db->where('ct.covenant_type_key',$data['covenant_type_key']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }

    public function addProjectCovenant($data)
    {
        $this->db->insert_batch('project_covenant', $data);
        return $this->db->insert_id();
    }

    public function deleteProjectCovenant($data)
    {
        $this->db->where($data);
        $this->db->delete('project_covenant');
    }

}