/**
 * Created by RAKESH on 13-04-2016.
 */
/**
 * A service that returns some data.
 */
'use strict';
angular.module('touch.points',[])
    .controller('touchPointViewCtrl',function($rootScope,$scope,crmService){
        $scope.touchPointsList = [];
        $scope.touchPointTypeList = [];

        /*add TouchPoint*/
        $scope.touchPointDescription = '';
        crmService.touchPoint.touchPointType().then(function(result){
            $scope.touchPointTypeList = result.data;
        });
        $scope.submit = function(postField,form){
            $scope.submitStatus = true;
            //console.log(form.$valid);
            if(form.$valid){
                postField.crm_module_type = $scope.crm_module_type;
                postField.crm_module_id = $scope.crm_module_id;
                postField.touch_point_from_id = $scope.crm_module_id;
                postField.created_by = $rootScope.userId;
                //console.log('postField', postField);
                crmService.touchPoint.post(postField).then(function(result){
                    $scope.postField = {'reminder_date':new Date()};
                    $scope.submitStatus = false;
                    $scope.togglePoint = false;
                    if(result.status){
                        $rootScope.toast('Success',result.message);
                        $scope.togglePoint = false;
                        $scope.getTouchPoints(true)
                    } else{
                        $rootScope.toast('Error',result.error,'error',$scope.sector);
                    }

                });
            }
        }

        $scope.getTouchPoints = function(refresh){
            if(refresh){
                $scope.touchPointsList = [];
            }
            crmService.touchPoint.get({
                'company_id':$rootScope.id_company,
                'crm_module_type':$scope.crm_module_type,
                'crm_module_id':$scope.crm_module_id,
                'offset':$scope.touchPointsList.length,
                'limit':10
            }).then(function(result){
                angular.forEach(result.data,function(item){
                    $scope.touchPointsList.push(item);
                })
                if(result.data.length>=10){
                    $scope.loadMoreShow = true;
                } else{
                    $scope.loadMoreShow = false;
                }
            });
        }
        $scope.loadMore = function(){
            $scope.getTouchPoints();
        }
    })

    .directive('touchPointView',function(){
        return {
            restrict:'EA',
            controller:'touchPointViewCtrl',
            link:function(scope,element,attrs){
                scope.crm_module_type = attrs.moduleType;
                scope.crm_module_id = attrs.moduleId;
                scope.getTouchPoints();
            },
            templateUrl:function(elem,attrs){
                return 'partials/component/touch_points/view.html'
            }
        }
    })




