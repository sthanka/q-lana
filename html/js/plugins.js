var chart = window.chart = $('.chart').data('easyPieChart');
$(window).on('load resize', function(){
	//Custom Scroll
	$(".groups-list ul, .height-full, .img-details-list, .scroll-area, .company-list, .acc_scroll-height, .details-area-sm").mCustomScrollbar({
		theme:"minimal-dark",
		scrollButtons:{enable:true},
		autoHideScrollbar:false,
		advanced:{
			updateOnContentResize: true

		},
		callbacks:{
			onOverflowY: function(){
				console.log('Scroll')
			}
		}
	});
	/*$(".details-area").mCustomScrollbar({
		theme:"minimal-dark",
		scrollButtons:{enable:true},
		updateOnContentResize: true,
		callbacks:{
			onScroll:function(){
				/!*if(this.mcs.topPct >=6 ){
					$(this).find('.details-fixed-top').fadeIn(500);
					$(this).find('.go-top').fadeIn(250);
				} else{
					$(this).find('.details-fixed-top').hide();
					$(this).find('.go-top').hide(250);
				}*!/
				if($(this).find('[data-section="button"]').length >= 1){
					if(this.mcs.topPct >= 30){
						$(this).find('[data-section="button"]').addClass('button-section-fixed');
						$(this).find('[data-section="button"]').find('.heading').remove();
						$(this).find('[data-section="button"]').prepend('<h3 class="heading" style="display:inline-block;margin-right:20px;vertical-align:middle">Section Heading</h3>')
						$(this).find('[data-section="button"]').width($(this).width()-10);
						$(this).find('[data-section="button"]').next('div').width($(this).width());
						
						$(this).find('[data-section="button"]').next('div').find('.scroll-area').mCustomScrollbar({setHeight : $(this).height()+100});
					} else{
						$(this).find('[data-section="button"]').removeClass('button-section-fixed');
						$(this).find('[data-section="button"]').find('.heading').remove();
					}
				}
			}
		}
	});
	
	$(".details-area").on("click",".dropdown-menu a[href^='#']",function(e){
		  var href=$(this).attr("href"),target=$(href).parents(".mCustomScrollbar"); 
		  var elTop=$(href).offset().top-$(".details-area .mCSB_container").offset().top;
		  elTop = elTop-55;
		  if(target.length){
			e.preventDefault();
			target.mCustomScrollbar("scrollTo",elTop);
		  }
	});*/
	$(".mCustomScrollbar").on("click","a[href='#topSection']",function(e){
		  var href=$(this).attr("href"),target=$(href).parents(".mCustomScrollbar"); 
		  var elTop=$(href).offset().top-$(this).closest(".mCSB_container").offset().top;
		  elTop = elTop-10;
		  if(target.length){
			e.preventDefault();
			target.mCustomScrollbar("scrollTo",elTop);
		  }
	});	
});
$(function(){

	$(window).resize();
	//MK Custom Tabs
	$.each($('body').find('[data-tab="custom"]'), function(i,o){
		$(o).find('li').removeClass('active');
		$(o).next('[data-area="tab"]').find('[data-content="tab"]').hide();
		$(o).next('[data-area="tab"]').find('[data-content="tab"]:eq(0)').fadeIn('fast');
		$(o).find('a[data-tag="tab"]:eq(0)').parent('li').addClass('active');
		$(o).find('a[data-tag="tab"]').on('click', function(){
			$(o).find('li').removeClass('active');
			$(o).next('[data-area="tab"]').find('[data-content="tab"]').hide();
			$(this).parent('li').addClass('active');
			$(o).next('[data-area="tab"]').find('#'+$(this).attr('data-select')).fadeIn('fast');
		});
	});
	
	//Button Section
	$.each($('body').find('[data-section="button"]'), function(i,o){
		$(o).find('span').on('click', function(){
			//Hide All Accordions
			$('.acc-wrap .accordion_body').hide();
			$('.acc-wrap .accordion_head').removeClass("acc-expand");
			//Show Text
			$(o).find('span').text('');
			$(this).text($(this).attr('data-content'));
			//Scroll to Section
			var dataId = $(this).attr('data-id');
			var _targetPos = $('[data-context="'+dataId+'"]').position().top + 235;
			$(o).closest('.mCustomScrollbar ').mCustomScrollbar("scrollTo",_targetPos);
			//Open Active SPAN Accordion
			$('[data-context="'+dataId+'"]').find('.accordion_head').addClass("acc-expand");
			$('[data-context="'+dataId+'"]').find('.accordion_head').next().show();
		});
	});

	$('body').on('click','.icons-view li div',function(){
		var source_link=$(this).attr('data-links');
		var dest_link=$('.accord-wrapper[data-targets="'+source_link+'"]');
		$(dest_link).find('.acc-body').css('display','block');
		setTimeout(function(){
			$('.scroll-div-wrap').mCustomScrollbar("scrollTo",dest_link);
		},200)


	})
	$('body').on('click','.p-over',function(){
		$('.popover.in').removeClass('in').hide();
		$(this).next().fadeIn(100);
	})
	$('body').on('click','.expanded-menu .left-menu .has-sub > a',function(){
		$(this).parent().toggleClass('parent-sub');
		$(this).toggleClass('menu-hide');
		$(this).next().slideToggle();
	})

	setInterval(function(){
		equalheight('.force-wrap .f-block');
	},500);
});
$(window).load(function() {
	equalheight('.force-wrap .f-block');
});


$(window).resize(function(){
	equalheight('.force-wrap .f-block');
});

equalheight = function(container){

	var currentTallest = 0,
		currentRowStart = 0,
		rowDivs = new Array(),
		$el,
		topPosition = 0;
	$(container).each(function() {

		$el = $(this);
		$($el).height('auto');
		topPostion = $el.position().top;

		if (currentRowStart != topPostion) {
			for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
			}
			rowDivs.length = 0; // empty the array
			currentRowStart = topPostion;
			currentTallest = $el.height();
			rowDivs.push($el);
		} else {
			rowDivs.push($el);
			currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
		}
		for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
			rowDivs[currentDiv].height(currentTallest);
		}
	});
}

