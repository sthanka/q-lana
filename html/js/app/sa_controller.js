angular.module('app')
    .controller('superAdminDashboardCtrl',function($rootScope,$scope,companyService){
        companyService.chartCustomersByCountry().then(function(result){
            $scope.chartCustomersCountry = $scope.processArray(result.data);
            $scope.chartObject = {};
            $scope.chartObject.data = {
                "cols":[
                    {id:"t",label:"Topping",type:"string"},
                    {id:"s",label:"Customers",type:"number"}
                ],"rows":$scope.chartCustomersCountry
            };

            // $routeParams.chartType == BarChart or PieChart or ColumnChart...
            $scope.chartObject.type = 'ColumnChart';
            $scope.chartObject.options = {};
        });

        companyService.chartCustomersByCategory().then(function(result){
            $scope.chartCustomersCategory = $scope.processArrayPie(result.data);
            $scope.chartObjectPie = {};
            $scope.chartObjectPie.data = {
                "cols":[
                    {id:"t",label:"Topping",type:"string"},
                    {id:"s",label:"Customers",type:"number"}
                ],"rows":$scope.chartCustomersCategory
            };

            // $routeParams.chartType == BarChart or PieChart or ColumnChart...
            $scope.chartObjectPie.type = 'PieChart';
            $scope.chartObjectPie.options = {};
        });

        $scope.processArray = function(json){
            var chartData = [];
            for(var i = 0,l = json.length; i<l; i++){
                chartData.push({c:[{v:json[i].country_name},{v:parseInt(json[i].total_customers)}]});
            }
            return chartData;
        }
        $scope.processArrayPie = function(json){
            var chartData = [];
            for(var i = 0,l = json.length; i<l; i++){
                chartData.push({c:[{v:json[i].bank_category_name},{v:parseInt(json[i].total_companies)}]});
            }
            return chartData;
        }
        $scope.companyList = [];
        companyService.get().then(function($result){
            //console.log($result.data);
            if($result.status){
                $scope.companyList = $result.data.data;
            }
        });


    })

    .controller('sectorCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        //console.log('sector');
        //data table load
        $scope.displayed = [];
        $scope.callServer = function callServer(tableState){
            //console.log('tableState',tableState);
            $scope.dtableState = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            var start = pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number||10;  // Number of entries showed per page.

            masterService.sector.getPage(start,number,tableState).then(function(result){
                //console.log(result);
                $scope.displayed = result.data;
                tableState.pagination.numberOfPages = Math.ceil(result.total_records/number);//set the number of pages so the pagination can update
                //console.log(tableState.pagination.numberOfPages);
                $scope.isLoading = false;
            });
        };

        //add new row in modal
        $scope.animationsEnabled = true;

        $scope.open = function($mode,$editId){
            var $item = {};
            if($mode=='edit'){
                $item.type = 'edit';
                $item.editId = $scope.editId;
            }
            var modalInstance = $uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/super-admin/master-data/create-sector.html',
                controller:'CreateSectorCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });

            modalInstance.result.then(function(){
                $scope.selected = {};$scope.selectedCount = 0;
                $scope.callServer($scope.dtableState);
                //$scope.selected = selectedItem;
            },function(){
                // $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.toggleAnimation = function(){
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId = 0;
        $scope.selected = {};
        $scope.updateSelected = function(){
            var count = 0;
            //console.log($scope.selected);
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId = x;
                    count++;
                }
            }
            $scope.selectedCount = count;
        };

    }])

    .controller('CreateSectorCtrl',function($scope,$uibModalInstance,items,masterService,$rootScope){
        //console.log(items);
        $scope.parentSectorsList = [];
        masterService.sector.getAll().then(function(result){
            $scope.parentSectorsList = result.data.data;
        });
        $scope.title = 'Create';
        $scope.btn = 'Add';
        $scope.sector = {};

        if(items.type=='edit'){
            $scope.title = 'Update';
            $scope.btn = 'Update';
            masterService.sector.getById(items.editId).then(function(result){
                $scope.sector = result.data;
            })
        }
        $scope.sector = {"sector_name":"","parent_sector_id":""};
        $scope.submitSectorForm = function($postData){
            if($postData.id_sector==undefined){
                $scope.add($postData);
            } else{
                $scope.update($postData);
            }
        };
        $scope.update = function($postData){
            masterService.sector.edit($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                } else{
                    $rootScope.toast('Error',result.error,'error',$scope.sector);
                }
            });
        };

        $scope.add = function($postData){
            masterService.sector.add($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                } else{
                    $rootScope.toast('Error',result.error,'error',$scope.sector);
                }
            });
        };

        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })
    //countryCtrl
    .controller('countryCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        //console.log('country');
        //data table load
        $scope.countriesList = [];
        $scope.callServer = function callServer(tableState){
            //console.log(0);

            $scope.isLoading = true;
            $scope.dtableState = tableState;
            var pagination = tableState.pagination;

            var start = pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number||2;  // Number of entries showed per page.

            masterService.country.getPage(start,number,tableState).then(function(result){
                //console.log(result);
                $scope.countriesList = result.data.data;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading = false;
            });
        };

        //add new row in modal
        $scope.animationsEnabled = true;

        $scope.open = function($mode,$editId){
            var $item = {};
            if($mode=='edit'){
                $item.type = 'edit';
                $item.editId = $scope.editId;
            }
            //console.log($item);
            var modalInstance = $uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/super-admin/master-data/create-country.html',
                controller:'CreateCountryCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });

            modalInstance.result.then(function(){
                $scope.selected = {};$scope.selectedCount = 0;
                $scope.callServer($scope.dtableState);
                //$scope.selected = selectedItem;
            },function(){
                // $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.toggleAnimation = function(){
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId = 0;
        $scope.selected = {};
        $scope.updateSelected = function(){
            var count = 0;
            //console.log($scope.selected);
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId = x;
                    count++;
                }
            }
            $scope.selectedCount = count;
        };
    }])

    .controller('CreateCountryCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){
        $scope.title = 'Create';
        $scope.btn = 'Add';
        $scope.country = {};
        if(items.type=='edit'){
            $scope.title = 'Update';
            $scope.btn = 'Update';
            masterService.country.getById(items.editId).then(function(result){
                //console.log(result);
                $scope.country = result.data;
            })
        }
        $scope.country = {"country_name":'',"country_code":''};
        $scope.save = function($postData){
            if($postData.id_country==undefined){
                $scope.add($postData);
            } else{
                $scope.update($postData);
            }
        };
        $scope.add = function($postData){
            masterService.country.createCountry($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                } else{
                    $rootScope.toast('Error',result.error,'error',$scope.country);
                }
            });
        };
        $scope.update = function($postData){
            masterService.country.edit($postData).then(function(result){
                //console.log(result);
                if(result.status){
                    //console.log('true');
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                } else{
                    //console.log('false');
                    $rootScope.toast('Error',result.error,'error',$scope.country);
                }
            });
        };


        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('branchTypeCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        //console.log('branchtype');
        //data table load
        $scope.countriesList = [];
        $scope.callServer = function callServer(tableState){
            //console.log(0);
            $scope.dtableState = tableState;
            $scope.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number||10;  // Number of entries showed per page.
            tableState.company_id = 0;
            masterService.branchType.getPage(tableState).then(function(result){
                //console.log(result);
                $scope.countriesList = result.data.data;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading = false;
            });
        };

        //add new row in modal
        $scope.animationsEnabled = true;

        $scope.open = function($mode,$editId){
            var $item = {};
            if($mode=='edit'){
                $item.type = 'edit';
                $item.editId = $scope.editId;
            }
            var modalInstance = $uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/super-admin/master-data/create-branch-type.html',
                controller:'CreateBranchTypeCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });

            modalInstance.result.then(function(){
                $scope.selected = {};$scope.selectedCount = 0;
                $scope.callServer($scope.dtableState);
                //$scope.selected = selectedItem;
            },function(){
                // $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.toggleAnimation = function(){
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId = 0;
        $scope.selected = {};
        $scope.updateSelected = function(){
            var count = 0;
            //console.log($scope.selected);
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId = x;
                    count++;
                }
            }
            $scope.selectedCount = count;
        };
    }])

    .controller('CreateBranchTypeCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){

        $scope.title = 'Create';
        $scope.btn = 'Add';
        $scope.branch = {};
        if(items.type=='edit'){
            $scope.title = 'Update';
            $scope.btn = 'Update';
            masterService.branchType.getById(items.editId).then(function(result){
                //console.log(result);
                $scope.branch = result.data;
            })
        }
        $scope.branch = {"branch_type_name":'',"branch_type_code":''};
        $scope.save = function($postData){
            if($postData.id_branch_type==undefined){
                $scope.add($postData);
            } else{
                $scope.update($postData);
            }
        };
        $scope.add = function($postData){
            masterService.branchType.createBranchType($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                } else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };
        $scope.update = function($postData){
            masterService.branchType.edit($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                } else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };

        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('ApprovalRoleCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        //console.log('Approvalrole');
        //data table load
        $scope.approvalRolesList = [];
        $scope.callServer = function callServer(tableState){
            $scope.dtableState = tableState;
            $scope.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number||2;  // Number of entries showed per page.
            tableState.company_id = 0;
            masterService.approvalRole.getPage(tableState).then(function(result){
                $scope.approvalRolesList = result.data.data;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading = false;
            });
        };

        //add new row in modal
        $scope.animationsEnabled = true;

        $scope.open = function($mode,$editId){
            var $item = {};
            if($mode=='edit'){
                $item.type = 'edit';
                $item.editId = $scope.editId;
            }
            //console.log($item);
            var modalInstance = $uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/super-admin/master-data/create-approval-role.html',
                controller:'CreateApprovalRoleCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });

            modalInstance.result.then(function(){
                $scope.selected = {};$scope.selectedCount = 0;
                $scope.callServer($scope.dtableState);
                //$scope.selected = selectedItem;
            },function(){
                // $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.toggleAnimation = function(){
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId = 0;
        $scope.selected = {};
        $scope.updateSelected = function(){
            var count = 0;
            //console.log($scope.selected);
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId = x;
                    count++;
                }
            }
            $scope.selectedCount = count;
        };
    }])

    .controller('CreateApprovalRoleCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){

        $scope.title = 'Create';
        $scope.btn = 'Add';
        $scope.approvalRole = {};
        if(items.type=='edit'){
            $scope.title = 'Update';
            $scope.btn = 'Update';
            masterService.approvalRole.getById(items.editId).then(function(result){
                $scope.approvalRole = result.data;
            })
        }
        $scope.approvalRole = {"approval_name":''};
        $scope.save = function($postData){
            if($postData.id_approval_role==undefined){
                $scope.add($postData);
            } else{
                $scope.update($postData);
            }
        };
        $scope.add = function($postData){
            masterService.approvalRole.createApprovalRole($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                } else{
                    $rootScope.toast('Error',result.error,'error',$scope.approvalRole);
                }
            });
        };
        $scope.update = function($postData){
            masterService.approvalRole.edit($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                } else{
                    $rootScope.toast('Error',result.error,'error',$scope.approvalRole);
                }
            });
        };

        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('plansCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        $scope.plansList = [];
        $scope.callServer = function callServer(tableState){
            $scope.dtableState = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            var start = pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number||2;  // Number of entries showed per page.

            masterService.plans.getPage(start,number,tableState).then(function(result){
                $scope.plansList = result.data;
                tableState.pagination.numberOfPages = Math.ceil(result.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading = false;
            });

        };

        //add new row in modal
        $scope.animationsEnabled = true;

        $scope.open = function($mode,$editId){

            var $item = {};
            if($mode=='edit'){
                $item.type = 'edit';
                $item.editId = $scope.editId;
            }
            var modalInstance = $uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/super-admin/master-data/create-plans.html',
                controller:'CreatePlansCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });

            modalInstance.result.then(function(){
                $scope.selected = {};$scope.selectedCount = 0;
                $scope.callServer($scope.dtableState);
                //$scope.selected = selectedItem;
            },function(){
                // $log.info('Modal dismissed at: ' + new Date());
            });
        };


        $scope.delete = function($mode,$editId){
            alert($editId);

        };


        $scope.toggleAnimation = function(){
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };

        //Check updated
        $scope.editId = 0;
        $scope.selected = {};
        $scope.updateSelected = function(){
            var count = 0;
            //console.log($scope.selected);
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId = x;
                    count++;
                }
            }
            $scope.selectedCount = count;
        };


    }])

    .controller('CreatePlansCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){

        $scope.plan = {};
        $scope.title = 'Create';
        $scope.btn = 'Add';

        if(items.type=='edit'){
            $scope.title = 'Update';
            $scope.btn = 'Update';
            masterService.plans.getPlan(items.editId).then(function(result){
                $scope.plan = result.data;
            })
        }
        $scope.plan = {"plan_name":'',"no_of_loans":'',"price_per_loan":'',"no_of_users":'',"disk_space":''};
        $scope.ok = function($postData){
            console.log($postData);
            console.log($scope.planForm.$valid);
            if($postData){
                // check to make sure the form is completely valid
                if($scope.planForm.$valid){
                    if($postData['id_plan']){
                        masterService.plans.updatePlan($postData).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                $uibModalInstance.close();
                            } else{
                                $rootScope.toast('Error',result.error,'error',$scope.plan);
                            }
                        });
                    }
                    else{
                        masterService.plans.createPlan($postData).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                $uibModalInstance.close();
                            } else{
                                $rootScope.toast('Error',result.error,'error',$scope.plan);
                            }
                        });
                    }
                }
            }
        };

        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('currencyCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        $scope.plansList = [];
        $scope.callServer = function callServer(tableState){
            $scope.dtableState = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            var start = pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number||2;  // Number of entries showed per page.

            masterService.currency.getTable(start,number,tableState).then(function(result){
                $scope.currencyList = result.data.data;
                tableState.pagination.numberOfPages = Math.ceil(result.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading = false;
            });

        };

        //add new row in modal
        $scope.animationsEnabled = true;

        $scope.open = function($mode,$editId){

            var $item = {};
            if($mode=='edit'){
                $item.type = 'edit';
                $item.editId = $scope.editId;
            }
            var modalInstance = $uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/super-admin/master-data/create-currency.html',
                controller:'createCurrencyCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    },
                    Countries:function(){
                        return masterService.country.get().then(function(result){
                            return result.data;
                        });
                    }

                }
            });

            modalInstance.result.then(function(){
                $scope.selected = {};$scope.selectedCount = 0;
                $scope.callServer($scope.dtableState);
                //$scope.selected = selectedItem;
            },function(){
                // $log.info('Modal dismissed at: ' + new Date());
            });
        };


        $scope.delete = function($mode,$editId){
            alert($editId);

        };


        $scope.toggleAnimation = function(){
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };

        //Check updated
        $scope.editId = 0;
        $scope.selected = {};
        $scope.updateSelected = function(){
            var count = 0;
            //console.log($scope.selected);
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId = x;
                    count++;
                }
            }
            $scope.selectedCount = count;
        };


    }])

    .controller('createCurrencyCtrl',function($scope,$uibModalInstance,items,Countries,masterService,$state,$rootScope){

        $scope.currency = {};
        $scope.currency.country_flag = '';
        $scope.Countries = Countries;
        $scope.title = 'Create';
        $scope.btn = 'Add';


        if(items.type=='edit'){
            $scope.title = 'Update';
            $scope.btn = 'Update';
            masterService.currency.getAll({'id_currency':items.editId}).then(function(result){
                $scope.currency = result.data;
            })
        }

        $scope.ok = function($postData){
            console.log($postData);
            if($postData){
                // check to make sure the form is completely valid

                if($scope.form.$valid){
                    var param = {}
                    param.country_id = $postData.country_id;
                    param.currency_code = $postData.currency_code;
                    param.currency_symbol = $postData.currency_symbol;
                    param.rounding_option = $postData.rounding_option;
                    param.formatting = $postData.formatting;
                    // param.country_flg = $scope.countryFlg;
                    if($postData.id_currency!=undefined){
                        param.id_currency = $postData.id_currency;
                    }

                    masterService.currency.post($scope.currency.country_flag,param).then(function(result){
                        if(result.status){
                            $rootScope.toast('Success',result.message);
                            $uibModalInstance.close();
                        } else{
                            $rootScope.toast('Error',result.error,'error');
                        }
                    });

                }
            }
        };

        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
        $scope.uploadCountryFlag = function(file){
            $scope.currency.country_flag = file;
            $scope.trash = true;
        };
        $scope.uploadCountryFlagRemove = function(){
            $scope.currency.country_flag = '';
            $scope.trash = false;
        };
    })

    .controller('bankCategoryCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        $scope.bankCategoryList = [];
        $scope.callServer = function callServer(tableState){
            $scope.dtableState = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            var start = pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number||10;  // Number of entries showed per page.

            masterService.bankCategory.getPage(start,number,tableState).then(function(result){
                $scope.bankCategoryList = result.data;
                tableState.pagination.numberOfPages = Math.ceil(result.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading = false;
            });

        };

        //add new row in modal
        $scope.animationsEnabled = true;

        $scope.open = function($mode,$editId){

            var $item = {};
            if($mode=='edit'){
                $item.type = 'edit';
                $item.editId = $scope.editId;
            }
            var modalInstance = $uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/super-admin/master-data/create-bank-category.html',
                controller:'CreateBankCategoryCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });
            modalInstance.result.then(function(){
                $scope.selected = {};$scope.selectedCount = 0;
                $scope.callServer($scope.dtableState);
                //$scope.selected = selectedItem;
            },function(){
                // $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.toggleAnimation = function(){
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };
        //Check updated
        $scope.editId = 0;
        $scope.selected = {};
        $scope.updateSelected = function(){
            var count = 0;
            //console.log($scope.selected);
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId = x;
                    count++;
                }
            }
            $scope.selectedCount = count;
        };
    }])

    .controller('CreateBankCategoryCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){
        //console.log(items);
        $scope.bankCategory = {};
        $scope.title = 'Create';
        $scope.btn = 'Add';

        if(items.type=='edit'){
            $scope.title = 'Update';
            $scope.btn = 'Update';
            masterService.bankCategory.getBankCategory(items.editId).then(function(result){
                $scope.bankCategory = result.data;
            })
        }

        $scope.bankCategory = {"bank_category_name":''};
        $scope.ok = function($postData){
            if($postData){
                // check to make sure the form is completely valid
                if($scope.bankCategoryForm.$valid){
                    if($postData['id_bank_category']){
                        masterService.bankCategory.updateBankCategory($postData).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                $uibModalInstance.close();
                            } else{
                                $rootScope.toast('Error',result.error,'error',$scope.bankCategory);
                            }
                        });
                    }
                    else{
                        masterService.bankCategory.createBankCategory($postData).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                $uibModalInstance.close();
                            } else{
                                $rootScope.toast('Error',result.error,'error',$scope.bankCategory);
                            }
                        });
                    }
                }
            }

        };

        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('riskCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        $scope.riskList = [];
        $scope.callServer = function callServer(tableState){
            $scope.dtableState = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            var start = pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number||2;  // Number of entries showed per page.

            masterService.risk.get(start,number,tableState).then(function(result){
                $scope.riskList = result.data.data;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading = false;
            });

        };
        //add new row in modal
        $scope.animationsEnabled = true;
        $scope.open = function($mode,$editId){

            var $item = {};
            if($mode=='edit'){
                $item.type = 'edit';
                $item.editId = $scope.editId;
            }
            var modalInstance = $uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/super-admin/master-data/create-risk.html',
                controller:'CreateRiskCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });

            modalInstance.result.then(function(){
                $scope.selected = {};$scope.selectedCount = 0;
                $scope.callServer($scope.dtableState);
                //$scope.selected = selectedItem;
            },function(){
                // $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.toggleAnimation = function(){
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };

        //Check updated
        $scope.editId = 0;
        $scope.selected = {};
        $scope.updateSelected = function(){
            var count = 0;
            //console.log($scope.selected);
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId = x;
                    count++;
                }
            }
            $scope.selectedCount = count;
        };

    }])

    .controller('CreateRiskCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){

        $scope.title = 'Create';
        $scope.btn = 'Add';
        $scope.risk = {};
        if(items.type=='edit'){
            $scope.title = 'Update';
            $scope.btn = 'Update';
            masterService.risk.getById(items.editId).then(function(result){
                //console.log(result);
                $scope.risk = result.data;
            })
        }
        $scope.risk = {"risk_name":'',"risk_description":''};
        $scope.save = function($postData){
            if($postData.id_risk_type==undefined){
                $scope.add($postData);
            } else{
                $scope.update($postData);
            }
        };
        $scope.add = function($postData){
            masterService.risk.create($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                } else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };
        $scope.update = function($postData){
            masterService.risk.update($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                } else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };

        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('socialCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        $scope.socialList = [];
        $scope.callServer = function callServer(tableState){
            $scope.dtableState = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            var start = pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number||2;  // Number of entries showed per page.

            masterService.social.get(start,number,tableState).then(function(result){
                $scope.socialList = result.data.data;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading = false;
            });

        };
        //add new row in modal
        $scope.animationsEnabled = true;
        $scope.open = function($mode,$editId){

            var $item = {};
            if($mode=='edit'){
                $item.type = 'edit';
                $item.editId = $scope.editId;
            }
            var modalInstance = $uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/super-admin/master-data/create-social.html',
                controller:'CreateSocialCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });

            modalInstance.result.then(function(){
                $scope.selected = {};$scope.selectedCount = 0;
                $scope.callServer($scope.dtableState);
                //$scope.selected = selectedItem;
            },function(){
                // $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.toggleAnimation = function(){
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };

        //Check updated
        $scope.editId = 0;
        $scope.selected = {};
        $scope.updateSelected = function(){
            var count = 0;
            //console.log($scope.selected);
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId = x;
                    count++;
                }
            }
            $scope.selectedCount = count;
        };
    }])

    .controller('CreateSocialCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){

        $scope.title = 'Create';
        $scope.btn = 'Add';
        $scope.social = {};
        if(items.type=='edit'){
            $scope.title = 'Update';
            $scope.btn = 'Update';
            masterService.social.getById(items.editId).then(function(result){
                //console.log(result);
                $scope.social = result.data;
            })
        }
        $scope.social = {"business_social_network":'',"business_social_network_description":''};
        $scope.save = function($postData){
            if($postData.id_business_social_network_type==undefined){
                $scope.add($postData);
            } else{
                $scope.update($postData);
            }
        };
        $scope.add = function($postData){
            masterService.social.create($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                } else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };
        $scope.update = function($postData){
            masterService.social.update($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                } else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };

        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('contactCtrl',['masterService','$scope','$uibModal',function(masterService,$scope,$uibModal){
        $scope.contactList = [];
        $scope.callServer = function callServer(tableState){
            $scope.dtableState = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            var start = pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number||2;  // Number of entries showed per page.

            masterService.contact.get(start,number,tableState).then(function(result){
                $scope.contactList = result.data.data;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading = false;
            });

        };
        //add new row in modal
        $scope.animationsEnabled = true;
        $scope.open = function($mode,$editId){

            var $item = {};
            if($mode=='edit'){
                $item.type = 'edit';
                $item.editId = $scope.editId;
            }
            var modalInstance = $uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/super-admin/master-data/create-contact.html',
                controller:'createContactCtrl',
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });

            modalInstance.result.then(function(){
                $scope.selected = {};$scope.selectedCount = 0;
                $scope.callServer($scope.dtableState);
                //$scope.selected = selectedItem;
            },function(){
                // $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.toggleAnimation = function(){
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };

        //Check updated
        $scope.editId = 0;
        $scope.selected = {};
        $scope.updateSelected = function(){
            var count = 0;
            //console.log($scope.selected);
            for(x in $scope.selected){
                if($scope.selected[x]){
                    $scope.editId = x;
                    count++;
                }
            }
            $scope.selectedCount = count;
        };
    }])

    .controller('createContactCtrl',function($scope,$uibModalInstance,items,masterService,$state,$rootScope){

        $scope.title = 'Create';
        $scope.btn = 'Add';
        $scope.contact = {};
        if(items.type=='edit'){
            $scope.title = 'Update';
            $scope.btn = 'Update';
            masterService.contact.getById(items.editId).then(function(result){
                //console.log(result);
                $scope.contact = result.data;
            })
        }
        $scope.contact = {"business_contact":'',"business_contact_description":''};
        $scope.save = function($postData){
            if($postData.id_business_contact_type==undefined){
                $scope.add($postData);
            } else{
                $scope.update($postData);
            }
        };
        $scope.add = function($postData){
            masterService.contact.create($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                } else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };
        $scope.update = function($postData){
            masterService.contact.update($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $uibModalInstance.close();
                } else{
                    $rootScope.toast('Error',result.error,'error',$scope.branch);
                }
            });
        };

        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('customerUserCtrl',function($rootScope,$scope,companyService,$stateParams,encodeFilter){
        var company_id = encodeFilter($stateParams.id);

        $scope.userList = [];
        $scope.callServer = function callServer(tableState){
            //console.log('tableState',tableState);
            $scope.dtableState = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            var start = pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number||10;  // Number of entries showed per page.

            companyService.userList(company_id,tableState).then(function(result){
                $scope.userList = result.data.data;
                //console.log($scope.userList);
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                //console.log(tableState.pagination.numberOfPages);
                $scope.isLoading = false;
            });
        };
    })

    .controller('CreateCustomerCtrl',function($rootScope,$scope,masterService,companyService,Upload,$state,$stateParams,$q){

        $scope.customerId = $stateParams.customerId;

        $scope.uploadCompanyLogo = function(file){
            $scope.companyLogo = file;
            $scope.trash = true;
        };
        $scope.uploadCompanyLogoRemove = function(){
            $scope.companyLogo = '';
            $scope.trash = false;
        };
        $scope.uploadUserImage = function(file){
            if(file!=null&&file!=''){
                $scope.userImage = file;
                $scope.trashUser = true;
            }
        };
        $scope.uploadUserImageRemove = function(){
            $scope.userImage = '';
            $scope.trashUser = false;
        };

        $scope.Countries = [];
        var service1 = masterService.country.get().then(function($result){
            $scope.Countries = $result.data;
        });
        $scope.categories = {};
        var service2 = masterService.bankCategory.getAll().then(function($result){
            $scope.categories = $result.data.data;
        });

        $scope.Plans = [];
        var service3 = masterService.plans.get().then(function($result){
            //console.log($result.data);
            $scope.Plans = $result.data.data;
        });

        $scope.currencies = [];
        var service4 = masterService.currency.getAll().then(function($result){
            $scope.currencies = $result.data;
        });


        $scope.plan = {};
        $scope.planInfo = {};
        $scope.planChange = function(plan_id){
            $scope.planInfo = _.findWhere($scope.Plans,{'id_plan':plan_id});//searching Element in Array
        }


        if($scope.customerId>0){
            $q.all([service1,service2,service3,service4]).then(function(){
                companyService.companyInformation({'company_id':$scope.customerId}).then(function($result){
                    //console.log($result.data);
                    if($result.status){
                        $scope.planChange($result.data.company.plan_id);
                        $scope.user = $result.data.user;

                        $scope.company =$result.data.company ;
                        $scope.companyLogo = $scope.company.company_logo

                        $scope.userImage =$result.data.user.profile_image;
                    }
                });
            })
        }else{
           // $scope.user = {'first_name':'','last_name':'','email_id':'','phone_number':''};
            $scope.user ={};
            $scope.planInfo = {};
            $scope.company = {
                'company_name':'',
                'country_id':'',
                'company_address':'',
                'plan_id':'',
                'bank_category_id':'',
                'currency_id':''
            };
        }

        $scope.submitForm = function(company){

            $scope.user.user_role_id = 2;
            Upload.upload({
                url:API_URL+'company/company',
                data:{
                    file:{'profile_image':$scope.userImage,'company_logo':$scope.companyLogo},
                    'user':$scope.user,
                    'company':$scope.company
                }
            }).then(function(resp){
                if(resp.data.status){
                    $rootScope.toast('Success',resp.data.message);
                    $state.go("sa.index");
                } else{
                    $rootScope.toast('Error',resp.data.error,'error',{
                        'company_name':'',
                        'country_id':'',
                        'first_name':'',
                        'last_name':'',
                        'email_id':'',
                        'phone_number':'',
                        'company_address':'',
                        'plan_id':'',
                        'bank_category_id':''
                    });
                }
                ////console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            },function(resp){
                $rootScope.toast('Error',resp.data.message);
                //console.log('Error status: ' + resp.status);
            },function(evt){
                var progressPercentage = parseInt(100.0*evt.loaded/evt.total);
                //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        }

        $scope.goBack = function(state){
            $state.go(state);
        }
    })

    .controller('CustomerOverviewCtrl',function($rootScope,$scope,$stateParams,companyService,$state,encodeFilter){
        var id = encodeFilter($stateParams.id);
        //console.log(id);
        var companyDetail = {};
        companyService.getById(id).then(function($result){
            //console.log($result.data);
            if($result.status){
                $scope.companyDetail = $result.data;
            }
        });
        $state.go('sa.customer-overview.customer-overview-tab');
    })

    .controller('customerBranchesCtrl',function($rootScope,$scope,companyService,$stateParams,encodeFilter){
        var company_id = encodeFilter($stateParams.id);

        $scope.branchList = [];
        $scope.callServer = function callServer(tableState){
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            var start = pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number||10;  // Number of entries showed per page.

            companyService.branchTable(company_id,start,number).then(function(result){
                $scope.branchList = result.data.data;
                //console.log(result.data.total_records);
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                ////console.log(tableState.pagination.numberOfPages);
                $scope.isLoading = false;
            });
        };
    })






