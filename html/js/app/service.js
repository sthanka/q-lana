/**
 * A service that returns some data.
 */
'use strict';
angular.module('app')
    .factory('onlineStatus',["$window","$rootScope",function($window,$rootScope){
        var onlineStatus = {};

        onlineStatus.onLine = $window.navigator.onLine;

        onlineStatus.isOnline = function(){
            return onlineStatus.onLine;
        }

        $window.addEventListener("online",function(){
            onlineStatus.onLine = true;
            $rootScope.$digest();
        },true);

        $window.addEventListener("offline",function(){
            onlineStatus.onLine = false;
            $rootScope.$digest();
        },true);

        return onlineStatus;
    }])

    .factory('errorInterceptor',['$q','$rootScope','$location',
        function($q,$rootScope,$location){
            return {
                request:function(config){
                    return config||$q.when(config);
                },
                requestError:function(request){
                    return $q.reject(request);
                },
                response:function(response){
                    return response||$q.when(response);
                },
                responseError:function(response){
                    //console.log('net',response);
                    if(response&&response.status===404){
                        $rootScope.toast('404','Not Found','warning');
                    }
                    if(response&&response.status=== -1){
                        $rootScope.toast('Service Connection Error','Error while fetching URL','warning');
                    }
                    if(response&&response.status===401){
                        $rootScope.toast('401','session expired','warning');
                        window.localStorage.clear();
                        $rootScope.$broadcast('loggedOut');
                    }
                    if(response&&response.status>=500){
                        $rootScope.toast('500','INTERNAL SERVER ERROR','warning');
                    }
                    return $q.reject(response);
                }
            };
        }])

    .config(['$httpProvider',function($httpProvider){
        $httpProvider.interceptors.push('errorInterceptor');
    }])

    .factory("transformRequestAsFormPost",function(){
        // I prepare the request data for the form post.
        function transformRequest(data,getHeaders){
            var headers = getHeaders();
            headers["Content-type"] = "application/x-www-form-urlencoded; charset=utf-8";
            return ( serializeData(data) );
        }

        // Return the factory value.
        return ( transformRequest );
        // ---
        // PRVIATE METHODS.
        // ---
        // I serialize the given Object into a key-value pair string. This
        // method expects an object and will default to the toString() method.
        // --
        // NOTE: This is an atered version of the jQuery.param() method which
        // will serialize a data collection for Form posting.
        // --
        // https://github.com/jquery/jquery/blob/master/src/serialize.js#L45
        function serializeData(data){
            // If this is not an object, defer to native stringification.
            if(!angular.isObject(data)){
                return ( ( data==null ) ? "" : data.toString() );
            }
            var buffer = [];
            // Serialize each key in the object.
            for(var name in data){
                if(!data.hasOwnProperty(name)){
                    continue;
                }
                var value = data[name];
                buffer.push(
                    encodeURIComponent(name)+
                    "="+
                    encodeURIComponent(( value==null ) ? "" : value)
                );
            }
            // Serialize the buffer and clean it up for transportation.
            var source = buffer
                .join("&")
                .replace(/%20/g,"+")
                ;
            return ( source );
        }
    })
    //Auth
    .factory('Auth',function($rootScope,$http){
        return {
            checkLogin:function(){
                // Check if logged in and fire events
                if(this.isLoggedIn()){
                    $rootScope.$broadcast('loggedIn');
                    return true;
                } else{
                    $rootScope.$broadcast('loggedOut');
                    return false;
                }
            },
            isLoggedIn:function(){
                if(window.localStorage['app']!=undefined)
                    return true;
                return false;
                // Check auth token here from localStorage
            },
            getFields:function(){
                if(window.localStorage['app']!=undefined)
                    return window.localStorage['app'];
            },
            login:function(data){
                window.localStorage['app'] = angular.toJson(data);

                $http.defaults.headers.common['Authorization'] = data.access_token;
                $http.defaults.headers.common['User'] = data.id_user;

                //window.localStorage['app'].id = data.id;
                //window.localStorage['app'].name = data.name;
                //window.localStorage['app'].spouce = data.spouce;
                $rootScope.$broadcast('loggedIn');
            },
            logout:function(user,pass){

                window.localStorage.clear();
                $rootScope.$broadcast('loggedOut');
            }
        }
    })
    //User
    .factory('UserService',function($http,$cacheFactory,$rootScope,transformRequestAsFormPost){
        // Might use a resource here that returns a JSON array
        return {
            login:function($postData){

                return $http.post(API_URL+'signup/login',$postData)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            forgotPassword:function($postData){

                return $http.post(API_URL+'signup/forgetPassword?emailId='+$postData)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            changePassword:function($postData){

                return $http.post(API_URL+'user/changePassword',$postData)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            getUserProfile:function($id){

                return $http.get(API_URL+'user/userInfo?id='+$id)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            getLoginHistory:function($id,start,number,params){
                var params = '?user_id='+$id+'&offset='+start+'&limit='+number;
                return $http.get(API_URL+'user/loginHistory'+params)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            getUserNotifications:function($id,$userId,start,number,params){
                var params = '?company_id='+$id+'&sent_to='+$userId+'&offset='+start+'&limit='+number;
                return $http.get(API_URL+'activity/notification'+params)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            }
        }
    })
    //masterService
    .factory('masterService',function($http,$cacheFactory,$rootScope,Upload,$timeout){
        // Might use a resource here that returns a JSON array
        return {
            country:{
                getPage:function(start,number){
                    var paramss = '?offset='+start+'&limit='+number;

                    return $http.get(API_URL+'master/country'+paramss)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                }, //
                get:function(){

                    return $http.get(API_URL+'master/country')
                        .error(function(){

                        }).then(function(response){

                            return response.data.data;
                        });
                },
                createCountry:function($postData){

                    return $http.post(API_URL+'master/country',$postData)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                getById:function($id){
                    return $http.get(API_URL+'master/countryById/'+$id)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                edit:function($postData){

                    var str = Object.keys($postData).map(function(key){
                        return encodeURIComponent(key)+'='+encodeURIComponent($postData[key]);
                    }).join('&');
                    return $http.put(API_URL+'master/country/?'+str)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                }
            },
            branchType:{
                getPage:function(params){
                    //  var paramss = '?offset='+start+'&limit='+number;

                    return $http.get(API_URL+'master/branch',{params:params})
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                getAll:function(start,number){

                    return $http.get(API_URL+'master/branch')
                        .error(function(){

                        }).then(function(response){

                            return response.data.data;
                        });
                },
                createBranchType:function($postData){

                    return $http.post(API_URL+'master/branch',$postData)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                getById:function($id){
                    return $http.get(API_URL+'master/branchTypeById/'+$id)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                edit:function($postData){
                    var str = Object.keys($postData).map(function(key){
                        return encodeURIComponent(key)+'='+encodeURIComponent($postData[key]);
                    }).join('&');
                    return $http.put(API_URL+'master/branch/?'+str)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                }
            },
            approvalRole:{
                getPage:function(params){
                    //var paramss = '?offset='+start+'&limit='+number;
                    return $http.get(API_URL+'master/approvalRole',{params:params})
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                }, //
                get:function(){

                    return $http.get(API_URL+'master/approvalRole')
                        .error(function(){

                        }).then(function(response){

                            return response.data.data;
                        });
                },
                createApprovalRole:function($postData){

                    return $http.post(API_URL+'master/approvalRole',$postData)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                getById:function($id){
                    return $http.get(API_URL+'master/approvalRoleById/'+$id)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                edit:function($postData){
                    console.log($postData);
                    var str = Object.keys($postData).map(function(key){
                        return encodeURIComponent(key)+'='+encodeURIComponent($postData[key]);
                    }).join('&');
                    return $http.put(API_URL+'master/approvalRole/?'+str)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                }
            },
            plans:{
                get:function(start,number,param){
                    var paramss = '?offset='+start+'&limit='+number;

                    return $http.get(API_URL+'master/plansList'+paramss)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                createPlan:function($postData){

                    return $http.post(API_URL+'master/plans',$postData)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                getPage:function(start,number,params){

                    var paramss = '?offset='+start+'&limit='+number;

                    return $http.get(API_URL+'master/plansList'+paramss)
                        .error(function(){

                        }).then(function(response){
                            return response.data.data;
                        });
                },
                getPlan:function($id){

                    return $http.get(API_URL+'master/plans/'+$id)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                updatePlan:function($postData){
                    var str = Object.keys($postData).map(function(key){
                        return encodeURIComponent(key)+'='+encodeURIComponent($postData[key]);
                    }).join('&');

                    return $http.put(API_URL+'master/plans/?'+str)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                }
            },
            risk:{
                get:function(start,number,param){
                    var paramss = '?offset='+start+'&limit='+number;

                    return $http.get(API_URL+'master/riskList'+paramss)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                getById:function($id){
                    return $http.get(API_URL+'master/risk/'+$id)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                create:function($postData){

                    return $http.post(API_URL+'master/risk',$postData)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                update:function($postData){

                    var str = Object.keys($postData).map(function(key){
                        return encodeURIComponent(key)+'='+encodeURIComponent($postData[key]);
                    }).join('&');

                    return $http.put(API_URL+'master/risk/?'+str)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                }
            },
            social:{
                get:function(start,number,param){
                    var paramss = '?offset='+start+'&limit='+number;

                    return $http.get(API_URL+'master/socialList'+paramss)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                getById:function($id){
                    return $http.get(API_URL+'master/social/'+$id)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                create:function($postData){

                    return $http.post(API_URL+'master/social',$postData)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                update:function($postData){

                    var str = Object.keys($postData).map(function(key){
                        return encodeURIComponent(key)+'='+encodeURIComponent($postData[key]);
                    }).join('&');

                    return $http.put(API_URL+'master/social/?'+str)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                }
            },
            contact:{
                get:function(start,number,param){
                    var paramss = '?offset='+start+'&limit='+number;

                    return $http.get(API_URL+'master/contactList'+paramss)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                getById:function($id){
                    return $http.get(API_URL+'master/contact/'+$id)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                create:function($postData){

                    return $http.post(API_URL+'master/contact',$postData)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                update:function($postData){

                    var str = Object.keys($postData).map(function(key){
                        return encodeURIComponent(key)+'='+encodeURIComponent($postData[key]);
                    }).join('&');

                    return $http.put(API_URL+'master/contact/?'+str)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                }
            },
            bankCategory:{
                getAll:function(start,number,param){
                    var paramss = '?offset='+start+'&limit='+number;
                    return $http.get(API_URL+'master/bankCategoryList'+paramss)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                get:function(){

                    return $http.get(API_URL+'master/bankCategory')
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                getPage:function(start,number,params){
                    var paramss = '?offset='+start+'&limit='+number;

                    return $http.get(API_URL+'master/bankCategory'+paramss)
                        .error(function(){

                        }).then(function(response){
                            return response.data.data;
                        });
                },
                getBankCategory:function($id){

                    return $http.get(API_URL+'master/bankCategoryById/'+$id)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                updateBankCategory:function($postData){
                    var str = Object.keys($postData).map(function(key){
                        return encodeURIComponent(key)+'='+encodeURIComponent($postData[key]);
                    }).join('&');


                    return $http.put(API_URL+'master/bankCategory/?'+str)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                createBankCategory:function($postData){

                    return $http.post(API_URL+'master/bankCategory',$postData)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                }
            },
            sector:{
                getPage:function(start,number,params){
                    console.log(params);
                    var paramss = '?offset='+start+'&limit='+number;


                    return $http.get(API_URL+'master/sectorList'+paramss)
                        .error(function(){

                        }).then(function(response){
                            return response.data.data;
                        });
                },
                getAll:function(params){
                    return $http.get(API_URL+'master/sector',{params:params}).then(function(response){
                        return response.data;
                    });
                },
                getAssessmentSectors:function(params){
                    return $http.get(API_URL+'master/sector',{params:params})
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                getAllSectors:function(){
                    return $http.get(API_URL+'master/sector/?parent_sector_id=0')
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                getLimitSectorsAll:function(){
                    return $http.get(API_URL+'company/approvalLimitSector')
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                getSubSector:function(parentId){
                    return $http.get(API_URL+'master/subSector/?id_sector='+parentId)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                getById:function($id){
                    return $http.get(API_URL+'master/sectorById/'+$id)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                add:function(data){
                    return $http.post(API_URL+'master/sector',data)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                },
                edit:function(data){
                    var str = Object.keys(data).map(function(key){
                        return encodeURIComponent(key)+'='+encodeURIComponent(data[key]);
                    }).join('&');
                    return $http.put(API_URL+'master/sector/?'+str)
                        .error(function(){

                        }).then(function(response){

                            return response.data;
                        });
                }
            },
            currency:{
                getAll:function(params){
                    return $http.get(API_URL+'master/currency',{params:params})
                        .then(function(response){
                            return response.data;
                        });
                },
                getTable:function(params){
                    return $http.get(API_URL+'master/currencyList',{params:params})
                        .then(function(response){
                            return response.data;
                        });
                },
                post:function(file,params){

                    return Upload.upload({
                        url:API_URL+'master/currency',
                        data:{
                            file:{'country_flag':file},
                            'currency':params
                        }
                    }).then(function(response){
                        return response.data;
                        ////console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                    },function(resp){
                        $rootScope.toast('Error',resp.data.message);
                        //console.log('Error status: ' + resp.status);
                    },function(evt){
                        var progressPercentage = parseInt(100.0*evt.loaded/evt.total);
                        //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                    });


                    /*  return $http.post(API_URL+'master/currency',params)
                     .then(function(response){
                     return response.data;
                     });*/
                }

            },
            projectLoanType:function(params){
                return $http.get(API_URL+'master/projectLoanType',{params:params})
                    .then(function(response){
                        return response.data;
                    });
            },
            projectPaymentType:function(params){
                return $http.get(API_URL+'master/projectPaymentType',{params:params})
                    .then(function(response){
                        return response.data;
                    });
            },
            getProductType:function(company_id,params){
                return $http.get(API_URL+'crm/product?company_id='+company_id,{params:params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getProductById:function(product_id,company_id){
                return $http.get(API_URL+'crm/product?id_product='+product_id+'&company_id='+company_id)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            addProductType:function(data){
                return $http.post(API_URL+'crm/product',data)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getApprovalLimitType:function(company_id,product_id){
                return $http.get(API_URL+'company/approvalLimit?company_id='+company_id+'&product_id='+product_id)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            addSectorToLimits:function(params){
                return $http.get(API_URL+'company/checkApprovalLimitSector',{params:params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            postApprovalLimits:function($postData){
                return $http.post(API_URL+'company/approvalLimit',$postData)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            updateApprovalStructure:function($postData){
                return $http.post(API_URL+'company/product',$postData)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            deleteApprovalLimitSector:function($postData){
                return $http.delete(API_URL+'company/approvalLimit',{params:$postData})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            }

        }
    })
    //company
    .factory('companyService',function($http,$cacheFactory,$rootScope){
        // Might use a resource here that returns a JSON array
        return {
            get:function(){

                return $http.get(API_URL+'company/companyList')
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            getById:function($id){

                return $http.get(API_URL+'company/company/'+$id)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            companyInformation:function(params){
                return $http.get(API_URL+'company/companyInformation',{params:params})
                    .error(function(){

                    }).then(function(response){
                        return response.data;
                    });
            },
            post:function($postData){

                return $http.post(API_URL+'company/company',$postData)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            userList:function($companyId,$params){

                return $http.get(API_URL+'company/userList?company_id='+$companyId,{params:$params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },

            /**
             *company_id
             * currency_id
             * @returns {*}
             */
            primaryCurrency:function(params){
                return $http.post(API_URL+'company/primaryCurrency',params).then(function(response){
                    return response.data;
                });
            },
            user:function($userId){
                return $http.get(API_URL+'company/user/'+$userId)
                    .then(function(response){
                        return response.data;
                    });
            },
            userView:function(params){
                /**
                 * company_id , user_id
                 */
                return $http.get(API_URL+'company/userList/',{params:params})
                    .then(function(response){
                        return response.data;
                    });
            },
            branchView:function(params){
                return $http.get(API_URL+'company/branchDetails/',{params:params})
                    .then(function(response){
                        return response.data;
                    });
            },
            branch:function($Id){
                return $http.get(API_URL+'company/branch/'+$Id)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            branchList:function($companyId,$branchId,start,number){
                var params = '';
                if($branchId==undefined) $branchId = 0;
                if(start!=undefined&&number!=undefined){
                    params = '&offset='+start+'&limit='+number;
                }
                return $http.get(API_URL+'company/companyBranch/'+$companyId+'/'+$branchId)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            /**
             *
             * @param company_id branch_type_id(optional)
             * @returns {*|Promise.<T>}
             */
            companyApprovalRoles:function(params){
                return $http.get(API_URL+'company/companyApprovalRole',{params:params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            companyBranchesList:function($companyId,branch_type_id){
                return $http.get(API_URL+'company/companyBranchList/?company_id= '+$companyId+' &branch_type_id='+branch_type_id)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            companyBranches:function($companyId,$branchTypeId,$branchId){
                return $http.get(API_URL+'company/companyBranches?company_id='+$companyId+'&branch_type_id='+$branchTypeId+'&branch_id='+$branchId)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            branchReportingToUsers:function($postData){
                return $http.get(API_URL+'company/userList',{params:$postData})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            branchTable:function($companyId,start,number){
                var params = '';
                if(start!=undefined&&number!=undefined){
                    params = '&offset='+start+'&limit='+number;
                }
                return $http.get(API_URL+'company/companyBranchList?company_id='+$companyId+params)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            branchStructure:function($companyId){
                return $http.get(API_URL+'company/branches/'+$companyId)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            roleStructure:function($companyId){
                return $http.get(API_URL+'company/approvals/'+$companyId)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            download:function(){

                return $http.get(API_URL+'company/downloadBranchExcel')
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            downloadUsers:function(){

                return $http.get(API_URL+'company/downloadUserExcel')
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            branchTypeStructure:function($postData){
                return $http.post(API_URL+'company/companyBranchType',$postData)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getBranchTypeStructure:function($company_id){
                return $http.get(API_URL+'company/branchTypeStructure/'+$company_id)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            approvalTypeStructure:function($postData){
                return $http.post(API_URL+'company/approvalRole',$postData)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getApprovalTypeStructure:function($company_id){
                return $http.get(API_URL+'company/approvalTypeStructure/'+$company_id)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            chartCustomersByCountry:function(){
                return $http.get(API_URL+'dashboard/customersByCountry')
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            chartCustomersByCategory:function(){
                return $http.get(API_URL+'dashboard/customersByCategory')
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            companyBranchType:function($companyId,$branchTypeId){
                if($branchTypeId==undefined){
                    $branchTypeId = 0;
                }
                return $http.get(API_URL+'company/companyBranchType?company_id='+$companyId)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            branchTypeRole:function(params){
                return $http.get(API_URL+'company/branchTypeRole',{params:params}).then(function(response){
                    return response.data;
                });
            },
            branchType:{
                post:function(params){
                    return $http.post(API_URL+'company/branchType',params).then(function(response){
                        return response.data;
                    });
                },
                get:function(params){
                    return $http.get(API_URL+'company/branchType',{params:params}).then(function(response){
                        return response.data;
                    });
                }
            },
            companyApprovalRole:function(params){
                return $http.get(API_URL+'company/companyApprovalRole',{params:params}).then(function(response){
                    return response.data;
                });
            },
            knowledgeManagement:{
                getTags:function(){
                    return $http.get(API_URL+'company/knowledgeTags').then(function(response){
                        return response.data;
                    });
                },
                get:function(params){
                    return $http.get(API_URL+'company/knowledge',{params:params}).then(function(response){
                        return response.data;
                    });
                },
                getSearchResult:function(params){
                    return $http.get(API_URL+'company/knowledgeList',{params:params}).then(function(response){
                        return response.data;
                    });
                },
                knowledgeManagementGraph:function(params){
                    return $http.get(API_URL+'company/knowledgeGraph',{params:params}).then(function(response){
                        return response.data;
                    });
                },
                getDetails:function(params){
                    return $http.get(API_URL+'crm/knowledgeDocumentDetails',{params:params}).then(function(response){
                        return response.data;
                    });
                },
                knowledgePublish:function($postData){
                    return $http.post(API_URL+'crm/knowledgePublish',$postData)
                        .error(function(){
                        }).then(function(response){
                            return response.data;
                        });
                }
            },
            knowledgeDocumentComment:{
                get:function(params){
                    return $http.get(API_URL+'crm/knowledgeDocumentComment',{params:params}).then(function(response){
                        return response.data;
                    });
                },
                add:function($postData){
                    return $http.post(API_URL+'crm/knowledgeDocumentComment',$postData)
                        .error(function(){
                        }).then(function(response){
                            return response.data;
                        });
                }

            },
            approvalStructure:{
                /**
                 * type Obj
                 * @params comapnay Id
                 */
                get:function(params){

                    return $http.get(API_URL+'company/approvalStructure',{params:params}).then(function(response){
                        return response.data;
                    });
                },
                /**
                 * type object
                 * @param params company_id,approval_structure_name
                 * if update  approval_id
                 */
                post:function(params){
                    return $http.post(API_URL+'company/approvalStructure',params).then(function(response){
                        return response.data;
                    });
                }
            },
            approvalCommittees:{
                /**
                 * type Obj
                 * @params comapnay Id
                 */
                get:function(params){

                    return $http.get(API_URL+'company/approvalStructure',{params:params}).then(function(response){
                        return response.data;
                    });
                },
                /**
                 *
                 * @param params
                 * @returns {}
                 */
                post:function(params){
                    return $http.post(API_URL+'company/approvalCreditCommittee',params).then(function(response){
                        return response.data;
                    });
                },
                /**
                 *
                 */

                forwardCommittee:function(params){

                    return $http.post(API_URL+'company/forwardCommittee',params).then(function(response){
                        return response.data;
                    });
                },
                approvalCreditCommitteeList:function(params){
                    return $http.get(API_URL+'company/approvalCreditCommitteeList',{params:params}).then(function(response){
                        return response.data;
                    });
                },
                /**
                 *
                 * @param params id_company_approval_credit_committee
                 * @returns {*}
                 */
                approvalCreditCommitteeById:function(params){
                    return $http.get(API_URL+'company/approvalCreditCommittee',{params:params}).then(function(response){
                        return response.data;
                    });
                }

            },
            assessmentQuestion:{
                category:{
                    get:function(params){
                        return $http.get(API_URL+'company/assessmentQuestionCategory',{params:params}).then(function(response){
                            return response.data;
                        });
                    },
                    post:function(params){
                        return $http.post(API_URL+'company/assessmentQuestionCategory',params).then(function(response){
                            return response.data;
                        });
                    }
                },
                question:{
                    get:function(params){
                        return $http.get(API_URL+'company/assessmentQuestion',{params:params}).then(function(response){
                            return response.data;
                        });
                    },
                    post:function(params){
                        return $http.post(API_URL+'company/assessmentQuestion',params).then(function(response){
                            return response.data;
                        });
                    }
                }

            },
            riskAssessment:{
                riskCategory:{
                    /*
                     *@params get company_id
                     */
                    get:function($params){
                        return $http.get(API_URL+'company/riskCategory',{params:$params})
                            .then(function(response){
                                return response.data;
                            });
                    },
                    /**
                     * @params
                     * risk_category_name
                     * parent_risk_category_id
                     * company_id
                     * risk_percentage
                     *
                     */
                    post:function($params){
                        return $http.post(API_URL+'company/riskCategory',$params)
                            .then(function(response){
                                return response.data;
                            });
                    }
                },
                riskItem:{
                    /*
                     *risk_category_id
                     *  risk_category_item_name
                     *  risk_category_item_grade
                     *  sector_id
                     *
                     */
                    post:function($params){
                        return $http.post(API_URL+'company/riskCategoryItem',$params)
                            .then(function(response){
                                return response.data;
                            });
                    }

                }
            },
            currency:{
                /**
                 *
                 * @param company_id
                 * @returns {*}
                 */
                get:function(params){
                    return $http.get(API_URL+'company/companyCurrency',{params:params})
                        .then(function(response){
                            return response.data;
                        });
                },
                currencyTable:function(params){
                    return $http.get(API_URL+'company/companyCurrencyList',{params:params})
                        .then(function(response){
                            return response.data;
                        });
                },
                /**
                 *
                 * @param company_id
                 * @returns {*}
                 */
                post:function(params){
                    return $http.post(API_URL+'company/companyCurrency',params)
                        .then(function(response){
                            return response.data;
                        });
                },
                /**
                 *
                 * @param company_id
                 * @returns {*}
                 * @constructor
                 */
                CurrencySelected:function(params){
                    return $http.get(API_URL+'company/companyCurrencySelected',{params:params})
                        .then(function(response){
                            return response.data;
                        });
                },
                /**
                 *
                 */
                currencyValue:function(params){
                    return $http.post(API_URL+'company/companyCurrencyValue',params)
                        .then(function(response){
                            return response.data;
                        });
                }

            },
            collateral:{
                collateralType:function(params){
                    return $http.get(API_URL+'company/collateralType',{params:params})
                        .then(function(response){
                            return response.data;
                        });
                },
                collateralStage:{
                    get:function(params){
                        return $http.get(API_URL+'company/collateralStage',{params:params})
                            .then(function(response){
                                return response.data;
                            });
                    },
                    post:function(params){
                        return $http.post(API_URL+'company/collateralTypeStage',params)
                            .then(function(response){
                                return response.data;
                            });
                    }

                }
            },
            covenant:{
                get:function(params){
                    return $http.get(API_URL+'company/covenant',{params:params})
                        .then(function(response){
                            return response.data;
                        });
                },
                post:function(params){
                    return $http.post(API_URL+'company/covenant',params)
                        .then(function(response){
                            return response.data;
                        });
                },
                delete:function($params){
                    return $http.delete(API_URL+'crm/covenant',{params:$params})
                        .then(function(response){
                            return response.data;
                        });
                },
                type:{
                    get:function(params){
                        return $http.get(API_URL+'company/covenantType',{params:params})
                            .then(function(response){
                                return response.data;
                            });
                    }
                },
                category:{
                    get:function(params){
                        return $http.get(API_URL+'company/covenantCategory',{params:params})
                            .then(function(response){
                                return response.data;
                            });
                    },
                    /**
                     * company_id=1&covenant_type_key=positive_covenant
                     * @param params
                     * @returns {*|Promise.<T>}
                     */
                    list:function(params){
                        return $http.get(API_URL+'company/covenantCategoryList',{params:params})
                            .then(function(response){
                                return response.data;
                            });
                    },
                    /**
                     *
                     * @param covenant_type_id.
                     sector(1,2,3)
                     covenant_category
                     company_id
                     * @returns {*|Promise.<T>}
                     */
                    post:function(params){
                        return $http.post(API_URL+'company/covenantCategory',params)
                            .then(function(response){
                                return response.data;
                            });
                    }

                }
            },
            role:{
                /**
                 * company_id
                 * @returns {*|Promise.<T>}
                 */
                get:function(params){
                    return $http.get(API_URL+'company/applicationRole',{params:params})
                        .then(function(response){
                            return response.data;
                        });
                },
                /**
                 * company_id
                 * @returns {*|Promise.<T>}
                 */
                post:function(params){
                    return $http.post(API_URL+'company/applicationRole',params)
                        .then(function(response){
                            return response.data;
                        });
                },
                mapping:{
                    /**
                     *
                     * @param params
                     * application_role_id company_approval_role_id or user_id
                     * type: approval_role or user
                     * @returns {*|Promise.<T>}
                     */
                    get:function(params){
                        return $http.get(API_URL+'company/applicationUserRole',{params:params})
                            .then(function(response){
                                return response.data;
                            });
                    },
                    post:function(params){
                        return $http.post(API_URL+'company/applicationUserRole',params)
                            .then(function(response){
                                return response.data;
                            });
                    },
                    delete:function(params){
                        return $http.delete(API_URL+'company/applicationUserRole',{params:params})
                            .then(function(response){
                                return response.data;
                            });
                    }
                },
                /**
                 *
                 * @param
                 * @returns {*|Promise.<T>}
                 */
                module:{
                    get:function(params){
                        return $http.get(API_URL+'company/module',{params:params})
                            .then(function(response){
                                return response.data;
                            });
                    },
                    post:function(params){
                        return $http.post(API_URL+'company/module',params)
                            .then(function(response){
                                return response.data;
                            });
                    }
                }


            }


        }
    })
    //crm
    .factory('crmService',function($http,$rootScope,$state,$stateParams,$timeout,$q){
        return {
            touchPoint:{
                get:function(params){
                    return $http.get(API_URL+'crm/touchPoint',{params:params})
                        .error(function(){
                        }).then(function(response){
                            return response.data;
                        });
                },
                post:function($postData){
                    return $http.post(API_URL+'crm/touchPoints',$postData)
                        .error(function(){
                        }).then(function(response){
                            return response.data;
                        });
                },
                touchPointType:function(){
                    return $http.get(API_URL+'crm/touchPointType')
                        .error(function(){
                        }).then(function(response){
                            return response.data;
                        });
                }
            },
            modulePermission:{
                /**
                 *
                 * @param company_id &module_key&user_id
                 * @returns {*|Promise.<T>}
                 */
                get:function(params){
                    return $http.get(API_URL+'crm/moduleAccess',{params:params})
                        .error(function(){
                        }).then(function(response){
                            return response.data;
                        });
                },
            },
            checkUrlPermission:function(user_type){

                if($rootScope.user_role_id == user_type){
                    return true;
                } else{
                   // $rootScope.toast('Error',result.error,'l-error');
                    $timeout(function(){
                        console.log('user_type',user_type);
                        $state.go('error.404');
                    });
                    return false;
                }

            },
            setPermission:function(module_key){
                var defer = $q.defer();
                var params = {};
                // console.log('$state.current',$state.current);
                params.module_key =module_key; //$state.current.data.module_key;
                params.user_id = $rootScope.userId;
                params.company_id = $rootScope.id_company;
                params.module_access = 1;
                $http.get(API_URL+'crm/moduleAccess',{params:params}).then(function(response){
                    var result = response.data;
                    $rootScope.permission = {};
                    angular.forEach(result.data,function(value,key){
                        angular.forEach(value,function(value,key){
                            $rootScope.permission[key] = value
                        })
                    })
                    if(result.status){
                        defer.resolve(result.data);
                    } else{
                        $rootScope.toast('Error',result.error,'l-error');
                        $timeout(function(){
                            $state.go('error.404');
                        })
                        defer.reject();
                    }
                });
                return defer.promise;
            }
        }
    })

    //contact
    .factory('crmContactService',function($http,$cacheFactory,$rootScope,transformRequestAsFormPost){
        return {
            contactInformation:function(params){
                return $http.get(API_URL+'crm/contactInformation',{params:params})
                    .then(function(response){
                        return response.data;
                    });
            },
            contactForms:function(params){
                return $http.get(API_URL+'crm/contactForms',{params:params})
                    .then(function(response){
                        return response.data;
                    });
            },
            getModules:function($module_id){

                return $http.get(API_URL+'crm/module/'+$module_id)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            saveApplicant:function($postData){

                return $http.post(API_URL+'crm/formData',$postData)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            contactList:function($companyId,$params){
                return $http.get(API_URL+'crm/contactList?company_id='+$companyId,{params:$params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            crmContactType:function(){
                return $http.get(API_URL+'crm/crmContactType')
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            changeUserContactType:function($postData){
                return $http.post(API_URL+'crm/contact',$postData)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            contactDetails:function($companyId,start){
                return $http.get(API_URL+'crm/contact/'+$companyId+'/'+start)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            deleteProjectContact:function(contactId){
                return $http.delete(API_URL+'crm/projectContact/'+contactId)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            contactFormDetails:function(id_crm_contact,formId,type){
                return $http.get(API_URL+'crm/formData/'+id_crm_contact+'/'+formId+'/'+type)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            contactType:function(){
                return $http.get(API_URL+'crm/contactType')
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getAttachmentsTypes:function(type){
                return $http.get(API_URL+'crm/documentType/'+type)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getUploadedAttachments:function(type,group_id){
                return $http.get(API_URL+'crm/document/?module_id='+type+'&group_id='+group_id)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getTouchPointsList:function(params){
                return $http.get(API_URL+'crm/touchPoints/?'+params)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            touchPointType:function(){
                return $http.get(API_URL+'crm/touchPointType')
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            contactTouchPoints:function(params){
                params.crm_module_type = 'contact';
                return $http.get(API_URL+'crm/touchPoint',{params:params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getCompanyTouchPoints:function(params){
                params.crm_module_type = 'company';
                return $http.get(API_URL+'crm/touchPoint',{params:params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getProjectTouchPoints:function(params){
                params.crm_module_type = 'project';
                return $http.get(API_URL+'crm/touchPoint',{params:params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            addTouchPointComment:function($postData){
                return $http.post(API_URL+'crm/touchPoints',$postData)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getCompanyAssessment:function($params){
                return $http.get(API_URL+'company/companyAssessment',{params:$params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            /**
             * company_id
             * @returns {*}
             */
            getAllAssessment:function($params){
                return $http.get(API_URL+'company/assessment',{params:$params})
                    .then(function(response){
                        return response.data;
                    });
            },
            updateAssessment:function($postData){
                return $http.post(API_URL+'company/companyAssessment',$postData)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            addCompanyAssessmentItemStep:function($postData){
                return $http.post(API_URL+'company/companyAssessmentItemStep',$postData)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            deleteCompanyAssessmentItemStep:function($postData){
                return $http.delete(API_URL+'company/companyAssessmentItemStep',{params:$postData})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            }
        }
    })
    //company
    .factory('crmCompanyService',function($http,$cacheFactory,$rootScope,transformRequestAsFormPost){
        return {
            getModules:function($module_id,$crm_id){

                return $http.get(API_URL+'crm/module/'+$module_id+'/'+$crm_id)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            getFinancialInfo:function(params){
                return $http.get(API_URL+'company/financialStatements',{params:params})
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            saveCompany:function($postData){

                return $http.post(API_URL+'crm/company',$postData)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },

            companyList:function($companyId,$params){
                var str = '';
                /* if ($params) {
                 str = Object.keys($params).map(function (key) {
                 return encodeURIComponent(key) + '=' + encodeURIComponent($params[key]);
                 }).join('&');
                 }*/

                return $http.get(API_URL+'crm/companyList?company_id='+$companyId,{params:$params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            companyDesignation:function($companyId,$params){
                return $http.get(API_URL+'crm/companyDesignation/')
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            companyDetails:function($companyId,formId){
                return $http.get(API_URL+'crm/company/'+$companyId+'/'+formId)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },

            companyFormDetails:function($companyId,formId,type){
                return $http.get(API_URL+'crm/companyForm/'+$companyId+'/'+formId+'/'+type)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            saveFinancialInfo:function($infoKey){
                return $http.post(API_URL+'company/financialExcel',$infoKey)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getIntelligenceData:function(params){
                return $http.get(API_URL+'crm/intelligence/',{params:params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getKnowledgeData:function(params){
                return $http.get(API_URL+'crm/knowledge/',{params:params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            updateViewCount:function(param){
                return $http.post(API_URL+'crm/knowledgeCount',param)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getFinancialData:function($params){
                return $http.get(API_URL+'company/financialInformation/',{params:$params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            addMapModule:function($fields){
                return $http.post(API_URL+'crm/mapModule',$fields)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },

            deleteContactCompany:function($id){
                return $http.delete(API_URL+'crm/companyContact/'+$id)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            crmListForMapping:function($params){
                var str = '';
                if($params){
                    str = Object.keys($params).map(function(key){
                        return encodeURIComponent(key)+'='+encodeURIComponent($params[key]);
                    }).join('&');
                }

                return $http.get(API_URL+'crm/crmListForMapping/?'+str)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },

            getCompanyListByContactId:function($params){
                return $http.get(API_URL+'crm/mapModule',{params:$params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getCompanyList:function($params){
                return $http.get(API_URL+'crm/companyList',{params:$params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            companyType:function(){
                return $http.get(API_URL+'crm/companyType')
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            projectCompany:function($fields){
                return $http.post(API_URL+'crm/projectCompany',$fields)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            projectCompanyListFun:function($postData){
                return $http.get(API_URL+'crm/projectCompany',{params:$postData})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            deleteProjectCompany:function($postData){
                return $http.delete(API_URL+'crm/projectCompany/'+$postData)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getcompanyPriviewDetails:function($params){
                return $http.get(API_URL+'company/allCompanyAssessments',{params:$params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            companyFinancialInformation:function($params){
                return $http.get(API_URL+'company/companyFinancialInformation',{params:$params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            assessmentQuestion:{
                question:{
                    get:function(params){
                        return $http.get(API_URL+'company/assessmentQuestion',{params:params}).then(function(response){
                            return response.data;
                        });
                    }
                },
                answer:{
                    post:function(params){
                        return $http.post(API_URL+'company/assessmentAnswer',params).then(function(response){
                            return response.data;
                        });
                    }
                }

            }


        }
    })
    //Project
    .factory('crmProjectService',function($http,$cacheFactory,$rootScope,transformRequestAsFormPost){
        return {
            currency:{
                /**
                 *
                 * @param company_id
                 * @returns {*}
                 */
                get:function(params){
                    return $http.get(API_URL+'crm/currency',{params:params})
                        .then(function(response){
                            return response.data;
                        });
                },
            },
            getModules:function($module_id){

                return $http.get(API_URL+'crm/module/'+$module_id)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            saveProject:function($postData){

                return $http.post(API_URL+'crm/project',$postData)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            projectList:function($companyId,$params){
                var str = '';
                return $http.get(API_URL+'crm/projectList?company_id='+$companyId,{params:$params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });

            },
            projectDetails:function($projectId,formId,userId){
                return $http.get(API_URL+'crm/project/'+$projectId+'/'+formId+'/'+userId)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            projectProductTerm:function($params){
                return $http.get(API_URL+'company/productTerm',{'params':$params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },

            projectFormDetails:function($companyId,formId,type){
                return $http.get(API_URL+'crm/projectForm/'+$companyId+'/'+formId+'/'+type)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },

            getProjectContactList:function($companyId,userId){
                return $http.get(API_URL+'company/userList/?company_id='+$companyId+'&current_user_id='+userId)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },

            projectReportingUser:function($postData){
                return $http.post(API_URL+'activity/approval',$postData)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            committeeSecretary:function($params){
                return $http.get(API_URL+'company/committeeSecretary',{params:$params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            projectTerm:{
                projectTremesAndConditions:function($params){
                    return $http.get(API_URL+'crm/terms',{params:$params})
                        .error(function(){
                        }).then(function(response){
                            return response.data;
                        });
                },
                projectTremesAndConditionUpdate:function($postData){
                    return $http.post(API_URL+'crm/productTermItemData',$postData)
                        .error(function(){
                        }).then(function(response){
                            return response.data;
                        });
                },
                projectTerms:function($params){
                    return $http.get(API_URL+'crm/projectTerms',{params:$params})
                        .error(function(){
                        }).then(function(response){
                            return response.data;
                        });
                }

            },
            riskAssessment:{
                /*
                 *@params get company_id
                 * crm_project_id
                 */
                get:function($params){
                    return $http.get(API_URL+'crm/riskCategory',{params:$params})
                        .then(function(response){
                            return response.data;
                        });
                },
                /**
                 * @params
                 *crm_project_id
                 *user_id
                 * attribute data array
                 */
                post:function($params){
                    return $http.post(API_URL+'crm/projectRiskCategory',$params)
                        .then(function(response){
                            return response.data;
                        });
                }

            },
            statusFlow:{
                /*
                 *@params get company_id
                 * crm_project_id company_id
                 */
                get:function($params){
                    return $http.get(API_URL+'crm/statusFlow',{params:$params})
                        .then(function(response){
                            return response.data;
                        });
                },
            },
            facility:{
                /**
                 *
                 * @param @company_id @crm_project_id
                 * @returns {*}
                 */
                get:function($params){
                    return $http.get(API_URL+'crm/facility',{params:$params})
                        .then(function(response){
                            return response.data;
                        });
                },
                /**
                 *
                 * @param
                 * id_project_facility
                 crm_project_id
                 currency_id
                 loan_amount
                 expected_start_date
                 expected_maturity
                 expected_maturity_type
                 expected_interest_rate
                 project_loan_type_id
                 project_payment_type_id
                 project_facility_status
                 created_by
                 created_date_time

                 * @returns {*}
                 */
                post:function($params){
                    return $http.post(API_URL+'crm/facility',$params)
                        .then(function(response){
                            return response.data;
                        });
                }
            },
            collateral:{
                /**
                 *
                 * @param @company_id @crm_project_id
                 * @returns {*}
                 */
                getList:function($params){
                    return $http.get(API_URL+'crm/collateralList',{params:$params})
                        .then(function(response){
                            return response.data;
                        });
                },
                get:function($params){
                    return $http.get(API_URL+'crm/collateral',{params:$params})
                        .then(function(response){
                            return response.data;
                        });
                },
                /**
                 *
                 * @param
                 * id_project_facility
                 crm_project_id
                 currency_id
                 loan_amount
                 expected_start_date
                 expected_maturity
                 expected_maturity_type
                 expected_interest_rate
                 project_loan_type_id
                 project_payment_type_id
                 project_facility_status
                 created_by
                 created_date_time

                 * @returns {*}
                 */
                post:function($params){
                    return $http.post(API_URL+'crm/collateral',$params)
                        .then(function(response){
                            return response.data;
                        });
                },
                /**
                 *
                 * @param params company_id
                 * @returns {*|Promise.<T>}
                 */
                collateralType:function(params){
                    return $http.get(API_URL+'company/collateralType',{params:params})
                        .then(function(response){
                            return response.data;
                        });
                },
                collateralTypeFormData:{
                    /**
                     *
                     * @param collateral_id,collateral_type_id
                     * @returns {*|Promise.<T>}
                     */
                    get:function(params){
                        return $http.get(API_URL+'crm/collateralTypeFormData',{params:params})
                            .then(function(response){
                                return response.data;
                            });
                    },
                    /**
                     * collateral_id,collateral_type_id form data key values
                     * @param params
                     */
                    post:function(params){
                        return $http.post(API_URL+'crm/collateralTypeFormData',params)
                            .then(function(response){
                                return response.data;
                            });
                    }

                },
                collateralStageFormData:{
                    get:function(params){
                        return $http.get(API_URL+'crm/collateralStageFormData',{params:params})
                            .then(function(response){
                                return response.data;
                            });
                    },
                    post:function(params){
                        return $http.post(API_URL+'crm/collateralStageFormData',params)
                            .then(function(response){
                                return response.data;
                            });
                    },
                    delete:function($postData){
                        return $http.delete(API_URL+'crm/collateralStageFormData',{params:$postData})
                            .then(function(response){
                                return response.data;
                            });
                    }

                },
                collateralQuestion:{
                    /**
                     *
                     * @param collateral_id,collateral_type_id
                     * @returns {*|Promise.<T>}
                     */
                    get:function(params){
                        return $http.get(API_URL+'crm/collateralTypeDocuments',{params:params})
                            .then(function(response){
                                return response.data;
                            });
                    },
                }


            },
            covenant:{
                /**
                 *
                 * @param crm_project_id , company_id
                 * @returns {*|Promise.<T>}
                 */
                get:function(params){
                    return $http.get(API_URL+'crm/covenant',{params:params})
                        .then(function(response){
                            return response.data;
                        });
                },
                /**
                 *
                 * @param crm_project_id , company_id
                 * @returns {*|Promise.<T>}
                 */
                projectCovenant:function(params){
                    return $http.get(API_URL+'crm/projectCovenant',{params:params})
                        .then(function(response){
                            return response.data;
                        });
                },
                getCovenantsCategoryItems:function(params){
                    return $http.get(API_URL+'crm/covenant',{params:params})
                        .then(function(response){
                            return response.data;
                        });
                },
                /**
                 *
                 * @param crm_project_id , company_id , covenant_ids
                 * @returns {*|Promise.<T>}
                 */
                post:function(params){
                    return $http.post(API_URL+'crm/covenant',params)
                        .then(function(response){
                            return response.data;
                        });
                }
            }


        }

    })
    .factory('taskService',function($http,$cacheFactory,$rootScope,transformRequestAsFormPost){
        return {
            saveTask:function($postData){

                return $http.post(API_URL+'activity/task',$postData)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            getTasks:function($params){
                return $http.get(API_URL+'activity/task',{params:$params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            deleteTask:function($params){
                return $http.delete(API_URL+'activity/task',{params:$params})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            updateTaskStatus:function($params){
                return $http.post(API_URL+'activity/updateTask',$params)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            }
        }

    })
    //quickService
    .factory('quickService',function($http,$cacheFactory,$rootScope,transformRequestAsFormPost){
        return {
            saveQuickContact:function($postData){

                return $http.post(API_URL+'crm/quickContact',$postData)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            saveQuickCompany:function($postData){
                return $http.post(API_URL+'crm/quickCompany',$postData)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            },
            saveQuickProject:function($postData){
                return $http.post(API_URL+'crm/quickProject',$postData)
                    .error(function(){

                    }).then(function(response){

                        return response.data;
                    });
            }

        }

    })


    .factory('Resource',['$q','$filter','$timeout',function($q,$filter,$timeout){

        //this would be the service to call your server, a standard bridge between your model an $http

        // the database (normally on your server)
        var randomsItems = [];

        function createRandomItem(id){
            var heroes = ['Batman','Superman','Robin','Thor','Hulk','Niki Larson','Stark','Bob Leponge'];
            return {
                id:id,
                name:heroes[Math.floor(Math.random()*7)],
                age:Math.floor(Math.random()*1000),
                saved:Math.floor(Math.random()*10000)
            };

        }

        for(var i = 0; i<1000; i++){
            randomsItems.push(createRandomItem(i));
        }


        //fake call to the server, normally this service would serialize table state to send it to the server (with query parameters for example) and parse the response
        //in our case, it actually performs the logic which would happened in the server
        function getPage(start,number,params){
            console.log(params);


            var deferred = $q.defer();

            var filtered = params.search.predicateObject ? $filter('filter')(randomsItems,params.search.predicateObject) : randomsItems;

            if(params.sort.predicate){
                filtered = $filter('orderBy')(filtered,params.sort.predicate,params.sort.reverse);
            }

            var result = filtered.slice(start,start+number);
            $timeout(function(){
                //note, the server passes the information about the data set size
                deferred.resolve({
                    data:result,
                    numberOfPages:Math.ceil(filtered.length/number)
                });
            },1500);


            return deferred.promise;
        }

        return {
            getPage:getPage
        };

    }])
    .factory('projectService',function($http,$cacheFactory,$rootScope,transformRequestAsFormPost){

        return {
            projectContact:function($postData){
                return $http.post(API_URL+'crm/projectContact',$postData)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getProjectContact:function($postData){
                return $http.get(API_URL+'crm/projectContact',{params:$postData})
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            addProjectTeam:function($postData){
                return $http.post(API_URL+'crm/projectTeam',$postData)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            getProjectTeamContact:function(id){
                return $http.get(API_URL+'company/branchUsers/?user_id='+id)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            projectApprovals:function(params){
                return $http.get(API_URL+'activity/activity/?'+params)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            },
            ProjectMeeting:{
                /**
                 *
                 * @param $params
                 * company_id   user_id
                 * @returns {*}
                 */
                getMeetingGuest:function($params){
                    return $http.get(API_URL+'activity/meetingGuest',{params:$params})
                        .then(function(response){
                            return response.data;
                        });
                },
                post:function($params){
                    return $http.post(API_URL+'activity/meeting',$params)
                        .error(function(){
                        }).then(function(response){
                            return response.data;
                        });
                },
                get:function($params){
                    //if get by id @ id_meeting
                    return $http.get(API_URL+'activity/meeting',{params:$params})
                        .then(function(response){
                            return response.data;
                        });
                },
                delete:function($params){
                    return $http.delete(API_URL+'activity/meeting',{params:$params})
                        .then(function(response){
                            return response.data;
                        });
                }

            }
        }
    })

    .factory('productService',function($http,$cacheFactory,$rootScope,transformRequestAsFormPost){
        return {
            getProductData:function($postData){
                return $http.get(API_URL+'company/productTermItem/?product_term_key='+$postData)
                    .error(function(){
                    }).then(function(response){
                        return response.data;
                    });
            }
        }

    })
    .factory('activityService',function($http,$cacheFactory,$rootScope){
        return {
            discussion:{
                /*
                 *@params get discussion_type, disussion_reference_id
                 */
                get:function($params){
                    return $http.get(API_URL+'activity/discussion',{params:$params})
                        .then(function(response){
                            return response.data;
                        });
                },
                /**
                 * @params discussion_type, discussion_reference_id,created_by,discussion_description
                 */
                post:function($params){
                    return $http.post(API_URL+'activity/discussion',{params:$params})
                        .then(function(response){
                            return response.data;
                        });
                }
            }
        }
    })


    .filter('decode',function(){

        return function(text){
            return window.btoa(text);

        }
    })
    .filter('encode',function(){

        return function(text){
            if(text!=undefined){
                return window.atob(text);
            }
        }
    })
    .service('anchorSmoothScroll',function(){
        this.scrollTo = function(eID){

            // This scrolling function
            // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript

            var startY = currentYPosition();
            var stopY = elmYPosition(eID);
            var distance = stopY>startY ? stopY-startY : startY-stopY;
            if(distance<100){
                scrollTo(0,stopY);
                return;
            }
            var speed = Math.round(distance/100);
            if(speed>=20) speed = 20;
            var step = Math.round(distance/25);
            var leapY = stopY>startY ? startY+step : startY-step;
            var timer = 0;
            if(stopY>startY){
                for(var i = startY; i<stopY; i += step){
                    setTimeout("window.scrollTo(0, "+leapY+")",timer*speed);
                    leapY += step;
                    if(leapY>stopY) leapY = stopY;
                    timer++;
                }
                return;
            }
            for(var i = startY; i>stopY; i -= step){
                setTimeout("window.scrollTo(0, "+leapY+")",timer*speed);
                leapY -= step;
                if(leapY<stopY) leapY = stopY;
                timer++;
            }

            function currentYPosition(){
                // Firefox, Chrome, Opera, Safari
                if(self.pageYOffset) return self.pageYOffset;
                // Internet Explorer 6 - standards mode
                if(document.documentElement&&document.documentElement.scrollTop)
                    return document.documentElement.scrollTop;
                // Internet Explorer 6, 7 and 8
                if(document.body.scrollTop) return document.body.scrollTop;
                return 0;
            }

            function elmYPosition(eID){
                var elm = document.getElementById(eID);
                var y = elm.offsetTop-165;
                var node = elm;

                while(node.offsetParent&&node.offsetParent!=document.body){
                    node = node.offsetParent;
                    y += node.offsetTop;

                }
                return y;
            }

        };

    })



