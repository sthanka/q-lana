'use strict';
angular.module('app',[
        'ngAnimate',
        'ngCookies',
        'ui.router',
        'oc.lazyLoad',
        'ui.bootstrap',
        'smart-table',
        'ngFileUpload',
        'easypiechart',
        'ui.utils.masks',
        "kendo.directives",
        "ngSanitize"
    ])

    .run(['$location','$rootScope','Auth','$state','$timeout','$http',function($location,$rootScope,Auth,$state,$timeout,$http){

        $rootScope.$state = $state;
        $rootScope.kendoOptions = ["bold","italic","underline","justifyLeft","justifyCenter","insertUnorderedList","insertOrderedList"];
        $rootScope.removeErrorMessages = function(){
            $('.has-error').removeClass('has-error');
            $('.server-message').remove();
        }
        $rootScope.toast = function(title,message,type,allFields){
            $timeout(function(){
                toastr.options = {
                    closeButton:true,
                    progressBar:true,
                    showMethod:'fadeIn',
                    preventDuplicates:true,
                    timeOut:4000
                };
                switch(type){
                    case 'error':
                        var errorCount = 0;
                        /*if(allFields != undefined){
                         jQuery.each(allFields, function( index, value ) {
                         if (message.hasOwnProperty(index)) {
                         errorCount++;
                         jQuery('[name="'+index+'"]').addClass('has-error');//.css({"border": "1px solid red"})
                         // jQuery('[name="'+index+'"]').parent().append('<span class="error">'+message[index]+'</span>');
                         }else{
                         jQuery('[name="'+index+'"]').removeClass('has-error');//.css({"border": ".0625rem solid #ccc"})
                         //jQuery('[name="'+index+'"]').closest('.error').remove();
                         }
                         });
                         }else{*/
                        $('.has-error').removeClass('has-error');
                        $('.error-message').remove();
                        jQuery.each(message,function(index,value){
                            if(message.hasOwnProperty(index)){
                                errorCount++;
                                jQuery('[name="'+index+'"]').addClass('has-error');
                                jQuery('[name="'+index+'"]').parent().prepend('<div class="error-message server-message">'+message[index]+'</div>');
                            }
                        })
                        /* }*/
                        if(errorCount<2){
                            jQuery.each(message,function(index,value){
                                toastr.error(value,title);
                            });
                        } else{
                            toastr.error('Invalid Form Fields',title);
                        }

                        break;
                    case 'image-error':
                        toastr.error(message,title);
                        break;
                    case 'l-error':
                        toastr.error(message,title);
                        break;
                    case 'Success':
                        toastr.success(message,title);
                        break;
                    case 'warning':
                        toastr.warning(message,title);
                        break;
                    case 'warning_m':
                        toastr.options.positionClass = 'toast-top-center';
                        toastr.warning(message,title);
                        break;
                    default:
                        if(title=='Success'){
                            toastr.success(message,title);
                        } else if(title=='Error'){
                            toastr.error(message,title);
                        } else{
                            toastr.info(message,title);
                        }
                        break;
                }
            },200);
        }
        $rootScope.goToState = function(data,params){
            $state.go(data,params);
        }
        $rootScope.errorMessage = '';
        $rootScope.loader = false;

        $rootScope.loaderOverlay = true;

        Auth.checkLogin();

        $rootScope.$on('loggedIn',function(){
            //window.location.href = "#/my-profile";
            $rootScope.site_url = '#/login';
            if(Auth.isLoggedIn()){
                var temp = angular.fromJson(Auth.getFields());
                console.log(temp);
                $rootScope.userId = temp.id_user;
                $rootScope.access_token = temp.access_token;
                $rootScope.user_role_id = temp.user_role_id;
                $rootScope.user_name = temp.first_name+' '+temp.last_name;
                $rootScope.first_name = temp.first_name;
                $rootScope.email_id = temp.email_id;
                $rootScope.id_company = temp.id_company;
                $rootScope.company_logo = temp.company_logo;
                $rootScope.profile_image = temp.profile_image;
                $rootScope.company_name = temp.company_name;
                $rootScope.branch_name = temp.legal_name;
                $rootScope.role_name = temp.role_name;
                if($rootScope.access_token!=undefined){
                    $http.defaults.headers.common['Authorization'] = $rootScope.access_token;
                    $http.defaults.headers.common['User'] = $rootScope.userId;
                }

                if($rootScope.user_role_id=='1'){
                    $rootScope.site_url = '#/sa/index';
                } else if($rootScope.user_role_id=='2'){
                    $rootScope.site_url = '#/admin-setup';
                } else if($rootScope.user_role_id=='3'){
                    $rootScope.site_url = '#/contact/list';
                }
                $rootScope.currentDateTime = new Date();

            }
            // console.log($rootScope.id_company);

            $timeout(function(){
                // This code runs after the authentication promise has been rejected.
                // Go to the log-in page
                //$state.go("index");
            })

        });
        $rootScope.datePickerOptions = {
            formatYear:'yy',
            showWeeks:false,
            shortcutPropagation:true,
            showButtonbar:false
        };
        $rootScope.dataFormate = "yyyy-MM-dd"

        $rootScope.$on('loggedOut',function(){
            /*  $rootScope.topheader = false;
             $rootScope.sidebar = false;
             $rootScope.footer = false;*/
            $state.go("login");
        });

        $rootScope.$on('$stateChangeStart',function(event,toState,toParams,fromState,fromParams){
            $rootScope.pageLoaded = false;
        });
        $rootScope.back = function(){
            if($rootScope.previousState_name){
                $state.go($rootScope.previousState_name,$rootScope.previousState_params);
            }
        };
        $rootScope.$on('$stateChangeSuccess',function(ev,to,toParams,fromState,fromParams){
            // scroll view to top
            $rootScope.previousState_name = fromState.name;
            $rootScope.previousState_params = fromParams;
            $("html, body").animate({
                scrollTop:0
            },200);

            /*$timeout(function () {
             $rootScope.pageLoading = false;
             $($window).resize();
             }, 300);
             */
            $timeout(function(){
                $rootScope.pageLoaded = true;
            },600);

        });

    }])

    .config(function($httpProvider){
        // delete $httpProvider.defaults.headers.common['X-Requested-With'];
    })

    .config(['$stateProvider','$urlRouterProvider','$ocLazyLoadProvider','$rootScopeProvider',function myAppConfig($stateProvider,$urlRouterProvider,$ocLazyLoadProvider,$rootScopeProvider){

        $rootScopeProvider.digestTtl(500);
        $urlRouterProvider.otherwise('404');

        $ocLazyLoadProvider.config({
            // Set to true if you want to see what and when is dynamically loaded
            debug:true
        });

        $stateProvider
        //error pages
            .state('error',{
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('error.404',{
                url:'/404',
                templateUrl:'partials/error/404.html'
            })
            //common for all user
            .state('app',{
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate,
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                insertBefore:'#loadBefore',
                                name:'localytics.directives',
                                files:['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                            }
                        ]);
                    }
                }
            })
            .state('app.dashboard',{
                url:'/dashboard',
                resolve:{
                    authenticate:LoginAuthenticate
                }
            })
            .state('login',{
                url:'/login',
                controller:'LoginCtrl',
                templateUrl:'partials/login.html',
                resolve:{
                    authenticate:LoginAuthenticate
                }
            })
            .state('forgot-password',{
                url:'/forgot-password',
                controller:'ForgotPasswordCtrl',
                templateUrl:'partials/forgot-password.html'
            })
            .state('app.my-profile',{
                url:'/my-profile',
                controller:'MyProfileCtrl',
                templateUrl:'partials/my-profile.html',
                resolve:{
                    authenticate:authenticate
                }
            })

            //super admin - index
            .state('sa',{
                controller:'',
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate,
                    checkUrl: function(crmService) {
                        return crmService.checkUrlPermission(1);
                    },
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                name:'app',
                                files:['js/app/sa_controller.js']
                            }
                        ]);
                    }
                }
            })
            .state('sa.index',{
                url:'/sa/index',
                controller:'superAdminDashboardCtrl',
                templateUrl:'partials/super-admin/dashboard.html',
                resolve:{
                    authenticate:authenticate,
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                name:'googlechart',
                                files:['js/plugins/google-charts/ng-google-chart.js']
                            }
                        ]);
                    }
                }
            })

            //super admin -  Master data
            .state('sa.master',{
                template:' <div ui-view>  </div>'
            })

            .state('sa.master.sector',{
                url:'/sa/master/sector',
                controller:'sectorCtrl',
                templateUrl:'partials/super-admin/master-data/sector.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('sa.master.branch-type',{
                url:'/sa/master/branch-type',
                controller:'branchTypeCtrl',
                templateUrl:'partials/super-admin/master-data/branch-type.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('sa.master.approval-role',{
                url:'/sa/master/approval-role',
                controller:'ApprovalRoleCtrl',
                templateUrl:'partials/super-admin/master-data/approval-role.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('sa.master.plans',{
                url:'/sa/master/plans',
                controller:'plansCtrl',
                templateUrl:'partials/super-admin/master-data/plans.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('sa.master.bank-category',{
                url:'/sa/master/bank-category',
                controller:'bankCategoryCtrl',
                templateUrl:'partials/super-admin/master-data/bank-category.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('sa.master.country',{
                url:'/sa/master/country',
                controller:'countryCtrl',
                templateUrl:'partials/super-admin/master-data/country.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('sa.master.currency',{
                url:'/sa/master/currency',
                controller:'currencyCtrl',
                templateUrl:'partials/super-admin/master-data/currency.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('sa.master.risk',{
                url:'/sa/master/risk',
                controller:'riskCtrl',
                templateUrl:'partials/super-admin/master-data/risk.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('sa.master.social',{
                url:'/sa/master/social',
                controller:'socialCtrl',
                templateUrl:'partials/super-admin/master-data/social.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('sa.master.contact',{
                url:'/sa/master/contact',
                controller:'contactCtrl',
                templateUrl:'partials/super-admin/master-data/contact.html',
                resolve:{
                    authenticate:authenticate
                }
            })


            //super admin - create customer
            .state('sa.create-customer',{
                url:'/sa/create-customer/:customerId',
                defaultParams:{customerId:0},
                controller:'CreateCustomerCtrl',
                templateUrl:'partials/super-admin/create-customer.html',
                resolve:{
                    authenticate:authenticate
                }
            })

            .state('sa.customer-overview',{
                url:'/sa/customer-overview/:id',
                controller:'CustomerOverviewCtrl',
                templateUrl:'partials/super-admin/customer-overview.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('sa.customer-overview.customer-overview-tab',{
                url:'/sa/customer-overview-tab',
                //controller: 'CustomerOverviewCtrl',
                templateUrl:'partials/super-admin/customer-tabs/over-view.html'

            })
            .state('sa.customer-overview.customer-loans',{
                url:'/customer-loans',
                //controller: 'CustomerOverviewCtrl',
                templateUrl:'partials/super-admin/customer-tabs/loans.html'
            })
            .state('sa.customer-overview.customer-branches',{
                url:'/customer-branches',
                controller:'customerBranchesCtrl',
                templateUrl:'partials/super-admin/customer-tabs/branches.html'

            })
            .state('sa.customer-overview.customer-login-history',{
                url:'/customer-login-history',
                //controller: 'CustomerOverviewCtrl',
                templateUrl:'partials/super-admin/customer-tabs/login-history.html'
            })
            .state('sa.customer-overview.customer-users',{
                url:'/customer-users',
                controller:'customerUserCtrl',
                templateUrl:'partials/super-admin/customer-tabs/users.html'
            })


            //Admin
            .state('adm',{
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate,
                    checkUrl: function(crmService) {
                        return crmService.checkUrlPermission(2);
                    },
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                insertBefore:'#loadBefore',
                                name:'localytics.directives',
                                files:['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                            }
                        ]);
                    }
                }
            })
            .state('adm.admin-setup',{
                url:'/admin-setup',
                controller:'AdminSetupCtrl',
                templateUrl:'partials/admin/admin-setup.html',
                resolve:{
                    authenticate:authenticate
                }

            })

            //super admin -  Roles Mnangement
            .state('roles',{
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate,
                    checkUrl: function(crmService) {
                        return crmService.checkUrlPermission(2);
                    },
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                name:'roles.module',
                                files:['partials/admin/roles-management/roles-module.js']
                            },
                            {
                                insertBefore:'#loadBefore',
                                name:'localytics.directives',
                                files:['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                            },

                        ]);
                    }
                }
            })
            .state('roles.roles-management',{
                url:'/roles/roles-management',
                controller:'rolesList',
                templateUrl:'partials/admin/roles-management/roles-management.html'
            })
            .state('roles.role-create',{
                url:'/roles/role-create',
                templateUrl:'partials/admin/roles-management/roles-management-create.html'
            })
            .state('roles.role-edit',{
                url:'/roles/role-edit',
                templateUrl:'partials/admin/roles-management/roles-management-edit.html'
            })
            .state('roles.role-module-create',{
                url:'/roles/module/:roleId',
                controller:'moduleCreation',
                templateUrl:'partials/admin/roles-management/roles-module-permission.html'
            })


            .state('org',{
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate,
                    checkUrl: function(crmService) {
                        return crmService.checkUrlPermission(2);
                    },
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                insertBefore:'#loadBefore',
                                name:'localytics.directives',
                                files:['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                            }
                        ]);
                    }
                }
            })
            .state('org.org-structure',{
                url:'/org/organisational-structure',
                controller:'orgStructureCtrl',
                templateUrl:'partials/admin/org/org-structure.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('org.org-structure-view',{
                url:'/org/organisational-structure-view',
                controller:'orgStructureViewCtrl',
                templateUrl:'partials/admin/org/org-structure-view.html',
                resolve:{
                    authenticate:authenticate,
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                insertBefore:'#loadBefore',
                                files:['css/plugins/org-chart/jquery.orgchart.css','js/plugins/org-chart/jquery.orgchart.js']
                            }
                        ]);
                    }
                }
            })

            .state('entity',{
                controller:'',
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate,
                    checkUrl: function(crmService) {
                        return crmService.checkUrlPermission(2);
                    },
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                insertBefore:'#loadBefore',
                                name:'localytics.directives',
                                files:['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                            }
                        ]);
                    }
                }
            })
            .state('entity.branch-structure',{
                url:'/entity/branch-structure',
                controller:'BranchStructureCtrl',
                templateUrl:'partials/admin/entity-structure/branch-structure.html'
            })
            .state('entity.branch-structure-view',{
                url:'/entity/branch-structure-view',
                controller:'BranchStructureViewCtrl',
                templateUrl:'partials/admin/entity-structure/branch-structure-view.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('entity.create-branch',{
                url:'/entity/create-branch',
                controller:'createBranchCtrl',
                templateUrl:'partials/admin/entity-structure/create-branch.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('entity.edit-branch',{
                url:'/entity/edit-branch/:id',
                controller:'editBranchCtrl',
                templateUrl:'partials/admin/entity-structure/create-branch.html',
                resolve:{
                    authenticate:authenticate
                }
            })


            .state('approval',{
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate,
                    checkUrl: function(crmService) {
                        return crmService.checkUrlPermission(2);
                    }
                }
            })
            .state('approval.approval-structure',{
                url:'/approval/approval-structure',
                controller:'approvalStructureCtrl',
                templateUrl:'partials/admin/approval/approval-structure.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('approval.approval-structure-view',{
                url:'/approval/approval-structure-view/:structure_id',
                controller:'approvalStructureViewCtrl',
                templateUrl:'partials/admin/approval/approval-structure-view.html',
                resolve:{
                    authenticate:authenticate
                }
            })

            .state('app.user-information',{
                url:'/user-information',
                controller:'UserInformationCtrl',
                templateUrl:'partials/admin/user-information.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('app.create-user',{
                url:'/create-user',
                controller:'UserCreationCtrl',
                templateUrl:'partials/admin/org-user/create-user.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('app.edit-user',{
                url:'/edit-user/:id',
                controller:'EditCompanyUserCtrl',
                templateUrl:'partials/admin/org-user/create-user.html',
                resolve:{
                    authenticate:authenticate
                }
            })

            .state('product',{
                abstract:true,
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate,
                    checkUrl: function(crmService) {
                        return crmService.checkUrlPermission(2);
                    }
                }
            })
            .state('product.list',{
                url:'/product/list',
                controller:'productCtrl',
                templateUrl:'partials/admin/product/product.html'
            })
            .state('product.tabs',{
                url:'/product/tabs/:productId',
                templateUrl:'partials/admin/product/product-tabs.html',
                controller:function($rootScope,$scope,$state,$stateParams,encodeFilter,masterService){
                    $rootScope.product_id = encodeFilter($stateParams.productId);
                    $state.go('product.tabs.product-data');
                    masterService.getProductById($rootScope.product_id,$rootScope.id_company).then(function(result){
                        $scope.productName = result.data.data[0].product_name;
                    });
                    /*$scope.productdata = [];
                     $scope.getProductStateData = function (key,state) {
                     productService.getProductData(key).then(function (result){
                     $scope.productdata = result.data;
                     console.log($scope.productdata);
                     });
                     $state.go(state);
                     };*/
                },
                resolve:{
                    authenticate:authenticate,
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                serie:true,
                                name:'ui.sortable',
                                files:['js/plugins/jquery-ui/jquery-ui.js','js/plugins/ui-sortable/sortable.js']
                            },
                            {
                                serie:true,
                                name:'ngDragDrop',
                                files:['js/plugins/jquery-ui/jquery-ui.js','js/plugins/ui-dragdrop/angular-dragdrop.js']
                            },
                            {
                                insertBefore:'#loadBefore',
                                name:'localytics.directives',
                                files:['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                            }
                        ]);
                    }
                }
            })
            .state('product.tabs.product-data',{
                url:'/product-data',
                templateUrl:'partials/admin/product/product-data.html'
            })
            .state('product.tabs.product-conditions',{
                url:'/product-conditions',
                controller:'productConditionsCtrl',
                templateUrl:'partials/admin/product/product-conditions.html'
            })
            .state('product.tabs.risk-assessment',{
                url:'/risk-assessment',
                templateUrl:'partials/admin/product/product-risk-assessment.html'
            })
            .state('product.tabs.summary-recommendation',{
                url:'/summary-recommendation',
                controller:'productSummaryRecommendationsCtrl',
                templateUrl:'partials/admin/product/product-summary-recommendation.html'
            })
            .state('product.tabs.disbursement-checklist',{
                url:'/disbursement-checklist',
                controller:'disbursementChecklistCtrl',
                templateUrl:'partials/admin/product/product-disbursement-checklist.html',
                data:{
                    id_assessment_question_type:3,
                    title:"Pre Disbursement Checklist"
                }
            })
            .state('product.tabs.restrictions',{
                url:'/restrictions',
                controller:'disbursementChecklistCtrl',
                templateUrl:'partials/admin/product/product-restrictions.html',
                data:{
                    id_assessment_question_type:4,
                    title:"Pre Disbursement Checklist"
                }
            })
            .state('product.tabs.product-limits-approvals',{
                url:'/product/tabs/product-limits-approvals',
                controller:'productLimitsApprovalsCtrl',
                templateUrl:'partials/admin/product/product-limits-approvals.html'
            })


            .state('assessment',{
                controller:'',
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate,
                    checkUrl: function(crmService) {
                        return crmService.checkUrlPermission(2);
                    },
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                serie:true,
                                name:'ui.sortable',
                                files:['js/plugins/jquery-ui/jquery-ui.js','js/plugins/ui-sortable/sortable.js']
                            },
                            {
                                serie:true,
                                name:'ngDragDrop',
                                files:['js/plugins/jquery-ui/jquery-ui.js','js/plugins/ui-dragdrop/angular-dragdrop.js']
                            },
                            {
                                insertBefore:'#loadBefore',
                                name:'localytics.directives',
                                files:['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                            }
                        ])
                    }
                }
            })
            .state('assessment.assessment-questions',{
                url:'/assessment-questions',
                controller:'assessmentCtrl',
                templateUrl:'partials/admin/assessment/assessment-questions.html',
                data:{
                    id_assessment_question_type:1,
                    title:"Assessment Questions"
                },
                resolve:{
                    authenticate:authenticate

                }
            })
            .state('assessment.business-plan',{
                url:'/business-plan',
                controller:'assessmentCtrl',
                templateUrl:'partials/admin/assessment/assessment-questions.html',
                data:{
                    id_assessment_question_type:2,
                    title:"Business Plan"
                },
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('assessment.assessment-questions-view',{
                url:'/assessment-questions-view',
                //controller: 'assessmentCtrl',
                templateUrl:'partials/admin/assessment/assessment-questions-view.html',
                resolve:{
                    authenticate:authenticate
                }
            })

            .state('adm.risk-assessment',{
                url:'/risk-assessment',
                controller:'riskAssessmentCtrl',
                templateUrl:'partials/admin/risk-assessment/risk-assessment.html',
                resolve:{
                    authenticate:authenticate
                }
            })

            .state('operating-currency',{
                abstract:true,
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate,
                    checkUrl: function(crmService) {
                        return crmService.checkUrlPermission(2);
                    }
                }
            })
            .state('operating-currency.tab',{
                url:'/operating-currency/tab',
                templateUrl:'partials/admin/operating-currency/tab.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('operating-currency.tab.primary-currency',{
                url:'/primary-currency',
                controller:'primaryCurrencyCtrl',
                templateUrl:'partials/admin/operating-currency/primary-currency.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('operating-currency.tab.operating-currency',{
                url:'/operating-currency',
                controller:'operatingCurrencyCtrl',
                templateUrl:'partials/admin/operating-currency/operating-currency.html',
                resolve:{
                    authenticate:authenticate
                }
            })

            .state('operating-currency.tab.exchange-rate',{
                url:'/exchange-rate',
                controller:'exchangeRateCtrl',
                templateUrl:'partials/admin/operating-currency/exchange-rate.html',
                resolve:{
                    authenticate:authenticate
                }
            })


            .state('knowledge-management',{
                controller:'',
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate,
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                /*  name: 'ui.knob',*/
                                files:['js/plugins/highcharts/highcharts.js']
                            },
                            {
                                insertBefore:'#loadBefore',
                                name:'localytics.directives',
                                files:['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                            }
                        ])
                    }
                }
            })
            .state('knowledge-management.knowledge-dashboard',{
                url:'/knowledge-management/dashboard',
                controller:'knowledgeManagementCtrl',
                templateUrl:'partials/admin/knowledge-management/knowledge-management-list.html',
                resolve:{
                    authenticate:authenticate

                }
            })
            .state('knowledge-management.view',{
                url:'/knowledge-management/view',
                controller:'searchKnowledgeManagementCtrl',
                templateUrl:'partials/admin/knowledge-management/knowledge-management-all.html',
                resolve:{
                    authenticate:authenticate
                }
            })

            .state('adm.business-assessment',{
                url:'/business-assessment',
                controller:'businessAssessmentCtrl',
                templateUrl:'partials/admin/business-assessment/business-assessment.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('knowledge-management.item-view',{
                url:'/knowledge-management/item-view/:id',
                controller:'knowledgeManagementViewCtrl',
                templateUrl:'partials/admin/knowledge-management/knowledge-management-view.html',
                resolve:{
                    authenticate:authenticate
                }
            })

            .state('collateral',{
                abstract:true,
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate,
                    checkUrl: function(crmService) {
                        return crmService.checkUrlPermission(2);
                    },
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                insertBefore:'#loadBefore',
                                name:'localytics.directives',
                                files:['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                            }
                        ]);
                    }

                }
            })
            .state('collateral.collateral-dashboard',{
                url:'/collateral-dashboard',
                templateUrl:'partials/admin/collateral/collateral-dashboard.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('collateral.collateral-list',{
                url:'/collateral/list',
                templateUrl:'partials/admin/collateral/collateral-list.html',
                controller:'collateralListCtrl',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('collateral.add-collateral',{
                url:'/collateral/config',
                controller:'collateralConfigCtrl',
                templateUrl:'partials/admin/collateral/collateral-config.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('covenant',{
                abstract:true,
                templateUrl:'partials/layout.html',
                controller:'covenantCtrl',
                resolve:{
                    authenticate:authenticate,
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                insertBefore:'#loadBefore',
                                name:'localytics.directives',
                                files:['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                            },
                            {
                                serie:true,
                                name:'ngDragDrop',
                                files:['js/plugins/jquery-ui/jquery-ui.js','js/plugins/ui-dragdrop/angular-dragdrop.js']
                            },
                            {
                                name:'covenant',
                                files:['partials/component/covenant/covenant-directive.js']
                            }
                        ]);
                    }

                }
            })
            .state('covenant.positive',{
                url:'/covenant/positive',
                template:'<h3 class="fixed content-heading">Positive Covenant</h3><covenant key="positive_covenant"></covenant>',
            })
            .state('covenant.negative',{
                url:'/covenant/negative',
                template:'<h3 class="fixed content-heading">Negative Covenant</h3><covenant key="negative_covenant"></covenant>',
            })
            .state('covenant.representations',{
                url:'/covenant/representations',
                template:'<h3 class="fixed content-heading">Representations Warranties</h3><covenant key="representations_warranties"></covenant>',
            })
            .state('covenant.events',{
                url:'/covenant/events-of-defaults',
                template:'<h3 class="fixed content-heading">Events Of Defaults</h3><covenant key="events_of_defaults"></covenant>',
            })
            //User Role
            //User - CRM states
            //contact Module
            .state('contact',{
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate,
                    checkUrl: function(crmService) {
                        return crmService.checkUrlPermission(3);
                    },
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                serie:true,
                                name:'ngHandsontable',
                                files:['css/ng-handsontables/handsontable.full.css','js/plugins/ng-handsontable/handsontable.full.js','js/plugins/ng-handsontable/ngHandsontable.min.js']
                            }
                        ]);
                    }
                }
            })
            .state('contact.contact-group',{
                url:'/contact/contact-group',
                controller:'ContactGroupCtrl',
                templateUrl:'partials/crm/contact-group.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('contact.contact-dashboard',{
                url:'/contact/list',
                controller:'ContactDashboardCtrl',
                templateUrl:'partials/crm/contact/contact-dashboard.html',
                params:{module_key:'contact_list'},
                resolve:{
                    permission:function(crmService) {
                        return crmService.setPermission('contact_list');
                    }
                }
            })
            .state('contact.contact-form-list',{
                url:'/contact/view/:id',
                controller:'contactFormListCtrl',
                templateUrl:'partials/crm/contact/contact-form-list.html',
                resolve:{
                    authenticate:authenticate,
                    permission: function(crmService) {
                        return crmService.setPermission('contact_view');
                    },
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                /*  name: 'ui.knob',*/
                                files:['js/plugins/highcharts/highcharts.js']
                            },
                            {
                                name:'touch.points',
                                files:['partials/component/touch_points/touch-points-directive.js']
                            }

                        ])
                    },

                }
            })
            .state('contact.contact-form-edit',{
                url:'/contact/form-view/:id',
                controller:'contactFormEditCtrl',
                templateUrl:'partials/crm/contact/contact-form-edit.html',
            })
            .state('contact.contact-complete-view',{
                url:'/contact/contact-complete-view/:id',
                controller:'ContactCompleteViewCtrl',
                templateUrl:'partials/crm/contact/contact-complete-view.html',
                resolve:{
                    authenticate:authenticate,
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                /*  name: 'ui.knob',*/
                                files:['js/plugins/highcharts/highcharts.js']
                            }
                        ])
                    },
                    permission: function(crmService) {
                        return crmService.setPermission('contact_complete_view');
                    }
                }
            })
            .state('contact.contact-creation',{
                url:'/contact/contact-creation/:id',
                controller:'ContactCreationCtrl',
                templateUrl:'partials/crm/contact/contact-creation.html',
                resolve:{
                    authenticate:authenticate
                }
            })

            //company Module
            .state('company',{
                abstract:true,
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate,
                    checkUrl: function(crmService) {
                        return crmService.checkUrlPermission(3);
                    },
                }
            })
            .state('company.company-dashboard',{
                url:'/company/list',
                controller:'CompanyDashboardCtrl',
                templateUrl:'partials/crm/company/company-dashboard.html',
                resolve:{
                    authenticate:authenticate,
                    permission: function(crmService) {
                        return crmService.setPermission('company_list');
                    }
                }
            })
            .state('company.company-creation',{
                url:'/company/view/:id',
                controller:'CompanyCreationCtrl',
                templateUrl:'partials/crm/company/company-creation.html',
                resolve:{
                    authenticate:authenticate,
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                name:'touch.points',
                                files:['partials/component/touch_points/touch-points-directive.js']
                            }
                        ])
                    },
                    permission:function(crmService) {
                        return crmService.setPermission('company_view');
                    }
                }
            })
            .state('company.business-assessment',{
                url:'/company/complete-view/:id',
                controller:'CompanyPriviewCtrl',
                templateUrl:'partials/crm/company/business-assessment.html',
                resolve:{
                    authenticate:authenticate,
                    permission:function(crmService) {
                        return crmService.setPermission('company_complete_view');
                    }
                }
            })
            .state('company.business-keydata',{
                url:'/company/form-view/:id',
                controller:'CompanyEditCtrl',
                templateUrl:'partials/crm/company/business-keydata.html',
                resolve:{
                    authenticate:authenticate
                }
            })

            //Project Module
            .state('project',{
                abstract:true,
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate,
                    checkUrl: function(crmService) {
                        return crmService.checkUrlPermission(3);
                    },
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                serie:true,
                                name:'ngHandsontable',
                                files:['css/ng-handsontables/handsontable.full.css','js/plugins/ng-handsontable/handsontable.full.js','js/plugins/ng-handsontable/ngHandsontable.min.js']
                            },
                            {
                                serie:true,
                                name:'angularjs-dropdown-multiselect',
                                files:['css/multiple-select.css','js/plugins/multi-select/jquery.multiple.select.js','js/plugins/multi-select/angularjs-dropdown-multiselect.js']
                            },
                            {
                                serie:true,
                                name:'ngTagsInput',
                                files:['css/plugins/ng-tag-input/ng-tags-input.bootstrap.css','css/plugins/ng-tag-input/ng-tags-input.css','js/plugins/ng-tag-input/ng-tags-input.js']
                            },
                            {
                                insertBefore:'#loadBefore',
                                name:'localytics.directives',
                                files:['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                            },
                            {
                                name:'covenant',
                                files:['partials/component/covenant/covenant-directive.js']
                            }

                        ]);
                    }
                }
            })
            .state('project.project-dashboard',{
                url:'/project/project-dashboard',
                controller:'ProjectDashboardCtrl',
                templateUrl:'partials/crm/project/project-dashboard.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('project.project-creation',{
                url:'/project/project-creation/:id',
                controller:'ProjectCreationCtrl',
                templateUrl:'partials/crm/project/project-creation.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('project.project-keydata',{
                url:'/project/project-keydata/:id',
                controller:'ProjectEditCtrl',
                templateUrl:'partials/crm/project/project-keydata.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('project.project-list',{
                url:'/project/project-list/:id',
                controller:'ProjectListCtrl',
                templateUrl:'partials/crm/project/project-list.html',
                resolve:{
                    authenticate:authenticate,
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                name:'touch.points',
                                files:['partials/component/touch_points/touch-points-directive.js']
                            }
                        ])
                    },
                    projectdetails:function($stateParams,encodeFilter,$rootScope,crmProjectService,$state){
                        $rootScope.currentProjectViewId = encodeFilter($stateParams.id);
                        $rootScope.currentModuleId = 3;
                        return crmProjectService.projectDetails($rootScope.currentProjectViewId,$rootScope.currentModuleId,$rootScope.userId).then(function(result){
                            $rootScope.projectDetails = result.data.details[0];
                            if($rootScope.projectDetails.assigned_to==$rootScope.userId&&$rootScope.projectDetails.created_by!=$rootScope.userId){
                                $rootScope.approvalStatus = true;
                                $state.go('project.project-approval-level.status-flow',{'id':$stateParams.id});
                            }
                            return result;
                        });
                    }

                }
            })
            .state('project.project-overview',{
                url:'/project/project-overview/:id',
                controller:'projectPriviewCtrl',
                templateUrl:'partials/crm/project/project-overview.html'
            })
            .state('project.project-mapping',{
                url:'/project/project-mapping',
                controller:'ProjectMappingCtrl',
                templateUrl:'partials/crm/project/project-mapping.html',
                resolve:{
                    authenticate:authenticate
                }
            })
            .state('project.terms-conditions',{
                url:'/project/terms-conditions/:id',
                controller:'ProjectListCtrl',
                templateUrl:'partials/crm/project/pre-disbursement-checklist.html',
                resolve:{
                    authenticate:authenticate
                }

            })
            .state('project.project-approval-level',{
                url:'/project/project-approval-level/:id',
                controller:'ProjectApprovalCtrl',
                templateUrl:'partials/crm/project/project-approval-level.html',
                resolve:{
                    authenticate:authenticate
                }

            })
            .state('project.project-approval-level.status-flow',{
                url:'/status-flow',
                templateUrl:'partials/crm/project/project-approval-level/status-flow.html',
                resolve:{
                    authenticate:authenticate
                }

            })
            .state('project.project-approval-level.summary-recommendations',{
                url:'/summary-recommendations',
                controller:'ProjectApprovalSummaryCtrl',
                templateUrl:'partials/crm/project/project-approval-level/summary-recommendations.html',
                resolve:{
                    authenticate:authenticate
                }

            })
            .state('project.project-approval-level.facility-table',{
                url:'/facility-table',
                templateUrl:'partials/crm/project/facility_com/facilities-approval-tableview.html',
                resolve:{
                    authenticate:authenticate
                }

            })
            .state('project.summary-recommendations',{
                url:'/project/summary-recommendations/:id',
                controller:'ProjectApprovalCtrl',
                templateUrl:'partials/crm/project/summary-recommendations.html',
                resolve:{
                    authenticate:authenticate
                }

            })

            //collateral Module
            .state('crm-collateral',{
                abstract:true,
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate,
                    checkUrl: function(crmService) {
                        return crmService.checkUrlPermission(3);
                    },
                    loadPlugin:function($ocLazyLoad){
                        return $ocLazyLoad.load([
                            {
                                insertBefore:'#loadBefore',
                                name:'localytics.directives',
                                files:['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                            }

                        ]);
                    }

                }
            })
            .state('crm-collateral.collateral-dashboard',{
                url:'/collateral/collateral-dashboard',
                templateUrl:'partials/crm/collateral/collateral-dashboard.html',
                controller:'collateralDashboardCtrl',
                resolve:{
                    authenticate:authenticate
                }

            })
            .state('crm-collateral.form-list',{
                url:'/collateral/form-list/:collateralId',
                templateUrl:'partials/crm/collateral/form-list.html',
                controller:'collateralFormListCtrl',
                resolve:{
                    authenticate:authenticate
                }

            })

            .state('crm-collateral.collateral-details',{
                url:'/collateral/collateral-details/:collateralId',
                controller:'collateralDetailsCtrl',
                templateUrl:'partials/crm/collateral/forms/collateral-details.html',
                resolve:{
                    authenticate:authenticate
                }

            })

            .state('crm-collateral.collateral-type-form',{
                url:'/collateral/collateral-type-form/:collateralId/:collateralTypeId',
                templateUrl:'partials/crm/collateral/collateral-types.html',
                controller:'collateralTypeFormCtrl',
                resolve:{
                    authenticate:authenticate
                }

            })

            //deligence Module
            .state('deligence',{
                abstract:true,
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate

                }
            })
            .state('deligence.pre-due-deligence',{
                url:'/deligence/pre-due-deligence',
                templateUrl:'partials/crm/deligence/pre-due-deligence.html',
                resolve:{
                    authenticate:authenticate
                }

            })
            .state('deligence.deligence',{
                url:'/deligence/deligence',
                templateUrl:'partials/crm/deligence/deligence.html',
                resolve:{
                    authenticate:authenticate
                }

            })

            //loan Module
            .state('loan',{
                abstract:true,
                templateUrl:'partials/layout.html',
                resolve:{
                    authenticate:authenticate

                }
            })
            .state('loan.loan-monitoring',{
                url:'/loan/loan-monitoring',
                templateUrl:'partials/crm/loan/loan-monitoring.html',
                resolve:{
                    authenticate:authenticate
                }

            })
            .state('loan.payment-history',{
                url:'/loan/payment-history',
                templateUrl:'partials/crm/loan/payment-history.html',
                resolve:{
                    authenticate:authenticate
                }

            })

        function authenticate($q,Auth,$state,$timeout){
            if(Auth.checkLogin()){
                console.log('login');
                return $q.when()
            } else{
                console.log('logout');
                $timeout(function(){
                    //console.log('logout');
                    $state.go('login')
                })
                return $q.reject()
            }
        }
        function LoginAuthenticate($q,Auth,$state,$timeout,$rootScope){
            $rootScope.site_url = '#/';
            var defer = $q.defer();
            //console.log('siteUrl');
            if(Auth.isLoggedIn()){
                console.log('login');
                var temp = angular.fromJson(Auth.getFields());
                $rootScope.user_role_id = temp.user_role_id;
                $timeout(function(){
                    if($rootScope.user_role_id=='1'){
                        $rootScope.site_url = '#/sa/index';
                        $state.go('sa.index');
                        defer.reject();
                    } else if($rootScope.user_role_id=='2'){
                        $rootScope.site_url = '#/admin-setup';
                        $state.go('adm.admin-setup');
                        defer.reject();
                    } else if($rootScope.user_role_id=='3'){
                        $rootScope.site_url = '#/contact/list';
                        $state.go('contact.contact-dashboard');
                        defer.reject();
                    }
                })
                return defer.promise;
            }

        }

    }]);








            



