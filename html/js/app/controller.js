angular.module('app')

    .controller('AppCtrl',function($rootScope,$scope,Auth,onlineStatus){


        //window.location.href = "#/login";
        $scope.logout = function(){
            Auth.logout();
            //window.location.href = "#";
        }
        $scope.menuOpened = false;
        $scope.toggleMenu = function(element,event){
            $scope.menuOpened = !($scope.menuOpened);
            event.stopPropagation();
        };
        window.onclick = function(e){
            if($scope.menuOpened){
                $scope.menuOpened = false;

                $scope.$apply();
            }
        };
        $scope.onlineStatus = onlineStatus;
        $scope.$watch('onlineStatus.isOnline()',function(online){
            $scope.online_status_string = online ? 'online' : 'offline';
            console.log($scope.online_status_string);
            if(!online){
                $rootScope.toast('ERR_INTERNET_DISCONNECTED','Unable to connect to the Internet','warning_m');
            }

        });
    })

    .controller('LoginCtrl',function($rootScope,$scope,Auth,UserService,$state){
        $scope.user = {};

        $scope.init = function(){
            //if (Auth.isLoggedIn()) {
            //    $state.go("/");
            //    alert(0);
            //}else{
            //    alert(1);
            //}
        }
        $scope.init();
        $scope.user = {"email_id":'',"password":''};
        $scope.submitForm = function(){
            // check to make sure the form is completely valid
            if($scope.loginForm.$valid){
                UserService.login($scope.user).then(function(result){
                    //console.log(result);
                    if(result.status){
                        var userObj = result.data;

                        userObj.access_token = result.access_token;
                        Auth.login(userObj);
                        $state.go("app.dashboard");
                    } else{
                        //$scope.user = {};
                        //$rootScope.toast('Error', result.error, 'error',$scope.user);  //toast('Error', result.error, 'error');
                        $rootScope.toast('Error',result.error,'error',$scope.user);
                        // $rootScope.errorMessage = result.message;
                    }
                });
            }
        };
        $scope.data = {"emailId":''};
        $scope.send = function(){
            if($scope.forgotPasswordForm.$valid){
                UserService.forgotPassword($scope.data.emailId).then(function(result){
                    if(result.status){
                        $rootScope.toast('Success',result.message);
                        $state.go("login");
                    } else{
                        $rootScope.toast('Error',result.error,'error',{'emailId':''});
                    }
                })
            }
        }
    })

    .controller('ForgotPasswordCtrl',function($rootScope,$scope,UserService,$state){
        $scope.send = function(email){
            UserService.forgotPassword(email).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $state.go("app.login");
                } else{
                    $rootScope.toast('Error',result.error,'error',{'email_id':''});
                }
            })
        }

    })

    .controller('MyProfileCtrl',function($rootScope,$scope,$uibModal,UserService){
        //add new row in modal
        $scope.animationsEnabled = true;

        $scope.open = function(){
            var modalInstance = $uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/change-password.html',
                controller:function($rootScope,$scope,$uibModalInstance,UserService){
                    $scope.user = {'oldpassword':'','password':'','cpassword':'','user_id':$rootScope.userId};
                    $scope.save = function(user){
                        UserService.changePassword($scope.user).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                $uibModalInstance.close();
                            } else{
                                $rootScope.toast('Error',result.error,'error',$scope.user);
                            }
                        })
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }

            });

            modalInstance.result.then(function(selectedItem){
                //$scope.selected = selectedItem;
            },function(){
                // $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.getUserProfile = function(){
            //console.log($rootScope.userId);
            UserService.getUserProfile($rootScope.userId).then(function(result){
                if(result.status){
                    $scope.userDetails = result.data;
                } else{
                    $rootScope.toast(result.error);
                }
            })
        }
        $scope.getUserProfile();
        $scope.displayed = [];
        $scope.callServer = function callServer(tableState){
            //console.log('tableState',tableState);
            $scope.dtableState = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            var start = pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number||10;  // Number of entries showed per page.

            UserService.getLoginHistory($rootScope.userId,start,number,tableState).then(function(result){
                //console.log(result);
                $scope.displayed = result.data.data;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading = false;
            });
        };


        $scope.callServerNotification = function callServer(tableState){
            //console.log('tableState',tableState);
            $scope.dtableState = tableState;
            $scope.isLoading = true;
            var pagination = tableState.pagination;
            var start = pagination.start||0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number||10;  // Number of entries showed per page.

            UserService.getUserNotifications($rootScope.id_company,$rootScope.userId,start,number,tableState).then(function(result){
                //console.log(result);
                $scope.notification = result.data.data;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/number);//set the number of pages so the pagination can update
                $scope.isLoading = false;
            });
        };


    })

    .controller('ContactGroupCtrl',function($rootScope,$scope){

        $scope.GROUPS_LIST = [
            {"id":0,"Name":"House Loan","Members":"20 members","Status":null},
            {"id":1,"Name":"USA","Members":"45 members","Status":null},
            {"id":2,"Name":"Defaulters","Members":"54 members","Status":null},
            {"id":3,"Name":"New Request","Members":"34 members","Status":null},
            {"id":4,"Name":"Late Payees","Members":"10 members","Status":null},
            {"id":5,"Name":"United Kingdom","Members":"39 members","Status":null},
            {"id":6,"Name":"Perfect Payees","Members":"110 members","Status":null}
        ]

    })

    .controller('CompaniesListCtrl',function($rootScope,$scope){
        $scope.COMPANY_RESULTS_DATA = [
            {"id":0,"Name":"Foxfire Technologies India Ltd","Contact":"London","Status":true},
            {"id":0,"Name":"Inooga Solutions PVT LTD","Contact":"India","Status":true},
            {"id":0,"Name":"Lance Soft India Pvt Ltd","Contact":"India","Status":false},
            {"id":0,"Name":"Azri Solutions Pvt Ltd","Contact":"UK","Status":null},
            {"id":0,"Name":"Infodat Technologies","Contact":"US","Status":null},
            {"id":0,"Name":"Sipera Systems PVT LTD","Contact":"Australia","Status":null},
            {"id":0,"Name":"Deep Channel Solutions","Contact":"China","Status":null},
            {"id":0,"Name":"Larsen & Toubro Ltd","Contact":"India","Status":false},
            {"id":0,"Name":"Kernex Microsystems India Ltd","Contact":"London","Status":true},
            {"id":0,"Name":"Progress Software Pvt Ltd","Contact":"Australia","Status":false}
        ];
    })

    .controller('SectorAdminCtrl',function($rootScope,$scope,$uibModal,$log){
        $scope.animationsEnabled = true;

        $scope.open = function(size){

            var modalInstance = $uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/super-admin/create-sector.html',
                controller:'CreateSectorCtrl',
                size:size,
                resolve:{
                    items:function(){
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem){
                $scope.selected = selectedItem;
            },function(){
                $log.info('Modal dismissed at: '+new Date());
            });
        };

        $scope.toggleAnimation = function(){
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };

    })

    .controller('CreateAdminSetupCtrl',function($scope,$uibModalInstance,items){
        $scope.ok = function(){
            $uibModalInstance.close($scope.selected.item);
        };

        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('AdminSetupCtrl',['$scope','$rootScope','$uibModal','$log','companyService',function($scope,$rootScope,$uibModal,$log,companyService){
        $scope.animationsEnabled = true;

        companyService.getById($rootScope.id_company).then(function($result){
            //console.log($result.data);
            if($result.status){
                $scope.companyDetail = $result.data;
            }
        });


        $scope.open = function(size){

            var modalInstance = $uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/admin/create-admin-setup.html',
                controller:'CreateAdminSetupCtrl',
                size:size,
                resolve:{
                    items:function(){
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem){
                $scope.selected = selectedItem;
            },function(){
                $log.info('Modal dismissed at: '+new Date());
            });
        };

        $scope.toggleAnimation = function(){
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };
    }])

    .controller('UserInformationCtrl',function($rootScope,$scope,companyService,masterService,Upload,$state,$location,decodeFilter){
        $scope.compnayUsers = [];
        $rootScope.currentUrl = 'app.user-information';
        companyService.userList($rootScope.id_company).then(function(result){
            $scope.companyUsers = result.data.data;
        });
        $scope.userView = function(result){
            //console.log(result);
            $scope.user = result;
        };
        $scope.editCompanyUser = function(id){
            $state.go('app.edit-user',{'id':decodeFilter(id)});
        };
    })

    .controller('UserCreationCtrl',function($rootScope,$scope,companyService,masterService,Upload,$state,$location){
        $scope.title = 'Create New User';
        $scope.btn = 'Create';

        companyService.companyBranchType($rootScope.id_company).then(function(result){
            $scope.companyBranchType = result.data;
        });
        masterService.country.get().then(function(result){
            $scope.countries = result.data;
        });

        $scope.updateBranchType = function(branch_type_id){
            $scope.user.branch_id = '';
            $scope.user.user_role_id = '';
            companyService.companyApprovalRoles({'company_id':$rootScope.id_company,'branch_type_id':branch_type_id}).then(function(result){
                $scope.userRole = result.data;
            });
            companyService.companyBranchesList($rootScope.id_company,branch_type_id).then(function(result){
                $scope.company_branches = result.data.data;
            });
        }

        $scope.updateRole = function(user_role_id,branch_id){
            if(user_role_id!=""&&branch_id!=""){
                var params = {
                    company_id:$rootScope.id_company,
                    id_company_approval_role:user_role_id,
                    branch_id:branch_id
                };
                companyService.branchReportingToUsers(params).then(function(result){
                    $scope.companyUsers = result.data.data;
                });
            }
        }
        $scope.updateReportingTo = function(user_id){
            companyService.user(user_id).then(function(result){
                $scope.reportingUserDetails = result.data;
            });
        }
        $scope.uploadUserImage = function(file){
            if(file!=null&&file!=''){
                $scope.userLogoRemove();
                setTimeout(function(){
                    $scope.userImage = file;
                    $scope.trash = true;
                    $scope.$apply();
                },100)
            }
        };
        $scope.userLogoRemove = function(){
            $scope.userImage = '';
            $scope.trash = false;

        };

        $scope.user = {
            'first_name':'',
            'last_name':'',
            'phone_number':'',
            'email_id':'',
            'address':'',
            'user_role_id':'',
            'branch_id':'',
            'reporting_user_id':''
        };
        $scope.selectedRole = {};
        if($rootScope.roleId!=undefined){
            $scope.selectedRole.id_approval_role = $rootScope.roleId;
        }
        $scope.submitForm = function(){
            if($scope.userForm.$valid){
                $scope.user.company_id = $rootScope.id_company;
                delete $scope.user['user_status']
                Upload.upload({
                    url:API_URL+'company/user',
                    data:{
                        file:{'profile_image':$scope.userImage},
                        'user':$scope.user
                    }
                }).then(function(resp){
                    //console.log(resp.data);
                    if(resp.data.status){
                        $rootScope.toast('Success',resp.data.message);
                        $state.go("org.org-structure-view");
                    } else{
                        $rootScope.toast('Error',resp.data.error,'error',$scope.user);
                    }
                    //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                },function(resp){
                    $rootScope.toast('Error','','error',resp.error);
                    //console.log('Error status: ' + resp.status);
                },function(evt){
                    var progressPercentage = parseInt(100.0*evt.loaded/evt.total);
                    //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
            }
        }
        $scope.navigatePrevious = function(state){
            if(state!=undefined)
                $state.go(state);
            else
                $state.go('app.user-information');
        };
    })

    .controller('EditCompanyUserCtrl',function(encodeFilter,$rootScope,$scope,companyService,Upload,$state,$location,$q,masterService,$stateParams){
        $scope.compnayUsers = [];

        $scope.uploadUserImage = function(file){

            if(file!=null&&file!=''){
                $scope.userLogoRemove();
                setTimeout(function(){
                    $scope.userImage = file;
                    $scope.trash = true;
                    $scope.$apply();
                },100)
            }
        };
        $scope.userLogoRemove = function(){
            $scope.userImage = '';
            $scope.trash = false;
        };
        $scope.title = 'Edit User';
        $scope.btn = 'Update';
        $rootScope.company_user_id = encodeFilter($stateParams.id);
        if($rootScope.company_user_id){
            var promise1 = companyService.companyBranchType($rootScope.id_company).then(function(result){
                $scope.companyBranchType = result.data;
            });
            /*  var promise2 = companyService.userList($rootScope.id_company,{'current_user_id':$rootScope.company_user_id}).then(function(result){
             $scope.companyUsers = result.data.data;
             });*/
            var promise2 = masterService.country.get().then(function(result){

                $scope.countries = result.data;
            });

            $q.all([promise1,promise2]).then(function(data){
                $rootScope.loaderOverlay = false;
                companyService.user($rootScope.company_user_id).then(function(result){
                    var res = result.data;
                    $scope.workingBranch = res.branch_id;
                    $scope.userRoletype = res.user_role_id;
                    $scope.reportingUserId = res.reporting_user_id;
                    delete res.branch_id;
                    delete res.user_role_id;
                    delete res.reporting_user_id;
                    $scope.user = res;
                });
            })

            $scope.updateBranchType = function(branch_type_id,flag){
                if(!flag){
                    $scope.user.user_role_id = '';
                    $scope.user.branch_id = '';
                    $scope.user.reporting_user_id = '';

                }
                $rootScope.loaderOverlay = false;
                companyService.companyApprovalRoles({'company_id':$rootScope.id_company,'branch_type_id':branch_type_id}).then(function(result){
                    $scope.userRole = result.data;
                    if(flag){
                        setTimeout(function(){
                            $scope.user.user_role_id = $scope.userRoletype;
                            $scope.$apply();
                        },100);
                    }
                });
                $rootScope.loaderOverlay = false;
                companyService.companyBranchesList($rootScope.id_company,branch_type_id).then(function(result){
                    $scope.company_branches = result.data.data;
                    if(flag){
                        setTimeout(function(){
                            $scope.user.branch_id = $scope.workingBranch;
                            $scope.$apply();
                        },100);
                    }
                });

            }

            $scope.updateRole = function(user_role_id,branch_id,flag){
                $rootScope.loaderOverlay = false;
                if(user_role_id!=undefined&&branch_id!=undefined){
                    var params = {
                        company_id:$rootScope.id_company,
                        id_company_approval_role:user_role_id,
                        branch_id:branch_id,
                        current_user_id:$rootScope.company_user_id
                    };
                    companyService.branchReportingToUsers(params).then(function(result){
                        $scope.companyUsers = result.data.data;
                        if(flag){
                            setTimeout(function(){
                                $scope.user.reporting_user_id = $scope.reportingUserId;
                                $scope.$apply();
                            },100);
                        }
                    });
                }
            }

            $scope.updateReportingTo = function(user_id){
                $rootScope.loaderOverlay = false;
                if(user_id>0){
                    companyService.user(user_id).then(function(result){
                        $scope.reportingUserDetails = result.data;
                    });
                }

            }

        }
        else{
            $location.path('/user-information');
        }
        $scope.getCheckedValue = function(val){
            if(val=="1"){
                return true;
            } else{
                return false;
            }
        }
        $scope.user = {
            "email_id":'',
            "phone_number":'',
            "last_name":'',
            "first_name":'',
            "company_id":'',
            "user_status":''
        };
        $scope.submitForm = function(){

            if($scope.userForm.$valid){
                $scope.user.company_id = $rootScope.id_company;
                Upload.upload({
                    url:API_URL+'company/user',
                    data:{
                        file:{'profile_image':$scope.userImage},
                        'user':$scope.user
                    }
                }).then(function(resp){
                    //console.log(resp.data);
                    if(resp.data.status){
                        $rootScope.toast('Success',resp.data.message);
                        $state.go("org.org-structure-view");
                    } else{
                        $rootScope.toast('Error',resp.data.error,'error',$scope.user);
                    }
                    //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                },function(resp){
                    $rootScope.toast('Error',resp.error);
                    //console.log('Error status: ' + resp.status);
                },function(evt){
                    var progressPercentage = parseInt(100.0*evt.loaded/evt.total);
                    //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
            }
        }

        $scope.navigatePrevious = function(state){
            if(state!=undefined)
                $state.go(state);
            else
                $state.go('app.user-information');
        };
    })

    .controller('BranchStructureViewCtrl',function($rootScope,$scope,companyService,$state,$uibModal,decodeFilter,$timeout){
        //add new row in modal
        $scope.animationsEnabled = true;
        $scope.brachTreeShow = true;

        $scope.open = function($mode,$editId){
            var $item = {};
            if($mode=='edit'){
                $item.type = 'edit';
                $item.editId = $scope.editId;
            }
            var modalInstance = $uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/admin/upload-branch.html',
                controller:'UploadBranchCtrl',
                backdrop:'static',
                keyboard:false,
                resolve:{
                    items:function(){
                        return $item;
                    }
                }
            });
            modalInstance.result.then(function(){
                $scope.branchStructure();
            },function(){
            });

        };

        $scope.toggleAnimation = function(){
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };

        $scope.branchTypes = [];
        $scope.branchTree = [];
        $scope.branchTreeStructure = [];
        $scope.treeDepth = [];
        $scope.setColor = [];
        $scope.branchTreeStructureShow = true;
        $scope.branchStructure = function(){
            companyService.branchStructure($rootScope.id_company).then(function(result){
                $scope.branchTypes = result.data;
                angular.forEach($scope.branchTypes,function(value,key){
                    angular.forEach(value.branches,function(branch){
                        branch.branch_type_name = value.branch_type_name;
                        branch.level = key;
                        $scope.branchTree.push(branch);
                    });
                });

                $scope.branchTreeStructure = $scope.getNestedChildren($scope.branchTree,0);


                if(result.data.length==0){
                    $state.go("entity.branch-structure");
                }
            });
        };
        $scope.branchChange = function(data){
            $scope.brachTreeShow = false;
             $scope.branchTreeStructure = [];
            if(data==null||data==undefined){
                data = 0;
            }
            if(data!=0){
                var branches = $scope.branchTree;
                var currentBranch = [];
                for(var b in branches){
                    if(branches[b].id_branch==data){
                        currentBranch.push(branches[b]);
                        $scope.branchTreeStructure = currentBranch;
                        console.log($scope.branchTreeStructure);
                        break;
                    }
                }
                $timeout(function(){
                    $scope.brachTreeShow = true;
                },1000)
            }
            else{
                $timeout(function(){
                    $scope.brachTreeShow = true;
                },1000)
                $scope.branchTreeStructure = $scope.getNestedChildren($scope.branchTree,data);
            }

        }
        $scope.treeStructureCount = function(treeB,count){
            console.log('treeB',treeB);
            return Bcount;
        }

        $scope.getNestedChildren = function(arr,parent){
            var out = []

            for(var i in arr){
                if(arr[i].reporting_branch_id==parent){
                    var nodes = $scope.getNestedChildren(arr,arr[i].id_branch);

                    if(nodes.length){
                        arr[i].nodes = nodes;
                    }
                    out.push(arr[i])
                }
            }
            return out
        }


        $scope.branchStructure();
        $scope.createBranch = function(branchId,currentBranchId){
            $rootScope.branchId = branchId;
            $rootScope.currentBranchId = currentBranchId;
            $state.go("entity.create-branch");
        };
        $scope.editBranch = function(branchId){
            $rootScope.branchId = branchId;
            $state.go("entity.edit-branch",{id:decodeFilter($rootScope.branchId)});
        };
        $scope.branchViewModal = function(branch_id){
            var modalInstance = $uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/admin/entity-structure/branch-view-modal.html',
                backdrop:'static',
                keyboard:false,
                resolve:{
                    item:function(){
                        return companyService.branchView({
                            'company_id':$rootScope.id_company,
                            'branch_id':branch_id
                        }).then(function(result){
                            return result.data[0];
                        })
                    }
                },
                controller:function(item,$scope,$uibModalInstance){
                    console.log('item',item);
                    $scope.branch = item;
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });

        }
    })

    .controller('UploadBranchCtrl',function($rootScope,$scope,$uibModalInstance,companyService,Upload){
        //console.log('$rootScope',$rootScope);

        companyService.download().then(function(result){ //console.log(result);
            if(result.status){
                $scope.download_url = result.data;
                /*$rootScope.toast('Success', result.error);
                 $uibModalInstance.close();*/
            } else{
                /*$rootScope.toast('Error', result.error, 'error');*/
            }
        });

        $scope.uploadBranch = function(file){
            if(file!=null&&file!=''){
                $scope.branchExcel = file;
            } else{
                $rootScope.toast('Error','file format not supported file formats','image-error');
            }
        };
        $scope.submitForm = function(){
            Upload.upload({
                url:API_URL+'company/uploadBranch',
                data:{
                    excel:$scope.branchExcel,
                    company_id:$rootScope.id_company
                }
            }).then(function(result){
                if(result.data.status){
                    $rootScope.toast('Success',result.data.error);
                    $uibModalInstance.close();
                } else{
                    $rootScope.toast('Error',result.data.error,'l-error',$scope.branchExcel);
                }
            });
        };

        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('orgStructureViewCtrl',function($rootScope,$scope,companyService,$state,$uibModal,decodeFilter,$timeout){
        //add new row in modal
        $scope.activeParentIndex;
        $scope.animationsEnabled = true;
        $scope.roleTypes = [];
        $scope.orgRoleTree = [];
        $scope.setColor = [];
        $scope.orgTreeShow = true;
        $rootScope.currentUrl = 'org.org-structure-view';

        $scope.toggleAnimation = function(){
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };


        $scope.rolesStructure = function(){
            companyService.roleStructure($rootScope.id_company).then(function(result){
                //console.log('result.data',result);
                if(result.data.length==0){
                    $state.go("org.org-structure");
                }
                $scope.roleTypes = result.data;
                //console.log('$scope.roleTypes', $scope.roleTypes);
                angular.forEach($scope.roleTypes,function(value,key){
                    angular.forEach(value.users,function(user){
                        user.approval_name = value.approval_name;
                        user.id_approval_role = value.id_approval_role;
                        //branch.level = key;
                        $scope.orgRoleTree.push(user);
                    });
                });
                $scope.orgRoleTreeStructure = $scope.getNestedChildren($scope.orgRoleTree,0);
            });
        };
        $scope.getNestedChildren = function(arr,parent){
            var out = []

            for(var i in arr){
                if(arr[i].reporting_user_id==parent){
                    var nodes = $scope.getNestedChildren(arr,arr[i].user_id);

                    if(nodes.length){
                        arr[i].nodes = nodes;
                    }
                    out.push(arr[i])
                }
            }
            return out
        }


        $scope.roleChange = function(data){
            $scope.orgTreeShow = false;
            $scope.orgRoleTreeStructure = [];
            if(data==null||data==undefined){
                data = 0;
            }
            if(data!=0){
                var roles = $scope.orgRoleTree;
                console.log('roles',roles);
                var currentRole = [];
                for(var b in roles){
                    if(roles[b].user_id==data){
                        currentRole.push(roles[b]);
                        $scope.orgRoleTreeStructure = currentRole;

                        console.log($scope.orgRoleTreeStructure);
                        break;
                    }
                }
                $timeout(function(){
                    $scope.orgTreeShow = true;
                },1000)
            }
            else{
                $timeout(function(){
                    $scope.orgTreeShow = true;
                },1000)
                $scope.orgRoleTreeStructure = $scope.getNestedChildren($scope.orgRoleTree,data);
            }
        }
        $scope.createUser = function(roleId){

            $rootScope.roleId = roleId;
            $state.go("app.create-user");
        };
        $scope.editCompanyUser = function(id){

            $rootScope.company_user_id = id;
            $state.go('app.edit-user',{id:decodeFilter($rootScope.company_user_id)});
        };

        $scope.userViewModal = function(user_id){
            var modalInstance = $uibModal.open({
                animation:$scope.animationsEnabled,
                templateUrl:'partials/admin/org-user/user-information-modal.html',
                backdrop:'static',
                keyboard:false,
                resolve:{
                    item:function(){
                        return companyService.userView({
                            'company_id':$rootScope.id_company,
                            'user_id':user_id,
                            'user_type':'view'
                        }).then(function(result){
                            return result.data.data[0];
                        })
                    }
                },
                controller:function(item,$scope,$uibModalInstance){
                    console.log('item',item);
                    $scope.user = item;
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });

        }


        //upload file excel
        /*  $scope.open = function($mode,$editId){
         var $item = {};
         if($mode=='edit'){
         $item.type = 'edit';
         $item.editId = $scope.editId;
         }
         var modalInstance = $uibModal.open({
         animation:$scope.animationsEnabled,
         templateUrl:'partials/admin/upload-approval.html',
         cUploadApprovalCtrl',
         backdrop:'static',
         keyboard:false,
         resolve:{
         items:function(){
         return $item;
         }
         }
         });
         modalInstance.result.then(function(){
         $scope.rolesStructure();
         },function(){
         });

         };*/

        //Init
        $scope.rolesStructure();

    })

    .controller('UploadApprovalCtrl',function($rootScope,$scope,$uibModalInstance,Upload,companyService){
        //console.log($rootScope);
        $scope.uploadUsers = function(file){
            if(file!=null&&file!=''){
                $scope.userExcel = file;
            } else{
                $rootScope.toast('Error','file format not supported file formats','image-error');
            }
        };
        companyService.downloadUsers().then(function(result){ //console.log(result);
            if(result.status){
                $scope.download_users = result.data;
            } else{
            }
        });
        $scope.submitUsersForm = function(){
            Upload.upload({
                url:API_URL+'company/uploadUser',
                data:{
                    excel:$scope.userExcel,
                    id_company:$rootScope.id_company
                }
            }).then(function(result){
                if(result.data.status){
                    $rootScope.toast('Success',result.data.error);
                    $uibModalInstance.close();
                } else{
                    $rootScope.toast('Error',result.data.error,'l-error',$scope.userExcel);
                }
            });
        };
        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('createBranchCtrl',function($rootScope,$scope,$state,Upload,masterService,companyService){

        if($rootScope.branchId==undefined){
            $state.go("entity.branch-structure-view");
        }
        $scope.Countries = [];
        //$scope.title = 'Create New Branch';
        $scope.title = 'Create New ';
        $scope.btn = 'Create';
        masterService.country.get().then(function($result){
            $scope.Countries = $result.data;
        });
        /*companyService.branchList($rootScope.id_company).then(function (result) {
         $scope.company_branches = result.data;
         });*/
        //console.log($rootScope);
        $scope.getReportingBranch = function(branchTypeId){
            companyService.companyBranches($rootScope.id_company,branchTypeId).then(function(result){
                $scope.company_branches = result.data.data;
                $scope.branchTypeName = result.data.branch_type.branch_type_name;
                $scope.branch.reporting_branch_id = $rootScope.currentBranchId;
            });
        };
        $scope.branch = {};
        companyService.companyBranchType($rootScope.id_company,$rootScope.branchId).then(function(result){
            $scope.branchTypes = result.data;
            $scope.branch.branch_type_id = $rootScope.branchId;

        });
        $scope.uploadBranchImage = function(file){
            if(file!=null&&file!=''){
                $scope.branch.branch_logo = file;
                $scope.trash = true;
            }
        };
        $scope.brachLogoRemove = function(){
            $scope.branch.branch_logo = '';
            $scope.trash = false;
        };

        $scope.branch = {
            'legal_name':'',
            'country_id':'',
            'branch_state':'',
            'branch_city':'',
            'branch_address':'',
            'reporting_branch_id':'',
            'branch_code':'',
            'branch_zip_code':'',
            'branch_email':'',
            'branch_phone_number':''
        };
        $scope.branch.branch_type_id = $rootScope.branchId;
        $scope.branch.company_id = $rootScope.id_company;
        $scope.submitForm = function(){
            //console.log('dsafdsaf'+$scope.branch);
            delete $scope.branch.id_branch_type;
            delete $scope.branch.branch_type_name;
            delete $scope.branch.branch_type_code;
            delete $scope.branch.description;
            delete $scope.branch.created_date_time;
            Upload.upload({
                url:API_URL+'company/companyBranch',
                data:{
                    file:{'branch_logo':$scope.branch.branch_logo},
                    'branch':$scope.branch
                }
            }).then(function(result){
                //console.log(result);
                if(result.data.status){
                    $rootScope.toast('Success',result.data.message);
                    $state.go("entity.branch-structure-view");
                } else{
                    $rootScope.toast('Error',result.data.error,'error',$scope.branch);
                }
            });
        }
    })

    .controller('editBranchCtrl',function($rootScope,$scope,$state,Upload,masterService,companyService,$q,$stateParams,encodeFilter){
        $rootScope.branchId = encodeFilter($stateParams.id);
        if($rootScope.branchId==undefined){
            $state.go("entity.branch-structure-view");
        }
        $scope.Countries = [];
        //$scope.title = 'Edit Branch';
        $scope.title = 'Edit ';
        $scope.btn = 'Update';

        /*companyService.branchList($rootScope.id_company, $rootScope.branchId).then(function (result) {
         $scope.company_branches = result.data;
         });*/
        $scope.branch = {};

        var getReportingBranch = $scope.getReportingBranch = function(branchTypeId,flag){
            if(flag){
                $scope.branch.reporting_branch_id = '';
            }
            companyService.companyBranches($rootScope.id_company,branchTypeId,$rootScope.branchId).then(function(result){
                $scope.company_branches = result.data.data;
                //$scope.branch = result.data.branch_type;
                $scope.branchTypeName = result.data.branch_type.branch_type_name;
            });
        };
        //var companyBranchType =
        var countries = masterService.country.get().then(function($result){
            $scope.Countries = $result.data;
        });
        var branch = $q.all([getReportingBranch,countries]).then(function(){
            companyService.branch($rootScope.branchId).then(function(result){
                $scope.branch = result.data;
                $scope.branch.branch_type_id = result.data.branch_type_id;
                $rootScope.branch_type_id = result.data.branch_type_id;
            });
        })

        $q.all([branch]).then(function(){
            companyService.companyBranchType($rootScope.id_company,$rootScope.branch_type_id).then(function(result){
                $scope.branchTypes = result.data;
                // $rootScope.branch_type_id =  $scope.branch.branch_type_id;
                //$scope.branch.id_branch_type = $rootScope.branchId;
            });
        })


        $scope.uploadBranchImage = function(file){
            if(file!=null&&file!=''){
                $scope.branch.branch_logo = file;
                $scope.trash = true;
            }
        };
        $scope.brachLogoRemove = function(){
            $scope.branch.branch_logo = '';
            $scope.trash = false;
        };
        $scope.branch = {
            'legal_name':'',
            'country_id':'',
            'branch_state':'',
            'branch_city':'',
            'branch_address':'',
            'reporting_branch_id':'',
            'branch_code':'',
            'branch_zip_code':'',
            'branch_email':'',
            'branch_phone_number':''
        };
        $scope.submitForm = function(){
            Upload.upload({
                url:API_URL+'company/companyBranch',
                data:{
                    file:{'branch_logo':$scope.branch.branch_logo},
                    'branch':$scope.branch
                }
            }).then(function(result){
                if(result.data.status){
                    $rootScope.toast('Success',result.data.message);
                    $state.go("entity.branch-structure-view");
                } else{
                    $rootScope.toast('Error',result.data.error,'error',$scope.branch);
                }
            });
        }
    })

    .controller('CreateProjectCtrl',function($rootScope,$scope){
        $scope.demo4 = {
            rangeMin:10,
            rangeMax:1500,
            min:80,
            max:1000,
            disabled:false
        };

    })

    .controller('BranchStructureCtrl',function($rootScope,$scope,masterService,companyService,$state,$uibModal){

        $scope.branchTree = [];
        $rootScope.branchList = [];
        $rootScope.branchListAll = [];
        $scope.addNewBtn = false;
        $scope.setColor = [];

        $rootScope.getBranchTypeStructure = function(){
            $scope.setColor = [];
            companyService.getBranchTypeStructure($rootScope.id_company).then(function($result){
                if($result.status){
                    $rootScope.branchData = $result.data;
                    console.log('$scope.branchData',$scope.branchData);
                    if($rootScope.branchData.length>0){
                        $scope.branchTree = $scope.getNestedChildren($result.data,0);
                    }
                }
            });
        }

        $scope.editBranchType = function(data){
            $scope.branchTypeModal(data);
        }

        $scope.createBranchType = function(){
            $scope.branchTypeModal();
        }

        $rootScope.branchListUpdate = function(){

            angular.forEach($rootScope.branchData,function(value,key){
                var a = _.findWhere($rootScope.branchList,{'id_branch_type':value.id_branch_type});//searching Element in Array
                var b = _.indexOf($rootScope.branchList,a);// getting index.
                if(b!= -1){
                    $rootScope.branchList.splice(b,1);
                }// removing.
            })

            /*$scope.treeStructure = function (tree) {
             angular.forEach(tree, function (value, key) {
             var a = _.findWhere($rootScope.branchList, {'id_branch_type': value.id_branch_type});//searching Element in Array
             var b = _.indexOf($rootScope.branchList, a);// getting index.
             if(b == 0) {
             $rootScope.branchList.splice(b, 1);
             }// removing.
             console.log('value', value);
             $scope.treeStructure(value.nodes, value.id_branch_type);
             }, tree);
             }
             $scope.treeStructure($scope.branchTree);*/

        }

        $scope.getNestedChildren = function(arr,parent){
            var out = []
            for(var i in arr){
                if(arr[i].reporting_branch_type_id==parent){
                    var nodes = $scope.getNestedChildren(arr,arr[i].id_branch_type)
                    if(nodes.length){
                        arr[i].nodes = nodes
                    }
                    out.push(arr[i])
                }
            }
            return out
        }

        $rootScope.getBranchTypeStructure();

        //branchTypeModal
        $scope.branchTypeModal = function(item){
            var modalInstance = $uibModal.open({
                animation:true,
                //windowClass: 'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'branch_structure_modal.html',
                resolve:{
                    item:item
                },
                controller:function($scope,$uibModalInstance,masterService,item,companyService){
                    $scope.branch = {};
                    $scope.submitBtn = false;
                    $scope.branch.is_primary = false;
                    companyService.branchType.get({'company_id':$rootScope.id_company}).then(function($result){

                        $rootScope.branchList = $result.data;
                        angular.copy($rootScope.branchList,$rootScope.branchListAll);

                        $scope.editMode = false;

                        if(item!=undefined){
                            console.log(item);
                            // $scope.branch = item;
                            $scope.branch.id_branch_type = item.branch_type_id;
                            $scope.branch.reporting_branch_type_id = item.reporting_branch_type_id;
                            $scope.branch.branch_type_name = item.branch_type_name;
                            $scope.branch.branch_type_code = item.branch_type_code;
                            $scope.branch.description = item.description;
                            $scope.branch.is_edit = item.is_edit;
                            if(item.is_edit==1){
                                $scope.editMode = true;
                            }
                            if(item.reporting_branch_type_id==0){
                                $scope.branch.is_primary = true;
                            }

                        } else{
                            $rootScope.branchListUpdate();

                        }
                        // buildSelectOption($result.data);
                    })
                    $scope.submitForm = function(branch){
                        $scope.submitBtn = true;
                        branch.company_id = $rootScope.id_company;
                        companyService.branchType.post(branch).then(function(result){
                            $scope.submitBtn = false;
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                $rootScope.getBranchTypeStructure();
                                $scope.cancel();
                            } else{
                                $rootScope.toast('Error',result.error,'error');
                            }
                        })
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function(){
            },function(){

            });
        };


    })

    .controller('orgStructureCtrl',function($rootScope,$scope,masterService,companyService,$state,$uibModal){
        $rootScope.roleList = [];
        $rootScope.roleListAll = [];
        $scope.approvalTree = [];
        $scope.addNewBtn = false;
        $scope.setColor = [];


        $rootScope.getApprovalStructure = function(){
            $scope.setColor = [];
            companyService.getApprovalTypeStructure($rootScope.id_company).then(function($result){
                if($result.status){
                    $rootScope.approvalList = $result.data;
                    if($rootScope.approvalList.length>0){
                        $scope.approvalTree = $scope.getNestedChildren($rootScope.approvalList,null);
                    } else{
                        $scope.addNewBtn = true;
                    }
                }
            })
        }
        $scope.getNestedChildren = function(arr,parent){
            var out = []
            for(var i in arr){
                if(arr[i].reporting_id==parent){
                    var nodes = $scope.getNestedChildren(arr,arr[i].id_approval_role)
                    if(nodes.length){
                        arr[i].nodes = nodes
                    }
                    out.push(arr[i])
                }
            }
            return out
        }
        $rootScope.getApprovalStructure();
        $rootScope.roleListUpdate = function(){
            //console.log(data);
            //console.log($scope.roleList);

            angular.forEach($rootScope.approvalList,function(data){
                var a = _.findWhere($rootScope.roleList,{'id_approval_role':data.id_approval_role});//searching Element in Array
                var b = _.indexOf($rootScope.roleList,a);// getting index.
                if(b!= -1){
                    $rootScope.roleList.splice(b,1);// removing.
                }
            })

        }


        $scope.modalOpen = function(sData){
            var modalInstance = $uibModal.open({
                animation:true,
                backdrop:'static',
                keyboard:false,
                //windowClass: 'my-class',
                openedClass:'right-panel-modal modal-open',
                templateUrl:'approvalStructureForm.html',
                resolve:{
                    item:function(){
                        return sData;
                    }
                },
                controller:function($rootScope,$scope,$uibModalInstance,companyService,item,$q,masterService){

                    $scope.sData = {};
                    $scope.sData.is_primary = false;
                    $scope.editMode = false;

                    $scope.approvalRole = masterService.approvalRole.getPage().then(function($result){
                        $rootScope.roleList = $result.data.data;
                        angular.copy($rootScope.roleList,$rootScope.roleListAll);
                    });
                    $scope.branchType = companyService.getBranchTypeStructure($rootScope.id_company).then(function($result){
                        $rootScope.branchList = $result.data;
                    });

                    $q.all([$scope.approvalRole,$scope.branchType]).then(function(){

                        if(item!=undefined){
                            console.log(item);
                            $scope.sData.id_approval_role = item.id_approval_role;
                            $scope.sData.id_branch_type = item.id_branch_type;
                            $scope.sData.id_company_approval_role = item.id_company_approval_role;
                            $scope.sData.reporting_approval_name = item.reporting_approval_name;
                            $scope.sData.approval_name = item.approval_name;
                            $scope.sData.description = item.description;
                            $scope.sData.reporting_id = item.reporting_id;
                            $scope.sData.is_edit = item.is_edit;
                            if(item.is_edit==1){
                                $scope.editMode = true;
                            }
                            if(item.reporting_id==0||item.reporting_id==null){
                                $scope.sData.is_primary = true;
                            }
                        } else{
                            $rootScope.roleListUpdate();
                            if($scope.approvalList.length>0&&$scope.approvalList[$scope.approvalList.length-1].id_approval_role!=undefined){
                                $scope.sData.reporting_approval_name = $scope.approvalList[$scope.approvalList.length-1].approval_name;
                            }
                        }

                    });


                    $scope.addApproval = function($data){

                        var postData = {};
                        postData.id_approval_role = $data.id_approval_role;
                        postData.branch_type_id = $data.id_branch_type;
                        postData.is_primary = $data.is_primary;
                        postData.company_id = $rootScope.id_company;
                        postData.approval_name = $data.approval_name;
                        postData.description = $data.description;
                        postData.reporting_role_id = $data.reporting_id;

                        /*if(item != undefined) {
                         postData.id_company_approval_role = item.id_company_approval_role;
                         }

                         if($scope.approvalList[$scope.approvalList.length - 1].id_approval_role != undefined) {
                         postData.reporting_role_id = $scope.approvalList[$scope.approvalList.length - 1].id_approval_role;
                         } else {
                         postData.reporting_role_id = 0;
                         }*/

                        companyService.approvalTypeStructure(postData).then(function($result){
                            //console.log($result);
                            $rootScope.getApprovalStructure();
                            if($result.status){
                                $rootScope.toast('Success',$result.message);
                                $uibModalInstance.dismiss('cancel');
                            } else{
                                $rootScope.toast('Error',$result.error,'error');
                            }
                        });
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }

            });

            modalInstance.result.then(function(selectedItem){
                //$scope.selected = selectedItem;
            },function(){
                // $log.info('Modal dismissed at: ' + new Date());
            });
        };


        $scope.companyId = $rootScope.id_company;

    })

    .controller('AdminManageDashboardCtrl',function($rootScope,$scope){

    })
    /*.controller('productListCtrl', function($rootScope, $scope, productService) {
     $scope.productdata = [];
     $scope.getProductData = function (key){
     alert(key);
     productService.getProductData(key).then(function (result){
     $scope.productdata = result;
     });
     };
     })*/
    .controller('productCtrl',function($rootScope,$scope,masterService,companyService){
        $scope.productList = [];
        /*masterService.getProductType({'company_id':$rootScope.id_company}).then(function ($result) {
         //console.log($result); $rootScope.id_company
         $scope.productList = $result.data;
         });*/
        companyService.approvalStructure.get({'company_id':$rootScope.id_company}).then(function(response){
            $scope.approvalList = response.data.data;
        });
        $scope.displayed = false;
        $scope.callServer = function(tableState){
            masterService.getProductType($rootScope.id_company,tableState).then(function(result){
                $scope.displayed = true;
                $scope.tableStateRef = tableState;
                $scope.productList = result.data.data;
                $scope.totalCount = result.data.total_records;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/tableState.pagination.number);
            });
        };
        $scope.submitQuickProduct = function($inputData,form){
            if(form.$valid){
                $inputData.company_id = $rootScope.id_company;
                $inputData.created_by = $rootScope.userId;
                masterService.addProductType($inputData).then(function(result){
                    if(result.status){
                        $rootScope.toast('Success',result.message);
                        $scope.product = {};
                        $scope.submitted = false;
                        $scope.callServer($scope.tableStateRef);
                    } else{
                        $rootScope.toast('Error',result.error,'error',result.error);
                    }
                })
            }
        };
    })

    .controller('productConditionsCtrl',function(productService,$scope){
        $scope.getProductStateData = function(){
            productService.getProductData('terms_and_conditions').then(function(result){
                $scope.productdata = result.data;
                console.log($scope.productdata);
            });
        };
        $scope.getProductStateData();
    })

    .controller('productSummaryRecommendationsCtrl',function(productService,$scope){
        $scope.getProductStateData = function(){
            productService.getProductData('summary_and_recommendations').then(function(result){
                $scope.productdata = result.data;
                console.log($scope.productdata);
            });
        };
        $scope.getProductStateData();
    })

    .controller('productLimitsApprovalsCtrl',function($rootScope,$scope,masterService,companyService,$q){
        $scope.productLimitsApprovals = [];

        var service1 = companyService.approvalStructure.get({'company_id':$rootScope.id_company}).then(function(response){
            $scope.approvalList = response.data.data;
        });
        var allSectors = [];
        var service2 = masterService.sector.getLimitSectorsAll().then(function(result){
            $scope.sectorsList = result.data.data;
        });
        $scope.getApprovalLimitType = function(){
            masterService.getApprovalLimitType($rootScope.id_company,$rootScope.product_id).then(function($result){
                $scope.productLimitsApprovals = $result.data;
                $scope.product = $result.data.approval_structure;
                $scope.existingSectors = $result.data.sectors;
                masterService.sector.getLimitSectorsAll().then(function(result){
                    $scope.sectorsList = result.data.data;
                    $scope.removeExistSector($scope.existingSectors,$scope.sectorsList);
                });
            })
        };
        $scope.removeExistSector = function(existingSectors,sectorsList){

            angular.forEach(existingSectors,function(object,index1){
                angular.forEach(sectorsList,function(value,index){
                    if(value.id_sector==object.id_sector){
                        $scope.sectorsList.splice(index,1);
                    }
                })
            })
        }
        $q.all([service1,service2]).then(function(){
            $scope.getApprovalLimitType();
        });

        $scope.addSector = function(sector){
            console.log(sector);
            if(sector==undefined){
                return false;
            }
            var details = {
                company_id:$rootScope.id_company,
                product_id:$rootScope.product_id,
                created_by:$rootScope.userId,
                'id_company_approval_structure':$scope.product.id_company_approval_structure,
                'sector_id':sector.id_sector
            }
            masterService.addSectorToLimits(details).then(function($result){
                $scope.getApprovalLimitType();
            })
            setTimeout(function(){
                var index = $scope.sectorsList.indexOf(sector);
                $scope.sectorsList.splice(index,1);
                $scope.sectorValue = '';
                $scope.addMore = !$scope.addMore;
                $scope.$apply();
                $scope.updateWidth();
            },1000)
        }

        $scope.updateSector = function(allSectorDetails,sectorId){
            var validate = $scope.validateData(sectorId);
            console.log('validate',validate);
            if(validate){
                var details = {
                    data:allSectorDetails,
                    details:{
                        id_company:$rootScope.id_company,
                        product_id:$rootScope.product_id,
                        created_by:$rootScope.userId,
                        'updatedSectorId':sectorId
                    }
                }
                masterService.postApprovalLimits(details).then(function($result){
                    $rootScope.toast('Success',$result.message);
                    $scope.getApprovalLimitType();
                })
            } else{
                alert('Lower committee approval limit should not greater than higher committee');
            }
        }
        $scope.validateData = function(sectorId){
            var valid = true;
            var previousValue = '';
            var setFirstVal = 0;
            angular.forEach($scope.productLimitsApprovals.data,function(value,peKey){
                angular.forEach($scope.productLimitsApprovals.data[peKey].committee,function(value,chKey){
                    angular.forEach($scope.productLimitsApprovals.data[peKey].committee[chKey].limit,function(value,key){
                        if($scope.productLimitsApprovals.data[peKey].committee[chKey].limit[key].id_sector==sectorId){
                            if(setFirstVal==0){
                                setFirstVal++;
                                previousValue = $scope.productLimitsApprovals.data[peKey].committee[chKey].limit[key].amount;
                            }
                            if(parseInt($scope.productLimitsApprovals.data[peKey].committee[chKey].limit[key].amount)>parseInt(previousValue)){
                                $scope.productLimitsApprovals.data[peKey].committee[chKey].limit[key].invalid = true;
                                valid = false;
                            } else{
                                $scope.productLimitsApprovals.data[peKey].committee[chKey].limit[key].invalid = false;
                            }
                            previousValue = $scope.productLimitsApprovals.data[peKey].committee[chKey].limit[key].amount;
                        }
                    });
                });
            });
            return valid;
        }
        $scope.deleteSector = function(sectorId){
            var r = confirm("Do you want to remove this sector?");
            if(r==true){
                var details = {
                    company_id:$rootScope.id_company,
                    product_id:$rootScope.product_id,
                    'sector_id':sectorId
                }
                masterService.deleteApprovalLimitSector(details).then(function($result){
                    $rootScope.toast('Success',$result.message);
                    $scope.getApprovalLimitType();
                })
            }
        }
        $scope.updateWidth = function(){
            var th_length = $('.table-scroll').find('th').length;
            if(th_length==1){
                //$(".table-scroll").css('width',$('.table-scroll').find('th').width()*2);
            }
            else{
                // $(".table-scroll").css('width',th_length*$('.table-scroll').find('th').width());
            }
            $(".table-scroll").css('max-width',$(window).width()-280);
            $(".table-scroll").mCustomScrollbar("update");
            $(".table-scroll").mCustomScrollbar("scrollTo","last",{
                scrollInertia:200,
                scrollEasing:"easeInOutQuad"
            });
        }

        $scope.updateApprovalStructure = function(ApprovalStructureId){
            masterService.updateApprovalStructure({
                company_id:$rootScope.id_company,
                id_product:$rootScope.product_id,
                'company_approval_structure_id':ApprovalStructureId
            }).then(function($result){
                $rootScope.toast('Success',$result.message);
                $scope.getApprovalLimitType();
            })
        }

    })

    .controller('approvalStructureCtrl',function($rootScope,$scope,$stateParams,$state,$uibModal,companyService){
        $scope.approvalList = {};
        $rootScope.getApprovalList = function(){
            companyService.approvalStructure.get({'company_id':$rootScope.id_company}).then(function(response){
                $scope.approvalList = response.data.data;
                //console.log(response.data.data);
            })
        };


        $scope.modalOpen = function(selectedItem){
            var modalInstance = $uibModal.open({
                animation:true,
                backdrop:'static',
                keyboard:false,
                //windowClass: 'my-class',
                openedClass:'right-panel-modal modal-open',
                templateUrl:'addApproval.html',
                resolve:{
                    item:selectedItem
                },
                controller:function($rootScope,$scope,$uibModalInstance,companyService,item){
                    $scope.approvalData = {};
                    $scope.btnDisabled = false;
                    if(item!=undefined){
                        $scope.approvalData = item;
                    }
                    $scope.saveApproval = function(approvalData){
                        $scope.btnDisabled = true;
                        var postParams = {};
                        postParams.company_id = $rootScope.id_company;
                        postParams.approval_structure_name = approvalData.approval_structure_name;
                        if(item!=undefined){
                            postParams.id_company_approval_structure = item.id_company_approval_structure;
                        }
                        companyService.approvalStructure.post(postParams).then(function(response){
                            if(response.status){
                                $scope.approvalData = {};
                                $rootScope.toast('Success',response.message,'Success');
                                $rootScope.getApprovalList();
                                $scope.cancel();
                            } else{
                                $scope.btnDisabled = false;
                                $rootScope.toast('Error',response.error,'error');
                            }
                        });
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }

            });

            modalInstance.result.then(function(selectedItem){

            },function(){

            });
        };


    })

    .controller('approvalStructureViewCtrl',function($rootScope,$scope,$stateParams,$state,$uibModal,companyService,encodeFilter){
        //console.log($stateParams.structure_id);
        $scope.setColor = [];
        $rootScope.current_structure_id = encodeFilter($stateParams.structure_id);
        $rootScope.getApprovalCommitees = function(){
            companyService.approvalCommittees.get({
                company_id:$rootScope.id_company,
                id_company_approval_structure:$rootScope.current_structure_id
            }).then(function(res){
                $scope.approvalCommittees = res.data.data;
                $rootScope.approval_structure_name = res.data.approval_structure_name;
            });
        }
        $scope.getApprovalCommitees();
        //Add Edit modal
        $scope.approvalStructureModal = function(branch_type_id,id_company_approval_credit_committee){
            var item = {};
            item.branch_type_id = branch_type_id;
            item.id_company_approval_credit_committee = id_company_approval_credit_committee;
            var modalInstance2 = $uibModal.open({
                animation:true,
                //windowClass: 'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/admin/approval/approval-structure-modal.html',
                controller:'approvalStructureViewModalCtrl',
                resolve:{
                    item:item
                }

            });
            modalInstance2.result.then(function(){
                $scope.getApprovalCommitees();
            },function(){
            });
        };
    })

    .controller('approvalStructureViewModalCtrl',function($rootScope,$scope,$uibModalInstance,companyService,item,$q){
        //scope
        $scope.committeeObj = {};
        $scope.committeeObj.committee = {};
        $scope.committeeObj.committee.company_id = $rootScope.id_company;
        $scope.committeeObj.committee.company_approval_structure_id = $rootScope.current_structure_id;//params
        $scope.committeeObj.committee.branch_type_id = item.branch_type_id;//params
        $scope.committeeObj.committee.is_primary_committee = false;
        $scope.committeeObj.committee_members = [];
        $scope.committeeObj.delete_members = [];
        $scope.member = {};
        $scope.submitBtn = false;


        $scope.braTypeRoleListFun = function(){
            companyService.branchTypeRole({
                'company_id':$rootScope.id_company,
                'id_branch_type_role':1
            }).then(function(response){
                $scope.branchTypeRoleList = response.data;
                // console.log(response.data);
            })
        }
        $scope.companyApprovalRoleFun = function(){
            companyService.companyApprovalRole({
                'company_id':$rootScope.id_company,
                'branch_type_id':item.branch_type_id
            }).then(function(response){
                $scope.companyApprovalRoleList = response.data;
                //console.log(response.data);
            })
        }

        $scope.approvalForwardFun = function(){
            var obj = {};
            obj.company_id = $rootScope.id_company;
            obj.branch_type_id = item.branch_type_id;
            obj.id_company_approval_structure = $rootScope.current_structure_id;
            if(item.id_company_approval_credit_committee){
                obj.id_company_approval_credit_committee = item.id_company_approval_credit_committee;
            }
            companyService.approvalCommittees.approvalCreditCommitteeList(obj).then(function(response){
                console.log(response.data);
                $scope.approvalForwardList = response.data;
                if($scope.approvalForwardList==0){
                    $scope.committeeObj.committee.is_primary_committee = true;
                }

            })
        }

        $q.all([$scope.companyApprovalRoleFun,$scope.braTypeRoleListFun,$scope.approvalForwardFun]).then(function(){
            if(item.id_company_approval_credit_committee!=undefined){

                companyService.approvalCommittees.approvalCreditCommitteeById({
                    'company_id':$rootScope.id_company,
                    'id_company_approval_credit_committee':item.id_company_approval_credit_committee
                }).then(function(response){
                    console.log(response.data.committee);
                    $scope.committeeObj.committee = response.data.committee;
                    if(response.data.committee_members){
                        $scope.committeeObj.committee_members = response.data.committee_members;
                    } else{
                        $scope.committeeObj.committee_members = [];
                    }

                })
            }
        })
        $scope.saveCommittee = function(committee){
            $scope.submitBtn = true;
            companyService.approvalCommittees.forwardCommittee(committee).then(function(response){
                $scope.submitBtn = false;
                if(response.status){
                    r = true;
                } else{
                    var r = confirm(response.error);
                }

                if(r==true){
                    companyService.approvalCommittees.post(committee).then(function(response){
                        if(response.status){
                            $rootScope.getApprovalCommitees();
                            $rootScope.toast('Success',response.message);
                            $scope.cancel();
                        } else{
                            $rootScope.toast('Error',response.error,'error');
                        }
                    })
                }

            });

        }

        //committee members
        $scope.addCommitteeMember = function(item){
            var existItem = false;
            $.each($scope.committeeObj.committee_members,function(key,data){
                if(data.branch_type_role_id==item.branch_type_role.id_branch_type_role&&data.company_approval_role_id==item.company_approval_role.id_company_approval_role){
                    existItem = true;
                }
            })
            if(!existItem){
                var obj = {};
                obj.branch_type_role_name = item.branch_type_role.branch_type_role_name;
                obj.branch_type_role_id = item.branch_type_role.id_branch_type_role;
                obj.company_approval_role_id = item.company_approval_role.id_company_approval_role;
                obj.approval_name = item.company_approval_role.approval_name;

                $scope.committeeObj.committee_members.push(obj);
            }

            $scope.member = {};
        }
        $scope.removeCommitteeMembers = function(item){
            if(item.id_company_approval_credit_committee_structure){
                $scope.committeeObj.delete_members.push(item.id_company_approval_credit_committee_structure);
            }

            var index = $scope.committeeObj.committee_members.indexOf(item);
            $scope.committeeObj.committee_members.splice(index,1);

        }


        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };

        //init
        $scope.braTypeRoleListFun();
        $scope.companyApprovalRoleFun();
        $scope.approvalForwardFun();
    })

    .controller('assessmentCtrl',function($scope,$rootScope,$uibModal,companyService,$state,$q){
        $scope.categoryQuestionList = [];
        $scope.show_options = true;
        $scope.id_assessment_question_type = $state.current.data.id_assessment_question_type;
        $scope.title = $state.current.data.title;
        $scope.sortableOptions = {
            update:function(e,ui){
                //console.log('ui',ui);

                /* var logEntry = tmpList.map(function(i){
                 return i;
                 }).join(', ');
                 $scope.sortingLog.push('Update: ' + logEntry);*/
            },
            stop:function(e,ui){
                // console.log('ui',ui);
                // this callback has the changed model
                /*  var logEntry = tmpList.map(function(i){
                 return i;
                 }).join(', ');
                 $scope.sortingLog.push('Stop: ' + logEntry);*/
            }
        };

        $scope.beforeDrop = function(evt,ui,category){
            angular.element(evt.target).removeClass("drop-hover");
            var option_type = ui.draggable.attr('qtype')
            $scope.id_assessment_question_category = category.category.id_assessment_question_category;
            $scope.assessmentModal(option_type);
        };
        $scope.onOver = function(e){
            angular.element(e.target).addClass("drop-hover");
        };
        $scope.onOut = function(e){
            angular.element(e.target).removeClass("drop-hover");
        };

        $scope.getAssessmentQuestion = function(){
            companyService.assessmentQuestion.question.get({
                'company_id':$rootScope.id_company,
                'id_assessment_question_type':$scope.id_assessment_question_type
            }).then(function(result){
                if(result.status){
                    $scope.categoryQuestionList = result.data[0].category;
                    //console.log(' $scope.categoryQuestionList', $scope.categoryQuestionList);
                }
            })
        }
        $scope.getAssessmentQuestion();


        $scope.statusChange = function(category,question){
            console.log('question',question);
            //console.log('question',question.question_status);
            // question.question_status = question.question_status==1 ? 1 : 0;
            $rootScope.loaderOverlay = false;
            companyService.assessmentQuestion.question.post(question).then(function(result){
                if(result.status){
                    //$scope.getAssessmentQuestion();
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })
        }
        $scope.categoryStatusChange = function(category){
            var $statusObj = {};
            $statusObj.assessment_question_type_id = category.assessment_question_type_id;
            $statusObj.company_id = $rootScope.id_company;
            $statusObj.assessment_question_category_name = category.assessment_question_category_name;
            $statusObj.assessment_question_category_status = category.assessment_question_category_status;
            $rootScope.loaderOverlay = false;
            companyService.assessmentQuestion.category.post($statusObj).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })
        }

        $scope.product_id = undefined;
        $scope.assessmentModal = function(option_type,question){
            var option = {};
            option.value = option_type;
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/admin/assessment/assessment-questions-view.html',
                scope:$scope,
                controller:'assessmentModalCtrl',
                resolve:{
                    category:option,
                    item:question
                }
            });
            modalInstance.result.then(function($data){

            },function(){

            });
        }

        $scope.addCategoryModal = function(selectedItem){
            var modalInstance = $uibModal.open({
                animation:true,
                //windowClass: 'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/admin/assessment/add-category-modal.html',
                scope:$scope,
                resolve:{
                    item:selectedItem
                },
                controller:function($uibModalInstance,$scope,$rootScope,item,companyService,masterService){
                    $scope.category = {};
                    $scope.category.assessment_question_category_name = '';
                    $scope.sectorsList = [];
                    $rootScope.loaderOverlay = false;
                    $scope.submitted = false;
                    masterService.sector.getAssessmentSectors().then(function(result){
                        $scope.sectorsList = result.data.data;
                    });
                    if(item!=undefined){
                        $scope.category.assessment_question_category_name = item.assessment_question_category_name;
                        $scope.category.id_assessment_question_category = item.id_assessment_question_category;
                        if(item.sector_id!=null&&item.sector_id!=undefined){
                            var strVale = item.sector_id;
                            $scope.category.sector_id = strVale.split(',');
                        }

                    }

                    $scope.save = function(category,form){
                        if(form.$valid){
                            category.company_id = $rootScope.id_company;
                            category.assessment_question_type_id = $scope.id_assessment_question_type;
                            category.sector_id = category.sector_id.toString();
                            companyService.assessmentQuestion.category.post(category).then(function(result){
                                if(result.status){
                                    $scope.getAssessmentQuestion();
                                    $rootScope.toast('Success',result.message);
                                    $scope.cancel();
                                } else{
                                    $rootScope.toast('Error',result.error,'error');
                                }
                            })

                        }
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function($data){

            },function(){

            });
        }

    })

    .controller('disbursementChecklistCtrl',function(encodeFilter,$scope,$rootScope,$uibModal,companyService,$state,$q,$stateParams){
        $scope.categoryQuestionList = [];
        $scope.show_options = true;
        $scope.id_assessment_question_type = $state.current.data.id_assessment_question_type;
        $scope.title = $state.current.data.title;
        $scope.product_id = encodeFilter($stateParams.productId);
        $scope.sortableOptions = {
            update:function(e,ui){
                //console.log('ui',ui);

                /* var logEntry = tmpList.map(function(i){
                 return i;
                 }).join(', ');
                 $scope.sortingLog.push('Update: ' + logEntry);*/
            },
            stop:function(e,ui){
                // console.log('ui',ui);
                // this callback has the changed model
                /*  var logEntry = tmpList.map(function(i){
                 return i;
                 }).join(', ');
                 $scope.sortingLog.push('Stop: ' + logEntry);*/
            }
        };

        $scope.beforeDrop = function(evt,ui,category){
            angular.element(evt.target).removeClass("drop-hover");
            var option_type = ui.draggable.attr('qtype')
            $scope.id_assessment_question_category = category.category.id_assessment_question_category;
            $scope.assessmentModal(option_type);
        };
        $scope.onOver = function(e){
            angular.element(e.target).addClass("drop-hover");
        };
        $scope.onOut = function(e){
            angular.element(e.target).removeClass("drop-hover");
        };

        $scope.getAssessmentQuestion = function(){
            companyService.assessmentQuestion.question.get({
                'company_id':$rootScope.id_company,
                'id_assessment_question_type':$scope.id_assessment_question_type,
                'product_id':$scope.product_id
            }).then(function(result){
                if(result.status){
                    $scope.categoryQuestionList = result.data[0].category;
                    //console.log(' $scope.categoryQuestionList', $scope.categoryQuestionList);
                }
            })
        }
        $scope.getAssessmentQuestion();


        $scope.statusChange = function(category,question){
            console.log('question',question);
            //console.log('question',question.question_status);
            // question.question_status = question.question_status==1 ? 1 : 0;
            $rootScope.loaderOverlay = false;
            companyService.assessmentQuestion.question.post(question).then(function(result){
                if(result.status){
                    //$scope.getAssessmentQuestion();
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })
        }
        $scope.categoryStatusChange = function(category){
            var $statusObj = {};
            $statusObj.assessment_question_type_id = category.assessment_question_type_id;
            $statusObj.company_id = $rootScope.id_company;
            $statusObj.assessment_question_category_name = category.assessment_question_category_name;
            $statusObj.assessment_question_category_status = category.assessment_question_category_status;
            $rootScope.loaderOverlay = false;
            companyService.assessmentQuestion.category.post($statusObj).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })
        }


        $scope.assessmentModal = function(option_type,question){
            var option = {};
            option.value = option_type;
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'modal-full my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/admin/assessment/assessment-questions-view.html',
                scope:$scope,
                controller:'assessmentModalCtrl',
                resolve:{
                    category:option,
                    item:question
                }
            });
            modalInstance.result.then(function($data){

            },function(){

            });
        }

        $scope.addCategoryModal = function(selectedItem){
            var modalInstance = $uibModal.open({
                animation:true,
                //windowClass: 'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/admin/assessment/add-category-modal.html',
                scope:$scope,
                resolve:{
                    item:selectedItem
                },
                controller:function($uibModalInstance,$scope,$rootScope,item,companyService,masterService){
                    $scope.category = {};
                    $scope.category.assessment_question_category_name = '';
                    $scope.sectorsList = [];
                    $rootScope.loaderOverlay = false;
                    masterService.sector.getAll().then(function(result){
                        $scope.sectorsList = result.data.data;
                    });
                    if(item!=undefined){
                        $scope.category.assessment_question_category_name = item.assessment_question_category_name;
                        $scope.category.id_assessment_question_category = item.id_assessment_question_category;
                        if(item.sector_id!=null&&item.sector_id!=undefined){
                            var strVale = item.sector_id;
                            $scope.category.sector_id = strVale.split(',');
                        }

                    }

                    $scope.save = function(category){
                        if($scope.form.$valid){
                            category.company_id = $rootScope.id_company;
                            category.assessment_question_type_id = $scope.id_assessment_question_type;
                            category.sector_id = category.sector_id.toString();
                            category.product_id = $scope.product_id;
                            companyService.assessmentQuestion.category.post(category).then(function(result){
                                if(result.status){
                                    $scope.getAssessmentQuestion();
                                    $rootScope.toast('Success',result.message);
                                    $scope.cancel();
                                } else{
                                    $rootScope.toast('Error',result.error,'error');
                                }
                            })

                        }
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function($data){

            },function(){

            });
        }

    })

    .controller('assessmentModalCtrl',function($uibModalInstance,$scope,$rootScope,item,category,companyService){
        // $scope.categoryTitle = category.assessment_question_category_name;

        $scope.question = {};
        $scope.question.required = 1;
        $scope.question.created_by = $rootScope.userId;

        $scope.question.question_option = [];
        $scope.question.question_type = category.value;

        $rootScope.loaderOverlay = false;
        $scope.getCategory = function(){
            companyService.assessmentQuestion.category.get({
                'company_id':$rootScope.id_company,
                'assessment_question_type_id':$scope.id_assessment_question_type,
                'product_id':$scope.product_id
            }).then(function(result){
                if(result.status){
                    $scope.categoryList = result.data;
                    if($scope.id_assessment_question_category!=undefined){
                        console.log('$scope.id_assessment_question_category',$scope.id_assessment_question_category);
                        $scope.question.assessment_question_category_id = $scope.id_assessment_question_category;
                    }
                    if(item!=undefined){
                        angular.copy(item,$scope.question);
                        $scope.question.required = parseInt(item.required);
                        $scope.option_type = item.question_type;
                        // $scope.question = item;
                        //  $scope.question.question_option = JSON.stringify(item.question_option);
                    }
                }
            })
        }
        $scope.getCategory();

        $scope.save = function(question){
            if($scope.form.$valid){
                companyService.assessmentQuestion.question.post(question).then(function(result){
                    if(result.status){
                        $scope.getAssessmentQuestion();
                        $rootScope.toast('Success',result.message);
                        $scope.cancel();
                    } else{
                        $rootScope.toast('Error',result.error,'error');
                    }
                })
            }
        }


        // add new option to the field
        $scope.addOption = function(question,title){

            if(!question.question_option)
                question.question_option = new Array();

            var lastOptionID = 0;

            if(question.question_option[question.question_option.length-1])
                lastOptionID = question.question_option[question.question_option.length-1].option_id;

            // new option's id
            var option_id = lastOptionID+1;

            var newOption = {
                "option_id":option_id,
                "option_title":title,
                "option_value":option_id,
                "option_name":'option_name_'+option_id,
                "option_check":true
            };

            // put new option into field_options array
            question.question_option.push(newOption);
        }

        // delete particular option
        $scope.deleteOption = function(question,option){
            for(var i = 0; i<question.question_option.length; i++){
                if(question.question_option[i].option_id==option.option_id){
                    question.question_option.splice(i,1);
                    break;
                }
            }
        }

        // decides whether field options block will be shown (true for dropdown and radio fields)
        $scope.showAddOptions = function(question){

            if(question.question_type=="radio"||question.question_type=="dropdown"||question.question_type=="checkbox"){
                if(question.question_option.length==0){
                    if(question.question_type=="radio"){
                        $scope.addOption(question,'Yes');
                        $scope.addOption(question,'No');
                        // $scope.addOption(question,'Not Mentioned');
                    } else{
                        $scope.addOption(question);
                    }
                }
                return true;
            }
            else
                return false;
        }
        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('assessmentModalCtrl2',function($uibModalInstance,$scope,$rootScope,item,category,companyService){
        $scope.categoryTitle = category.assessment_question_category_name;
        $scope.question = {};
        $scope.question.required = true;
        $scope.question.created_by = $rootScope.userId;
        $scope.question.assessment_question_category_id = category.id_assessment_question_category;
        $scope.question.question_option = [];
        if(item!=undefined){
            angular.copy(item,$scope.question);
            // $scope.question = item;
            //  $scope.question.question_option = JSON.stringify(item.question_option);

        }

        $scope.save = function(question){
            if($scope.form.$valid){
                companyService.assessmentQuestion.question.post(question).then(function(result){
                    if(result.status){
                        $scope.getAssessmentQuestion();
                        $rootScope.toast('Success',result.message);
                        $scope.cancel();
                    } else{
                        $rootScope.toast('Error',result.error,'error');
                    }
                })
            }
        }


        // add new option to the field
        $scope.addOption = function(question){
            if(!question.question_option)
                question.question_option = new Array();

            var lastOptionID = 0;

            if(question.question_option[question.question_option.length-1])
                lastOptionID = question.question_option[question.question_option.length-1].option_id;

            // new option's id
            var option_id = lastOptionID+1;

            var newOption = {
                "option_id":option_id,
                "option_title":"",
                "option_value":option_id,
                "option_name":'option_name_'+option_id,
                "option_check":true
            };

            // put new option into field_options array
            question.question_option.push(newOption);
        }

        // delete particular option
        $scope.deleteOption = function(question,option){
            for(var i = 0; i<question.question_option.length; i++){
                if(question.question_option[i].option_id==option.option_id){
                    question.question_option.splice(i,1);
                    break;
                }
            }
        }

        // decides whether field options block will be shown (true for dropdown and radio fields)
        $scope.showAddOptions = function(question){
            if(question.question_type=="radio"||question.question_type=="dropdown"||question.question_type=="checkbox"){
                if(question.question_option.length==0){
                    $scope.addOption(question);
                }

                return true;
            }
            else
                return false;
        }
        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('assessmentQuestionView',function($scope,$rootScope,companyService){

        $scope.categoryQuestionList = [];
        $scope.getAssessmentQuestion = function(){
            companyService.assessmentQuestion.question.get({
                'company_id':$rootScope.id_company,
                'id_assessment_question_type':3
            }).then(function(result){
                if(result.status){
                    $scope.categoryQuestionList = result.data[0].category;
                    //console.log(' $scope.categoryQuestionList', $scope.categoryQuestionList);
                }
            })
        };
        $scope.getAssessmentQuestion();

    })

    .controller('knowledgeManagementCtrl',function($scope,$rootScope,$state,$uibModal,companyService){
        $scope.addKnowledgeManagementModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                backdrop:'static',
                keyboard:false,
                windowClass:'modal-full my-class modal-bodyPaddingnone',
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/admin/knowledge-management/add-knowledge-management.html',
                controller:'addKnowledgeManagementCtrl'
            });
            modalInstance.result.then(function(){
            },function(){
                $scope.knowledgeManagement();
                $rootScope.knowledgeManagementObj = '';
            });
        }
        $scope.knowledgeManagement = function(){
            var params = {};
            params.company_id = $rootScope.id_company;
            if($rootScope.user_role_id!=2){
                params.created_by = $rootScope.userId;
            }
            companyService.knowledgeManagement.get(params).then(function(result){
                $scope.knowledgeData = result.data.data;
                $scope.countData = result.data.document_type_data;
                $scope.usersData = result.data.users;
            })
        };
        $scope.knowledgeManagementGraph = function(){
            companyService.knowledgeManagement.knowledgeManagementGraph({'company_id':$rootScope.id_company}).then(function(result){
                $scope.knowledgeGraphData = result.data;
                $scope.graph = {views:'',uploads:''};
                var views = [];
                var uploads = [];
                angular.forEach($scope.knowledgeGraphData.views,function(value,key){
                    views.push([new Date(value.date_time).getTime(),parseInt(value.views)])
                });
                angular.forEach($scope.knowledgeGraphData.uploads,function(value,key){
                    uploads.push([new Date(value.date_time).getTime(),parseInt(value.uploads)])
                });
                $scope.graph = {views:views,uploads:uploads};
            })
        };
    })

    .controller('searchKnowledgeManagementCtrl',function($scope,$rootScope,$state,$uibModal,companyService,crmContactService){
        companyService.knowledgeManagement.getTags().then(function(result){
            $scope.tagsData = result.data;
        });
        $scope.knowledgeManagement = function(){
            companyService.knowledgeManagement.get({
                'company_id':$rootScope.id_company,
                'created_by':$rootScope.userId
            }).then(function(result){
                $scope.countData = result.data.document_type_data;
            })
        };
        $scope.knowledgeManagement();
        $scope.changedValue = function(val){
            console.log('valvalval',val);
            $scope.tableStateRef.search_tags = JSON.stringify(val);
            $scope.callServer($scope.tableStateRef);
        }
        $scope.displayed = [];
        $scope.callServer = function(tableState){
            tableState.company_id = $rootScope.id_company;
            if($rootScope.user_role_id!=2){
                tableState.created_by = $rootScope.userId
            }

            companyService.knowledgeManagement.getSearchResult(tableState).then(function(result){
                $scope.displayed = true;
                $scope.tableStateRef = tableState;
                $scope.displayed = result.data.data;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/tableState.pagination.number);
            });
        };
        $scope.addKnowledgeManagementModal = function(knowledgeObj){
            if(knowledgeObj){
                $rootScope.knowledgeManagementObj = knowledgeObj;
            }
            var modalInstance = $uibModal.open({
                animation:true,
                backdrop:'static',
                keyboard:false,
                windowClass:'modal-full my-class modal-bodyPaddingnone',
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/admin/knowledge-management/add-knowledge-management.html',
                controller:'addKnowledgeManagementCtrl'
            });
            modalInstance.result.then(function(){
            },function(){
                $rootScope.knowledgeManagementObj = '';
                $scope.callServer($scope.tableStateRef);
            });
        }
    })

    .controller('addKnowledgeManagementCtrl',function($scope,$rootScope,$uibModalInstance,companyService,Upload){
        $scope.knowledge = {};
        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
        companyService.knowledgeManagement.getTags().then(function(result){
            $scope.tagsData = result.data;
            if($rootScope.knowledgeManagementObj&&$rootScope.knowledgeManagementObj!=''){
                $scope.knowledge = $rootScope.knowledgeManagementObj;
            }
        });
        $scope.uploadDocument = function(file){
            if(file!=null&&file!=''){
                $scope.file = file;
            } else{
                $rootScope.toast('Error','file format not supported file formats','image-error');
            }
        };
        $scope.save = function(knowledge,knowledgeForm){
            $scope.submitStatus = true;

            if(!knowledgeForm.$valid){
                return false;
            }
            if(knowledge.document_type=='document'&&$rootScope.knowledgeManagementObj==undefined&&$scope.file==undefined){
                $scope.fileError = true;
                return false;
            }
            var postObj = knowledge;
            postObj.uploaded_by = $rootScope.userId;
            postObj.company_id = $rootScope.id_company;

            Upload.upload({
                url:API_URL+'company/knowledge',
                data:{
                    file:{'document':$scope.file},
                    'data':postObj
                }
            }).then(function(resp){
                if(resp.data.status){
                    $rootScope.toast('Success',resp.data.message);
                    $uibModalInstance.dismiss('cancel');
                } else{
                    $rootScope.toast('Error',resp.data.error,'error',$scope.user);
                }
            });
        }
    })

    .controller('knowledgeManagementViewCtrl',function($scope,$rootScope,companyService,$stateParams){
        $scope.knowledgeId = $stateParams.id;

        $scope.knowledgeDocumentDetails = function(){
            companyService.knowledgeManagement.getDetails({'knowledge_document_id':$scope.knowledgeId}).then(function(result){
                $scope.knowledgeDocumentDetails = result.data;
            })
        };
        $scope.knowledgeDocumentDetails();

        $scope.getKnowledgeDocumentComment = function(){
            companyService.knowledgeDocumentComment.get({'knowledge_document_id':$scope.knowledgeId}).then(function(result){
                $scope.knowledgeDocumentComment = result.data;
            })
        };
        $scope.getKnowledgeDocumentComment();

        $scope.changeKnowledgeDocumentStatus = function(){
            companyService.knowledgeManagement.knowledgePublish({
                'knowledge_document_id':$scope.knowledgeId,
                'knowledge_document_status':1
            }).then(function(result){
                $scope.knowledgeDocumentDetails.knowledge_document_status = 1
                $rootScope.toast('Success',result.message);
                ;
            })
        };

        $scope.addKnowledgeManagementComment = function(comment,form){
            $scope.submitStatus = true;
            if(form.$valid){
                companyService.knowledgeDocumentComment.add(
                    {
                        'knowledge_document_id':$stateParams.id,
                        'commented_by':$rootScope.userId,
                        'comment':comment
                    }
                ).then(function(result){
                    $scope.comment = '';
                    $scope.submitStatus = false;
                    $scope.getKnowledgeDocumentComment();
                })
            }
        };


    })

    .controller('riskAssessmentCtrl',function($scope,$rootScope,masterService,companyService){

        $scope.riskCategory = [];
        $scope.subCategoryForm = {};
        $scope.clone = function($org_obj){
            return angular.copy($org_obj);
        }
        masterService.sector.getAll().then(function(result){
            $scope.sectorsList = result.data.data;
        });
        companyService.riskAssessment.riskCategory.get({'company_id':$rootScope.id_company}).then(function(result){
            if(result.status){
                $scope.riskCategory = result.data;
            } else{
                $rootScope.toast('Error',result.error,'error');
            }
        })

        $scope.addRiskCategory = function(category){
            //category.sub_category = [];
            var param = {};
            param.company_id = $rootScope.id_company;
            param.parent_risk_category_id = 0;
            param.risk_percentage = category.risk_percentage;
            param.risk_category_name = category.risk_category_name;
            $rootScope.loaderOverlay = false;
            companyService.riskAssessment.riskCategory.post(param).then(function(result){
                console.log('data',result);
                if(result.status){
                    result.data.sub_category = [];
                    $scope.riskCategory.push(result.data);
                    $scope.category = {};
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })

        }
        $scope.addSubRiskCategory = function(categoryIndex,category){
            // if(category.subCategoryForm.risk_percentage == undefined)  category.subCategoryForm.risk_percentage = '';
            // if(category.subCategoryForm.risk_category_name == undefined)  category.subCategoryForm.risk_category_name = '';
            var param = {};
            param.company_id = $rootScope.id_company;
            param.parent_risk_category_id = category.id_risk_category;
            param.risk_percentage = category.subCategoryForm.risk_percentage;
            param.risk_category_name = category.subCategoryForm.risk_category_name;
            //item.attributes = [];
            $rootScope.loaderOverlay = false;
            companyService.riskAssessment.riskCategory.post(param).then(function(result){
                console.log('data',result);
                if(result.status){
                    result.data.attributes = [];
                    $scope.riskCategory[categoryIndex].sub_category.push(result.data);
                    category.subCategoryForm = {};
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })


        }
        $scope.addSubcategoryAttr = function(category){

            var param = {};
            param.company_id = $rootScope.id_company;
            param.parent_risk_category_id = category.subSelected.id_risk_category;
            param.risk_percentage = 0;
            param.risk_category_name = category.subSelected.selectedForm.risk_category_name;
            $rootScope.loaderOverlay = false;
            companyService.riskAssessment.riskCategory.post(param).then(function(result){
                if(result.status){
                    result.data.sector_list = [];
                    category.subSelected.attributes.push(result.data);
                    category.subSelected.selectedForm = {};
                    category.subSelected.attrForm = false;
                    $rootScope.toast('Success',result.message);
                } else{

                    $rootScope.toast('Error',result.error,'error');
                }
            })
        }
        $scope.addSectorItems = function(attribute){
            var param = {};
            param.risk_category_id = attribute.id_risk_category;
            param.risk_category_item_name = attribute.sectorSelected.form.risk_category_item_name;
            param.risk_category_item_grade = attribute.sectorSelected.form.risk_category_item_grade;
            param.sector_id = attribute.sectorSelected.id_sector;
            $rootScope.loaderOverlay = false;
            companyService.riskAssessment.riskItem.post(param).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    attribute.sectorSelected.items.push(result.data);
                    attribute.sectorSelected.form = {};
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })
        }
        $scope.subCategoryClick = function(subCategory,category){

            category.subSelected = subCategory;
            category.attrDivShow = true;
        }
        $scope.sectorClick = function(attribute,sector){

            attribute.sectorSelected = sector;
            attribute.itemDivShow = true;
        }
        $scope.sectorDropdownChange = function(attribute,item){
            if(item!=undefined){
                var obj = [];
                obj.id_sector = item.id_sector
                obj.parent_id = item.parent_id
                obj.sector_name = item.sector_name
                obj.parent = item.parent
                obj.items = [];
                attribute.sector_list.push(obj);
            }
        }
        $scope.checkSectorDropdown = function(sectorList,option){
//            console.log('option',_.findWhere(sectorList, {'id_sector' : option.id_sector}));
            return _.findWhere(sectorList,{'id_sector':option.id_sector})
        }
        $scope.isActive = function(item,category){
            return category.subSelected===item;
        };

        $scope.isSectorActive = function(attribute,item){
            return attribute.sectorSelected.id_sector===item.id_sector;
        };

        $scope.editRiskCategory = function(subCategory){
            console.log('category',subCategory);
            var param = {};
            param.company_id = $rootScope.id_company;
            param.id_risk_category = subCategory.id_risk_category;
            param.risk_percentage = subCategory.risk_percentage;
            param.risk_category_name = subCategory.risk_category_name;
            param.parent_risk_category_id = subCategory.parent_risk_category_id;

            $rootScope.loaderOverlay = false;
            companyService.riskAssessment.riskCategory.post(param).then(function(result){
                console.log('data',result);
                if(result.status){
                    subCategory.popover = false;
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })

        }
        $scope.editAttributeItem = function(item){

            var param = {};
            param.id_risk_category_item = item.id_risk_category_item;
            param.risk_category_id = item.risk_category_id;
            param.risk_category_item_name = item.risk_category_item_name;
            param.risk_category_item_grade = item.risk_category_item_grade;
            param.sector_id = item.sector_id;
            item.popover = false;
            $rootScope.loaderOverlay = false;
            companyService.riskAssessment.riskItem.post(param).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })

        }

    })
    .controller('businessAssessmentCtrl',function($scope,$rootScope,crmContactService){
        $scope.allAssessmentList = [];
        $scope.getBusinessAssessment = function(){
            crmContactService.getAllAssessment({'company_id':$rootScope.id_company}).then(function(result){
                $scope.allAssessmentList = result.data;
                angular.forEach($scope.allAssessmentList,function(pvalue,pkey){
                    if(pvalue.checked==0){
                        $scope.allAssessmentList[pkey].status = false;
                        $scope.allAssessmentList[pkey].checked = false;
                    } else{
                        $scope.allAssessmentList[pkey].status = true;
                        $scope.allAssessmentList[pkey].checked = true;
                    }


                });
            });
        }
        $scope.getBusinessAssessment();
        $scope.updateAssessment = function(){
            var assessment = [];
            angular.forEach($scope.allAssessmentList,function(pvalue,pkey){
                if(pvalue.status){
                    assessment.push($scope.allAssessmentList[pkey].id_assessment);
                }
            });
            var $postData = {
                'company_id':$rootScope.id_company,
                'created_by':$rootScope.userId,
                'assessments':assessment
            }
            crmContactService.updateAssessment($postData).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error',$scope.sector);
                }
            });
        }
    })
    .controller('exchangeRateCtrl',function($scope,$rootScope,companyService,$uibModal){

        $scope.currencyList = [];
        $scope.companyInfo = {};
        $scope.init = function(){
            companyService.currency.currencyTable({'company_id':$rootScope.id_company}).then(function(result){
                // console.log('result.data',result.data);
                $scope.currencyList = result.data;
            })
            companyService.getById($rootScope.id_company).then(function(result){
                // console.log('result.data',result.data);
                $scope.companyInfo = result.data;
            })
        }

        $scope.addCurrencyModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                backdrop:'static',
                keyboard:false,
                windowClass:'my-class',
                scope:$scope,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/admin/operating-currency/exchange-rate-modal.html',
                resolve:{
                    currency:function(){
                        return companyService.currency.get({'company_id':$rootScope.id_company}).then(function($result){
                            return $result.data;
                        });
                    }
                },
                controller:function($rootScope,$scope,$uibModalInstance,currency,$q,companyService){
                    $scope.currencyList = currency;
                    $scope.params = [];
                    $scope.save = function(currencyItems){
                        angular.forEach(currencyItems,function(item){
                            var $obj = {};
                            $obj.company_id = $rootScope.id_company;
                            $obj.company_currency_id = item.id_company_currency;
                            $obj.company_currency_value = item.currency_value;
                            $scope.params.push($obj);
                        })

                        $q.all($scope.params).then(function(params){
                            companyService.currency.currencyValue(params).then(function(result){
                                if(result.status){
                                    $rootScope.toast('Success',result.message);
                                    $uibModalInstance.close();
                                } else{
                                    $rootScope.toast('Error',result.error);
                                }
                            })
                        });
                    }

                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }

            });

            modalInstance.result.then(function(selectedItem){
                $scope.init();
            },function(){

            });
        };


    })
    .controller('primaryCurrencyCtrl',function($scope,$rootScope,masterService,companyService){

        $scope.currencyList = [];
        $scope.companyInfo = {};
        $scope.init = function(){
            masterService.currency.getAll().then(function(result){
                console.log('result.data',result.data);
                $scope.currencyList = result.data;
                companyService.getById($rootScope.id_company).then(function(result){
                    // console.log('result.data',result.data);
                    $scope.companyInfo = result.data;
                })
            })
        }
        $scope.updateCurrency = function($data){
            var param = {};
            param.company_id = $rootScope.id_company;
            param.currency_id = $data;
            if(confirm("If you change currency, it will effect future projects")){
                companyService.primaryCurrency(param).then(function(result){
                    if(result.status){
                        $rootScope.toast('Success',result.message);
                    } else{
                        $rootScope.toast('Error',result.error,'error');
                    }
                });
            }
        }
        $scope.init();

    })
    .controller('operatingCurrencyCtrl',function($scope,$rootScope,masterService,companyService){

        $scope.currencyList = [];
        $scope.companyInfo = {};
        $scope.init = function(){
            $scope.currencyList = [];
            companyService.currency.CurrencySelected({'company_id':$rootScope.id_company}).then(function(result){
                //$scope.currencyList = result.data;
                angular.forEach(result.data,function(item){
                    var obj = item;
                    obj.checkStatus = (item.is_selected==0) ? false : true;
                    $scope.currencyList.push(obj);
                })
            })
        }
        $scope.updateSelected = function(){
            $scope.selectedIds = [];
            angular.forEach($scope.currencyList,function(item){
                if(item.checkStatus){
                    $scope.selectedIds.push(item.id_currency);
                }
            })
            var param = {};
            param.company_id = $rootScope.id_company;
            param.currency_id = $scope.selectedIds;
            companyService.currency.post(param).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $scope.init();
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })

        }
        $scope.init();

    })

    .controller('collateralListCtrl',function($scope,$rootScope,masterService,companyService,$state){
        $scope.collateralTypeList = [];
        $scope.collateralType = function(){
            companyService.collateral.collateralType({'company_id':$rootScope.id_company}).then(function(result){
                $scope.collateralTypeList = result.data;
            });
        }
        $scope.collateralType();
        $scope.goToState = function(item,tab){
            $rootScope.collateralTypeInfo = {item:item,tab:tab};
            $state.go('collateral.add-collateral')
        }
    })

    .controller('collateralConfigCtrl',function($scope,$rootScope,masterService,companyService,$state){
        $scope.collateralStageList = [];
        $scope.templateInfo = {};
        if($rootScope.collateralTypeInfo==undefined){
            $state.go('collateral.collateral-list')
            return false;
        }

        $scope.tabActive1=$scope.tabActive2=$scope.tabActive3= false;
        if($rootScope.collateralTypeInfo.tab == 1){ $scope.tabActive1=true; }else if($rootScope.collateralTypeInfo.tab == 2){$scope.tabActive2=true;}else{$scope.tabActive3=true;}

        $scope.init = function(){
            $scope.loadTemplate($rootScope.collateralTypeInfo.item);
        }
        $scope.loadTemplate = function(collateralType){
            $rootScope.currentCollateralId = collateralType.id_collateral_type;
            console.log('collateralType',collateralType);
            $scope.templateInfo = collateralType;
            $scope.collateralStage(collateralType.id_collateral_type);
            $scope.getAssessmentQuestion = function(){
                console.log('$rootScope.currentCollateralId',$rootScope.currentCollateralId);
                companyService.assessmentQuestion.question.get({
                    'company_id':$rootScope.id_company,
                    'id_collateral_type':$rootScope.currentCollateralId,

                }).then(function(result){
                    if(result.status){
                        $scope.categoryQuestionList = result.data[0].category;
                        //console.log(' $scope.categoryQuestionList', $scope.categoryQuestionList);
                    }
                })
            }
        }
        $scope.shuffle = function(item,type){
            if(type){
                item.status=1;
            }else{
                item.status=0;
            }
        }
        $scope.collateralStage = function(collateral_type_id){
            $scope.collateralStageList = [];
            companyService.collateral.collateralStage.get({
                'company_id':$rootScope.id_company,
                'collateral_type_id':collateral_type_id
            }).then(function(result){
                $scope.collateralStageList = result.data;
            });
        }
        $scope.saveCheckList = function(){
            $scope.arrayChecks = [];
            $scope.collateralStageList.forEach(function(data){
                if(data.status==1){
                    $scope.arrayChecks.push(data.id_collateral_stage);
                }
            })

            companyService.collateral.collateralStage.post({
                'company_id':$rootScope.id_company,
                'collateral_type_id':$scope.templateInfo.id_collateral_type,
                'collateral_stage_ids':$scope.arrayChecks
            }).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            });
            console.log(' $scope.arrayChecks',$scope.arrayChecks);
        }


        $scope.init();
    })

    .controller('collateralChecklistCtrl',function(encodeFilter,$scope,$rootScope,$uibModal,companyService,$state,$q,$stateParams){
        $scope.categoryQuestionList = [];
        $scope.show_options = true;
        $scope.id_assessment_question_type = '';
        $scope.title = "Title";
        // $scope.product_id = encodeFilter($stateParams.productId);
        $scope.sortableOptions = {
            update:function(e,ui){
                //console.log('ui',ui);

                /* var logEntry = tmpList.map(function(i){
                 return i;
                 }).join(', ');
                 $scope.sortingLog.push('Update: ' + logEntry);*/
            },
            stop:function(e,ui){
                // console.log('ui',ui);
                // this callback has the changed model
                /*  var logEntry = tmpList.map(function(i){
                 return i;
                 }).join(', ');
                 $scope.sortingLog.push('Stop: ' + logEntry);*/
            }
        };

        $scope.beforeDrop = function(evt,ui,category){
            angular.element(evt.target).removeClass("drop-hover");
            var option_type = ui.draggable.attr('qtype')
            $scope.id_assessment_question_category = category.category.id_assessment_question_category;
            $scope.assessmentModal(option_type);
        };
        $scope.onOver = function(e){
            angular.element(e.target).addClass("drop-hover");
        };
        $scope.onOut = function(e){
            angular.element(e.target).removeClass("drop-hover");
        };

        $scope.getAssessmentQuestion = function(){
            console.log('$rootScope.currentCollateralId',$rootScope.currentCollateralId);
            companyService.assessmentQuestion.question.get({
                'company_id':$rootScope.id_company,
                'collateral_type_id':$rootScope.currentCollateralId,
                'assessment_question_type_key':'collateral_document'

            }).then(function(result){
                if(result.status){
                    $scope.categoryQuestionList = result.data[0].category;
                    //console.log(' $scope.categoryQuestionList', $scope.categoryQuestionList);
                }
            })
        }
        //$scope.getAssessmentQuestion();


        $scope.statusChange = function(category,question){
            console.log('question',question);
            //console.log('question',question.question_status);
            // question.question_status = question.question_status==1 ? 1 : 0;
            $rootScope.loaderOverlay = false;
            companyService.assessmentQuestion.question.post(question).then(function(result){
                if(result.status){
                    //$scope.getAssessmentQuestion();
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })
        }
        $scope.categoryStatusChange = function(category){
            var $statusObj = {};
            $statusObj.assessment_question_type_id = category.assessment_question_type_id;
            $statusObj.company_id = $rootScope.id_company;
            $statusObj.assessment_question_category_name = category.assessment_question_category_name;
            $statusObj.assessment_question_category_status = category.assessment_question_category_status;
            $rootScope.loaderOverlay = false;
            companyService.assessmentQuestion.category.post($statusObj).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            })
        }


        $scope.assessmentModal = function(option_type,question){
            var option = {};
            option.value = option_type;
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'modal-full my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/admin/assessment/assessment-questions-view.html',
                scope:$scope,
                controller:function($uibModalInstance,$scope,$rootScope,item,category,companyService){
                    // $scope.categoryTitle = category.assessment_question_category_name;

                    $scope.question = {};
                    $scope.question.required = 1;
                    $scope.question.created_by = $rootScope.userId;

                    $scope.question.question_option = [];
                    $scope.question.question_type = category.value;

                    $rootScope.loaderOverlay = false;
                    $scope.getCategory = function(){
                        companyService.assessmentQuestion.category.get({
                            'company_id':$rootScope.id_company,
                            'assessment_question_type_id':5,
                            'collateral_type_id':$rootScope.currentCollateralId
                        }).then(function(result){
                            if(result.status){
                                $scope.categoryList = result.data;
                                if($scope.id_assessment_question_category!=undefined){
                                    console.log('$scope.id_assessment_question_category',$scope.id_assessment_question_category);
                                    $scope.question.assessment_question_category_id = $scope.id_assessment_question_category;
                                }
                                if(item!=undefined){
                                    angular.copy(item,$scope.question);
                                    $scope.question.required = parseInt(item.required);
                                    $scope.option_type = item.question_type;
                                    // $scope.question = item;
                                    //  $scope.question.question_option = JSON.stringify(item.question_option);
                                }
                            }
                        })
                    }
                    $scope.getCategory();

                    $scope.save = function(question){
                        if($scope.form.$valid){
                            companyService.assessmentQuestion.question.post(question).then(function(result){
                                if(result.status){
                                    $scope.getAssessmentQuestion();
                                    $rootScope.toast('Success',result.message);
                                    $scope.cancel();
                                } else{
                                    $rootScope.toast('Error',result.error,'error');
                                }
                            })
                        }
                    }


                    // add new option to the field
                    $scope.addOption = function(question,title){

                        if(!question.question_option)
                            question.question_option = new Array();

                        var lastOptionID = 0;

                        if(question.question_option[question.question_option.length-1])
                            lastOptionID = question.question_option[question.question_option.length-1].option_id;

                        // new option's id
                        var option_id = lastOptionID+1;

                        var newOption = {
                            "option_id":option_id,
                            "option_title":title,
                            "option_value":option_id,
                            "option_name":'option_name_'+option_id,
                            "option_check":true
                        };

                        // put new option into field_options array
                        question.question_option.push(newOption);
                    }

                    // delete particular option
                    $scope.deleteOption = function(question,option){
                        for(var i = 0; i<question.question_option.length; i++){
                            if(question.question_option[i].option_id==option.option_id){
                                question.question_option.splice(i,1);
                                break;
                            }
                        }
                    }

                    // decides whether field options block will be shown (true for dropdown and radio fields)
                    $scope.showAddOptions = function(question){

                        if(question.question_type=="radio"||question.question_type=="dropdown"||question.question_type=="checkbox"){
                            if(question.question_option.length==0){
                                if(question.question_type=="radio"){
                                    $scope.addOption(question,'Yes');
                                    $scope.addOption(question,'No');
                                    // $scope.addOption(question,'Not Mentioned');
                                } else{
                                    $scope.addOption(question);
                                }
                            }
                            return true;
                        }
                        else
                            return false;
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                resolve:{
                    category:option,
                    item:question
                }
            });
            modalInstance.result.then(function($data){

            },function(){

            });
        }

        $scope.addCategoryModal = function(selectedItem){
            var modalInstance = $uibModal.open({
                animation:true,
                //windowClass: 'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/admin/assessment/add-category-modal.html',
                scope:$scope,
                resolve:{
                    item:selectedItem
                },
                controller:function($uibModalInstance,$scope,$rootScope,item,companyService,masterService){
                    $scope.category = {};
                    $scope.category.assessment_question_category_name = '';
                    $scope.sectorsList = [];
                    $rootScope.loaderOverlay = false;
                    masterService.sector.getAll().then(function(result){
                        $scope.sectorsList = result.data.data;
                    });
                    if(item!=undefined){
                        $scope.category.assessment_question_category_name = item.assessment_question_category_name;
                        $scope.category.id_assessment_question_category = item.id_assessment_question_category;
                        if(item.sector_id!=null&&item.sector_id!=undefined){
                            var strVale = item.sector_id;
                            $scope.category.sector_id = strVale.split(',');
                        }

                    }

                    $scope.save = function(category){
                        if($scope.form.$valid){
                            category.company_id = $rootScope.id_company;
                            category.assessment_question_type_id = 5;
                            category.sector_id = category.sector_id.toString();
                            category.collateral_type_id = $rootScope.currentCollateralId;
                            companyService.assessmentQuestion.category.post(category).then(function(result){
                                if(result.status){
                                    $scope.getAssessmentQuestion();
                                    $rootScope.toast('Success',result.message);
                                    $scope.cancel();
                                } else{
                                    $rootScope.toast('Error',result.error,'error');
                                }
                            })

                        }
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function($data){

            },function(){
                $scope.getAssessmentQuestion();
            });
        }

    })



