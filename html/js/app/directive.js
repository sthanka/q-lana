
angular.module('app')
    .directive("multiSelect",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                jQuery.each(jQuery("select[multiple='multiple']"),function(i,o){
                    jQuery(o).multipleSelect({
                        position:jQuery(o).attr('position'),
                        placeholder:jQuery(o).attr('text'),
                        single:jQuery(o).attr('single')==='true' ? true : false,
                        filter:jQuery(o).attr('filter')==='true' ? true : false,
                        selectAll:jQuery(o).attr('selectAll')==='true' ? true : false
                    });
                });
            }
        }
    }])

    .directive("datePicker",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                jQuery('#appointment').datetimepicker({
                    format:'DD/MM/YYYY',
                    //pickTime: false,
                    icons:{
                        time:"fa fa-clock-o",
                        date:"fa fa-calendar",
                        up:"fa fa-arrow-up",
                        down:"fa fa-arrow-down"
                    }
                });
            }
        }
    }])

    .directive("donutGraph",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                jQuery('#loansPerformance').highcharts({
                    chart:{
                        type:'pie',
                        options3d:{
                            enabled:true,
                            alpha:0
                        }
                    },
                    credits:{
                        enabled:false
                    },
                    title:{
                        text:"Total 5",
                        verticalAlign:'middle',
                        y:2,
                        x:-110
                    },
                    subtitle:{
                        text:null
                    },
                    plotOptions:{
                        pie:{
                            innerSize:110,
                            depth:0
                        },
                        showInLegend:true,
                        dataLabels:{
                            enabled:false,
                            formatter:function(){
                                return this.percentage.toFixed(2)+'%';
                            }
                        }
                    },
                    legend:{
                        enabled:true,
                        layout:'vertical',
                        align:'right',
                        width:200,
                        verticalAlign:'middle',
                        useHTML:true,
                        labelFormatter:function(){
                            return '<div style="line-height:15px;margin-bottom:5px;"><b>'+this.y+'</b>&nbsp;'+this.name+'</div>';
                        }
                    },
                    series:[{
                        name:'Delivered amount',
                        data:[
                            ['Loans Payment',3],
                            ['New Loans',1],
                            ['Pending Loans',1],
                            ['Cancelled Loans',1]
                        ],
                        showInLegend:true,
                        dataLabels:{
                            enabled:false
                        }
                    }]
                });
            }
        }
    }])

    .directive("tabContent",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                element.find(".sub-list li:first-child").trigger('click');
                $('div.handsontable').handsontable('render');
                jQuery(".tabs li").on('click',function(e){

                    e.preventDefault();

                    if(!jQuery(this).hasClass('show')){
                        e.stopImmediatePropagation();
                    }
                    else{
                        /*jQuery(".tabs li").removeClass("current-tab");
                         jQuery(this).addClass("current-tab");*/
                        jQuery(".tab-data, .sub-list").hide();
                        var x = jQuery(this).find("a").attr("data-tabs"),
                            y = jQuery(this).find(".sub-list"); //Find the rel attribute value to identify the active tab + content
                        //$('data-tabs').animate({scrollTop:$(x).position().top}, 'slow');
                        jQuery(x).fadeIn();
                        jQuery(y).show();
                        jQuery(y).find('li:first-child').trigger('click');
                        return false;
                    }
                });
            }
        }
    }])

    .directive("tabNext",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                jQuery('.next-btn').on('click',function(e){
                    e.preventDefault();
                    //toastr();
                    jQuery(".status-list li.status-active").next().addClass("status-active")
                    jQuery('.current-tab').next().addClass('show');
                    jQuery(".tabs .show.current-tab").next().trigger("click");
                    jQuery(".tabs .show.current-tab").prev().addClass("active-tab");
                });
            }
        }
    }])

    .directive("starRating",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                //Star Rating
                jQuery.each(jQuery('.rate-star > span'),function(ind,elem){
                    jQuery(elem).on('click',function(){
                        jQuery(this).closest('.rate-star').find('span > i.fa').css('color','#ccc');
                        jQuery(this).find('i.fa').css('color','#FBCD45');
                        jQuery.each(jQuery(this).prevAll('span'),function(i,o){
                            jQuery(o).find('i.fa').css('color','#FBCD45');
                        });
                    });
                });
                jQuery.each(jQuery('.rate-star > span'),function(i,o){
                    if(jQuery(o).hasClass('active')){
                        jQuery(o).find('i.fa').css('color','#FBCD45');
                        jQuery(o).prevAll('span').find('i.fa').css('color','#FBCD45');
                    }
                });
            }
        }
    }])

    .directive("selectedAlpha",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                jQuery('.alpha-list > ul > li:eq(0)').addClass('selected-alpha');
                jQuery('.alpha-list > ul > li').on('click',function(){
                    jQuery('.alpha-list > ul').find('.selected-alpha').removeClass('selected-alpha');
                    jQuery(this).addClass('selected-alpha');
                });
            }
        }
    }])

    .directive("selectedResult",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                jQuery('.selected-result-list li:eq(0)').addClass('selected-result');
                jQuery('.selected-result-list li').on('click',function(){
                    jQuery('.selected-result-list li').removeClass('selected-result');
                    jQuery(this).addClass('selected-result');
                });
            }
        }
    }])

    .directive("toggleCheckbox",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                jQuery.each(jQuery('body').find('.toggle'),function(i,o){
                    jQuery(o).toggles({on:true});
                });
            }
        }
    }])

    .directive("searchBox",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                $('.search-box span').on('click',function(){
                    $('.search-box').toggleClass('expand');
                    $(this).closest('.search-box').find('input').focus();
                })

            }
        }
    }])


    .directive("tabData",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                $.each($('body').find('.custom-tabs'),function(i,o){
                    $(o).find('li').removeClass('active');
                    $(o).next('[data-area="tab"]').find('[data-content="tab"]').hide();
                    $(o).next('[data-area="tab"]').find('[data-content="tab"]:eq(0)').fadeIn('fast');
                    $(o).find('a[data-tag="tab"]:eq(0)').parent('li').addClass('active');
                    $(o).find('a[data-tag="tab"]').on('click',function(){
                        $(o).find('li').removeClass('active');
                        $(o).next('[data-area="tab"]').find('[data-content="tab"]').hide();
                        $(this).parent('li').addClass('active');
                        $(o).next('[data-area="tab"]').find('#'+$(this).attr('data-select')).fadeIn('fast');
                    });
                });
            }
        }
    }])

    .directive('activeLink',['$location',function(location){
        return {
            restrict:'A',
            link:function(scope,element,attrs,controller){
                //console.log(location.path());
                var clazz = attrs.activeLink;
                var path = attrs.href;

                if(path!=undefined){
                    path = decodeURIComponent(path.substring(1));
                } else{
                    path = '#';

                }//hack because path does not return including hashbang
                scope.location = location;
                scope.$watch('location.path()',function(newPath){
                    var url = attrs.aliasUrl;
                    console.log('url',url);

                    if(path===newPath){
                        element.parent().addClass(clazz);
                    } else{
                        element.parent().removeClass(clazz);
                    }
                });
            }
        };
    }])

    .directive('httpResponse',['$http','$rootScope','$timeout',function($http,$rootScope,$timeout){
        return {
            restrict:'A',
            link:function(scope,elm,attrs){
                scope.isLoading = function(){
                    return $http.pendingRequests.length>0;
                };

                scope.$watch(scope.isLoading,function(v){
                    //  console.log('v', v);
                    if(v){
                        $(elm).fadeIn();
                    } else{
                        $timeout(function(){
                            $(elm).fadeOut(function(){
                                $rootScope.loaderOverlay = true;
                            });
                        },600);

                    }
                });
            }
        };

    }])

    /*.directive("contentArea", function () {
     return {
     restrict: "C",
     link: function (scope, element, attrs) {
     element.height($(window).height()-51);

     }
     }
     })*/
    /*.directive('resize', function ($window) {
     return function (scope, element) {
     scope.$watch(function (newValue, oldValue) {
     alert();
     jQuery('#contentArea').height($(window).height() - 51);
     }, true);
     }
     })*/
    /*    .directive('resize', function ($window) {
     return function (scope, element) {
     scope.$watch(function (newValue, oldValue) {
     // alert();
     jQuery('.content-container').height($window.height() - 119);
     }, true);
     }
     })*/
    /*.directive("contentContainer", function ($window) {
     return {
     restrict: "C",
     link: function (scope, element, attrs,$window) {
     element.height($window.height() - 119);
     }
     }
     })*/
    /*.directive('resize', function ($window) {
     return function (scope, element) {
     var w = angular.element($window);
     scope.getWindowDimensions = function () {
     return {
     'h': w.height(),
     'w': w.width()
     };
     };
     scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
     //alert();
     element.height($window.height() - 119);

     }, true);
     }
     })*/
    //Sub Menu
    .directive("openSubmenu",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                element.addClass('active in');
                jQuery('ul#menu > li').on('click',function(){
                    jQuery('ul#menu > li').removeClass('active');
                    jQuery(this).addClass('active');
                    jQuery(this).addClass('in');
                });


            }
        }
    }])

    .directive("removeSubmenu",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                jQuery('ul#menu').on('mouseleave',function(){
                    //jQuery('ul#menu li').removeClass('active');
                });
            }
        }
    }])

    .directive("listActive",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                jQuery('ul.groups-list li').on('click',function(){
                    jQuery(this).closest('ul.groups-list').find('li').removeClass('list-active');
                    jQuery(this).addClass('list-active');
                });
            }
        }
    }])

    .directive('dropDown',function($compile){
        return {

            restrict:'E',

            scope:{
                user:'=user',
                branchs:'=branchs'
            },

            controller:function($scope){
                $scope.branchsList = $scope.branchs;
                $scope.brachType = {};
                $scope.children = {};
                $scope.addChild = function(child){
                    var index = $scope.user.children.length;
                    $scope.children.push({
                        "parent":$scope.user,
                        "children":[],
                        "index":index
                    });
                }

                $scope.remove = function(){
                    if($scope.user.parent){
                        var parent = $scope.user.parent;
                        var index = parent.children.indexOf($scope.user);
                        parent.children.splice(index,1);
                    }
                }
            },

            templateUrl:'partials/admin/dropdown.html',

            link:function($scope,$element,$attrs){

            },

            compile:function(tElement,tAttr){
                var contents = tElement.contents().remove();
                var compiledContents;
                return function(scope,iElement,iAttr){
                    if(!compiledContents){
                        compiledContents = $compile(contents);
                    }
                    compiledContents(scope,function(clone,scope){
                        iElement.append(clone);
                    });
                };
            }
        };
    })

    .directive("bulidSelect",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                element.on('change',function(){
                    element.closest('.level').find('.ok-btn').attr('data-level',$(element).find(':selected').text());
                });
            }
        }
    }])

    .directive("buildLevel",[function($compile){
        return {
            //restrict: "A",
            template:'<span>TEST'// + attr + ''
            +'<div class="others-menu-wrap small-menu right-aligned" data-actions="menu">'
            +'<a href="javascript:;" class="others-menu mt5" onclick="showActionsMenu(this)">'
            +'<img src="images/others.png" alt="others-image" class="h13" />'
            +'</a>'
            +'<div class="others-menu-list">'
            +'<div class="clearfix ptrl5">'
            +'<h2 class="pull-left f13 gray mt5">'
            +'<a class="others-menu" href="javascript:;"  onclick="showActionsMenu(this)">'
            +'<img alt="others-image" src="images/others.png" class="h13">'
            +'</a>'
            +'Actions</h2>'
            +'</div>'
            +'<ul>'
            +'<li><a onclick="editLevel(this);">Edit</a></li>'
            +'<li><a onclick="deleteLevel(this);">Delete</a></li>'
            +'</ul>'
            +'</div>'
            +'</div>'
            +'</span>'
            +'<div class="level">'
            +'<select build-select>'
            +'<option ng-repeat="branch in branchList">{{branch.branch_type_name}}</option>'
            +'</select>'
            +'<a class="ok-btn" build-level><i class="fa fa-check"></i></a>'
            +'</div>',
            link:function(scope,element,attr){
                scope.active = false;
                element.on('click',function(){

                    // var lh = $compile(_levelHTML)($scope);
                    // element.closest('.level').html(_levelHTML);
                    scope.$apply(function(){
                        scope.active = !scope.active;
                    });
                });
            }
        }
    }])

    .directive("accordianContent",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                $(".accordion_head").click(function(){
                    if($('.accordion_body').is(':visible')){
                        $(this).parent('div').find($(".accordion_body")).slideUp(300);
                        /*$(".plusminus").text('+').removeClass("minustext");
                         $(".accordion-header").removeClass("acc-expand")*/
                    }
                    if($(this).next(".accordion_body").is(':visible')){
                        $(this).next(".accordion_body").slideUp(300);
                        /*$(this).children(".plusminus").text('+').removeClass("minustext");
                         $(this).removeClass("acc-expand");*/
                    } else{
                        $(this).next(".accordion_body").slideDown(300);
                        /*$(this).children(".plusminus").text('-').addClass("minustext");
                         $(this).addClass("acc-expand");*/
                    }
                });
            }
        }
    }])

    .directive("accordianData",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                $(".accordion_head").click(function(){
                    $(".accordion_head").removeClass("acc-expand");
                    $(this).addClass("acc-expand");
                    if($('.accordion_body').is(':visible')){
                        $(this).parent('div').find($(".accordion_body")).slideUp(300);
                        /*$(".plusminus").text('+').removeClass("minustext");*/

                    }
                    if($(this).next(".accordion_body").is(':visible')){
                        $(this).next(".accordion_body").slideUp(300);
                        $(".accordion_head").removeClass("acc-expand");
                        /*$(this).children(".plusminus").text('+').removeClass("minustext");*/
                        //$(this).removeClass("acc-expand");
                    } else{
                        $(this).next(".accordion_body").slideDown(300);
                        /*$(this).children(".plusminus").text('-').addClass("minustext");*/
                        //$(this).addClass("acc-expand");
                    }
                });
            }
        }
    }])

    .directive('pdfDownload',function(){
        return {
            restrict:'E',
            templateUrl:'http://desk8-tss/q-lana/rest/Classes/company_branches.xls',
            scope:true,
            link:function(scope,element,attr){
                var anchor = element.children()[0];

                // When the download starts, disable the link
                scope.$on('download-start',function(){
                    $(anchor).attr('disabled','disabled');
                });

                // When the download finishes, attach the data to the link. Enable the link and change its appearance.
                scope.$on('downloaded',function(event,data){
                    $(anchor).attr({
                            href:'data:application/pdf;base64,'+data,
                            download:attr.filename
                        })
                        .removeAttr('disabled')
                        .text('Save')
                        .removeClass('btn-primary')
                        .addClass('btn-success');

                    // Also overwrite the download pdf function to do nothing.
                    scope.downloadPdf = function(){
                    };
                });
            },
            controller:['$scope','$attrs','$http',function($scope,$attrs,$http){
                $scope.downloadPdf = function(){
                    $scope.$emit('download-start');
                    $http.get($attrs.url).then(function(response){
                        $scope.$emit('downloaded',response.data);
                    });
                };
            }]
        }
    })

    .directive("dropdownMenu",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){

            }
        }
    }])

    .directive('resizeWindow',function($window){
        return {
            link:function(scope,elem,attrs){
                scope.onResize = function(){
                    $('.col-fixed-width').height($(window).height()-195);
                    $('.col-fixed-width ul.liPadLeft0').height($(window).height()-235);
                }
                scope.onResize();

                angular.element($window).bind('resize',function(){
                    scope.onResize();
                })
            }
        }
    })

    .directive('colsHorizontal',['$window',function($window){
        return {
            link:function(scope,elem,attrs){
                scope.onResize = function(){
                    $('.horizontal-div-for-scroll').width($(window).width()-80);
                    $('.custom-width').width($(window).width()-100);
                    $('.custom-width-new').width($(window).width()-$('.left-menu').width()-30);
                    $('.h-width').width($(window).width()-230);
                    $('.horizontal-div-for-scroll').mCustomScrollbar({
                        theme:"minimal-dark",
                        horizontalScroll:true,
                        scrollButtons:{enable:true},
                        updateOnContentResize:true,
                        axis:"x",
                        scrollbarPosition:"outside",
                        autoHideScrollbar:true,
                        autoExpandScrollbar:true,
                        advanced:{
                            autoExpandHorizontalScroll:true
                        }
                    });
                }
                scope.onResize();

                angular.element($window).bind('resize',function(){
                    scope.onResize();
                })
            }
        }
    }])

    .directive('minScroll',['$document','$window','$timeout','$interval',function($document,$window,$timeout,$interval){
        return {
            link:function(scope,elem,attrs){
                scope.onResize = function(){
                    $timeout(function(){
                        var top_pos = elem.offset().top;
                        var scroll_height = $window.innerHeight-top_pos;
                        elem.css('height',scroll_height);
                        $('.scroll-div-wrap').mCustomScrollbar({
                            theme:"minimal-dark",
                            scrollButtons:{enable:true},
                            updateOnContentResize:true,
                            axis:"y",
                            scrollbarPosition:"outside",
                            autoHideScrollbar:false,
                            autoExpandScrollbar:false
                        });

                    },500)
                }
                $interval(function(){
                    scope.onResize();
                },100);
                scope.onResize();
                angular.element($window).bind('resize',function(){
                    scope.onResize();
                })

            }
        }
    }])
    //parent-scroll
    .directive('parentScroll',['$document','$window','$timeout','$interval',function($document,$window,$timeout,$interval){
        return {
            link:function(scope,elem,attrs){
                scope.onResize = function(){
                    $timeout(function(){
                        var top_pos = elem.offset().top;
                        var scroll_height = elem.closest('.parent').height()-100;
                        $('.scroll-div-wrap-parent').css('min-height',$window.innerHeight-top_pos);
                        $('.scroll-div-wrap-parent').css('height',scroll_height);
                        $('.scroll-div-wrap-parent').mCustomScrollbar({
                            theme:"minimal-dark",
                            scrollButtons:{enable:true},
                            updateOnContentResize:true,
                            axis:"y",
                            scrollbarPosition:"outside",
                            autoHideScrollbar:false,
                            autoExpandScrollbar:false
                        });

                    },500)
                }

                scope.onResize();
                angular.element($window).bind('resize',function(){
                    scope.onResize();
                })

            }
        }
    }])

    .directive('winHeight',['$document','$window','$timeout',function($document,$window,$timeout){
        return {
            link:function(scope,elem,attrs){
                scope.onResize = function(){
                    $timeout(function(){
                        var top_pos = elem.offset().top;
                        var scroll_height = $window.innerHeight-top_pos;
                        elem.css('height',scroll_height);
                    });
                }
                scope.onResize();
                angular.element($window).bind('resize',function(){
                    scope.onResize();
                })
            }
        }
    }])
    .directive('docHeight',['$document','$window','$timeout','$interval',function($document,$window,$timeout,$interval){
        return {
            link:function(scope,elem){
                scope.onResize = function(){
                        var top_pos = elem.offset().top;
                        var scroll_height = $document.height()-top_pos;
                        elem.css('height',scroll_height);
                        $('.scroll-div').mCustomScrollbar({
                            theme:"minimal-dark",
                            scrollButtons:{enable:true},
                            updateOnContentResize:true,
                            axis:"y",
                            scrollbarPosition:"outside",
                            autoHideScrollbar:false,
                            autoExpandScrollbar:false
                        });

                }
                /*$interval(function(){
                    scope.onResize();
                },500);*/
                scope.onResize();

                angular.element($window).bind('resize',function(){
                    scope.onResize();
                });


            }
        }
    }])


    .directive('tableHorizontal',['$window','$interval',function($window,$interval){
        return {
            link:function(scope,elem,attrs){
                scope.onResize = function(){
                    $(".table-scroll").css('max-width',$(window).width()-280);
                    $(".table-scroll-new").css('max-width',$(window).width()-180);

                    $(".table-scroll-div").css('max-width',$(window).width()-400);
                    $(".c-hscroll").css('max-width',$('.page-left-wrap').width()-$('.t-str-view .table-fixed td').width()-30);
                    /*var max_width=$('.table-scroll').find('th').length*200;
                     $(".table-scroll").css('width',max_width);*/
                    $('.table-scroll,.table-scroll-new,.table-scroll-div,.c-hscroll').mCustomScrollbar({
                        theme:"minimal-dark",
                        horizontalScroll:true,
                        scrollButtons:{enable:true},
                        updateOnContentResize:true,
                        axis:"x",
                        scrollbarPosition:"outside",
                        autoHideScrollbar:true,
                        autoExpandScrollbar:true,
                        advanced:{
                            autoExpandHorizontalScroll:true
                        }
                    });
                }
                $interval(function(){
                    scope.onResize();
                },500);

                    scope.onResize();
                angular.element($window).bind('resize',function(){
                    scope.onResize();
                })
            }
        }
    }])

    .directive('onlyDigits',function(){
        return {
            require:'ngModel',
            restrict:'A',
            link:function(scope,element,attr,ctrl){
                function inputValue(val){
                    if(val){
                        var digits = val.replace(/[^0-9]/g,'');

                        if(digits!==val){
                            ctrl.$setViewValue(digits);
                            ctrl.$render();
                        }
                        return parseInt(digits,10);
                    }
                    return undefined;
                }

                ctrl.$parsers.push(inputValue);
            }
        };
    })

    .directive('resultHeight',function($window){
        return {

            link:function(scope,elem,attrs){
                scope.onResize = function(){
                    elem.height($window.innerHeight-150);
                    elem.mCustomScrollbar({
                        autoHideScrollbar:true
                    });
                }
                scope.onResize();

                angular.element($window).bind('resize',function(){
                    scope.onResize();
                })
            }
        }
    })

    .directive('addfieldsRow',function(){
        return {
            restrict:"A",
            link:function(scope,elem,attrs){
                elem.on('click',function(){
                    elem.closest('tbody').find('.added-row').slideDown();
                });
            }
        }
    })

    .directive('addRow',function(){
        return {
            restrict:"A",
            link:function(scope,elem,attrs){
                var _rowAdd = '';
                elem.on('click',function(){
                    var _type = elem.closest('tr').find('input[name="type"]').val(),
                        _amount = elem.closest('tr').find('input[name="amount"]').val();
                    var _rowAdd = '<tr>'
                        +'<td name="type">'+_type+'</td>'
                        +'<td name="amount">'+_amount+'</td>'
                        +'<td>'
                        +'<a class="custom-color-btn" edit-row><i class="fa fa-pencil"></i></a>'
                        +'</td></tr>'
                    elem.closest('tbody').append(_rowAdd);
                    elem.closest('tr').find('input').val('');
                    elem.closest('tbody').find('.added-row').hide();
                });
            }
        }
    })

    .directive('cancelRow',function(){
        return {
            restrict:"A",
            link:function(scope,elem,attrs){
                elem.on('click',function(){
                    elem.closest('tr').find('input').val('');
                    elem.closest('tbody').find('.added-row').slideUp();
                });
            }
        }
    })
    /* .directive('editRow', function () {
     return {
     restrict: "A",
     link: function(scope, elem, attrs) {
     var _rowAdd = '';
     elem.on('click', function(){
     var _type = elem.closest('tr').find('td[name="type"]').val(),
     _amount = elem.closest('tr').find('td[name="amount"]').val();
     var _editAdd =  '<tr>'
     +'<td><input type="text" value="'+_type+'" /></td>'
     +'<td><input type="text" value="'+_amount+'" /></td>'
     +'<td>'
     +'<a class="custom-color-btn" save-edit><i class="fa fa-check"></i></a>'
     +'<a class="custom-color-btn" cancel-edit><i class="fa fa-check"></i></a>'
     +'</td></tr>'
     elem.closest('tr').html(_editAdd);
     elem.closest('tr').find('input').val('');
     elem.closest('tbody').find('.added-row').slideUp();
     });
     }
     }
     })
     .directive('saveEdit', function () {
     return {
     restrict: "A",
     link: function(scope, elem, attrs) {
     var _rowAdd = '';
     elem.on('click', function(){
     var _type = elem.closest('tr').find('input[name="type"]').val(),
     _amount = elem.closest('tr').find('input[name="amount"]').val();
     var _editAdd =  '<tr>'
     +'<td>'+_type+'</td>'
     +'<td>'+_amount+'</td>'
     +'<td>'
     +'<a class="custom-color-btn" edit-row><i class="fa fa-check"></i></a>'
     +'</td></tr>'
     elem.closest('tr').html(_editAdd);
     elem.closest('tr').find('input').val('');
     elem.closest('tbody').find('.added-row').slideUp();
     });
     }
     }
     })*/
    .directive("accordianTabbed",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                element.click(function(){

                    $('.tab-data').find('div.acc_wrap').removeClass('open');
                    $('.tab-data').find('div.acc_wrap').find('.accordion_body').slideUp();
                    $(this).closest('div.acc_wrap').find('.accordion_body').slideDown();

                })

            }
        }
    }])

    .directive('triggerAccord',function($window){
        return {
            restrict:"A",
            link:function(scope,elem,attrs){
                elem.closest('ul').find('li:first-child').addClass('active');
                elem.on('click',function($event){
                    $event.stopPropagation();
                    elem.closest('ul').find('li').removeClass('active');
                    elem.addClass('active');
                    var _acc = elem.attr('data-link');
                    $('.content-container').find('.accordion_head[data-head="'+_acc+'"]').trigger('click');
                });
            }
        }
    })

    .directive('rightModal',function($window){
        return {
            restrict:"A",
            link:function(scope,elem,attrs){
                elem.click(function(){
                    var _modal = attrs.modal,
                        _rightModalOverlay = '<div id="right-overlay"></div>';
                    $(window).find('#right-overlay').remove();
                    $('[data-modal="'+_modal+'"]').addClass('shown');
                    $('body').addClass('overflow-hidden');
                    $('[data-modal="'+_modal+'"] .right-modal-body').height($(window).height()-41)
                    $(_rightModalOverlay).insertAfter($('[data-modal="'+_modal+'"]'));
                });
            }
        }
    })

    .directive('closeRight',function($window){
        return {
            restrict:"A",
            link:function(scope,elem,attrs){
                elem.click(function(){
                    elem.closest('.right-modal').removeClass('shown');
                    $('#right-overlay').remove();
                    $('body').removeClass('overflow-hidden');
                });
            }
        }
    })

    .directive("fixedTop",function($window){
        return function(scope,element,attrs){
            angular.element($window).bind("scroll",function(){
                if(this.pageYOffset>=70){
                    scope.boolChangeClass = true;
                } else{
                    scope.boolChangeClass = false;
                }
                scope.$apply();
            });
        };
    })

    .directive("firstAlpha",function(){
        return {
            restrict:"A",
            link:function(scope,elem,attrs){
                //console.log('elem, attrs',elem,attrs);
//                var randomColor = Math.floor(Math.random() * 16777215).toString(16);
                //backgroundColor: '#' + randomColor

                elem.text($.trim(attrs.firstAlpha.charAt(0).toUpperCase()));
                var colorArray = ['#f27620','#76ad50','#4f78c8','#a75cd3','#42acc6',
                    '#989898','#75579C','#A02783','#3a27ab','#856B07',
                    '#48463F','#4D5F9F','#173631','#0D3A6A','#009ABC','#FF2D81','#DE5D83','#8E7F06',
                    '#623E34','#233B6D','#807F06','#028F67','#822b4e','#584a38','#012129','#62619c',
                    '#136287','#305203','#961900','#efb400','#acad00','#22baf1'];

                if(attrs.backroundColor==undefined){
                    elem.css({
                        backgroundColor:colorArray[Math.floor((Math.random()*colorArray.length))]
                    });
                }

            }
        }
    })

    .directive("bkColor",function(){
        return {
            restrict:"A",
            link:function(scope,elem,attrs){
                var str = attrs.bkColor;
                if(scope.setColor==undefined){
                    scope.setColor = [];
                }

                var randomColor = Math.floor(Math.random()*16777215).toString(16);
                var colors = ['#f27620','#76ad50','#4f78c8','#a75cd3','#42acc6',
                    '#989898','#75579C','#A02783','#3a27ab','#856B07',
                    '#48463F','#4D5F9F','#173631','#0D3A6A','#009ABC','#FF2D81','#DE5D83','#8E7F06',
                    '#623E34','#233B6D','#807F06','#028F67','#822b4e','#584a38','#012129','#62619c','#136287','#305203','#961900'];


                if(colors.length>scope.setColor.length){
                    var colorCode = colors[scope.setColor.length];
                } else{
                    var colorCode = '#'+randomColor;
                }
                var a = _.findWhere(scope.setColor,{'id':str});

                if(a){
                    var _color = a.code;
                } else{
                    var _color = colorCode;
                    scope.setColor.push({'code':colorCode,'id':str});
                }
                elem.css({
                    backgroundColor:_color
                });
            }
        }
    })
    .directive("textColor",function(){
        return {
            restrict:"A",
            link:function(scope,elem,attrs){
                var str = attrs.textColor;
                if(scope.setColor==undefined){
                    scope.setColor = [];
                }

                var randomColor = Math.floor(Math.random()*16777215).toString(16);
                var colors = ['#f27620','#76ad50','#4f78c8','#a75cd3','#42acc6',
                    '#989898','#75579C','#A02783','#3a27ab','#856B07',
                    '#48463F','#4D5F9F','#173631','#0D3A6A','#009ABC','#FF2D81','#DE5D83','#8E7F06',
                    '#623E34','#233B6D','#807F06','#028F67','#822b4e','#584a38','#012129','#62619c','#136287','#305203','#961900'];

                if(colors.length>scope.setColor.length){
                    var colorCode = colors[scope.setColor.length];
                } else{
                    var colorCode = '#'+randomColor;
                }
                var a = _.findWhere(scope.setColor,{'id':str});

                if(a){
                    var _color = a.code;
                } else{
                    var _color = colorCode;
                    scope.setColor.push({'code':colorCode,'id':str});
                }
                elem.css({
                    color:_color
                });
            }
        }
    })

    .directive('uploadFile',function(){
        return {
            restrict:"A",
            link:function(scope,elem,attrs){
                switch(attrs.filetype){
                    case 'image/jpeg':
                        elem.find('i').addClass('jpg-icon');
                        break;
                    case 'image/jpg':
                        elem.find('i').addClass('jpg-icon');
                        break;
                    case 'image/png':
                        elem.find('i').addClass('png-icon');
                        break;
                    case 'application/pdf':
                        elem.find('i').addClass('pdf-icon');
                        break;
                    case 'application/octet-stream':
                        elem.find('i').addClass('zip-icon');
                        break;
                    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                        elem.find('i').addClass('word-icon');
                        break;
                    default:
                        elem.find('i').addClass('default-file-icon');
                        break;
                }
            }
        }
    })

    .filter('getFileName',function(){
        return function(value,fileName){
            return fileName.replace(/\.[^/.]+$/,"");
        };
    })

    .directive('touchPoints',function(){
        return {
            restrict:"A",
            link:function(scope,elem,attrs){
                if(attrs.isreminder==1){
                    //elem.parent().parent().addClass('light-orange-bg light-o-bg');
                }
                switch(attrs.tauchpoint){
                    case 'Mail':
                        elem.find('i').addClass('fa fa-envelope');
                        break;
                    case 'Call':
                        elem.find('i').addClass('fa fa-phone');
                        break;
                    case 'Chat':
                        elem.find('i').addClass('fa fa-comments');
                        break;
                    case 'Message':
                        elem.find('i').addClass('fa fa-mobile f18');
                        break;
                    case 'Note':
                        elem.find('i').addClass('fa fa-file-text-o');
                        break;
                }
            }
        }
    })
    .directive('knowledgeIcons',function(){
        return {
            restrict:"A",
            link:function(scope,elem,attrs){
                switch(attrs.image){
                    case 'document':
                        elem.html('<span class="icon-file"></span>');
                        break;
                    case 'blog':
                        elem.html('<span class="icon-clipped-file"></span>');
                        break;
                    case 'news':
                        elem.html('<span class="icon-workload"></span>');
                        break;
                    case 'video':
                        elem.html('<span class="icon-play"></span>');
                        break;
                    case 'Total':
                        elem.html('<span class=" icon-harddrive1"></span>');
                        break;
                }
            }
        }
    })
    .directive('qtypeIcons',function(){
        return {
            restrict:"A",
            link:function(scope,elem,attrs){
                switch(attrs.qtype){
                    case 'text':
                        elem.html('<i class="icon-input-text mt4 w25"></i>');
                        break;
                    case 'textarea':
                        elem.html('<i class="icon-input-text mt4 w25"></i>');
                        break;
                    case 'radio':
                        elem.html('<i class="icon-yes-no mt4 w25"></i>');
                        break;
                    case 'date':
                        elem.html('<i class="icon-calendar1 mt4 w25"></i>');
                        break;
                    case 'dropdown':
                        elem.html('<i class="icon-list mt4 w25"></i>');
                        break;
                    case 'file':
                        elem.html('<i class="icon-cloud-export mt4 w25"></i>');
                        break;
                    case 'checkbox':
                        elem.html('<i class="icon-right-circle mt4 w25"></i>');
                        break;
                }
            }
        }
    })

    .directive('taskStatusBg',function(){
        return {
            restrict:"A",
            link:function(scope,elem,attrs){
                switch(attrs.bgcolor){
                    case 'completed':
                        elem.addClass('btn-success');
                        break;
                    case 'pending':
                        elem.addClass('btn-warning');
                        break;
                    case 'open':
                        elem.addClass('btn-primary');
                        break;
                    case 'cancel':
                        elem.addClass('btn-danger');
                        break;
                    case 'cancelled':
                        elem.addClass('btn-danger');
                        break;

                }
            }
        }
    })

    .directive('approvals',function(){
        return {
            restrict:"A",
            scope:true,
            link:function(scope,elem,attrs){
                switch(attrs.approvaltype){
                    case 'meeting':
                        var id = attrs.approvalid;
                        elem.find('.icon-btn').addClass('btn-primary');
                        elem.find('.icon-btn i').addClass('fa fa-users');
                        elem.find('.btn-box').addClass('blue-status');
                        elem.find('.template').bind('click',function(){
                            scope.meetingModal(id)
                        });
                        break;
                    case 'task':
                        var id = attrs.approvalid;
                        elem.find('.template').bind('click',function(){
                            scope.addTasksModal(id)
                        });
                        elem.find('.icon-btn').addClass('btn-info');
                        elem.find('.icon-btn i').addClass('fa fa-tasks');
                        elem.find('.btn-box').addClass('light-blue-status');
                        break;
                    case 'approval':
                        elem.find('.icon-btn').addClass('btn-success');
                        elem.find('.icon-btn i').addClass('fa fa-check');
                        elem.find('.btn-box').addClass('green-status');
                        break;
                    case 'file':
                        elem.bind('click',function(){
                            scope.quickAttachmentModal()
                        });
                        elem.find('.icon-btn').addClass('lt-bg');
                        elem.find('.icon-btn i').addClass('fa fa-file');
                        elem.find('.btn-box').addClass('light-grey-status');
                        break;
                }
            }
        }
    })

    .directive('ngEnter',function(){
        return function(scope,element,attrs){
            element.bind("keydown",function(e){
                if(e.which===13){
                    scope.$apply(function(){
                        scope.$eval(attrs.ngEnter,{'e':e});
                    });
                    e.preventDefault();
                }
            });
        };
    })
    .filter('split',function(){
        return function(input,splitChar,splitIndex){
            // do some bounds checking here to ensure it has that index
            if(input!=undefined){
                return input.split(splitChar)[splitIndex];
            }
        }
    })

    // hierarchical show
    .directive('hierarchicalShow',[
        '$timeout',
        '$rootScope',
        function($timeout,$rootScope){
            return {
                restrict:'A',
                scope:true,
                link:function(scope,elem,attrs){


                    var parent_el = $(elem),
                        baseDelay = 60;


                    var add_animation = function(children,length){
                        children
                            .each(function(index){
                                $(this).css({
                                    '-webkit-animation-delay':(index*baseDelay)+"ms",
                                    '-moz-animation-delay':(index*baseDelay)+"ms",
                                    '-o-animation-delay':(index*baseDelay)+"ms",
                                    '-ms-animation-delay':(index*baseDelay)+"ms",
                                    'animation-delay':(index*baseDelay)+"ms"
                                })
                            })
                            .end()
                            .waypoint({
                                element:elem[0],
                                handler:function(){
                                    parent_el.addClass('hierarchical_show_inView');
                                    setTimeout(function(){
                                        parent_el
                                            .removeClass('hierarchical_show hierarchical_show_inView fast_animation')
                                            .children()
                                            .css({
                                                '-webkit-animation-delay':'',
                                                '-moz-animation-delay':'',
                                                '-o-animation-delay':'',
                                                '-ms-animation-delay':'',
                                                'animation-delay':''
                                            });
                                    },(length*baseDelay)+1200);
                                    this.destroy();
                                },
                                context:window,
                                offset:'90%'
                            });
                    };

                    $rootScope.$watch('pageLoaded',function(){
                        if($rootScope.pageLoaded){
                            var children = parent_el.children(),
                                children_length = children.length;

                            $timeout(function(){
                                add_animation(children,children_length)
                            },560)

                        }
                    });

                }
            }
        }
    ])

    .directive('hierarchicalSlide',[
        '$timeout',
        '$rootScope',
        function($timeout,$rootScope){
            return {
                restrict:'A',
                scope:true,
                link:function(scope,elem,attrs){

                    var $this = $(elem),
                        baseDelay = 100;

                    var add_animation = function(children,context,childrenLength){

                        children.each(function(index){
                            $(this).css({
                                '-webkit-animation-delay':(index*baseDelay)+"ms",
                                'animation-delay':(index*baseDelay)+"ms"
                            })
                        });
                        $this.waypoint({
                            handler:function(){
                                $this.addClass('hierarchical_slide_inView');
                                $timeout(function(){
                                    $this.removeClass('hierarchical_slide hierarchical_slide_inView');
                                    children.css({
                                        '-webkit-animation-delay':'',
                                        'animation-delay':''
                                    });
                                },(childrenLength*baseDelay)+1200);
                                this.destroy();
                            },
                            context:context[0],
                            offset:'90%'
                        });
                    };

                    $rootScope.$watch('pageLoaded',function(){
                        if($rootScope.pageLoaded){
                            var thisChildren = attrs['slideChildren'] ? $this.children(attrs['slideChildren']) : $this.children(),
                                thisContext = attrs['slideContext'] ? $this.closest(attrs['slideContext']) : 'window',
                                thisChildrenLength = thisChildren.length;
                            if(thisChildrenLength>=0){

                                $timeout(function(){
                                    add_animation(thisChildren,thisContext,thisChildrenLength)
                                },560)
                            }
                        }
                    });

                }
            }
        }
    ])

    .directive("childCount",function(){
        return {
            restrict:"EA",
            template:'{{count}}',
            link:function(scope,elem,attrs){
                var tree = scope.data;
                scope.count = 0;
                treeStructure(tree);
                // console.log('tree', tree);
                function treeStructure(tree){
                    angular.forEach(tree.nodes,function(value,key){
                        scope.count++;
                        treeStructure(value);
                    });
                }
            }
        }
    })

    .directive("basicAccordian",[function(){
        return {
            restrict:"A",
            link:function(scope,element,attr){
                element.click(function(){
                    element.parent('.acc-head').toggleClass('acc-open');
                    element.parent().parent().toggleClass('racc-open');
                    element.parents('.acc-head').toggleClass('acc-open');
                    element.parentsUntil('.accordian-wrapper').next('.acc-body').slideToggle();
                })

            }
        }
    }])

    // coffeescript's for in loop

    .directive('fieldDirective',function($http,$compile){
        var __indexOf = [].indexOf||function(item){
                for(var i = 0,l = this.length; i<l; i++){
                    if(iinthis&&this[i]===item) return i;
                }
                return -1;
            };

        var getTemplateUrl = function(field){
            var type = field.field_type;
            var templateUrl = './partials/directive-templates/field/';
            var supported_fields = [
                'textfield',
                'email',
                'textarea',
                'checkbox',
                'date',
                'dropdown',
                'hidden',
                'password',
                'radio'
            ]

            if(__indexOf.call(supported_fields,type)>=0){
                return templateUrl += type+'.html';
            }
        }

        var linker = function(scope,element){
            // GET template content from path
            var templateUrl = getTemplateUrl(scope.field);
            $http.get(templateUrl).success(function(data){
                element.html(data);
                $compile(element.contents())(scope);
            });
        }

        return {
            template:'<div ng-bind="field"></div>',
            restrict:'E',
            scope:{
                field:'='
            },
            link:linker
        };
    })
    .directive("dynamicName",function($compile){
        return {
            restrict:"A",
            terminal:true,
            priority:1000,
            link:function(scope,element,attrs){
                element.attr('name',scope.$eval(attrs.dynamicName));
                element.removeAttr("dynamic-name");
                $compile(element)(scope);
            }
        }
    })
    .directive('cdTrueValue',[function(){
        return {
            restrict:'A',
            require:'ngModel',
            link:function(scope,element,attrs,ngModel){
                ngModel.$parsers.push(function(v){
                    return v ? scope.$eval(attrs.cdTrueValue) : scope.$eval(attrs.cdFalseValue);
                });
            }
        };
    }])

    .filter('split',function(){
        return function(input,splitChar,splitIndex){
            // do some bounds checking here to ensure it has that index
            if(input!=undefined){
                return input.split(splitChar)[splitIndex];
            }
        }
    })

    .filter('underscoreless',function(uppercaseFilter){
        return function(input){
            return uppercaseFilter(input.replace(/_/g,' '));
        };
    })
    .filter('underscoreadd',function(lowercaseFilter){
        return function(input){
            return lowercaseFilter(input.replace(/ /g,'_'));
        };
    })

    .directive('datetimepickerNeutralTimezone',function(){
        return {
            restrict:'A',
            priority:1,
            require:'ngModel',
            link:function(scope,element,attrs,ctrl){
                ctrl.$formatters.push(function(value){
                    var date = new Date(Date.parse(value));
                    date = new Date(date.getTime()+(60000*date.getTimezoneOffset()));
                    return date;
                });

                ctrl.$parsers.push(function(value){
                    var date = new Date(value.getTime()-(60000*value.getTimezoneOffset()));
                    return date;
                });
            }
        };
    })
    .directive('addIframe',function(){
        return {
            restrict:"A",
            link:function(scope,elem,attrs){
                elem.html('<iframe width="100%" height="415" src="'+attrs.iframesrc+'" frameborder="0" allowfullscreen></iframe>');
            }
        }
    })

    .directive('newtouchPoints',function(){
        return {
            restrict:"A",
            link:function(scope,elem,attrs){
                switch(attrs.tauchpoint){
                    case 'Mail':
                        elem.parent('.icons-div').addClass('sky-blue-bg');
                        break;
                    case 'Call':
                        elem.parent('.icons-div').addClass('btn-primary');
                        break;
                    case 'Chat':
                        elem.parent('.icons-div').addClass('btn-success');
                        break;
                    case 'Message':
                        elem.parent('.icons-div').addClass('btn-orange');
                        break;
                    case 'Note':
                        elem.parent('.icons-div').addClass('btn-dark');
                        break;
                }
            }
        }
    })
    .directive('appFilereader',function($q){
        /*
         made by elmerbulthuis@gmail.com WTFPL licensed
         */
        var slice = Array.prototype.slice;

        return {
            restrict:'A',
            require:'?ngModel',
            link:function(scope,element,attrs,ngModel){
                if(!ngModel) return;

                ngModel.$render = function(){
                }

                element.bind('change',function(e){
                    var element = e.target;
                    if(!element.value) return;

                    element.disabled = true;
                    $q.all(slice.call(element.files,0).map(readFile))
                        .then(function(values){
                            if(element.multiple) ngModel.$setViewValue(values);
                            else ngModel.$setViewValue(values.length ? values[0] : null);
                            element.value = null;
                            element.disabled = false;
                        });

                    function readFile(file){
                        var deferred = $q.defer();

                        var reader = new FileReader()
                        reader.onload = function(e){
                            deferred.resolve(e.target.result);
                        }
                        reader.onerror = function(e){
                            deferred.reject(e);
                        }
                        reader.readAsDataURL(file);

                        return deferred.promise;
                    }

                }); //change

            } //link

        }; //return

    }) //appFilereader
    .directive('ngConfirmClick',[
        function(){
            return {
                link:function(scope,element,attr){
                    var msg = attr.ngConfirmClick||"Are you sure?";
                    var clickAction = attr.confirmedClick;
                    element.bind('click',function(event){
                        if(window.confirm(msg)){
                            scope.$eval(clickAction)
                        }
                    });
                }
            };
        }])


    .directive('knowledgeManagementGraph',function(){
        return {
            restrict:"A",
            scope:{graph:'='},
            link:function(scope,elem,attrs){
                scope.$watch('graph',function(){
                    if(scope.graph==undefined){
                        return false;
                    }
                    jQuery('#knowledgeManagementGraph').highcharts({
                        "chart":{
                            "type":"column",
                            "style":{
                                "fontFamily":"Arial"
                            },
                            "height": 250
                        },
                        "yAxis":{
                            "labels":{
                                "format":"{value:,.of}"
                            },
                            "title":{
                                "text":null
                            }
                        },
                        "exporting":{
                            "enabled":false
                        },
                        "colors":[
                            "#39B468",
                            "#bf0000",
                        ],
                        "tooltip":{
                            "shared":true
                        },
                        "title":{
                            "text":" ",
                            "align":"left"
                        },
                        "xAxis":{
                            "type":"datetime"
                        },
                        "series":[
                            {
                                "index":0,
                                "name":"Doc.'s Viewed",
                                "data":scope.graph.views
                            },
                            {
                                "index":1,
                                "name":"Doc.'s Uploaded",
                                "data":scope.graph.uploads
                            }
                        ]
                    });
                })

            }
        }
    })
    /**
     * projectRiskAssessmentCtrl
     */
    .filter('attrFactor',function(){
        return function(sub_category){
            var count = 0;
            var factorSum = 0;
            angular.forEach(sub_category.attributes,function(item){
                if(item.grade!=undefined&&item.grade>0){

                    factorSum = factorSum+parseInt(item.grade);
                    count++;
                }
                //console.log('attribute',item.factor);
            })

            var totalSum = factorSum/count;
            if(isNaN(totalSum)){
                totalSum = 0;
            }
            sub_category.factor = totalSum;
            return totalSum.toFixed(2);
            // do some bounds checking here to ensure it has that index

        }
    })
    .filter('subCategoryFactor',function(){
        return function(cateogry){
            var count = 0;
            var factorSum = 0;
            angular.forEach(cateogry.sub_category,function(item){
                if(item.factor!=undefined&&item.factor>0){
                    // console.log('item',item);
                    factorSum = factorSum+(parseInt(item.risk_percentage)*parseFloat(item.factor));

                }
                //console.log('attribute',item.factor);
            })

            return (factorSum/100).toFixed(2);
            // do some bounds checking here to ensure it has that index

        }
    })
    .filter('ellipsis',function(){
        return function(text,limit){

            var changedString = String(text).replace(/<[^>]+>/gm,'');
            var length = changedString.length;

            return changedString.length>limit ? changedString.substr(0,limit-1)+'...' : changedString;
        }
    })
    .filter('limitchr',function(){
        return function(text,limit){

            var changedString = String(text).replace(/<[^>]+>/gm,'');
            var length = changedString.length;

            return changedString.length>limit ? changedString.substr(0,limit-1) : changedString;
        }
    })
    .directive('hotSize',function(){ //declaration; identifier master
        function link(scope,element,attrs){ //scope we are in, element we are bound to, attrs of that element
            scope.$watch(function(){ //watch any changes to our element

                var hotTableHeight = element.find('.wtHider').height();
                var tableHeight = element.find('.wtHolder').height();
                var setTableHeight = 0;
                if(hotTableHeight<tableHeight){
                    setTableHeight = hotTableHeight;
                } else{
                    setTableHeight = tableHeight;
                }

                element.css({ //scope variable style, shared with our controller
                    height:setTableHeight+20+'px', //set the height in style to our elements height
                });
            });
        }

        return {
            restrict:'AE', //describes how we can assign an element to our directive in this case like <div master></div
            link:link // the function to link to our element
        };
    })

    .directive('parentActive',function(){
            return {
                link:function(scope,element){
                    var leng = element.find('ul li.in').length;
                    scope.$watch('location.path()',function(){
                        var leng = element.find('ul li.in').length;
                        if(leng > 0){
                            element.addClass('active parent-sub');
                            //element.find('.sub-menu').css('display',"block");
                        } else{
                            element.removeClass('active parent-sub');
                           // element.find('.sub-menu').css('display',"none");
                        }

                    })

                },
            };
        })
    .directive("scrollPos", function ($window) {
    return function(scope, element, attrs) {
        angular.element($window).bind("scroll", function() {
           // console.log('scrolled'+ element.offset().top+'--'+this.pageYOffset);
            element.css({
                'top':this.pageYOffset
            })
            scope.visible = false;
            scope.$apply();
        });
    };
    })
    .directive('textLine',function(){
            return {
                link:function(scope,element){
                    element.addClass('text-line');
                    element.bind('click',function(){
                        element.toggleClass('text-line');

                    });
                },
            };
        })
    .directive('iconAlltouchpointsTypes',function(){
            return {
                link:function(scope,element,attrs){
                    switch(attrs.tuchpointtype){
                            case 'touchpoint':

                                if(attrs.isremainder==1){
                                    element.addClass('alert-box');
                                }
                                else{
                                    element.addClass('red-box');
                                }
                                element.find('.t-icon i').addClass('icon-touchpoint');

                            break;
                        case 'contact':
                            element.addClass('sky-blue-box');
                            element.find('.t-icon i').addClass('icon-users2');
                            break;
                        case 'attachment':
                            element.addClass('attachment-box');
                            element.find('.t-icon i').addClass('icon-workload');
                            break;
                        case 'company':
                            element.addClass('sky-blue-box');
                            element.find('.t-icon i').addClass('icon-building');
                            break;
                        case 'mail':
                            element.addClass('mail-box');
                            element.find('.t-icon i').addClass('icon-mail');
                            break;
                        case 'meeting':
                            element.addClass('meeting-box');
                            element.find('.t-icon i').addClass('icon-users1');
                            break;
                        case 'knowledge':
                            element.addClass('knowledge-box');
                            element.find('.t-icon i').addClass('icon-clipped-file2');
                            break;
                        case 'facility':
                            element.addClass('facility-box');
                            element.find('.t-icon i').addClass('icon-info');
                            break;
                        case 'approval':
                            element.addClass('approval-box');
                            element.find('.t-icon i').addClass('icon-right-circle');
                            break;
                        default :
                            element.addClass('red-box');
                            element.find('.t-icon i').addClass('icon-clipped-file');
                            break;
                    }
                }
            };
        })
    .directive('orgchart', function() {
        return {
            restrict: 'AE',
            scope: {
                source     : '=',
                container  : '=',
                stack      : '=',
                depth      : '=',
                interactive: '=',
                showLevels : '=',
                clicked    : '=' // We use '=' here rather than '&' for this callback function
            },
            link: function(scope, element, attrs) {
                $("#basic-styles-source").orgChart({
                    container: $("#chart"),
                    interactive: true
                });
            }
        }
    })

    .directive('questionOption', function() {
        return {
            restrict: 'E',
            link: function(scope, element, attrs) {
                // some ode
            },
            templateUrl: function(elem,attrs) {
                return attrs.templateUrl || 'partials/directive-templates/question_options/all_inputs.html'
            }
        }
    })







