/**
 * Created by RAKESH on 18-12-2015.
 */
angular.module('app')

    .controller('ContactDashboardCtrl',function($scope,$rootScope,crmContactService,crmCompanyService,quickService,$state,decodeFilter){

        $scope.companyContacts = [];
        $scope.totalContactCount = 0;
        /*$scope.contactView = function ($id_crm_contact) {
         $rootScope.currentContactViewId = $id_crm_contact;
         $state.go('app.contact-list');
         };*/
        crmContactService.contactList($rootScope.id_company).then(function(result){
            $scope.totalContactCount = result.data.total_records;
        });
        crmContactService.crmContactType().then(function(result){
            $scope.contactTypes = result.data;
        });
        $scope.displayed = false;
        $scope.callServer = function(tableState){
            crmContactService.contactList($rootScope.id_company,tableState).then(function(result){
                $scope.displayed = true;
                $scope.tableStateRef = tableState;
                $scope.companyContacts = result.data.data;
                // $scope.totalContactCount = result.data.total_records;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/tableState.pagination.number);
            });
        };
        $scope.gotoState = function(row){
            console.log('row',row);
            var Id = decodeFilter(row.id_crm_contact);
            if(row.created_by==$rootScope.userId){
                $state.go('contact.contact-form-list',{'id':Id});
            } else{
                $state.go('contact.contact-complete-view',{'id':Id});
            }
        }
        $scope.getCompanyList = function(search_key){
            $scope.visibleAddButton = false;
            return crmCompanyService.crmListForMapping({
                'crm_contact_id':0,
                'search_key':search_key,
                'company_id':$rootScope.id_company
            }).then(function(result){
                if(result.status){
                    return result.data;
                }
            });
        };
        //$scope.onSelect = function ($item, $model, $label) {
        //    $scope.visibleAddButton = true;
        //    console.log('$item', $item);
        //    $scope.selectedItem = $item;
        //    $scope.visibleAddButton = false;
        //    $scope.visibleDesignationDropDown = true;
        //    $scope.asyncSelected = '';
        //
        //};
        /*crmCompanyService.companyDesignation().then(function (response) {
         $scope.companyDesignations = response.data;
         console.log(response.data);
         });*/
        $scope.onSelect = function($item,$model,$label){
            $scope.visibleAddButton = true;
            //console.log('$item', $item);
            $scope.selectedItem = $item;
            $scope.visibleAddButton = false;
            $scope.asyncSelected = '';
            $scope.crm_company_name = '';
            //console.log('onSelect');
            //$rootScope.companyList.unshift($scope.selectedItem);
        };
        $scope.crm_company_name = '';
        $scope.change = function(companyName){
            //console.log(companyName);
            $scope.crm_company_name = companyName;
        };
        crmCompanyService.companyDesignation().then(function(response){
            $scope.companyDesignations = response.data;
            //console.log(response.data);
        });
        $scope.submitBtnDisabled = false;
        $scope.submitQuickContact = function($inputData,form,moreFields){
            $rootScope.removeErrorMessages();
            if(form.$valid){
                $scope.submitBtnDisabled = true;
                if($scope.crm_company_name!=''){
                    $inputData.crm_company_name = $scope.crm_company_name;
                } else{
                    delete $inputData.crm_company_name;
                }
                $inputData.company_id = $rootScope.id_company;
                $inputData.created_by = $rootScope.userId;
                quickService.saveQuickContact($inputData).then(function(result){
                    $scope.submitBtnDisabled = false;
                    if(result.status){
                        $rootScope.toast('Success',result.message);
                        $scope.submitted = false;
                        $scope.contact = {};
                        if(moreFields){
                            $rootScope.currentContactViewId = result.data.crm_contact_id;
                            $state.go('contact.contact-form-list',{id:decodeFilter($rootScope.currentContactViewId)});
                        }else{ $scope.callServer($scope.tableStateRef); }
                        //$rootScope.crm_contact_id = result.data.crm_contact_id;
                    } else{
                        $scope.submitted = false;
                        $rootScope.toast('Error',result.error,'error',result.error);
                    }
                })
            }
        };
    })

    .controller('contactFormListCtrl',function($rootScope,$scope,crmContactService,encodeFilter,$stateParams,$state,$uibModal,crmCompanyService,$q){
        $rootScope.contactId = $rootScope.currentContactViewId = encodeFilter($stateParams.id);
        $rootScope.currentModuleId = 1;
        $scope.contactForms = [];
        $scope.init = function(){
            $scope.contactInformation = crmContactService.contactInformation({'crm_contact_id':$rootScope.currentContactViewId}).then(function(result){
                console.log('result',result);
                $rootScope.companyContactsDetails = result.data;

            });
            $scope.contactType = crmContactService.crmContactType().then(function(result){
                $scope.contactTypes = result.data;
            });
            $q.all([$scope.contactType,$scope.contactInformation]).then(function(result){
                $scope.contact_type = $rootScope.companyContactsDetails.crm_contact_type_id;
            });
            crmContactService.contactForms({
                'crm_contact_id':$rootScope.currentContactViewId,
                'crm_module_id':1
            }).then(function(result){
                $rootScope.contactFormDetails = [];
                $scope.contactForms = result.data;
                angular.forEach($scope.contactForms,function(section){
                    angular.forEach(section.forms,function($item){
                        var $obj = {};
                        $obj.sectionName = $item.sectionName;
                        $obj.tag = $item.form_name;
                        $obj.formId = $item.id_form;
                        $obj.template = $item.form_template;
                        $obj.form_key = $item.form_key;
                        $rootScope.contactFormDetails.push($obj);
                    })
                })

            });
        }

        $scope.goToEditCtrl = function(data){
            $rootScope.contactParames = data;
            console.log('data',data);
            $state.go("contact.contact-form-edit",{'id':$stateParams.id});
        };

        $rootScope.selectedCompanyListFun = function(){//{'crm_company_id':$rootScope.contactId}
            crmCompanyService.getCompanyListByContactId({'crm_contact_id':$rootScope.currentContactViewId}).then(function(result){
                //console.log(result.data);
                $rootScope.selectedCompanyList = result.data;
            });
        };

        $scope.getChangeContactType = function(contactId){
            crmContactService.changeUserContactType({
                'id_crm_contact':$rootScope.currentContactViewId,
                'crm_contact_type_id':contactId
            }).then(function(result){
                //$state.reload();
                $scope.init();
            });
        }


        $rootScope.selectedCompanyListFun();
        /*Attachments*/
        $scope.quickAttachmentModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/contact/file-attachment.html',
                controller:'attachmentsCtrl'
            });
            modalInstance.result.then(function(){
            },function(){
                $scope.init();
                $state.reload();
            });
        }

        /*Added Companies Modal*/
        $scope.AddedCompaniesModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                scope:$scope,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/contact/added-companies.html',
                controller:function($scope,$uibModalInstance,crmCompanyService){
                    $rootScope.selectedCompanyListFun();
                    //alert($rootScope.id_company);
                    $scope.getCompanyList = function(search_key){
                        $scope.visibleAddButton = false;
                        return crmCompanyService.crmListForMapping({
                            'crm_contact_id':$rootScope.contactId,
                            'search_key':search_key,
                            'company_id':$rootScope.id_company
                        }).then(function(result){
                            return result.data;
                        });
                    };
                    $scope.onSelect = function($item,$model,$label){
                        $scope.visibleAddButton = true;
                        //console.log('$item', $item);
                        $scope.selectedItem = $item;
                        $scope.visibleAddButton = false;
                        $scope.visibleDesignationDropDown = true;
                        $scope.asyncSelected = '';
                        //$rootScope.companyList.unshift($scope.selectedItem);
                        crmCompanyService.companyDesignation().then(function(response){
                            $scope.companyDesignations = response.data;
                            //console.log(response.data);
                        });
                    };
                    $scope.visibleDesignationDropDown = false;
                    $scope.save = function(designation,form){
                        $scope.submitStatus = true;
                        if(form.$valid){
                            var fields = {
                                'crm_contact_id':$rootScope.contactId,
                                'crm_company_id':$scope.selectedItem.id_crm_company,
                                'crm_company_designation_id':designation.id_crm_company_designation,
                                'is_primary_company':designation.is_primary_company,
                                'created_by':$rootScope.userId,
                                'type':'contact'
                            };
                            crmCompanyService.addMapModule(fields).then(function(result){
                                $scope.visibleDesignationDropDown = false;
                                if(result.status){
                                    $rootScope.toast('Success',result.message);
                                    $rootScope.selectedCompanyListFun();
                                    $scope.init();
                                } else{
                                    $rootScope.toast('Error',result.error,'error',$scope.sector);
                                }
                            });

                        }
                        ;
                    };
                    $scope.cancelAssign = function(){
                        $scope.visibleDesignationDropDown = false;
                    };
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.removeContactCompany = function($id){
                        var r = confirm("Are you sure want to remove!");
                        if(r==true){
                            crmCompanyService.deleteContactCompany($id).then(function(result){
                                $rootScope.selectedCompanyListFun();
                            });
                        }
                    }
                }
            });
            modalInstance.result.then(function(){
            },function(){
                $state.reload();
            });
        };

        $scope.touchpointsModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/contact/touchpoints.html',
                controller:'touchPointsCtrl',
                resolve:{
                    crmParams:{
                        'crm_module_type':'contact',
                        'crm_module_id':$rootScope.currentContactViewId
                    }
                },
            });
            modalInstance.result.then(function(){
            },function(){
                $state.reload();
            });
        }
        $scope.init();


    })

    .controller('contactFormEditCtrl',function($rootScope,$scope,crmContactService,encodeFilter,$stateParams,$state){

        $rootScope.crm_contact_id = $rootScope.currentContactViewId = encodeFilter($stateParams.id);
        console.log('$rootScope.contactFormDetails',$rootScope.contactFormDetails);
        $scope.minSpareRows = 1;
        $scope.rowHeaders = false;
        $scope.db = {items:[[]]};
        $scope.settings = {colHeaders:true,autoWrapCol:true,contextMenu:['row_above','row_below','remove_row']};
        $scope.prevBtnShow = false;
        $scope.nextBtnShow = false;

        if($rootScope.contactParames==undefined){
            $state.go("contact.contact-form-list",{'id':$stateParams.id});
            return false;
        }
        //goto next form
        $scope.nextForm = function(){
            // var a = //searching Element in Array
            var nextIndex = $scope.currentFormIndex+1;
            console.log('nextIndex',nextIndex);
            console.log('$rootScope.contactFormDetails',$rootScope.contactFormDetails[nextIndex]);
            $rootScope.contactParames = $rootScope.contactFormDetails[nextIndex];
            $scope.init();
        }
        //goto prev form
        $scope.prevForm = function(){
            var nextIndex = $scope.currentFormIndex-1;
            console.log('nextIndex',nextIndex);
            $rootScope.contactParames = $rootScope.contactFormDetails[nextIndex];
            $scope.init();
        }
        $scope.init = function(){
            //console.log('$rootScope.contactFormDetails',$rootScope.contactFormDetails);
            if($rootScope.contactParames.formId){
                var a = _.findWhere($rootScope.contactFormDetails,{'tag':$rootScope.contactParames.tag});//searching Element in Array
                $scope.currentFormIndex = _.indexOf($rootScope.contactFormDetails,a);// getting index.
                if($scope.currentFormIndex!= -1){
                    $scope.prevBtnShow = ($scope.currentFormIndex>0) ? true : false;
                    $scope.nextBtnShow = ($scope.currentFormIndex<($rootScope.contactFormDetails.length-1)) ? true : false;
                }
                console.log('$rootScope.contactFormDetails.length',($rootScope.contactFormDetails.length-1));
                console.log('$scope.currentFormIndex',$scope.currentFormIndex);
            }

            $scope.template = $rootScope.contactParames.template;
            $scope.formName = $rootScope.contactParames.formName;
            $scope.sectionName = $rootScope.contactParames.tag;
            $scope.formId = $rootScope.contactParames.formId;

            $scope.form_key = $rootScope.contactParames.form_key;
            console.log('$rootScope.contactParames.form_key',$rootScope.contactParames.form_key);


            crmContactService.contactFormDetails($rootScope.crm_contact_id,$scope.formId,'edit').then(function(result){
                if(result.status){
                    $scope.contact = result.data.data;
                }
            });

        }
        $scope.submitForm = function($inputData,$form_data,form){
            $scope.submitStatus = true;
            //console.log(form.$valid);
            //console.log('$form_data', $inputData);
            if(form.$valid){
                $inputData.company_id = $rootScope.id_company;
                $inputData.form_id = $scope.formId;
                $inputData.user_id = $rootScope.userId;
                if($rootScope.crm_contact_id!=undefined){
                    $inputData.crm_contact_id = $rootScope.crm_contact_id;
                }
                crmContactService.saveApplicant($inputData).then(function(result){
                    if(result.status){
                        // $scope.checkForm.push($form_data.id_form);
                        $rootScope.toast('Success',result.message);
                    } else{
                        $rootScope.toast('Error',result.error,'error',$scope.sector);
                    }
                })
            }
        };


        $scope.init();


    })

    .controller('ContactCompleteViewCtrl',function($rootScope,$scope,crmContactService,$state,$uibModal,crmCompanyService,$stateParams,encodeFilter,$q,anchorSmoothScroll,$location){
        $scope.gotoElement = function(eID){
            $location.hash();
            anchorSmoothScroll.scrollTo(eID);
        };

        $scope.settings = {
            colHeaders:true,
            autoWrapCol:true,
            readOnly:true,
            contextMenu:['row_above','row_below','remove_row']
        };
        $rootScope.currentContactViewId = encodeFilter($stateParams.id);
        $rootScope.contactId = $rootScope.currentContactViewId;
        $rootScope.currentModuleId = 1;

        var service1 = crmContactService.contactDetails($rootScope.contactId,1).then(function(result){
            $scope.companyContactsDetails = result.data.details[0];
            $scope.companyContactForms = result.data.forms;
            $rootScope.formStatus = result.data.active_forms;
        });
        $q.all([service1]).then(function(){
            $scope.buildMenu($scope.companyContactsDetails.crm_contact_type_id);
        });

        $scope.buildMenu = function(contactType){
            var menu = $scope.companyContactForms;
            var newMenu = [];
            if(contactType==1||contactType==2){
                angular.forEach(menu,function(pvalue,pkey){
                    angular.forEach(pvalue.form,function(cvalue,ckey){
                        //console.log('pkey',pkey,'ckey',ckey);
                        if(pkey==0&&ckey==0){
                            console.log('cvalue',cvalue);
                            newMenu.push({
                                'id_section':pvalue.id_section,
                                'section_name':pvalue.section_name,
                                'form':[]
                            });
                            newMenu[0].form.push({
                                'form_name':cvalue.form_name,
                                'form_template':cvalue.form_template,
                                'id_form':cvalue.id_form,
                                'id_section':cvalue.id_section,
                                'field_data':cvalue.field_data
                            });
                        }
                    });
                });
                $scope.companyContactForms = newMenu;
            }
        }

        $scope.getFormData = function(formId){
            crmContactService.contactFormDetails($rootScope.contactId,formId,'view').then(function(result){
                if(result.status){

                    $scope.viewContactForm = result.data.data.form_data;
                    $scope.formName = result.data.data.form_name;
                }
            });
        };

    })

    .controller('ContactCreationCtrl',function($rootScope,$scope,crmContactService,$state,$stateParams,encodeFilter,$q,decodeFilter){
        $scope.contact = {};
        $scope.contact.date_of_birth = new Date();
        $scope.maxDate = new Date();
        $rootScope.crm_contact_id = encodeFilter($stateParams.id);
        $rootScope.module_id = 1;
        $scope.contactObj = {};
        $scope.checkForm = [];
        if($rootScope.crm_contact_id!=undefined){
            var service1 = crmContactService.contactDetails($rootScope.crm_contact_id,1).then(function(result){
                $scope.companyContactsDetails = result.data.details[0];
                $scope.companyContactForms = result.data.forms;
                $rootScope.formStatus = result.data.active_forms;
                if($rootScope.formStatus!=undefined){
                    angular.forEach($rootScope.formStatus,function(task,index){
                        if(task.status){
                            $scope.checkForm.push(task.id);
                        }
                    });
                }
            });
            var service2 = crmContactService.crmContactType().then(function(result){
                $scope.contactTypes = result.data;
            });
            $q.all([service1,service2]).then(function(){
                crmContactService.getModules($rootScope.module_id).then(function($result){
                    //console.log($result);
                    $scope.contactObj = $result.data;
                    $scope.buildMenu($scope.companyContactsDetails.crm_contact_type_id);
                });
            });
        }
        $scope.buildMenu = function(contactType){
            var menu = $scope.contactObj;
            var newMenu = [];
            if(contactType==1||contactType==2){
                angular.forEach(menu,function(pvalue,pkey){
                    angular.forEach(pvalue.form,function(cvalue,ckey){
                        //console.log('pkey',pkey,'ckey',ckey);
                        if(pkey==0&&ckey==0){
                            //console.log('cvalue',cvalue);
                            newMenu.push({
                                'id_section':pvalue.id_section,
                                'section_name':pvalue.section_name,
                                'form':[]
                            });
                            newMenu[0].form.push({
                                'form_name':cvalue.form_name,
                                'form_template':cvalue.form_template,
                                'id_form':cvalue.id_form,
                                'id_section':cvalue.id_section
                            });
                        }
                    });
                });
                $scope.contactObj = newMenu;
            }
        }
        $scope.getChangeContactType = function(contactId){
            crmContactService.changeUserContactType({
                'id_crm_contact':$rootScope.crm_contact_id,
                'crm_contact_type_id':contactId
            }).then(function(result){
                $state.reload();
            });
        }

        $scope.accordionGroups = {};
        $scope.openAccordion = function($id){
            $scope.accordionGroups[$id] = true;
        };

        $scope.nextFormOpen = function($form_data){
            //console.log($form_data);
            var form_id = $form_data.id_form;
            var section_id = $form_data.id_section;
            $arrayData = $scope.contactObj;
            angular.forEach($arrayData,function($section){
                if($section.id_section==section_id){
                    angular.forEach($section.form,function($form){
                        if($form.id_form==form_id){
                            if($section.form.length>$section.form.indexOf($form)+1){
                                $scope.loadTemplate($section.form[$section.form.indexOf($form)+1],$section.id_section);
                                //console.log('next Form', $section.form.indexOf($form) + 2);
                                return false;
                            } else{
                                //console.log('no form are there in the section', $arrayData.indexOf($section) + 1);
                                if($arrayData[$arrayData.indexOf($section)+1]){
                                    if($arrayData[$arrayData.indexOf($section)+1].form.length>0){
                                        $scope.loadTemplate($arrayData[$arrayData.indexOf($section)+1].form[0],$arrayData[$arrayData.indexOf($section)+1].id_section);
                                        return false;
                                    }
                                } else{
                                    //close form
                                    $state.go("contact.contact-list",{'id':decodeFilter($rootScope.crm_contact_id)});
                                }
                            }
                        }
                    })
                }
            });
        };
        $scope.percentage = 0;
        $scope.getCheckSection = function($section_id){
            var $form = $activeForm = 0;
            angular.forEach($rootScope.formStatus,function(task,index){
                if(task.section_id==$section_id){
                    $form++;
                    if(task.status==1){
                        $activeForm++;
                    }
                }
            });

            if($form==$activeForm&&$activeForm>0){
                //if($scope.companyContactsDetails.crm_contact_type_id == 1)
                return 'fa fa-check-circle green';
            } else if($activeForm>0){
                return 'fa-exclamation-circle orange';
            } else{
                return 'fa-times-circle red';
            }
        }
        $scope.getCheck = function($form_id){

            /*console.log($form_id, $scope.checkForm);*/
            if($.inArray($form_id,$scope.checkForm)!= -1){
                return 'fa-check green';
            } else{
                return 'fa-close red';
            }
        }

        $scope.submitForm = function($inputData,$form_data,form){
            $scope.submitStatus = true;
            //console.log(form.$valid);
            //console.log('$form_data', $inputData);
            if(form.$valid){
                $inputData.company_id = $rootScope.id_company;
                $inputData.form_id = $form_data.id_form;
                $inputData.user_id = $rootScope.userId;
                if($rootScope.crm_contact_id!=undefined){
                    $inputData.crm_contact_id = $rootScope.crm_contact_id;
                }
                crmContactService.saveApplicant($inputData).then(function(result){
                    if(result.status){
                        // $scope.checkForm.push($form_data.id_form);
                        $rootScope.toast('Success',result.message);
                        $rootScope.crm_contact_id = result.data.crm_contact_id;
                        $scope.percentage = Math.ceil(result.data.percentage);

                        $scope.checkForm.push($form_data.id_form);
                        $scope.nextFormOpen($form_data);
                    } else{
                        $rootScope.toast('Error',result.error,'error',$scope.sector);
                    }
                })
            }
        };
        $scope.minSpareRows = 1;
        $scope.rowHeaders = false;

        $scope.db = {items:[[]]};
        $scope.settings = {colHeaders:true,autoWrapCol:true,contextMenu:['row_above','row_below','remove_row']};

        $scope.anotherOptions = {
            barColor:'#02B000',
            trackColor:'#f9f9f9',
            scaleColor:'#dfe0e0',
            scaleLength:5,
            lineCap:'round',
            lineWidth:3,
            size:50,
            rotate:0,
            animate:{
                duration:1000,
                enabled:true
            }
        };
        $scope.checkFieldEdited = function(form){
            console.log('form',form);
            $scope.myForm = form;
            angular.forEach($scope.myForm,function(value,key){
                if(key[0]=='$') return;
                //console.log(key, value.$pristine)
            });
        };
        $scope.loadTemplate = function(form,id_section){
            $scope.submitStatus = false;
            $scope.formData = form;
            $scope.formData.id_section = id_section;
            if($rootScope.crm_contact_id!=undefined&&$rootScope.id_company!=undefined){
                crmContactService.contactFormDetails($rootScope.crm_contact_id,form.id_form,'edit').then(function(result){
                    if(result.status){
                        $scope.contact = result.data.data;
                        $scope.percentage = Math.ceil(result.data.percentage);
                        $scope.contact_created_date = result.data.contact_details.created_date_time;
                        $scope.first_name = result.data.contact_details.first_name;
                        $scope.last_name = result.data.contact_details.last_name;
                        $scope.email = result.data.contact_details.email;
                        $scope.contact_type = $scope.crm_contact_type_id = result.data.contact_details.crm_contact_type_id;
                    }
                });
            }
        }
    })

    .controller('CompanyDashboardCtrl',function($scope,$rootScope,crmCompanyService,$state,quickService,decodeFilter){

        $scope.totalCount = 0;
        $scope.companyView = function($id_crm_company){
            $rootScope.currentCompanyViewId = $id_crm_company;
            $state.go('company.company-list');
        };
        crmCompanyService.companyList($rootScope.id_company).then(function(result){
            $scope.totalCount = result.data.total_records;
        });

        $scope.displayed = false;
        $scope.callServer = function(tableState){
            crmCompanyService.companyList($rootScope.id_company,tableState).then(function(result){
                $scope.displayed = true;
                $scope.tableStateRef = tableState;
                $scope.companyList = result.data.data;
                //$scope.totalCount = result.data.total_records;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/tableState.pagination.number);
            });
        }
        $scope.gotoState = function(row){
            var Id = decodeFilter(row.id_crm_company);
            if(row.created_by==$rootScope.userId){
                $state.go('company.company-creation',{'id':Id});
            } else{
                $state.go('company.business-assessment',{'id':Id});
            }
        }
        $scope.submitBtnDisabled = false;
        $scope.submitQuickCompany = function($inputData,form,moreFields){
            $rootScope.removeErrorMessages();
            if(form.$valid){
                $scope.submitBtnDisabled = true;
                $inputData.company_id = $rootScope.id_company;
                $inputData.created_by = $rootScope.userId;
                quickService.saveQuickCompany($inputData).then(function(result){
                    $scope.submitBtnDisabled = false;
                    if(result.status){
                        $rootScope.toast('Success',result.message);
                        $scope.company = {};
                        $scope.submitted = false;
                        $scope.callServer($scope.tableStateRef);
                        if(moreFields){
                            $rootScope.currentCompanyViewId = result.data.crm_company_id;
                            $state.go('company.company-creation',{id:decodeFilter($rootScope.currentCompanyViewId)});
                        }
                    } else{
                        $scope.submitted = false;
                        $rootScope.toast('Error',result.error,'error',result.error);
                    }
                })
            }
        };
    })

    .controller('CompanyListCtrl',function($rootScope,$scope,crmCompanyService,crmContactService,$state,$uibModal,Upload,$stateParams,encodeFilter){
        $scope.settings = {
            colHeaders:true,
            autoWrapCol:true,
            readOnly:true,
            contextMenu:['row_above','row_below','remove_row']
        };
        $rootScope.currentCompanyViewId = encodeFilter($stateParams.id);
        $rootScope.contactId = $rootScope.currentCompanyViewId;
        $rootScope.currentModuleId = 2;

        crmCompanyService.companyDetails($rootScope.currentCompanyViewId,2).then(function(result){
            $scope.companyDetails = result.data.details[0];
            $scope.companyForms = result.data.forms;
            $rootScope.formStatus = result.data.active_forms;
        });


        $scope.getFormData = function(formId){
            crmCompanyService.companyFormDetails($rootScope.currentCompanyViewId,formId,'view').then(function(result){
                if(result.status){
                    $scope.viewContactForm = result.data.data.form_data;
                    $scope.formName = result.data.data.form_name;
                }
            });
        };
        $scope.addNewCompany = function(){
            $rootScope.id_crm_company = undefined;
            $rootScope.formStatus = undefined;
        };
        $rootScope.selectedCompanyList = [];
        $rootScope.selectedCompanyListFun = function(){
            crmCompanyService.getCompanyListByContactId({'crm_company_id':$rootScope.contactId}).then(function(result){
                $rootScope.selectedCompanyList = result.data;
            });
        };
        $scope.selectedCompanyListFun();

        /*Attachments*/
        $scope.quickAttachmentModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/contact/file-attachment.html',
                controller:'attachmentsCtrl'
            });
            modalInstance.result.then(function(){
            },function(){
            });
        };
        $scope.AddedContactsModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/company/added-contacts.html',
                controller:function($scope,$uibModalInstance,crmCompanyService){
                    $scope.getContactList = function(search_key){
                        $scope.visibleAddButton = false;
                        return crmCompanyService.crmListForMapping({
                            'crm_company_id':$rootScope.contactId,
                            'search_key':search_key,
                            'company_id':$rootScope.id_company
                        }).then(function(result){
                            return result.data;
                            ////console.log(result.data.data);
                        });
                    };
                    $scope.onSelect = function($item,$model,$label){
                        $scope.visibleAddButton = true;
                        //console.log('$item', $item);
                        $scope.selectedItem = $item;
                        $scope.visibleAddButton = false;
                        $scope.visibleDesignationDropDown = true;
                        $scope.asyncSelected = '';

                        //$rootScope.companyList.unshift($scope.selectedItem);
                        crmCompanyService.companyDesignation().then(function(response){
                            $scope.companyDesignations = response.data;
                            //console.log(response.data);
                        });

                        //$rootScope.companyList.unshift($scope.selectedItem);
                        crmContactService.contactType().then(function(response){
                            $scope.contactType = response.data;
                        });
                    };

                    $scope.visibleDesignationDropDown = false;
                    $scope.save = function(designation,form){
                        $scope.submitStatus = true;
                        if(form.$valid){
                            var fields = {
                                'crm_contact_id':$scope.selectedItem.id_crm_contact,
                                'crm_company_id':$rootScope.contactId,
                                'crm_company_designation_id':designation.id_crm_company_designation,
                                'is_primary_company':designation.is_primary_company,
                                'created_by':$rootScope.userId
                            };
                            crmCompanyService.addMapModule(fields).then(function(result){
                                if(result.status){
                                    $rootScope.toast('Success',result.message);
                                    $scope.visibleDesignationDropDown = false;
                                    $scope.selectedCompanyListFun();
                                } else{
                                    $rootScope.toast('Error',result.error,'error',$scope.sector);
                                }
                            });
                        }
                    };
                    $scope.cancelAssign = function(){
                        $scope.visibleDesignationDropDown = false;
                    };
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.removeContactCompany = function($id){
                        alert('hello');
                        var r = confirm("Are you sure want to remove!");
                        if(r==true){
                            crmCompanyService.deleteContactCompany($id).then(function(result){
                                $rootScope.selectedCompanyListFun();
                            });
                        }
                    }
                }
            });
        }
        /*Touch points*/
        $scope.touchpointsModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/contact/touchpoints.html',
                controller:'touchPointsCtrl'
            });
            modalInstance.result.then(function(){
            },function(){
                $scope.getTouchPointsList();
            });
        }

        $scope.getTouchPointsList = function(){
            var getParams = 'crm_module_id='+$rootScope.currentModuleId+'&from_id='+$rootScope.contactId;
            crmContactService.getTouchPointsList(getParams).then(function(result){
                $scope.touchPointsList = result.data.data;
            });
        }
        $scope.getTouchPointsList();

    })

    .controller('CompanyCreationCtrl',function($rootScope,$scope,$state,crmCompanyService,crmContactService,masterService,$stateParams,encodeFilter,$uibModal,decodeFilter,$q){

        $scope.isEditMode = function(flag){
            $scope.isEdit = flag;
        }
        $scope.submitBudgetForm = function(budgetForm){
            console.log($scope.budgetExcel);
        }
        $scope.uploadBudget = function(file){
            if(file!=null&&file!=''){
                $scope.budgetExcel = file;
            } else{
                $rootScope.toast('Error','file format not supported file formats','image-error');
            }
        }

        $rootScope.currentCompanyViewId = encodeFilter($stateParams.id);
        $rootScope.module_id = 2;
        $rootScope.contactId = $rootScope.currentCompanyViewId;
        $rootScope.currentModuleId = 2;
        $scope.companyObj = {};
        $scope.minSpareRows = 1;
        $scope.rowHeaders = false;
        $scope.checkForm = [];
        $scope.touchPointsList = [];


        $scope.init = function(){
            $rootScope.companyParames = {};

            $q.all({
                'companyDetails':$scope.getCompanyModule(),
                'financialInformaion':$scope.getFinancialInformation(),
                'companyAssessment':$scope.getCompanyAssessment()
            }).then(function(result){
                $rootScope.companyFormDetails = [];
                angular.forEach($scope.companyForms,function(section){
                    angular.forEach(section.form,function($item){
                        var $obj = {};
                        $obj.sectionName = 'business_key_data';
                        $obj.tag = $item.form_name;
                        $obj.formId = $item.id_form;
                        $obj.template = $item.form_template;
                        $obj.form_key = $item.form_key;
                        $rootScope.companyFormDetails.push($obj);
                    })
                })
                angular.forEach($scope.financialInfo,function(section){

                    var $obj = {};
                    $obj.sectionName = 'financial_information';
                    $obj.tag = section.statement_name;
                    $obj.formId = section.id_financial_statement;
                    $obj.template = 'upload-financial-information.html';
                    $obj.formName = section.statement_name;
                    $obj.form_key = section.statement_key;
                    $rootScope.companyFormDetails.push($obj);
                })
                angular.forEach($scope.companyAssessmentList,function(section){
                    var $obj = {};
                    $obj.sectionName = 'business_assessment';
                    $obj.tag = section.assessment_name;
                    $obj.formId = section.id_assessment;
                    $obj.template = section.assessment_template;
                    $obj.formName = section.assessment_name;
                    $obj.type = section.assessment_type;
                    $obj.assessmentId = section.id_assessment;
                    $obj.assessment_key = section.assessment_key;
                    $obj.form_key = section.assessment_key;
                    $rootScope.companyFormDetails.push($obj);
                })
                console.log(' $rootScope.companyFormDetails',$rootScope.companyFormDetails);
            })
            $scope.selectedCompanyListFun();
        }

        $scope.percentage = 0;
        $scope.parentSectorsList = [];
        $scope.subSectorList = [];
        $scope.anotherOptions = {
            barColor:'#02B000',
            trackColor:'#f9f9f9',
            scaleColor:'#dfe0e0',
            scaleLength:5,
            lineCap:'round',
            lineWidth:3,
            size:50,
            rotate:0,
            animate:{
                duration:1000,
                enabled:true
            }
        }


        $scope.getCompanyModule = function(){
            return crmCompanyService.companyDetails($rootScope.currentCompanyViewId,$rootScope.module_id).then(function(result){
                $scope.companyDetails = result.data.details[0];
                $rootScope.business_name = result.data.details[0].company_name;
                $rootScope.company_created_date = result.data.details[0].created_date_time;
                $rootScope.sector = result.data.details[0].sector;
                $rootScope.subsector = result.data.details[0].sub_sector;
                $scope.companyForms = result.data.forms;
                return result;

            });

        }


        //financial information
        $scope.getFinancialInformation = function(){
            return crmCompanyService.getFinancialInfo({'crm_company_id':$rootScope.currentCompanyViewId}).then(function($result){
                return $scope.financialInfo = $result.data;
            });
        };

        $scope.getCompanyAssessment = function(){
            return crmContactService.getCompanyAssessment({
                company_id:$rootScope.id_company,
                'crm_company_id':$rootScope.currentCompanyViewId
            }).then(function(result){
                return $scope.companyAssessmentList = result.data;
                //$rootScope.existingAssessmentList = result.data;
            });
        }

        $scope.formView = function(id_form){
            crmCompanyService.companyFormDetails($rootScope.id_crm_company,id_form,'view').then(function(result){
                if(result.status){
                    $scope.viewContactForm = result.data.data.form_data;
                    if(result.data.data.form_data==undefined){
                        $scope.viewContactForm = [];
                    }
                    $scope.formName = result.data.data.form_name;
                }
            });
        }
        //
        $rootScope.selectedCompanyList = [];
        $rootScope.selectedCompanyListFun = function(){
            crmCompanyService.getCompanyListByContactId({'crm_company_id':$rootScope.contactId}).then(function(result){
                //console.log(result.data);
                $rootScope.selectedCompanyList = result.data;
            });
        };


        /*add TouchPoint*/
        $scope.showReminders = false;
        $scope.showAllPoints = false;
        $scope.touchPointDescription = '';
        $scope.serchType = '';

        $scope.postField = {'reminder_date':new Date()};
        $scope.submit = function(postField,form){
            $scope.submitStatus = true;
            //console.log(form.$valid);
            if(form.$valid){
                postField.crm_module_id = $rootScope.currentModuleId;
                postField.touch_point_from_id = $rootScope.contactId;
                postField.created_by = $rootScope.userId;
                //console.log('postField', postField);
                crmContactService.addTouchPointComment(postField).then(function(result){
                    $scope.postField = {'reminder_date':new Date()};
                    $scope.submitStatus = false;
                    $scope.togglePoint = false;
                    if(result.status){
                        $rootScope.toast('Success',result.message);
                        $scope.togglePoint = false;
                       $state.reload();
                    } else{
                        $rootScope.toast('Error',result.error,'error',$scope.sector);
                    }

                });
            }
        }


        /*Attachments*/
        $scope.quickAttachmentModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/contact/file-attachment.html',
                controller:'attachmentsCtrl'
            });
            modalInstance.result.then(function(){
            },function(){
               $state.reload();
            });
        };
        $scope.AddedContactsModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/company/added-contacts.html',
                controller:'AddedContactsModalCtrl'
            });
            modalInstance.result.then(function(){
            },function(){
                $state.reload();
            });
        }
        /*Touch points*/
        $scope.touchpointsModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/contact/touchpoints.html',
                controller:'touchPointsCtrl',
                resolve:{
                    crmParams:{
                        'crm_module_type':'company',
                        'crm_module_id':$rootScope.currentCompanyViewId
                    }
                }
            });
            modalInstance.result.then(function(){
            },function(){
                $state.reload();
            });
        }
        /*  $scope.getTouchPointsList = function(){
         var getParams = 'crm_module_id='+$rootScope.currentModuleId+'&from_id='+$rootScope.contactId;
         crmContactService.getTouchPointsList(getParams).then(function(result){
         $scope.touchPointsList = result.data.data;
         });
         }
         */

        $scope.goToEditCtrl = function(data){
            $rootScope.companyParames = data;
            $state.go("company.business-keydata",{'id':decodeFilter($rootScope.contactId)});
        };
    })

    .controller('AddedContactsModalCtrl',function($rootScope,$scope,$uibModalInstance,crmCompanyService,crmContactService){
        $scope.getContactList = function(search_key){
            $scope.visibleAddButton = false;
            return crmCompanyService.crmListForMapping({
                'crm_company_id':$rootScope.currentCompanyViewId,
                'search_key':search_key,
                'company_id':$rootScope.id_company
            }).then(function(result){
                return result.data;
                ////console.log(result.data.data);
            });
        };
        $scope.onSelect = function($item,$model,$label){
            $scope.visibleAddButton = true;
            //console.log('$item', $item);
            $scope.selectedItem = $item;
            $scope.visibleAddButton = false;
            $scope.visibleDesignationDropDown = true;
            $scope.asyncSelected = '';

            //$rootScope.companyList.unshift($scope.selectedItem);
            crmCompanyService.companyDesignation().then(function(response){
                $scope.companyDesignations = response.data;
                //console.log(response.data);
            });

            //$rootScope.companyList.unshift($scope.selectedItem);
            crmContactService.contactType().then(function(response){
                $scope.contactType = response.data;
            });
        };

        $scope.visibleDesignationDropDown = false;
        $scope.save = function(designation,form){
            $scope.submitStatus = true;
            if(form.$valid){
                var fields = {
                    'crm_contact_id':$scope.selectedItem.id_crm_contact,
                    'crm_company_id':$rootScope.currentCompanyViewId,
                    'crm_company_designation_id':designation.id_crm_company_designation,
                    'is_primary_company':designation.is_primary_company,
                    'created_by':$rootScope.userId,
                    'type':'company'
                };
                crmCompanyService.addMapModule(fields).then(function(result){
                    if(result.status){
                        $rootScope.toast('Success',result.message);
                        $scope.visibleDesignationDropDown = false;
                        $rootScope.selectedCompanyListFun();
                    } else{
                        $rootScope.toast('Error',result.error,'error',$scope.sector);
                    }
                });
            }
        };
        $scope.cancelAssign = function(){
            $scope.visibleDesignationDropDown = false;
        };
        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
        $scope.removeContactCompany = function($id){
            var r = confirm("Are you sure want to remove!");
            if(r==true){
                crmCompanyService.deleteContactCompany($id).then(function(result){
                    $rootScope.selectedCompanyListFun();
                });
            }
        }
    })

    .controller('CompanyEditCtrl',function($rootScope,$scope,$state,crmCompanyService,crmContactService,masterService,$stateParams,encodeFilter,$uibModal,Upload){
        $scope.minDate = $scope.minDate ? null : new Date();
        if($rootScope.companyParames==undefined){
            $state.go("company.company-creation",{'id':$stateParams.id});
            return false;
        }
        $rootScope.id_crm_company = encodeFilter($stateParams.id);

        console.log('$rootScope.companyFormDetails',$rootScope.companyFormDetails);
        $scope.prevBtnShow = false;
        $scope.nextBtnShow = false;

        //goto next form
        $scope.nextForm = function(){
            // var a = //searching Element in Array
            var nextIndex = $scope.currentFormIndex+1;
            console.log('nextIndex',nextIndex);
            console.log('$rootScope.companyFormDetails',$rootScope.companyFormDetails[nextIndex]);
            $rootScope.companyParames = $rootScope.companyFormDetails[nextIndex];
            $scope.init();
        }
        //goto prev form

        $scope.prevForm = function(){
            var nextIndex = $scope.currentFormIndex-1;
            console.log('nextIndex',nextIndex);
            $rootScope.companyParames = $rootScope.companyFormDetails[nextIndex];
            $scope.init();
        }

        $scope.init = function(){
            //find current index
            console.log('=$rootScope.companyFormDetails',$rootScope.companyFormDetails);

            if($rootScope.companyParames.formId){

                var a = _.findWhere($rootScope.companyFormDetails,{'tag':$rootScope.companyParames.tag});//searching Element in Array
                $scope.currentFormIndex = _.indexOf($rootScope.companyFormDetails,a);// getting index.
                if($scope.currentFormIndex!= -1){
                    $scope.prevBtnShow = ($scope.currentFormIndex>0) ? true : false;
                    $scope.nextBtnShow = ($scope.currentFormIndex<($rootScope.companyFormDetails.length-1)) ? true : false;
                }
                console.log('$rootScope.companyFormDetails.length',($rootScope.companyFormDetails.length-1));
                console.log('$scope.currentFormIndex',$scope.currentFormIndex);
            }
            $scope.form_key = $rootScope.companyParames.form_key;
            console.log(' $scope.form_key', $scope.form_key);
            crmCompanyService.companyFormDetails(encodeFilter($stateParams.id),$rootScope.companyParames.formId,'edit').then(function(result){
                $scope.company_created_date = result.data.company_details.created_date_time;
                $scope.business_name = result.data.company_details.company_name;
                $scope.companyDescription = result.data.company_details.company_description;
                //$scope.company_created_date = result.data.details[0].created_date_time;
                $scope.sector = result.data.company_details.sector;
                $scope.subsector = result.data.company_details.sub_sector;
                $scope.sectorId = result.data.company_details.sector_id;
                $scope.subSectorId = result.data.company_details.sub_sector_id;
                $scope.getIntelligenceDetails();
            });
            switch($rootScope.companyParames!=undefined&&$rootScope.companyParames.sectionName){
                case 'business_key_data':
                    $scope.sectionName = "Business Key Data";
                    $scope.form_key = $rootScope.companyParames.form_key;
                    //$scope.company.ownership_details = [];
                    crmCompanyService.companyFormDetails(encodeFilter($stateParams.id),$rootScope.companyParames.formId,'edit').then(function(result){
                        $scope.company = result.data.data;
                        $scope.percentage = Math.ceil(result.data.percentage);
                        $scope.company_created_date = result.data.company_details.created_date_time;
                        $scope.business_name = result.data.company_details.company_name;
                        $scope.formName = result.data.data.form_name;
                        $scope.template = $rootScope.companyParames.template;
                    });
                    $scope.submitForm = function($inputData,$form_data,form){
                        console.log(form);
                        $scope.submitStatus = true;
                        if(form.$valid){
                            $inputData.company_id = $rootScope.id_company;
                            $inputData.form_id = $rootScope.companyParames.formId;
                            $inputData.user_id = $rootScope.userId;

                            //console.log('$rootScope.crm_company_id', $rootScope.id_crm_company);
                            if($rootScope.id_crm_company!=undefined){
                                $inputData.crm_company_id = $rootScope.id_crm_company;
                            }
                            //console.log('$inputData', $inputData);
                            crmCompanyService.saveCompany($inputData).then(function(result){
                                //console.log(result);
                                if(result.status){
                                    // $scope.checkForm.push($form_data.id_form);

                                    $rootScope.toast('Success',result.message);
                                    if($rootScope.crm_company_id==undefined){
                                        $rootScope.crm_company_id = result.data.crm_company_id;
                                    }
                                    $scope.percentage = Math.ceil(result.data.percentage);
                                } else{
                                    $rootScope.toast('Error',result.error,'error',$scope.sector);
                                }
                            })
                        }
                    };
                    break;
                case 'financial_information':
                    $scope.form_key = $rootScope.companyParames.form_key;
                    $scope.template = $rootScope.companyParames.template;
                    $scope.formName = $rootScope.companyParames.formName;
                    $scope.sectionName = "Financial Information";
                    $scope.statementId = $rootScope.companyParames.formId;
                    crmCompanyService.getFinancialData({
                        financial_statement_id:$rootScope.companyParames.formId,
                        crm_company_id:$rootScope.id_crm_company
                    }).then(function(result){
                        $scope.statementData = result.data;
                    });
                    $scope.uploadFinancialExcel = function(file){
                        console.log(file);
                        if(file!=null&&file!=''){
                            $scope.uploadFile = file;
                            $scope.isExcel = false;
                        }
                        /* else {
                         $scope.isExcel = true;
                         $rootScope.toast('Error', 'file format not supported file formats', 'image-error');
                         }*/
                    };
                    $scope.submitUpload = function(statement_id){
                        if($scope.uploadFile==null||$scope.uploadFile==''){
                            $scope.isExcel = true;
                        } else{
                            $scope.isExcel = false;
                        }
                        if($scope.uploadFile&&$scope.uploadFile!=null){
                            $scope.uploadData = {
                                'user_id':$rootScope.userId,
                                'crm_company_id':$rootScope.id_crm_company,
                                'financial_statement_id':statement_id
                            };
                            Upload.upload({
                                url:API_URL+'company/financialExcel',
                                data:{
                                    file:{'excel':$scope.uploadFile},
                                    'financeData':$scope.uploadData
                                }
                            }).success(function(result){
                                if(result.status){
                                    $rootScope.toast('Success',result.message);
                                    $scope.statementData = result.data;
                                    $scope.uploadFile = '';
                                    angular.element("input[type='file']").val(null);
                                    $state.reload()
                                } else{
                                    $rootScope.toast('Error',[result.error],'error',[]);
                                }
                            });
                        }
                    };
                    break;
                case 'business_assessment':
                    /*Add AddAssessment*/
                    $scope.form_key = $rootScope.companyParames.form_key;
                    $scope.template = $rootScope.companyParames.template;
                    $scope.formName = $rootScope.companyParames.formName;
                    $scope.sectionName = "Business Assessment";
                    $scope.getCompanyAssessment = function(id){
                        crmContactService.getCompanyAssessment({
                            assessment_id:$rootScope.companyParames.assessmentId,
                            type:$rootScope.companyParames.type,
                            crm_company_id:$rootScope.id_crm_company,
                            company_id:$rootScope.id_company,
                        }).then(function(result){
                            if(result.status){
                                $scope.companyAssessmentsItems = result.data[0].item;
                                $scope.tag = result.data[0].assessment_name;
                                $scope.companyAssessmentsId = result.data[0].id_company_assessment;
                                if(id){
                                    setTimeout(function(){
                                        var elm = $('#'+id).find('h1');
                                        elm.trigger("click");
                                    },500)
                                }
                            }
                            $scope.getKnowledgeDetails();
                        });

                    };
                    $scope.getCompanyAssessment();
                    $scope.addComment = function(comment,id_assessment_item){
                        var $params = {
                            //company_assessment_id:$scope.companyAssessmentsId,
                            crm_company_id:$rootScope.id_crm_company,
                            assessment_item_id:id_assessment_item,
                            'assessment_id':$rootScope.companyParames.assessmentId,
                            step_title:comment,
                            created_by:$rootScope.userId
                        };
                        crmContactService.addCompanyAssessmentItemStep($params).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                $scope.getCompanyAssessment(id_assessment_item);
                            } else{
                                $rootScope.toast('Error',result.error,'error',$scope.sector);
                            }
                        });
                    };
                    $scope.deleteComment = function(comment_id,id_assessment_item){
                        var r = confirm("Are you sure want to remove!");
                        if(r==false){
                            return false;
                        }
                        crmContactService.deleteCompanyAssessmentItemStep({id_company_assessment_item_step:comment_id}).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                $scope.getCompanyAssessment(id_assessment_item);
                            } else{
                                $rootScope.toast('Error',result.error,'error',$scope.sector);
                            }
                        });
                    }
                    break;
                default:
            }


        }

        $scope.init();


        $scope.parentSectorsList = [];
        masterService.sector.getAllSectors().then(function(result){
            $scope.parentSectorsList = result.data.data;
        });
        /* masterService.sector.getAll().then(function(result){
         $scope.parentSectorsList = result.data.data;
         });*/
        $scope.getSubSector = function(parentId){
            //console.log(parentId);
            masterService.sector.getSubSector(parentId).then(function(result){
                $scope.subSectorList = result.data;
            });
        };
        $scope.Countries = [];
        masterService.country.get().then(function($result){
            $scope.Countries = $result.data;
        });


        $scope.getIntelligenceDetails = function(){
            crmCompanyService.getIntelligenceData({
                'company_id':$rootScope.id_company,
                'sector_id':$scope.sectorId,
                'sub_sector_id':$scope.subSectorId,
                type:'crm_company',
                'crm_company_id':$rootScope.id_crm_company
            }).then(function(result){
                $scope.knowledgeDetails = result.data.list;
                $scope.peopleDetails = result.data.user_list;
            });
        };
        $scope.tag = '';
        $scope.getKnowledgeDetails = function(){
            crmCompanyService.getKnowledgeData({
                'company_id':$rootScope.id_company,
                'type':'crm_company',
                'tags':$rootScope.companyParames.tag,
                'crm_company_id':$rootScope.id_crm_company
            }).then(function(result){
                $scope.knowledgeData = result.data;
            });
        };
        $scope.getKnowledgeDetails();

        $scope.modalOpen = function(selectedItem){
            $scope.btnDisabled = false;
            if(selectedItem!=undefined){
                crmCompanyService.updateViewCount({'id_knowledge_document':selectedItem.id_knowledge_document}).then(function(result){
                    if(result.status){
                        //$rootScope.toast('Success', result.message);
                    } else{
                        //$rootScope.toast('Error', result.error, 'error', $scope.sector);
                    }
                })
            }

        };
    })

    .controller('CompanyPriviewCtrl',function($rootScope,$scope,$state,crmCompanyService,$stateParams,encodeFilter,anchorSmoothScroll,$location,underscoreaddFilter){
        $scope.gotoElement = function(eId){
            eId = underscoreaddFilter(eId);
            if(eId=='') return false;
            var elm = $('body').find('#'+eId);
            if(elm.length){
                $location.hash();
                anchorSmoothScroll.scrollTo(eId);
            }
        };
        $scope.currentCompanyViewId = encodeFilter($stateParams.id);
        crmCompanyService.getcompanyPriviewDetails({
            'company_id':$rootScope.id_company,
            'crm_company_id':encodeFilter($stateParams.id)
        }).then(function(result){
            $scope.companyBusinessAssessment = result.data;
        });
        crmCompanyService.companyDetails(encodeFilter($stateParams.id),2).then(function(result){
            $scope.businessKeyData = result.data.forms;
            $rootScope.business_name = result.data.details[0].company_name;
            $rootScope.sector = result.data.details[0].sector;
            $rootScope.subsector = result.data.details[0].sub_sector;
            $rootScope.companyDescription = result.data.details[0].company_description;
        });
        crmCompanyService.companyFinancialInformation({'crm_company_id':encodeFilter($stateParams.id)}).then(function(result){
            $scope.companyFinancialInformation = result.data;
        });
    })

    .controller('ProjectDashboardCtrl',function($scope,$rootScope,crmProjectService,$state,quickService,masterService,decodeFilter){
        $scope.projectList = [];
        $scope.totalProjectCount = 0;


        $scope.projectView = function($id_crm_project){
            $rootScope.currentProjectViewId = $id_crm_project;
            $state.go('project.project-list',{'id':$rootScope.currentProjectViewId});
        };

        $scope.gotoState = function(project){
            console.log('project',project);
            var id = decodeFilter(project.id_crm_project);
            $rootScope.currentProjectViewId = project.id_crm_project;
            if(project.user_access=='edit'){
                $state.go('project.project-list',{'id':id});
            } else if(project.user_access=='status_flow'){
                $state.go('project.project-approval-level.status-flow',{'id':id});
            } else{
                $state.go('project.project-overview',{'id':id});
            }
        }
        $scope.parentSectorsList = [];
        masterService.sector.getAllSectors().then(function(result){
            $scope.parentSectorsList = result.data.data;
        });
        $scope.subSectorList = [];
        $scope.getSubSector = function(parentId){
            masterService.sector.getSubSector(parentId).then(function(result){
                $scope.subSectorList = result.data;
            });
        };
        masterService.getProductType($rootScope.id_company).then(function(result){
            $scope.productTypeList = result.data.data;
        });
        $scope.displayed = false;
        crmProjectService.projectList($rootScope.id_company).then(function(result){
            $scope.totalProjectCount = result.data.total_records;
        });
        $scope.callServer = function(tableState){
            crmProjectService.projectList($rootScope.id_company,tableState).then(function(result){
                $scope.displayed = true;
                $scope.tableStateRef = tableState;
                $scope.projectList = result.data.data;
                //$scope.totalProjectCount = result.data.total_records;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/tableState.pagination.number);
            });
        };
        $scope.project = {};
        $scope.submitBtnDisabled = false;
        $scope.submitQuickProject = function($inputData,form,moreFields){
            ////console.log(form.$valid);
            if(form.$valid){
                $scope.submitBtnDisabled = true;
                $inputData.company_id = $rootScope.id_company;
                $inputData.created_by = $rootScope.userId;
                quickService.saveQuickProject($inputData).then(function(result){
                    $scope.submitBtnDisabled = false;
                    if(result.status){
                        $rootScope.toast('Success',result.message);
                        $scope.project = {};
                        $scope.submitted = false;

                        $scope.callServer($scope.tableStateRef);
                        if(moreFields){
                            $rootScope.currentProjectViewId = result.data.crm_project_id;
                            $state.go('project.project-list',{'id':decodeFilter($rootScope.currentProjectViewId)});
                        }
                        // $rootScope.crm_contact_id = result.data.crm_contact_id;
                    } else{
                        $rootScope.toast('Error',result.error,'error',result.error);
                    }
                })

            }
        };

    })

    .controller('ProjectEditCtrl',function($cookies,$rootScope,$scope,$sce,crmProjectService,$state,$uibModal,crmContactService,crmCompanyService,masterService,projectService,taskService,$stateParams,encodeFilter,decodeFilter){
        $rootScope.currentProjectViewId = encodeFilter($stateParams.id);
        if($rootScope.projectParames==undefined){
            $state.go("project.project-list",{'id':$stateParams.id});
            return false;
        }

        $scope.prevBtnShow = false;
        $scope.nextBtnShow = false;

        //goto next form
        $scope.nextForm = function(){
            // var a = //searching Element in Array
            var nextIndex = $scope.currentFormIndex+1;
            console.log('nextIndex',nextIndex);
            $rootScope.projectParames = $rootScope.projectFormDetails[nextIndex];
            $scope.init();
        }
        //goto prev form

        $scope.prevForm = function(){
            var nextIndex = $scope.currentFormIndex-1;
            console.log('nextIndex',nextIndex);
            $rootScope.projectParames = $rootScope.projectFormDetails[nextIndex];
            $scope.init();
        }

        $scope.init = function(){

            //find current index
            if($rootScope.projectParames.formId){

                var a = _.findWhere($rootScope.projectFormDetails,{'formId':$rootScope.projectParames.formId});//searching Element in Array
                $scope.currentFormIndex = _.indexOf($rootScope.projectFormDetails,a);// getting index.
                if($scope.currentFormIndex!= -1){
                    $scope.prevBtnShow = ($scope.currentFormIndex>0) ? true : false;
                    $scope.nextBtnShow = ($scope.currentFormIndex<($rootScope.projectFormDetails.length-1)) ? true : false;
                }
                console.log('$rootScope.projectFormDetails.length',($rootScope.projectFormDetails.length-1));
                console.log('$scope.currentFormIndex',$scope.currentFormIndex);
            }

            crmProjectService.projectDetails($rootScope.currentProjectViewId,3,$rootScope.userId).then(function(result){
                $rootScope.projectDetails = result.data.details[0];
                $scope.businessKeyData = result.data.forms;
                $rootScope.formStatus = result.data.active_forms;
                $rootScope.reportingUser = result.data.reporting;
                if($rootScope.projectDetails.assigned_to==$rootScope.userId){
                    $rootScope.approvalStatus = true;
                }
            });

            crmProjectService.projectFormDetails($rootScope.currentProjectViewId,$rootScope.projectParames.formId,'edit').then(function(result){
                $scope.project = {};
                $scope.project = result.data.data;
                $scope.percentage = Math.ceil(result.data.percentage);
                $scope.project_title = result.data.project_details.project_title;
                $scope.project_description = result.data.project_details.project_description;
                $scope.project_created_date = result.data.project_details.created_date_time;
                //$scope.template = $rootScope.projectParames.template;
                $scope.sectorId = result.data.project_details.project_main_sector_id;
                $scope.subSectorId = result.data.project_details.project_sub_sector_id;
                $scope.getIntelligenceDetails();
                $scope.getKnowledgeDetails();
            });
            switch($rootScope.projectParames!=undefined&&$rootScope.projectParames.sectionName){
                case 'general_information':
                    $scope.sectionName = $rootScope.projectParames.tag;
                    crmProjectService.projectFormDetails($rootScope.currentProjectViewId,$rootScope.projectParames.formId,'edit').then(function(result){
                        $scope.project = result.data.data;
                        $scope.percentage = Math.ceil(result.data.percentage);
                        $scope.project_title = result.data.project_details.project_title;
                        $scope.project_created_date = result.data.project_details.created_date_time;
                        $scope.template = $rootScope.projectParames.template;
                        $scope.formName = result.data.data.form_name;
                    });
                    $scope.submitForm = function($inputData,$form_data,form){
                        $scope.submitStatus = true;
                        if(form.$valid){
                            $inputData.company_id = $rootScope.id_company;
                            $inputData.form_id = $rootScope.projectParames.formId;
                            $inputData.user_id = $rootScope.userId;

                            if($rootScope.currentProjectViewId!=undefined){
                                $inputData.crm_project_id = $rootScope.currentProjectViewId;
                            }
                            crmProjectService.saveProject($inputData).then(function(result){
                                //console.log(result);
                                if(result.status){
                                    // $scope.checkForm.push($form_data.id_form);
                                    $rootScope.toast('Success',result.message);
                                    if($rootScope.currentProjectViewId==undefined){
                                        $rootScope.crm_project_id = result.data.crm_project_id;
                                    }
                                    $scope.percentage = Math.ceil(result.data.percentage);
                                } else{
                                    $rootScope.toast('Error',result.error,'error',$scope.sector);
                                }
                            })
                        }
                    };
                    break;
                case 'terms_and_conditions':
                case 'summary_and_recommendations':
                    $scope.condition = '';
                    if($rootScope.projectParames.sectionName=='terms_and_conditions')
                        $scope.sectionName = $scope.formName = "Terms and Conditions";
                    else
                        $scope.sectionName = $scope.formName = "Summary and Recommendations";
                    console.log($rootScope.projectParames);
                    crmProjectService.projectTerm.projectTremesAndConditions({
                        product_term_key:'terms_and_conditions',
                        product_term_id:$rootScope.projectParames.formId,
                        company_id:$rootScope.id_company,
                        crm_project_id:$rootScope.currentProjectViewId,
                        product_term_type:$rootScope.projectParames.product_term_type
                    }).then(function(result){
                        $scope.projectTermAndConditions = result.data;
                        $scope.template = $rootScope.projectParames.template;
                    });
                    $scope.loadTermAndCondition = function(condition){
                        $scope.item = condition;
                    }
                    $scope.submitCondition = function(obj){
                        crmProjectService.projectTerm.projectTremesAndConditionUpdate({
                            item_data:obj.answer,
                            product_term_item_id:obj.id_product_term_item,
                            crm_project_id:$rootScope.currentProjectViewId,
                            created_by:$rootScope.userId
                        }).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                            } else{
                                $rootScope.toast('Error',result.error,'error',$scope.sector);
                            }
                        });
                    };

                    break;
                case 'risk_assessment':
                    $scope.template = $rootScope.projectParames.template;
                    $scope.formName = $scope.sectionName = 'Risk Assessment';
                    break;
                case 'pre_disbursement_checklist':
                    $scope.template = $rootScope.projectParames.template;
                    $scope.formName = $scope.sectionName = 'Pre-Disbursement Checklist';
                    break;
                case 'restriction_limit':
                    $scope.template = $rootScope.projectParames.template;
                    $scope.formName = $scope.sectionName = 'Restrictions, Limits, Approval Authorities';
                    break;
                case 'covenant':
                    $scope.template = $rootScope.projectParames.template;
                    $scope.formName = $scope.sectionName = 'Covenant';
                    break;
                default:
            }
        }
        $scope.init();

        masterService.sector.getAll().then(function(result){
            $scope.parentSectorsList = result.data.data;
        });
        $scope.getSubSector = function(parentId){
            //console.log(parentId);
            masterService.sector.getSubSector(parentId).then(function(result){
                $scope.subSectorList = result.data;
            });
        };

        $scope.knowledgeDetails = [];
        $scope.peopleDetails = [];
        $scope.getIntelligenceDetails = function(){
            crmCompanyService.getIntelligenceData({
                'company_id':$rootScope.id_company,
                'sector_id':$scope.sectorId,
                'sub_sector_id':$scope.subSectorId,
                type:'crm_project',
                'crm_project_id':$rootScope.currentProjectViewId
            }).then(function(result){
                $scope.knowledgeDetails = result.data.list;
                $scope.peopleDetails = result.data.user_list;
            });
        };
        $scope.knowledgeData = [];
        $scope.getKnowledgeDetails = function(){
            crmCompanyService.getKnowledgeData({
                'company_id':$rootScope.id_company,
                'type':'crm_project',
                'tags':$rootScope.projectParames.tag,
                'crm_project_id':$rootScope.currentProjectViewId
            }).then(function(result){
                $scope.knowledgeData = result.data;
            });
        };
        $scope.settings = {
            colHeaders:true,
            manualColumnResize:true,
            contextMenu:['row_above','row_below','remove_row']
        };
        $scope.anotherOptions = {
            barColor:'#02B000',
            trackColor:'#f9f9f9',
            scaleColor:'#dfe0e0',
            scaleLength:5,
            lineCap:'round',
            lineWidth:3,
            size:50,
            rotate:0,
            animate:{
                duration:1000,
                enabled:true
            }
        };
        $scope.minSpareRows = 1;
        $scope.rowHeaders = false;

        $scope.modalOpen = function(selectedItem){
            $scope.btnDisabled = false;
            if(selectedItem!=undefined){
                crmCompanyService.updateViewCount({'id_knowledge_document':selectedItem.id_knowledge_document}).then(function(result){
                    if(result.status){
                        //$rootScope.toast('Success', result.message);
                    } else{
                        //$rootScope.toast('Error', result.error, 'error', $scope.sector);
                    }
                })
            }
        };
    })

    .controller('projectPriviewCtrl',function($rootScope,$scope,$state,crmProjectService,$stateParams,encodeFilter,anchorSmoothScroll,$location,underscoreaddFilter){
        $scope.gotoElement = function(eId){
            eId = underscoreaddFilter(eId);
            if(eId=='') return false;
            var elm = $('body').find('#'+eId);
            if(elm.length){
                $location.hash();
                anchorSmoothScroll.scrollTo(eId);
            }
        };
        $rootScope.currentProjectViewId = encodeFilter($stateParams.id);
        /*http://localhost/q-lana/rest/crm/projectTerms/?crm_project_id=50&company_id=1*/
        crmProjectService.projectTerm.projectTerms({
            'company_id':$rootScope.id_company,
            'crm_project_id':$rootScope.currentProjectViewId
        }).then(function(result){
            $scope.projectTermPreviewDetails = result.data;
        });
        crmProjectService.projectDetails($rootScope.currentProjectViewId,3,$rootScope.userId).then(function(result){
            $rootScope.projectDetails = result.data.details[0];
            $scope.businessKeyData = result.data.forms;
            $rootScope.formStatus = result.data.active_forms;
            $rootScope.reportingUser = result.data.reporting;
            if($rootScope.projectDetails.assigned_to==$rootScope.userId){
                $rootScope.approvalStatus = true;
            }
        });
    })

    .controller('ProjectListCtrl',function($rootScope,$scope,$sce,$q,crmProjectService,$state,$uibModal,crmContactService,crmCompanyService,projectService,taskService,$stateParams,encodeFilter,decodeFilter){
        $rootScope.currentProjectViewId = encodeFilter($stateParams.id);


        if($rootScope.currentProjectViewId==undefined){
            $state.go('project.project-dashboard');
            return false;
        }
        $rootScope.approvalStatus = false;

        $scope.init = function(){

            $rootScope.currentModuleId = 3;

            $rootScope.projectFormDetails = [];
            $rootScope.projectDetails = [];
            $scope.projectForms = [];
            $rootScope.formStatus = [];
            $rootScope.reportingUser = [];

            $scope.projectProductTerm = crmProjectService.projectProductTerm({'crm_project_id':$rootScope.currentProjectViewId}).then(function(result){
                $scope.ProductTerm = result.data;

            });

            $scope.projectDetailsFun = crmProjectService.projectDetails($rootScope.currentProjectViewId,$rootScope.currentModuleId,$rootScope.userId).then(function(result){

                $rootScope.projectDetails = result.data.details[0];
                $scope.projectForms = result.data.forms;
                $rootScope.formStatus = result.data.active_forms;
                $rootScope.reportingUser = result.data.reporting;


                if($rootScope.projectDetails.assigned_to==$rootScope.userId){
                    $rootScope.approvalStatus = true;
                }
                return $scope.projectForms;

            });


            $q.all([$scope.projectDetailsFun,$scope.projectProductTerm]).then(function(){

                angular.forEach($scope.projectForms,function(section){
                    angular.forEach(section.form,function($item){
                        var $obj = {};
                        $obj.sectionName = 'general_information';
                        $obj.tag = $item.form_name;
                        $obj.formId = $item.id_form;
                        $obj.template = $item.form_template;
                        $obj.product_term_type = $item.product_term_type;
                        $rootScope.projectFormDetails.push($obj);
                    })
                })
                angular.forEach($scope.ProductTerm,function(section){

                    var $obj = {};
                    $obj.sectionName = section.product_term_key;
                    $obj.tag = section.product_term_name;
                    $obj.formId = section.id_product_term;
                    $obj.template = section.product_term_template;
                    $obj.product_term_type = section.product_term_type;
                    $rootScope.projectFormDetails.push($obj);
                })
            })
            $rootScope.projectApprovalsFun();
        }
        $rootScope.contactId = $rootScope.currentProjectViewId;

        $scope.goToEditCtrl = function(data){
            $rootScope.projectParames = data;
            $state.go("project.project-keydata",{'id':decodeFilter($rootScope.currentProjectViewId)});
        };
        $scope.goToState = function(data){
            $state.go(data);
        }

        $scope.getStatus = function(form){
            $scope.status = false;
            angular.forEach($rootScope.formStatus,function(formStatus,index){

                if(formStatus.id==form.id_form&&formStatus.status==1){
                    $scope.status = true;

                }
            });
            if($scope.status){
                return "box-active";
            }

        }

        $rootScope.selectedCompanyList = [];
        $rootScope.selectedCompanyListFun = function(){
            crmCompanyService.projectCompanyListFun({'crm_project_id':$rootScope.currentProjectViewId}).then(function(result){
                $rootScope.selectedCompanyList = result.data;
            });
        };


        $rootScope.projectContactList = [];
        $rootScope.projectContactListFun = function(){
            projectService.getProjectContact({'crm_project_id':$rootScope.currentProjectViewId}).then(function(result){
                $rootScope.projectContactList = result.data;
            });
        };


        $rootScope.projecTeamtContactList = [];
        $rootScope.projectTeamContactListFun = function(){
            projectService.getProjectTeamContact($rootScope.userId).then(function(result){
                //console.log(result.data);
                $rootScope.projectTeamContactList = result.data;
            });
        };


        $rootScope.projectApprovalsList = [];
        $rootScope.projectApprovalsFun = function(){
            var getParams = 'project_id='+$rootScope.currentProjectViewId;
            if($scope.activity!=''){
                getParams = getParams+'&activity_type='+$scope.activity;
            }
            projectService.projectApprovals(getParams).then(function(result){
                $rootScope.projectApprovalsList = result.data;
            });
        };


        $scope.activity = '';
        $scope.updateApprovals = function(filterValue){
            $scope.activity = '';
            if(filterValue!=''){
                $scope.activity = filterValue;
            }
            $rootScope.projectApprovalsFun();
        }

        /*add TouchPoint*/
        $scope.showReminders = false;
        $scope.showAllPoints = false;
        $scope.touchPointDescription = '';
        $scope.serchType = '';
        crmContactService.touchPointType().then(function(result){
            $scope.touchPointList = result.data;
        });
        $scope.postField = {'reminder_date':new Date()};
        $scope.submit = function(postField,form){
            $scope.submitStatus = true;
            //console.log(form.$valid);
            if(form.$valid){
                postField.crm_module_id = $rootScope.currentModuleId;
                postField.touch_point_from_id = $rootScope.contactId;
                postField.created_by = $rootScope.userId;
                //console.log('postField', postField);
                crmContactService.addTouchPointComment(postField).then(function(result){
                    $scope.postField = {'reminder_date':new Date()};
                    $scope.submitStatus = false;
                    $scope.togglePoint = false;
                    if(result.status){
                        $rootScope.toast('Success',result.message);
                        $scope.togglePoint = false;
                        $state.reload();
                    } else{
                        $rootScope.toast('Error',result.error,'error',$scope.sector);
                    }

                });
            }
        }
        /*Attachments*/
        $scope.quickAttachmentModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/contact/file-attachment.html',
                controller:'attachmentsCtrl'
            });
            modalInstance.result.then(function(){
            },function(){
                $rootScope.projectApprovalsFun();
                $state.reload();
            });
        };
        /*Touch points*/
        $scope.touchpointsModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                backdrop:'static',
                keyboard:false,
                windowClass:'my-class',
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/contact/touchpoints.html',
                controller:'touchPointsCtrl',
                resolve:{
                    crmParams:{
                        'crm_module_type':'project',
                        'crm_module_id':$rootScope.currentProjectViewId
                    }
                }
            });
            modalInstance.result.then(function(){
            },function(){
                $rootScope.projectApprovalsFun();
                $state.reload();
            });
        };
        /*Added Contacts Modal*/
        $rootScope.AddedContactsModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/project/added-contacts.html',
                controller:function($scope,$uibModalInstance,crmCompanyService,projectService){
                    $scope.getContactList = function(search_key){
                        $scope.visibleAddButton = false;
                        return crmContactService.contactList($rootScope.id_company,{
                            'crm_project_id':$rootScope.currentProjectViewId,
                            'search_key':search_key
                        }).then(function(result){
                            return result.data.data;
                        });
                    };
                    $scope.deleteProjectContact = function(contactId){
                        var r = confirm("Are you sure want to remove!");
                        if(r==true){
                            return crmContactService.deleteProjectContact(contactId).then(function(result){
                                $rootScope.toast('Success',result.data.message);
                                $rootScope.projectContactListFun();
                            });
                        }
                    };

                    $scope.onSelect = function($item,$model,$label){
                        $scope.visibleAddButton = true;
                        //console.log('$item', $item);
                        $scope.selectedItem = $item;
                        $scope.visibleAddButton = false;
                        $scope.visibleDesignationDropDown = true;
                        $scope.asyncSelected = '';

                        //$rootScope.companyList.unshift($scope.selectedItem);
                        crmContactService.contactType().then(function(response){
                            $scope.contactType = response.data;
                        });
                    };
                    $scope.save = function(designation,form){
                        $scope.submitStatus = true;
                        if(form.$valid){
                            var fields = {
                                'crm_project_id':$rootScope.currentProjectViewId,
                                'crm_contact_id':$scope.selectedItem.id_crm_contact,
                                'created_by':$rootScope.userId,
                                contact_type_id:designation.contact_type_id
                                /*is_primary_contact: designation.is_primary_contact*/
                            }
                            projectService.projectContact(fields).then(function(result){
                                if(result.status){
                                    $scope.visibleDesignationDropDown = false;
                                    $rootScope.toast('Success',result.message);
                                    $rootScope.projectContactListFun();
                                } else{
                                    $rootScope.toast('Error',result.error,'error',$scope.sector);
                                }
                            });
                        }
                    };
                    $scope.cancelAssign = function(){
                        $scope.visibleDesignationDropDown = false;
                    };
                    $scope.visibleDesignationDropDown = false;
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function(){
            },function(){
                $state.reload();
            });
        };
        /*Added Companies Modal*/
        $rootScope.AddedCompaniesModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/project/added-companies.html',
                controller:function($scope,$uibModalInstance,crmCompanyService){
                    /*$rootScope.selectedCompanyListFun();*/
                    $scope.getCompanyList = function(search_key){
                        $scope.visibleAddButton = false;
                        return crmCompanyService.getCompanyList({
                            'crm_project_id':$rootScope.currentProjectViewId,
                            'search_key':search_key
                        }).then(function(result){
                            return result.data.data;
                        });
                    };
                    $scope.onSelect = function($item,$model,$label){
                        $scope.visibleAddButton = true;
                        //console.log('$item', $item);
                        $scope.selectedItem = $item;
                        $scope.visibleAddButton = false;
                        $scope.visibleDesignationDropDown = true;
                        $scope.asyncSelected = '';
                        //company types
                        crmCompanyService.companyType().then(function(response){
                            $scope.companyType = response.data;
                            //console.log(response.data);
                        });
                    };
                    $scope.visibleDesignationDropDown = false;
                    $scope.save = function(designation,form){
                        $scope.submitStatus = true;
                        if(form.$valid){
                            var fields = {
                                'crm_project_id':$rootScope.currentProjectViewId,
                                'crm_company_id':$scope.selectedItem.id_crm_company,
                                'company_type_id':designation.company_type_id,
                                /*'is_primary_company': designation.is_primary_company,*/
                                'created_by':$rootScope.userId
                            };
                            crmCompanyService.projectCompany(fields).then(function(result){
                                if(result.status){
                                    $scope.visibleDesignationDropDown = false;
                                    $rootScope.toast('Success',result.message);
                                    $scope.selectedCompanyListFun();
                                } else{
                                    $rootScope.toast('Error',result.error,'error',$scope.sector);
                                }

                            });
                        }
                    };
                    $scope.deleteProjectCompany = function($id){
                        var r = confirm("Are you sure want to remove!");
                        if(r==true){
                            crmCompanyService.deleteProjectCompany($id).then(function(result){
                                $rootScope.toast('Success',result.data.message);
                                $rootScope.selectedCompanyListFun();
                            });
                        }
                    };
                    $scope.cancelAssign = function(){
                        $scope.visibleDesignationDropDown = false;
                    };
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function(){
            },function(){
                $state.reload();
            });
        };
        /*Team modal*/
        $rootScope.teamModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                openedClass:'right-panel-modal modal-open',
                backdrop:'static',
                keyboard:false,
                templateUrl:'partials/crm/project/project-team-modal.html',
                controller:function($scope,$uibModalInstance,companyService,projectService){
                    $scope.getContactList = function(search_key){
                        $scope.visibleAddButton = false;
                        return companyService.userList($rootScope.id_company,{
                            'crm_project_id':$rootScope.currentProjectViewId,
                            'search_key':search_key
                        }).then(function(result){
                            return result.data.data;
                            //console.log(result.data.data);
                        });
                    };
                    $scope.selectedItems = [];
                    $scope.onSelect = function($item,$model,$label){
                        /*projectService.addProjectTeam({'crm_project_id':$rootScope.currentProjectViewId,'team_member_id':$item.id_company_user,'created_by':$rootScope.userId}).then(function(result){
                         $rootScope.projectTeamContactListFun();
                         });*/
                        //console.log('$item', $item);
                        $scope.selectedItem = $item;
                        $scope.searchTeam = '';
                    };

                    $scope.visibleDesignationDropDown = false;
                    $scope.save = function(){

                    };
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
        }
        /*Tasks Modal*/
        $scope.addTasksModal = function(id){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/project/add-tasks-modal.html',
                controller:function($scope,$uibModalInstance,companyService,taskService,Upload){
                    $scope.taskDetails = [];
                    $scope.tasksList = function(){
                        taskService.getTasks({'project_id':$rootScope.currentProjectViewId}).then(function(result){
                            $scope.taskDetails = result.data.data;
                            //console.log($rootScope.taskDetails);
                        });
                    };
                    $scope.tasksList();
                    $scope.showDetailtaskList = false;
                    $scope.currentView = 'List';
                    if(id!=undefined){
                        $scope.currentView = 'View';
                    }
                    companyService.userList($rootScope.id_company).then(function(result){
                        $scope.companyUsers = result.data.data;
                    });
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.submitStatus = false;
                    $scope.saveTask = function(taskDetails,form){
                        //console.log('naresh gurrala',taskDetails,form.$valid);
                        /*return false;*/
                        $scope.submitStatus = true;
                        if(form.$valid){
                            taskDetails.project_id = $rootScope.currentProjectViewId;
                            taskDetails.created_by = $rootScope.userId;
                            //taskDetails.member = taskDetails.assigned_to;
                            delete taskDetails.assigned_to;
                            //console.log(taskDetails);
                            Upload.upload({
                                async:true,
                                url:API_URL+'activity/task',
                                data:{
                                    file:{'attachment':$scope.taskDocuments},
                                    'task':taskDetails
                                }
                            }).success(function(data){
                                if(data.status){
                                    $rootScope.toast('Success',data.message);
                                    $scope.currentView = 'List';
                                    $scope.task = {};
                                    $scope.taskDocuments = {};
                                    $scope.tasksList();
                                    $rootScope.projectApprovalsFun();
                                }
                                else{
                                    $rootScope.toast('Error',data.error,'error',$scope.sector);
                                }
                            });
                        }
                    };
                    $scope.uploadFiles = function(file){
                        $scope.taskDocuments = file;
                        //console.log($scope.taskDocuments);
                    };
                    $scope.removeImage = function(obj){
                        $scope.taskDocuments.splice(obj.$$hashKey,1);
                    };
                    $rootScope.viewtaskDetails = [];
                    $scope.viewTask = function(taskId){
                        taskService.getTasks({'id_task':taskId}).then(function(result){
                            if(result.status){
                                $scope.viewtaskDetails = result.data.data[0];
                                $scope.currentView = 'View';
                            }
                            else{
                                $rootScope.toast('Error',result.error,'error',$scope.sector);
                            }

                        });

                    };
                    if(id!=undefined){
                        $scope.viewTask(id);
                    }
                    $scope.previousFiles = [];
                    $scope.task = {};
                    $scope.editTask = function(taskId){
                        $scope.id_task = taskId;
                        taskService.getTasks({'id_task':taskId}).then(function(result){
                            var res = result.data.data[0];
                            $scope.task.task_name = res.task_name;
                            $scope.task.task_description = res.task_description;
                            $scope.task.task_due_date = res.task_due_date;
                            $scope.task.member = res.member;
                            $scope.task.id_task = res.id_task;
                            $scope.previousFiles = res.attachment;
                            $scope.currentView = 'Form';

                        });
                    };
                    $scope.deleteTask = function(taskId){
                        var r = confirm("Are you sure want to remove!");
                        if(r==true){
                            taskService.deleteTask({'id_task':taskId}).then(function(result){
                                if(result.status){
                                    $rootScope.toast('Success',result.message);
                                    $scope.currentView = 'List';
                                    $scope.tasksList();
                                }
                                else{
                                    $rootScope.toast('Error',result.error,'error',$scope.sector);
                                }
                            });
                        }
                    };
                    $scope.updateTaskDetails = {};
                    $scope.updateStatus = function(taskId,taskStatus){
                        $scope.updateTaskDetails = {
                            'id_task':taskId,
                            'task_status':taskStatus
                        };
                        taskService.updateTaskStatus($scope.updateTaskDetails).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                $scope.currentView = 'List';
                                $scope.tasksList();
                                $scope.task = {};
                            }
                            else{
                                $rootScope.toast('Error',result.error,'error',$scope.sector);
                            }
                        });
                    };
                    $scope.goToList = function(){
                        $scope.taskList = false;
                        $scope.showDetailtaskList = false;
                    };
                    $scope.closeViewTask = function(){
                        $scope.showDetailtaskList = false;
                    };
                    $scope.showViewTask = function(index){
                        $scope.showIndex = index;
                    };


                    //get attachments types based on contact
                    crmContactService.getAttachmentsTypes($rootScope.currentModuleId).then(function(result){
                        if(result.status){
                            $scope.documentsTypes = result.data;
                        }
                    });
                    //get uploaded attachments
                    $scope.getFiles = function(){
                        crmContactService.getUploadedAttachments($rootScope.currentModuleId,$rootScope.currentProjectViewId).then(function(result){
                            if(result.status){
                                $scope.uploadedDocuments = result.data;
                            }
                        });
                    }
                    $scope.getFiles();


                }
            });
            modalInstance.result.then(function(){
            },function(){
                $state.reload();
            });

        }
        /*meating modal*/
        $scope.meetingModal = function(id){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/project/meeting-modal.html',
                controller:function($scope,$rootScope,$uibModalInstance,crmContactService,companyService,Upload,projectService){
                    $scope.minDate = $scope.minDate ? null : new Date();
                    //$scope.meeting = {};
                    $scope.MeetingFormShow = false;
                    $scope.meetingView = function($id_meeting){
                        projectService.ProjectMeeting.get({'id_meeting':$id_meeting}).then(function(result){
                            if(result.status){
                                $scope.meeting = {};
                                // //console.log(result.data);
                                $scope.meetingDetails = result.data.data;
                                $scope.meeting = result.data.data;
                                //$scope.meetingDocuments = [];
                                $scope.meeting.invitation_id = result.data.data.invitation_id;
                                //$date =new Date(result.data.data.when);
                                $scope.meetingDocuments = [];
                                /*$scope.meeting.when = result.data.data.when.split(' ')[0];*/

                            }
                        });
                    }
                    if(id!=undefined){
                        $scope.meetingView(id);
                        $scope.currentView = 'View'
                    }
                    $scope.timeList = function(){
                        var $time = [];
                        var $mintIncrement = 10;
                        var hr = 24;
                        for(i = 0; i<24; i++){
                            for(j = 0; j<60; j = j+$mintIncrement){
                                $obj = {};
                                var jVal = (j<10) ? '0'+''+j : j;
                                var iVal = (i<10) ? '0'+''+i : i;
                                $obj.id = iVal+':'+jVal;
                                $obj.value = iVal+':'+jVal;
                                $time.push($obj);
                            }
                        }
                        return $time;
                    }
                    $rootScope.timeListData = $scope.timeList();
                    $scope.meeting = {};
                    $scope.meeting.invitation_id = [];
                    $scope.saveMeeting = function($meeting,form){
                        // console.log($meeting);return false;
                        $scope.submitStatus = true;

                        if(form.$valid){
                            $formData = {};
                            $formData.project_id = $rootScope.currentProjectViewId;

                            $formData.created_by = $rootScope.userId;
                            $formData.meeting_name = $meeting.meeting_name;
                            $formData.where = $meeting.where;
                            $formData.when = $meeting.when;
                            $formData.from_time = $meeting.from_time;
                            $formData.to_time = $meeting.to_time;
                            $formData.agenda = $meeting.agenda;
                            $formData.meeting_mom = $meeting.meeting_mom;
                            $formData.meeting_status = $meeting.meeting_status;
                            if($meeting.id_meeting!=undefined){
                                $formData.id_meeting = $meeting.id_meeting;
                            }

                            //delete $meeting.invitation_user_name;
                            //delete $meeting.updated_date_time;
                            //delete $meeting.created_date_time;
                            //delete $meeting.meeting_type;
                            //delete $meeting.when_date;
                            //delete $meeting.meeting_expiry_status;
                            //delete $meeting.user_name;
                            //delete $meeting.attachment;
                            var $invitation = $meeting.invitation_id.map(function(obj){
                                //var rObj = {};
                                //rObj[obj.id_user] = obj.value;
                                if(obj.id_user!=undefined&&obj.id_user!=''){
                                    return obj.id_user;
                                }
                            });
                            $formData.invitation_id = $invitation;

                            $scope.submitStatus = false;
                            Upload.upload({
                                async:true,
                                url:API_URL+'activity/meeting',
                                data:{
                                    file:{'attachment':$scope.meetingDocuments},
                                    'meeting':$formData
                                }
                            }).then(function(data){
                                $scope.currentView = 'List';
                                $scope.meeting = {};
                                $scope.meeting.invitation_id = [];

                                if(data.status){
                                    $rootScope.toast('Success',data.message);
                                    $scope.getMeetingList();
                                    $rootScope.projectApprovalsFun();

                                } else{
                                    $rootScope.toast('Error',data.error,'error');
                                }
                            });

                        }
                    }

                    $scope.deleteMeeting = function($id_meeting){
                        var r = confirm("Are you sure want to remove!");
                        if(r==true){
                            projectService.ProjectMeeting.delete({
                                'id_meeting':$id_meeting,
                                'meeting_status':'completed'
                            }).then(function(){
                                $scope.getMeetingList();
                            });
                        }
                    }
                    $scope.uploadFiles = function(file){
                        $scope.meetingDocuments = file;
                        //console.log($scope.meetingDocuments);
                    };
                    $scope.removeImage = function(obj){
                        //console.log(obj);
                        //console.log(obj.$$hashKey);
                        $scope.meetingDocuments.splice(obj.$$hashKey,1);
                    };

                    $scope.getMeetingList = function(){
                        projectService.ProjectMeeting.get({'project_id':$rootScope.currentProjectViewId}).then(function(result){
                            if(result.status){
                                $scope.meetingList = result.data.data;
                            }
                        });
                    }
                    $scope.getMeetingList();
                    $scope.userList = [];
                    $scope.multipleSettings = {
                        enableSearch:true,
                        showCheckAll:false,
                        showUncheckAll:false,
                        smartButtonMaxItems:5,
                        idProp:'id_user',
                        externalIdProp:'id_user',
                        smartButtonTextConverter:function(itemText,originalItem){
                            return itemText;
                        }
                    };
                    companyService.userList($rootScope.id_company).then(function(result){
                        if(result.status){
                            $.each(result.data.data,function($key,$data){
                                var $item = {};
                                $item.id_user = $data.id_user;
                                $item.label = $data.first_name+' '+$data.last_name;
                                $scope.userList.push($item);
                            })
                            // $scope.userList = result.data.data;
                        }
                    });

                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                        $state.reload();
                    };
                    //get uploaded attachments
                    $scope.getFiles = function(){
                        crmContactService.getUploadedAttachments($rootScope.currentModuleId,$rootScope.currentProjectViewId).then(function(result){
                            if(result.status){
                                $scope.uploadedDocuments = result.data;
                            }
                        });
                    }
                    $scope.getFiles();
                    //discussion
                    $scope.addComment = function(discussion){
                        discussion.discussion_type = 'meeting';
                        discussion.discussion_reference_id = $scope.meeting.id_meeting;


                    }
                }
            });
            modalInstance.result.then(function(){
            },function(){
                $state.reload();
            });
        }
        /*submit for approval modal*/
        $scope.submitforapprovalModal = function(buttonType){
            $rootScope.submitType = buttonType;
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/project/submit-for-approval-modal.html',
                controller:'submitForApprovalModalCtrl'
            });
            modalInstance.result.then(function(){
            },function(){
                $state.reload();
            });
        }
        $scope.selectedCompanyListFun();
        $rootScope.projectContactListFun();
        $rootScope.projectTeamContactListFun();

    })

    .controller('ProjectApprovalCtrl',function($rootScope,$scope,$sce,crmProjectService,masterService,projectService,$state,$uibModal,crmContactService,crmCompanyService,projectService,taskService,$stateParams,encodeFilter,decodeFilter){
        $rootScope.currentProjectViewId = encodeFilter($stateParams.id);
        if($rootScope.currentProjectViewId==undefined){
            $state.go('project.project-dashboard');
            return false;
        }
        $rootScope.approvalStatus = false;

        $rootScope.projectDetails = [];
        $scope.projectForms = {};
        $rootScope.formStatus = [];
        $rootScope.reportingUser = {};
        $rootScope.approvalStatus = false;
        $scope.approvalLevels = [];

        $scope.statusFlow = function(){
            $scope.currentCommitteeId = $rootScope.projectDetails.committee_id;
            console.log($rootScope.projectDetails);
            crmProjectService.statusFlow.get({
                'crm_project_id':$rootScope.currentProjectViewId,
                'company_id':$rootScope.id_company
            }).then(function($result){
                if($result.status){
                    $scope.approvalLevels = $result.data;
                }
            })
        }
        $scope.init = function(){

            crmProjectService.projectProductTerm().then(function(result){
                $scope.ProductTerm = result.data;
            });
            $rootScope.currentModuleId = 3;

            crmProjectService.projectDetails($rootScope.currentProjectViewId,$rootScope.currentModuleId,$rootScope.userId).then(function(result){
                $rootScope.projectDetails = result.data.details[0];
                $scope.projectForms = result.data.forms;
                $rootScope.formStatus = result.data.active_forms;
                $rootScope.reportingUser = result.data.reporting;
                if($rootScope.projectDetails.assigned_to==$rootScope.userId){
                    $rootScope.approvalStatus = true;
                }
                $scope.statusFlow();
            });


        }
        $scope.editForm = function(id_project_approval,id_meeting){
            $scope.meetingModal(id_project_approval,id_meeting)
        };
        $scope.meetingModal = function(id_project_approval,id_meeting){
            $scope.id_project_approval = id_project_approval;
            /*  if(id_meeting){
             $scope.id_meeting = id_meeting;
             }*/
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                scope:$scope,
                openedClass:'right-panel-modal modal-open',
                resolve:{
                    item:function(){
                        console.log('id_meeting',id_meeting);
                        if(id_meeting){
                            return projectService.ProjectMeeting.get({'id_meeting':id_meeting}).then(function($result){
                                console.log('$result',$result);
                                return $result.data.data;
                            });
                        }
                    }
                },
                templateUrl:'partials/crm/project/meeting-min-modal.html',
                controller:function($scope,$rootScope,$uibModalInstance,item,crmContactService,companyService,Upload,projectService){

                    $scope.meeting = {};
                    $scope.meeting.invitation_id = [];
                    $scope.submitStatus = false;
                    $scope.meetingDocuments = [];
                    $scope.meetingDetails = {};
                    $scope.meetingDetails.attachment = [];
                    $scope.meeting.invitation_id = [];
                    $scope.meeting.guest = [];
                    $scope.minDate = $scope.minDate ? null : new Date();
                    $scope.timeList = function(){
                        var $time = [];
                        var $mintIncrement = 10;
                        var hr = 24;
                        for(i = 0; i<24; i++){
                            for(j = 0; j<60; j = j+$mintIncrement){
                                $obj = {};
                                var jVal = (j<10) ? '0'+''+j : j;
                                var iVal = (i<10) ? '0'+''+i : i;
                                $obj.id = iVal+':'+jVal;
                                $obj.value = iVal+':'+jVal;
                                $time.push($obj);
                            }
                        }
                        return $time;
                    }
                    $rootScope.timeListData = $scope.timeList();

                    if(item){
                        console.log('item Ct',item);
                        $scope.meeting = item;
                        if(item.guest!=null){
                            $scope.meeting.guest = item.guest.split(',');
                            //console.log('$scope.meeting.guest',$scope.meeting.guest);
                        }

                    }


                    $scope.saveMeeting = function($meeting,form){
                        // console.log($meeting);return false;
                        $scope.submitStatus = true;

                        if(form.$valid){
                            var $formData = {};
                            $formData.project_id = $rootScope.currentProjectViewId;
                            $formData.project_approval_id = $scope.id_project_approval;
                            $formData.created_by = $rootScope.userId;
                            $formData.meeting_name = $meeting.meeting_name;
                            $formData.where = $meeting.where;
                            $formData.when = $meeting.when;
                            $formData.from_time = $meeting.from_time;
                            $formData.to_time = $meeting.to_time;
                            $formData.agenda = $meeting.agenda;
                            $formData.meeting_mom = $meeting.meeting_mom;
                            $formData.meeting_status = $meeting.meeting_status;
                            if(item){
                                $formData.id_meeting = item.id_meeting;
                            }

                            var $invitation = $meeting.invitation_id.map(function(obj){
                                //var rObj = {};
                                //rObj[obj.id_user] = obj.value;
                                if(obj.id_user!=undefined&&obj.id_user!=''){
                                    return obj.id_user;
                                }
                            });
                            $formData.invitation_id = $invitation;

                            var guest = $meeting.guest.map(function(tag){
                                return tag.text;
                            });

                            $formData.guest = guest;


                            $scope.submitStatus = false;
                            Upload.upload({
                                async:true,
                                url:API_URL+'activity/meeting',
                                data:{
                                    file:{'attachment':$scope.meetingDocuments},
                                    'meeting':$formData
                                }
                            }).then(function(data){
                                if(data.data.status){
                                    $rootScope.toast('Success',data.data.message);
                                    $scope.cancel();
                                    $scope.statusFlow();

                                } else{
                                    $rootScope.toast('Error',data.data.error,'error');
                                }
                            });

                        }
                    }

                    $scope.uploadFiles = function(file){
                        $scope.meetingDocuments = file;
                        //console.log($scope.meetingDocuments);
                    };
                    $scope.removeImage = function(obj){
                        //console.log(obj);
                        //console.log(obj.$$hashKey);
                        $scope.meetingDocuments.splice(obj.$$hashKey,1);
                    };

                    $scope.userList = [];
                    $scope.multipleSettings = {
                        enableSearch:true,
                        showCheckAll:false,
                        showUncheckAll:false,
                        smartButtonMaxItems:5,
                        idProp:'id_user',
                        externalIdProp:'id_user',
                        smartButtonTextConverter:function(itemText,originalItem){
                            return itemText;
                        }
                    };
                    projectService.ProjectMeeting.getMeetingGuest({
                        'company_id':$rootScope.id_company,
                        'user_id':$rootScope.userId
                    }).then(function(result){
                        console.log('result.status',result);
                        if(result.status){
                            $.each(result.data,function($key,$data){
                                var $item = {};
                                $item.id_user = $data.id_user;
                                $item.label = $data.first_name+' '+$data.last_name;
                                $scope.userList.push($item);
                            })
                            // $scope.userList = result.data.data;
                        }
                    });

                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                    //get uploaded attachments
                    /*       $scope.getFiles = function(){
                     crmContactService.getUploadedAttachments($rootScope.currentModuleId,$rootScope.currentProjectViewId).then(function(result){
                     if(result.status){
                     $scope.uploadedDocuments = result.data;
                     }
                     });
                     }
                     $scope.getFiles();*/
                    //discussion
                    /*      $scope.addComment = function(discussion){
                     discussion.discussion_type = 'meeting';
                     discussion.discussion_reference_id = $scope.meeting.id_meeting;


                     }*/
                }
            });
        }

        $scope.submitforapprovalModal = function(buttonType){
            $rootScope.submitType = buttonType;
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/project/submit-for-approval-modal.html',
                controller:'submitForApprovalModalCtrl'
            });
            modalInstance.result.then(function(){
                $scope.statusFlow();
            });
        }
        $scope.editApprovalFacility = function(row,project_approval_id){
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                scope:$scope,
                openedClass:'right-panel-modal modal-open',
                resolve:{
                    project_approval_id:function(){
                        return project_approval_id;
                    },
                    item:function(){
                        if($scope.selectedRow){
                            return $scope.selectedRow;
                        }
                    }
                },
                templateUrl:'partials/crm/project/facility_com/facilities-approval-form-fields.html',
                controller:function($uibModalInstance,$scope,item,project_approval_id){
                    $scope.facility = {};
                    $scope.facility.expected_maturity_type = "Years";
                    //$scope.currency = [{"id":1,"name":"INR"},{"id":2,"name":"USD"}];
                    $scope.maturity_type = [{"id":"Years","name":"Years"},{"id":"Months","name":"Months"}];

                    $scope.update = false;
                    $scope.title = 'Add';
                    console.log('item',item);
                    if(item){

                        angular.copy(item,$scope.facility);

                        //= item;
                        $scope.submitStatus = true;
                        $scope.update = true;
                        $scope.title = 'Edit';
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save = function(item){
                        // var params = facilityData;
                        console.log('item',item);
                        var params = {};
                        params.currency_id = item.currency_id;
                        params.expected_interest_rate = item.expected_interest_rate;
                        params.expected_maturity = item.expected_maturity;
                        params.expected_maturity_type = item.expected_maturity_type;
                        params.expected_start_date = item.expected_start_date;
                        params.loan_amount = item.loan_amount;
                        params.project_loan_type_id = item.project_loan_type_id;
                        params.project_payment_type_id = item.project_payment_type_id;
                        params.project_payment_type_id = item.project_payment_type_id;
                        params.id_project_facility = item.id_project_facility;
                        params.facility_name = item.facility_name;
                        params.project_facility_status = item.project_facility_status;
                        params.facility_comments = item.facility_comments;
                        params.project_approval_id = project_approval_id;
                        //params.company_id = $rootScope.id_company;
                        params.created_by = $rootScope.userId;
                        params.crm_project_id = $rootScope.currentProjectViewId;
                        $scope.submitStatus = true;
                        crmProjectService.facility.post(params).then(function(result){
                            if(result.status){
                                //$scope.getAssessmentQuestion();
                                $rootScope.toast('Success',result.message);

                                $uibModalInstance.close();
                            } else{
                                $rootScope.toast('Error',result.error,'error');
                            }
                            console.log('result',result);
                        });

                    }
                }


            });
            modalInstance.result.then(function($data){
                $scope.statusFlow();
            });
        }

        $scope.init();

    })

    .controller('ProjectApprovalSummaryCtrl',function($scope,$rootScope,crmProjectService){
        $scope.summaryDetails = [];
        var param = {};
        param.crm_project_id = $rootScope.currentProjectViewId;
        param.company_id = $rootScope.id_company;
        param.product_term_key = 'summary_and_recommendations';
        param.product_term_type = 'item';
        crmProjectService.projectTerm.projectTremesAndConditions(param).then(function($result){
            if($result.status){
                $scope.summaryDetails = $result.data;
            }
        })
    })

    .controller('submitForApprovalModalCtrl',function($scope,$rootScope,$uibModalInstance,crmProjectService){
        $scope.reportingUser = $rootScope.reportingUser;
        $scope.committeeDetails = {};
        $scope.reporting = {};
        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
        //get attachments types based on contact
        crmProjectService.committeeSecretary({
            'crm_project_id':$rootScope.currentProjectViewId,
            'company_id':$rootScope.id_company,
            'user_id':$rootScope.userId
        }).then(function(result){
            if(result.status){
                $scope.companyUsers = result.data.users;
                if($scope.companyUsers.length==1) $scope.reporting.forwarded_to = $scope.companyUsers[0].id_user;
                $scope.committeeDetails = result.data.details;
            }
        });

        $scope.submitReporting = function(reporting,form){
            $scope.submitStatus = true;
            if(form.$valid){
                reporting.approval_status = $rootScope.submitType;
                reporting.project_id = $rootScope.currentProjectViewId;
                reporting.forwarded_by = $rootScope.userId;
                reporting.committee_id = $scope.committeeDetails.committee_id;
                if(reporting.approval_status!='approved'&&reporting.approval_status!='rejected'){
                    if(reporting.forwarded_to==''||reporting.forwarded_to==undefined){
                        reporting.forwarded_to = $rootScope.reportingUser.reporting_user_id;
                        if(reporting.forwarded_to==''||$rootScope.reportingUser.reporting_user_id==0){
                            alert('Please select reporting User');
                            return false;
                        }
                    }
                } else{
                    reporting.forwarded_to = reporting.forwarded_by;
                }
                crmProjectService.projectReportingUser(reporting).then(function(result){
                    if(result.status){
                        $rootScope.toast('Success',result.message);
                        $uibModalInstance.close();
                        $rootScope.approvalStatus = false;
                        $scope.projectDetails();
                        $rootScope.projectApprovalsFun();
                    }
                });
            }
        }
        $scope.projectDetails = function(){
            crmProjectService.projectDetails($rootScope.currentProjectViewId,3,$rootScope.userId).then(function(result){
                //console.log(result);
                $rootScope.projectDetails = result.data.details[0];
                $scope.projectForms = result.data.forms;
                $rootScope.formStatus = result.data.active_forms;
                $rootScope.reportingUser = result.data.reporting;
                if($rootScope.projectDetails.assigned_to==$rootScope.userId){
                    $rootScope.approvalStatus = true;
                }
            });
        }
    })

    .controller('ProjectCreationCtrl',function($rootScope,$scope,$stateParams,$state,crmProjectService,masterService,encodeFilter){

        $rootScope.currentProjectViewId = encodeFilter($stateParams.id);
        if($rootScope.currentProjectViewId!=undefined){
            crmProjectService.projectDetails($rootScope.currentProjectViewId,3,$rootScope.userId).then(function(result){
                //console.log(result);
                $rootScope.projectDetails = result.data.details[0];
                $scope.projectForms = result.data.forms;
                $rootScope.formStatus = result.data.active_forms;
                $rootScope.reportingUser = result.data.reporting;
                if($rootScope.projectDetails.assigned_to==$rootScope.userId){
                    $rootScope.approvalStatus = true;
                }
                if($rootScope.formStatus!=undefined){
                    angular.forEach($rootScope.formStatus,function(task,index){
                        if(task.status){
                            $scope.checkForm.push(task.id);
                        }
                    });
                }
            });
        }
        $scope.settings = {
            colHeaders:true,
            autoWrapCol:true,
            manualColumnResize:true,
            contextMenu:['row_above','row_below','remove_row']
        };
        $rootScope.module_id = 3;
        $scope.projectObj = {};
        $scope.minSpareRows = 1;
        $scope.rowHeaders = false;

        $scope.checkForm = [];
        if($rootScope.formStatus!=undefined){
            angular.forEach($rootScope.formStatus,function(task,index){
                if(task.status){
                    $scope.checkForm.push(task.id);
                }
            });
        }
        $scope.percentage = 0;
        $scope.getCheckSection = function($section_id){
            var $form = $activeForm = 0;
            angular.forEach($rootScope.formStatus,function(task,index){
                if(task.section_id==$section_id){
                    $form++;
                    if(task.status==1){
                        $activeForm++;
                    }
                }
            });

            if($form==$activeForm&&$activeForm>0){
                return 'fa fa-check-circle green';
            } else if($activeForm>0){
                return 'fa-exclamation-circle orange';
            } else{
                return 'fa-times-circle red';
            }
        };
        $scope.parentSectorsList = [];
        $scope.subSectorList = [];


        $scope.getCheck = function($form_id){
            if($.inArray($form_id,$scope.checkForm)!= -1){
                return 'fa-check green';
            } else{
                return 'fa-close red';
            }
        };

        $scope.settings = {colHeaders:true,autoWrapCol:true,contextMenu:['row_above','row_below','remove_row']};
        $scope.anotherOptions = {
            barColor:'#02B000',
            trackColor:'#f9f9f9',
            scaleColor:'#dfe0e0',
            scaleLength:5,
            lineCap:'round',
            lineWidth:3,
            size:50,
            rotate:0,
            animate:{
                duration:1000,
                enabled:true
            }
        };

        crmProjectService.getModules($rootScope.module_id).then(function($result){
            //console.log($result);
            $scope.projectObj = $result.data;
        });
        $scope.nextFormOpen = function($form_data){
            var form_id = $form_data.id_form;
            var section_id = $form_data.id_section;
            $arrayData = $scope.projectObj;
            angular.forEach($arrayData,function($section){
                if($section.id_section==section_id){
                    angular.forEach($section.form,function($form){
                        if($form.id_form==form_id){
                            if($section.form.length>$section.form.indexOf($form)+1){
                                $scope.loadTemplate($section.form[$section.form.indexOf($form)+1],$section.id_section);
                                //console('next Form', $section.form.indexOf($form) + 2);
                                return false;
                            } else{
                                //console('no form are there in the section', $arrayData.indexOf($section) + 1);
                                if($arrayData[$arrayData.indexOf($section)+1]){
                                    if($arrayData[$arrayData.indexOf($section)+1].form.length>0){
                                        $scope.loadTemplate($arrayData[$arrayData.indexOf($section)+1].form[0],$arrayData[$arrayData.indexOf($section)+1].id_section);
                                        return false;
                                    }
                                } else{
                                    //console('colse');
                                    $state.go("project.project-dashboard");
                                }
                            }
                        }
                    })
                }
            });
        };
        $scope.submitForm = function($inputData,$form_data,form){
            $scope.submitStatus = true;
            if(form.$valid){
                $inputData.company_id = $rootScope.id_company;
                $inputData.form_id = $form_data.id_form;
                $inputData.user_id = $rootScope.userId;

                if($rootScope.currentProjectViewId!=undefined){
                    $inputData.crm_project_id = $rootScope.currentProjectViewId;
                }
                crmProjectService.saveProject($inputData).then(function(result){
                    //console.log(result);
                    if(result.status){
                        // $scope.checkForm.push($form_data.id_form);
                        $rootScope.toast('Success',result.message);
                        if($rootScope.currentProjectViewId==undefined){
                            $rootScope.crm_project_id = result.data.crm_project_id;
                        }
                        $scope.percentage = Math.ceil(result.data.percentage);
                        $scope.checkForm.push($form_data.id_form);
                        $scope.nextFormOpen($form_data);
                    } else{
                        $rootScope.toast('Error',result.error,'error',$scope.sector);
                    }
                })
            }
        };
        masterService.sector.getAll().then(function(result){
            $scope.parentSectorsList = result.data.data;
        });
        $scope.getSubSector = function(parentId){
            //console.log(parentId);
            masterService.sector.getSubSector(parentId).then(function(result){
                $scope.subSectorList = result.data;
            });
        };
        $scope.loadTemplate = function(form,id_section){
            $scope.submitStatus = false;
            $scope.formData = form;
            $scope.formData.id_section = id_section;
            if($rootScope.currentProjectViewId!=undefined){
                crmProjectService.projectFormDetails($rootScope.currentProjectViewId,form.id_form,'edit').then(function(result){
                    $scope.project = result.data.data;
                    $scope.percentage = Math.ceil(result.data.percentage);
                    $scope.project_title = result.data.project_details.project_title;
                    $scope.project_created_date = result.data.project_details.created_date_time;
                });
            }
        };
    })

    .controller('ProjectMappingCtrl',function($rootScope,$scope,crmContactService,crmCompanyService,quickService,$state,$uibModal){
        $scope.getContactList = function(search_key){
            return crmContactService.contactList($rootScope.id_company,{'search_key':search_key}).then(function(result){
                //console.log(result.data.data);
                return result.data.data;
            });
        }
        $scope.getCompanyList = function(search_key){
            return crmCompanyService.companyList($rootScope.id_company,{'search_key':search_key}).then(function(result){
                //console.log(result.data.data);
                return result.data.data;
            });
        }


        $scope.onSelect = function($item,$model,$label){
            //console.log('item', $item);
        }

        $scope.submitQuickContact = function($inputData,form){
            if(form.$valid){
                $inputData.company_id = $rootScope.id_company;
                $inputData.created_by = $rootScope.userId;
                quickService.saveQuickContact($inputData).then(function(result){
                    if(result.status){
                        $scope.sumitted = false;
                        $rootScope.toast('Success',result.error);
                        $rootScope.crm_contact_id = result.data.crm_contact_id;
                    } else{
                        $rootScope.toast('Error',result.error,'error',$scope.sector);
                    }
                })
            }
        };
        $scope.submitQuickCompany = function($inputData,form){
            if(form.$valid){
                $inputData.company_id = $rootScope.id_company;
                $inputData.created_by = $rootScope.userId;
                quickService.saveQuickCompany($inputData).then(function(result){
                    if(result.status){
                        $rootScope.toast('Success',result.error);
                        $rootScope.crm_company_id = result.data.crm_company_id;
                    } else{
                        $rootScope.toast('Error',result.error,'error',$scope.sector);
                    }
                })
            }
        };

        $scope.quickContactModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/contact/quick-contact.html',
                controller:'quickContactCtrl'
            });
            modalInstance.result.then(function(){

            },function(){
            });
        }

        $scope.quickCompanyModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/company/quick-company.html',
                controller:'quickContactCtrl'
            });
            modalInstance.result.then(function(){

            },function(){
            });
        }
        $scope.quickProjectModal = function(){
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/project/quick-project.html',
                controller:'quickContactCtrl'
            });
            modalInstance.result.then(function(){

            },function(){
            });
        }

    })

    .controller('attachmentsCtrl',function($scope,$rootScope,$uibModalInstance,crmContactService,Upload){

        //get attachments types based on contact
        crmContactService.getAttachmentsTypes($rootScope.currentModuleId).then(function(result){
            if(result.status){
                $scope.documentsTypes = result.data;
            }
        });
        //get uploaded attachments
        $scope.getFiles = function(){
            crmContactService.getUploadedAttachments($rootScope.currentModuleId,$rootScope.contactId).then(function(result){
                if(result.status){
                    $scope.uploadedDocuments = result.data;
                }
            });
        }
        $scope.getFiles();

        $scope.save = function(upload,form){
            $scope.submitStatus = true;
            if(form.$valid){
                var $fileCount = 1;
                $.each($scope.uploadImage,function(parentKey,parentvalue){
                    Upload.upload({
                        async:true,
                        url:API_URL+'crm/document',
                        data:{
                            file:{'attachment':parentvalue},
                            'group_id':$rootScope.contactId,
                            'user_id':$rootScope.userId,
                            'document_type':this.document_type,
                            'description':this.description,
                            'module_id':$rootScope.currentModuleId
                        }
                    }).progress(function(e){
                        $.each($scope.uploadImage,function(value,key){
                            //console.log('parentvalue.$$hashKey', parentvalue.$$hashKey, 'value.$$hashKey', this.$$hashKey);
                            if(this.$$hashKey==parentvalue.$$hashKey){
                                this.progress = parseInt(100.0*e.loaded/e.total);
                            }
                        })
                    }).success(function(data){
                        //console.log($fileCount, $scope.uploadImage.length);
                        if($scope.uploadImage.length==$fileCount){

                            $scope.getFiles();
                            $rootScope.toast('Success',data.message);
                            $scope.uploadImage = [];
                            $scope.submitStatus = false;
                        }
                        $fileCount++;
                        if(data.status){

                            // $uibModalInstance.dismiss('cancel');
                        } else{
                            $rootScope.toast('Error',data.error,'error',$scope.sector);
                        }
                    });
                });
            }
        }
        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
        $scope.uploadImage = [];
        $scope.uploadFiles = function(file){
            if(!$.isArray(file)){
                return false;
            }
            if(file.length==0){
                $rootScope.toast('Error','invalid format','image-error');
            }
            $.each(file,function(key,value){
                value.document_type = '';
                value.progress = 0;
                value.description = '';
                value.descriptionName = 'description_'+key;
                value.documentTypeName = 'document_type_'+key;
                $scope.uploadImage.push(value);
            });
            //$scope.uploadImage = file;
        };
        $scope.removeImage = function(obj,index){
            var r = confirm("Are you sure want to remove!");
            if(r==true){
                $scope.uploadImage.splice(index,1);
            }
        };

    })

    .controller('touchPointsCtrl',function($rootScope,$scope,crmContactService,$uibModalInstance,crmParams){
        $scope.showReminders = false;
        $scope.showAllPoints = false;
        $scope.touchPointDescription = '';
        $scope.serchType = '';
        $scope.getTouchPointsList = function(){
            //var getParams = 'crm_module_id='+crmParams.crm_module_id+'&crm_module_type='+crmParams.crm_module_type;
            var getParams = 'crm_module_type='+crmParams.crm_module_type+'&from_id='+crmParams.crm_module_id;
            if($scope.serchType!=null){
                getParams = getParams+'&crm_touch_point_type_id='+$scope.serchType
            }
            if($scope.showReminders){
                getParams = getParams+'&is_reminder='+$scope.showReminders
            }
            if($scope.showAllPoints){
                getParams = getParams+'&is_all='+$scope.showAllPoints
            }
            crmContactService.getTouchPointsList(getParams).then(function(result){
                $scope.touchPointsList = result.data.data;
                $scope.reminder = result.data.reminder;
                $scope.showReminders = false;
                $scope.showAllPoints = false;
            });
        }
        $scope.showAllReminders = function(){
            $scope.serchType = '';
            $scope.crm_touch_point_type_filter = '';
            $scope.showReminders = true;
            $scope.getTouchPointsList();
        }
        $scope.showAllTouchPoints = function(){
            $scope.serchType = '';
            $scope.crm_touch_point_type_filter = '';
            $scope.showAllPoints = true;
            $scope.getTouchPointsList();
        }
        $scope.changedValue = function(val){
            if(val==''){
                $scope.serchType = '';
            } else{
                $scope.serchType = val;
            }
            $scope.getTouchPointsList();
        }
        crmContactService.touchPointType().then(function(result){
            $scope.touchPointList = result.data;
        });
        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
        $scope.activeTouchPoint = function(touchPoint){
            //console.log(touchPoint);
            $scope.currentTouchPoint = touchPoint;
            //console.log($scope.touchPointDescription);
        }
        $scope.postField = {'reminder_date':new Date()};
        $scope.submit = function(postField,form){
            $scope.submitStatus = true;
            //console.log(form.$valid);
            if(form.$valid){
                postField.crm_module_id = crmParams.crm_module_id;
                postField.crm_module_type= crmParams.crm_module_type;
               // postField.touch_point_from_id = $scope.crm_module_id;
                postField.created_by = $rootScope.userId;
                //console.log('postField', postField);
                crmContactService.addTouchPointComment(postField).then(function(result){
                    $scope.postField = {'reminder_date':new Date()};
                    $scope.submitStatus = false;
                    $scope.togglePoint = false;
                    if(result.status){
                        $rootScope.toast('Success',result.message);
                        $scope.getTouchPointsList();
                    } else{
                        $rootScope.toast('Error',result.error,'error',$scope.sector);
                    }

                });
            }
        }
        $scope.getTouchPointsList();
    })

    .controller('quickContactCtrl',function($uibModalInstance,$scope){
        $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
        };
    })

    .controller('facilityComponentsCtrl',function($scope,$rootScope,$uibModal,crmProjectService,masterService){
        // $scope.facility_details = [];

        $scope.removeItem = function removeItem(row){
            var index = $scope.project.facility_details.indexOf(row);
            if(index!== -1){
                $scope.project.facility_details.splice(index,1);
            }
        };

        $scope.init = function(){
            $scope.facilities = [];
            crmProjectService.facility.get({
                'company_id':$rootScope.id_company,
                'crm_project_id':$rootScope.currentProjectViewId
            }).then(function(result){
                $scope.facilities = result.data;
            });
        }

        $scope.approvalStatusChange = function(item,status){
            var obj = {};
            obj.currency_id = item.currency_id;
            obj.expected_interest_rate = item.expected_interest_rate;
            obj.expected_maturity = item.expected_maturity;
            obj.expected_maturity_type = item.expected_maturity_type;
            obj.expected_start_date = item.expected_start_date;
            obj.loan_amount = item.loan_amount;
            obj.project_loan_type_id = item.project_loan_type_id;
            obj.project_payment_type_id = item.project_payment_type_id;
            obj.project_payment_type_id = item.project_payment_type_id;
            obj.id_project_facility = item.id_project_facility;
            obj.project_facility_status = item.project_facility_status;
            obj.facility_comments = item.facility_comments;
            obj.project_approval_id = item.project_approval_id;

            var params = obj;
            //params.company_id = $rootScope.id_company;
            params.created_by = $rootScope.userId;
            params.crm_project_id = $rootScope.currentProjectViewId;
            $scope.submitStatus = true;
            crmProjectService.facility.post(params).then(function(result){
                if(result.status){
                    //$scope.getAssessmentQuestion();
                    $rootScope.toast('Success',result.message);
                    $scope.init();
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            });


        }

        $scope.addFacility = function(row){
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                resolve:{
                    item:function(){
                        if($scope.selectedRow){
                            return $scope.selectedRow;
                        }
                    },
                    currency:function(){
                        return crmProjectService.currency.get({'company_id':$rootScope.id_company}).then(function($result){
                            return $result.data;
                        });
                    },
                    loan_type:function(){
                        return masterService.projectLoanType().then(function($result){
                            return $result.data;
                        });
                    },
                    payment_type:function(){
                        return masterService.projectPaymentType().then(function($result){
                            return $result.data;
                        });
                    }
                },
                templateUrl:'partials/crm/project/facility_com/facilities-form-fields.html',
                controller:function($uibModalInstance,$scope,item,currency,loan_type,payment_type){
                    $scope.facility = {};
                    $scope.facility.expected_maturity_type = 'Years';
                    //$scope.currency = [{"id":1,"name":"INR"},{"id":2,"name":"USD"}];
                    $scope.maturity_type = [{"id":"Years","name":"Years"},{"id":"Months","name":"Months"}];
                    // $scope.loan_type = [{"id":1,"name":"Term Loan"},{"id":2,"name":"Working"},{"id":3,"name":"Capital"}];
                    // $scope.payment_type = [{"id":1,"name":"Term Loan"},{"id":2,"name":"Working"},{"id":3,"name":"Capital"}];
                    $scope.currency = currency;
                    $scope.loan_type = loan_type;
                    $scope.payment_type = payment_type;
                    $scope.update = false;
                    $scope.title = 'Add';
                    console.log('item',item);
                    if(item){
                        var obj = {};
                        obj.currency_id = item.currency_id;
                        obj.expected_interest_rate = item.expected_interest_rate;
                        obj.expected_maturity = item.expected_maturity;
                        obj.expected_maturity_type = item.expected_maturity_type;
                        obj.expected_start_date = item.expected_start_date;
                        obj.loan_amount = item.loan_amount;
                        obj.project_loan_type_id = item.project_loan_type_id;
                        obj.project_payment_type_id = item.project_payment_type_id;
                        obj.project_payment_type_id = item.project_payment_type_id;
                        obj.id_project_facility = item.id_project_facility;
                        obj.facility_name = item.facility_name;
                        $scope.facility = obj;
                        $scope.submitStatus = true;
                        $scope.update = true;
                        $scope.title = 'Edit';
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save = function(facilityData){
                        var params = facilityData;
                        //params.company_id = $rootScope.id_company;
                        params.created_by = $rootScope.userId;
                        params.crm_project_id = $rootScope.currentProjectViewId;
                        $scope.submitStatus = true;
                        crmProjectService.facility.post(params).then(function(result){
                            if(result.status){
                                //$scope.getAssessmentQuestion();
                                $rootScope.toast('Success',result.message);
                                $uibModalInstance.close();
                            } else{
                                $rootScope.toast('Error',result.error,'error');
                            }
                            console.log('result',result);
                        });

                    }
                }


            });
            modalInstance.result.then(function($data){
                $scope.init();
            });
        }

        $scope.viewFacility = function(row){
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/project/facility_com/view-facility-form.html',
                controller:function($uibModalInstance,$scope,item){
                    $scope.currency = [{"id":1,"name":"INR"},{"id":2,"name":"USD"}];
                    $scope.facility = {};

                    $scope.update = false;
                    //console.log(item);
                    if(item){
                        $scope.facility = item;
                        //console.log('facit', $scope.facility);
                        $scope.update = true;
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                resolve:{
                    item:function(){
                        if($scope.selectedRow){
                            return $scope.selectedRow;
                        }
                    }
                }

            });

        };
    })

    .controller('previousExperienceCtrl',function($scope,$rootScope,$uibModal){

        $scope.removeItem = function removeItem(row){
            var index = $scope.contact.previous_experience_institution.indexOf(row);
            if(index!== -1){
                $scope.contact.previous_experience_institution.splice(index,1);
            }
        }

        $scope.addPreviousExperience = function(row){
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation:true,
                //windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/contact/forms/previous-experience-fields.html',
                controller:function($uibModalInstance,$scope,item){
                    $scope.previousExperience = {
                        "current_loan_amount_distribution":"",
                        "client_since":"",
                        "current_outstanding_loan_amount":"",
                        "current_loan_payments_on_time":"",
                        "total_previous_loans":"",
                        "previous_loan_payments_on_time":""
                    };
                    $scope.update = false;
                    $scope.title = 'Add';
                    //console.log(item);
                    if(item){
                        $scope.previousExperience = item;
                        $scope.submitStatus = true;
                        $scope.update = true;
                        $scope.title = 'Edit';
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save = function($Data,form){
                        $scope.submitStatus = true;
                        if(form.$valid){
                            $uibModalInstance.close($scope.previousExperience);
                        }
                    }
                },
                resolve:{
                    item:function(){
                        if($scope.selectedRow){
                            return $scope.selectedRow;
                        }
                    }
                }

            });
            modalInstance.result.then(function($data){
                if(!$scope.contact.previous_experience_institution){
                    $scope.contact.previous_experience_institution = [];
                }

                $scope.contact.previous_experience_institution.push($data);
            },function(){

            });
        }
        $scope.viewPreviousExperience = function(row){
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation:true,
                windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/contact/forms/view-previous-experience-form.html',
                controller:function($uibModalInstance,$scope,item){
                    $scope.previousExperience = {};
                    $scope.update = false;
                    //console.log(item);
                    if(item){
                        $scope.previousExperience = item;
                        //console.log('facit', $scope.facility);
                        $scope.update = true;
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                resolve:{
                    item:function(){
                        if($scope.selectedRow){
                            return $scope.selectedRow;
                        }
                    }
                }

            });

        };
    })

    .controller('companyOwnershipCtrl',function($scope,$rootScope,$uibModal){
        /*$scope.company = {};
         $scope.company.ownership_details = [];*/
        $scope.removeItem = function removeItem(row){
            var index = $scope.company.ownership_details.indexOf(row);
            if(index!== -1){
                $scope.company.ownership_details.splice(index,1);
            }
        }
        $scope.addOwnerShip = function(row){
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation:true,
                //windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                scope:$scope,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/company/forms/ownership-form-fields.html',
                controller:function($uibModalInstance,$scope,item,$q){
                    $scope.ownership = {first_name:'',last_name:'',ownership_percentage:'',no_of_shares:''};
                    $scope.update = false;
                    $scope.title = 'Add';
                    //console.log(item);
                    if(item){
                        $scope.ownership = item;
                       // angular.copy(item,$scope.ownership);
                        $scope.submitStatus = true;
                        $scope.update = true;
                        $scope.title = 'Edit';
                    }
                    $scope.validOwnerShip = function(val){
                        var ownershipSum = 0;
                        angular.forEach($scope.company.ownership_details,function(ownership,$index){
                            ownershipSum = ownershipSum + ownership.ownership_percentage;
                        });
                        if(ownershipSum+val > 100){$rootScope.toast('Error',['All Ownership % less than or equal to 100%'],'error'); return false; }else{return true;}
                    }
                    $scope.cancel = function(){
                        var status = $scope.validOwnerShip(0);
                        if(status){
                            $uibModalInstance.dismiss('cancel');
                        }
                    };
                    $scope.save = function($Data,form){
                        $scope.submitStatus = true;
                        if(form.$valid){
                            var status = $scope.validOwnerShip($scope.ownership.ownership_percentage);
                            if(status){
                                $uibModalInstance.close($scope.ownership);
                            }
                        }
                    }
                },
                resolve:{
                    item:function(){
                        if($scope.selectedRow){
                            return $scope.selectedRow;
                        }
                    }
                }

            });
            modalInstance.result.then(function($data){
                $scope.company.ownership_details.push($data);
            },function(){

            });
        }
        $scope.viewOwnership = function(row){
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation:true,
                //windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/company/forms/view-ownership-form.html',
                controller:function($uibModalInstance,$scope,item){
                    $scope.ownership = {};
                    $scope.update = false;
                    //console.log(item);
                    if(item){
                        $scope.ownership = item;
                        //console.log('facit', $scope.facility);
                        $scope.update = true;
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                resolve:{
                    item:function(){
                        if($scope.selectedRow){
                            return $scope.selectedRow;
                        }
                    }
                }

            });

        };
    })

    .controller('socialNetworkCtrl',function($scope,$rootScope,$uibModal){
        $scope.removeItem = function removeItem(row){
            var index = $scope.company.business_social_networks.indexOf(row);
            if(index!== -1){
                $scope.company.business_social_networks.splice(index,1);
            }
        }

        $scope.addNetwork = function(row){
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation:true,
                //windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/company/forms/network-form-fields.html',
                controller:function($uibModalInstance,$scope,item){
                    $scope.networkDetails = {'name':'','url':''};
                    $scope.update = false;
                    $scope.title = 'Add';
                    //console.log(item);
                    if(item){
                        $scope.networkDetails = item;
                        $scope.update = true;
                        $scope.title = 'Edit';
                        $scope.submitStatus = true;
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.save = function($Data,form){
                        $scope.submitStatus = true;
                        if(form.$valid){
                            $uibModalInstance.close($scope.networkDetails);
                        }
                    }
                },
                resolve:{
                    item:function(){
                        if($scope.selectedRow){
                            return $scope.selectedRow;
                        }
                    }
                }

            });
            modalInstance.result.then(function($data){
                $scope.company.business_social_networks.push($data);
            },function(){

            });
        }
        $scope.viewNetwork = function(row){
            $scope.selectedRow = row;
            var modalInstance = $uibModal.open({
                animation:true,
                //windowClass:'my-class',
                backdrop:'static',
                keyboard:false,
                openedClass:'right-panel-modal modal-open',
                templateUrl:'partials/crm/company/forms/view-network-form.html',
                controller:function($uibModalInstance,$scope,item){
                    $scope.networkDetails = {};
                    $scope.update = false;
                    //console.log(item);
                    if(item){
                        $scope.networkDetails = item;
                        //console.log('facit', $scope.facility);
                        $scope.update = true;
                    }
                    $scope.cancel = function(){
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                resolve:{
                    item:function(){
                        if($scope.selectedRow){
                            return $scope.selectedRow;
                        }
                    }
                }

            });

        };
    })

    .controller('companyAssessmentQuestions',function($scope,$rootScope,crmCompanyService,crmContactService,Upload,$timeout){

        $scope.categoryQuestionList = [];
        //console.log('$rootScope.companyParames',$rootScope.companyParames);
        $scope.getAssessmentQuestion = function(){
            crmContactService.getCompanyAssessment({
                'assessment_id':$rootScope.companyParames.assessmentId,
                'assessment_key':$rootScope.companyParames.assessment_key,
                'type':'question',
                'crm_company_id':$rootScope.currentCompanyViewId,
                'company_id':$rootScope.id_company
            }).then(function(result){
                if(result.status){
                    $scope.categoryQuestionList = result.data[0].category;
                }
            });
            /*crmCompanyService.assessmentQuestion.question.get({'company_id':$rootScope.id_company,'id_assessment_question_type':1}).then(function(result){
             if (result.status) {
             $scope.categoryQuestionList =result.data[0].category;
             console.log(' $scope.categoryQuestionList', $scope.categoryQuestionList);
             }
             })*/
        }
        $scope.getAssessmentQuestion();
        $scope.saveAnswer = function(category){
            //console.log('questions',category); return false;
            bulkAnswers = [];
            angular.forEach(category,function(category){
                angular.forEach(category.question,function(question,$index){
                    var $obj = {};
                    $obj.assessment_answer = question.answer.assessment_answer;
                    $obj.assessment_question_id = question.id_assessment_question;
                    $obj.crm_reference_id = $rootScope.currentCompanyViewId;
                    $obj.crm_company_id = $rootScope.currentCompanyViewId;
                    $obj.created_by = $rootScope.userId;
                    $obj.company_id = $rootScope.id_company;
                    $obj.assessment_id = $rootScope.companyParames.assessmentId;
                    if(question.answer.id_assessment_answer!=undefined){
                        $obj.id_assessment_answer = question.answer.id_assessment_answer;
                    }


                    $rootScope.loaderOverlay = false;
                    if(question.question_type=='file'){

                        if(question.answer.file.name!=undefined){
                            Upload.upload({
                                url:API_URL+'company/assessmentAnswer',
                                data:{
                                    file:question.answer.file,
                                    'answer':$obj
                                }
                            }).then(function(result){
                                if(result.status){
                                    //$scope.getAssessmentQuestion();
                                    if($index==category.question.length-1){
                                        $scope.getAssessmentQuestion();
                                    }
                                    $rootScope.toast('Success',result.message);
                                } else{
                                    $rootScope.toast('Error',result.error,'error');
                                }


                            });
                        }

                    } else{
                        crmCompanyService.assessmentQuestion.answer.post($obj).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                if($index==category.question.length-1){
                                    $scope.getAssessmentQuestion();
                                }
                            } else{
                                $rootScope.toast('Error',result.error,'error');
                            }

                        })
                    }

                })
            })
        }

    })

    .controller('projectAssessmentQuestionsCheckList',function($scope,$rootScope,crmProjectService,crmCompanyService,Upload,$q,$timeout){

        $scope.categoryQuestionList = [];
        console.log('$rootScope.projectParames',$rootScope.projectParames);
        $scope.getAssessmentQuestion = function(){

            crmProjectService.projectTerm.projectTremesAndConditions({
                product_term_key:$rootScope.projectParames.sectionName,
                product_term_id:$rootScope.projectParames.formId,
                company_id:$rootScope.id_company,
                crm_project_id:$rootScope.currentProjectViewId,
                product_term_type:$rootScope.projectParames.product_term_type
            }).then(function(result){
                if(result.status){
                    $scope.categoryQuestionList = result.data[0].category;
                }
            });
        }
        $scope.getAssessmentQuestion();
        $scope.saveAnswerRestrictions = function(category){
            //console.log('questions',category); return false;
            bulkAnswers = [];
            angular.forEach(category,function(category){
                angular.forEach(category.question,function(question,$index){
                    var $obj = {};
                    $obj.assessment_answer = question.answer.assessment_answer;
                    $obj.assessment_question_id = question.id_assessment_question;
                    $obj.crm_reference_id = $rootScope.currentProjectViewId;
                    $obj.created_by = $rootScope.userId;
                    $obj.company_id = $rootScope.id_company;
                    $obj.id_assessment_question_type = $rootScope.projectParames.formId;
                    $obj.crm_project_id = $rootScope.currentProjectViewId;
                    if(question.answer.id_assessment_answer!=undefined){
                        $obj.id_assessment_answer = question.answer.id_assessment_answer;
                    }

                    $rootScope.loaderOverlay = false;
                    if(question.question_type=='file'){

                        if(question.answer.file.name!=undefined){
                            Upload.upload({
                                url:API_URL+'company/assessmentAnswer',
                                data:{
                                    file:question.answer.file,
                                    'answer':$obj
                                }
                            }).then(function(result){
                                if(result.status){
                                    //$scope.getAssessmentQuestion();
                                    if($index==category.question.length-1){
                                        $scope.getAssessmentQuestion();
                                    }
                                    $rootScope.toast('Success',result.message);
                                } else{
                                    $rootScope.toast('Error',result.error,'error');
                                }


                            });
                        }

                    } else{
                        crmCompanyService.assessmentQuestion.answer.post($obj).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                if($index==category.question.length-1){
                                    $scope.getAssessmentQuestion();
                                }
                            } else{
                                $rootScope.toast('Error',result.error,'error');
                            }

                        })
                    }

                })
            })
        }

        $scope.saveAnswer = function(questions){

            var bulkAnswers = [];

            angular.forEach(questions,function(question,$index){
                console.log('question',question);
                var $obj = {};
                $obj.assessment_answer = question.answer.assessment_answer;
                $obj.assessment_question_id = question.id_assessment_question;
                $obj.crm_reference_id = $rootScope.currentProjectViewId;
                $obj.created_by = $rootScope.userId;
                $obj.company_id = $rootScope.id_company;
                $obj.comment = question.answer.comment;
                $obj.type = 'media';
                $obj.crm_project_id = $rootScope.currentProjectViewId;
                $obj.id_assessment_question_type = $rootScope.projectParames.formId;
                if(question.answer.id_assessment_answer!=undefined){
                    $obj.id_assessment_answer = question.answer.id_assessment_answer;
                }

                $rootScope.loaderOverlay = false;

                console.log('question.answer.assessment_answer',question.answer.file.name);
                if(question.answer.file.name!=undefined){
                    Upload.upload({
                        url:API_URL+'company/assessmentAnswer',
                        data:{
                            file:question.answer.file,
                            'answer':$obj
                        }
                    }).then(function(result){

                        if(result.status){
                            //$scope.getAssessmentQuestion();
                            $rootScope.toast('Success',result.message);
                            if($index==questions.length-1){
                                $scope.getAssessmentQuestion();
                            }
                        } else{
                            //$rootScope.toast('Error',result.error,'error');
                        }
                        console.log('$index',$index);
                        console.log('question.length',questions.length);

                    });
                } else{
                    crmCompanyService.assessmentQuestion.answer.post($obj).then(function(result){
                        console.log('$index',$index);
                        if(result.status){
                            $rootScope.toast('Success',result.message);
                            if($index==questions.length-1){
                                $scope.getAssessmentQuestion();
                            }
                        } else{
                            //$rootScope.toast('Error',result.error,'error');
                        }
                        console.log('question.length',questions.length);

                    })
                }


            })
        }


    })

    .controller('projectRiskAssessmentCtrl',function($scope,$rootScope,crmProjectService){

        $scope.riskCategory = [];
        crmProjectService.riskAssessment.get({
            'company_id':$rootScope.id_company,
            'crm_project_id':$rootScope.currentProjectViewId
        }).then(function(result){
            if(result.status){
                $scope.riskCategory = result.data;
            } else{
                $rootScope.toast('Error',result.error,'error');
            }

        });
        $scope.convertToInt = function(value){
            return parseInt(value);
        };
        $scope.subAttrFactor = 0;
        $scope.dropdownSelected = function(attribute){
            angular.forEach(attribute.items,function(item){
                console.log('item.selected',item.selected);
                if(item.selected==1){
                    attribute.selectedItem = item;
                }
            })
        }
        $scope.saveAttribute = function(data){
            console.log('data',data);
            var param = {};
            param.company_id = $rootScope.id_company;
            param.crm_project_id = $rootScope.currentProjectViewId;
            param.user_id = $rootScope.userId;
            param.data = [];
            angular.forEach(data,function(item){
                if(item.selectedItem!=undefined){
                    var obj = {};
                    obj.grade = item.grade;
                    obj.id_risk_category = item.id_risk_category;
                    obj.id_risk_category_item = item.selectedItem.id_risk_category_item;
                    obj.risk_category_id = item.id_risk_category;
                    param.data.push(obj);
                }
            })

            $scope.projectDetails = function(){
                crmProjectService.projectDetails($rootScope.currentProjectViewId,3,$rootScope.userId).then(function(result){
                    $rootScope.projectDetails = result.data.details[0];
                    /* $scope.businessKeyData = result.data.forms;
                     $rootScope.formStatus = result.data.active_forms;
                     $rootScope.reportingUser = result.data.reporting;
                     if($rootScope.projectDetails.assigned_to==$rootScope.userId){
                     $rootScope.approvalStatus = true;
                     }*/
                });
            }


            crmProjectService.riskAssessment.post(param).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                    $scope.projectDetails();
                    // $rootScope.
                } else{
                    $rootScope.toast('Error',result.error,'error',result.error);
                }
            })
        }


    })

    .controller('collateralDashboardCtrl',function($scope,$rootScope,crmProjectService,companyService,$state,decodeFilter){
        $scope.projectList = [];
        $scope.facilityList = [];
        $scope.collateral = {};
        $scope.collateralTypeList = [];

        crmProjectService.projectList($rootScope.id_company).then(function(result){
            console.log('result',result.data.data);
            $scope.projectList = result.data.data;
        });

        crmProjectService.collateral.collateralType({'company_id':$rootScope.id_company}).then(function(result){
                $scope.collateralTypeList = result.data;
            });


        $scope.getFacilityList = function(crm_project_id){
            var param = {};
            param.company_id = $rootScope.id_company;
            param.crm_project_id = crm_project_id;
            crmProjectService.facility.get(param).then(function(result){
                $scope.facilityList = result.data;
            });
        }

        $scope.gotoState = function(row){
            $state.go('crm-collateral.form-list',{'collateralId':decodeFilter(row.id_collateral)})
        }
        $scope.submitQuickCollateral = function($moreFields){
            var params = $scope.collateral;
            //params.company_id = $rootScope.id_company;
            params.created_by = $rootScope.userId;
            crmProjectService.collateral.post(params).then(function(result){
                if(result.status){
                    $scope.submitted = false;
                    $scope.collateral = {};
                    if($moreFields){
                        $state.go('crm-collateral.form-list',{'collateralId':decodeFilter(result.data)})
                    }else{
                        $scope.callServer($scope.tableStateRef);
                    }

                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            });
        }


        $scope.callServer = function(tableState){
            tableState.company_id = $rootScope.id_company;
            crmProjectService.collateral.getList(tableState).then(function(result){
                $scope.displayed = true;
                $scope.tableStateRef = tableState;
                $scope.collateralList = result.data.data;
                //$scope.totalProjectCount = result.data.total_records;
                tableState.pagination.numberOfPages = Math.ceil(result.data.total_records/tableState.pagination.number);
            });
        };

    })

    .controller('collateralFormListCtrl',function($scope,$rootScope,masterService,companyService,crmProjectService,$state,$stateParams,encodeFilter,decodeFilter){

        $rootScope.id_collateral = encodeFilter($stateParams.collateralId);
        $scope.collateralInfo = {};
        $scope.init = function(){
            $scope.getCollateralInfo();
        }
        $scope.getCollateralInfo = function(){
            var params = {};
            params.company_id = $rootScope.id_company;
            params.id_collateral = $rootScope.id_collateral;
            crmProjectService.collateral.get(params).then(function(result){
                if(result.status){
                    //console.log('result',result);
                    $scope.collateralInfo = result.data;
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            });
        }
        $scope.goToEdit = function(data){
            $rootScope.collateralParams = {};
            $rootScope.collateralParams = data;
            if(data.type=='collateralInfo'){
                $state.go('crm-collateral.collateral-details',{'collateralId':$stateParams.collateralId});
            }else{
                $state.go('crm-collateral.collateral-type-form',{'collateralId':$stateParams.collateralId,'collateralTypeId':decodeFilter(data.collateral_type_id)});
            }
        }

        $scope.init();
    })

    .controller('collateralDetailsCtrl',function($scope,$rootScope,masterService,companyService,crmProjectService,$state,$stateParams,encodeFilter,decodeFilter){

        if($rootScope.collateralParams == undefined){
            $state.go('crm-collateral.form-list',{'collateralId':$stateParams.collateralId});
            return false;
        }
        $rootScope.id_collateral = encodeFilter($stateParams.collateralId);
        $scope.collateralTypeList = [];
        $scope.formData = {};

        $scope.init = function(){
            $scope.collateralType();
        }
        $scope.collateralType = function(){
            companyService.collateral.collateralType({'company_id':$rootScope.id_company}).then(function(result){
                $scope.collateralTypeList = result.data;
                $scope.getCollateralInfo();
            });
        }

        $scope.getCollateralInfo = function(){
            var params = {};
            params.company_id = $rootScope.id_company;
            params.id_collateral = $rootScope.id_collateral;
            crmProjectService.collateral.get(params).then(function(result){
                if(result.status){
                    //console.log('result',result);
                    $scope.formData = result.data;

                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            });
        }
        $scope.saveCollateralDetails = function(){

            var params = $scope.formData;
            params.collateral_type_id =  params.id_collateral_type;
            params.id_collateral = $rootScope.id_collateral;
             delete  params.id_collateral_type;
            //delete  params.id_collateral;
            delete  params.collateral_type_name;
            delete  params.collateral_type_template;
            delete  params.created_date_time;
            // params.company_id = $rootScope.id_company;
            crmProjectService.collateral.post(params).then(function(result){
                if(result.status){
                    $state.go('collateral.form-list',{'collateralId':decodeFilter(result.data)})
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            });
        }


        $scope.init();
    })

    .controller('collateralTypeFormCtrl',function($scope,$rootScope,masterService,companyService,crmProjectService,$state,$stateParams,encodeFilter,decodeFilter){

        if($rootScope.collateralParams == undefined){
            $state.go('crm-collateral.form-list',{'collateralId':$stateParams.collateralId});
            return false;
        }

        $rootScope.collateral_id = encodeFilter($stateParams.collateralId);
        $rootScope.collateral_type_id = encodeFilter($stateParams.collateralTypeId);

        $scope.collateralTypeList = [];
        $scope.formData = {};

        $scope.init = function(){
            $scope.getcollateralTypeFormData();
            $scope.collateralStageFormData();
        }
        $scope.getcollateralTypeFormData = function(){
            crmProjectService.collateral.collateralTypeFormData.get({'company_id':$rootScope.id_company,'collateral_id':$rootScope.collateral_id,'collateral_type_id':$rootScope.collateral_type_id}).then(function(result){
                $scope.formData = result.data.collateral_type_field_data;
            });
        }
        $scope.stageFormList = [];
        $scope.collateralStageFormData = function(){
            crmProjectService.collateral.collateralStageFormData.get({'company_id':$rootScope.id_company,'collateral_id':$rootScope.collateral_id,'collateral_type_id':$rootScope.collateral_type_id}).then(function(result){
                $scope.stageFormList = result.data;
            });
        }

        $scope.saveStageFormFiels = function($fieldData,id_collateral_stage){
            console.log($fieldData);
            if($fieldData == undefined){ $rootScope.toast('Error',['Please enter any one field'],'error'); return false; }
            var param =$fieldData.data;
            param.collateral_id = $rootScope.collateral_id;
            param.collateral_stage_id =id_collateral_stage;
            crmProjectService.collateral.collateralStageFormData.post(param).then(function(result){
                if(result.status){
                    $scope.collateralStageFormData();
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            });
        }
        $scope.deleteHistory = function(selected_form,id_collateral_stage){
            crmProjectService.collateral.collateralStageFormData.delete({date:selected_form.date,collateral_stage_id : id_collateral_stage,collateral_id : $rootScope.collateral_id}).then(function(result){
                if(result.status){
                    $scope.collateralStageFormData();
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            });
        }

        $scope.saveFormFieds = function(){
            var params =  $scope.formData;
            params.collateral_id = $rootScope.collateral_id;
            params.collateral_type_id = $rootScope.collateral_type_id;

            crmProjectService.collateral.collateralTypeFormData.post(params).then(function(result){
                if(result.status){
                    $rootScope.toast('Success',result.message);
                } else{
                    $rootScope.toast('Error',result.error,'error');
                }
            });
        }


        $scope.init();
    })

    .controller('collateralAssessmentQuestionsCheckList',function($scope,$rootScope,crmProjectService,crmCompanyService,Upload,encodeFilter,$stateParams,$q,$timeout){

        $scope.categoryQuestionList = [];
        $rootScope.collateral_id = encodeFilter($stateParams.collateralId);
        $rootScope.collateral_type_id = encodeFilter($stateParams.collateralTypeId);

        $scope.getAssessmentQuestion = function(){

            crmProjectService.collateral.collateralQuestion.get({
                collateral_id: $rootScope.collateral_id ,
                collateral_type_id:$rootScope.collateral_type_id,
                company_id:$rootScope.id_company
            }).then(function(result){
                if(result.status){
                   $scope.categoryQuestionList = result.data;
                }
            });
        }
        $scope.getAssessmentQuestion();
        $scope.saveAnswerRestrictions = function(category){
            //console.log('questions',category); return false;
            bulkAnswers = [];
            angular.forEach(category,function(category){
                angular.forEach(category.question,function(question,$index){
                    var $obj = {};
                    $obj.assessment_answer = question.answer.assessment_answer;
                    $obj.assessment_question_id = question.id_assessment_question;
                    $obj.crm_reference_id = $rootScope.currentProjectViewId;
                    $obj.created_by = $rootScope.userId;
                    $obj.company_id = $rootScope.id_company;
                    $obj.id_assessment_question_type = $rootScope.projectParames.formId;
                    $obj.crm_project_id = $rootScope.currentProjectViewId;
                    if(question.answer.id_assessment_answer!=undefined){
                        $obj.id_assessment_answer = question.answer.id_assessment_answer;
                    }

                    $rootScope.loaderOverlay = false;
                    if(question.question_type=='file'){

                        if(question.answer.file.name!=undefined){
                            Upload.upload({
                                url:API_URL+'company/assessmentAnswer',
                                data:{
                                    file:question.answer.file,
                                    'answer':$obj
                                }
                            }).then(function(result){
                                if(result.status){
                                    //$scope.getAssessmentQuestion();
                                    if($index==category.question.length-1){
                                        $scope.getAssessmentQuestion();
                                    }
                                    $rootScope.toast('Success',result.message);
                                } else{
                                    $rootScope.toast('Error',result.error,'error');
                                }


                            });
                        }

                    } else{
                        crmCompanyService.assessmentQuestion.answer.post($obj).then(function(result){
                            if(result.status){
                                $rootScope.toast('Success',result.message);
                                if($index==category.question.length-1){
                                    $scope.getAssessmentQuestion();
                                }
                            } else{
                                $rootScope.toast('Error',result.error,'error');
                            }

                        })
                    }

                })
            })
        }

        $scope.saveAnswer = function(questions){

            var bulkAnswers = [];

            angular.forEach(questions,function(question,$index){
                console.log('question',question);
                var $obj = {};
                $obj.assessment_answer = question.answer.assessment_answer;
                $obj.assessment_question_id = question.id_assessment_question;
                $obj.crm_reference_id = $rootScope.collateral_id;
                $obj.created_by = $rootScope.userId;
                $obj.company_id = $rootScope.id_company;
                $obj.comment = question.answer.comment;
                $obj.type = 'media';
                //$obj.collateral_id = $rootScope.collateral_id;
                $obj.id_assessment_question_type =5;
                if(question.answer.id_assessment_answer!=undefined){
                    $obj.id_assessment_answer = question.answer.id_assessment_answer;
                }

                $rootScope.loaderOverlay = false;

                console.log('question.answer.assessment_answer',question.answer.file.name);
                if(question.answer.file.name!=undefined){
                    Upload.upload({
                        url:API_URL+'company/assessmentAnswer',
                        data:{
                            file:question.answer.file,
                            'answer':$obj
                        }
                    }).then(function(result){

                        if(result.status){
                            //$scope.getAssessmentQuestion();
                            $rootScope.toast('Success',result.message);
                            if($index==questions.length-1){
                                $scope.getAssessmentQuestion();
                            }
                        } else{
                            //$rootScope.toast('Error',result.error,'error');
                        }
                        console.log('$index',$index);
                        console.log('question.length',questions.length);

                    });
                } else{
                    crmCompanyService.assessmentQuestion.answer.post($obj).then(function(result){
                        console.log('$index',$index);
                        if(result.status){
                            $rootScope.toast('Success',result.message);
                            if($index==questions.length-1){
                                $scope.getAssessmentQuestion();
                            }
                        } else{
                            //$rootScope.toast('Error',result.error,'error');
                        }
                        console.log('question.length',questions.length);

                    })
                }


            })
        }


    })







