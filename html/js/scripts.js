/* Naming Conventions
 function name/ methods / variables etc				--- Camel Case (example-- functionName)
 dynamic build / variable 							--- Must start with underscore / _
 GLOBAL VARIABLE in allCaps separated underscore 	--- MY_VAR
 */


var COMPANY_GROUPS_LIST = [
	{"id":0,"Name":"Business Loan","Members":"30 members","Status":null},
	{"id":1,"Name":"Corporate Loan","Members":"14 members","Status":null},
	{"id":2,"Name":"Industrial Loan","Members":"India","Status":null},
	{"id":3,"Name":"Corporate Loan","Members":"India","Status":null},
	{"id":4,"Name":"Business Loan","Members":"India","Status":null},
	{"id":5,"Name":"Corporate Loan","Members":"USA","Status":null},
	{"id":6,"Name":"Industrial Loan","Members":"UK","Status":null},
	{"id":7,"Name":"Corporate Loan","Members":"AUS","Status":null},
	{"id":8,"Name":"Business Loan","Members":"India","Status":null}
]

var PROJECTS_LIST = [
	{"id":0,"Icon":"car","Name":"Car Loan","Amount":"$ 135,500"},
	{"id":0,"Icon":"home","Name":"Home Loan","Amount":"$ 135,500"},
	{"id":0,"Icon":"graduation-cap","Name":"Educational Loan","Amount":"$ 135,500"},
	{"id":0,"Icon":"briefcase","Name":"Business Loan","Amount":"$ 135,500"}
];
//WINDOW - LOAD / RESIZE
$(window).on('load resize', function(){
	//Customheight();
	//loader
	$('.container-fluid.wrapper').addClass('expanded-menu');
	//$('#loader').fadeOut();
	$('.wrapper').fadeIn();
	//details area width & height
	$(' .details-area-x-alpha-result , .details-area-sm, .height-full, .details-area-x-result').height($(window).height()-55);

	//$('.details-area').height($(document).height()-114);
	$('body').find('.col-fixed-width ul.liPadLeft0').mCustomScrollbar({
		autoHideScrollbar:true
	});
});
/*function Customheight(){
 setTimeout(function(){
	 $('#contentArea').height($(window).height()-51);
	 $('.content-container').height($(window).height()-120);} ,1000);

}*/
//DOCUMENT READY
$(function(){

	//Customheight();
	//$('.details-area').height($(document).height()-114);

	$(window).resize();
	//Menu Toggler
	$('body').delegate('#toggleLeftMenu', 'click' , function(){
	//$('#toggleLeftMenu').on('click', function(){
		$(this).closest('.wrapper').toggleClass('expanded-menu');
			
	});
	//Menu Active
	$('#menu > li').on('click', function(){
		$('#menu > li').removeClass('active');
		$(this).addClass('active');
	});

	//Projects Loan list generation
	$('[data-list="projects"]').addClass('groups-list').empty();
	$(PROJECTS_LIST).each(function(i, o) {
		$('ul[data-list="projects"]').append('<li class="relative pl0">'
			+'<i class="fa fa-'+o.Icon+' f24 prl10" style="width:45px;"></i>'
			+'<label>'+o.Name+'<span>'+o.Amount+'</span></label></li>');
	});
	//Projects Loan Selection
	$('[data-list="projects"] li:eq(0)').addClass('selected-result');
	$('[data-list="projects"] li').on('click', function(){
		$('[data-list="projects"] li').removeClass('selected-result');
		$(this).addClass('selected-result');
	});

	/*//Star Rating
	$.each($('.rate-star > span'), function(ind, elem){
		$(elem).on('click', function(){
			$(this).closest('.rate-star').find('span > i.fa').css('color','#ccc');
			$(this).find('i.fa').css('color','#FBCD45');
			$.each($(this).prevAll('span'), function(i,o){
				$(o).find('i.fa').css('color','#FBCD45');
			});
		});
	});
	$.each($('.rate-star > span'), function(i,o){
		if($(o).hasClass('active')){
			$(o).find('i.fa').css('color','#FBCD45');
			$(o).prevAll('span').find('i.fa').css('color','#FBCD45');
		}
	});*/

	//Communication Type Selection
	$('.communication-types span').on('click', function(){
		$('.communication-types span').removeClass('selected-type');
		$(this).addClass('selected-type');
	});


	//Company Groups Generation
	$('ul[data-list="company-groups"]').empty();
	$(COMPANY_GROUPS_LIST).each(function(i, o) {
		if(o.Status==true){
			$('ul[data-list="company-groups"]').append('<li class="relative">'
				+'<i class="fa fa-star green l4 t15 absolute f12"></i>'
				+'<label>'+o.Name+'</label><span>'+o.Members+'</span></li>');
		} else if(o.Status==false){
			$('ul[data-list="company-groups"]').append('<li class="relative">'
				+'<i class="fa fa-star red l4 t15 absolute f12"></i>'
				+'<label>'+o.Name+'</label><span>'+o.Members+'</span></li>');
		} else {
			$('ul[data-list="company-groups"]').append('<li class="relative">'
				+'<i class="fa fa-star transparent l4 t15 absolute f12"></i>'
				+'<label>'+o.Name+'</label><span>'+o.Members+'</span></li>');
		}
	});
	//Company Groups Selection
	$('[data-list="company-groups"] li:eq(0)').addClass('selected-result');
	$('[data-list="company-groups"] li').on('click', function(){
		$('[data-list="company-groups"] li').removeClass('selected-result');
		$(this).addClass('selected-result');
	});

	//Company Result Selection
	$('[data-list="company_results"] li:eq(0)').addClass('selected-result');
	$('[data-list="company_results"] li').on('click', function(){
		$('[data-list="company_results"] li').removeClass('selected-result');
		$(this).addClass('selected-result');
	});
	

	// Expand Search Form
	$('.search-box span').on('click',function(){
		$('.search-box').toggleClass('expand');
		$(this).closest('.search-box').find('input').focus();
	})


//for tabs
	$(".tabs-wrapper li").click(function(){
		$(".tabs-wrapper li").removeClass("current");
		$(this).addClass("current");
		$(".tab-content").hide();
		var x = $(this).find("a").attr("data-tabs"); //Find the rel attribute value to identify the active tab + content
		$(x).fadeIn(); //Fade in the active content
		return false;
	});

	
//for accordian
	/*$(".accordion_head").click(function () {
		if ($('.accordion_body').is(':visible')){
			$(".accordion_body").slideUp(300);
			$(".plusminus").text('+').removeClass("minustext");
			$(".accordion-header").removeClass("acc-expand")
		}
		 if ($(this).next(".accordion_body").is(':visible')) {
			 $(this).next(".accordion_body").slideUp(300);
			 $(this).children(".plusminus").text('+').removeClass("minustext");
			 $(this).removeClass("acc-expand");
		 } else {
			 $(this).next(".accordion_body").slideDown(300);
			 $(this).children(".plusminus").text('-').addClass("minustext");
			 $(this).addClass("acc-expand");
		 }
	});*/
	
	
	
	//Accordian navigation

	$('.nav-prev').click(function(){
		$('.nav-prev,.nav-next').css('opacity','1');
		$('.slick-prev').trigger('click');
		if($('.slick-prev').hasClass('slick-disabled')){
			$('.nav-prev').css('opacity','.3');
		}
	})
	$('.nav-next').click(function(){
		$('.nav-prev,.nav-next').css('opacity','1');
		$('.slick-next').trigger('click');
		if($('.slick-next').hasClass('slick-disabled')){
			$('.nav-next').css('opacity','.3');
		}
	});

	$('body').on('click','.forgot-pwd',function(){
		$(this).parents('.signin-wrapper').parent().addClass('flipped');
	})
	$('body').on('click','.cancel-btn',function(){
		$('.form-wrapper').removeClass('flipped');
	})
});




